﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Benchmark;

/// <summary>
/// ref: https://www.meziantou.net/fastest-way-to-enumerate-a-list-t.htm
/// </summary>
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktItem
{
    public UInt64 _id; ///
	public UInt32 _designId; ///
	public Int32 _count; ///

                         /// <summary>
                         /// 생성자
                         /// </summary>
    public PktItem() { }
}

[RPlotExporter]
public class ListIteration
{
    private List< PktItem > _list = default!;

    [Params( 10, 1_000, 10_000, 100_000, 1_000_000 )]
    public int Size { get; set; }

    [GlobalSetup]
    public void Setup()
    {
        _list = new List< PktItem >( Size );
        _list.Capacity = Size;
        for ( var i = 0; i < Size; i++ )
            _list.Add( new PktItem() );
    }

    [Benchmark( Baseline = true )]
    public void Foreach()
    {
        long count = 0;

        foreach ( var item in _list )
        {
            count += item._count;
        }
    }

    [Benchmark]
    public void List_Foreach()
    {
        long count = 0;

        _list.ForEach( _ => { count += _._count; } );
    }

    [Benchmark]
    public void For()
    {
        long count = 0;
        for ( var i = 0; i < _list.Count; i++ )
        {
            count += _list[ i ]._count;
        }
    }

    [Benchmark]
    public void Foreach_Span()
    {
        long count = 0;
        foreach ( var item in CollectionsMarshal.AsSpan( _list ) )
        {
            count += item._count;
        }
    }

    [Benchmark]
    public void Foreach_SpanRef()
    {
        long count = 0;
        foreach ( ref var item in CollectionsMarshal.AsSpan( _list ) )
        {
            count += item._count;
        }
    }
}

/*
2024.11.09 19:40
<BuiltInComInteropSupport>false</BuiltInComInteropSupport>
| Method       | Size    | Mean           | Error         | StdDev        | Ratio | RatioSD |
|------------- |-------- |---------------:|--------------:|--------------:|------:|--------:|
| Foreach      | 10      |       3.020 ns |     0.0783 ns |     0.1148 ns |  1.00 |    0.05 |
| List_Foreach | 10      |       5.505 ns |     0.0511 ns |     0.0478 ns |  1.83 |    0.07 |
| For          | 10      |       3.177 ns |     0.0809 ns |     0.0717 ns |  1.05 |    0.05 |
| Foreach_Span | 10      |       1.641 ns |     0.0212 ns |     0.0188 ns |  0.54 |    0.02 |
|              |         |                |               |               |       |         |
| Foreach      | 1000    |     226.799 ns |     4.4964 ns |     6.7300 ns |  1.00 |    0.04 |
| List_Foreach | 1000    |     460.981 ns |     4.7301 ns |     4.4245 ns |  2.03 |    0.06 |
| For          | 1000    |     281.110 ns |     4.8376 ns |     4.5251 ns |  1.24 |    0.04 |
| Foreach_Span | 1000    |     183.591 ns |     3.5953 ns |     4.1403 ns |  0.81 |    0.03 |
|              |         |                |               |               |       |         |
| Foreach      | 10000   |   2,313.549 ns |    44.0639 ns |    47.1479 ns |  1.00 |    0.03 |
| List_Foreach | 10000   |   4,589.945 ns |    40.7442 ns |    38.1122 ns |  1.98 |    0.04 |
| For          | 10000   |   2,710.096 ns |    14.8101 ns |    13.1288 ns |  1.17 |    0.02 |
| Foreach_Span | 10000   |   1,798.291 ns |    16.9364 ns |    13.2228 ns |  0.78 |    0.02 |
|              |         |                |               |               |       |         |
| Foreach      | 100000  |  22,656.269 ns |   276.8084 ns |   258.9268 ns |  1.00 |    0.02 |
| List_Foreach | 100000  |  45,631.024 ns |   276.2501 ns |   258.4045 ns |  2.01 |    0.02 |
| For          | 100000  |  27,224.222 ns |   255.2270 ns |   226.2521 ns |  1.20 |    0.02 |
| Foreach_Span | 100000  |  17,953.178 ns |   217.9807 ns |   193.2342 ns |  0.79 |    0.01 |
|              |         |                |               |               |       |         |
| Foreach      | 1000000 | 226,769.062 ns | 1,890.3171 ns | 1,768.2038 ns |  1.00 |    0.01 |
| List_Foreach | 1000000 | 459,763.551 ns | 4,913.4227 ns | 4,596.0187 ns |  2.03 |    0.02 |
| For          | 1000000 | 272,983.757 ns | 5,134.3674 ns | 4,802.6905 ns |  1.20 |    0.02 |
| Foreach_Span | 1000000 | 179,489.323 ns | 2,484.6184 ns | 2,324.1136 ns |  0.79 |    0.01 |

// * Hints *
Outliers
  ListIteration.For: Default          -> 1 outlier  was  removed (4.38 ns)
  ListIteration.Foreach_Span: Default -> 1 outlier  was  removed (2.71 ns)
  ListIteration.Foreach: Default      -> 5 outliers were removed (257.26 ns..278.86 ns)
  ListIteration.For: Default          -> 2 outliers were removed (294.64 ns, 295.11 ns)
  ListIteration.For: Default          -> 1 outlier  was  removed (2.76 us)
  ListIteration.Foreach_Span: Default -> 3 outliers were removed (1.85 us..1.85 us)
  ListIteration.For: Default          -> 1 outlier  was  removed (28.34 us)
  ListIteration.Foreach_Span: Default -> 1 outlier  was  removed, 2 outliers were detected (17.53 us, 18.36 us)
*/

/*
2024.11.09 19:40
List<PktItem>

.NET 8.0
// * Summary *

BenchmarkDotNet v0.14.0, Windows 11 (10.0.22631.4317/23H2/2023Update/SunValley3)
AMD Ryzen 9 7950X3D, 1 CPU, 32 logical and 16 physical cores
.NET SDK 9.0.100-rc.1.24452.12
  [Host]     : .NET 8.0.8 (8.0.824.36612), X64 AOT AVX-512F+CD+BW+DQ+VL+VBMI
  DefaultJob : .NET 8.0.8 (8.0.824.36612), X64 RyuJIT AVX-512F+CD+BW+DQ+VL+VBMI


| Method          | Size    | Mean           | Error         | StdDev        | Ratio | RatioSD |
|---------------- |-------- |---------------:|--------------:|--------------:|------:|--------:|
| Foreach         | 10      |       4.707 ns |     0.1126 ns |     0.1382 ns |  1.00 |    0.04 |
| List_Foreach    | 10      |      16.198 ns |     0.1902 ns |     0.1779 ns |  3.44 |    0.11 |
| For             | 10      |       3.815 ns |     0.0634 ns |     0.0593 ns |  0.81 |    0.03 |
| Foreach_Span    | 10      |       3.314 ns |     0.0459 ns |     0.0430 ns |  0.70 |    0.02 |
| Foreach_SpanRef | 10      |       2.712 ns |     0.0249 ns |     0.0233 ns |  0.58 |    0.02 |
|                 |         |                |               |               |       |         |
| Foreach         | 1000    |     464.550 ns |     9.1298 ns |     8.5400 ns |  1.00 |    0.03 |
| List_Foreach    | 1000    |     616.442 ns |     4.4804 ns |     4.1909 ns |  1.33 |    0.03 |
| For             | 1000    |     368.329 ns |     2.9459 ns |     2.7556 ns |  0.79 |    0.02 |
| Foreach_Span    | 1000    |     366.397 ns |     3.6612 ns |     3.2455 ns |  0.79 |    0.02 |
| Foreach_SpanRef | 1000    |     468.632 ns |     7.5356 ns |     6.6801 ns |  1.01 |    0.02 |
|                 |         |                |               |               |       |         |
| Foreach         | 10000   |   4,560.859 ns |    33.6313 ns |    31.4587 ns |  1.00 |    0.01 |
| List_Foreach    | 10000   |   6,625.539 ns |    39.3508 ns |    36.8088 ns |  1.45 |    0.01 |
| For             | 10000   |   3,712.520 ns |    52.6875 ns |    49.2839 ns |  0.81 |    0.01 |
| Foreach_Span    | 10000   |   4,494.173 ns |    40.8394 ns |    38.2012 ns |  0.99 |    0.01 |
| Foreach_SpanRef | 10000   |   4,724.876 ns |    65.5055 ns |    58.0689 ns |  1.04 |    0.01 |
|                 |         |                |               |               |       |         |
| Foreach         | 100000  |  45,828.919 ns |   302.2648 ns |   282.7387 ns |  1.00 |    0.01 |
| List_Foreach    | 100000  |  68,856.735 ns | 1,021.2030 ns |   955.2340 ns |  1.50 |    0.02 |
| For             | 100000  |  37,011.984 ns |   164.9514 ns |   128.7832 ns |  0.81 |    0.01 |
| Foreach_Span    | 100000  |  39,051.536 ns |   596.4341 ns |   557.9048 ns |  0.85 |    0.01 |
| Foreach_SpanRef | 100000  |  47,138.788 ns |   530.0080 ns |   495.7698 ns |  1.03 |    0.01 |
|                 |         |                |               |               |       |         |
| Foreach         | 1000000 | 464,398.988 ns | 4,724.1840 ns | 4,419.0046 ns |  1.00 |    0.01 |
| List_Foreach    | 1000000 | 686,551.751 ns | 5,939.8722 ns | 5,556.1601 ns |  1.48 |    0.02 |
| For             | 1000000 | 373,882.971 ns | 2,076.9555 ns | 1,621.5503 ns |  0.81 |    0.01 |
| Foreach_Span    | 1000000 | 384,514.974 ns | 6,171.7616 ns | 5,773.0696 ns |  0.83 |    0.01 |
| Foreach_SpanRef | 1000000 | 471,513.128 ns | 4,444.5459 ns | 3,939.9742 ns |  1.02 |    0.01 |


.NET 9.0
// * Summary *

BenchmarkDotNet v0.14.0, Windows 11 (10.0.22631.4317/23H2/2023Update/SunValley3)
AMD Ryzen 9 7950X3D, 1 CPU, 32 logical and 16 physical cores
.NET SDK 9.0.100-rc.1.24452.12
  [Host]     : .NET 9.0.0 (9.0.24.43107), X64 AOT AVX-512F+CD+BW+DQ+VL+VBMI
  DefaultJob : .NET 9.0.0 (9.0.24.43107), X64 RyuJIT AVX-512F+CD+BW+DQ+VL+VBMI


| Method          | Size    | Mean           | Error         | StdDev        | Ratio | RatioSD |
|---------------- |-------- |---------------:|--------------:|--------------:|------:|--------:|
| Foreach         | 10      |       3.397 ns |     0.0395 ns |     0.0369 ns |  1.00 |    0.01 |
| List_Foreach    | 10      |      18.041 ns |     0.2708 ns |     0.2533 ns |  5.31 |    0.09 |
| For             | 10      |       3.229 ns |     0.0592 ns |     0.0554 ns |  0.95 |    0.02 |
| Foreach_Span    | 10      |       1.843 ns |     0.0384 ns |     0.0320 ns |  0.54 |    0.01 |
| Foreach_SpanRef | 10      |       3.026 ns |     0.0586 ns |     0.0548 ns |  0.89 |    0.02 |
|                 |         |                |               |               |       |         |
| Foreach         | 1000    |     197.499 ns |     3.8390 ns |     3.5910 ns |  1.00 |    0.03 |
| List_Foreach    | 1000    |     609.803 ns |     5.0130 ns |     4.6891 ns |  3.09 |    0.06 |
| For             | 1000    |     278.898 ns |     4.1383 ns |     3.8709 ns |  1.41 |    0.03 |
| Foreach_Span    | 1000    |     186.251 ns |     2.5742 ns |     2.2819 ns |  0.94 |    0.02 |
| Foreach_SpanRef | 1000    |     186.768 ns |     2.2519 ns |     1.9963 ns |  0.95 |    0.02 |
|                 |         |                |               |               |       |         |
| Foreach         | 10000   |   1,814.261 ns |    14.2869 ns |    11.9302 ns |  1.00 |    0.01 |
| List_Foreach    | 10000   |   6,312.864 ns |    77.6419 ns |    68.8275 ns |  3.48 |    0.04 |
| For             | 10000   |   2,800.981 ns |    53.0027 ns |    49.5788 ns |  1.54 |    0.03 |
| Foreach_Span    | 10000   |   1,833.490 ns |    22.0745 ns |    20.6485 ns |  1.01 |    0.01 |
| Foreach_SpanRef | 10000   |   1,966.038 ns |    27.5864 ns |    25.8043 ns |  1.08 |    0.02 |
|                 |         |                |               |               |       |         |
| Foreach         | 100000  |  18,015.047 ns |   230.7141 ns |   215.8101 ns |  1.00 |    0.02 |
| List_Foreach    | 100000  |  67,965.425 ns |   521.5094 ns |   435.4840 ns |  3.77 |    0.05 |
| For             | 100000  |  27,335.685 ns |   312.7099 ns |   292.5090 ns |  1.52 |    0.02 |
| Foreach_Span    | 100000  |  18,142.239 ns |   271.0872 ns |   226.3701 ns |  1.01 |    0.02 |
| Foreach_SpanRef | 100000  |  22,102.309 ns |   242.6332 ns |   215.0880 ns |  1.23 |    0.02 |
|                 |         |                |               |               |       |         |
| Foreach         | 1000000 | 181,951.864 ns | 2,467.6990 ns | 2,308.2872 ns |  1.00 |    0.02 |
| List_Foreach    | 1000000 | 684,474.163 ns | 7,153.7673 ns | 6,341.6284 ns |  3.76 |    0.06 |
| For             | 1000000 | 272,024.541 ns | 3,249.3126 ns | 3,039.4090 ns |  1.50 |    0.02 |
| Foreach_Span    | 1000000 | 186,601.324 ns | 3,639.7613 ns | 3,574.7323 ns |  1.03 |    0.02 |
| Foreach_SpanRef | 1000000 | 221,422.339 ns | 4,289.1244 ns | 4,212.4937 ns |  1.22 |    0.03 |

*/