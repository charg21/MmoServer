﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class HashGenerator
{
    public static string Generate( string text )
    {
        // SHA256Manager를 통해 생성
        using var sha256 = new System.Security.Cryptography.SHA256Managed();

        byte[] textBytes = System.Text.Encoding.UTF8.GetBytes( text );
        byte[] hashBytes = sha256.ComputeHash( textBytes );

        return BitConverter.ToString( hashBytes ).Replace( "-", string.Empty );
    }
}
