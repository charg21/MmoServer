﻿public enum EClassType
{
    Enum,
    Struct,
    Packet,
    PacketHandler,
    PacketDispatcher,
    PacketFactory,
    ProtocolDb,
}