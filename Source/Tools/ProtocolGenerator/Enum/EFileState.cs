﻿public enum EFileState
{
    None,
    Create,
    Edited,
    Removed,
}