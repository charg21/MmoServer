﻿public enum EPacketType
{
    Request,
    Response,
    Notify
}