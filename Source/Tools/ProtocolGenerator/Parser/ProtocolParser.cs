using CodeGen;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Loader;

public static class ProtocolParser
{
    private static readonly HashSet< string > _enums = [];
    private static readonly HashSet< PacketBuilder > _packets = [];
    private static readonly HashSet< StructBuilder > _structs = [];

    static HashSet< Assembly > referencedAssemblies =
    [
        Assembly.Load( new AssemblyName( "Microsoft.CSharp" ) ),
        Assembly.Load( new AssemblyName( "netstandard" ) ),
        Assembly.Load( new AssemblyName( "System.Runtime" ) ),
        Assembly.Load( new AssemblyName( "System.Collections" ) ),
        Assembly.Load( new AssemblyName( "System.Linq" ) ),
        Assembly.Load( new AssemblyName( "System.Linq.Expressions" ) ),
        typeof( object ).Assembly,
        typeof( PacketSchema ).GetTypeInfo().Assembly,
        typeof( ValueList<> ).GetTypeInfo().Assembly,
    ];

    private static List< PortableExecutableReference > _references;
    private static CSharpCompilationOptions _options = new CSharpCompilationOptions( OutputKind.WindowsRuntimeMetadata );

    static ProtocolParser()
    {
        _references = referencedAssemblies
                        .Select( assembly => MetadataReference.CreateFromFile( assembly.Location ) ).ToList();
    }


    public static List< ICodeBuilder > ParseCode( string packetCode )
    {
        const string dependency = "using System.Collections.Generic;";
        var assembly = Compile( dependency + packetCode );
        if ( assembly is null )
            return null;

        var fromToList = new HashSet< ( string from, string to ) >();
        var codeGenList = new List< ICodeBuilder >();
        foreach ( var typeInfo in assembly.DefinedTypes )
            ParseSchema( fromToList, typeInfo ).ForEach( codeGen => codeGenList.Add( codeGen ) );
        PostParseSchema( fromToList ).ForEach( codeGen => codeGenList.Add( codeGen ) );

        return codeGenList;
    }

    private static List< ICodeBuilder > PostParseSchema( HashSet<( string from, string to )> fromToList )
    {
        var codeGenList = new List< ICodeBuilder >();

        // post gen
        var packetTypeBuilder = new EnumBuilder( "EPacketType", _packets );
        packetTypeBuilder.HasMaxField = true;
        packetTypeBuilder.UnderlyingType = "UInt16";
        codeGenList.Add( packetTypeBuilder );

        foreach ( var ( from, to ) in fromToList )
            codeGenList.Add( new PacketDispatcherBuilder( from, _packets, from, to ) );

        // for c#
        if ( Config.NeedProtocolDb )
            codeGenList.Add( new ProtocolDbBuilder( _packets ) );

        var factoryGen = new PacketFactoryBuilder( _packets );
        codeGenList.Add( factoryGen );

        return codeGenList;
    }

    private static List< ICodeBuilder > ParseSchema(
        HashSet< ( string from, string to ) > fromToList,
        System.Reflection.TypeInfo            typeInfo )
    {
        if ( typeInfo.IsEnum )
        {
            var @enum = new EnumBuilder( typeInfo );
            _enums.Add( typeInfo.Name );
            return [ @enum ];
        }

        var pktSchema = typeInfo.GetCustomAttribute< PacketSchema >();
        if ( pktSchema is not null )
        {
            var pktBuilder    = new PacketBuilder( pktSchema, typeInfo );
            var handleBuilder = new PacketHandlerBuilder( pktSchema, typeInfo );

            _packets.Add( pktBuilder );
            fromToList.Add( (pktSchema.From, pktSchema.To) );

            return [ pktBuilder, handleBuilder ];
        }

        var structSchema = typeInfo.GetCustomAttribute< StructSchema >();
        if ( structSchema is not null )
        {
            var strt = new StructBuilder( structSchema, typeInfo );
            _structs.Add( strt );

            return [ strt ];
        }

        return null;
    }

    public static Assembly? Compile( string code )
    {
        var syntaxTree = CSharpSyntaxTree.ParseText( code );
        var compilation = CSharpCompilation.Create( "Protocol", [ syntaxTree ], _references, _options );

        using var  memoryStream = new MemoryStream();
        EmitResult result       = compilation.Emit( memoryStream );
        if ( !result.Success )
        {
            Console.WriteLine( $"코드 컴파일 실패...{result.Diagnostics}" );

            var failures = result.Diagnostics.Where( diagnostic =>
                                                         diagnostic.IsWarningAsError || diagnostic.Severity ==
                                                         DiagnosticSeverity.Error );

            foreach ( Diagnostic diagnostic in failures )
                Console.Error.WriteLine( $"{ diagnostic.Id } : { diagnostic.GetMessage() }" );

            return null;
        }

        memoryStream.Seek( 0, SeekOrigin.Begin );

        return AssemblyLoadContext.Default.LoadFromStream( memoryStream );
    }
}

