﻿using System.Text;
using System.Reflection;
using System.Text.Json;
using CodeGen;

/// <summary>
/// 설정
/// </summary>
public static class Config
{
    public static List< string > Spaces { get; private set; } = new();
    public static List< string > Tabs { get; private set; } = new();
    public static List< string > NextLines { get; private set; } = new();

    static Config()
    {
        var spaceBuilder    = new StringBuilder();
        var tabBuilder      = new StringBuilder();
        var nextLineBuilder = new StringBuilder();

        Spaces.Add( string.Empty );
        Tabs.Add( string.Empty );
        NextLines.Add( string.Empty );

        for ( int i = 0; i < 100; i += 1 )
        {
            spaceBuilder.Append( ' ' );
            tabBuilder.Append( '\t' );
            nextLineBuilder.Append( "\r\n" );

            Spaces.Add( spaceBuilder.ToString() );
            Tabs.Add( tabBuilder.ToString() );
            NextLines.Add( nextLineBuilder.ToString() );
        }
    }

    /// <summary>
    /// 모아서 카피
    /// </summary>
    public static bool CanBulkCopy = false;

    /// <summary>
    ///  모든 테이블 강제 생성
    /// </summary>
    public static bool ForceGenerate = false;

    /// <summary>
    /// 로그를 남길지 여부
    /// </summary>
    public static bool UseConsoleLog = false;

    /// <summary>
    /// 로그를 남길지 여부
    /// </summary>
    public static bool AsyncIO = false;

    /// <summary>
    /// Task Async Pattern 사용 여부
    /// </summary>
    public static bool UseTap = false;

    /// <summary>
    /// enum 파일을 통합시킬지 여부
    /// </summary>
    public static bool UseUnityEnum = false;

    /// <summary>
    /// 언어
    /// </summary>
    public static string Language = "cs";


    // 경로
    public static string ServerPacketDestPath = string.Empty; //<
    public static string ServerPacketHandlerDestPath = string.Empty; //<
    public static string ClientPacketDestPath = string.Empty;
    public static string ClientPacketHandlerDestPath = string.Empty; //<
    public static string ClientPacketHeader = string.Empty;
    public static string ServerPacketHeader = string.Empty;

    public static string SchemaPath = string.Empty;
    public static string UnityEnumName = string.Empty; //< 통합 enum 이름

    #region OutputPath
    /// <summary>
    /// 열거형 경로
    /// </summary>
    public static string EnumPath = string.Empty;

    /// <summary>
    /// 패킷 경로
    /// </summary>
    public static string PacketPath = string.Empty;

    /// <summary>
    /// 패킷 핸들러 경로
    /// </summary>
    public static string PacketHandlerPath = string.Empty;

    /// <summary>
    ///
    /// </summary>
    // public static List< string > PacketHandlerPathList = new();
    public static List< PathDto > PacketHandlerPathList = new();

    /// <summary>
    /// 패킷 디스패처 경로
    /// </summary>
    public static string PacketDispatcherPath = string.Empty;

    /// <summary>
    /// 패킷 디스패처 경로
    /// </summary>
    // public static List< string > PacketDispatcherPathList = new();
    public static List< PathDto > PacketDispatcherPathList = new();

    /// <summary>
    /// 서버 패킷 디스패처 경로
    /// </summary>
    public static string ServerPacketDispatcherPath = string.Empty;

    /// <summary>
    /// 클라이언트 패킷 디스패처 경로
    /// </summary>
    public static string ClientPacketDispatcherPath = string.Empty;

    /// <summary>
    /// 패킷 팩토리 경로
    /// </summary>
    public static string PacketFactoryPath = string.Empty;
    #endregion

    /// <summary>
    /// 플랫폼
    /// </summary>
    public static ServerType ServerType = ServerType.Cpp;   //<
    public static ClientType ClientType = ClientType.Unity; //<

    /// <summary>
    /// 프로젝트 설정
    /// </summary>
    public static string ProjectName { get; set; } = string.Empty;

    /// <summary>
    ///
    /// </summary>
    public static bool NeedProtocolDb { get; internal set; }


    public static string ProjectPrefix   = string.Empty;
    public static string OutputPath      = "Output";
    public static bool   NeedDebugString = true;

    public static void Load( string[] args )
    {
        var configPath = $"{ nameof( ProtocolGenerator ) }.json";

        if ( args.Length > 0 && !string.IsNullOrEmpty( args[ 0 ] ) )
            configPath = args[ 0 ];

        var configString = Importer.Import( configPath );
        Console.WriteLine( $"Config Path = {configPath}" );

        /// Json 역직렬화를 진행한다.
        var config = JsonSerializer.Deserialize< ConfigDto >( configString );
        ImportFrom( in config );

        Init();
    }

    private static void Init()
    {
        //ExtractToList( PacketDispatcherPath, PacketDispatcherPathList );
        //ExtractToList( PacketHandlerPath, PacketHandlerPathList );
    }

    private static void ExtractToList( string path, List< string > pathList )
    {
        var paths = path.Split( "," );
        foreach ( var p in paths )
            pathList.Add( p );
    }

    /// <summary>
    /// 리플렉션을 사용하여 환경설정을 반영한다.
    /// </summary>
    private static void ImportFrom< T >( in T config )
    {
        foreach ( var propConfig in config.GetType().GetProperties() )
        {
            var fieldEnv = typeof( Config ).GetField( propConfig.Name );
            if ( fieldEnv != null )
            {
                fieldEnv.SetValue( null, propConfig.GetValue( config ) );
                Console.WriteLine( $"{ propConfig.Name } = { propConfig.GetValue( config ) } " );
            }
        }
    }
}

/// <summary>
/// 경로 데이터 전달 객체
/// </summary>
public struct PathDto
{
    /// <summary>
    /// 생성자
    /// </summary>
    public PathDto()
    {
    }

    /// <summary>
    /// 목적지
    /// </summary>
    public string To { get; set; } = string.Empty;

    /// <summary>
    /// 경로
    /// </summary>
    public string Path { get; set; } = string.Empty;
}


/// <summary>
/// 설정 전달 객체
/// </summary>
public struct ConfigDto
{
    /// <summary>
    /// 생성자
    /// </summary>
    public ConfigDto()
    {
    }

    /// <summary>
    /// 모아서 카피
    /// </summary>
    public bool CanBulkCopy { get; set; } = false;

    /// <summary>
    ///  모든 테이블 강제 생성
    /// </summary>
    public bool ForceGenerate { get; set; } = false;

    /// <summary>
    /// 로그를 남길지 여부
    /// </summary>
    public bool UseConsoleLog { get; set; } = false;

    /// <summary>
    /// 로그를 남길지 여부
    /// </summary>
    public bool AsyncIO { get; set; } = false;

    /// <summary>
    /// Task Async Pattern 사용 여부
    /// </summary>
    public bool UseTap { get; set; } = false;

    /// <summary>
    /// enum 파일을 통합시킬지 여부
    /// </summary>
    public bool UseUnityEnum { get; set; } = false;

    /// <summary>
    /// 언어
    /// </summary>
    public string Language { get; set; } = "cs";

    // 경로
    public string ServerPacketDestPath { get; set; } = string.Empty; //<
    public string ServerPacketHandlerDestPath { get; set; } = string.Empty; //<
    public string ClientPacketDestPath { get; set; } = string.Empty; //<
    public string ClientPacketHandlerDestPath { get; set; } = string.Empty; //<
    public string ClientPacketHeader { get; set; } = string.Empty;
    public string ServerPacketHeader { get; set; } = string.Empty;
    public string SchemaPath { get; set; } = string.Empty;


#region OutputPath
    /// <summary>
    /// 열거형 경로
    /// </summary>
    public string EnumPath { get; set; } = string.Empty;

    /// <summary>
    /// 패킷 경로
    /// </summary>
    public string PacketPath { get; set; } = string.Empty;

    /// <summary>
    /// 패킷 핸들러 경로
    /// </summary>
    public string PacketHandlerPath { get; set; } = string.Empty;

    /// <summary>
    /// 패킷 디스패처 경로
    /// </summary>
    public string PacketDispatcherPath { get; set; } = string.Empty;

    /// <summary>
    /// 패킷 핸들러 경로
    /// </summary>
    public List< PathDto > PacketHandlerPathList { get; set; } = new();

    /// <summary>
    /// 패킷 디스패처 경로
    /// </summary>
    public List< PathDto > PacketDispatcherPathList { get; set; } = new();

    /// <summary>
    /// 서버 패킷 디스패처 경로
    /// </summary>
    public string ServerPacketDispatcherPath { get; set; } = string.Empty;

    /// <summary>
    /// 클라이언트 패킷 디스패처 경로
    /// </summary>
    public string ClientPacketDispatcherPath { get; set; } = string.Empty;

    /// <summary>
    /// 패킷 팩토리 경로
    /// </summary>
    public string PacketFactoryPath { get; set; } = string.Empty;
#endregion

    /// <summary>
    ///  통합 enum 이름
    /// </summary>
    public string UnityEnumName { get; set; } = string.Empty;

    /// <summary>
    ///
    /// </summary>
    public bool NeedProtocolDb { get; set; } = false;

    public ServerType ServerType { get; set; } = ServerType.Cpp;   //<
    public ClientType ClientType { get; set; } = ClientType.Unity; //<

    /// <summary>
    /// 프로젝트 이름
    /// </summary>
    public string ProjectName { get; set; } = string.Empty;

    /// <summary>
    /// 프로젝트 접두사
    /// </summary>
    public string ProjectPrefix { get; set; } = string.Empty;

    /// <summary>
    /// 출력 경로
    /// </summary>
    public string OutputPath { get; set; } = string.Empty;
    public bool NeedDebugString { get; set; } = true;
}
