[AttributeUsage( AttributeTargets.Class )]
public class StructSchema : System.Attribute
{
    public string Category { get; set; }

    public StructSchema( string category )
    {
        Category = category;
    }
}
