﻿[AttributeUsage( AttributeTargets.Class )]
public class PacketSchema : System.Attribute
{
    /// <summary>
    /// 종류
    /// </summary>
    public string Category { get; set; }

    /// <summary>
    ///
    /// </summary>
    public string From { get; set; }

    /// <summary>
    ///
    /// </summary>
    public string To { get; set; }

    /// <summary>
    ///
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// 사용처
    /// </summary>
    public string Use { get; set; }

    /// <summary>
    /// 생성자
    /// </summary>
    public PacketSchema( string from, string to, string category, string use = "" )
    {
        From     = from;
        To       = to;
        Category = category;
        Use      = use;
    }

}
