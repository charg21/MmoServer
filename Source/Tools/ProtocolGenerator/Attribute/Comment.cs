﻿public class Comment : System.Attribute
{
    public string Message { get; set; }

    public override string ToString() => Message;
    public Comment( string message ) { Message = message; }
}
