﻿// See https://aka.ms/new-console-template for more information

using CodeGen;
using System.Diagnostics;

Stopwatch watch = Stopwatch.StartNew();

GC.TryStartNoGCRegion( 4L * 1024 * 1024 * 1024 );

// Code import -> parse -> generate -> export
Config.Load( args );

var code       = Importer.Import( Config.SchemaPath );
var packetList = ProtocolParser.ParseCode( code );

//// 메인 루프 처리
//if ( Config.Language == "cs" )
//    ProtocolGenerator.Generate( packetList );
//else
    ProtocolGenerator.GenerateCpp( packetList );

// TAP
//var codeGenTasks = new List< Task >();
//foreach ( var packet in packetList )
//    codeGenTasks.Add( Task.Run( () => ProtocolGenerator.Generate( packet ) ) );

//Task.WaitAll( codeGenTasks.ToArray() );

Console.WriteLine( $"Total Elapsed Time : { watch.ElapsedMilliseconds } ms" );
watch.Stop();
Console.ReadLine();