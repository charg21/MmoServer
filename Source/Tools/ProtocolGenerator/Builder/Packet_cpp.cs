﻿using System.Reflection;
using System.Text;
using CodeGen;

public partial class PacketBuilder : ICodeBuilder
{
    public void Build( CodeGenContextCpp context )
    {
        _GenImport( context );
        using ( var classScope = _GenClassHeader( context ) )
        {
            _GenField( context );

            _GenWrite( context );
            _GenRead( context );
        }
        context.NextLine();
        context.WriteLine( ";" );
        context.WriteLine( "#pragma pack( pop )" );

        //if ( Environment.NeedDebugString )
        //{
        //    context.NextLine().
        //            WriteLine( "Mala::Container::String ToString()" ).
        //            Scope();

        //    context.WriteLine( "Mala::Container::String str;" );

        //    foreach ( var ( _, field ) in FieldList )
        //    {
        //        context.WriteLine( $"str += \"{ field.Name }: \";" );
        //        context.WriteLine( $"str += std::to_wstring( { field.Name } );" );
        //    }

        //    context.NextLine().
        //            WriteLine( "return std::move( str )" ).
        //            Scope();
        //}
    }

    private CodeGenContextScope _GenClassHeader( CodeGenContextCpp context )
    {
        context.WriteLine( "#pragma pack( push, 1 )" );

        context.WriteLine( $"export class {Type}" );

        return context.Scope2();
    }


    private void _GenField( CodeGenContextCpp context )
    {
        if ( FieldList.Count != 0 )
            context.Public();

        // Field, Property
        foreach ( var ( _, field ) in FieldList )
        {
            context.WriteLine( $"{ field.CppType() } { field.Name }; /// { field.Comment }" );
        }
    }

    private void _GenRead( CodeGenContextCpp context )
    {
        context.NextLine().
                WriteLine( "void Read( BYTE* buffer, int len )" ).
                Scope();

        context.WriteLine( "int count{ 4 };" ).
                NextLine();

        // Read
        foreach ( var ( _, field ) in FieldList )
        {
            field.GenReadCode( context );
        }

        context.UnScope().
                NextLine();
    }

    private void _GenWrite( CodeGenContextCpp context )
    {
        context.NextLine();
        context.Public();
        using ( var scope = context.WriteLine( "SendBufferPtr Write()" ).Scope2() )
        {
            context.WriteLine( "auto s = GSendBufferManager->Open( 1024 );" ).
                    WriteLine( $"bool success = BitConverter::TryWriteBytes( s, sizeof( u16 ), (u16)( EPacketType::{Type} ) );" ).
                    WriteLine( "i32 count{ sizeof( u32 ) };" ).
                    NextLine();

            foreach ( var ( _, field ) in FieldList )
            {
                field.GenWriteCode( context );
            }

            context.WriteLine( "BitConverter::TryWriteBytes( s, 0, u16( count ) );" )
                   .WriteLine( "s->Close( count );" )
                   .WriteLine( "return std::move( s );" );
        }

        context.NextLine();
    }

    private void _GenImport( CodeGenContextCpp ctx )
    {
        ctx.WriteLine( $@"export module {Category}Packet.{Type};" ).
                NextLine();

        ctx.WriteLine( @"import Mala.Core.CoreGlobal;
import Mala.Core.Types;
import Mala.Container.String;
import Mala.Text;
import Mala.Net.Buffer;
import Mala.Net.SendBuffer;
import Mala.Net.PacketHeader;
import EPacketType;" ).
NextLine();


        HashSet< string > firstImport  = new();
        HashSet< string > secondImport = new();
        foreach ( var ( _, field ) in FieldList )
        {
            if ( !field.IsCollectionType )
            {
                if ( field.Type == "String" )
                    firstImport.Add( "string" );

                if ( !field.IsDefaultType )
                    secondImport.Add( field.Type );

                continue;
            }

            firstImport.Add( field.ContainerType );

            foreach ( var t in field.GenericTypes )
            {
                if ( IsDefaultType( t ) )
                {
                    if ( t == "String" )
                        firstImport.Add( t );

                    continue;
                }

                secondImport.Add( t );
            }
        }

        foreach ( var t in firstImport )
        {
            ctx.WriteLine( $"import <{t}>;" );
        }

        foreach ( var t in secondImport )
        {
            ctx.WriteLine( $"import {t};" );
        }

        ctx.NextLine();

        ctx.WriteLine( $"using namespace std;" ).
            WriteLine( $"using namespace Mala::Net;" ).
            NextLine();
    }


    public static bool IsDefaultType( string type )
    {
        switch ( type )
        {
            case "string": return true;
            case "String": return true;
        }

        return IsDefaultValueType( type );
    }


    /// <summary>
    /// 기본 값 타입 여부를 반환한다.
    /// </summary>
    public static bool IsDefaultValueType( string type )
    {
        return type switch
        {
            "bool" => true,
            "Boolean" => true,
            "byte" => true,
            "Byte" => true,
            "char" => true,
            "Char" => true,
            "int" => true,
            "Int32" => true,
            "uint" => true,
            "UInt32" => true,
            "short" => true,
            "Int16" => true,
            "long" => true,
            "Int64" => true,
            "ulong" => true,
            "UInt64" => true,
            "ushort" => true,
            "UInt16" => true,
            "float" => true,
            "double" => true,
            "Single" => true,
            "Double" => true,

            _ => false,
        };
    }
}