using System.Reflection;
using CodeGen;

public class StructBuilder : ICodeBuilder
{
    /// <summary>
    /// 출력 경로
    /// </summary>
    public List< string > OutputPath => [ Config.PacketPath ];
    public string CodeType => EClassType.Packet.ToString();
    public string Category { get; set; } = string.Empty;
    public string Type { get; set; } = string.Empty;
    public string Comment { get; set; } = string.Empty;
    public bool HasCustemSerialization { get; set; } = false;
    public bool HasCustemSerializationInAnyField { get; set; } = false;
    public Dictionary< int, FieldBuilder > FieldList { get; set; } = new();

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    public bool NeedOverwrite => true;

    public string FileName => Type;

    public StructBuilder( StructSchema schema, TypeInfo typeInfo )
    {
        Category                         = schema.Category;
        Type                             = typeInfo.Name;
        Comment                          = typeInfo.GetCustomAttribute< Comment >()?.Message;
        HasCustemSerialization           = typeInfo.GetCustomAttribute< CustomSerialization >() is not null;
        HasCustemSerializationInAnyField = HasCustemSerialization || typeInfo.DeclaredFields.Any( field =>
                                                field.GetCustomAttribute< CustomSerialization >() is not null ||
                                                field.FieldType.GetCustomAttributes< CustomSerialization >().Any() );
        int n = 0;
        foreach ( var pktField in typeInfo.DeclaredFields )
        {
            FieldList.Add( n++, new FieldBuilder( pktField ) );
        }
    }

    static bool IsCollection( string type ) => type.Contains( '`' );

    public void Build( CodeGenContext ctx )
    {
        GenImport( ctx );

        // TODO Import 추가
        ctx.WriteLine( $"[StructLayout( LayoutKind.Sequential, Pack = 1 )]" );
        using ( var structScope = ctx.WriteLine( $"public partial struct {Type}" ).Scope2() )
        {
            // Field, Property
            foreach ( var ( _, field ) in FieldList )
            {
                if ( field.IsDefaultValueOrEnumType )
                    ctx.WriteLine( $"public {field.Type} {field.Name}; /// {field.Comment}" );
                else if ( field.IsString )
                    ctx.WriteLine( $"public {field.Type} {field.Name} = string.Empty;  /// {field.Comment}" );
                else
                    ctx.WriteLine( $"public {field.Type} {field.Name} = new();  /// {field.Comment}" );
            }
            ctx.NextLine();

            // Ctor
            ctx.Comment( "생성자" );
            ctx.WriteLine( $"public {Type}(){{}}" );
            ctx.NextLine();

            if ( HasCustemSerialization )
                return;

            /// Write
            using ( var writeScope = ctx.WriteLine( $"public bool Write( Span< byte > s, ref ushort count )" ).Scope2() )
            {
                if ( FieldList.Count != 0 && !HasCustemSerializationInAnyField )
                {
                    /// 직렬화 최적화
                    ctx.WriteLine( $"if ( !RuntimeHelpers.IsReferenceOrContainsReferences< {Type} >() )" ).
                        Scope().
                        WriteLine( $"Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );" ).
                        WriteLine( $"count += (ushort)( Unsafe.SizeOf< {Type} >() );" ).
                        WriteLine( $"return true;" ).
                        UnScope().
                        NextLine();
                }

                ctx.WriteLine( "bool success = true;" ).
                    NextLine();

                foreach ( var ( _, field) in FieldList )
                {
                    field.GenWriteCode( ctx );
                }

                ctx.NextLine().
                    WriteLine( "return success;" );
            }
            ctx.NextLine();

            /// Read
            using ( var readScope = ctx.WriteLine( $"public void Read( ReadOnlySpan< byte > s, ref ushort count )" ).Scope2() )
            {
                if ( FieldList.Count != 0 && !HasCustemSerializationInAnyField )
                {
                    /// 역직렬화 최적화
                    ctx.WriteLine( $"if ( !RuntimeHelpers.IsReferenceOrContainsReferences< {Type} >() )" )
                       .Scope()
                       .WriteLine( $"this = Unsafe.ReadUnaligned< {Type} >( ref MemoryMarshal.GetReference( s[ count.. ] ) );" )
                       .WriteLine( $"count += (ushort)( Unsafe.SizeOf< {Type} >() );" )
                       .WriteLine( $"return;" )
                       .UnScope()
                       .NextLine();
                }

                foreach ( var ( _, field ) in FieldList )
                {
                    field.GenReadCode( ctx );
                }
            }
            ctx.NextLine();
        }
    }

    private static void GenImport( CodeGenContext context )
    {
        context.WriteLine( "using System;" )
               .WriteLine( "using System.Collections.Generic;" )
               .WriteLine( "using System.Runtime.CompilerServices;" )
               .WriteLine( "using System.Runtime.InteropServices;" )
               .WriteLine( "using System.Text;" )
               .WriteLine( "using Mala.Net;" );
    }


    public void Build( CodeGenContextCpp ctx )
    {
        // TODO Import 추가
        // Class
        ctx.WriteLine( $"export module {Type};" ).
            NextLine();

        ctx.WriteLine( $"import Mala.Core.Types;" ).
            WriteLine( $"import Mala.Core.TypeTraits;" ).
            WriteLine( $"import Mala.Container.String;" ).
            WriteLine( $"import Mala.Text;" ).
            WriteLine( $"import Mala.Net.Buffer;" ).
            WriteLine( $"import Mala.Net.SendBuffer;" ).
            WriteLine( $"import Mala.Net.PacketHeader;" ).
            NextLine();

        _GetDependencyImport( ctx );
        ctx.NextLine();

        ctx.WriteLine( $"using namespace std;" ).
            WriteLine( $"using namespace Mala::Net;" ).
            NextLine();

        ctx.WriteLine( "#pragma pack( push, 1 )" ).
            NextLine();

        using ( var classScope = ctx.WriteLine( $"export class {Type}" ).Scope2() )
        {
            ctx.Public();
            // Field, Property
            foreach ( var (_, field) in FieldList )
            {
                ctx.WriteLine( $"{field.CppType()} {field.Name}; /// {field.Comment}" );
            }
            ctx.NextLine();

            ctx.Public();
            // Write
            using ( var writeScope = ctx.WriteLine( "bool Write( SendBufferRef s, int& count )" ).Scope2() )
            {
                // Fast Write
                using ( var _ = ctx.WriteLine( $"if constexpr ( IsPod< {Type} >::value )" ).Scope2() )
                {
                    ctx.WriteLine( $"std::memcpy( s->Buffer() + count, this, sizeof( {Type} ) );" ).
                        WriteLine( $"count += sizeof( {Type} );" ).
                        WriteLine( $"return true;" );
                }
                using ( var @else = ctx.WriteLine( "else" ).Scope2() )
                {
                    ctx.WriteLine( "bool success = true;" );
                    foreach ( var ( _, field ) in FieldList )
                    {
                        field.GenWriteCode( ctx );
                    }

                    ctx.WriteLine( "return success;" );
                }
            }
            ctx.NextLine();

            using ( var readScope = ctx.WriteLine( "void Read( BYTE* buffer, int& count )" ).Scope2() )
            {
                using ( var _ = ctx.WriteLine( $"if constexpr ( IsPod< {Type} >::value )" ).Scope2() )
                {
                    ctx.WriteLine( $"std::memcpy( this, buffer + count, sizeof( {Type} ) );" ).
                        WriteLine( $"count += sizeof( {Type} );" ).
                        WriteLine( $"return;" );
                }
                using ( var @else = ctx.WriteLine( "else" ).Scope2() )
                {
                    // Deserializer
                    foreach ( var ( _, field ) in FieldList )
                    {
                        field.GenReadCode( ctx );
                    }
                }
            }
            ctx.NextLine();
        }

        ctx.WriteLine( ";" );
        ctx.WriteLine( "#pragma pack( pop )" );
    }

    private void _GetDependencyImport( CodeGenContextCpp ctx )
    {
        HashSet< string > firstImport  = new();
        HashSet< string > secondImport = new();
        foreach ( var (_, field) in FieldList )
        {
            if ( !field.IsCollectionType )
            {
                if ( field.Type == "String" )
                    firstImport.Add( "string" );

                if ( !field.IsDefaultType )
                {
                    secondImport.Add( field.Type );
                }

                continue;
            }

            firstImport.Add( field.ContainerType );

            foreach ( var t in field.GenericTypes )
            {
                if ( CodeHelper.IsDefaultType( t ) )
                {
                    if ( t == "String" )
                        firstImport.Add( t );

                    continue;
                }

                secondImport.Add( t );
            }
        }

        foreach ( var t in firstImport )
        {
            ctx.WriteLine( $"import <{t}>;" );
        }

        foreach ( var t in secondImport )
        {
            ctx.WriteLine( $"import {t};" );
        }
    }
}
