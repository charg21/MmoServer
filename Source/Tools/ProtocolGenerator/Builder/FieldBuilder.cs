using CodeGen;
using Microsoft.CodeAnalysis;
using System.Reflection;

public partial class FieldBuilder
{
    public FieldBuilder( FieldInfo pktField )
    {
        Name                   = pktField.Name;
        Type                   = pktField.FieldType.Name;
        Comment                = pktField.GetCustomAttribute< Comment >()?.Message;
        IsEnum                 = pktField.FieldType.IsEnum;
        IsNullable             = Nullable.GetUnderlyingType( pktField.FieldType ) is not null;
        HasCustemSerialization = pktField.GetCustomAttribute< CustomSerialization >() is not null;

        if ( IsEnum )
            UnderlyingType = Enum.GetUnderlyingType( pktField.FieldType ).Name;

        if ( IsNullable )
            Type = Nullable.GetUnderlyingType( pktField.FieldType )!.Name;

        if ( IsCollection( Type ) )
        {
            GenericTypes   = new();
            CollectionType = pktField.FieldType.Name.Split( '`' )[ 0 ];
            Type           = CollectionType + "< ";

            foreach ( var a in pktField.FieldType.GenericTypeArguments )
            {
                Type += a.Name;
                GenericTypes.Add( a.Name );
            }

            Type += " >";
        }
    }

    public string Type { get; set; }
    public string? CollectionType { get; set; }
    public List< string >? GenericTypes { get; set; }
    public string Name { get; set; }
    public string Comment { get; set; }
    public bool IsEnum { get; set; }
    public string UnderlyingType { get; set; } = "Int32";
    public bool IsNullable { get; set; }

    static bool IsCollection( string type ) => type.Contains( '`' );

    public bool IsCollectionType => GenericTypes != null;

    public string ContainerType => TypeUtil.CollectionToContainer( CollectionType );

    public bool IsDefaultType => CodeGen.CodeHelper.IsDefaultType( Type );

    public bool IsDefaultOrEnumType => CodeGen.CodeHelper.IsDefaultType( Type ) || IsEnum;

    public bool IsDefaultValueOrEnumType => CodeGen.CodeHelper.IsDefaultValueType( Type ) || IsEnum;

    public bool IsString => Type.Equals( "string" ) || Type.Equals( "String" );
    public bool IsList => Type.StartsWith( "List<" ) || Type.StartsWith( "ValueList<" );

    public bool HasCustomReadWrite { get; internal set; }
    public bool HasCustemSerialization { get; internal set; }

    //|| Type.EndsWith( ">" );

    public string CppType()
    {
        if ( IsCollectionType )
        {
            var containerType = TypeUtil.CollectionToContainer( CollectionType );

            containerType += "< ";
            foreach ( var type in GenericTypes )
            {
                containerType += TypeUtil.ToCppType( type );
                containerType += ", ";
            }

            containerType = containerType.Substring( 0, containerType.Length - 2 );
            containerType += " >";

            return containerType;
        }


        return TypeUtil.ToCppType( Type );
    }


    public void GenWriteCode( CodeGenContext context )
    {
        if ( IsNullable )
            context.WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], {Name}.HasValue );" )
                   .WriteLine( $"count   += sizeof( bool );" )
                   .WriteLine( $"if ( {Name}.HasValue )" )
                   .Scope();

        if ( !IsCollectionType )
        {
            if ( IsString )
            {
                context.WriteLine( $"ushort {Name}Len = (ushort)Encoding.Unicode.GetBytes( {Name}.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );" )
                       .WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], {Name}Len );" )
                       .WriteLine( "count += sizeof( ushort );" )
                       .WriteLine( $"count += {Name}Len;" );

            }
            else if ( CodeGen.CodeHelper.IsDefaultType( Type ) )
            {
                context.WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], {Name}{ ( IsNullable ? ".Value" : "" ) } );" );
                context.WriteLine( $"count   += sizeof( {Type} );" );
            }
            else if ( IsEnum )
            {
                if ( UnderlyingType.Equals( "Byte" ) || UnderlyingType.Equals( "SByte" ) )
                {
                    context.WriteLine( $"s[ count.. ][ 0 ] = (byte){Name};" );
                    context.WriteLine( $"count += sizeof( byte );" );
                }
                else
                {
                    //Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s ), (byte)_result );
                    context.WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], ({UnderlyingType}){Name} );" );
                    context.WriteLine( $"count   += sizeof( {Type} );" );
                }
            }
            else
            {
                context.WriteLine( $"success &= {Name}.Write( s, ref count );" );
            }
        }
        else if ( IsList )
        {
            context.WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( {Name}.Count ) );" ).
                    WriteLine( "count += sizeof( ushort );" );

            // Benchmakr/ListIteration.cs 결과 토대로 최적화
            using ( var s = context.WriteLine( $"foreach ( var element in {Name}.AsSpan() )" ).Scope2() )
            {
                foreach ( var elementType in GenericTypes )
                {
                    if ( CodeGen.CodeHelper.IsDefaultType( elementType ) )
                    {
                        context.WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], element );" );
                        context.WriteLine( $"count   += sizeof( {elementType} );" );
                    }
                    else
                        context.WriteLine( "success &= element.Write( s, ref count );" );
                }
            }
        }
        else
        {
            context.WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( {Name}.Count ) );" ).
                    WriteLine( "count += sizeof( ushort );" ).
                    WriteLine( $"foreach ( var element in {Name} )" ).
                    Scope();

            foreach ( var elementType in GenericTypes )
            {
                if ( CodeGen.CodeHelper.IsDefaultType( elementType ) )
                {
                    context.WriteLine( $"success &= BitConverter.TryWriteBytes( s[ count.. ], element );" );
                    context.WriteLine( $"count   += sizeof( {elementType} );" );
                }
                else
                    context.WriteLine( "success &= element.Write( s, ref count );" );
            }

            context.UnScope();
        }

        if ( IsNullable )
            context.UnScope();
    }

    public void GenReadCode( CodeGenContext context )
    {
        if ( IsNullable )
        {
            context.WriteLine( $"bool has{Name} = BitConverter.ToBoolean( s[ count.. ] );" )
                   .WriteLine( $"count   += sizeof( bool );" )
                   .WriteLine( $"if ( has{Name} )" )
                   .Scope();
        }

        /// 컬렉션 타입 여부
        if ( !IsCollectionType )
        {
            if ( IsString )
            {
                context.WriteLine( $"ushort {Name}Len = BitConverter.ToUInt16( s[ count.. ] );" )
                       .WriteLine( "count += sizeof( ushort ); " )
                       .WriteLine( $"{Name} = Encoding.Unicode.GetString( s.Slice( count, {Name}Len ) ); " )
                       .WriteLine( $"count += {Name}Len; " );
            }
            else if ( CodeGen.CodeHelper.IsDefaultType( Type ) )
            {
                context.WriteLine( $"{Name} = BitConverter.To{Type}( s[ count.. ] );" );
                context.WriteLine( $"count   += sizeof( {Type} );" );
            }
            else if ( IsEnum )
            {
                if ( UnderlyingType.Equals( "Byte" ) || UnderlyingType.Equals( "SByte" ) )
                {
                    context.WriteLine( $"{Name} = ({Type})s[ count.. ][ 0 ];" );
                    context.WriteLine( $"count += sizeof( byte );" );
                }
                else
                {
                    context.WriteLine( $"{Name} = ({Type})BitConverter.To{UnderlyingType}( s[ count.. ] );" );
                    context.WriteLine( $"count   += sizeof( {Type} );" );
                }
            }
            else
            {
                context.WriteLine( $"{Name}.Read( s, ref count );" );
            }
        }
        else if ( IsList )
        {
            context.WriteLine( $"{Name}.Clear();" ).
                    WriteLine( $"ushort {Name}Length = BitConverter.ToUInt16( s[ count.. ] );" ).
                    WriteLine( $"count += sizeof( ushort );" );

            /// Optimize
            context.WriteLine( $"{Name}.Capacity = {Name}Length;" );

            using ( var _ = context.WriteLine( $"for ( int i = 0; i < {Name}Length; i += 1 )" ).Scope2() )
            {
                foreach ( var elementType in GenericTypes )
                {
                    if ( CodeGen.CodeHelper.IsDefaultType( elementType ) )
                    {
                        context.WriteLine( $"var element = BitConverter.To{elementType}( s[ count.. ] );" );
                        context.WriteLine( $"count += sizeof( {elementType} );" );
                        context.WriteLine( $"{Name}.Add( element );" );
                    }
                    else
                    {
                        context.WriteLine( $"ref var element = ref {Name}.ReserveOneItem();" );
                        context.WriteLine( "element.Read( s, ref count );" );
                    }
                }
            }
        }
        else
        {
            context.WriteLine( $"{Name}.Clear();" ).
                    WriteLine( $"ushort {Name}Length = BitConverter.ToUInt16( s[ count.. ] );" ).
                    WriteLine( $"count += sizeof( ushort );" );
            using ( var _ = context.WriteLine( $"for ( int i = 0; i < {Name}Length; i += 1 )" ).Scope2() )
            {
                foreach ( var elementType in GenericTypes )
                {
                    if ( CodeGen.CodeHelper.IsDefaultType( elementType ) )
                    {
                        context.WriteLine( $"var element = BitConverter.To{elementType}( s[ count.. ] );" );
                        context.WriteLine( $"count += sizeof( {elementType} );" );
                    }
                    else
                    {
                        context.WriteLine( $"var element = new {elementType}();" );
                        context.WriteLine( "element.Read( s, ref count );" );
                    }

                    context.WriteLine( $"{Name}.Add( element );" );
                }
            }
        }

        if ( IsNullable )
            context.UnScope();
    }
}

public static class TypeUtil
{
    public static string ToCppType( string csType )
    {
        switch ( csType )
        {
            case "string":  return "String";
            case "String":  return "String";
            case "bool":    return "bool";
            case "Boolean": return "bool";
            case "Int8":    return "char";
            case "Int16":   return "short";
            case "Int32":   return "int";
            case "Int64":   return "long";
            case "UInt8":   return "unsigned char";
            case "UInt16":  return "unsigned short";
            case "UInt32":  return "unsigned int";
            case "UInt64":  return "unsigned long long";
            case "byte":    return "char";
            case "shot":    return "short";
            case "int":     return "int";
            case "long":    return "long long";
            case "ubyte":   return "unsigned char";
            case "ushot":   return "unsigned short";
            case "uint":    return "unsigned int";
            case "ulong":   return "unsigned long long";
            case "Single":  return "float";
            case "Double":  return "double";
        }

        return csType;
    }

    public static string CollectionToContainer( string? collectionType )
    {
        if ( string.IsNullOrEmpty( collectionType ) )
            return string.Empty;

        switch ( collectionType )
        {
            case "List":             return "Vector";
            case "ValueList":        return "Vector";
            case "Dictionary":       return "HashMap";
            case "SortedDictionary": return "Map";
            case "Set":              return "HashSet";
            case "SortedSet":        return "Set";
        }

        //switch ( collectionType )
        //{
        //    case "List":             return "vector";
        //    case "ValueList":        return "vector";
        //    case "Dictionary":       return "unordered_map";
        //    case "SortedDictionary": return "map";
        //    case "Set":              return "unordered_set";
        //    case "SortedSet":        return "set";
        //}

        return collectionType;
    }

}


public partial class FieldBuilder
{

    public void GenWriteCode( CodeGenContextCpp context )
    {
        if ( IsNullable )
        {
            context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, {Name}.HasValue );" )
                   .WriteLine( $"count   += sizeof( bool );" )
                   .WriteLine( $"if ( {Name}.HasValue )" )
                   .Scope();
        }

        if ( !IsCollectionType )
        {
            if ( IsString )
            {
                context.WriteLine( $"u16 {Name}Len = (u16)BitConverter::TryWriteBytes( s, count, {Name} );" )
                       .WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, {Name}Len );" )
                       .WriteLine( "count += sizeof( u16 );" )
                       .WriteLine( $"count += {Name}Len;" );

            }
            else if ( CodeGen.CodeHelper.IsDefaultType( Type ) )
            {
                context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, {Name}{( IsNullable ? ".Value" : "" )} );" );
                context.WriteLine( $"count   += sizeof( {CppType()} );" );
            }
            else if ( IsEnum )
            {
                if ( UnderlyingType.Equals( "Byte" ) || UnderlyingType.Equals( "SByte" ) )
                {
                    context.WriteLine( $"s[ count ] = (BYTE){Name};" );
                    context.WriteLine( $"count += sizeof( BYTE );" );
                }
                else
                {
                    //Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s ), (byte)_result );
                    //context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, ({UnderlyingType}){Name} );" );
                    context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, ( std::underlying_type_t< decltype( {Name} ) > ){Name} );" );
                    context.WriteLine( $"count   += sizeof( {CppType()} );" );
                }
            }
            else
            {
                context.WriteLine( $"success &= {Name}.Write( s, count );" );
            }
        }
        else if ( IsList )
        {
            context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, (u16)( {Name}.size() ) );" ).
                    WriteLine( "count += sizeof( u16 );" );

            string item = "auto& item";
            if ( GenericTypes.Count == 1 && CodeGen.CodeHelper.IsDefaultType(  GenericTypes[ 0 ] ) )
                item = "auto item";

            using ( var s = context.WriteLine( $"for ( {item} : {Name} )" ).Scope2() )
            {
                foreach ( var elementType in GenericTypes )
                {
                    if ( CodeGen.CodeHelper.IsDefaultType( elementType ) )
                    {
                        context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, item );" );
                        context.WriteLine( $"count   += sizeof( item );" );
                    }
                    else
                        context.WriteLine( "success &= item.Write( s, count );" );
                }
            }
        }
        else
        {
            context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, (u16)( {Name}.size() ) );" ).
                    WriteLine( "count += sizeof( u16 );" ).
                    WriteLine( $"for ( auto element : {Name} )" ).
                    Scope();

            foreach ( var elementType in GenericTypes )
            {
                if ( CodeGen.CodeHelper.IsDefaultTypeCpp( elementType ) )
                {
                    context.WriteLine( $"success &= BitConverter::TryWriteBytes( s, count, element );" );
                    context.WriteLine( $"count   += sizeof( {elementType} );" );
                }
                else if ( elementType == "string" )
                {
                    context.WriteLine( "success &= element.Write( s, count );" );
                }
                else
                {
                    context.WriteLine( "success &= element.Write( s, count );" );
                }

            }

            context.UnScope();
        }

        if ( IsNullable )
            context.UnScope();
    }

    public void GenReadCode( CodeGenContextCpp context )
    {
        if ( !IsCollectionType )
        {
            if ( IsString )
            {
                context.WriteLine( $"u16 {Name}ByteLen = BitConverter::ToUInt16( buffer, count );" )
                       .WriteLine( "count += sizeof( u16 ); " )
                       .WriteLine( $"{Name} = Encoding::Unicode::GetString( buffer, count, {Name}ByteLen ); " )
                       .WriteLine( $"count += {Name}ByteLen; " );
            }
            else if ( CodeHelper.IsDefaultType( Type ) )
            {
                context.WriteLine( $"{Name} = BitConverter::To{Type}( buffer, count );" );
                context.WriteLine( $"count   += sizeof( {CppType()} );" );
            }
            //else if ( IsEnum )
            //{
            //    if ( UnderlyingType.Equals( "Byte" ) || UnderlyingType.Equals( "SByte" ) )
            //    {
            //        context.WriteLine( $"{Name} = ({Type})s[ count.. ][ 0 ];" );
            //        context.WriteLine( $"count += sizeof( byte );" );
            //    }
            //    else
            //    {
            //        context.WriteLine( $"{Name} = ({Type})BitConverter.To{UnderlyingType}( s[ count.. ] );" );
            //        context.WriteLine( $"count   += sizeof( {Type} );" );
            //    }
            //}


            else if ( IsEnum )
            {
                //    if ( UnderlyingType.Equals( "Byte" ) || UnderlyingType.Equals( "SByte" ) )
                //    {
                //        context.WriteLine( $"{Name} = ({Type})s[ count.. ][ 0 ];" );
                //        context.WriteLine( $"count += sizeof( byte );" );
                //    }
                //    else
                //    {
                //        context.WriteLine( $"{Name} = ({Type})BitConverter.To{UnderlyingType}( s[ count.. ] );" );
                //        context.WriteLine( $"count   += sizeof( {Type} );" );
                //    }

                context.WriteLine( $"{Name} = ({Type})BitConverter::To{UnderlyingType}( buffer, count );" ).
                        WriteLine( $"count += sizeof( {Type} );" );
            }
            else
            {
                context.WriteLine( $"{Name}.Read( buffer, count );" );
            }
        }
        else
        {
            context.WriteLine( $"u16 {Name}Length = BitConverter::ToUInt16( buffer, count );" ).
                    WriteLine( $"{Name}.resize( {Name}Length );" ).
                    WriteLine( $"for ( int i = 0; i < {Name}Length; i += 1 )" ).
                    Scope();

            foreach ( var eachType in GenericTypes )
            {
                context.WriteLine( $"auto& _each_ = {Name}.emplace_back();" );
                if ( CodeHelper.IsDefaultTypeCpp( eachType ) )
                {
                    context.WriteLine( $"BufferReader::TryRead( buffer, count, _each_ );" );
                    context.WriteLine( "count += sizeof( _each_ );" );
                }
                else if ( eachType == "string" )
                {
                    context.WriteLine( $"BufferReader::TryRead( buffer, count, _each_ );" );
                    context.WriteLine( "count += sizeof( _each_ );" );
                }
                else
                {
                    context.WriteLine( $"_each_.Read( buffer, count );" );
                }
            }

            context.UnScope();
        }
    }


    public void GenReadCode2( CodeGenContext context )
    {
        if ( IsNullable )
        {
            context.WriteLine( $"bool has{Name} = BitConverter.ToBoolean( s[ count.. ] );" )
                   .WriteLine( $"count   += sizeof( bool );" )
                   .WriteLine( $"if ( has{Name} )" )
                   .Scope();
        }

        /// 컬렉션 타입 여부
        if ( !IsCollectionType )
        {
            if ( IsString )
            {
                context.WriteLine( $"ushort {Name}Len = BitConverter.ToUInt16( s[ count.. ] );" )
                       .WriteLine( "count += sizeof( ushort ); " )
                       .WriteLine( $"{Name} = Encoding.Unicode.GetString( s.Slice( count, {Name}Len ) ); " )
                       .WriteLine( $"count += {Name}Len; " );
            }
            else if ( CodeGen.CodeHelper.IsDefaultType( Type ) )
            {
                context.WriteLine( $"{Name} = BitConverter.To{Type}( s[ count.. ] );" );
                context.WriteLine( $"count   += sizeof( {Type} );" );
            }
            else if ( IsEnum )
            {
                if ( UnderlyingType.Equals( "Byte" ) || UnderlyingType.Equals( "SByte" ) )
                {
                    context.WriteLine( $"{Name} = ({Type})s[ count.. ][ 0 ];" );
                    context.WriteLine( $"count += sizeof( byte );" );
                }
                else
                {
                    context.WriteLine( $"{Name} = ({Type})BitConverter.To{UnderlyingType}( s[ count.. ] );" );
                    context.WriteLine( $"count   += sizeof( {Type} );" );
                }
            }
            else
            {
                context.WriteLine( $"{Name}.Read( s, ref count );" );
            }
        }
        else if ( IsList )
        {
            context.WriteLine( $"{Name}.Clear();" ).
                    WriteLine( $"ushort {Name}Length = BitConverter.ToUInt16( s[ count.. ] );" ).
                    WriteLine( $"count += sizeof( ushort );" );

            /// Optimize
            context.WriteLine( $"{Name}.Capacity = {Name}Length;" );

            using ( var _ = context.WriteLine( $"for ( int i = 0; i < {Name}Length; i += 1 )" ).Scope2() )
            {
                foreach ( var elementType in GenericTypes )
                {
                    if ( CodeGen.CodeHelper.IsDefaultType( elementType ) )
                    {
                        context.WriteLine( $"var element = BitConverter.To{elementType}( s[ count.. ] );" );
                        context.WriteLine( $"count += sizeof( {elementType} );" );
                        context.WriteLine( $"{Name}.Add( element );" );
                    }
                    else
                    {
                        context.WriteLine( $"ref var element = ref {Name}.ReserveOneItem();" );
                        context.WriteLine( "element.Read( s, ref count );" );
                    }
                }
            }
        }
        else
        {
            context.WriteLine( $"{Name}.Clear();" ).
                    WriteLine( $"ushort {Name}Length = BitConverter.ToUInt16( s[ count.. ] );" ).
                    WriteLine( $"count += sizeof( ushort );" );
            using ( var _ = context.WriteLine( $"for ( int i = 0; i < {Name}Length; i += 1 )" ).Scope2() )
            {
                foreach ( var elementType in GenericTypes )
                {
                    if ( CodeGen.CodeHelper.IsDefaultType( elementType ) )
                    {
                        context.WriteLine( $"var element = BitConverter.To{elementType}( s[ count.. ] );" );
                        context.WriteLine( $"count += sizeof( {elementType} );" );
                    }
                    else
                    {
                        context.WriteLine( $"var element = new {elementType}();" );
                        context.WriteLine( "element.Read( s, ref count );" );
                    }

                    context.WriteLine( $"{Name}.Add( element );" );
                }
            }
        }

        if ( IsNullable )
            context.UnScope();
    }
}
