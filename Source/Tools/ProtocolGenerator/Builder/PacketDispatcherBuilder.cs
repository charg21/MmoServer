﻿using CodeGen;
using Windows.System;

public class PacketDispatcherBuilder : ICodeBuilder
{
    /// <summary>
    /// 코드 타입
    /// </summary>
    public string CodeType => EClassType.PacketDispatcher.ToString();

    /// <summary>
    /// 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 출발지
    /// </summary>
    public string From { get; set; } = string.Empty;

    /// <summary>
    /// 목적지
    /// </summary>
    public string To { get; set; } = string.Empty;

    public string FileName => Name + "PacketDispatcher";

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    public bool NeedOverwrite => true;

    /// <summary>
    /// 출력 경로
    /// </summary>
    public List< string > OutputPath
        => Config.PacketDispatcherPathList.Where( dto => dto.To == To )
            .Select( dto => dto.Path )
            .ToList();

    public IEnumerable< PacketBuilder > _packets;

    /// <summary>
    /// 생성자
    /// </summary>
    public PacketDispatcherBuilder(
        string                name,
        IEnumerable< PacketBuilder > packets,
        string                from,
        string                to )
    {
        Name     = name;
        _packets = packets;
        From     = from;
        To       = to;
    }

    public void Build( CodeGenContext context )
    {
        GenImport( context );

        context.WriteLine( $"public class { FileName } : PacketDispatcher" )
               .Scope()
               .Comment( "정적 인스턴스" )
               .WriteLine( $"public static { FileName } Instance = new();" );

        context.WriteLine( "public override void Initialize()" )
               .Scope();

        foreach ( var pkt in _packets )
        {
            if ( pkt.From.Equals( From ) )
                context.WriteLine( $"HandlerMap.Add( (int)( EPacketType.{ pkt.Type } ), { pkt.Type }Handler.DoHandle );" );
        }

        context.UnScope().
                UnScope().
                NextLine();
    }

    private static void GenImport( CodeGenContext context )
    {
        context.WriteLine( $"using Mala.Net;" )
                       .NextLine();
    }

    public void Build( CodeGenContextCpp context )
    {
        context.WriteLine( $"export module { FileName };" ).
                NextLine();

        context.WriteLine( "/// <summary>" ).
                WriteLine( "/// 툴에 의해 자동 생성된 코드입니다." ).
                WriteLine( "/// </summary>" ).
                NextLine();

        context.WriteLine( "import Mala.Net.Buffer;" ).
                WriteLine( "import Mala.Net.PacketDispatcher;" ).
                NextLine();

        context.WriteLine( "import EPacketType;" ).
                NextLine();

        foreach ( var pkt in _packets )
        {
            if ( !pkt.From.Equals( From ) )
                continue;
            context.WriteLine( $"import { pkt.Category }Packet.{ pkt.Type };" );
            context.WriteLine( $"import { pkt.Category }Packet.{ pkt.Type }Handler;" );
        }
        context.NextLine();

        context.WriteLine( "using namespace std;" ).
                WriteLine( "using namespace Mala::Net;" ).
                NextLine();

        context.WriteLine( "#define REGISTER_HANDLER( PKT_TYPE ) \\" ).
                WriteLine( "Base::Table[ (size_t)( EPacketType::PKT_TYPE ) ] = handler_job< PKT_TYPE >;" ).
                NextLine();

        context.WriteLine( $"export class { FileName } : public Mala::Net::PacketDispatcher" ).
                Scope().
                WriteLine( "using Base = Mala::Net::PacketDispatcher;" ).
                NextLine().
                UnIndent().
                WriteLine( "public:" ).
                Indent().
                WriteLine( "using Mala::Net::PacketDispatcher::Table;" ).
                NextLine();

        context.Public().
                WriteLine( $"{ FileName }()" ).
                Scope();

        foreach ( var p in _packets )
        {
            if ( p.From.Equals( From ) )
                context.WriteLine( $"REGISTER_HANDLER( { p.Type } );" );
        }

        context.NextLine().
                WriteLine( $"Base::SetCapacity( (int)( EPacketType::{ _packets.Last()?.Type } ) + 1 );" );

        context.UnScope().
                UnScope().
                NextLine();

        context.WriteLine( $"inline static { FileName } initOnce{{}};" );
    }
}