﻿using CodeGen;

public class PacketFactoryBuilder : ICodeBuilder
{
    /// <summary>
    ///
    /// </summary>
    public IEnumerable< PacketBuilder > _packets;

    /// <summary>
    ///
    /// </summary>
    public string CodeType => EClassType.PacketFactory.ToString();

    /// <summary>
    /// 출력 경로
    /// </summary>
    public List< string > OutputPath => [ Config.PacketFactoryPath ];

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    public bool NeedOverwrite => true;

    /// <summary>
    /// 파일 이름
    /// </summary>
    public string FileName => "PacketFactory";

    /// <summary>
    /// 생성자
    /// </summary>
    public PacketFactoryBuilder( IEnumerable< PacketBuilder > packets )
        => _packets = packets;

    /// <summary>
    /// 생성한다
    /// </summary>
    public void Build( CodeGenContext context )
    {
        // TODO Import 추가
        context.WriteLine( $"using Mala.Net;" );

        context.WriteLine( $"public class PacketFactory" ).
                Scope();

        AppendFields( context );

        context.Comment( "생성자" )
               .WriteLine( $"public PacketFactory()" )
               .Scope()
               .WriteLine( "foreach ( EPacketType pktType in Enum.GetValues< EPacketType >() )" )
               .Scope()
               .WriteLine( "_packets[ (int)( pktType ) ] = MakePacket( (int)( pktType ) );" )
               .UnScope()
               .UnScope()
               .NextLine();

        context.Comment( "패킷을 반환한다." )
               .WriteLine( $"public PacketHeader? GetPacket( int type ) => _packets[ type ];" )
               .NextLine();

        context.Comment( "패킷을 생성한다." )
               .WriteLine( $"public static PacketHeader? MakePacket( int type )" )
               .Scope();

        context.WriteLine( $"return (EPacketType)( type ) switch" ).
                Scope();

        foreach ( var p in _packets )
        {
            context.WriteLine( $"EPacketType.{p.Type} => new {p.Type}()," );
        }

        context.WriteLine( $"_ => null" );

        context.UnScope().
                WriteLine( ";" ).
                UnScope().
                UnScope().
                NextLine();
    }

    private static void AppendFields( CodeGenContext context )
    {
        context.Comment( "" )
               .WriteLine( "PacketHeader[] _packets = GC.AllocateArray< PacketHeader >( (int)EPacketType.Max + 1, true );" )
               .NextLine();

        context.Comment( "Tls별 인스턴스" )
               .WriteLine( "public static ThreadLocal< PacketFactory > TlsInstance = new( () => new() );" )
               .NextLine();
    }

    public void Build( CodeGenContextCpp context )
    {
    }
}
