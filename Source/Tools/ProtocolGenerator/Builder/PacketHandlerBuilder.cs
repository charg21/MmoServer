﻿using System.Reflection;
using CodeGen;

public class PacketHandlerBuilder : ICodeBuilder
{
    /// <summary>
    /// 출력 경로
    /// </summary>
    public List< string > OutputPath => Config.PacketHandlerPathList.Where( dto => dto.To == Receiver )
        .Select( dto => dto.Path )
        .ToList();

    /// <summary>
    /// 
    /// </summary>
    public string CodeType => EClassType.PacketHandler.ToString();
    
    public string Category { get; set; } = string.Empty;

    public string Receiver { get; set; } = string.Empty;

    public string PacketName { get; set; } = string.Empty;

    public string FileName => PacketName + "Handler";

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    public bool NeedOverwrite => false;

    public PacketHandlerBuilder( PacketSchema packetSchema, TypeInfo typeInfo )
    {
        PacketName = typeInfo.Name;
        Category   = packetSchema.Category;
        Receiver   = packetSchema.To;
    }

    public void Build( CodeGenContext context )
    {
        context.WriteLine( $"using Mala.Net;" );

        context.WriteLine( $"public partial class { PacketName }Handler" ).
                Scope();
        
        context.NextLine().
                WriteLine( "public static void DoHandle( PacketHeader packet, PacketSession session )" ).
                Scope(); //     {

        context.WriteLine( "// TODO: 로직을 작성하세요." );
        // context.WriteLine( $"// var pkt = packet as { PacketName };" );

        context.UnScope(). //   }
                NextLine().
                UnScope(). // }
                NextLine();
    }

    public void Build( CodeGenContextCpp context )
    {
        context.WriteLine( $"export module { Category }Packet.{ PacketName }Handler;" ).
                NextLine( 2 );

        context.WriteLine( "import Mala.Net.PacketSession;" ).
                WriteLine( "import Mala.Net.PacketDispatcher;" ).
                NextLine();

        context.WriteLine( $"import { Category }Packet.{ PacketName };" ).
                NextLine();

        context.WriteLine( "using namespace Mala::Net;" ).
                NextLine();

        context.WriteLine( "template<>" );
        context.WriteLine( $"void Mala::Net::Handler< { PacketName } >( PacketSession* session, { PacketName }& packet )" ).
                Scope();

        context.WriteLine( "// TODO: 로직을 작성하세요." );

        context.UnScope().
                NextLine();
    }
}