﻿using CodeGen;

public class ProtocolDbBuilder : ICodeBuilder
{
    public string CodeType => EClassType.ProtocolDb.ToString();
    public string Type { get; set; } = string.Empty;

    /// <summary>
    /// 주석
    /// </summary>
    public string Comment  { get; set; } = string.Empty;

    /// <summary>
    /// 패킷 목록
    /// </summary>
    public IEnumerable< PacketBuilder > _packets;

    public string FileName => nameof( ProtocolDb );

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    public bool NeedOverwrite => true;

    /// <summary>
    /// 출력 경로
    /// </summary>
    public List< string > OutputPath => [ Config.OutputPath ];

    public ProtocolDbBuilder( IEnumerable< PacketBuilder > packets )
    {
        _packets = packets;
    }

    public void Build( CodeGenContext context )
    {
        context.WriteLine( protocolDbCode );

        context.WriteLine( $"public static partial class { FileName }" ).
                Scope();

        context.WriteLine( $"static { FileName }()" ).
                Scope();

        foreach ( var p in _packets )
        {
            context.WriteLine( $"Insert( \"{ p.Type }\", \"{ p.Hash }\" );" );
        }

        context.UnScope().
                UnScope().
                NextLine();
    }

    public void Build( CodeGenContextCpp context )
    {
    }

    public static string protocolDbCode =
        @"
public static partial class ProtocolDb
{
    public static Dictionary< string, string > ProtocolRecords { get; set; } = new();

    public static void Insert( string packet, string hash )
        => ProtocolRecords.Add( packet, hash );

    public static bool Contains( string packet )
        => ProtocolRecords.ContainsKey( packet );
}
";

}

public static partial class ProtocolDb
{
    public static Dictionary< string, string > ProtocolRecords { get; set; } = new();

    public static void Insert( string packet, string hash )
        => ProtocolRecords.Add( packet, hash );

    public static bool Contains( string packet )
        => ProtocolRecords.ContainsKey( packet );

    public static bool HasDiff( PacketBuilder packet )
    {
        if ( !ProtocolRecords.TryGetValue( packet.Type, out var prevHash ) )
            return false;

        return packet.Hash != prevHash;
    }
}
