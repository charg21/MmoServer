﻿using System.Reflection;
using CodeGen;
using Mala.Math;
using Microsoft.CodeAnalysis.CSharp.Syntax;

public class EnumBuilder : ICodeBuilder
{
    /// <summary>
    ///
    /// </summary>
    public string CodeType => EClassType.Enum.ToString();

    /// <summary>
    /// 파일 이름
    /// </summary>
    public List< string > OutputPath => [ Config.EnumPath ];

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    public bool NeedOverwrite => true;

    /// <summary>
    /// 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 기본 타입
    /// </summary>
    public string UnderlyingType { get; set; } = "Int32";

    /// <summary>
    ///
    /// </summary>
    public List< ( string, int, string? ) > Fields { get; set; } = new();

    /// <summary>
    /// 바이패스 패킷용
    /// </summary>
    public List< ( string, int, string? ) > BypassMembers { get; set; } = new();

    /// <summary>
    /// EnumFlag 타입 여부
    /// </summary>
    public bool IsEnumFlag { get; set; }

    /// <summary>
    ///
    /// </summary>
    public bool HasMaxField { get; set; }

    /// <summary>
    ///
    /// </summary>
    public int MaxFieldValue { get; set; } = 0;

    public int FieldNameMaxLength => Fields.Max( x => x.Item1.Length );

    /// <summary>
    /// 생성자
    /// </summary>
    public EnumBuilder( string name, IEnumerable< PacketBuilder > packets )
    {
        Name = name;

        int packetNo = 0;
        int bypassPacketNo = 0x8000;

        foreach ( var p in packets )
        {
            Fields.Add( ( p.Type, packetNo++ , string.Empty ) );
        }

        MaxFieldValue = packetNo;
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public EnumBuilder( TypeInfo typeInfo )
    {
        Name = typeInfo.Name;
        IsEnumFlag = typeInfo.GetCustomAttribute< EnumFlag >() is not null;
        UnderlyingType = Enum.GetUnderlyingType( typeInfo ).Name;

        foreach ( var enumField in typeInfo.GetFields() )
        {
            var     enumComment   = enumField.GetCustomAttribute< Comment >();
            string? commentString = enumComment?.ToString();

            if ( System.Enum.TryParse( typeInfo, enumField.Name, out var value ) )
            {
                int intValue = Convert.ToInt32( value );
                Fields.Add( ( enumField.Name, intValue, commentString ) );
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    public void ForEachField( Action< ( string, int, string? ) > job )
    {
        foreach ( var nameAndValue in Fields )
        {
            job( nameAndValue );
        }

        foreach ( var nameAndValue in BypassMembers )
        {
            job( nameAndValue );
        }

        if ( HasMaxField )
        {
            job( new( "Max", MaxFieldValue, "이 열거형의 최대값" ) );
        }
    }

    /// <summary>
    ///
    /// </summary>
    public string FileName => Name;

    /// <summary>
    ///
    /// </summary>
    public void Build( CodeGenContext context )
    {
        context.WriteLine( $"public enum { Name } : { UnderlyingType }" ).
                WriteLine( "{" ).
                Indent();

        BuildBody( context );
    }

    public void Build( CodeGenContextCpp context )
    {
        context.WriteLine( $"export module { Name };" ).
                NextLine().
                WriteLine( $"export enum class { Name }" ).
                WriteLine( "{" ).
                Indent();

        BuildBody( context );
    }

    private void BuildBody( CodeGenContext context )
    {
        var fieldNameMaxLength = FieldNameMaxLength;

        ForEachField( field =>
        {
            var space1 = CodeHelper.Spaces[ fieldNameMaxLength - field.Item1.Length ];

            if ( IsEnumFlag && MathHelper.TryGetExponentIfPowOfTwo( (ulong)field.Item2, out var exponent ) )
                context.WriteLine( $"{ field.Item1 }{ space1 } = 1 << { exponent }, /// { field.Item3 }" );
            else
                context.WriteLine( $"{ field.Item1 }{ space1 } = { field.Item2 }, /// { field.Item3 }" );
        } );

        context.UnIndent().
                WriteLine( "};" ).
                NextLine();
    }
};