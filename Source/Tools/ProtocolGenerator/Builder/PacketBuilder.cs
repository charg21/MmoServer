using System.Reflection;
using System.Text;
using CodeGen;


public partial class PacketBuilder : ICodeBuilder
{
    /// <summary>
    /// 출발지
    /// </summary>
    public string From { get; set; } = string.Empty;

    /// <summary>
    /// 도착지
    /// </summary>
    public string To { get; set; } = string.Empty;

    /// <summary>
    /// 카테고리
    /// </summary>
    public string Category { get; set; } = string.Empty;

    /// <summary>
    /// 타입 문자열
    /// </summary>
    public string Type { get; set; } = string.Empty;

    public string CodeType  => EClassType.Packet.ToString();

    public int Id { get; set; } = 0;

    public string Comment { get; set; } = string.Empty;

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    public bool NeedOverwrite => true;

    /// <summary>
    /// 출력 경로
    /// </summary>
    public List< string > OutputPath => [ Config.PacketPath ];

    /// <summary>
    /// 필드 목록
    /// </summary>
    public Dictionary< int, FieldBuilder > FieldList { get; set; } = new();

    /// <summary>
    /// 해시
    /// </summary>
    public string Hash { get; set; } = string.Empty;

    /// <summary>
    /// 바이패스 사용처
    /// </summary>
    public string Use { get; set; } = string.Empty;
    public bool HasCustemSerialization { get; private set; }
    public bool HasCustemSerializationInAnyField { get; }

    /// <summary>
    /// 파일명
    /// </summary>
    public string FileName => Type;

    public int FieldNameMaxLength
    {
        get
        {
            if ( FieldList.Count == 0 )
                return 0;

            return FieldList.Max( x => x.Value.Name.Length );
        }
    }

    /// <summary>
    ///
    /// </summary>
    public int FieldTypeMaxLength
    {
        get
        {
            if ( FieldList.Count == 0 )
                return 0;

            return FieldList.Max( x => x.Value.Type.Length );
        }
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public PacketBuilder( PacketSchema schema, TypeInfo typeInfo )
    {
        Type                             = typeInfo.Name;
        Category                         = schema.Category;
        From                             = schema.From;
        To                               = schema.To;
        Comment                          = typeInfo.GetCustomAttribute< Comment >()?.Message;
        Use                              = schema.Use;
        HasCustemSerialization           = typeInfo.GetCustomAttribute< CustomSerialization >() is not null;
        HasCustemSerializationInAnyField = HasCustemSerialization || typeInfo.DeclaredFields.Any( field =>
            field.GetCustomAttribute< CustomSerialization >() is not null ||
            HasCustomSerializationAttribute( field.FieldType ) );

        int n = 0;
        foreach ( var pktField in typeInfo.DeclaredFields )
            FieldList.Add( n++, new FieldBuilder( pktField ) );

        BuildHash();
    }

    private static bool HasCustomSerializationAttribute( Type type, int depth = 0 )
    {
        if ( type.GetCustomAttribute< CustomSerialization >() is not null )
            return true;

        if ( depth >= 2 )
            return false;

        foreach ( var field in type.GetFields( BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance ) )
        {
            if ( field.GetCustomAttribute< CustomSerialization >() is not null )
                return true;

            if ( HasCustomSerializationAttribute( field.FieldType, depth + 1 ) )
                return true;
        }

        return false;
    }

    public void BuildHash()
    {
        var hashBuilder = new StringBuilder();
        foreach ( var (_, f) in FieldList )
        {
            hashBuilder.Append( f.Type );
            if ( f.GenericTypes != null )
                f.GenericTypes.ForEach( type => hashBuilder.Append( type ) );

            hashBuilder.Append( f.Name );
            hashBuilder.Append( f.Comment );
        }

        Hash = HashGenerator.Generate( hashBuilder.ToString() );
    }

    static bool IsCollection( string type ) => type.Contains( '`' );

    public void Build( CodeGenContext context )
    {
        GenImport( context );

        GenPacket( context );

        GenValuePacket( context );
    }

    private static void GenImport( CodeGenContext context )
    {
        context.WriteLine( "using System;" )
               .WriteLine( "using System.Collections.Generic;" )
               .WriteLine( "using System.Runtime.CompilerServices;" )
               .WriteLine( "using System.Runtime.InteropServices;" )
               .WriteLine( "using System.Text;" )
               .WriteLine( "using Mala.Collection;" )
               .WriteLine( "using Mala.Net;" );
    }

    private void GenValuePacket( CodeGenContext ctx )
    {
        ctx.NextLine();
        // Struct
        ctx.WriteLine( $"[StructLayout( LayoutKind.Sequential, Pack = 1 )]" );
        using ( var structScope = ctx.WriteLine( $"public partial struct Value{Type} : IPacketHeader" ).Scope2() )
        {
            // To
            string use = string.IsNullOrEmpty( Use ) ? To : Use;
            ctx.Comment( "목적지" )
               .WriteLine( $"public EServiceType To => EServiceType.{use};" )
               .NextLine();

            var fieldNameMaxLength = FieldNameMaxLength;
            GenFieldOrProperty( ctx );

            // Constructor
            ctx.Comment( "생성자" );
            using ( var ctorScope = ctx.WriteLine( $"public Value{Type}()" ).Scope2() ){}
            ctx.NextLine();

            /// 프로퍼티
            ctx.WriteLine( "public ushort Length => throw new NotImplementedException();" )
               .WriteLine( $"public ushort Type => (ushort)( EPacketType.{Type} );" )
               .NextLine();

            if ( HasCustemSerialization )
                return;

            // Write
            ctx.Comment( "직렬화 한다." );
            using ( var writeScope = ctx.WriteLine( "public SendSegment Write()" ).Scope2() )
            {
                ctx.WriteLine( "var s = SendBufferHelper.Open( 25600 ).Span;" ).
                    WriteLine( $"bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.{Type} ) );" ).
                    WriteLine( "ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type" ).
                    NextLine();

                // Write
                if ( FieldList.Count != 0 )
                {
                    ctx.WriteLine( "#if !UNITY_EDITOR" );
                    if ( !HasCustemSerializationInAnyField )
                    {
                        // Optimized Write
                        using ( var writreUnalignedScope = ctx.WriteLine( $"if ( !RuntimeHelpers.IsReferenceOrContainsReferences< Value{Type} >() )" ).Scope2() )
                        {
                            ctx.WriteLine( "Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );" ).
                                WriteLine( $"count += (ushort)( Unsafe.SizeOf< Value{Type} >() );" );
                        }
                        ctx.WriteLine( "else" );
                    }

                    ctx.Scope().
                        WriteLine( "#endif" );
                }

                foreach ( var (_, field) in FieldList )
                {
                    field.GenWriteCode( ctx );
                }

                if ( FieldList.Count != 0 )
                {
                    ctx.WriteLine( "#if !UNITY_EDITOR" );
                    ctx.UnScope();
                    ctx.WriteLine( "#endif" );
                }

                ctx.WriteLine( "success &= BitConverter.TryWriteBytes( s, count );" ).
                    WriteLine( "if ( !success )" ).
                    WriteLine( "    return new( null, null );" ).
                    NextLine().
                    WriteLine( "return SendBufferHelper.Close( count );" );
            }
            ctx.NextLine();

            // Read
            ctx.Comment( "역직렬화 한다." );
            using ( var readScope = ctx.WriteLine( "public void Read( ref ReadOnlySpan< byte > s )" ).Scope2() )
            {
                // Read Body
                if ( FieldList.Count != 0 )
                {
                    ctx.WriteLine( "ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type" );

                    if ( !HasCustemSerializationInAnyField )
                    {
                        // Optimized Read
                        ctx.WriteLine( $"#if !UNITY_EDITOR" ).
                            WriteLine( $"if ( !RuntimeHelpers.IsReferenceOrContainsReferences< Value{Type} >() )" ).
                            Scope().
                            WriteLine( $"this = Unsafe.ReadUnaligned< Value{Type} >( ref MemoryMarshal.GetReference( s[ count.. ] ) );" ).
                            WriteLine( "return;" ).
                            UnScope();
                        ctx.WriteLine( "#endif" );
                    }
                }

                foreach ( var ( _, field ) in FieldList )
                {
                    field.GenReadCode( ctx );
                }
            }
            ctx.NextLine();
        }
    }

    private void GenFieldOrProperty( CodeGenContext context )
    {
        // Field, Property
        foreach ( var ( _, field ) in FieldList )
        {
            var fieldType = field.IsNullable ? $"Nullable< {field.Type} >" : field.Type;

            //if ( !string.IsNullOrEmpty( field.Comment ) )
            //{
            //    context.NextLine();
            //    context.Comment( field.Comment );
            //}

            if ( field.IsDefaultValueOrEnumType )
                context.WriteLine( $"public {fieldType} {field.Name}; { ( string.IsNullOrEmpty( field.Comment ) ? "" : $"/// {field.Comment}" ) }" );
            else if ( field.IsString )
                context.WriteLine( $"public {fieldType} {field.Name} = string.Empty; {( string.IsNullOrEmpty( field.Comment ) ? "" : $"/// {field.Comment}" )}" );
            else
                context.WriteLine( $"public {fieldType} {field.Name} = new(); {( string.IsNullOrEmpty( field.Comment ) ? "" : $"/// {field.Comment}" )}" );
        }

        context.NextLine();
    }

    private void GenPacket( CodeGenContext ctx )
    {
        // Class
        using ( var class_ = ctx.WriteLine( $"public partial class {Type} : PacketHeader" ).Scope2() )
        {
            // To
            string use = string.IsNullOrEmpty( Use ) ? To : Use;
            ctx.Comment( "목적지" ).
                WriteLine( $"public override EServiceType To => EServiceType.{use};" ).
                NextLine();

            var _ = FieldNameMaxLength;
            GenFieldOrProperty( ctx );

            // Constructor
            ctx.Comment( "생성자" );
            using ( var ctorScope = ctx.WriteLine( $"public {Type}()" ).Scope2() )
            {
                ctx.WriteLine( $"_type = (ushort)( EPacketType.{Type} );" );
            }
            ctx.NextLine();

            if ( HasCustemSerialization )
                return;

            // Write
            ctx.Comment( "직렬화 한다." );
            using ( var writeScope = ctx.WriteLine( "public override SendSegment Write()" ).Scope2() )
            {
                ctx.WriteLine( "var s = SendBufferHelper.Open( 25600 ).Span;" ).
                    WriteLine( $"bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.{Type} ) );" ).
                    WriteLine( "ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type" ).
                    NextLine();

                foreach ( var ( _, field ) in FieldList )
                {
                    field.GenWriteCode( ctx );
                }

                ctx.WriteLine( "success &= BitConverter.TryWriteBytes( s, count );" ).
                    WriteLine( "if ( !success )" ).
                    WriteLine( "    return new( null, null );" ).
                    NextLine().
                    WriteLine( "return SendBufferHelper.Close( count );" );
            }
            ctx.NextLine();

            // Read
            ctx.Comment( "역직렬화 한다." );
            using ( var readScope = ctx.WriteLine( "public override void Read( ref ReadOnlySpan< byte > s )" ).Scope2() )
            {
                // Read Body
                if ( FieldList.Count != 0 )
                    ctx.WriteLine( "ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type" )
                       .NextLine();

                foreach ( var ( _, field ) in FieldList )
                {
                    field.GenReadCode( ctx );
                }
            }
            ctx.NextLine();
        }
    }
}
