using CodeGen;
using System.Threading;

public static class ProtocolGenerator
{
    public static void Generate( List< ICodeBuilder > classList )
    {
        CodeGenContext context = new CodeGenContext();
        foreach ( var @class in classList )
        {
            var csFile  = $"{@class.FileName}.cs";

            @class.Build( context );
            Exporter.Export( @class.OutputPath, csFile, context.Code, @class.CodeType, @class.NeedOverwrite );
            context.Clear();
        }
    }

    public static void GenerateCpp( List< ICodeBuilder > classList )
    {
        var context = new CodeGenContextCpp();

        foreach ( var @class in classList )
        {
            var ixxFile = $"{@class.FileName}.ixx";

            @class.Build( context );
            Exporter.Export( @class.OutputPath, ixxFile, context.Code, @class.CodeType, @class.NeedOverwrite );
            context.Clear();
        }
    }

    private static ThreadLocal< CodeGenContext > _context  = new( () => new CodeGenContext() );

    public static void Generate( ICodeBuilder @class )
    {
        var context = _context.Value;
        var csPath  = @class.OutputPath + "\\" + @class.CodeType + "\\" + @class.FileName;

        @class.Build( context );
        Exporter.Export( csPath + ".cs", context.Code, @class.NeedOverwrite );
        context.Clear();
    }
}
