﻿//// See https://aka.ms/new-console-template for more information

//using System.Runtime.InteropServices;
//using System.Text;


//string rootPath = @"F:\Dev\SeamlessServer\Source\Mala.Core";
//string prefix = @"Unity";
//List< string > extendList = [ "*.cpp", "*.cxx" ];
//HashSet< string > excludeStartWithList = [ prefix ];

//// rootPath 디렉토리 내 서브 디렉토리별 cpp 파일을 읽어들여 UnityBuild.cpp 파일을 생성한다.
//Directory.GetDirectories( rootPath, "*", SearchOption.AllDirectories )
//    .ToList()
//    .ForEach( subDir =>
//    {
//        var sourceFiles = new List< string >();
//        var includeList = new List< string >();

//        extendList.ForEach( extend =>
//            sourceFiles.AddRange( Directory.GetFiles( subDir, extend, SearchOption.TopDirectoryOnly ) ) );

//        foreach ( var sourceFilePathWithDir in sourceFiles )
//        {
//            var sourceFileName = Path.GetFileName( sourceFilePathWithDir );

//            if ( CheckFilter( excludeStartWithList, sourceFileName ) )
//                continue;

//            includeList.Add( $"#include \"{sourceFileName}\"" );
//        }

//        // 1개면 유니티 빌드 파일을 생성하기 애매해서 패스
//        if ( includeList.Count <= 1 )
//            return;

//        var lastDir = Path.GetFileName( subDir );
//        var unityBuild = UnityBuildGenerator.GenerateUnityBuild( includeList );
//        File.WriteAllText( Path.Combine( subDir, $"{prefix}.{lastDir}.cpp" ), unityBuild, Encoding.UTF8 );
//    } );

//static bool CheckFilter( HashSet< string > excludeStartWithList, string sourceFileName )
//{
//    foreach ( var excludeFilter in excludeStartWithList )
//    {
//        if ( sourceFileName.StartsWith( excludeFilter ) )
//            return true;
//    }

//    return false;
//}

//class UnityBuildGenerator
//{
//    static StringBuilder unityBuildSb = new( 65536 );

//    public static string GenerateUnityBuild( List< string > sourceList )
//    {
//        unityBuildSb.Clear();
//        unityBuildSb.AppendLine( "/// <summary>" );
//        unityBuildSb.AppendLine( "/// UnityBuildGenerator툴에 의해 생성된 파일입니다." );
//        unityBuildSb.AppendLine( "/// </summary>" );
//        unityBuildSb.AppendLine();
//        unityBuildSb.AppendLine();
//        foreach ( var source in sourceList )
//        {
//            var lines = source.Split( [ Environment.NewLine ], StringSplitOptions.None );
//            foreach ( var line in lines )
//                unityBuildSb.AppendLine( line );
//        }

//        return unityBuildSb.ToString();
//    }
//}


using System.Text;

/// 특정 폴더에 있는 파일들 utf8 bom으로 변경

string rootPath = @"F:\Dev\SeamlessServer\Source";
List< string > extendList = [ "*.h", "*.cpp", "*.cxx", "*.ixx" ];
foreach ( var extend in extendList )
{
    var files = Directory.GetFiles( rootPath, extend, SearchOption.AllDirectories );
    foreach ( var file in files )
    {
        RemoveBom( file );
    }
}

static void RemoveBom( string filePath )
{
    var utf8Bom = new UTF8Encoding(true);
    var content = File.ReadAllBytes(filePath);

    // UTF-8 BOM (EF BB BF)
    byte[] bom = utf8Bom.GetPreamble();

    if ( content.Length >= bom.Length && HasBom( content, bom ) )
    {
        // Remove BOM
        var newContent = new byte[ content.Length - bom.Length ];
        Array.Copy( content, bom.Length, newContent, 0, newContent.Length );
        File.WriteAllBytes( filePath, newContent );
    }
}

static bool HasBom( byte[] content, byte[] bom )
{
    for ( int i = 0; i < bom.Length; i++ )
    {
        if ( content[ i ] != bom[ i ] )
            return false;
    }
    return true;
}