﻿using System.Text;

namespace CodeGen;

/// <summary>
///
/// </summary>
public static class CodeHelper
{
    const int Capacity = 101;

    /// <summary>
    ///
    /// </summary>
    public static List< string > Spaces { get; set; } = new( Capacity );

    /// <summary>
    ///
    /// </summary>
    public static List< string > Tabs { get; set; } = new( Capacity );

    /// <summary>
    ///
    /// </summary>
    public static List< string > NextLines { get; set; } = new( Capacity );

    /// <summary>
    /// 생성자
    /// </summary>
    static CodeHelper()
    {
        var spaceBuilder    = new StringBuilder( Capacity );
        var tabBuilder      = new StringBuilder( Capacity );
        var nextLineBuilder = new StringBuilder( Capacity );

        Spaces.Add( string.Empty );
        Tabs.Add( string.Empty );
        NextLines.Add( string.Empty );

        for ( int i = 0; i < Capacity - 1; i += 1 )
        {
            spaceBuilder.Append( ' ' );
            tabBuilder.Append( '\t' );
            nextLineBuilder.Append( "\r\n" );

            Spaces.Add( spaceBuilder.ToString() );
            Tabs.Add( tabBuilder.ToString() );
            NextLines.Add( nextLineBuilder.ToString() );
        }
    }

    public static bool IsDefaultValueType( string type )
    {
        return type switch
        {
            "bool" => true,
            "Boolean" => true,
            "byte" => true,
            "Byte" => true,
            "char" => true,
            "Char" => true,
            "int" => true,
            "Int32" => true,
            "uint" => true,
            "UInt32" => true,
            "short" => true,
            "Int16" => true,
            "long" => true,
            "Int64" => true,
            "ulong" => true,
            "UInt64" => true,
            "ushort" => true,
            "UInt16" => true,
            "float" => true,
            "double" => true,
            "Single" => true,
            "Double" => true,
            _ => false,
        };
    }

    public static bool IsDefaultType( string type )
    {
        if ( IsDefaultValueType( type ) )
            return true;

        switch ( type )
        {
            case "string":  return true;
            case "String":  return true;
        }

        return false;
    }

    public static bool IsDefaultTypeCpp( string type )
    {
        if ( IsDefaultValueType( type ) )
            return true;

        return false;
    }
}

