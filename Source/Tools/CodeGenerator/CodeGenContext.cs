﻿using System.Text;

namespace CodeGen;

/// <summary>
/// 코드 생성 문맥
/// </summary>
public class CodeGenContext
{
    /// <summary>
    ///
    /// </summary>
    enum ECommand
    {
        NoCommand,
        Write,
        WriteLine,
        Indent,
        UnIndent,
        Scope,
        UnScope,
        NextLine,
    }

    /// <summary>
    ///
    /// </summary>
    public int FileName { get; set; } = 0;

    /// <summary>
    ///
    /// </summary>
    public int Depth { get; set; } = 0;

    /// <summary>
    ///
    /// </summary>
    public StringBuilder Builder { get; set; } = new();

    /// <summary>
    /// 마지막 실행한 명령어
    /// </summary>
    ECommand LastCommand = ECommand.NoCommand;

    /// <summary>
    ///
    /// </summary>
    public string Code => Builder.ToString();

    public CodeGenContext Indent( int no = 1 )
    {
        Depth += no;

        return this;
    }

    public CodeGenContext UnIndent( int no = 1 )
    {
        Depth -= no;

        return this;
    }

    public CodeGenContext Scope( int no = 1 )
    {
        WriteLine( "{" );
        Indent();

        return this;
    }

    public virtual CodeGenContextScope Scope2( bool useBrace = true, string? endStr = null )
    {
        if ( useBrace )
            WriteLine( "{" );
        Indent();

        return new( this, useBrace );
    }

    public CodeGenContext UnScope( int no = 1, bool useBrace = true )
    {
        UnIndent();

        if ( useBrace )
            WriteLine( "}" );

        return this;
    }

    public CodeGenContext NextLine( int no = 1 )
    {
        Builder.Append( CodeHelper.NextLines[ no ] );

        return this;
    }

    public CodeGenContext Clear()
    {
        Builder.Clear();
        Depth = 0;

        return this;
    }

    public CodeGenContext Write( string code, int depthOffset = 0 )
    {
        int depth = Math.Max( 0, Depth + depthOffset );

        Builder.Append( CodeHelper.Tabs[ depth ] );
        Builder.Append( code                      );

        return this;
    }

    public CodeGenContext Remove( int length )
    {
        Builder.Remove( Builder.Length - length, length );

        return this;
    }

    public CodeGenContext WriteLine( string code, int depthOffset = 0 )
    {
        Write( code, depthOffset );
        NextLine();

        return this;
    }

    /// <summary>
    /// 이전 동작을 되돌린다.
    /// </summary>
    /// <returns></returns>
    public CodeGenContext Undo()
    {
        return this;
    }

}

/// <summary>
///
/// </summary>
public static class CodeGenContextComment
{
    public static CodeGenContext Comment( this CodeGenContext context, string msg )
        => context.Comment( new string[ 1 ] { msg } );

    public static CodeGenContext Comment( this CodeGenContext context, IEnumerable< string > msgs )
    {
        context.WriteLine( "/// <summary>" );

        foreach ( var msg in msgs )
            context.WriteLine( $"/// { msg }" );

        context.WriteLine( "/// </summary>" );

        return context;
    }
}


/// <summary>
/// 코드 생성 문맥
/// </summary>
public class CodeGenContextScope : IDisposable
{
    CodeGenContext _context;
    bool _useBrace;

    public CodeGenContextScope( CodeGenContext context, bool useBrace )
    {
        _context = context;
        _useBrace = useBrace;
    }

    public virtual void Dispose()
    {
        _context.UnScope( 1, _useBrace );
    }
}
