﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.CodeAnalysis;
using System.Reflection;
using System.Runtime.Loader;

namespace CodeGen;

/// <summary>
/// 코드 구문 분석기
/// </summary>
public class CodeParser
{
    /// <summary>
    ///
    /// </summary>
    static HashSet< Assembly > referencedAssemblies = new()
    {
        Assembly.Load( new AssemblyName( "Microsoft.CSharp" ) ),
        Assembly.Load( new AssemblyName( "netstandard" ) ),
        Assembly.Load( new AssemblyName( "System.Runtime" ) ),
        Assembly.Load( new AssemblyName( "System.Collections" ) ),
        Assembly.Load( new AssemblyName( "System.Linq" ) ),
        Assembly.Load( new AssemblyName( "System.Linq.Expressions" ) ),
        typeof( object ).Assembly,
    };

    private static List< PortableExecutableReference > _references;
    private static CSharpCompilationOptions _options = new( OutputKind.WindowsRuntimeMetadata );

    /// <summary>
    /// 생성자
    /// </summary>
    static CodeParser()
    {
    }

    public static List< ICodeBuilder > ParseCode( string packetCode )
    {
        return new();
    }

    public static Assembly? Compile( string code )
    {
        var syntaxTree = CSharpSyntaxTree.ParseText( code );
        var compilation = CSharpCompilation.Create( "Protocol",
                                                    new[] { syntaxTree },
                                                    _references,
                                                    _options );

        using var  memoryStream = new MemoryStream();
        EmitResult result       = compilation.Emit( memoryStream );
        if ( !result.Success )
        {
            Console.WriteLine( $"코드 컴파일 실패...{ result.Diagnostics }" );

            var failures = result.Diagnostics.Where( diagnostic =>
                                                         diagnostic.IsWarningAsError || diagnostic.Severity ==
                                                         DiagnosticSeverity.Error );

            foreach ( Diagnostic diagnostic in failures )
                Console.Error.WriteLine( $"{ diagnostic.Id } : { diagnostic.GetMessage() }" );

            return null;
        }

        memoryStream.Seek( 0, SeekOrigin.Begin );

        return AssemblyLoadContext.Default.LoadFromStream( memoryStream );
    }
}
