﻿namespace CodeGen;

/// <summary>
///
/// </summary>
public static class CodeGenerator
{
    public static bool Initialize()
    {
        return true;
    }

    public static void Generate( CodeGenContext context )
    {
    }
}

public class CodeGenContextCpp : CodeGenContext
{
    public enum State
    {
        Public,
        Private,
    }

    State _state = State.Private;

    public CodeGenContextCpp Public()
    {
        WriteLine( "public:", -Depth );

        _state = State.Public;

        return this;
    }

    public CodeGenContextCpp Private()
    {
        WriteLine( "private:", -Depth );

        _state = State.Private;

        return this;
    }

    public CodeGenContextCpp UnScope( bool useEnd = false )
    {
        if ( Depth != 1 )
        {
            base.UnScope();
        }
        else
        {
            UnIndent();
            if ( useEnd )
                WriteLine( "};" );
            else
                WriteLine( "}" );
        }

        return this;
    }

    public override CodeGenContextScope Scope2( bool useBrace = true, string? endStr = null )
    {
        if ( useBrace )
            WriteLine( "{" );
        Indent();

        return new CodeGenContextScopeCpp( this, useBrace, endStr );
    }

    public virtual CodeGenContextScope Struct( string type, bool useBrace = true )
    {
        return WriteLine( $"export struct {type}" ).Scope2( useBrace, ";" );
    }

    public virtual CodeGenContextScope Class( string type, bool useBrace = true )
    {
        return WriteLine( $"export struct {type}" ).Scope2( useBrace, ";" );
    }

    public virtual CodeGenContextScope For( string forCode )
    {
        return WriteLine( $"{forCode}" ).Scope2( true );
    }
}

/// <summary>
/// 코드 생성 문맥
/// </summary>
public class CodeGenContextScopeCpp : CodeGenContextScope
{
    CodeGenContextCpp _context;
    bool _useBrace;
    string? _endStr;

    public CodeGenContextScopeCpp( CodeGenContextCpp context, bool useBrace, string? endStr )
    : base( context, useBrace )
    {
        _context = context;
        _useBrace = useBrace;
        _endStr = endStr;
    }

    public override void Dispose()
    {
        _context.UnIndent();
        if ( !_useBrace )
            return;

        if ( _endStr is null )
            _context.WriteLine( "}" );
        else
            _context.WriteLine( $"}}{_endStr}" );
    }
}

/// <summary>
///
/// </summary>
public interface ICodeBuilder
{
    /// <summary>
    /// 코드 형식
    /// </summary>
    string CodeType { get; }

    /// <summary>
    /// 파일 이름
    /// </summary>
    List< string > OutputPath { get; }

    /// <summary>
    /// 파일 이름
    /// </summary>
    string FileName { get; }

    /// <summary>
    /// 덮어쓰기 필요 여부
    /// </summary>
    bool NeedOverwrite { get; }

    /// <summary>
    /// 생성한다
    /// </summary>
    public void Build( CodeGenContext context );
    public void Build( CodeGenContextCpp context );
}

public class Cpp
{

}
