﻿namespace CodeGen;

/// <summary>
///
/// </summary>
public static class Importer
{
    public static string Import( string path )
    {
        if ( !File.Exists( path ) )
        {
            Console.WriteLine( $"Invalid Path... CurPath: {Path.GetFullPath( path )}" );

            return string.Empty;
        }

        return File.ReadAllText( path );
    }
}

