﻿using System.Text;
using System.Text.RegularExpressions;

namespace CodeGenerator;

/// <summary>
/// 
/// </summary>
public static class CaseHelper
{
    public static string SnakeCaseToPascalCase( string snakeCaseString )
    {
        StringBuilder stringBuilder = new StringBuilder( snakeCaseString.Length );

        var wordList = snakeCaseString.Split( '_' ).ToList();

        wordList.ForEach( word =>
        {
            stringBuilder.Append( char.ToUpper( word[ 0 ] ) );
            stringBuilder.Append( word.AsSpan ( 1 )         );
        } );

        return stringBuilder.ToString();
    }

    public static string SnakeCaseToCamelCase( string snakeCaseString )
        => Regex.Replace(
            snakeCaseString,
            "_[a-z]",
            c => "" + char.ToUpper( c.ToString()[ 1 ] ) );
    
    public static string CamelCaseToSnakeCase( string camelCaseString )
        => Regex.Replace(
            camelCaseString,
            "[A-Z]",
            c => $"_{ c.ToString().ToLower() }" );

    public static string CamelToPascal( string camelCaseVar )
        => char.ToUpper( camelCaseVar[ 0 ] ) + camelCaseVar.Substring( 1 );
    public static string PascalToCamel( string parscalCaseVar )
        => char.ToLower( parscalCaseVar[ 0 ] ) + parscalCaseVar.Substring( 1 );


    public static string PascalCaseToCamelCase( string parscalCaseString )
    {
        var sb = new StringBuilder( parscalCaseString.Length );
        sb.Append( char.ToLower( parscalCaseString[ 0 ] ) );
        sb.Append( parscalCaseString.AsSpan( 1 ) );

        return sb.ToString();
    }

}