﻿namespace CodeGen;

/// <summary>
/// 
/// </summary>
public static class Exporter
{
    /// <summary>
    /// 
    /// </summary>
    public static void Export( List< string > fullPaths, string text, bool needOverwrite = true ) 
        => fullPaths.ForEach( fullPath => Export( fullPath, text, needOverwrite ) );

    /// <summary>
    /// 
    /// </summary>
    public static void Export( List< string > dirPaths, string fileName, string text, string dirPathOffset, bool needOverwrite = true )
    {
        foreach ( var dirPath in dirPaths )
        {
            Export( $"{ dirPath }{ ( dirPathOffset.Length > 0 ? "//" : "" ) }{ dirPathOffset }",
                fileName,
                text,
                needOverwrite );
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static void Export( string fullPath, string text, bool needOverwrite = true )
    {
        var fileName = Path.GetFileName( fullPath );
        var dirName  = Path.GetDirectoryName( fullPath );

        Export( dirName, fileName, text, needOverwrite );
    }

    /// <summary>
    /// 
    /// </summary>
    public static void Export( string dirPath, string fileName, string text, bool needOverwrite = true )
    {
        if ( string.IsNullOrEmpty( text ) )
            return;

        if ( string.IsNullOrEmpty( dirPath ) )
            return;

        if ( !Directory.Exists( dirPath ) )
            Directory.CreateDirectory( dirPath );

        string fullPath = dirPath + "\\" + fileName;
        if ( !needOverwrite && File.Exists( fullPath ) )
            return;

        File.WriteAllText( fullPath, text );
    }
}

