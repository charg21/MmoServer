﻿using CodeGen;
using Mala.Db;
using System.Diagnostics;
using DbSynchronizer = OrmGenerator.DbSynchronizer;

Stopwatch watch = Stopwatch.StartNew();

Config.Load( args );

DbConnectionPool.Instance.Initialize( Config.DbSchema, Config.DbUser, Config.DbPassword, 10 );
Console.WriteLine( $"Step0 : {watch.ElapsedMilliseconds} ms" );

DbSynchronizer.Instance.LoadRemoteDbInfo( Config.DbSchema ); // 1. 각 데이터를 불러온다
Console.WriteLine( $"Step1 : {watch.ElapsedMilliseconds} ms" );

DbSynchronizer.Instance.LoadLocalDbInfo( Config.SchemaPath );
Console.WriteLine( $"Step2 : {watch.ElapsedMilliseconds} ms" );

DbSynchronizer.Instance.Merge();                                // 2. 로컬을 기준으로 병합합니다.
Console.WriteLine( $"Step3 : {watch.ElapsedMilliseconds} ms" );

DbSynchronizer.Instance.SyncDb();                               // 3. 로컬을 기준으로 DB를 동기화한다
Console.WriteLine( $"Step4 : {watch.ElapsedMilliseconds} ms" );

DbSynchronizer.Instance.ForEachLocalTable( table =>
{
    //if ( !table.HasDiff || !Environment.ForceGenerate )
    //    return;
    if ( Config.Cs )
    {
        var dbModelBuilder = new DbModelBuilder( table );
        dbModelBuilder.Build();

        string savePath = $"{Config.OutputPath}\\{table.SourceName}DbModel.cs";
        Exporter.Export( Config.OutputPath, $"{table.SourceName}DbModel.cs", dbModelBuilder.ToString() );
    }

    //if ( Config.Cpp )
    {
        var dbModelBuilder = new DbModelBuilderCpp( table );
        dbModelBuilder.Build();

        string savePath = $"{Config.OutputPath}\\cpp\\{table.SourceName}DbModel.ixx";
        Exporter.Export( Config.OutputPath, $"cpp\\{table.SourceName}DbModel.ixx", dbModelBuilder.ToString() );
    }

} );

Console.WriteLine( $"Total Elapsed Time : {watch.ElapsedMilliseconds} ms" );

watch.Stop();

Console.ReadLine();
