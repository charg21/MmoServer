﻿using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using Microsoft.CodeAnalysis;
using System.Reflection;
using System.Runtime.Loader;
using CodeGen;
using OrmGenerator;
using System;
using System.Data;

public static class DbSchemaParser
{
    public static readonly Dictionary< string, TableInfo > _tableInfos = new();

    static HashSet< Assembly > _referencedAssemblies = new()
    {
        Assembly.Load( new AssemblyName( "Microsoft.CSharp" ) ),
        Assembly.Load( new AssemblyName( "netstandard" ) ),
        Assembly.Load( new AssemblyName( "System.Runtime" ) ),
        Assembly.Load( new AssemblyName( "System.Collections" ) ),
        Assembly.Load( new AssemblyName( "System" ) ),
        Assembly.Load( new AssemblyName( "System.Linq" ) ),
        Assembly.Load( new AssemblyName( "System.Linq.Expressions" ) ),
        typeof( object ).Assembly,
        typeof( Table ).GetTypeInfo().Assembly,
        typeof( Index ).GetTypeInfo().Assembly,
        typeof( Required ).GetTypeInfo().Assembly,
        typeof( Comment ).GetTypeInfo().Assembly,
        typeof( StrongType ).GetTypeInfo().Assembly,
    };

    private static List< PortableExecutableReference > _references;
    private static CSharpCompilationOptions _options = new ( OutputKind.WindowsRuntimeMetadata );

    static DbSchemaParser()
    {
        _references = _referencedAssemblies
                        .Select( assembly => MetadataReference.CreateFromFile( assembly.Location ) ).ToList();
    }



    public static List< ICodeBuilder > ParseCode( string dbSchemaCode )
    {
        var codeBuilders = new List< ICodeBuilder >();

        const string dependency = "";// "using System.Collections.Generic;";
        var assembly = Compile( dependency + dbSchemaCode );
        if ( assembly is null )
            return codeBuilders;

        foreach ( var typeInfo in assembly.DefinedTypes )
        {
            var tableInfo = TableInfoMapper.Map( typeInfo );
            if ( tableInfo is null )
                continue;

            _tableInfos.Add( tableInfo.NormalizedName, tableInfo );
        }

        return codeBuilders;
    }


    public static int n = 0;
    public static Assembly? Compile( string code )
    {
        var syntaxTree = CSharpSyntaxTree.ParseText( code );
        var compilation = CSharpCompilation.Create( $"DbModel{ n }", [ syntaxTree ],
                                                    _references,
                                                    _options );
        n += 1;

        using var  memoryStream = new MemoryStream();
        EmitResult result       = compilation.Emit( memoryStream );
        if ( !result.Success )
        {
            Console.WriteLine( $"코드 컴파일 실패...{result.Diagnostics}" );

            foreach ( var diagnostic in result.Diagnostics )
                Console.Error.WriteLine( $"{ diagnostic.Id } : { diagnostic.GetMessage() }" );

            return null;
        }

        memoryStream.Seek( 0, SeekOrigin.Begin );

        return AssemblyLoadContext.Default.LoadFromStream( memoryStream );
    }

}

public class TableInfoMapper
{
    public static TableInfo? Map( System.Reflection.TypeInfo typeInfo )
    {
        var ( dbTable, comment, required ) = GetTableAttribute( typeInfo );
        if ( dbTable is null )
            return null;

        var tableInfo = new TableInfo( dbTable, comment );
        //FieldInfo fieldInfo;
        int fieldNo = 0;

        typeInfo.DeclaredFields.All( field => true );

        List< FieldInfo > fieldInfos = new List< FieldInfo >();

        foreach ( var field in typeInfo.DeclaredFields )
        {
            var ( fieldAt, indexList, customType ) = GetFieldAttribute( field );

            var fieldInfo = new FieldInfo();

            foreach ( var index in indexList )
            {
                tableInfo.MaxIndexNameLength = Math.Max( index.Name.Length, tableInfo.MaxIndexNameLength );

                var indexInfo = tableInfo.GetOrDefaultIndexInfo( index?.Name );
                indexInfo.TableInfo = tableInfo;
                fieldInfo = indexInfo.GetOrDefaultFieldInfo( fieldAt.DbName, field.Name );
                fieldInfo.InIndex = true;

                indexInfo.BitFlagNum |= (long)( 1 << fieldNo );
            }

            fieldInfo = tableInfo.GetOrDefaultFieldInfo( field );
            fieldInfo.fieldNo = fieldNo;
            fieldInfo.Nullable = required is null ? false : true;
            fieldInfo.StrongType = customType?.Type ?? string.Empty;

            fieldNo += 1;
        }

        return tableInfo;

        static ( Field?, IEnumerable< Index >, StrongType? ) GetFieldAttribute( System.Reflection.FieldInfo field )
        {
            return new
            (
                field.GetCustomAttribute< Field >(),
                field.GetCustomAttributes< Index >(),
                field.GetCustomAttribute< StrongType >()
            );
        }

        static ( Table?, Comment?, Required? ) GetTableAttribute( System.Reflection.TypeInfo typeInfo )
        {
            return new
            (
                typeInfo.GetCustomAttribute< Table >(),
                typeInfo.GetCustomAttribute< Comment >(),
                typeInfo.GetCustomAttribute< Required >()
            );
        }
    }
}