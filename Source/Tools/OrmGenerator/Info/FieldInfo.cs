﻿using System;

/// <summary>
/// 필드 정보
/// </summary>
public record FieldInfo()// : IEquatable< FieldInfo >
{
    /// <summary>
    /// 필드 순서
    /// </summary>
    public int fieldNo { get; set; } = 0;

    /// <summary>
    /// 주석
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// 스키마 및 DB table 이름
    /// </summary>
    public string DbName { get; set; } = string.Empty;

    /// <summary>
    /// Source에 생성되는 이름
    /// </summary>
    public string SrcName { get; set; } = string.Empty;

    /// <summary>
    /// DB에서 쓰이는 타입
    /// </summary>
    public string DbType { get; set; } = string.Empty;

    /// <summary>
    /// 소스에서 쓰이는 타입
    /// </summary>
    public string SourceType { get; set; } = string.Empty;
    public string SourceTypeCpp { get; set; } = string.Empty;
    public string StrongType { get; set; } = string.Empty;

    /// <summary>
    /// Mala.Db.Varaint 내부 enum 타입
    /// </summary>
    public string EnumTypeInVariant { get; set; } = string.Empty;
    public string EnumTypeInVariantCpp { get; set; } = string.Empty;

    /// <summary>
    /// Mala.Db.Varaint 내부 필드 타입
    /// </summary>
    public string FieldTypeInVariant { get; set; } = string.Empty;
    public string FieldTypeInVariantCpp { get; set; } = string.Empty;

    /// <summary>
    /// 널 허용 여부
    /// </summary>
    public bool Nullable { get; set; } = false;

    /// <summary>
    /// 기본 값
    /// </summary>
    public string DefaultValue { get; set; } = string.Empty;

    /// <summary>
    /// 인덱스에 포함되는지 여부
    /// </summary>
    public bool InIndex { get; set; } = false;

    /// <summary>
    /// 유니크 키 보유 여부
    /// </summary>
    public bool HasUniqueKey { get; set; } = false;

    /// <summary>
    /// 이전 필드 보유 여부
    /// </summary>
    public bool HasPrevField => fieldNo > 0;

    public string DefaultValueString()
    {
        return IsStringType ? $"\"{DefaultValue}\"" : DefaultValue;
    }

    //public string SourceName          { get; set; } = string.Empty; //< 자동 생성 소스 파일내 디폴트 이름
    /// 복사 생성자
    public FieldInfo( FieldInfo other )
    {
        //ReadOnlySpan< FieldInfo > rhs = [ other ];
        //rhs.CopyTo( [ this ] );

        fieldNo = other.fieldNo;
        Description = other.Description;
        DbName = other.DbName;
        SrcName = other.SrcName;
        DbType = other.DbType;
        SourceType = other.SourceType;
        SourceTypeCpp = other.SourceTypeCpp;
        EnumTypeInVariant = other.EnumTypeInVariant;
        EnumTypeInVariantCpp = other.EnumTypeInVariantCpp;
        Nullable = other.Nullable;
        DefaultValue = other.DefaultValue;
        InIndex = other.InIndex;
    }

    /// <summary>
    /// 소스코드 자동 생성시 쓰이는 메소드
    /// </summary>
    public bool IsValueType
    {
        get
        {
            switch ( SourceType )
            {
                case "String":
                case "string":
                case "wstring":
                case "timestamp":
                    return false;

                default:
                    return true;
            }

        }
    }

    public bool IsStringType => SourceType is "String";
    public string AccessSymbolIfNeed => IsValueType ? string.Empty : "*";
    public string RefSymbolIfNeed => IsValueType ? string.Empty : "&";
    public string ConstArgsIfNeed => IsValueType ? string.Empty : "const ";
    public bool IsStrongType => StrongType.Length > 0;
    public string ExtractValueIfNeed => IsStrongType ? "*" : string.Empty;

    public string ArgType
    {
        get
        {
            if ( IsValueType )
            {
                return $"{( IsStrongType ? $"::{StrongType}" : SourceTypeCpp )}";
            }
            else
            {
                return  $"const {SourceTypeCpp}&" ;
            }
        }
    }

    public string SrcFieldType => $"{( IsStrongType ? $"::{StrongType}" : SourceTypeCpp )}";

    public string GatValueInSetter
    {
        get
        {
            return $"{ExtractValueIfNeed}{DbName}";
        }
    }


    /// <summary>
    ///
    /// </summary>
    public bool IsEquals( FieldInfo? rhs )
    {
        if ( this.DbName != rhs.DbName )
            return false;

        if ( this.Nullable != rhs.Nullable )
            return false;

        if ( this.DbType != rhs.DbType )
            return false;

        if ( this.DefaultValue != rhs.DefaultValue )
            return false;

        return true;
    }

}