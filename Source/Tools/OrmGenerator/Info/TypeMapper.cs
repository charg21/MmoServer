﻿public class TypeMapper
{
    public static string ToFieldTypeInVariant( string definitionType )
    {
        if ( definitionType.Length >= 7 )
        {
            if ( definitionType.Contains( "varchar" ) )
                //if ( definitionType.AsSpan( 0, 7 ) == "varchar" )
                return "String";
        }

        return ToSignedIfUnsigned( definitionType );
    }

    public static string ToFieldTypeInVariantCpp( string definitionType )
    {
        if ( definitionType.Length >= 7 )
        {
            if ( definitionType.Contains( "varchar" ) )
                //if ( definitionType.AsSpan( 0, 7 ) == "varchar" )
                return "String";
        }

        return definitionType switch
        {
            "Boolean" => "bool",

            "Int16" => "short",
            "Int32" => "int",
            "Int64" => "long",
            "UInt16" => "ushort",
            "UInt32" => "uint",
            "UInt64" => "ulong",

            "String" => "wstring",
            "Single" => "single",
            "Double" => "double",

            _ => definitionType
        };

        //return ToSignedIfUnsigned( definitionType );
    }

    public static string ToEnumTypeInVariant( string definitionType )
    {
        if ( definitionType.Length >= 7 )
        {
            if ( definitionType.Contains( "varchar" ) )
                return "Str";
        }

        return definitionType switch
        {
            "Int16" => "I16",
            "Int32" => "I32",
            "Int64" => "I64",
            "UInt16" => "U16",
            "UInt32" => "U32",
            "UInt64" => "U64",
            "Boolean" => "Bool",

            "String" => "Str",

            "Single" => "F32",
            "Double" => "F64",

            _ => definitionType
        };
    }

    public static string ToEnumTypeInVariantCpp( string definitionType )
    {
        if ( definitionType.Length >= 7 )
        {
            if ( definitionType.Contains( "varchar" ) )
                return "Str";
        }

        return definitionType switch
        {
            "Boolean" => "Bool",

            "Int16" => "Short",
            "Int32" => "Int",
            "Int64" => "Long",
            "UInt16" => "UShort",
            "UInt32" => "UInt",
            "UInt64" => "ULong",

            "String" => "WString",
            "bool" => "Bool",
            "Single" => "Single",
            "Double" => "Double",

            _ => definitionType
        };
    }

    /// <summary>
    ///
    /// </summary>
    public static string ToDbType( string definitionType )
    {
        return ToSignedIfUnsigned( definitionType ) switch
        {
            "SByte" => "tinyint",
            "Int16" => "smallint",
            "Int32" => "int",
            "Int64" => "bigint",

            "Single" => "float",
            "Double" => "double",

            "Boolean" => "tinyint(1)",
            "DateTime" => "datetime",

            _ => definitionType
        };
    }

    /// <summary>
    ///
    /// </summary>
    public static string ToSourceType( string definitionType )
    {
        if ( definitionType.Length >= 7 )
        {
            if ( definitionType.Contains( "varchar" ) )
                return "string";
        }

        return definitionType switch
        {
            "char" => "i8",
            "short" => "i16",
            "int" => "i32",
            "long" => "i64",
            "uchar" => "u8",
            "ushort" => "u16",
            "uint" => "u32",
            "ulong" => "u64",

            _ => definitionType
        };
    }

    /// <summary>
    ///
    /// </summary>
    public static string ToSourceTypeCpp( string definitionType )
    {
        if ( definitionType.Length >= 7 )
        {
            if ( definitionType.Contains( "varchar" ) )
                return "string";
        }

        return definitionType switch
        {
            "Boolean" => "bool",

            "char" => "i8",
            "short" => "i16",
            "int" => "i32",
            "long" => "i64",
            "uchar" => "u8",
            "ushort" => "u16",
            "uint" => "u32",
            "ulong" => "u64",

            "Int8" => "i8",
            "Int16" => "i16",
            "Int32" => "i32",
            "Int64" => "i64",

            "UInt8" => "u8",
            "UInt16" => "u16",
            "UInt32" => "u32",
            "UInt64" => "u64",

            "Single" => "float",
            "Double" => "double",

            _ => definitionType
        };
    }

    /// <summary>
    ///
    /// </summary>
    public static string ToSignedIfUnsigned( string definitionType )
    {
        return definitionType switch
        {
            "Byte" => "SByte",
            "UInt16" => "Int16",
            "UInt32" => "Int32",
            "UInt64" => "Int64",

            _ => definitionType
        };
    }



}
