using CodeGenerator;
using OrmGenerator;
using System;
using System.Reflection;

/// <summary>
/// 테이블 정보
/// </summary>
public class TableInfo// : IEquatable< TableInfo >
{
    /// <summary>
    /// 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 이름
    /// </summary>
    public string NormalizedName { get; set; } = string.Empty;

    /// <summary>
    /// 소스 이름
    /// </summary>
    public string SourceName { get; set; } = string.Empty;

    /// <summary>
    /// 주석
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// 해시 ( 버전 관리 )
    /// </summary>
    public string Hash { get; set; } = string.Empty;

    /// <summary>
    /// MySql 엔진 버전
    /// </summary>
    public EEngine Engine { get; set; } = EEngine.InnoDB;

    /// <summary>
    ///
    /// </summary>
    public CharacterSet CharacterSet { get; set; } = CharacterSet.Utf8;

    /// <summary>
    /// 테이블 상태
    /// </summary>
    public TableState State { get; set; } = TableState.None;

    /// <summary>
    /// 필드 이름 최대 길이( 소스 )
    /// </summary>
    public int MaxSourceFieldNameLength { get; set; } = 0;

    /// <summary>
    /// 인덱스 이름 최대 길이
    /// </summary>
    public int MaxIndexNameLength { get; set; } = 0;

    /// <summary>
    /// 필드 이름 최대 길이( Db )
    /// </summary>
    public int MaxDbFieldNameLength { get; set; } = 0;

    /// <summary>
    /// 인덱스 정보 목록
    /// </summary>
    public List< IndexInfo > IndexInfoList { get; set; } = new();

    /// <summary>
    /// 필드 정보 목록
    /// </summary>
    public List< FieldInfo > FieldInfoList { get; set; } = new();

    /// <summary>
    /// 인덱스 정보 딕셔너리
    /// </summary>
    public Dictionary< string, IndexInfo > IndexInfoDict { get; set; } = new();

    /// <summary>
    /// 필드 정보 딕셔너리
    /// </summary>
    public Dictionary< string, FieldInfo > FieldInfoDict { get; set; } = new();

    /// <summary>
    /// 각 순회
    /// </summary>
    public void ForEachField( Action< FieldInfo > job ) => FieldInfoList.ForEach( column => job!( column ) );
    public void ForEachIndex( Action< IndexInfo > job ) => IndexInfoList.ForEach( idx => job!( idx ) );


    public IndexInfo PrimaryIndex => IndexInfoList[ 0 ];
    public void ForEachSecondaryIndex( Action< IndexInfo > job ) => IndexInfoList.ForEach( idx => job!( idx ) );

    public bool HasSecondaryIndex => IndexInfoList.Count > 1;
    public bool HasDiff => State is TableState.Created or TableState.Updated;

    /// <summary>
    /// 생성자
    /// </summary>
    public TableInfo()
    {
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public TableInfo( Table dbTable, Comment? comment = null )
    {
        Name           = dbTable.Name;
        NormalizedName = dbTable.Name.ToLower();
        SourceName     = CaseHelper.SnakeCaseToPascalCase( dbTable.Name );
        Description    = comment?.Message ?? string.Empty;
    }

    /// <summary>
    /// 인덱스 정보를 추가한다.
    /// </summary>
    public IndexInfo AddIndexInfo( IndexInfo indexInfo )
    {
        if ( IndexInfoDict.TryAdd( indexInfo.Name, indexInfo ) )
        {
            IndexInfoList.Add( indexInfo );
            indexInfo.TableInfo = this;
        }

        return indexInfo;
    }

    /// <summary>
    /// 인덱스 정보를 반환한다. 없다면 기본값을 반환한다.
    /// </summary>
    public IndexInfo GetOrDefaultIndexInfo( string indexKey )
    {
        if ( !IndexInfoDict.TryGetValue( indexKey, out var index ) )
        {
            index = new IndexInfo()
            {
                BitFlagNum    = 0,
                FieldInfoList = new(),
                FieldInfoDict = new(),
                Name          = indexKey,
            };

            return AddIndexInfo( index );
        }

        return index;
    }

    /// <summary>
    /// 필드 정보를 추가한다.
    /// </summary>
    public FieldInfo AddFieldInfo( FieldInfo fieldInfo )
    {
        if ( FieldInfoDict.TryAdd( fieldInfo.DbName, fieldInfo ) )
            FieldInfoList.Add( fieldInfo );

        return fieldInfo;
    }


    /// <summary>
    /// 필드 정보를 반환한다. 없다면 기본값을 반환한다.
    /// </summary>
    public FieldInfo GetOrDefaultFieldInfo( string fieldDbName )
    {
        if ( !FieldInfoDict.TryGetValue( fieldDbName, out var field ) )
        {
            var newField = new FieldInfo()
            {
                SrcName = fieldDbName,
                DbName     = fieldDbName,
            };

            field = AddFieldInfo( newField );
        }

        return field;
    }


    /// <summary>
    /// 필드 정보를 반환한다. 없다면 기본값을 반환한다.
    /// </summary>
    public FieldInfo GetOrDefaultFieldInfo( System.Reflection.FieldInfo fieldInfo )
    {
        var ( fieldAtbt, indexList, fieldDefault, comment, strongType ) = GetFieldAttribute( fieldInfo );

        var field = GetOrDefaultFieldInfo( fieldAtbt.DbName );

        field.DbName                = !string.IsNullOrEmpty( fieldAtbt?.DbName ) ? fieldAtbt.DbName : fieldInfo.Name;
        field.SrcName               = CaseHelper.SnakeCaseToPascalCase( fieldInfo.Name );
        field.DbType                = !string.IsNullOrEmpty( fieldAtbt?.DbType ) ? fieldAtbt.DbType : TypeMapper.ToDbType( fieldInfo.FieldType.Name );
        field.SourceType            = TypeMapper.ToSourceType( fieldInfo.FieldType.Name );
        field.SourceTypeCpp         = TypeMapper.ToSourceTypeCpp( fieldInfo.FieldType.Name );
        field.EnumTypeInVariant     = TypeMapper.ToEnumTypeInVariant( fieldInfo.FieldType.Name );
        field.EnumTypeInVariantCpp  = TypeMapper.ToEnumTypeInVariantCpp( fieldInfo.FieldType.Name );
        field.FieldTypeInVariant    = TypeMapper.ToFieldTypeInVariant( fieldInfo.FieldType.Name );
        field.FieldTypeInVariantCpp = TypeMapper.ToFieldTypeInVariantCpp( fieldInfo.FieldType.Name );
        field.Description           = comment?.Message ?? string.Empty;
        field.DefaultValue          = fieldDefault?.Value ?? string.Empty;
        field.StrongType            = strongType?.Type ?? string.Empty;


        MaxDbFieldNameLength     = Math.Max( field.DbName.Length, MaxDbFieldNameLength );
        MaxSourceFieldNameLength = Math.Max( field.SrcName.Length, MaxSourceFieldNameLength );

        return field;

        static ( Field?, IEnumerable< Index >, Default?, Comment?, StrongType? ) GetFieldAttribute( System.Reflection.FieldInfo field )
        {
            return new
            (
                field.GetCustomAttribute< Field >(),
                field.GetCustomAttributes< Index >(),
                field.GetCustomAttribute< Default >(),
                field.GetCustomAttribute< Comment >(),
                field.GetCustomAttribute< StrongType >()
            );
        }

    }






    /// <summary>
    /// 비교한다.
    /// </summary>
    internal bool Compare( TableInfo other )
    {
        //// 리플렉션을 사용하여 속성 접근
        //var properties = typeof( TableInfo ).GetProperties();

        //foreach ( var property in properties )
        //{
        //    if ( property.Name != "Name" &&
        //         property.Name != "IndexInfoList" &&
        //         property.Name != "FieldInfoList" )
        //        continue;

        //    object lhs = property.GetValue( this );
        //    object rhs = property.GetValue( other );

        //    //object ic = typeof( List<> );
        //    //object gt = null;
        //    //if ( property.PropertyType.IsGenericType )
        //    //{
        //    //    gt = property.PropertyType.GetGenericTypeDefinition();

        //    //    if ( gt == ic )
        //    //        Console.WriteLine( $"??{ gt } {ic} " );
        //    //    else
        //    //        Console.WriteLine( $"!!{gt} {ic} " );
        //    //}



        //    if ( property.PropertyType.IsGenericType &&
        //         property.PropertyType.GetGenericTypeDefinition() == typeof( List<> ) )
        //    {
        //        // ICollection<T> 형태의 속성인 경우, 컬렉션 비교
        //        var elementType            = property.PropertyType.GetGenericArguments()[0];
        //        var collectionComparerType = typeof( EqualityComparer<> ).MakeGenericType( elementType );
        //        var collectionComparer     = collectionComparerType.GetProperty( "Default" ).GetValue( null );

        //        var collectionEqualsMethod = collectionComparerType.GetMethod("Equals", new[] { elementType, elementType });

        //        var collection1 = ( IEnumerable< IndexInfo > )lhs;
        //        var collection2 = ( IEnumerable< IndexInfo > )rhs;

        //        if ( collection1 == null || collection2 == null )
        //            return false;

        //        var enumerator1 = collection1.GetEnumerator();
        //        var enumerator2 = collection2.GetEnumerator();

        //        while ( enumerator1.MoveNext() && enumerator2.MoveNext() )
        //        {
        //            if ( !enumerator1.Equals( enumerator2 ) )
        //                return false;

        //            //if ( !(bool)collectionEqualsMethod.Invoke( collectionComparer, new[] { enumerator1.Current, enumerator2.Current } ) )
        //            //    return false;
        //        }

        //        if ( enumerator1.MoveNext() || enumerator2.MoveNext() )
        //            return false;
        //    }
        //    else
        //    {
        //        // 일반 속성 비교
        //        if ( !Equals( lhs, rhs ) )
        //            return false;
        //    }
        //}

        return true;
    }

    public bool IsEquals( TableInfo? rhs )
    {
        /// 추가 또는 삭제가 있는 경우
        var addedList   = IndexInfoDict.Where( kv => !rhs.IndexInfoDict.ContainsKey( kv.Key ) );
        var removedList = rhs.IndexInfoDict.Where( kv => !IndexInfoDict.ContainsKey( kv.Key ) );
        if ( addedList.Any() || removedList.Any() )
            return false;

        /// 같은 컬럼들 비교
        var indexList = IndexInfoDict.Where( kv => rhs.IndexInfoDict.ContainsKey( kv.Key ) );
        foreach ( var ( name, index ) in indexList )
        {
            if ( !index.IsEquals( rhs.IndexInfoDict[ name ] ) )
                return false;
        }

        /// 추가 또는 삭제가 있는 경우
        var addedFields   = FieldInfoDict.Where( kv => !rhs.FieldInfoDict.ContainsKey( kv.Key ) );
        var removedFields = rhs.FieldInfoDict.Where( kv => !FieldInfoDict.ContainsKey( kv.Key ) );
        if ( addedFields.Any() || removedFields.Any() )
            return false;

        /// 같은 컬럼들 비교
        var fields = FieldInfoDict.Where( kv => rhs.FieldInfoDict.ContainsKey( kv.Key ) );
        foreach ( var ( name, f ) in fields )
        {
            if ( !f.IsEquals( rhs.FieldInfoDict[ name ] ) )
                return false;
        }

        return true;
    }
}
public enum CharacterSet
{
    Utf8,
    Utf16,
    EucKr,
}

public enum EEngine
{
    InnoDB,
    MyIsam,
}

/// <summary>
///
/// </summary>
public enum TableState
{
    None,
    Created,
    Updated,
    Deleted,
}
