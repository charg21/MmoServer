using Mala.Collection;

namespace OrmGenerator;


/// <summary>
/// 인덱스 타입 열거형
/// </summary>
public enum EIndexType
{
    PrimaryKey,
    MultipleKey,
    UniqueKey,
};

/// <summary>
/// 인덱스 정보
/// </summary>
public record IndexInfo() : IEquatable< IndexInfo >
{
    /// <summary>
    /// 테이블 정보
    /// </summary>
    public TableInfo? TableInfo;

    /// <summary>
    /// 인덱스 마스크 플래그
    /// </summary>
    public long BitFlagNum { get; set; }

    /// <summary>
    /// 필드 정보 목록 리스트
    /// </summary>
    public List< FieldInfo > FieldInfoList { get; set; } = new();

    /// <summary>
    /// 필드 정보 맵
    /// </summary>
    public Dictionary< string, FieldInfo > FieldInfoDict { get; set; } = new();

    /// <summary>
    /// 인덱스 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 인덱스 이름
    /// </summary>
    public string NormalizedName { get; set; } = string.Empty;

    /// <summary>
    /// 인덱스 타입
    /// </summary>
    public EIndexType Type { get; set; }

    /// <summary>
    /// 복사 생성자
    /// </summary>
    public IndexInfo( IndexInfo other )
    {
        FieldInfoDict = new();
        FieldInfoList = new();

        foreach ( var fieldInfo in other.FieldInfoList )
            AddFieldInfo( new FieldInfo( fieldInfo ) );

        Name           = other.Name;
        NormalizedName = other.Name.ToLower();
        BitFlagNum     = other.BitFlagNum;
        Type           = other.Type;
    }

    /// <summary>
    /// 필드 정보를 추가한다.
    /// </summary>
    public FieldInfo AddFieldInfo( FieldInfo fieldInfo )
    {
        if ( FieldInfoDict.TryAdd( fieldInfo.DbName, fieldInfo ) )
        {
            FieldInfoList.Add( fieldInfo );
            TableInfo?.AddFieldInfo( fieldInfo );
        }

        return fieldInfo;
    }

    /// <summary>
    /// 필드 정보를 반환한다. 없다면 기본값을 반환한다.
    /// </summary>
    public FieldInfo GetOrDefaultFieldInfo( string fieldDbName, string fieldName = "" )
    {
        if ( FieldInfoDict.TryGetValue( fieldDbName, out var field ) )
            return field;

        if ( TableInfo.FieldInfoDict.TryGetValue( fieldDbName, out var field2 ) )
            return AddFieldInfo( field2 );

        var newField = new FieldInfo()
        {
            SrcName = fieldDbName,
            DbName     = fieldDbName,
        };

        return AddFieldInfo( newField );
    }

    /// <summary>
    ///
    /// </summary>
    public FieldInfo GetOrDefaultFieldInfo( System.Reflection.FieldInfo fieldInfo, Field? fieldAttr )
    {
        if ( !FieldInfoDict.TryGetValue( fieldInfo.Name, out var field ) )
        {
            var newField = new FieldInfo()
            {
                SrcName = fieldInfo.Name,
                DbName     = fieldAttr.DbName,
            };

            field = AddFieldInfo( newField );
        }

        field.InIndex = true;

        return field;
    }

    /// <summary>
    ///
    /// </summary>
    public bool IsEquals( IndexInfo? rhs )
    {
        if ( Name != rhs.Name )
            return false;

        /// 추가 또는 삭제가 있는 경우
        var addedList   = FieldInfoDict.Where( kv => !rhs.FieldInfoDict.ContainsKey( kv.Key ) );
        var removedList = rhs.FieldInfoDict.Where( kv => !FieldInfoDict.ContainsKey( kv.Key ) );
        if ( addedList.Any() || removedList.Any() )
            return false;

        /// 같은 컬럼들 비교
        var fieldList = FieldInfoDict.Where( kv => rhs.FieldInfoDict.ContainsKey( kv.Key ) );
        foreach ( var ( name, field ) in fieldList )
        {
            if ( !field.IsEquals( rhs.FieldInfoDict[ name ] ) )
                return false;
        }

        return true;
    }

    public void ForEachFieldInfo( Action< FieldInfo > action )
    {
        foreach ( var fieldInfo in FieldInfoList.AsSpan() )
            action( fieldInfo );
    }
}
