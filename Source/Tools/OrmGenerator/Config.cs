﻿using CodeGen;
using System.Reflection;
using System.Text.Json;

/// <summary>
/// 환경
/// </summary>
public static class Config
{
    /// <summary>
    /// 모아서 카피
    /// </summary>
    public static bool CanBulkCopy = false;

    /// <summary>
    ///  모든 테이블 강제 생성
    /// </summary>
    public static bool ForceGenerate = false;

    /// <summary>
    /// Drop Table 사용 여부
    /// </summary>
    public static bool UseDropTable = false;

    /// <summary>
    /// 로그를 남길지 여부
    /// </summary>
    public static bool UseQueryConsoleLog = false;

    /// <summary>
    /// 스키마
    /// </summary>
    public static string DbSchema = string.Empty;

    /// <summary>
    /// 유저
    /// </summary>
    public static string DbUser = string.Empty;

    /// <summary>
    /// 비밀번호
    /// </summary>
    public static string DbPassword = string.Empty;

    /// <summary>
    /// Db 호스트
    /// </summary>
    public static string DbHost = string.Empty;

    /// <summary>
    /// Db 포트
    /// </summary>
    public static int DbPort = 0;

    /// <summary>
    /// 로컬 DB 스키마 경로
    /// </summary>
    public static string SchemaPath = string.Empty;

    /// <summary>
    /// 스키마 결과물 경로
    /// </summary>
    public static string OutputPath = string.Empty;

    /// <summary>
    /// 근본은 C#인것..
    /// </summary>
    public static bool Cs = true;
    public static bool Cpp = false;

    public static void Load( string[] args )
    {
        string configPath = "OrmGenerator.json";

        if ( args.Length > 0 && !string.IsNullOrEmpty( args[ 0 ] ) )
            configPath = args[ 0 ];

        var configString = Importer.Import( configPath );
        Console.WriteLine( $"Config Path = { configPath }" );

        var config = JsonSerializer.Deserialize< ConfigDto >( configString );
        ImportFrom( in config );
    }

    private static void ImportFrom< T >( in T config )
    {
        foreach ( var propConfig in config.GetType().GetProperties() )
        {
            var fieldEnv = typeof( Config ).GetField( propConfig.Name );
            if ( fieldEnv != null )
            {
                fieldEnv.SetValue( null, propConfig.GetValue( config ) );
                Console.WriteLine( $"{ propConfig.Name } = { propConfig.GetValue( config ) } " );
            }
        }
    }
}

/// <summary>
///
/// </summary>
public struct ConfigDto
{
    /// <summary>
    /// 생성자
    /// </summary>
    public ConfigDto()
    {
    }

    /// <summary>
    /// 모아서 카피
    /// </summary>
    public bool CanBulkCopy { get; set; } = false;

    /// <summary>
    ///  모든 테이블 강제 생성
    /// </summary>
    public bool ForceGenerate { get; set; } = false;

    /// <summary>
    /// Drop Table 사용 여부
    /// </summary>
    public bool UseDropTable { get; set; } = false;

    /// <summary>
    /// 로그를 남길지 여부
    /// </summary>
    public bool UseQueryConsoleLog { get; set; } = false;

    /// <summary>
    /// Db 호스트
    /// </summary>
    public string DbHost { get; set; } = string.Empty;

    /// <summary>
    /// Db 스키마
    /// </summary>
    public string DbSchema { get; set; } = string.Empty;

    /// <summary>
    /// Db 유저
    /// </summary>
    public string DbUser { get; set; } = string.Empty;

    /// <summary>
    /// Db 비밀번호
    /// </summary>
    public string DbPassword { get; set; } = string.Empty;

    /// <summary>
    /// Db 포트
    /// </summary>
    public int DbPort { get; set; } = 0;

    /// <summary>
    /// 로컬 DB 스키마 경로
    /// </summary>
    public string SchemaPath { get; set; } = string.Empty;

    /// <summary>
    /// 스키마 결과물 경로
    /// </summary>
    public string OutputPath { get; set; } = string.Empty;

    /// <summary>
    /// 근본은 C#인것..
    /// </summary>
    public bool Cs = true;
    public bool Cpp = false;

}
