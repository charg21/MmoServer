﻿using Mala.Core;
using Mala.Db;
using MySql.Data.MySqlClient;

namespace OrmGenerator;

/// <summary>
///
/// </summary>
public class DbSynchronizer : Singleton< DbSynchronizer >
{
    /// <summary>
    /// 스키마
    /// </summary>
    public string Schema { get; set; } = string.Empty;

    /// <summary>
    /// 유저
    /// </summary>
    public string User { get; set; } = string.Empty;

    /// <summary>
    /// 비밀번호
    /// </summary>
    public string Password { get; set; } = string.Empty;
    private string _connectionString = string.Empty;

    /// <summary>
    ///
    /// </summary>
    public bool UseQueryConsoleLog { get; set; } = true;

    /// <summary>
    /// Diff 과정에서 로컬에서 제거된 Table을 제거할지 여부
    /// </summary>
    public bool UseDropTable { get; set; } = true;

    /// <summary>
    /// 원격 테이블 정보
    /// </summary>
    public Dictionary< string, TableInfo > RemoteTableInfoMap { get; set; } = new();

    /// <summary>
    /// 로컬 테이블 정보
    /// </summary>
    public Dictionary< string, TableInfo > LocalTableInfoMap  { get; set; } = new();

    /// <summary>
    ///
    /// </summary>
    public Dictionary< string, ( TableInfo, TableInfo ) > TableInfoDiff { get; set; } = new();

    /// <summary>
    ///
    /// </summary>
    public void ForEachDiffTable( Action< ( TableInfo, TableInfo? ) > job )
    {
        foreach ( var ( _, tableInfo ) in TableInfoDiff )
            job( tableInfo );
    }

    public void ForEachLocalTable( Action< TableInfo > job )
    {
        foreach ( var ( _, tableInfo ) in LocalTableInfoMap )
            job( tableInfo );
    }

    /// <summary>
    /// 병합한다.
    /// </summary>
    public void Merge()
    {
        /// 기존 테이블 정보를 수집한다.
        var updateTables = LocalTableInfoMap.Where(
            nameAndLocalTable => RemoteTableInfoMap.ContainsKey( nameAndLocalTable.Key ) );
        foreach ( var ( name, localTable ) in updateTables )
        {
            if ( localTable.IsEquals( RemoteTableInfoMap[ name ] ) )
            {
                Console.WriteLine( $"같음 { name }" );
                continue;
            }
            else
            {
                Console.WriteLine( $"다름 {name}" );
            }

            localTable.State = TableState.Updated;
            TableInfoDiff.Add( name, ( localTable, RemoteTableInfoMap[ name ] ) );
        }

        /// 신규 테이블 정보를 수집한다.
        var newTables = LocalTableInfoMap.Where(
            nameAndTable => !RemoteTableInfoMap.ContainsKey( nameAndTable.Key ) );
        foreach ( var ( name, newTable ) in newTables )
        {
            newTable.State = TableState.Created;
            TableInfoDiff.Add( name, ( newTable, null ) );
        }

        /// 삭제된 테이블 정보를 수집한다.
        var removedTables = RemoteTableInfoMap.Where(
            nameAndTable => !LocalTableInfoMap.ContainsKey( nameAndTable.Key ) );
        foreach ( var ( name, dropTable ) in removedTables )
        {
            dropTable.State = TableState.Deleted;
            TableInfoDiff.Add( name, ( dropTable, null ) );
        }
    }

    /// <summary>
    /// Db와 동기화한다.
    /// </summary>
    public void SyncDb()
    {
        var connection = DbConnectionPool.Instance.Pop();

        foreach ( var ( _, tableInfo ) in TableInfoDiff )
        {
            var query =  string.Empty;

            var queryBuilder = new QueryBuilder();
            switch ( tableInfo.Item1.State )
            {
                case TableState.Created:
                    query = queryBuilder.BuildCreateQuery( Schema, tableInfo.Item1 );
                    break;

                case TableState.Deleted:
                    if ( !UseDropTable )
                        continue;

                    query = queryBuilder.BuildDropQuery( Schema, tableInfo.Item1 );
                    break;

                case TableState.Updated:
                    //continue;
                    query = queryBuilder.BuildAlterTableQuery( Schema, tableInfo.Item1, tableInfo.Item2 );
                    break;

                case TableState.None:
                    continue;
            }

            if ( string.IsNullOrEmpty( query ) )
                continue;

            //if ( UseQueryConsoleLog )
            Console.WriteLine( query );

            using var createCommand = new MySqlCommand( query, connection );
            using var reader        = createCommand.ExecuteReader();
        }

        DbConnectionPool.Instance.Push( connection );
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="dirPath"></param>
    /// <returns></returns>
    public bool LoadLocalDbInfo( string dirPath )
    {
        if ( !Directory.Exists( dirPath ) )
        {
            Console.WriteLine( $"Not Found Directory... Path : {dirPath}" );
            return false;
        }

        var filePathList = Directory.GetFiles( dirPath );
        foreach ( var filePath in filePathList )
        {
            if ( !_ParseLocalDbInfo( filePath ) )
                Console.WriteLine( $"Invalid File Path... : { filePath }" );
        }

        foreach ( var ( name, tableInfo ) in DbSchemaParser._tableInfos )
            LocalTableInfoMap.Add( name, tableInfo );

        return true;
    }

    private bool _ParseLocalDbInfo( string path )
    {
        if ( !path.Contains( ".cs" ) )
            return false;

        if ( !File.Exists( path ) )
        {
            Console.WriteLine( $"Cant Find File : {path}" );
            return false;
        }

        string code = File.ReadAllText( path );
        if ( string.IsNullOrEmpty( code ) )
        {
            Console.WriteLine( $"Empty File : { path }" );
            return false;
        }

        DbSchemaParser.ParseCode( code );

        return true;
    }


    public void LoadRemoteDbInfo( string schema )
    {
        Schema = schema;

        var connection = DbConnectionPool.Instance.Pop();
        var allTableSelectQuery = $"SELECT `TABLE_NAME` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{schema}';";
        var cmd = new MySqlCommand( allTableSelectQuery, connection );

        using MySqlDataReader tableNameReader = cmd.ExecuteReader();

        while ( tableNameReader.Read() )
        {
            var tableName = tableNameReader[ "TABLE_NAME" ].ToString();
            var tableInfo = new TableInfo{ Name = tableName, NormalizedName = tableName.ToLower() };

            RemoteTableInfoMap.Add( tableName, tableInfo );

            _LoadAndParseRemoteFieldInfo( tableInfo );
            _LoadAndParseRemoteIndexInfo( tableInfo );
        }

        DbConnectionPool.Instance.Push( connection );
    }

    private void _LoadAndParseRemoteFieldInfo( TableInfo tableInfo )
    {
        var connection           = DbConnectionPool.Instance.Pop();
        var allColumnSelectQuery = $"show full columns from `{ tableInfo.Name }`;";
        var cmd1                 = new MySqlCommand( allColumnSelectQuery, connection );

        using MySqlDataReader fieldReader = cmd1.ExecuteReader();

        int fieldNo = 0;
        while ( fieldReader.Read() )
        {
            var fieldName = fieldReader[ 0 ].ToString();
            var fieldInfo = tableInfo.GetOrDefaultFieldInfo( fieldName );
            fieldInfo.DbType = fieldReader[ 1 ].ToString()!;
            fieldInfo.Nullable = fieldReader[ 3 ].ToString() != "NO";
            fieldInfo.InIndex = fieldReader[ 4 ].ToString() != string.Empty; // "PRI" or "MUL" or "UNI"
            fieldInfo.fieldNo = fieldNo;
            // Console.WriteLine( $"Remote Field....{fieldName}  {fieldInfo.fieldNo}" );

            fieldNo += 1;
        }

        DbConnectionPool.Instance.Push( connection );
    }

    /// <summary>
    ///
    /// </summary>
    private void _LoadAndParseRemoteIndexInfo( TableInfo tableInfo )
    {
        var connection          = DbConnectionPool.Instance.Pop();
        var allIndexSelectQuery = $"show indexes from { tableInfo.Name }";
        var cmd2                = new MySqlCommand( allIndexSelectQuery, connection );

        using MySqlDataReader indexReader = cmd2.ExecuteReader();

        while ( indexReader.Read() )
        {
            var indexName = indexReader![ "Key_name"    ].ToString()!;
            var fieldName = indexReader![ "Column_name" ].ToString()!;

            var indexInfo = tableInfo.GetOrDefaultIndexInfo( indexName );
            indexInfo.TableInfo = tableInfo;
            var fieldInfo = indexInfo.GetOrDefaultFieldInfo( fieldName );

            indexInfo.BitFlagNum |= (long)( 1 << fieldInfo.fieldNo );

            // Console.WriteLine( $"Remote {indexName} {fieldName}  {fieldInfo.fieldNo}" );
        }

        DbConnectionPool.Instance.Push( connection );
    }

}


/*
// DB의 전체 테이블 조회
SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DB명';

// DB의 전체 테이블명 및 테이블 코멘트 조회
SELECT TABLE_NAME, TABLE_COMMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DB명' ORDER BY TABLE_NAME;

// 특정 테이블의 모든 컬럼 정보 조회
SHOW FULL COLUMNS FROM 테이블명;

// 여러 테이블의 모든 컬럼 정보 조회
SELECT
TABLE_NAME AS 'table_name',
ORDINAL_POSITION AS 'no',
COLUMN_NAME AS 'field_name',
COLUMN_COMMENT AS 'comment',
COLUMN_TYPE AS 'type',
COLUMN_KEY AS 'KEY',
IS_NULLABLE AS 'NULL',
EXTRA AS 'auto',
COLUMN_DEFAULT 'default'
FROM
INFORMATION_SCHEMA.COLUMNS
WHERE
TABLE_SCHEMA = 'DB명'
AND TABLE_NAME IN ('테이블명','테이블명','테이블명')
ORDER BY
TABLE_NAME, ORDINAL_POSITION
;

*/