﻿using CodeGen;
using Mala.Core;
using Mala.Db;
using System.Text;


/// <summary>
///
/// </summary>
public partial class DbModelBuilder
{
    /// <summary>
    /// 코드 생성 문맥
    /// </summary>
    CodeGenContext _context { get; set; } = new();

    /// <summary>
    /// 테이블 정보
    /// </summary>
    private TableInfo _tableInfo;

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    public override string ToString() => _context.Code;

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="tableInfo"></param>
    public DbModelBuilder( TableInfo tableInfo )
    {
        _tableInfo = tableInfo;

    }

    private void _AddImplPartialClass( string tableSourceName )
    {
         string sourceTableInfoName = $"{ _tableInfo.SourceName }TableInfo";

        _context.Comment( $"{ _tableInfo.Name } Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )" );

        using ( var modelScope = _context.WriteLine( @$"public partial class {tableSourceName} : IDbModel" ).Scope2() )
        {
            _context.Comment( "작업 실행기" );
            _context.WriteLine( $"public Actor JobExecutor => throw new NotImplementedException();" )
                    .NextLine();

            _context.Comment( "Db 동기화 객체 인터페이스 구현체" );
            _context.WriteLine( $"public DbModel DbModel => dbModel;" )
                    .NextLine();

            _context.Comment( "원본" );
            _context.WriteLine( @"public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }" )
                    .NextLine();

            //_context.Comment( "동기화 한다." );
            //_context.WriteLine( $"public void Sync( EQueryType queryType )" )
            //        .Scope()
            //        .UnScope()
            //        .NextLine();

            _context.Comment( "Db 동기화 객체" );
            _context.WriteLine( $"public {_tableInfo.SourceName}DbModel dbModel = new();" )
                    .NextLine();

            //// Getter
            foreach ( var field in _tableInfo.FieldInfoList )
            {
                _context.Comment( $"{field.SrcName}를( 을 ) 반환한다" );

                _context.WriteLine( $"public {field.SourceType} Get{field.SrcName}() => dbModel.Get{field.SrcName}();" )
                        .NextLine();
            }

            // Setter
            foreach ( var field in _tableInfo.FieldInfoList )
            {
                _context.Comment( $"{field.SrcName}를( 을 ) 설정한다" );
                _context.WriteLine( $"public void Set{field.SrcName}( {field.SourceType} {field.DbName} ) " +
                    $"=> dbModel.Set{field.SrcName}( {field.DbName} );" );

                /// 인덱스에 포함되는 컬럼이라면? 키 설정 인터페이스 추가
                if ( !field.InIndex )
                    continue;

                _context.WriteLine( $"public void SetKey{field.SrcName}( {field.SourceType} {field.DbName} ) " +
                    $"=> dbModel.SetKey{field.SrcName}( {field.DbName} );" )
                        .NextLine();
            }

            _context.Comment( "동기화한다." )
                    .WriteLine( @" public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );" )
                    .Comment( "Select" )
                    .WriteLine( @"public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );" )
                    .Comment( "Insert" )
                    .WriteLine( @"public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );" )
                    .Comment( "키 컬럼을 복사한다" )
                    .WriteLine( @"public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );" )
                    .Comment( "필드를 설정한다" )
                    .WriteLine( @"public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );" )
                    .Comment( "필드 참조를 획득한다" )
                    .WriteLine( @"public ref Field GetField( int no ) => ref dbModel.GetField( no );" )
                    .Comment( "모든 필드를 바인딩한다" )
                    .WriteLine( @"public void BindAllField() => dbModel.BindAllField();" );

        }
        _context.NextLine();
    }

    private void _AddDbModel( string tableSourceName )
    {
        string srcTableInfoName = $"{ _tableInfo.SourceName }TableInfo";

        _context.Comment( $"{ _tableInfo.Name } Db 테이블과 동기화 가능한 Db Orm 객체" );

        using ( var classScope = _context.WriteLine( @$"public partial class {tableSourceName}DbModel : DbModel" ).Scope2() )
        {
            //// static ctor
            //_context.Comment( "정적 생성자" )
            //        .WriteLine( $"static { tableSourceName }DbModel()" )
            //        .Scope()
            //        .WriteLine( $"StaticTableInfo = { _tableInfo.SourceName }TableInfo.Instance;" )
            //        .UnScope()
            //        .NextLine();

            // Ctor
            _context.Comment( "생성자" );

            using ( var ctorScope = _context.WriteLine( $"public {tableSourceName}DbModel() : base( {srcTableInfoName}.Instance )" ).Scope2() )
            { }
            _context.NextLine();

            // Getter
            foreach ( var field in _tableInfo.FieldInfoList )
            {
                _context.Comment( $"{field.SrcName}를( 을 ) 반환한다" )
                        .WriteLine( $"public {field.SourceType} Get{field.SrcName}()" )
                        .Scope()
                        .WriteLine( $"return Fields[ (int)( {srcTableInfoName}.EFieldNumber.{field.DbName} ) ].Var.Get{field.EnumTypeInVariant}();" )
                        .UnScope()
                        .NextLine();
            }

            // Setter
            foreach ( var field in _tableInfo.FieldInfoList )
            {
                _context.Comment( $"{field.SrcName}를( 을 ) 설정한다" )
                        .WriteLine( $"public void Set{field.SrcName}( {field.SourceType} {field.DbName} )" )
                        .Scope()
                        .WriteLine( $"Fields[ (int)( {srcTableInfoName}.EFieldNumber.{field.DbName} ) ].Var.Set{field.EnumTypeInVariant}( {field.DbName} );" )
                        .WriteLine( $"SetUpdateField( (int)( {srcTableInfoName}.EFieldNumber.{field.DbName} ) );" )
                        .UnScope()
                        .NextLine();

                if ( !field.InIndex )
                    continue;

                _context.WriteLine( $"public void SetKey{field.SrcName}( {field.SourceType} {field.DbName} )" )
                        .Scope()
                        .WriteLine( $"Fields[ (int)( {srcTableInfoName}.EFieldNumber.{field.DbName} ) ].Var.Set{field.EnumTypeInVariant}( {field.DbName} );" )
                        .WriteLine( $"SetUpdateAndSetupKeyField( (int)( {srcTableInfoName}.EFieldNumber.{field.DbName} ) );" )
                        .UnScope()
                        .NextLine();
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    private void _AddDbTableInfo()
    {
        string sourceTableInfoName = $"{_tableInfo.SourceName}TableInfo";

        _context.Comment( $"{_tableInfo.Name} Db 테이블 정보" );
        using ( var scope = _context.WriteLine( $"public class {sourceTableInfoName} : TableInfo" ).Scope2() )
        {
            _context.Comment( $"{_tableInfo.Name} 테이블 정보 전역 인스턴스" )
                    .WriteLine( $"public static {sourceTableInfoName} Instance {{ get; set; }} = new();" )
                    .NextLine();

            using ( var enumScope = _context.WriteLine( $"public enum EFieldNumber" ).Scope2() )
            {
                foreach ( var column in _tableInfo.FieldInfoList )
                    _context.WriteLine( $"{column.DbName}," );

                _context.WriteLine( @"max," );
            }
            _context.NextLine();

            _context.Comment( "생성자" );
            using ( var ctopScope = _context.WriteLine( $"public {sourceTableInfoName}() : base( \"{_tableInfo.Name}\" )" ).Scope2() )
            {
                foreach ( var field in _tableInfo.FieldInfoList )
                {
                    var fieldSpaceSize = _tableInfo.MaxDbFieldNameLength - field.DbName.Length;

                    _context.WriteLine( $"AddFieldInfo( \"{field.DbName}\"{CodeHelper.Spaces[ fieldSpaceSize ]}" +
                            $", (int)( EFieldNumber.{field.DbName} ){CodeHelper.Spaces[ fieldSpaceSize ]}" +
                            $", EVariantType.{field.EnumTypeInVariant}, {( field.InIndex ? "true " : "false" )} );" );
                }
                _context.NextLine();

                foreach ( var index in _tableInfo.IndexInfoList )
                    foreach ( var field in index.FieldInfoList )
                        _context.WriteLine( $"AddIndexInfo( \"{index.Name}\", FieldInfos[ (int)( EFieldNumber.{field.DbName} ) ] );" );
            }
            _context.NextLine();
        }
    }

    public void _AddImportDependency()
    {
        _context.Comment  ( "툴에 의해 자동 생성된 코드입니다." )
                .WriteLine( @"
using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;

" );
    }

    internal void Build()
    {
        _AddImportDependency();
        _AddDbTableInfo();
        _AddDbModel( _tableInfo.SourceName );
        _AddImplPartialClass( _tableInfo.SourceName );

        string code = _context.Code;
        //Console.WriteLine( code );
    }
}