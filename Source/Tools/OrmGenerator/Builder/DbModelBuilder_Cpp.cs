﻿using CodeGen;
using Mala.Core;
using Mala.Db;
using System.Text;
using Windows.UI;


/// <summary>
///
/// </summary>
public partial class DbModelBuilderCpp
{
    /// <summary>
    /// 코드 생성 문맥
    /// </summary>
    CodeGenContextCpp _ctx { get; set; } = new();
    private TableInfo _tableInfo;

    public override string ToString() => _ctx.Code;

    public DbModelBuilderCpp( TableInfo tableInfo )
    {
        _tableInfo = tableInfo;

        _AddModule();
        _AddCommnet( "툴에 의해 자동 생성된 코드입니다." );
        _AddImportDependency();
        _AddCommnet( "Db 컬럼 메타 데이터" );
        _AddDbInfo();
        _AddDbTable();
        _AddMacro();
        _AddCommnet( $"{tableInfo.Name}테이블 메타 데이터 전역 객체" );
        _AddGlobalDbTable();
        _AddCommnet( $"{tableInfo.Name}테이블과 동기화 가능한 Db Orm 객체" );
        _AddDbModel( _tableInfo.SourceName );
    }

    private void _AddMacro()
    {
        _ctx.WriteLine( "#define DECLARE_COLUMN( SourceFieldName, DbFieldName ) \\" )
            .WriteLine( "struct SourceFieldName final : public NullColumn \\" )
            .WriteLine( "{ \\" )
            .WriteLine( "     using DbType = DbModelType; \\" )
            .WriteLine( "\\" )
            .WriteLine( $"     inline static const char* name                 {{ {_tableInfo.SourceName}DbInfo::columnStrings[ DbFieldName ] }};  \\" )
            .WriteLine(  "     inline static const std::string_view  name_view{ name };\\" )
            .WriteLine( $"     inline static const wchar_t*          nameW    {{ {_tableInfo.SourceName}DbInfo::columnWStrings[ DbFieldName ] }};  \\" )
            .WriteLine(  "     inline static const std::wstring_view nameViewW{ nameW          };  \\" )
            .WriteLine( "     inline static constexpr DbInfoType::EColumn no{ DbFieldName };  \\" )
            .WriteLine( "};" );
    }

    private void _AddGlobalDbTable()
    {
        _ctx.WriteLine(
            $"export inline extern {_tableInfo.SourceName}DbTableInfo* G{_tableInfo.SourceName}DbTableInfo{{ new {_tableInfo.SourceName}DbTableInfo() }};" );
    }

    private void _AddDbModel( string tableSourceName )
    {
        _ctx.WriteLine(
@$"export struct {tableSourceName}DbModel : public DbModel
{{
    using BaseDbModel = DbModel;
    using NullColumn  = NullColumn;
    using DbModelType = {tableSourceName}DbModel;
    using DbInfoType  = {tableSourceName}DbInfo;
" );

        // ADD DECLARE_COLUMN( ColumnName, ColumnNameText );
        _tableInfo.ForEachField( column =>
        {
            int sourceSpaceLength = _tableInfo.MaxSourceFieldNameLength - column.SrcName.Length;
            int dbSpaceLegnth     = _tableInfo.MaxDbFieldNameLength     - column.DbName.Length;
            sourceSpaceLength = Math.Max( sourceSpaceLength, 0 );
            dbSpaceLegnth     = Math.Max( dbSpaceLegnth, 0 );

            _ctx.Write( "    DECLARE_COLUMN( " );
            _ctx.Write( column.SrcName );
            _ctx.WriteLine( $", {CodeHelper.Spaces[ sourceSpaceLength ]}{_tableInfo.SourceName}DbInfo::EColumn::{column.DbName} );" );
        } );

        _ctx.WriteLine( "" );

        // ADD DECLARE_INDEX( ColumnName, ColumnNameText, DbModelType );
        _tableInfo.ForEachIndex( index =>
        {
            string columnNames = string.Join( ", ", index.FieldInfoList.Select( column => $"{column.SrcName}::no" ) );
            string columnTypes = string.Join( ", ", index.FieldInfoList.Select( column => $"{column.SrcFieldType}" ) );

            using ( var indexScope = _ctx.WriteLine( $"struct {index.Name} final : public NullIndex" ).Scope2( true, ";" ) )
            {
                _ctx.WriteLine( $"using DbType = DbModelType;" );
                _ctx.WriteLine( $"using Tuple = std::tuple< { columnTypes } >;" );
                _ctx.WriteLine( $"static constexpr DbInfoType::EColumn cols[]" )
                    .WriteLine( "{" )
                    .WriteLine( $"    {columnNames}," )
                    .WriteLine( "};" );
            }
        } );

        // Ctor
        _AddCommnet( "생성자", 1 );
        _ctx.WriteLine( $"    {tableSourceName}DbModel()" )
            .WriteLine( $"    : BaseDbModel{{ *G{tableSourceName}DbTableInfo }}" )
            .WriteLine( @"    , _columns" )
            .WriteLine( @"        {" );
        foreach ( var column in _tableInfo.FieldInfoList )
            _ctx.WriteLine( $"            G{tableSourceName}DbTableInfo->_columnInfos[ {tableSourceName}DbInfo::{column.DbName} ], " );
        _ctx.WriteLine( @"        }" )
            .WriteLine( @"    {" )
            .WriteLine( $"        _columnList.reserve( {tableSourceName}DbInfo::max );" )
            .WriteLine( $"        for ( int n{{}}; n < {tableSourceName}DbInfo::max; n += 1 )" )
            .WriteLine( @"            _columnList.push_back( &_columns[ n ] );" )
            .WriteLine( @"    }" );

        // Dtor
        _AddCommnet( "소멸자", 1 );
        _ctx.WriteLine( $"    ~{_tableInfo.SourceName}DbModel() override = default;" );

        // Getter
        foreach ( var column in _tableInfo.FieldInfoList )
        {
            _AddCommnet( $"{column.SrcName}를( 을 ) 반환한다", 1 );
            _ctx.WriteLine( $"    {column.ArgType} Get{column.SrcName}() const" )
                .WriteLine( @"    {" );
            if ( column.IsStrongType )
                _ctx.WriteLine( $"        return {{ {column.AccessSymbolIfNeed}_columns[ {tableSourceName}DbInfo::EColumn::{column.DbName} ]._var._{column.FieldTypeInVariantCpp} }};" );
            else
                _ctx.WriteLine( $"        return {column.AccessSymbolIfNeed}_columns[ {tableSourceName}DbInfo::EColumn::{column.DbName} ]._var._{column.FieldTypeInVariantCpp};" );
            _ctx.WriteLine( @"    }" );
        }

        // Setter
        foreach ( var col in _tableInfo.FieldInfoList )
        {
            _AddCommnet( $"{col.SrcName}를( 을 ) 설정한다", 1 );
            _ctx.WriteLine( $"    void Set{col.SrcName}( {col.ArgType} {col.DbName} )" )
                .WriteLine( @"    {" )
                .WriteLine( $"        {col.AccessSymbolIfNeed}_columns[ {_tableInfo.SourceName}DbInfo::EColumn::{col.DbName} ]._var._{col.FieldTypeInVariantCpp} = {col.GatValueInSetter};" )
                .WriteLine( $"        SetUpdateColumn( {_tableInfo.SourceName}DbInfo::EColumn::{col.DbName} );" )
                .WriteLine( @"    }" );

            if ( !col.InIndex )
                continue;

            _ctx.WriteLine( $"    void SetKey{col.SrcName}( {col.ArgType} {col.DbName} )" )
                .WriteLine( @"    {" )
                .WriteLine( $"        {col.AccessSymbolIfNeed}_columns[ {_tableInfo.SourceName}DbInfo::EColumn::{col.DbName} ]._var._{col.FieldTypeInVariantCpp} = {col.GatValueInSetter};" )
                .WriteLine( $"        SetUpdateAndSetupKeyColumn( {_tableInfo.SourceName}DbInfo::EColumn::{col.DbName} );" )
                .WriteLine( @"    }" );
        }

        // Factory
        _ctx.WriteLine( $"    inline static std::shared_ptr< {_tableInfo.SourceName}DbModel > GetFactory()" )
            .WriteLine( @"    {" )
            .WriteLine( $"            return G{_tableInfo.SourceName}DbTableInfo->Factory();" )
            .WriteLine( @"    }" )
            .NextLine()
            .WriteLine( $"    template< typename T >" )
            .WriteLine( $"    inline static void SetFactory( T&& factory )" )
            .WriteLine( @"    {" )
            .WriteLine( $"            G{_tableInfo.SourceName}DbTableInfo->Factory = std::forward< T >( factory );" )
            .WriteLine( @"    }" )
            .NextLine();

        var paramFilter = new HashSet< string >();
        _tableInfo.ForEachIndex( index =>
        {
            if ( index.FieldInfoList.Count < 2 )
                return;

            var fieldInfos = index.FieldInfoList.ToList();
            fieldInfos.RemoveAt( fieldInfos.Count - 1 );

            while ( fieldInfos.Count > 0 )
            {
                string param = string.Join(
                    ", ",
                    fieldInfos.Select( col => $"{col.ArgType} {col.DbName}" ) );

                string setKey = string.Join(
                    "",
                    fieldInfos.Select( col => $"forSelect.SetKey{col.SrcName}( {col.DbName} );\r\n\t\t" ) );

                fieldInfos.RemoveAt( fieldInfos.Count - 1 );
                if ( paramFilter.TryGetValue( param, out string _ ) )
                    continue;

                _ctx.WriteLine( $"    template< std::derived_from< {_tableInfo.SourceName}DbModel > T >" );
                _ctx.WriteLine( $"    static Vector< std::shared_ptr< T > > SelectMany( {param} )" )
                    .WriteLine( "    {" )
                    .WriteLine( $"        T forSelect;" )
                    .WriteLine( $"        forSelect.BindAllColumn();" )
                    .WriteLine( $"        {setKey}" )
                    .WriteLine( $"        return Mala::Db::SelectMany< T >( forSelect, LDbConnection );" )
                    .WriteLine( "    }" );
                paramFilter.Add( param );
            }
        } );

        // Member
        _ctx.WriteLine( @"protected:" )
            .WriteLine( $"    Mala::Db::Column _columns[ (int)( {_tableInfo.SourceName}DbInfo::EColumn::max ) ];" )
            .WriteLine( @"};" );
    }


    private void _AddDbTable()
    {
        string tableSourceName = _tableInfo.SourceName;
        _AddCommnet( $"{_tableInfo.Name} 테이블 인포 타입 정의" );
        using ( var dbTableInfo = _ctx.Struct( $"{_tableInfo.SourceName}DbTableInfo final : public TableInfo" ) )
        {
            _ctx.WriteLine( @"using Base = TableInfo;" )
                .NextLine()
                .WriteLine( $"Func< std::shared_ptr< struct {tableSourceName}DbModel > > Factory{{ []{{ return std::make_shared< struct {tableSourceName}DbModel >(); }} }};" )
                .NextLine();

            _AddCommnet( $"생성자" );
            using ( var ctor = _ctx.WriteLine( $"{tableSourceName}DbTableInfo()" ).Scope2() )
            {
                _ctx.WriteLine( $"_name  =  \"{_tableInfo.Name}\";" )
                    .WriteLine( $"_nameW = L\"{_tableInfo.Name}\";" )
                    .WriteLine( $"_columnInfos.reserve( {tableSourceName}DbInfo::max );" );

                using ( var @for = _ctx.For( $"for ( int n = 0; n < {tableSourceName}DbInfo::max; n += 1 )" ) )
                {
                    _ctx.WriteLine( @"auto* columnInfo{ _columnInfos.emplace_back( new ColumnInfo( *this, n ) ) };" )
                        .WriteLine( $"columnInfo->_type  = {tableSourceName}DbInfo::columnTypes[ n ];" )
                        .WriteLine( $"columnInfo->_name  = {tableSourceName}DbInfo::columnStrings[ n ];" )
                        .WriteLine( $"columnInfo->_nameW = {tableSourceName}DbInfo::columnWStrings[ n ];" )
                        .WriteLine( $"columnInfo->_isInPrimaryIndex   = {tableSourceName}DbInfo::columnInIndex[ n ];" )
                        .WriteLine( $"columnInfo->_isInSecondaryIndex = {tableSourceName}DbInfo::columnInIndex[ n ];" );
                }

                using ( var @indexInit = _ctx.WriteLine( @"_indexs =" ).Scope2( true, ";" ) )
                {
                    foreach ( var index in _tableInfo.IndexInfoList )
                    {
                        using ( var newIndexInfo = _ctx.WriteLine( @"new IndexInfo" ).Scope2( true, "," ) )
                        {
                            _ctx.WriteLine( @$"._name {{  ""{index.Name}"" }}," )
                                .WriteLine( @$"._nameW{{ L""{index.Name}"" }}," )
                                .WriteLine( $"._mask {{ 0b{Convert.ToString( index.BitFlagNum, 2 )} }}," );

                            using ( var colInfos = _ctx.WriteLine( @"._columnInfos" ).Scope2( true, "," ) )
                            {
                                foreach ( var columnInfo in index.FieldInfoList )
                                    _ctx.WriteLine( $"_columnInfos[ {tableSourceName}DbInfo::EColumn::{columnInfo.DbName} ]," );
                            }
                        }
                    }
                }
            }
        }
    }

    private void _AddDbInfo()
    {
        using ( var dbInfo = _ctx.Struct( $"{_tableInfo.SourceName}DbInfo" ) )
        {
            using ( var @enum = _ctx.WriteLine( $"enum EColumn" ).Scope2( true, ";" ) )
            {
                foreach ( var column in _tableInfo.FieldInfoList )
                    _ctx.WriteLine( $"{column.DbName}," );
                _ctx.WriteLine( "max," );
            }

            using ( var str = _ctx.WriteLine( @"inline static const char* columnStrings[ EColumn::max ]" ).Scope2( true, ";" ) )
            {
                foreach ( var column in _tableInfo.FieldInfoList )
                    _ctx.WriteLine( $"\"{column.DbName}\"," );
            }

            using ( var wstr = _ctx.WriteLine( $"inline static const wchar_t* columnWStrings[ EColumn::max ]" ).Scope2( true, ";" ) )
            {
                foreach ( var column in _tableInfo.FieldInfoList )
                    _ctx.WriteLine( $"L\"{column.DbName}\"," );
            }

            using ( var types = _ctx.WriteLine( @"inline static constexpr Mala::Core::EVarType columnTypes[ EColumn::max ]" ).Scope2( true, ";" ) )
            {
                foreach ( var column in _tableInfo.FieldInfoList )
                    _ctx.WriteLine( $"Mala::Core::EVarType::{column.EnumTypeInVariantCpp}," );
            }

            using ( var defaultVal = _ctx.WriteLine( $"inline static Mala::Core::Variant columnDefaultValue[ EColumn::max ]" ).Scope2( true, ";" ) )
            {
                foreach ( var column in _tableInfo.FieldInfoList )
                    _ctx.WriteLine( $"{{ columnTypes[ {column.DbName} ] }}," );
            }

            using ( var inIndex = _ctx.WriteLine( $"inline static bool columnInIndex[ EColumn::max ]" ).Scope2( true, ";" ) )
            {
                foreach ( var column in _tableInfo.FieldInfoList )
                    _ctx.WriteLine( $"{column.InIndex.ToString().ToLower()}," );
            }
        }
    }

    public void _AddModule()
    {
        _ctx.WriteLine( $"export module {_tableInfo.SourceName}DbModel;" );
    }

    public void _AddCommnet( string text, int depth = 0 )
    {
        _ctx.WriteLine( "" )
            .WriteLine( $"{CodeHelper.Tabs[ depth ]}/// <summary>" )
            .WriteLine( $"{CodeHelper.Tabs[ depth ]}/// {text}" )
            .WriteLine( $"{CodeHelper.Tabs[ depth ]}/// </summary>" );
    }


    public void _AddImportDependency()
    {
        _ctx.WriteLine(
            @"
import <string>;
import <string_view>;
import <vector>;
import <functional>;

import Mala.Core.Variant;
import Mala.Core.CoreTLS;
import Mala.Container;

import Mala.Db.DbTypes;
import Mala.Db.Table;
import Mala.Db.Column;
import Mala.Db.ColumnInfo;
import Mala.Db.IndexInfo;
import Mala.Db.DbModel;
import Mala.Db.DbModelHelper;

import CommonTypes;

using namespace std;
using namespace Mala::Container;
using namespace Mala::Db;

" );

    }
    internal void Build()
    {
        string code = _ctx.Code;
    }
}