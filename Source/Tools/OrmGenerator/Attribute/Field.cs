﻿/// <summary>
/// 필드 속성
/// </summary>
[AttributeUsage( AttributeTargets.Field )]
public class Field : Attribute
{
    /// <summary>
    /// 설명
    /// </summary>
    public string DbName { get; set; } = string.Empty;

    /// <summary>
    /// 타입
    /// </summary>
    public string? DbType { get; set; } = string.Empty;

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="dbName"></param>
    public Field( string dbName, string? dbType = null )
    {
        DbName = dbName;
        DbType = dbType;
    }
}
