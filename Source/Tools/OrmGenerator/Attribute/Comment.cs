﻿/// <summary>
/// 주석 속성
/// </summary>
[AttributeUsage( AttributeTargets.Field | AttributeTargets.Class )]
public class Comment : System.Attribute
{
    public string Message { get; set; } = string.Empty;

    public override string ToString() => Message;
    public Comment( string message ) { Message = message; }
}
