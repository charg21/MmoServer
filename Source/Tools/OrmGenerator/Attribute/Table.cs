﻿/// <summary>
/// 테이블 속성
/// </summary>
[AttributeUsage( AttributeTargets.Class )]
public class Table : System.Attribute
{
    /// <summary>
    /// 테이블 이름
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 엔진
    /// </summary>
    public string Engine { get; set; } = "innodb";

    /// <summary>
    /// 생성자
    /// </summary>
    public Table( string name, string? engine = null )
    {
        Name    = name;
        Engine  = engine;
    }
}