﻿/// <summary>
/// 기본 값 속성
/// </summary>
[AttributeUsage( AttributeTargets.Field )]
public class Default : Attribute
{
    public string Value { get; set; } = string.Empty;

    public Default( string value )
    {
        Value = value;
    }
}
