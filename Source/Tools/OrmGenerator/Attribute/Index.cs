﻿/// <summary>
/// 인덱스 속성
/// </summary>
[AttributeUsage( AttributeTargets.Field, AllowMultiple = true )]
public class Index : Attribute
{
    public string Name { get; set; } = string.Empty;

    public int IndexFieldNo { get; set; } = 0;

    public Index( string name, int indexFieldNo = 0 )
    {
        Name = name;
        this.IndexFieldNo = indexFieldNo;
    }
}
