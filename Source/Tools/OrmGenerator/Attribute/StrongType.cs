﻿/// <summary>
/// 커스텀 타입 속성
/// </summary>
[AttributeUsage( AttributeTargets.Field )]
public class StrongType : Attribute
{
    /// <summary>
    /// 타입
    /// </summary>
    public string Type { get; set; } = string.Empty;

    /// <summary>
    /// 생성자
    /// </summary>
    public StrongType( string type )
    {
        Type = type;
    }
}