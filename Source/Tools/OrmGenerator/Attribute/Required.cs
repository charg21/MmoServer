﻿/// <summary>
/// 필수 항목 속성 ( Not Null )
/// </summary>
[AttributeUsage( AttributeTargets.Field )]
public class Required : Attribute
{
}