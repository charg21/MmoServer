using System.Text;

namespace OrmGenerator;

public class QueryBuilder
{
    StringBuilder _stringBuilder = new();

    /*
    CREATE TABLE `test`.`pc` (
      `name` VARCHAR(45) NOT NULL,
      `x` VARCHAR(45) NOT NULL,
      `y` VARCHAR(45) NOT NULL,
      `z` VARCHAR(45) NOT NULL,
      PRIMARY KEY (`name`),
      INDEX `Secondary` (`x` ASC, `y` ASC) VISIBLE);

    CREATE TABLE `test`.`p` (
  `a` INT NOT NULL,
  `b` VARCHAR(45) NULL,
  `c` VARCHAR(45) NULL,
  `d` VARCHAR(45) NULL,
  PRIMARY KEY (`a`),
  INDEX `a` (`a` ASC, `d` ASC) INVISIBLE,
  INDEX `b` (`b` ASC, `c` ASC) VISIBLE,
  INDEX `c` (`b` ASC, `d` ASC) INVISIBLE,
  INDEX `e` (`a` ASC, `b` ASC, `c` ASC, `d` ASC) VISIBLE);

    CREATE TABLE `test`.`rrr` (
  `idrrra` INT NOT NULL DEFAULT 23,
  `rrrcol` VARCHAR(45) NULL DEFAULT 'ggg',
  PRIMARY KEY (`idrrra`));
     */

    public string BuildCreateQuery( string schema, TableInfo tableInfo )
    {
        _stringBuilder.AppendLine( $"CREATE TABLE `{schema}`.`{tableInfo.Name}` (" );

        tableInfo.ForEachField( field =>
        {
            _stringBuilder.Append( $"`{field.DbName}` {field.DbType} NOT NULL" );
            if ( !string.IsNullOrEmpty( field.DefaultValueString() ) )
                if ( !field.IsStringType || !string.IsNullOrEmpty( field.DefaultValue ) )
                    _stringBuilder.Append( $" DEFAULT {field.DefaultValueString()}" );

            _stringBuilder.AppendLine( "," );
        } );

        _AddPrimaryIndex( tableInfo );
        _AddSecondaryIndex( tableInfo );
        _AddUniqueIndex( tableInfo );
        _stringBuilder.Append( ')' );
        _AddCharacterSet( tableInfo );
        _stringBuilder.Append( ';' );

        return _stringBuilder.ToString();
    }

    private void _AddUniqueIndex( TableInfo tableInfo )
    {
        tableInfo.ForEachField( field =>
        {
            if ( !field.HasUniqueKey )
                return;

            // ", uqniue index { col.DbName }_UNIQUE ( `{ col.name }` ) VISIBLE"
            _stringBuilder.Append( ", UNIQUE INDEX`" );
            _stringBuilder.Append( field.DbName       );
            _stringBuilder.Append( "_UNIQUE` ( `"     );
            _stringBuilder.Append( field.DbName       );
            _stringBuilder.Append( "` ) VISIBLE"      );
        } );
    }

    private void _AddPrimaryIndex( TableInfo tableInfo )
    {
        _stringBuilder.Append( "PRIMARY KEY (" );
        tableInfo.PrimaryIndex.ForEachFieldInfo( column => _stringBuilder.Append( $"`{column.DbName}`," ) );
        _stringBuilder.Remove( _stringBuilder.Length - 1, 1 );
        _stringBuilder.Append( ')' );
    }

    private void _AddSecondaryIndex( TableInfo tableInfo )
    {
        // add secondary index
        if ( tableInfo.HasSecondaryIndex )
        {
            //, index `{ col }` (`{ col }`),
            _stringBuilder.AppendLine( "," );

            for ( int i = 1; i < tableInfo.IndexInfoList.Count; i++ )
            {
                _stringBuilder.Append( $"INDEX `{ tableInfo.IndexInfoList[ i ].Name }` (" );

                foreach ( var column in tableInfo.IndexInfoList[ i ].FieldInfoList )
                    _stringBuilder.Append( $"`{ column.DbName }`," );

                _stringBuilder.Remove( _stringBuilder.Length - 1, 1 );
                _stringBuilder.AppendLine( $")," );
            }

            _stringBuilder.Remove( _stringBuilder.Length - 3, 3 );
        }
    }

    void _AddCharacterSet( TableInfo tableInfo )
    {
        _stringBuilder.Append( $"DEFAULT CHARACTER SET = {tableInfo.CharacterSet.ToString()}" );
    }

    public string BuildDropQuery( string schema, TableInfo tableInfo )
    {
        return $"DROP TABLE `{schema}`.`{tableInfo.Name}`;";
    }

    public string BuildAlterTableQuery( string schema, TableInfo localTableInfo, TableInfo remoteTableInfo )
    {
        /// 종속성에 의해 순서대로 진행해야함
        /// 1. 인덱스 제거
        /// 2. 필드 제거
        /// 3. 필드 추가
        /// 4. 인덱스 추가

        /// 1. 인덱스 제거( DROP INDEX )
        remoteTableInfo.ForEachIndex( remoteIdx =>
        {
            if ( !localTableInfo.IndexInfoDict.ContainsKey( remoteIdx.Name ) )
                _stringBuilder.AppendLine( $"ALTER TABLE `{schema}`.`{localTableInfo.Name}` DROP INDEX `{remoteIdx.Name}`;" );
        } );

        /// 2. 필드 제거( DROP COLUMN )
        remoteTableInfo.ForEachField( field =>
        {
            if ( !localTableInfo.FieldInfoDict.ContainsKey( field.DbName ) )
                _stringBuilder.AppendLine( $"ALTER TABLE `{schema}`.`{localTableInfo.Name}` DROP COLUMN `{field.DbName}`;" );
        } );

        /// 2. 필드 추가( add or modify )
        localTableInfo.ForEachField( localField =>
        {
            /// Add: 필드가 없는 경우
            if ( !remoteTableInfo.FieldInfoDict.TryGetValue( localField.DbName, out var remoteField ) )
                _stringBuilder.AppendLine( $"ALTER TABLE `{schema}`.`{localTableInfo.Name}` ADD COLUMN `{localField.DbName}` {localField.DbType} NOT NULL;" );

            /// Modify: 먼가 먼가... 다름
            else if ( !remoteField.IsEquals( localField ) )
            {
                if ( localField.Nullable != remoteField.Nullable )
                    _stringBuilder.AppendLine( $"ALTER TABLE `{schema}`.`{localTableInfo.Name}` MODIFY COLUMN `{localField.DbName}` {localField.DbType} {(localField.Nullable?"":"NOT NULL")};" );
                else if ( localField.DbType != remoteField.DbType )
                    _stringBuilder.AppendLine( $"ALTER TABLE `{schema}`.`{localTableInfo.Name}` MODIFY COLUMN `{localField.DbName}` {localField.DbType};" );
            }
        } );

        // FIELD RENAME( 이건 대응 가능한가? 생각을 해봐야할듯... )
        //_stringBuilder.AppendLine( $"CHANGE COLUMN `{field.DbName}` `{field.DbName}` {field.DbType} NOT NULL;" );
        /// 3. 인덱스 추가
        localTableInfo.ForEachIndex( localIdx =>
        {
            if ( !remoteTableInfo.IndexInfoDict.TryGetValue( localIdx.Name, out var remoteIndex ) )
            {
                if ( localIdx.FieldInfoList.Count > 0 )
                {
                    _stringBuilder.Append( $"ALTER TABLE `{schema}`.`{localTableInfo.Name}` ADD INDEX `{localIdx.Name}` (" );
                    foreach ( var field in localIdx.FieldInfoList )
                        _stringBuilder.Append( $"`{field.DbName}`," );
                    _stringBuilder.Remove( _stringBuilder.Length - 1, 1 );
                    _stringBuilder.AppendLine( ");" );
                }
                else
                {
                    Console.WriteLine( "인덱스에 열이 없음. ( 스키마 에러 )" );
                }
            }
            else if ( !remoteIndex.IsEquals( localIdx ) )
            {
                Console.WriteLine( "인덱스 다름 아직 미지원..." );
            }

        } );

        //    // ADD COLUMN `3` VARCHAR(45) NULL AFTER `player2colw`;
        //    _stringBuilder.AppendLine( $"ADD COLUMN `{field.DbName}` {field.DbType} NOT NULL" );// DEFAULT { field.DefaultValueString() }{ columnPosition },");

        return _stringBuilder.ToString();
    }
}
