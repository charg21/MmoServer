﻿using CodeGen;

Config.Load( args );

var excelParser = new ExcelParser();
var paramList = excelParser.Parse( "" );

foreach ( var param in paramList )
{
    var managerGeneratoer = new DesignManagerGenerator();
    var managerContext = managerGeneratoer.Generate( param );

    var managerTemplateGeneratoer = new DesignManagerTemplateGenerator();
    var managerTemplateContext    = managerTemplateGeneratoer.Generate( param );

    var generatoer = new DesignGenerator();
    var context = generatoer.Generate( param );

    var templateGeneratoer = new DesignTemplateGenerator();
    var templateContext = templateGeneratoer.Generate( param );

    var ptrGeneratoer = new DesignPtrGenerator();
    var ptrContext = ptrGeneratoer.Generate( param );

    
    //Console.WriteLine( $"Coed:\n\n\n{ managerContext.Code }");
    Console.WriteLine( $"Coed:\n\n\n{ managerTemplateContext.Code }");
    Console.WriteLine( $"Coed:\n\n\n{ context.Code }");
    Console.WriteLine( $"Coed:\n\n\n{ templateContext.Code }");
    //Console.WriteLine( $"Coed:\n\n\n{ ptrContext.Code }" );

    Exporter.Export( "Output\\", $"{ param.Design }.ixx", context.Code );
    Exporter.Export( "Output\\", $"{ param.DesignTemplate }.ixx", templateContext.Code );
    Exporter.Export( "Output\\", $"{ param.DesignPtr }.ixx", ptrContext.Code );
    Exporter.Export( "Output\\", $"{ param.DesignManager }.ixx", managerContext.Code );
    Exporter.Export( "Output\\", $"{ param.DesignManagerTemplate }.ixx", managerTemplateContext.Code );

    if ( param.HasGroupIndexColumn )
    {
        var groupPtrGeneratoer = new DesignGroupPtrGenerator();
        var groupPtrContext = groupPtrGeneratoer.Generate( param );

        Console.WriteLine( $"Coed:\n\n\n{ groupPtrContext.Code }" );
        Exporter.Export( "Output\\", $"{ param.DesignGroupPtr }.ixx", groupPtrContext.Code );
    }
}

var managerCenterGeneratoer = new DesignManagerCenterGenerator();
var managerCenterContext = managerCenterGeneratoer.Generate( paramList );
Exporter.Export( "Output\\", $"DesignManagerCenter.ixx", managerCenterContext.Code );
