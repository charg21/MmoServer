﻿using CodeGen;
using System.Text.Json;
//using System.Text.Json.JsonSerializer.IsReflectionEnabledByDefault;

//public class MyTypeInfoResolver : IJsonTypeInfoResolver
//{

//    public JsonTypeInfo? GetTypeInfo( Type type, JsonSerializerOptions options )
//    {
//        var op = new JsonSerializerOptions()
//        {
//            PropertyNamingPolicy = JsonNamingPolicy.CamelCase
//        };

//        if ( type == typeof( List< string > ) )
//        {
//            return JsonTypeInfo.CreateJsonTypeInfo( type, op );
//        }

//        return null;
//    }
//}

/// <summary>
/// 환경
/// </summary>
public static class Config
{
    /// <summary>
    /// 임포트 리스트
    /// </summary>
    public static string ImportList = string.Empty;

    /// <summary>
    /// 커스텀 타입( StrongType )
    /// </summary>
    public static List< CustomType > CustomTypeList = new();
    public static Dictionary< string, CustomType > CustomTypeDict = new();

    /// <summary>
    /// 열거형 타입
    /// </summary>
    public static List< CustomType > EnumTypeList { get; set; } = new();
    public static Dictionary< string, CustomType > EnumTypeDict = new();

    public static void Load( string[] args )
    {
        string configPath = "DesignGenerator.json";

        if ( args.Length > 0 && !string.IsNullOrEmpty( args[ 0 ] ) )
            configPath = args[ 0 ];
        
        var configString = Importer.Import( configPath );
        Console.WriteLine( $"Config Path = { configPath }" );

        var config = JsonSerializer.Deserialize< ConfigDto >( configString );
        ImportFrom( in config );

        foreach ( var customType in Config.CustomTypeList )
            Config.CustomTypeDict.Add( customType.Name, customType );

        foreach ( var customType in Config.EnumTypeList )
            Config.EnumTypeDict.Add( customType.Name, customType );
    }

    private static void ImportFrom< T >( in T config )
    {
        foreach ( var propConfig in config.GetType().GetProperties() )
        {
            var fieldEnv = typeof( Config ).GetField( propConfig.Name );
            if ( fieldEnv != null )
            {
                fieldEnv.SetValue( null, propConfig.GetValue( config ) );
                Console.WriteLine( $"{ propConfig.Name } = { propConfig.GetValue( config ) } " );
            }
        }
    }
}

public class CustomType
{
    public string Name { get; set; } = string.Empty;

    public string BaseType { get; set; } = string.Empty;
}


/// <summary>
/// 
/// </summary>
//[JsonSerializable( typeof( ConfigDto ) )]
public class ConfigDto
{
    /// <summary>
    /// 생성자
    /// </summary>
    public ConfigDto()
    {
    }

    /// <summary>
    /// 임포트 리스트
    /// </summary>
    public List< CustomType > CustomTypeList { get; set; } = new();

    /// <summary>
    /// 임포트 리스트
    /// </summary>
    public List< CustomType > EnumTypeList { get; set; } = new();

    /// <summary>
    /// 임포트 리스트
    /// </summary>
    public List< string > ImportList { get; set; } = new();
}