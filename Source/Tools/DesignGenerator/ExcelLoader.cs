﻿using MiniExcelLibs;
using MiniExcelLibs.OpenXml;

/// <summary>
///
/// </summary>
public class ExcelParser
{
    OpenXmlConfiguration config = new() { FillMergedCells = false, };

    public List< DesignGenParam > Parse( string path )
    {
        if ( path.Length == 0 )
            path = Directory.GetCurrentDirectory();

        if ( !Directory.Exists( path ) )
        {
            Console.WriteLine( $"Not Found Directory... Path : {path}" );
            return null;
        }

        var paramList = new List< DesignGenParam >();
        var filePathList = Directory.GetFiles( path );
        foreach ( var filePath in filePathList )
        {
            if ( !filePath.Contains( ".xlsx" ) )
                continue;

            if ( filePath.Contains( ".lnk" ) )
                continue;

            var param = _ParseParam( filePath );
            if ( param is null )
            {
                Console.WriteLine( $"Invalid File Path... : { filePath }" );
                continue;
            }

            foreach ( var p in param )
            {
                paramList.Add( p );
            }

        }

        return paramList;
    }

    private List< DesignGenParam > _ParseParam( string path )
    {
        var sheetNames = MiniExcel.GetSheetNames( path, config );
        var paramList = new List< DesignGenParam >();
        foreach ( var sheetName in sheetNames )
        {
            var rows = MiniExcel.Query( path, false, sheetName ).Cast< IDictionary< string, object > >().ToList();

            _ParseDesignColumn( rows, out var names, out var types, out var descList, out var useList );

            var columns   = MiniExcel.GetColumns( path, true, sheetName, configuration: config ).ToList();
            var columnCnt = columns.Count;
            var param = new DesignGenParam();
            param.SheetName = sheetName;
            for ( int i = 0; i < columnCnt; i += 1 )
            {
                var col = new DesignColumn
                {
                    Name = names[ i ],
                    Type = types[ i ].Replace( "*", "" ).Replace( "#", "" ),
                    Desc = descList[ i ],
                    Use  = useList[ i ],
                    IsIndexColumn = types[ i ].Contains( '*' ),
                    IsGroupIndexColumn = types[ i ].Contains( '#' ),
                };

                param.IsIndexColumn       |= col.IsIndexColumn;
                param.HasGroupIndexColumn |= col.IsGroupIndexColumn;
                param.DesignColumns.Add( col );

                if ( col.IsGroupIndexColumn )
                    param.AddGroup( col );
            }

            paramList.Add( param );
        }

        return paramList;
    }

    private static void _ParseDesignColumn(
        List< IDictionary< string, object > > rows,
        out List< string > names,
        out List< string > types,
        out List< string > descList,
        out List< string > useList )
    {
        names    = new List< string >();
        types    = new List< string >();
        descList = new List< string >();
        useList  = new List< string >();

        foreach ( var value in rows[ Const.SchemaName ].Values.ToList() )
            names.Add( value.ToString() );
        foreach ( var value in rows[ Const.SchemaType ].Values.ToList() )
            types.Add( value.ToString() );
        foreach ( var value in rows[ Const.SchemaDesc ].Values.ToList() )
            descList.Add( value.ToString() );
        foreach ( var value in rows[ Const.SchemaIndex ].Values.ToList() )
            useList.Add( value.ToString() );
    }
}
