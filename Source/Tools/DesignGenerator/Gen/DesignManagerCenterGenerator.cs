﻿using CodeGen;

/// <summary>
/// 디자인 매지너 센터 제네레이터
/// </summary>
public class DesignManagerCenterGenerator
{
    /// <summary>
    ///
    /// </summary>
    public CodeGenContext Generate( List< DesignGenParam > param )
    {
        var context = new CodeGenContext();

        _GenImportCode( context, param );
        _GetClassHeader( context );
        //_GenMethod( context, param );
        _GenField( context );
        _GenClassFooter( context );
        _GenGlobalInstance( context );
        context.WriteLine( $"EXPORT_END" );

        return context;
    }

    private static void _GenClassFooter( CodeGenContext context )
    {
        context.WriteLine(
        $"}};" );
        context.NextLine();
    }

    private static void _GetClassHeader( CodeGenContext context )
    {
        context.WriteLine( $"EXPORT_BEGIN" );
        context.NextLine();
        context.WriteLine( $"export class DesignManagerCenter\n" +
        $"{{" );
        context.Indent();
        context.WriteLine( $"GENERATE_CLASS_TYPE_INFO( DesignManagerCenter );" );
        context.NextLine();
        context.UnIndent();
        context.WriteLine( $"public:" );
        context.Indent();
        context.Comment( $"디자인 매니저 목록" );
        context.WriteLine( $"using DesignManagerList = Vector< IDesignManager* >;" );
        context.UnIndent();
        context.NextLine();
    }

    private void _GenMethod( CodeGenContext context, List< DesignGenParam > param )
    {
        //foreach ( var column in param.DesignColumns )
        //{
        //    //var colName = CaseHelper.PascalCaseToCamelCase( column.Name );
        //    //context.WriteLine( $"\t/// {column.Desc}" );
        //    //context.WriteLine( $"\tFiled( _{colName} )" );
        //    //context.WriteLine( $"\t{column.Type} _{colName};" );
        //    //context.NextLine();
        //}
    }

    public void _GenField( CodeGenContext context )
    {
        //foreach ( var column in param.DesignColumns )
        //{
        //    var colName = CaseHelper.PascalCaseToCamelCase( column.Name );
        //    context.WriteLine( $"\t/// { column.Desc }" );
        //    context.WriteLine( $"\tFiled( _{ colName } )" );
        //    context.WriteLine( $"\t{ column.Type } _{ colName }{{}};" );
        //    context.NextLine();
        //}
        context.WriteLine( $"public:" );
        context.Indent();
        context.Comment( $"디자인 매니저 목록" );
        context.WriteLine( $"Field( _designManagerList )" );
        context.WriteLine( $"DesignManagerList _designManagerList;" );
        context.UnIndent();
    }

    public void _GenImportCode( CodeGenContext context, List< DesignGenParam > paramList )
    {
        context.WriteLine( $"export module DesignManagerCenter;" );
        context.NextLine();
        context.WriteLine( $"import <Macro.h>;" );
        context.NextLine();
        context.WriteLine( $"import Mala.Core.Types;" );
        context.WriteLine( $"import Mala.Container;"  );
        context.WriteLine( $"import Mala.Reflection;" );
        context.NextLine();
        context.WriteLine( $"import DesignTypes;" );
        context.WriteLine( $"import IDesignManager;" );
        context.WriteLine( $"import IDesign;" );
        context.NextLine();

        foreach ( var param in paramList )
            context.WriteLine( $"import { param.DesignManager };" );
        if ( paramList.Count != 0 )
            context.NextLine();
    }

    public void _GenGlobalInstance( CodeGenContext context )
    {
        context.WriteLine( $"extern inline DesignManagerCenter* GDesignManagerCenter{{}};" );
        context.NextLine();
    }
}
