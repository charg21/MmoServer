using CodeGen;
using CodeGenerator;

/// <summary>
/// 디자인 오브젝트 제네레이터
/// </summary>
public class DesignTemplateGenerator
{
    /// <summary>
    ///
    /// </summary>
    public CodeGenContext Generate( DesignGenParam param )
    {
        var context = new CodeGenContext();

        _GenImportCode( context, param );
        _GetClassHeader( param, context );
        _GenMethod( context, param );
        _GenField( context, param );
        _GenClassFooter( context );

        return context;
    }

    private static void _GenClassFooter( CodeGenContext context )
    {
        context.WriteLine(
        $"}};" );
        context.NextLine();
        context.WriteLine( $"EXPORT_END" );
    }

    private static void _GetClassHeader( DesignGenParam param, CodeGenContext context )
    {
        context.WriteLine( $"EXPORT_BEGIN" );
        context.NextLine();
        context.WriteLine( $"class { param.DesignManagerTemplate };" );
        context.NextLine();
        context.WriteLine( $"class { param.DesignTemplate } : public IDesign\n" +
        $"{{" );
        context.Indent();
        context.WriteLine( $"GENERATE_CLASS_TYPE_INFO( { param.DesignTemplate } );" );
        context.UnIndent();
        context.NextLine();
    }

    private void _GenMethod( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( "public:" );
        context.Indent();

        context.Comment( $"생성자" );
        context.WriteLine( $"{ param.DesignTemplate }() = default;" );
        context.WriteLine( $"{ param.DesignTemplate }({ param.DesignCtorParamsWithType } )" );
        context.Write( $": " );

        int offset = -1;
        foreach ( var column in param.DesignColumns )
        {
            var colName = CaseHelper.PascalCaseToCamelCase( column.Name );
            context.WriteLine( $"_{ colName }{{ { colName } }},", offset );

            offset = 0;
        }

        if ( param.DesignColumns.Count != 0 )
            context.Remove( 3 ); // remove ",\n"

        context.WriteLine( $"\n\t{{}}" );

        context.UnIndent();
    }

    public void _GenField( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( "public:" );
        context.Indent();
        foreach ( var column in param.DesignColumns )
        {
            var colName = CaseHelper.PascalCaseToCamelCase( column.Name );

            context.Comment( $"{ column.Desc }" );
            context.WriteLine( $"Field( _{ colName } )" );
            context.WriteLine( $"{ column.ToCppType() } _{ colName }{{}};" );
            context.NextLine();
        }

        context.Comment( $"매니저" );
        context.WriteLine( $"Field( _manager )" );
        context.WriteLine( $"const { param.DesignManagerTemplate }* _manager{{ nullptr }};" );
        context.NextLine();

        context.UnIndent();
    }

    public void _GenImportCode( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"export module { param.DesignTemplate };" );
        context.NextLine();
        context.WriteLine( $"import <Macro.h>;" );
        context.NextLine();
        context.WriteLine( $"import Mala.Core.Types;" );
        context.WriteLine( $"import Mala.Container;"  );
        context.WriteLine( $"import Mala.Reflection;" );
        context.NextLine();
        context.WriteLine( $"import DesignTypes;" );
        context.WriteLine( $"import IDesign;" );
        context.NextLine();
    }

}
