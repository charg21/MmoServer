using CodeGen;

/// <summary>
/// 디자인 오브젝트 제네레이터
/// </summary>
public class DesignManagerTemplateGenerator
{
    /// <summary>
    ///
    /// </summary>
    public CodeGenContext Generate( DesignGenParam param )
    {
        var context = new CodeGenContext();

        _GenImportCode( context, param );
        _GenClassHeader( param, context );
        _GenMethod( context, param );
        _GenField( context, param );
        _GenClassFooter( context );
        context.WriteLine( $"EXPORT_END" );

        return context;
    }

    private static void _GenClassFooter( CodeGenContext context )
    {
        context.WriteLine( $"}};" );
        context.NextLine();
    }

    private static void _GenClassHeader( DesignGenParam param, CodeGenContext context )
    {
        context.WriteLine( $"EXPORT_BEGIN" );
        context.NextLine();

        context.WriteLine( $"class { param.DesignManagerTemplate } : public IDesignManager\n" +
        $"{{" );
        context.Indent();
        context.WriteLine( $"GENERATE_CLASS_TYPE_INFO( { param.DesignManagerTemplate } );" );
        context.UnIndent();
        context.NextLine();

        context.WriteLine( $"public:" );
        context.Indent();
        context.Comment( "디자인 포인터 맵 타입 정의" );
        context.WriteLine( $"using { param.Design }Map = HashMap< { param.Design }Id, const { param.Design }* >;" );
        context.NextLine();
        context.Comment( "디자인 데이터 목록 타입 정의" );
        context.WriteLine( $"using { param.Design }List = Vector< { param.Design } >;" );
        context.NextLine();
        if ( param.HasGroupIndexColumn )
        {
            context.Comment( "그룹 맵 컨테이너" );
            // context.WriteLine( $"{param.Design}GroupMap _{param.SheetNameCamel}GroupMap;" );
            context.NextLine();
        }

        context.UnIndent();
    }

    private void _GenMethod( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"public:" );
        context.Indent();

        context.Comment( "데이터를 로드한다" );
        context.WriteLine( $"bool Load( StringRef path ) final" );
        context.Scope();
        context.WriteLine( $"auto doc = JsonHelper::LoadAndParse( path );" );
        context.WriteLine( $"if ( doc.HasParseError() )" );
        context.WriteLine( $"   return false;" );
        context.NextLine();
        context.WriteLine( $"_path = path;" );
        context.WriteLine( $"rapidjson::Value& v = doc[ \"{ param.SheetNameCamel }List\" ];" );
        context.WriteLine( $"_{ param.SheetNameCamel }DesignList.reserve( v.Size() );" );
        context.WriteLine( $"_{ param.SheetNameCamel }DesignMap.reserve( v.Size() );" );
        context.WriteLine( $"for ( decltype( v.Size() ) i{{}}; i < v.Size(); i += 1 )" );
        context.Scope();
        context.WriteLine( $"auto& _{ param.SheetNameCamel } = v[ i ];" );
        context.WriteLine( $"_{ param.SheetNameCamel }DesignList.emplace_back" );
        context.WriteLine( $"( \n{ param.DeserializeParams }" );
        context.WriteLine( $");" );
        context.UnScope();
        context.NextLine();
        context.WriteLine( $"return true;" );
        context.UnScope();
        context.NextLine();

        context.Comment( "초기화 한다" );
        context.WriteLine( $"bool Initialize() final" );
        context.Scope();
        context.WriteLine( $"for ( auto& design : _{ param.SheetNameCamel }DesignList )" );
            context.Scope();
            context.WriteLine( $"if ( !design.Initialize() )" );
            context.WriteLine( $"    return false;" );
            context.NextLine();
            context.WriteLine( $"design._manager = this;" );
            context.UnScope();
        context.NextLine();
        context.WriteLine( $"for ( auto& design : _{ param.SheetNameCamel }DesignList )" );
            context.Scope();
            context.WriteLine( $"_{ param.SheetNameCamel }DesignMap.emplace( design._id, &design );" );
            context.UnScope();
        context.NextLine();
        context.WriteLine( $"return true;" );
        context.UnScope();
        context.NextLine();

        context.Comment( "기획 데이터를 획득한다" );
        context.WriteLine( $"const { param.Design }* Get{ param.Design }( { param.Design }Id id ) const" );
        context.Scope();
        context.WriteLine( $"auto iter = _{ param.SheetNameCamel }DesignMap.find( id );" );
        context.WriteLine( $"if ( iter != _{ param.SheetNameCamel }DesignMap.end() )" );
        context.WriteLine( $"   return iter->second;" );
        context.NextLine();
        context.WriteLine( $"return nullptr;" );
        context.UnScope();
        context.NextLine();

        context.Comment( "기획 시트명을 반환한다" );
        context.WriteLine( $"const StringView& GetSheetName() const final {{ return _sheetName; }}" );
        context.NextLine();

        context.Comment( "경로를 반환한다" );
        context.WriteLine( $"const String& GetPath() const {{ return _path; }}" );
        context.NextLine();

        if ( param.HasGroupIndexColumn )
        {
            context.Comment( "그룹 맵 컨테이너" );
            // context.WriteLine( $"{param.Design}GroupMap _{param.SheetNameCamel}GroupMap;" );
            // context.NextLine();
        }

        context.UnIndent();
        context.NextLine();
    }

    public void _GenField( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"public:" );
        context.Indent();

        context.Comment( "맵 컨테이너" );
        context.WriteLine( $"{ param.Design }Map _{ param.SheetNameCamel }DesignMap;" );
        context.NextLine();

        context.Comment( "리스트 컨테이너" );
        context.WriteLine( $"{ param.Design }List _{ param.SheetNameCamel }DesignList;" );
        context.NextLine();

        if ( param.HasGroupIndexColumn )
        {
            context.Comment( "그룹 맵 컨테이너" );
            context.WriteLine( $"{ param.Design }GroupMap _{ param.SheetNameCamel }GroupMap;" );
            context.NextLine();
        }

        context.Comment( "시트 명" );
        context.WriteLine( $"Field( _sheetName )" );
        context.WriteLine( $"StringView _sheetName{{ L\"{ param.SheetName }\" }};" );
        context.NextLine();

        context.Comment( "경로" );
        context.WriteLine( $"Field( _path )" );
        context.WriteLine( $"String _path;" );
        context.NextLine();

        context.UnIndent();
    }

    public void _GenImportCode( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"export module { param.DesignManagerTemplate };" );
        context.NextLine();
        context.WriteLine( $"import <Macro.h>;" );
        context.WriteLine( $"import \"document.h\";" );
        context.WriteLine( $"import \"rapidjson.h\";" );
        context.NextLine();
        context.WriteLine( $"import Mala.Core.Types;" );
        context.WriteLine( $"import Mala.Container;"  );
        context.WriteLine( $"import Mala.Reflection;" );
        context.WriteLine( $"import Mala.Io.File;" );
        context.WriteLine( $"import Mala.Text;" );
        context.NextLine();
        context.WriteLine( $"import DesignTypes;" );
        context.WriteLine( $"import IDesignManager;" );
        context.WriteLine( $"import IDesign;" );
        context.WriteLine( $"import { param.Design };" );
        context.WriteLine( $"import JsonHelper;" );
        context.NextLine();
        context.WriteLine( $"using namespace Mala::Container;" );
        context.WriteLine( $"using namespace Mala::Text::Utf8;" );
        context.WriteLine( $"using namespace Mala::Text::Utf16;" );
        context.NextLine();
    }

}
