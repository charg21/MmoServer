using CodeGen;

/// <summary>
/// 디자인 오브젝트 제네레이터
/// </summary>
public class DesignGenerator
{
    /// <summary>
    ///
    /// </summary>
    public CodeGenContext Generate( DesignGenParam param )
    {
        var context = new CodeGenContext();

        _GenImportCode( context, param );
        _GetClassHeader( param, context );
        _GenMethod( context, param );
        _GenField( context, param );
        _GenClassFooter( context );

        return context;
    }

    private static void _GenClassFooter( CodeGenContext context )
    {
        context.WriteLine(
        $"}};" );
        context.NextLine();
        context.WriteLine( $"EXPORT_END" );
    }

    private static void _GetClassHeader( DesignGenParam param, CodeGenContext context )
    {
        context.WriteLine( $"EXPORT_BEGIN" );
        context.NextLine();
        context.Comment( $"{ param.SheetName } 기획 데이터 클래스" );
        context.WriteLine( $"class { param.Design } final : public { param.DesignTemplate }\n" +
        $"{{" );
        context.Indent();
        context.WriteLine( $"GENERATE_CLASS_TYPE_INFO( { param.Design } );" );
        context.UnIndent();
        context.NextLine();
    }

    private void _GenMethod( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( "public:" );
        context.Indent();
        context.Comment( $"데이터를 초기화한다." );
        context.WriteLine( $"bool Initialize() final{{ return true; }}" );
        context.NextLine();
        context.UnIndent();

        foreach ( var column in param.DesignColumns )
        {
            //var colName = CaseHelper.PascalCaseToCamelCase( column.Name );
            //context.WriteLine( $"\t/// {column.Desc}" );
            //context.WriteLine( $"\tFiled( _{colName} )" );
            //context.WriteLine( $"\t{column.Type} _{colName};" );
            //context.NextLine();
        }

        context.WriteLine( "public:" );
        context.Indent();
        context.Comment( $"생성자" );
        context.WriteLine( $"{ param.Design }() = default;" );
        context.WriteLine( $"{ param.Design }({ param.DesignCtorParamsWithType } )");
        context.WriteLine( $": {param.DesignTemplate}{{ {param.DesignCtorArgs} }}{{}}" );
        context.NextLine();
        context.UnIndent();
    }

    public void _GenField( CodeGenContext context, DesignGenParam param )
    {
        // context.WriteLine( "public:" );
        // context.Indent();
        // context.UnIndent();
    }

    public void _GenImportCode( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"export module { param.Design };" );
        context.NextLine();
        context.WriteLine( $"import <Macro.h>;" );
        context.NextLine();
        context.WriteLine( $"import Mala.Core.Types;" );
        context.WriteLine( $"import Mala.Container;"  );
        context.WriteLine( $"import Mala.Reflection;" );
        context.NextLine();
        context.WriteLine( $"import DesignTypes;" );
        context.WriteLine( $"import IDesign;" );
        context.WriteLine( $"import { param.DesignTemplate };" );
        context.NextLine();
    }

}
