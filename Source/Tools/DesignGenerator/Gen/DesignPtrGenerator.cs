﻿using CodeGen;
using CodeGenerator;

/// <summary>
/// 디자인 오브젝트 제네레이터
/// </summary>
public class DesignPtrGenerator
{
    /// <summary>
    /// 
    /// </summary>
    public CodeGenContext Generate( DesignGenParam param )
    {
        var context = new CodeGenContext();

        _GenImportCode( context, param );
        _GenClassHeader( param, context );
        _GenMethod( context, param );
        _GenField( context, param );
        _GenClassFooter( context );

        return context;
    }

    private static void _GenClassFooter( CodeGenContext context )
    {
        context.WriteLine(
        $"}};" );
        context.NextLine();
        context.WriteLine( $"EXPORT_END" );
    }

    private static void _GenClassHeader( DesignGenParam param, CodeGenContext context )
    {
        context.NextLine();
        context.WriteLine( $"EXPORT_BEGIN" );
        context.NextLine();
        context.WriteLine( $"class { param.Design };" );
        context.NextLine();
        context.WriteLine( $"class { param.DesignPtr }\n" +
        $"{{" );
        //context.WriteLine( $"\tGENERATE_CLASS_TYPE_INFO( { param.SheetName }DesignPtr );" );
        context.NextLine();
    }

    private void _GenMethod( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"public:" );

        context.Indent();
        context.Comment( $"{ param.DesignPtr } 생성자" );
        
        bool hasKey = false;
        foreach ( var column in param.DesignColumns )
        {
            if ( !column.IsIndexColumn )
                continue;

            hasKey = true;
            
            context.WriteLine( $"{ param.DesignPtr }( { column.Type } { CaseHelper.PascalCaseToCamelCase( column.Name ) } )" );
            context.WriteLine( $": _ptr{{ { param.GDesignManager }->Get{ param.Design }( { CaseHelper.PascalCaseToCamelCase( column.Name ) } ) }}" );
            context.WriteLine( $"{{}}" );
        }
        
        context.NextLine();

        context.WriteLine( $"const { param.Design }* operator->() const {{ return _ptr; }}" );
        context.NextLine();
        context.WriteLine( $"operator bool() const {{ return _ptr; }}" );
        context.NextLine();
        context.UnIndent();
        context.NextLine();
    }

    public void _GenField( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"\tconst { param.Design }* _ptr{{}};" );
        context.NextLine();
    }

    public void _GenImportCode( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"export module { param.DesignPtr };" );
        context.NextLine();
        context.WriteLine( $"import <Macro.h>;" );
        context.NextLine();
        context.WriteLine( $"import Mala.Core.Types;" );
        context.WriteLine( $"import Mala.Container;"  );
        context.WriteLine( $"import Mala.Reflection;" );
        context.NextLine();
        context.WriteLine( $"import DesignTypes;" );
        context.WriteLine( $"import { param.DesignManager };" );
        context.NextLine();
    }

}
