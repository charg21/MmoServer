﻿using CodeGen;
using CodeGenerator;

/// <summary>
/// 디자인 오브젝트 제네레이터
/// </summary>
public class DesignGroupPtrGenerator
{
    /// <summary>
    /// 
    /// </summary>
    public CodeGenContext Generate( DesignGenParam param )
    {
        var context = new CodeGenContext();

        _GenImportCode( context, param );
        context.NextLine();
        context.WriteLine( $"EXPORT_BEGIN" );
        context.NextLine();
        context.WriteLine( $"class { param.DesignGroupPtr }\n" +
        $"{{" );
        context.WriteLine( $"\tGENERATE_CLASS_TYPE_INFO( { param.DesignGroupPtr } );" );
        context.NextLine();

        _GenMethod( context, param );
        _GenField( context, param );

        context.WriteLine(
        $"}};" );
        context.NextLine();
        context.WriteLine( $"EXPORT_END" );

        return context;
    }

    private void _GenMethod( CodeGenContext context, DesignGenParam param )
    {
        context.Indent();
        context.Comment( $"생성자" );
        context.WriteLine( $"{ param.DesignGroupPtr }( { param.SheetName }DesignId id )" );
        context.WriteLine( $": _ptrs{{ { param.GDesignManager }->Get{ param.DesignGroup }( id ) }}" );
        context.WriteLine( $"{{}}" );
        context.NextLine();

        context.Comment( $"" );
        context.WriteLine( $"auto begin() const {{ return _ptrs.begin(); }}" );
        context.WriteLine( $"auto end() const {{ return _ptrs.end(); }}" );
        context.NextLine();

        context.Comment( $"" );
        context.WriteLine( $"auto empty() const {{ return _ptrs.empty(); }}" );
        context.NextLine();
        context.UnIndent();
    }

    public void _GenField( CodeGenContext context, DesignGenParam param )
    {
        //context.WriteLine( $"\tFiled( _ptrs )" );
        context.Indent();
        context.Comment( $"기획 데이터 포인터 목록" );
        context.WriteLine( $"const { param.DesignManager }::{ param.DesignGroup }& _ptrs;" );
        context.UnIndent();
        context.NextLine();
    }

    public void _GenImportCode( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"export module { param.DesignGroupPtr };" );
        context.NextLine();
        context.WriteLine( $"import <Macro.h>;" );
        context.NextLine();
        context.WriteLine( $"import Mala.Core.Types;" );
        context.WriteLine( $"import Mala.Container;"  );
        context.WriteLine( $"import Mala.Reflection;" );
        context.NextLine();
        context.WriteLine( $"import DesignTypes;" );
        context.WriteLine( $"import { param.DesignManager };" );
        context.NextLine();
    }

}
