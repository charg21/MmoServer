using CodeGen;

/// <summary>
/// 디자인 오브젝트 제네레이터
/// </summary>
public class DesignManagerGenerator
{
    /// <summary>
    ///
    /// </summary>
    public CodeGenContext Generate( DesignGenParam param )
    {
        var context = new CodeGenContext();

        _GenImportCode( context, param );
        _GenClassHeader( param, context );
        _GenClassFooter( context );
        _GenGlobalInstance( context, param );
        context.WriteLine( $"EXPORT_END" );

        return context;
    }

    private static void _GenClassFooter( CodeGenContext context )
    {
        context.WriteLine( $"}};" );
        context.NextLine();
    }

    private static void _GenClassHeader( DesignGenParam param, CodeGenContext context )
    {
        context.WriteLine( $"EXPORT_BEGIN" );
        context.NextLine();

        context.WriteLine( $"class { param.DesignManager } final : public { param.DesignManagerTemplate }\n" +
        $"{{" );
        context.Indent();
        context.WriteLine( $"GENERATE_CLASS_TYPE_INFO( { param.DesignManager } );" );
        context.UnIndent();
        context.NextLine();
    }

    private void _GenMethod( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"public:" );
        context.Indent();

        context.Comment( "데이터를 로드한다" );
        context.WriteLine( $"bool Load( StringRef path ) final;" );
        context.NextLine();

        context.Comment( "초기화 한다" );
        context.WriteLine( $"bool Initialize() final;" );
        context.NextLine();

        context.Comment( "기획 데이터를 획득한다" );
        context.WriteLine( $"const { param.Design }* Get{ param.Design }() const;" );
        context.NextLine();

        context.UnIndent();
        context.NextLine();
    }

    public void _GenField( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"public:" );
        context.Indent();

        context.Comment( "컨테이너" );
        context.WriteLine( $"{ param.Design }Map _infos;" );
        context.NextLine();

        context.UnIndent();
        context.NextLine();
    }

    public void _GenImportCode( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"export module { param.DesignManager };" );
        context.NextLine();
        context.WriteLine( $"import <Macro.h>;" );
        context.NextLine();
        context.WriteLine( $"import Mala.Core.Types;" );
        context.WriteLine( $"import Mala.Container;"  );
        context.WriteLine( $"import Mala.Reflection;" );
        context.NextLine();
        context.WriteLine( $"import DesignTypes;" );
        context.WriteLine( $"import IDesignManager;" );
        context.WriteLine( $"import IDesign;" );
        context.NextLine();
        context.WriteLine( $"import { param.DesignManagerTemplate };" );
        context.NextLine();
    }

    public void _GenGlobalInstance( CodeGenContext context, DesignGenParam param )
    {
        context.WriteLine( $"extern inline { param.DesignManager }* { param.GDesignManager }{{}};" );
        context.NextLine();
    }

}
