﻿
using CodeGenerator;

/// <summary>
/// 디자인 오브젝트
/// </summary>
public partial class DesignGenParam
{
    /// <summary>
    /// 이름
    /// </summary>
    public string Name = string.Empty;

    /// <summary>
    /// 시트명
    /// </summary>
    public string SheetName = string.Empty;

    /// <summary>
    /// 시트명
    /// </summary>
    public string SheetNameCamel => CaseHelper.PascalToCamel( SheetName );

    /// <summary>
    /// 컬럼
    /// </summary>
    public List< DesignColumn > DesignColumns = new();

    /// <summary>
    /// 프라이머리 인덱스
    /// </summary>
    public PrimaryIndex PrimaryIndex = new();

    /// 세컨더리 인덱스 목록

    /// <summary>
    /// 그룹 인덱스
    /// </summary>
    public GroupIndex GroupIndex = new();

    public bool IsIndexColumn = false;
    public bool HasGroupIndexColumn = false;
    public bool HasIndex => IsIndexColumn || HasGroupIndexColumn;

    internal void AddGroup( DesignColumn col )
    {
        GroupIndex.GroupIndexColumn.Add( col.Name );
    }
}



/// <summary>
/// 현재는
/// </summary>
public class DesignColumn
{
    public string Name { get; set; } = string.Empty;
    public string Type { get; set; } = string.Empty;
    public string Desc { get; set; } = string.Empty;
    public string Use { get; set; } = string.Empty;

    public bool IsIndexColumn = false;
    public bool IsGroupIndexColumn = false;
    public bool HasIndex => IsIndexColumn || IsGroupIndexColumn;

    public string ToCppParamType()
    {
        return Type switch
        {
            "string" => "const String&",
            "String" => "const String&",

            _ => Type
        };
    }

    public string ToCppType()
    {
        return Type switch
        {
            "string" => "String",

            _ => Type
        };
    }

    public string ToRapidJsonGetter()
    {
        string type = Type;
        if ( Config.CustomTypeDict.TryGetValue( Type, out var value ) )
            type = value.BaseType;

        return type switch
        {
            "String" => "GetString()",
            "string" => "GetString()",
            "int"    => "GetInt()",
            "uint"   => "GetUint()",
            "long"   => "GetInt64()",
            "ulong"  => "GetUint64()",
            "float"  => "GetFloat()",
            "f32"    => "GetFloat()",
            "double" => "GetDouble()",
            "bool"   => "GetBool()",

            _ => "GetInt()"
        };
    }

    public string ToRapidJsonGetterW()
    {
        return Type switch
        {
            "String" => "ToUtf16( GetString() )",
            "string" => "ToUtf16( GetString() )",

            _ => ToRapidJsonGetter()
        };
    }
}

public class PrimaryIndex
{
    public List< string > IndexColumn = new();
}

public class GroupIndex
{
    public List< string > GroupIndexColumn = new();
}

/// <summary>
/// 디자인 오브젝트
/// </summary>
public partial class DesignGenParam
{
    /// <summary>
    /// 디자인 매니저
    /// </summary>
    private string DesignManagerString = string.Empty;
    public string DesignManager
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignManagerString ) )
                DesignManagerString = $"{ SheetName }DesignManager";

            return DesignManagerString;
        }
    }

    /// <summary>
    /// 디자인 매니저
    /// </summary>
    private string DesignManagerTemplateString = string.Empty;
    public string DesignManagerTemplate
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignManagerTemplateString ) )
                DesignManagerTemplateString = $"{ SheetName }DesignManagerTemplate";

            return DesignManagerTemplateString;
        }
    }

    /// <summary>
    /// 디자인 템플릿
    /// </summary>
    private string DesignTemplateString = string.Empty;
    public string DesignTemplate
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignTemplateString ) )
                DesignTemplateString = $"{ SheetName }DesignTemplate";

            return DesignTemplateString;
        }
    }

    /// <summary>
    /// 글로벌 디자인 매니저
    /// </summary>
    private string GDesignManagerString = string.Empty;
    public string GDesignManager
    {
        get
        {
            if ( string.IsNullOrEmpty( GDesignManagerString ) )
                GDesignManagerString = $"G{ SheetName }DesignManager";

            return GDesignManagerString;
        }
    }

    /// <summary>
    /// 디자인
    /// </summary>
    private string DesignString = string.Empty;
    public string Design
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignString ) )
                DesignString = $"{ SheetName }Design";

            return DesignString;
        }
    }

    /// <summary>
    /// 디자인 그룹
    /// </summary>
    private string DesignGroupString = string.Empty;
    public string DesignGroup
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignGroupString ) )
                DesignGroupString = $"{ SheetName }DesignGroup";

            return DesignGroupString;
        }
    }

    /// <summary>
    /// 디자인 포인터
    /// </summary>
    private string DesignPtrString = string.Empty;
    public string DesignPtr
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignPtrString ) )
                DesignPtrString = $"{ SheetName }DesignPtr";

            return DesignPtrString;
        }
    }

    /// <summary>
    /// 디자인 그룹 포인터
    /// </summary>
    private string DesignGroupPtrString = string.Empty;
    public string DesignGroupPtr
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignGroupPtrString ) )
                DesignGroupPtrString = $"{ SheetName }DesignGroupPtr";

            return DesignGroupPtrString;
        }
    }

    /// <summary>
    /// 디자인 그룹 포인터
    /// </summary>
    private string DesignCtorParamsWithTypeString = string.Empty;
    public string DesignCtorParamsWithType
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignCtorParamsWithTypeString ) )
            {
                foreach ( var column in DesignColumns )
                {
                    var colName = CaseHelper.PascalCaseToCamelCase( column.Name );
                    DesignCtorParamsWithTypeString += $" { column.ToCppParamType() } { colName },";
                }

                DesignCtorParamsWithTypeString = DesignCtorParamsWithTypeString.Substring( 0, DesignCtorParamsWithTypeString.Length - 1 );
            }

            return DesignCtorParamsWithTypeString;
        }
    }

    /// <summary>
    /// 디자인 그룹 포인터
    /// </summary>
    private string DesignCtorParamsString = string.Empty;
    public string DesignCtorArgs
    {
        get
        {
            if ( string.IsNullOrEmpty( DesignCtorParamsString ) )
            {
                foreach ( var column in DesignColumns )
                {
                    var colName = CaseHelper.PascalCaseToCamelCase( column.Name );
                    DesignCtorParamsString += $" { colName },";
                }

                DesignCtorParamsString = DesignCtorParamsString.Substring( 0, DesignCtorParamsString.Length - 1 );
            }

            return DesignCtorParamsString;
        }
    }

    /// <summary>
    /// 디자인 그룹 포인터
    /// </summary>
    private string DeserializeParamsString = string.Empty;
    public string DeserializeParams
    {
        get
        {
            if ( string.IsNullOrEmpty( DeserializeParamsString ) )
            {
                foreach ( var column in DesignColumns )
                {
                    var str = $"_{ SheetNameCamel }[ \"{ column.Name }\" ].{ column.ToRapidJsonGetter() }";
                    DeserializeParamsString += "\t\t\t";

                    if ( column.Type == "String" || column.Type == "string" )
                    {
                        DeserializeParamsString += $" ToUtf16( {str} ),\n";
                    }
                    else if ( Config.CustomTypeDict.ContainsKey( column.Type ) )
                    {
                        DeserializeParamsString += $"{ column.Type }{{ {str} }},\n";
                    }
                    else
                    {
                        DeserializeParamsString += $"{str},\n";
                    }
                }

                DeserializeParamsString = DeserializeParamsString.Substring( 0, DeserializeParamsString.Length - 2 );
            }

            return DeserializeParamsString;
        }
    }

}

public class DesignObject
{
    public string Name = string.Empty;
    public List< DesignGenParam > Params;
}

public static class Const
{
    public const int SchemaRowCount = 0_3;
    public const int SchemaName  = 0;
    public const int SchemaType  = 1;
    public const int SchemaDesc  = 2;
    public const int SchemaIndex = 3;
}


public enum EUse
{
    None = 0,

    /// <summary>
    /// 서버
    /// </summary>
    S = 1 << 0,
    /// <summary>
    /// 클라
    /// </summary>
    C = 1 << 1,
    /// <summary>
    /// ( Reserved... )
    /// </summary>
    T = 1 << 2,

    PrimaryIndex = 1 << 8,
    GroupIndex   = 1 << 9,

    SC = S | C,
    CS = SC,
    ST = S | T,
    CT = C | T,
}