﻿using Mala.Collection;
using Mala.Db;
using Mala.Threading;
using System.Net;
using System.Runtime;

/// <summary>
/// 월드 서버
/// </summary>
public class WorldServer2 : NativeServerService
{
    public WorldServer2( IPEndPoint endPoint, SessionFactory factory, int capacity )
    : base( endPoint, factory, capacity )
    {
    }

    protected override bool OnPreInit()
    {
        Actor.Initialize( Config.WorkerThreadCount );
        SetThreadCount( Config.WorkerThreadCount );
        GC.TryStartNoGCRegion( 4L * Consts.GB );
        return true;
    }

    protected override bool OnPostInit()
    {
        List< Task > tasks =
        [
            Task.Run( () =>
            {
                if ( !Config.UseDb )
                    return;

                DbConnectionPool.Instance.Initialize( Config.WorldDbConnectionString, 10 );
                DbSchemaChecker.Initialize();
            } ),

            Task.Run( () =>
            {
                DesignManagerCenter.Load( Config.DesignPath );
                DesignManagerCenter.Initialize();
            } ),

            Task.Run( () =>
            {
                ClientPacketDispatcher.Instance.Initialize();
                CheatManager.Instance.Initialize();
            } ),

            Task.Run( () =>
            {
                if ( !Config.UseWebService )
                    return;

                RestApiService.Global.Initialize();
                RestApiService.Global.Start();
            } ),
        ];

        ThreadManager.Launch( () => WorldExecutorThread.DoWorkerJob( this ), "WorldWorker", Config.WorkerThreadCount );
        ChannelManager.Instance.Initialize();
        Task.WaitAll( tasks.AsSpan() );

        /// 각 월드 워커 스레드에서 월드에 접근하여 강제 초기화 진행
        ChannelManager.Instance.ForEach( channel => channel.World.Initialize() );

        return true;
    }

    /// <summary>
    /// 설정을 불러온다.
    /// </summary>
    public bool LoadConfig( string[] args )
    {
        return true;
    }

    /// <summary>
    /// 시작한다
    /// </summary>
    public override bool OnStart()
    {
        Console.Title = $"WorldServer2 Started... Server GC: { GCSettings.IsServerGC } ";

        // 적당한 버퍼 타임을 준다.
        Thread.Sleep( 5000 );
        ChannelManager.Instance.ForEach( channel => channel.World.SpawnDummyNpc() );

        for ( ;; )
        {
            Thread.Sleep( 30 * 1000 );
            ServerMonitor.Report( this );
        }
    }
}