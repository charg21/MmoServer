﻿using Mala.Core;
using Mala.Db;
using Mala.Logging;
using Mala.Net;
using Mala.Threading;
using System.Net;
using System.Runtime;

/// <summary>
/// 월드 서버
/// </summary>
public class WorldServer : Service
{
    /// <summary>
    ///
    /// </summary>
    public Listener ClientListener = new();

    /// <summary>
    ///
    /// </summary>
    public Listener ServerListener = new();

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public override bool Initialize()
    {
        GC.TryStartNoGCRegion( 4L * Consts.GB );

        ExceptionHandler.Initialize();
        LogManager.Initialize( Name );

        if ( Config.UseDb )
        {
            DbConnectionPool.Instance.Initialize( Config.WorldDbConnectionString, 10 );
            DbSchemaChecker.Initialize();
        }

        DesignManagerCenter.Initialize();
        ChannelManager.Instance.Initialize();
        CheatManager.Instance.Initialize();

        InitNetowrk();
        InitThreads();

        return true;
    }

    /// <summary>
    /// 스레드를 초기화를 한다.
    /// </summary>
    private void InitThreads()
    {
        var _ = ThreadPoolExecutorService.SharedContext;
        GlobalQueue._threadPoolCount = Config.WorkerThreadCount;

        ThreadManager.Launch(
            () => WorldExecutorThread.DoWorkerJob( null ),
            "Worker",
            Config.WorkerThreadCount );

#if NATIVE
        ThreadPool.SetMinThreads( 16, 1 );
        ThreadPool.SetMaxThreads( 16, 1 );
#endif
    }

    /// <summary>
    /// 네트워크 초기화를 한다.
    /// </summary>
    private void InitNetowrk()
    {
        ClientPacketDispatcher.Instance.Initialize();

        var ipAddress = IPAddress.Parse( Config.ClientListenerIp );
        var endPoint  = new IPEndPoint( ipAddress, Config.ClientListenerPort );

#if !NATIVE
        ClientListener.Initialize(
            endPoint,
            ClientSessionManager.Generate,
            Config.ClientSessionPrepareCount );
#endif
        //ServerListener.Initialize(
        //    endPoint,
        //    ServerSessionManager.Generate,
        //    Config.ServerSessionPrepareCount );
    }

    /// <summary>
    /// 설정을 불러온다.
    /// </summary>
    public override bool LoadConfig( string[] args )
    {
        Config.Load( args );
        // DesignManagerCenter.Load( Config.DesignPath );

        return true;
    }

    /// <summary>
    /// 시작한다
    /// </summary>
    public override void Start()
    {
        Console.Title = $"{ Name } Started... Server GC: { GCSettings.IsServerGC } ";

        // 적당한 버퍼 타임을 준다.
        Thread.Sleep( 5000 );

        ChannelManager.Instance.ForEach( ch => ch.World.SpawnDummyNpc() );

        var asMB = ( long byteSize ) => { return byteSize / ( 1024 * 1024 ); };
        for ( ;; )
        {
            // WorkerThread.DoWorkerJob2( this );
            Thread.Sleep( 30 * 1000 );

            var gcMemoryInfo = GC.GetGCMemoryInfo();
            var totalMemroy = GC.GetTotalMemory( false );

            Log.Info(
                $"GC Gen0: { GC.CollectionCount( 0 ) } Gen1: { GC.CollectionCount( 1 ) } Gen2: { GC.CollectionCount( 2 ) }, \n" +
                $"PinnedObj: { gcMemoryInfo.PinnedObjectsCount } \n" +
                $"Memory Total { asMB( totalMemroy ) }MB Promoted: { asMB( gcMemoryInfo.PromotedBytes ) } MB, " +
                $"Heap: { asMB( gcMemoryInfo.HeapSizeBytes ) } MB \n" +
                $"CLR.ThreadPool ThreadCount: { ThreadPool.ThreadCount } WorldServer ThreadCount: { Config.WorkerThreadCount } \n" +
                $"SendBuffer New[ { SendBuffer.NewCount } ] Rent[ { SendBuffer.RentCount } ] Return[ { SendBuffer.ReturnCount } ]\n" );
        }
    }
}
