﻿[CheatCommand( command:"despawn", desc:"액터를 디스폰한다" ) ]
public class DespawnCheat : Cheat
{
    public override void Handle( ClientSession session, string commands )
    {
        var pc = session?.Pc;
        if ( pc != null )
        {
            session?.MergeSend( new ValueS_Cheat() { _result = ELogicResult.NullPc }.Write() );
            return;
        }

        pc!.Post( () =>
        {
            // TODO 꾸현해야함

            pc.Send( new ValueS_Cheat() { _result = ELogicResult.Success }.Write() );
        } );
    }
}