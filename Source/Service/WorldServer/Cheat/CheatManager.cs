﻿using Mala.Core;
using Mala.Db;
using Mala.Net;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Reflection;

/// <summary>
/// 치트 관리자
/// </summary>
public class CheatManager : Singleton< CheatManager >
{
    Dictionary< string, Action< ClientSession, string > > _cheatHandlers = new();

    public void Initialize()
    {
        var baseCheatType = typeof( Cheat );
        var classTypes = Assembly.GetExecutingAssembly().GetTypes()
            .Where( t => t.IsClass && baseCheatType.IsAssignableFrom( t ) && t != baseCheatType );

        foreach ( var type in classTypes )
        {
            AddCommand( type );
        }
    }

    internal void AddCommand( Type cheatType )
    {
        var cheatCmd = cheatType.GetCustomAttribute< CheatCommand >();

        var cheat = Activator.CreateInstance( cheatType ) as Cheat;
        cheat.Command = cheatCmd.Command;

        _cheatHandlers.Add( cheat.Command, cheat.Handle );
    }

    internal void Handle( ClientSession session, string command )
    {
        if ( _cheatHandlers.TryGetValue( command, out var handler ) )
            handler.Invoke( session, command );
    }
}
