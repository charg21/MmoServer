﻿[CheatCommand( command:"spawn", desc:"액터를 스폰한다" ) ]
public class SpawnCheat : Cheat
{
    public override void Handle( ClientSession session, string commands )
    {
        var pc = session?.Pc;
        if ( pc != null )
        {
            session?.MergeSend( new ValueS_Cheat() { _result = ELogicResult.NullPc }.Write() );
            return;
        }

        pc!.Post( () =>
        {
            // 좌표 파싱

            // 액터 스폰시 호출될 콜백 필요

            pc.World.Spawn< Npc >(
                new( IdHelper.Next() ),
                new Position(),
                new Direction() );

            pc.Send( new ValueS_Cheat() { _result = ELogicResult.Success }.Write() );
            pc.Send( new ValueS_Message() { _message = $"View Pc: {pc.View.Count}" }.Write() );
        } );
    }
}