﻿[CheatCommand( command:"pos", desc:"현재 좌표를 표시한다" ) ]
public class PosCheat : Cheat
{
    public override void Handle( ClientSession session, string commands )
    {
        var pc = session?.Pc;
        if ( pc != null )
        {
            session?.MergeSend( new ValueS_Cheat() { _result = ELogicResult.NullPc }.Write() );
            return;
        }

        pc.Post( () =>
        {
            pc.Send( new ValueS_Cheat() { _result = ELogicResult.Success }.Write() );
            pc.Send( new ValueS_Message() { _message = $"MyPos[ { pc.Movement.Pos } ]" }.Write() );
        } );
    }
}