﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// 치트 커맨드
/// </summary>
public class CheatCommand : System.Attribute
{
    /// <summary>
    /// 커맨드
    /// </summary>
    public string Command { get; set; } = string.Empty;

    /// <summary>
    /// 설명
    /// </summary>
    public string Description { get; set; } = string.Empty;

    /// <summary>
    /// 생성자
    /// </summary>
    public CheatCommand( string command, string desc )
    {
        Command = command;
        Description = desc;
    }
}
