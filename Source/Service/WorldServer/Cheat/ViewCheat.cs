﻿[CheatCommand( command:"view", desc:"주변 시야의 액터 수를 표시한다" ) ]
public class ViewCheat : Cheat
{
    public override void Handle( ClientSession session, string commands )
    {
        var pc = session?.Pc;
        if ( pc != null )
        {
            session?.MergeSend( new ValueS_Cheat() { _result = ELogicResult.NullPc }.Write() );
            return;
        }

        pc!.Post( () =>
        {
            pc.Send( new ValueS_Cheat() { _result = ELogicResult.Success }.Write() );
            pc.Send( new ValueS_Message() { _message = $"View Pc: {pc.View.Count}" }.Write() );
        } );
    }
}