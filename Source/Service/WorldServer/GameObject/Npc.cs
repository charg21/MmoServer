﻿using Mala.Logging;
using Mala.Math;
using Mala.Threading;


/// <summary>
/// NPC 수면 상태 여부
/// </summary>
public enum EWakeUpState
{
    Sleep,
    Awaken
};

/// <summary>
/// 논 플레이어블 캐릭터
/// </summary>
public partial class Npc : WorldObject
{
    /// <summary>
    /// NPC AI
    /// </summary>
    public NpcFsmAi Ai { get; private set; }

    /// <summary>
    /// NPC 활성화 상태
    /// </summary>
    public EWakeUpState WakeUpState { get; set; } = EWakeUpState.Sleep;

    /// <summary>
    /// 복귀 위치
    /// </summary>
    public Vector3 ReturnPos { get; set; } = new();
    public Pc? Target { get; internal set; }

    /// <summary>
    /// 기획 정보
    /// </summary>
    public Design< NpcDesign > Design { get; internal set; }
    public DesignId DesignId => Design.Ref.Id;

    /// <summary>
    /// 어그로 컴포넌트
    /// </summary>
    public AggroComponent Aggro { get; internal set; }

    /// <summary>
    /// 생성자
    /// </summary>
    public Npc() : base( EWorldObjectType.Npc )
    {
        Ai = new NpcFsmAi( this );
        Combat = new NpcCombatComponent( this );
        View = new NpcViewComponent( this );
        Stat = new NpcStatComponent( this );
        Movement = new NpcMovementComponent( this );
        Aggro = new AggroComponent( this );

        Stat.Init();
    }

    public void RefreshStat()
    {
        Stat.Refresh();
    }

    /// <summary>
    /// 틱마다 호출되는 콜백 함수
    /// </summary>
    public override void OnTick( TimeSpan deltaTme )
    {
        if ( deltaTme.Ticks <= 0 )
        {
            // 음수가 나오는 경우가 있음? 다음 프레임으로 업데이트를 미룬다
            PostAfter( Consts.Npc.TickInterval, TickJob.DoTick, this );
            return;
        }

        // 시야에 아무도 없거나, 복귀 상태가 아닌경우 체크
        if ( View!.Empty && !Ai.IsBusy )
        {
            WakeUpState = EWakeUpState.Sleep;
            return;
        }

        WakeUpState = EWakeUpState.Awaken;

        Ai.Tick( deltaTme );

        PostAfter( Consts.Npc.TickInterval, TickJob.DoTick, this );
    }


    public bool IsSleep() => WakeUpState == EWakeUpState.Sleep;
    public bool IsAwaken() => WakeUpState == EWakeUpState.Awaken;
}

public partial class Npc
{
    public override void OnEnterWorld()
    {
        LastTick = ThreadPoolExecutorService.SharedContext.LastTickCount;
        Movement.ToPos = Movement.Pos;
        if ( ReturnPos.SizeSquared() == 0 )
            ReturnPos = Movement.Pos;

        if ( IsAwaken() )
            return;

        RegisterTick( Consts.Npc.TickInterval );
    }

    /// <summary>
    /// 이벤트 드리븐 기반 인터페이스
    /// </summary>
    public void OnRecvEvent( int @event )
    {
    }
}
