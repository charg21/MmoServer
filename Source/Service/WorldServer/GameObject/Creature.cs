﻿using Mala.Threading;


/// <summary>
/// 액터
/// </summary>
public class Creature : Actor
{
    Action TickJob;

    public Creature()
    {
        TickJob = Tick;
    }

    public ObjectId Id = new( 0 );

    /// <summary>
    /// 마지막 갱신 틱
    /// </summary>
    public long LastTick { get; set; } = 0;
    public bool _tickRegistered = false;

    public override void Tick()
    {
        var curTick = Environment.TickCount64;
        long deltaTick = curTick - LastTick;

        _tickRegistered = false;

        OnTick( new( deltaTick ) );

        LastTick = curTick;
    }

    /// <summary>
    /// 틱을 등록한다.
    /// </summary>
    public void RegisterTick( TimeSpan afterTime )
    {
        if ( _tickRegistered )
            return;

        PostAfter( afterTime, TickJob );

        _tickRegistered = true;
    }

    public virtual void OnTick( TimeSpan deltaTime )
    {
    }

    public TimeSpan DeltaTime
        => new( ThreadPoolExecutorService._tlsContext.LastTickCount - LastTick );

    /// <summary>
    /// 잡큐를 비워내는 이벤트시 실행되는 콜백
    /// 한번의 Flush()중 여러번 실행될 수 있음
    /// </summary>
    protected override void OnFlushJob()
    {
        LastTick = Math.Max( ThreadPoolExecutorService._tlsContext.LastTickCount, LastTick );
    }
}

