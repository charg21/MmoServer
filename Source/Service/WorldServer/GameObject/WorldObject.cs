/// <summary>
/// 월드상에 배치되는 게임 오브젝트
/// </summary>
public partial class WorldObject : ComponentObject
{
    public EWorldObjectType Type { get; set; } = EWorldObjectType.Max;
    public World World { get; set; }
    public ClusterManager ClusterManager => World.ClusterManager;
    public Index _wideClusterIndex { get; set; } = new();
    public Index _clusterIndex { get; set; } = new();
    public ClusterLayer ClusterLayer => ClusterManager.ClusterLayer;
    public WideClusterLayer WideClusterLayer => ClusterManager.WideClusterLayer;
    public ref WideCluster CurWideCluster => ref WideClusterLayer.GetCluster( _wideClusterIndex );
    //public ref Cluster CurCluster => ref ClusterLayer.GetCluster( _wideClusterIndex );
    public ViewComponent? View { get; set; }
    public MovementComponent? Movement { get; set; }
    public CombatComponent Combat { get; set; }
    public BagComponent? Bag { get; set; }
    public ChatComponent? Chat { get; set; }
    public IAiComponent? Ai { get; set; }
    public IEquipmentComponent? Equip { get; set; }
    public StatComponent? Stat { get; set; }
    public SkillListComponent? SkillList { get; set; }
    public ISenderComponent? Sender { get; set; }
    public CollisionGridOwnerComponent? CollisionGridOwner { get; set; }

    public int ChannelId => World.ChannelId;

    public string Name = string.Empty;

    public WorldObject( EWorldObjectType objectType )
    {
        Type = objectType;
    }

    public void EnterWorld( World world )
    {
        World = world;
        Movement?.OnEnterWorld();
        ClusterManager.OnEnterWorld( this );

        OnEnterWorld();
        View?.OnEnterWorld();
    }

    public void LeaveWorld()
    {
        OnLeaveWorld();
        ClusterManager.OnLeaveWorld( this );
        //WideClusterLayer.Leave( this );

        View?.OnLeaveWorld();
    }

    public void SetPos( Position toPos, Direction dir )
    {
        Movement?.SetPos( toPos, dir );
        View?.Rebuild();
        Combat?.OnTick( DeltaTime );

        ClusterManager.OnMove( Movement.FromPos, Movement.Pos, this );
    }

    public void SetPosOnly( Position pos )
    {
        Movement.Pos = pos;
    }

    /// <summary>
    /// 목표 좌표를 설정한다.
    /// </summary>
    public void SetDestPos( Position destPos )
    {
        Movement.ToPos = destPos;
    }

    public virtual void OnEnterWorld()
    {}

    public virtual void OnLeaveWorld()
    {}

}
