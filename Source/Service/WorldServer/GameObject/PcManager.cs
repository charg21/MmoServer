﻿using Mala.Threading;
using System.Collections.Concurrent;

/// <summary>
/// 플레이어 관리자
/// </summary>
public partial class PcManager
{
    /// <summary>
    /// TLS별 PC 컬렉션
    /// </summary>
    static ThreadLocal< Dictionary< ObjectId, Pc > > _sharedInstance = new( () => new() );
    public static Dictionary< ObjectId, Pc > Shared => _sharedInstance.Value!;

    /// <summary>
    /// 글로벌 플레이어 컬렉션
    /// </summary>
    public static ConcurrentDictionary< ObjectId, Pc > Global = new();

    public static bool TryAdd( Pc pc )
    {
        if ( pc.Id == 0 )
            return false;

        if ( !Global.TryAdd( pc.Id, pc ) )
            return false;

        Actor.Global.Post(
            () => PcManager.Shared.Add( pc.Id, pc ) );

        return true;
    }

    public static bool TryGet( ObjectId actorId, out Pc pc )
    {
        if ( PcManager.Shared.TryGetValue( actorId, out pc ) )
            return true;

        if ( PcManager.Global.TryGetValue( actorId, out pc ) )
            return true;

        return false;
    }

    public static bool TryRemove( ObjectId actorId )
    {
        if ( !PcManager.Global.TryRemove( actorId, out var _ ) )
            return false;

        Actor.Global.Post(
            () => PcManager.Shared.Remove( actorId ) );

        return true;
    }
}


