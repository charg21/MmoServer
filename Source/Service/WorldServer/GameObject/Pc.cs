﻿using Mala.Db;
using Mala.Db.Model;
using Mala.Math;
using Mala.Net;
using Mala.Threading;


/// <summary>
/// 플레이어블 캐릭터
/// </summary>
public partial class Pc : WorldObject
{
    /// <summary>
    /// 생성자
    /// </summary>
    public Pc()
    : base( EWorldObjectType.Pc )
    {
        Bag = new BagComponent( this );
        Combat = new PcCombatComponent( this );
        Chat = new ChatComponent( this );
        View = new PcViewComponent( this );
        Equip = new PcEquipComponent( this );
        Stat = new PcStatComponent( this );
        SkillList = new PcSkillListComponent( this );
        TxExecutor = new TxExecutor( this );
        Movement = new PcMovementComponent( this );
        Sender = new PcSenderComponent( this );

        if ( Config.UseCollisionGrid )
            CollisionGridOwner = new CollisionGridOwnerComponent();

        Stat.Init();
        SkillList.Init();
    }

    /// <summary>
    /// 월드 입장시 호출되는 함수
    /// </summary>
    public override void OnEnterWorld()
    {
        Send( PacketWriter.FromS_EnterWorld( this ) );
        RegisterTick( new( 100 ) );
    }

    /// <summary>
    /// 틱마다 호출되는 콜백 함수
    /// </summary>
    public override void OnTick( TimeSpan deltaTick )
    {
        if ( Session is null )
            return;

        /// 일단 폴링을 통해 처리( 디버깅 ), 잡 처리 인터페이스 있음
        TxExecutor.FlushTx();
        PostAfter( Consts.Actor.TickInterval, TickJob.DoTick, this );
    }

    /// <summary>
    /// 월드 퇴장시 호출되는 함수
    /// </summary>
    public override void OnLeaveWorld()
    {
        Session?.Send( PacketWriter.FromS_LeaveWorld( this ) );
        Session = null;
    }

    /// <summary>
    /// 클라이언트 세션
    /// </summary>
    public ClientSession? Session { get; internal set; }

    public void Send( ref SendSegment segment )
        => Session?.Send( ref segment );

    /// <summary>
    /// 패킷을 전송한다.
    /// </summary>
    public void Send( SendSegment segment )
        => Session?.Send( ref segment );
}

/// <summary>
/// ITxExecutor 구현체
/// </summary>
public partial class Pc : ITxContextOwner, ITxExecutor
{
    /// <summary>
    /// 트랜잭션 실행기
    /// </summary>
    TxExecutor TxExecutor;

    public Actor TxJobExecutor => TxExecutor.TxJobExecutor;

    public void PostTx( TxContext txContext ) => TxExecutor.PostTx( txContext );
    public void PostTx( TxContext txContext, Action< ETxResult > completionJob )
        => TxExecutor.PostTx( txContext, completionJob );

    public void PopTx() => TxExecutor.PopTx();

    /// <summary>
    /// 트랜잭션을 예약한다.
    /// </summary>
    public void ReserveTx() => TxExecutor.ReserveTx();

    /// <summary>
    /// 저장한다.
    /// </summary>
    public void Save( ESaveType saveType = ESaveType.Expired )
    {
        //var txContext = TxContext.New();
        //var planPc    = txContext.Update< Pc >( this );
        //var newId = IdHelper.Generate( JobDispatcher.GetTlsContext().Id );
        //planPc.SetHp( planPc.GetHp() + 1 );
        //DoTxJob( txContext );
        //_nextSaveTick -= deltaTime;
        //if ( _nextSaveTick < 0.0f )
        //{
        //    SavepcJob.Do( this, ESaveType.Max, EContentsType.All );
        //    _nextSaveTick = 5.0f;
        //}
    }

}

/// <summary>
/// ITxExecutor 구현체
/// </summary>
public partial class Pc : IDbModel
{
    /// <summary>
    /// 작업 실행기
    /// </summary>
    public Actor JobExecutor => this;

    /// <summary>
    /// 원본
    /// </summary>
    public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

    /// <summary>
    /// Db 동기화 객체
    /// </summary>
    public PcDbModel dbModel = new();

    /// <summary>
    ///
    /// </summary>
    public DbModel DbModel => dbModel;

    /// <summary>
    /// 모든 필드를 바인딩한다.
    /// </summary>
    public void BindAllField() => dbModel.BindAllField();

    /// <summary>
    /// Id를( 을 ) 반환한다
    /// </summary>
    public UInt64 GetId() => dbModel.GetId();

    /// <summary>
    /// UserId를( 을 ) 반환한다
    /// </summary>
    public UInt64 GetUserId() => dbModel.GetUserId();

    /// <summary>
    /// Name를( 을 ) 반환한다
    /// </summary>
    public String GetName() => dbModel.GetName();

    /// <summary>
    /// Hp를( 을 ) 반환한다
    /// </summary>
    public Int32 GetHp() => dbModel.GetHp();

    /// <summary>
    /// X를( 을 ) 반환한다
    /// </summary>
    public Single GetX() => dbModel.GetX();

    /// <summary>
    /// Y를( 을 ) 반환한다
    /// </summary>
    public Single GetY() => dbModel.GetY();

    /// <summary>
    /// Z를( 을 ) 반환한다
    /// </summary>
    public Single GetZ() => dbModel.GetZ();

    /// <summary>
    /// Id를( 을 ) 설정한다
    /// </summary>
    public void SetId( UInt64 id ) => dbModel.SetId( id );
    public void SetKeyId( UInt64 id ) => dbModel.SetKeyId( id );

    /// <summary>
    /// UserId를( 을 ) 설정한다
    /// </summary>
    public void SetUserId( UInt64 user_id ) => dbModel.SetUserId( user_id );
    public void SetKeyUserId( UInt64 user_id ) => dbModel.SetKeyUserId( user_id );

    /// <summary>
    /// Name를( 을 ) 설정한다
    /// </summary>
    public void SetName( String name ) => dbModel.SetName( name );
    /// <summary>
    /// Hp를( 을 ) 설정한다
    /// </summary>
    public void SetHp( Int32 hp ) => dbModel.SetHp( hp );
    /// <summary>
    /// X를( 을 ) 설정한다
    /// </summary>
    public void SetX( Single x ) => dbModel.SetX( x );
    /// <summary>
    /// Y를( 을 ) 설정한다
    /// </summary>
    public void SetY( Single y ) => dbModel.SetY( y );
    /// <summary>
    /// Z를( 을 ) 설정한다
    /// </summary>
    public void SetZ( Single z ) => dbModel.SetZ( z );

    /// <summary>
    /// 동기화한다.
    /// </summary>
    public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );

    /// <summary>
    ///
    /// </summary>
    public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );

    public void CopyToOnlyKeys( IDbModel model )
    {
        dbModel.CopyToOnlyKeys( model );
    }

    public ref Field GetField( int no ) => ref dbModel.GetField( no );

    /// <summary>
    ///
    /// </summary>
    public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );

    public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
}



///// <summary>
///// pc Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
///// </summary>
//public partial class PcDbModel : DbModel
//{
//    /// <summary>
//    /// 동기화 한다.
//    /// </summary>
//    public override void Sync( EQueryType queryType )
//    {
//        switch ( queryType )
//        {
//            case EQueryType.Update:
//                {
//                    var pc = _origin as pc/*DbModel*/;
//                    //var pc = _origin as pcDbModel;
//                    CopyToOnlyUpdateField( pc.DbModel );
//                    break;
//                }

//            case EQueryType.Insert:
//                {
//                    var pc = _origin as pcDbModel;
//                    CopyTo( pc );
//                    break;
//                }

//            case EQueryType.Delete:
//                break;

//            default:
//                break;
//        }
//    }
//}
