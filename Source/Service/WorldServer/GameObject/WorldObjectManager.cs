﻿using Mala.Core;

public class WorldObjectManager
{
    static ThreadLocal< WorldObjectManager > _localInstance = new( () => new WorldObjectManager() );
    public static WorldObjectManager Local => _localInstance.Value;

    /// <summary>
    /// 오브젝트 컬렉션
    /// </summary>
    public NpcMap _npcMap { get; init; } = new( Consts.World.DefaultNpcMapCapacity );
    public PcMap _pcMap { get; init; } = new( Consts.World.DefaultPcMapCapacity );

    /// <summary>
    /// 제거한다.
    /// </summary>
    public void Unregister( ObjectId objectId, EWorldObjectType objType )
    {
        switch ( objType )
        {
            case EWorldObjectType.Npc:
                Mala.Core.Exception.ThrowIfFailed( _npcMap.Remove( objectId ), "12" );
                break;
            case EWorldObjectType.Pc:
                Mala.Core.Exception.ThrowIfFailed( _pcMap.Remove( objectId ), "23" );
                break;
        }

    }

    /// <summary>
    /// 등록한다
    /// </summary>
    public void Register( WorldObject worldObj )
    {
        switch ( worldObj.Type )
        {
            case EWorldObjectType.Pc:
                _pcMap.Add( worldObj.Id, worldObj as Pc );
                break;
            case EWorldObjectType.Npc:
                _npcMap.Add( worldObj.Id, worldObj as Npc );
                break;
        }
    }

    /// <summary>
    /// Pc를 획득한다.
    /// </summary>
    public Pc GetPc( ObjectId pcId )
    {
        _pcMap.TryGetValue( pcId, out var pc );
        return pc;
    }

    /// <summary>
    /// Npc를 획득한다.
    /// </summary>
    public Npc GetNpc( ObjectId npcId )
    {
        _npcMap.TryGetValue( npcId, out var npc );
        return npc;
    }

    /// <summary>
    /// Npc를 획득한다.
    /// </summary>
    public bool Contains( ObjectId objectId )
    {
        return _npcMap.ContainsKey( objectId ) || _pcMap.ContainsKey( objectId );
    }

    internal bool TryGetNpc( ObjectId npcId, out Npc? npc )
    {
        return _npcMap.TryGetValue( npcId, out npc );
    }

    internal bool TryGetPc( ObjectId npcId, out Pc? pc )
    {
        return _pcMap.TryGetValue( npcId, out pc );
    }
}
