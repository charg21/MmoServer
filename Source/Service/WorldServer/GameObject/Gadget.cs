/// <summary>
/// 월드상 상호작용 하는 오브젝트
/// </summary>
public partial class Gadget : WorldObject
{
    GadgetInteractionComponent Interaction;

    public Gadget() : base( EWorldObjectType.Gadget )
    {
        Interaction = new GadgetInteractionComponent( this );
    }


    public override void OnTick( TimeSpan deltaTme )
    {
        PostAfter( Consts.Npc.TickInterval, TickJob.DoTick, this );
    }

}
