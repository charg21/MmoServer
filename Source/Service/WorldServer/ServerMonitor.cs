﻿using Mala.Core;
using Mala.Db;
using Mala.Logging;
using Mala.Net;
using Mala.Threading;
using System.Diagnostics;



public class ServerMonitor
{
    public static void Report( WorldServer2 server )
    {
        var gcMemoryInfo = GC.GetGCMemoryInfo();
        var totalMemroy  = GC.GetTotalMemory( false );
        var perfLog = CreatePerfLog( server, ref gcMemoryInfo, totalMemroy );

        Log.Info(
            $"GC Gen0: {perfLog.GetGCGen0()}| Gen1: {perfLog.GetGCGen1()}| Gen2: {perfLog.GetGCGen2()}\n" +
            $"PinnedObj: {gcMemoryInfo.PinnedObjectsCount} \n" +
            $"Memory Native: {ToMB( server.GetNativeMemorySize() )}MB" +
            $"Memory Managed: Total {ToMB( totalMemroy )}MB Promoted: {ToMB( gcMemoryInfo.PromotedBytes )} MB, " +
            $"Heap: {ToMB( gcMemoryInfo.HeapSizeBytes )} MB WorkingSet: {ToMB( perfLog.GetWorkingSet() )} MB \n" +
            $"ThreadCount: CLR.ThreadPool: {ThreadPool.ThreadCount} WorldServer: {Config.WorkerThreadCount} \n" +
            $"SeytemCall/sec: {perfLog.GetSystemCall()} Context Switching/sec: {perfLog.GetContextSwitching()} \n" +
            $"RecvBytes: {ToMB( perfLog.GetSystemRecvBytes() )} , SentBytes: {ToMB( perfLog.GetSystemSentBytes() )} \n" +
            $"Job: GloblaJobCount: {Actor.Global.GetJobCount()} \n" +
            $"SendBuffer New[ {SendBuffer.NewCount} ] Rent[ {SendBuffer.RentCount} ] Return[ {SendBuffer.ReturnCount} ]\n" );

        DbExectuor.Post( ctx => perfLog.Insert( ref ctx ) );
    }

    private static PerfLog CreatePerfLog( WorldServer2 server, ref GCMemoryInfo gcMemoryInfo, long totalMemroy )
    {
        var perfLog = new PerfLog();
        perfLog.SetKeyId( IdHelper.Next() );
        perfLog.SetTime( DateTime.Now );
        perfLog.SetGCGen0( GC.CollectionCount( 0 ) );
        perfLog.SetGCGen1( GC.CollectionCount( 1 ) );
        perfLog.SetGCGen2( GC.CollectionCount( 2 ) );
        perfLog.SetMemory( $"Total {ToMB( totalMemroy )}MB Promoted: {ToMB( gcMemoryInfo.PromotedBytes )} MB" );
        perfLog.SetNativeMemory( $"{ToMB( server.GetNativeMemorySize() )}MB" );
        perfLog.SetContextSwitching( (long)PerfHelper.ContextSwitchesCount );
        perfLog.SetCcu( ClientSessionManager.Count() );
        perfLog.SetWorkingSet( (long)PerfHelper.WorkingSet );
        perfLog.SetGlobalJobCount( (int)Actor.Global.GetJobCount() );
        perfLog.SetSendBufferCount( (int)SendBuffer.NewCount );
        perfLog.SetSystemCall( (long)PerfHelper.SytemCallPerSec );
        perfLog.SetSystemRecvBytes( (long)PerfHelper.ReceivedBytes );
        perfLog.SetSystemSentBytes( (long)PerfHelper.SentBytes );
        perfLog.SetSystemRecvPackets( (long)PerfHelper.ReceivedPackets );
        perfLog.SetSystemSentPackets( (long)PerfHelper.SentPackets );
        perfLog.SetNpc( 40000 );
        perfLog.SetCpuUsage( PerfHelper.CpuUsage );
        perfLog.SetCriticalLogCount( Log.CriticalLogCount );
        perfLog.SetDebugLogCount( Log.DebugLogCount );
        perfLog.SetErrorLogCount( Log.ErrorLogCount );
        perfLog.SetInfoLogCount( Log.InfoLogCount );

        return perfLog;
    }

    private static float ToMB( long byteSize ) => MathF.Round( byteSize / ( 1024 * 1024.0f ), 2 );
}
