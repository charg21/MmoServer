﻿using Mala.Logging;
using System.Text.Json;
using System.Text.Json.Serialization;

public static class Config
{
    /// <summary>
    /// 서비스 타입
    /// </summary>
    public static EServiceType ServiceType = EServiceType.World;

    /// <summary>
    /// 워커 스레드 갯수
    /// </summary>
    public static int WorkerThreadCount = 0;

#region Net

    /// <summary>
    /// 서버 리스너 IP
    /// </summary>
    public static string ServerListenerIp = "127.0.0.1";

    /// <summary>
    /// 서버 리스너 포트
    /// </summary>
    public static int ServerListenerPort = 0;

    /// <summary>
    /// 클라이언트 리스너 IP
    /// </summary>
    public static string ClientListenerIp = "127.0.0.1";

    /// <summary>
    /// 클라이언트 리스너 포트
    /// </summary>
    public static int ClientListenerPort = 0;

    /// <summary>
    /// 클라이언트 세션 준비 수
    /// </summary>
    public static int ClientSessionPrepareCount = 500;

    /// <summary>
    /// 서버 세션 준비 수
    /// </summary>
    public static int ServerSessionPrepareCount = 20;

#endregion

#region Db
    /// <summary>
    /// Db 사용
    /// </summary>
    public static bool UseDb = false;

    /// <summary>
    /// 월드 DB 호스트
    /// </summary>
    public static string WorldDbHost = string.Empty;

    /// <summary>
    /// 월드 DB 포트
    /// </summary>
    public static int WorldDbPort = 0;

    /// <summary>
    /// 월드 DB 스키마
    /// </summary>
    public static string WorldDbSchema = string.Empty;

    /// <summary>
    /// 월드 DB 유저
    /// </summary>
    public static string WorldDbUser = string.Empty;

    /// <summary>
    /// 월드 DB 비밀번호
    /// </summary>
    public static string WorldDbPassword = string.Empty;

    /// <summary>
    /// 월드 DB 커넥션 수
    /// </summary>
    public static int WorldDbConnectionCount = 0;

#endregion

#region Design
    /// <summary>
    /// 기획 데이터 경로
    /// </summary>
    public static string DesignPath = string.Empty;
#endregion

#region Log
    /// <summary>
    /// 로그 레벨
    /// </summary>
    public static ELogLevel LogLevel = ELogLevel.All;

#endregion

#region Auth
    /// <summary>
    /// 게임 마스터 토큰
    /// </summary>
    public static string GmToken = "1q2w3e4r";
#endregion

#region Web
    /// <summary>
    /// 웹서비스 사용 여부
    /// </summary>
    public static bool UseWebService = false;

    /// <summary>
    /// 웹서비스 호스트
    /// </summary>
    public static string WebHost = string.Empty;
#endregion

#region Script
    /// <summary>
    /// 스크립트 경로
    /// </summary>
    public static string ScriptPath = string.Empty;
#endregion

#region Contents
    public static bool UseCollisionGrid => false;
#endregion

    /// <summary>
    /// 정적 생성자
    /// </summary>
    static Config()
    {
    }

    /// <summary>
    ///
    /// </summary>
    public static void Load( string[] args, string configPath = "" )
    {
        if ( string.IsNullOrEmpty( configPath ) )
            configPath = $"ServiceConfig";

        configPath += ".json";

        if ( args.Length > 0 && !string.IsNullOrEmpty( args[ 0 ] ) )
            configPath = args[ 0 ];

        Console.WriteLine( $"Config Path = { configPath }" );
        var configString = File.ReadAllText( configPath );

        using var document = JsonDocument.Parse( configString );
        var nationServerConfigElement = document.RootElement.GetProperty( "WorldServer" );

        /// Json 역직렬화를 진행한다.
        var option = new JsonSerializerOptions();
        option.Converters.Add( new JsonStringEnumConverter< ELogLevel >() );

        var config = JsonSerializer.Deserialize< ServerConfigDto >( nationServerConfigElement, option );
        ImportFrom( in config );
    }

    /// <summary>
    /// 설정 Dto객체로부터 설정값을 불러온다.( 리플렉션으로 반영 )
    /// </summary>
    private static void ImportFrom< T >( in T config )
    {
        foreach ( var propConfig in config.GetType().GetProperties() )
        {
            var fieldEnv = typeof( Config ).GetField( propConfig.Name );
            if ( fieldEnv != null )
            {
                fieldEnv.SetValue( null, propConfig.GetValue( config ) );
                Console.WriteLine( $"{ propConfig.Name } = { propConfig.GetValue( config ) } " );
            }
        }
    }

    /// <summary>
    /// 리플렉션을 사용하여 필드 값을 수정한다.
    /// </summary>
    public static void SetField( string fieldName, string value )
    {
        var fieldEnv = typeof( Config ).GetField( fieldName );
        if ( fieldEnv is null )
            return;

        object typeValue = Convert.ChangeType( value, fieldEnv.FieldType );

        fieldEnv.SetValue( null, typeValue );
        Console.WriteLine( $"{ fieldName } = { value }" );
    }

    /// <summary>
    ///
    /// </summary>
    public static string WorldDbConnectionString => $"Server=localhost;Database={ WorldDbSchema };Uid={ WorldDbUser };Pwd={ WorldDbPassword }; ";
}

/// <summary>
/// 설정 데이터 전달 객체
/// </summary>
public class WorldServerConfigDto : ServerConfigDto
{
    /// <summary>
    /// 생성자
    /// </summary>
    public WorldServerConfigDto()
    {
    }
}


/// <summary>
/// 설정 데이터 전달 객체
/// </summary>
public class ServerConfigDto
{
    /// <summary>
    ///
    /// </summary>
    public WorldServerConfigDto[] WorldServerList { get; set; }

    /// <summary>
    /// 생성자
    /// </summary>
    public ServerConfigDto()
    {
    }

#region Io

    /// <summary>
    /// 워커 스레드 수
    /// </summary>
    public int WorkerThreadCount { get; set; } = 0;

#endregion

#region Net

    /// <summary>
    /// 서버 리스너 IP
    /// </summary>
    public string ServerListenerIp { get; set; } = string.Empty;

    /// <summary>
    /// 월드 리스너 포트
    /// </summary>
    public int ServerListenerPort { get; set; } = 0;

    /// <summary>
    /// 클라이언트 리스너 IP
    /// </summary>
    public string ClientListenerIp { get; set; } = string.Empty;

    /// <summary>
    /// 클라이언트 리스너 포트
    /// </summary>
    public int ClientListenerPort { get; set; } = 0;

    /// <summary>
    /// 클라이언트 세션 준비 수
    /// </summary>
    public int ClientSessionPrepareCount { get; set; } = 0;

    /// <summary>
    /// 서버 세션 준비 수
    /// </summary>
    public int ServerSessionPrepareCount { get; set; } = 0;

#endregion

#region Db

    /// <summary>
    /// Db 사용
    /// </summary>
    public bool UseDb { get; set; } = false;

    /// <summary>
    /// 월드 DB 호스트
    /// </summary>
    public string WorldDbHost { get; set; } = string.Empty;

    /// <summary>
    /// 월드 DB 포트
    /// </summary>
    public int WorldDbPort { get; set; } = 0;

    /// <summary>
    /// 월드 DB 스키마
    /// </summary>
    public string WorldDbSchema { get; set; } = string.Empty;

    /// <summary>
    /// 월드 DB 유저
    /// </summary>
    public string WorldDbUser { get; set; } = string.Empty;

    /// <summary>
    /// 월드 DB 비밀번호
    /// </summary>
    public string WorldDbPassword { get; set; } = string.Empty;

    /// <summary>
    /// 월드 DB 커넥션 수
    /// </summary>
    public int WorldDbConnectionCount { get; set; } = 0;

#endregion

#region Design
    /// <summary>
    /// 기획 데이터 경로
    /// </summary>
    public string DesignPath { get; set; } = string.Empty;
#endregion

#region Log
    /// <summary>
    /// 로그 레벨
    /// </summary>
    public ELogLevel LogLevel { get; set; } = ELogLevel.All;

#endregion

#region Auth
    /// <summary>
    /// 게임 마스터 토큰
    /// </summary>
    public string GmToken { get; set; } = "1q2w3e4r";
#endregion

#region Web
    /// <summary>
    /// 웹서비스 사용 여부
    /// </summary>
    public bool UseWebService { get; set; } = false;

    /// <summary>
    /// 웹서비스 호스트
    /// </summary>
    public string WebHost { get; set; } = "http://localhost:8080/";
#endregion

#region Script
    /// <summary>
    /// 스크립트 경로
    /// </summary>
    public string ScriptPath { get; set; } = string.Empty;
#endregion

#region Contents
    public static bool UseCollisionGrid { get; set; } = false;
#endregion


}

