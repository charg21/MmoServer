﻿using Mala.Net;
/// <summary>
/// 월드의 가상의 격자 공간
/// </summary>
public struct Cluster
{
    public Cluster()
    {
    }

    public ActorMap Pcs { get; } = new( Consts.Cluster.DefaultPcMapCapacity );
    public ActorMap Npcs { get; } = new( Consts.Cluster.DefaultNpcMapCapacity );

    /// <summary>
    /// 입장한다
    /// </summary>
    public bool Enter( WorldObject worldObject )
    {
        return worldObject.Type switch
        {
            EWorldObjectType.Pc => Pcs.TryAdd( worldObject.Id, worldObject as Pc ),
            EWorldObjectType.Npc => Npcs.TryAdd( worldObject.Id, worldObject as Npc ),

            _ => false,
        };
    }

    /// <summary>
    /// 퇴장한다
    /// </summary>
    public bool Leave( WorldObject worldObject )
    {
        return worldObject.Type switch
        {
            EWorldObjectType.Pc => Pcs.Remove( worldObject.Id ),
            EWorldObjectType.Npc => Npcs.Remove( worldObject.Id ),

            _ => false,
        };
    }

    public void Send( ref SendSegment sendSegment )
    {
        foreach ( var ( _, pc ) in Pcs )
            pc.Sender.Send( ref sendSegment );
    }
};
