﻿using Mala.Logging;
using Mala.Threading;
using GC = System.GC;


public enum EClusterLayer
{
    Default,
    Wide
}

/// <summary>
/// 클러스터 관리자
/// TLS별 생성 스레드 세이프하게 오브젝트의 추가 삭제 검색 연산이 가능하도록 함
/// </summary>
public partial class WideClusterLayer
{
    public World OwnerWorld { get; private set; }

    /// <summary>
    /// 클러스터 2차원 배열
    /// </summary>
    public WideCluster[] _wideClusterGrid = GC.AllocateArray< WideCluster >( Consts.WideCluster.Capacity, true );

    /// <summary>
    /// 생성자
    /// </summary>
    public WideClusterLayer( World world )
    {
        OwnerWorld = world;
        PrepareCacheJob();
    }

    /// <summary>
    /// 초기화한다.
    /// </summary>
    public void Init()
    {
        for ( int y = 0; y < Consts.WideCluster.MaxRow; y += 1 )
        {
            for ( int x = 0; x < Consts.WideCluster.MaxCol; x += 1 )
            {
                var index = WorldHelper.ToClusterSclarIndex( x, y );

                _wideClusterGrid[ index ] = new WideCluster();
                _wideClusterGrid[ index ].Initialize( new( x, y ) );
            }
        }
    }

    /// <summary>
    /// 클러스터를 획득한다.
    /// </summary>
    public ref WideCluster GetCluster( Index index )
    {
        // TODO Index Check
        return ref _wideClusterGrid[ ( index.y * Consts.WideCluster.MaxCol ) + index.x ];
    }

    /// <summary>
    /// 클러스터를 획득한다.
    /// </summary>
    public ref WideCluster GetCluster( Position pos )
    {
        var index = WorldHelper.ToWideClusterIndex( pos );
        return ref GetCluster( index );
    }

    /// <summary>
    /// 인덱서
    /// </summary>
    public ref WideCluster this[ Index index ]
    {
        get => ref GetCluster( index );
    }

    public void OnMove( Position fromPos, Position toPos, WorldObject @object )
    {
        var fromIndex = WorldHelper.ToWideClusterIndex( fromPos );
        var toIndex   = WorldHelper.ToWideClusterIndex( toPos );

        if ( fromIndex != toIndex )
            Switch( fromIndex, toIndex, @object );
    }

    public void Switch( Index from, Index to, WorldObject actor )
    {
        OnLeave( from, actor );
        OnEnter( to, actor );
    }

    private void OnLeave( Index from, WorldObject actor )
    {
        ref var fromCluster = ref GetCluster( from );
        if ( fromCluster.Leave( actor ) )
            return;

        Log.Critical( $"Remove Fail.... WorldObjId[{actor.Id}] Index[{from}] " );
    }

    /// <summary>
    /// 클러스터를 입장한다
    /// </summary>
    public void OnEnter( Index wideClusterIndex, WorldObject obj )
    {
        ref var toCluster = ref GetCluster( wideClusterIndex );
        toCluster.Enter( obj );
    }

    /// <summary>
    /// 작업을 캐싱한다.
    /// ( 매번 람다 생성시 newobj를 호출하는것 방지용 )
    /// </summary>
    private void PrepareCacheJob()
    {
        LeaveClusterJob = ( Index index, WorldObject object2 )
            => OwnerWorld.ClusterManager.WideClusterLayer._wideClusterGrid[ ( index.y * Consts.WideCluster.MaxCol ) + index.x ].Leave( object2 );

        LeaveJob = ( obj, toPos ) =>
        {
            WorldObjectManager.Local.Unregister( obj.Id, obj.Type );

            var index = WorldHelper.ToClusterIndex( toPos );
            OwnerWorld.ClusterManager.ClusterLayer.OnLeave( index, obj );
        };
    }

    Action< Index, WorldObject > LeaveClusterJob;
    Action< WorldObject, Position > LeaveJob;

    /// <summary>
    /// 클러스터를 퇴장한다
    /// </summary>
    public void LeaveCluster( Index from, WorldObject obj )
    {
        Actor.Global.Post( LeaveClusterJob, in from, in obj );
    }

    /// <summary>
    /// 퇴장한다
    /// </summary>
    public void Leave( WorldObject obj )
    {
        var movement = obj.Movement;
        LeaveCluster( obj._wideClusterIndex, obj );
        Actor.Global.Post( LeaveJob, in obj, in movement.Pos );
    }

}
