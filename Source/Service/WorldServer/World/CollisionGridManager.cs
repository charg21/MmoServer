﻿using Mala.Collection;
using Mala.Math;
using Mala.Threading;

/// <summary>
/// 충돌 격자, 격자와 소유자의 관계는 N:1이다.
/// </summary>
[System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Explicit )]
public struct CollisionGrid
{
    /// <summary>
    /// CAS가 자주 발생하기 때문에, False Sharing 방지용 캐시 라인 패딩
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(0)]
    CacheLinePadding64 _padding = new();

    /// <summary>
    /// 소유자 식별자
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(0)]
    public ulong OwnerId = 0;

    /// <summary>
    /// 56바이트 여유가 생김
    /// </summary>

    public CollisionGrid()
    {
    }
}

/// <summary>
/// 충돌 그리드 관리자
/// </summary>
public class CollisionGridManager
{
    public const int WIDTH  = Consts.CollisionGrid.Width;
    public const int HEIGHT = Consts.CollisionGrid.Height;
    public const int WIDTH_WITH_OFFSET = WIDTH + 1;
    public const int HEIGHT_WITH_OFFSET = HEIGHT + 1;

    CollisionGrid[] _collisionGridList =
        GC.AllocateArray< CollisionGrid >( WIDTH_WITH_OFFSET * HEIGHT_WITH_OFFSET, true );

    /// <summary>
    /// 생성자
    /// </summary>
    public CollisionGridManager( World world )
    {
    }

    /// <summary>
    /// 좌표에 존재하는 그리드에 대해 잠금( 상호 배제 )을(를) 시도한다.
    /// </summary>
    public bool TryLock( Position pos, ObjectId id )
    {
        var gridIndex = WorldHelper.GetCollisionGridIndex( pos );
        return TryLock( gridIndex.x, gridIndex.y, id );
    }

    /// <summary>
    /// 그리드에 대해 잠금( 상호 배제 )을(를) 시도한다.
    /// </summary>
    public bool TryLock( int x, int z, ObjectId id )
    {
        /// 성능을 위해선 CCAS 방식으로 느슨한 체크를 추가해도 될듯?
        /// if ( ref GetGrid( x, z ).OwnerId == 0 )
        return 0 == Interlocked.CompareExchange( ref GetGrid( x, z ).OwnerId, id.AsPrimitive(), 0 );
    }

    /// <summary>
    /// 그리드에 대해 잠금을 해제한다.
    /// </summary>
    public void Unlock( int x, int z )
    {
        ref var grid = ref GetGrid( x, z );
        grid.OwnerId = 0;
    }

    /// <summary>
    /// 좌표에 존재하는 그리드에 대해 잠금을 해제한다.
    /// </summary>
    internal void Unlock( Position fromPos )
    {
        var fromIndex = WorldHelper.GetCollisionGridIndex( fromPos );
        Unlock( fromIndex );
    }

    /// <summary>
    /// 좌표에 존재하는 그리드에 대해 잠금을 해제한다.
    /// </summary>
    internal void Unlock( Index fromIndex )
    {
        Unlock( fromIndex.x, fromIndex.y );
    }

    /// <summary>
    /// 그리드를 획득한다.
    /// </summary>
    private ref CollisionGrid GetGrid( int x, int z )
    {
        return ref _collisionGridList[ ( WIDTH * z ) + x ];
    }

}