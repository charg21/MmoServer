﻿using Mala.Collection;
using Mala.Math;
using Mala.Threading;
using System.Threading;

/// <summary>
/// 충돌 격자, 격자와 소유자의 관계는 N:1이다.
/// </summary>
[System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Explicit )]
public struct CollisionHexGrid
{
    /// <summary>
    /// 소유자 식별자
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(0)]
    public ulong OwnerId = 0;

    /// <summary>
    /// CAS가 자주 발생하기 때문에, False Sharing 방지용 캐시 라인 패딩
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(0)]
    CacheLinePadding64 _padding = new();

    [System.Runtime.InteropServices.FieldOffset(8)]
    int Q;
    [System.Runtime.InteropServices.FieldOffset(12)]
    int R;
    [System.Runtime.InteropServices.FieldOffset(16)]
    int S;

    public CollisionHexGrid()
    {
    }
}

public static class HexGridHelper
{
    /// <summary>
    /// q는 col, r은 row. s는 q + r = -s
    /// </summary>
    public static ( int q, int r, int s ) OffsetToCube( int x, int z )
    {
        int q = x - ( z - ( z & 1 ) ) / 2;
        int r = z;
        int s = -q - r;
        return ( q, r, s );
    }

    public static ( int x, int z ) CubeToOffset( int q, int r, int s )
    {
        int x = q + ( r - ( r & 1 ) ) / 2;
        int z = r;
        return ( x, z );
    }
}

/// <summary>
/// 충돌 그리드 관리자
/// [1][2]
/// [3][4]
///
/// [2][3][4]에 액터가 존재하고, [4] => [1]로 이동을 막는것은 CAS1회로 불가
/// 대각선 이동을 막기 위해 육각형 타일을 사용
/// </summary>
public class CollisionHexGridManager
{
    public const int WIDTH = Consts.CollisionGrid.Width;
    public const int HEIGHT = Consts.CollisionGrid.Height;
    public const int WIDTH_WITH_OFFSET = WIDTH + 1;
    public const int HEIGHT_WITH_OFFSET = HEIGHT + 1;

    CollisionHexGrid[] _collisionGridList =
        GC.AllocateArray< CollisionHexGrid >( WIDTH_WITH_OFFSET * HEIGHT_WITH_OFFSET, true );

    /// <summary>
    /// 생성자
    /// </summary>
    public CollisionHexGridManager( World world )
    {
    }

    /// <summary>
    /// 좌표에 존재하는 헥스 그리드에 대해 잠금( 상호 배제 )을(를) 시도한다.
    /// </summary>
    public bool TryLock( ref Position pos, ActorId id )
    {
        /// 헥스 그리드를 획득
        ref var hexGrid = ref GetGrid( ref pos );

        /// 성능을 위해서 CCAS 방식으로 느슨한 체크
        if ( hexGrid.OwnerId != 0 )
            return false;

        /// 실제 CAS 진행
        return 0 == Interlocked.CompareExchange( ref hexGrid.OwnerId, id, 0 );
    }

    /// <summary>
    /// Vector3 포지션을 통해 해당 위치의 그리드를 획득한다.
    /// </summary>
    public ref CollisionHexGrid GetGrid( ref Position pos )
    {
        /// 좌표를 헥스 그리드 인덱스로 변환
        var hexGridIndex = WorldHelper.GetCollisionHexGridIndex( pos );
        return ref GetGrid( hexGridIndex );
    }

    private ref CollisionHexGrid GetGrid( Index hexGridIndex )
    {
        /// 헥스 그리드를 반환
        return ref _collisionGridList[ ( WIDTH * hexGridIndex.y ) + hexGridIndex.x ];
    }

    /// <summary>
    /// 좌표에 존재하는 그리드에 대해 잠금을 해제한다.
    /// </summary>
    internal void Unlock( Index hexGridIndex )
    {
        /// 헥스 그리드 인덱스로 Grid를 얻음
        ref var hexGrid = ref GetGrid( hexGridIndex );
        hexGrid.OwnerId = 0;
    }

    /// <summary>
    /// 좌표에 존재하는 그리드에 대해 잠금을 해제한다.
    /// </summary>
    internal void Unlock( Position pos )
    {
        var gridIndex = WorldHelper.GetCollisionHexGridIndex( pos );
        Unlock( gridIndex );
    }

}