﻿using Mala.Math;

/// <summary>
///
/// </summary>
public static class ClusterHelper
{
    static ThreadLocal< ActorMap > _objectList = new( () => new() );


    /// <summary>
    /// 범위내 클러스터를 모은다.
    /// </summary>
    public static void GatherRangeCluster( WorldObject caster, float skillRange )
    {
        /// 인접시 시야범위내에 드는 클러스터 수집
        var fromPos     = caster.Movement.FromPos;
        //var nearCluster = caster.World.WideClusterManager.GetNearCluster( ref fromPos );

        /// 내가 바라 보는 방향 기준으로 스킬 범위 박스를 생성
        var direction = caster.Movement.Dir; // 액터의 방향
        var boxCenter = fromPos + ( direction * ( skillRange / 2 ) );

        //var skillRangeBox = new Box( new ( boxCenter.x, boxCenter.z ), 5, 5 );

        ///// AABB로 스킬 범위내에 드는 클러스터 수집
        //var nearClusterList = new List< Index >();
        //for ( int i = 0; i < nearCluster.Count(); i += 1 )
        //{
        //    ref var c = ref caster.World.ClusterManager.GetCluster( nearCluster[ i ] );
        //    if ( c._box.IsOverlapped( ref skillRangeBox ) )
        //        nearClusterList.Add( c._index );
        //}
    }

    public static bool IsInRange( Position fromPos, Position otherPos, float range = Consts.View.Fov )
    {
        var distance = fromPos - otherPos;
        return distance.SizeSquared() <= Consts.View.FovSquard;
    }


    public static ActorMap GatherActortList(
        WorldObject caster,
        Position   fromPos,
        EHitShape  shpae,
        float      range,
        Position   targetPos )
    {
        //var nearCluster = caster.World.ClusterManager.GetNearCluster( ref fromPos );

        //NearCluster nearCluster1 = new();
        //Box box = new Box();
        //for ( int i = 0; i < nearCluster.Count(); i += 1 )
        //{
        //    ref var c = ref caster.World.ClusterManager.GetCluster( nearCluster[ i ] );
        //    if ( c._box.IsOverlapped( ref box ) )
        //        nearCluster1.Add( ref c );
        //}

        //ref var cluster = ref caster.World.ClusterManager.GetCluster( fromPos );

        //var rangeSqrt = range * range;

        //foreach ( var target in cluster._actors )
        //{
        //    var distanceSqrt = ( fromPos + targetPos ).SizeSquared();
        //    if ( distanceSqrt > rangeSqrt )
        //        continue;

        //    _objectList.Value.Add( target.Key, target.Value );
        //}

        //if ( range > Consts.View.Width )
        //{
        //}

        return _objectList.Value;
    }
}

