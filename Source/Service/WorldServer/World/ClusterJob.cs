﻿using Mala.Math;
using Mala.Threading;


/// <summary>
/// 분할된 공간 단위
/// </summary>
public class ClusterEnterJob : GlobalJob
{
    static Action< Index, WorldObject > _action;

    static ClusterEnterJob()
    {
    }

    public ClusterEnterJob( Index to, WorldObject obj )
    : base( Config.WorkerThreadCount, () => { _action( to, obj ); } )
    {
        obj._wideClusterIndex = to;
    }
}
