﻿using Mala.Logging;
using Mala.Threading;

public class ClusterManager
{
    public World OwnerWorld { get; private set; }

    public WideClusterLayer WideClusterLayer { get; private set; }
    public ClusterLayer ClusterLayer { get; private set; }


    Action< Index, Index, WorldObject > ChangeClusterJob;
    Action< Index, WorldObject > EnterClusterJob;
    Action< Index, WorldObject > LeaveClusterJob;
    Action< WorldObject, Position > EnterJob;
    Action< WorldObject, Position > LeaveJob;
    public Action< WorldObject, Position, Position > UpdatePosJob;

    private void PrepareCacheJob()
    {
        /// 입장 및 퇴장을 한개의 작업으로 처리
        /// 클러스터 변경을 두개의 작업으로 나누어 처리시,
        /// 순간적으로 다른 스레드에서 퇴장, 입장중 퇴장 까지만 처리한 케이스가 발생 가능하다.
        /// 중간에 액터 관측이 되지 않아, 공격대상 처리의 누락이 발생 할 수 있다.
        ChangeClusterJob = ( Index from, Index to, WorldObject @object ) =>
        {
            var movement = @object.Movement;
            var wideClusterLayer = @object.ClusterManager.WideClusterLayer;
            //var clusterLayer = @object.ClusterManager.WideClusterLayer;

            if ( !wideClusterLayer._wideClusterGrid[ ( from.y * Consts.WideCluster.MaxCol ) + from.x ].Leave( @object ) )
                Log.Critical( $" Remove Failed... ObjectId: {@object.Id} Cluster: ( {from.y}, {from.x} ) => ( {to.y}, {to.x} ), Pos( {movement.Pos.x}, {movement.Pos.z} )" );
            if ( !wideClusterLayer._wideClusterGrid[ ( to.y * Consts.WideCluster.MaxCol ) + to.x ].Enter( @object ) )
                Log.Critical( $" Enter Failed... ObjectId: {@object.Id} ClusterIdx: ( {from.y}, {from.x} ) => ( {to.y}, {to.x} ), Pos( {movement.Pos.x}, {movement.Pos.z} )" );
        };

        EnterJob = ( worldObj, fromPos ) =>
        {
            WorldObjectManager.Local.Register( worldObj );

            var clusterIndex = WorldHelper.ToClusterIndex( fromPos );
            var wideClusterIndex = WorldHelper.ToWideClusterIndex( fromPos );

            var wideClusterLayer = worldObj.ClusterManager.WideClusterLayer;
            var clusterLayer = worldObj.ClusterManager.ClusterLayer;

            wideClusterLayer?.OnEnter( wideClusterIndex, worldObj );
            clusterLayer.OnEnter( clusterIndex, worldObj );
        };

        UpdatePosJob = ( WorldObject worldObj, Position fromPos, Position toPos ) =>
        {
            var wideClusterLayer = worldObj.ClusterManager.WideClusterLayer;
            var clusterLayer = worldObj.ClusterManager.ClusterLayer;

            wideClusterLayer?.OnMove( fromPos, toPos, worldObj );
            clusterLayer.OnMove( fromPos, toPos, worldObj );
        };

        LeaveJob = ( obj, toPos ) =>
        {
            WorldObjectManager.Local.Unregister( obj.Id, obj.Type );

            var index = WorldHelper.ToClusterIndex( toPos );
            OwnerWorld.ClusterManager.ClusterLayer.OnLeave( index, obj );
        };
    }

    public ClusterManager( World world )
    {
        OwnerWorld = world;
        WideClusterLayer = new WideClusterLayer( world );
        ClusterLayer = new ClusterLayer();
        PrepareCacheJob();
    }
    public void Init()
    {
        WideClusterLayer?.Init();
        ClusterLayer.Init();
    }

    public void OnEnterWorld( WorldObject worldObj )
    {
        Actor.Global.Post( EnterJob, in worldObj, in worldObj.Movement.Pos );
    }

    public void OnMove( Position fromPos, Position toPos, WorldObject worldObj )
    {
        var movement = worldObj.Movement;
        if ( movement.FromPos == movement.Pos )
            return;

        for ( ;; )
        {
            // 여기서 비교시 같다면 return
            // 현재는 클러스터와 와이드 클러스터는 배율 스케일링 된 상태로 일반 클러스의 변경점이 있는 상황에서
            // 와이드 클러스터는 변경이 생김
            {
                var fromIndex = WorldHelper.ToClusterIndex( movement.FromPos );
                var toIndex   = WorldHelper.ToClusterIndex( movement.Pos );
                if ( fromIndex != toIndex )
                    break;
            }

            //{
            //    var fromIndex = WorldHelper.ToWideClusterIndex( movement.FromPos );
            //    var toIndex   = WorldHelper.ToWideClusterIndex( movement.Pos );
            //    if ( fromIndex != toIndex )
            //        break;
            //}

            return;
        }

        Actor.Global.Post( UpdatePosJob, worldObj, movement.FromPos, movement.Pos );
    }


    public void OnLeaveWorld( WorldObject worldObj )
    {
        Actor.Global.Post( LeaveJob, in worldObj, in worldObj.Movement.Pos );
    }

}