﻿using Mala.Logging;
using Mala.Threading;
using System.Collections.Concurrent;

/// <summary>
/// 월드
/// </summary>
public class World
{
    /// <summary>
    /// 격자 충돌 관리자
    /// </summary>
    public CollisionGridManager CollisionGridManager { get; private set; }

    /// <summary>
    /// 월드 액터 컬렉션
    /// 스레드별 액터는 클러스터 매니저를 통해 접근
    /// </summary>
    public ConcurrentDictionary< ObjectId, WorldObject > _objects { get; set; } = new( concurrencyLevel: 30, capacity: 10000 );

    /// <summary>
    /// 채널
    /// </summary>
    public int ChannelId => Channel.Id;
    public Channel Channel { get; set; }

    /// <summary>
    /// 생성자
    /// </summary>
    public World( Channel channel )
    {
        Channel = channel;
		_clusterManager = new( () =>
		{
			var clusterManager = new ClusterManager( this );
			clusterManager.Init();
			return clusterManager;
		} );

		if ( Config.UseCollisionGrid )
            CollisionGridManager = new CollisionGridManager( this );
    }

    /// <summary>
    /// 클러스터 관리자
    /// </summary>
	public ClusterManager ClusterManager => _clusterManager.Value!;
	private ThreadLocal< ClusterManager > _clusterManager;

    /// <summary>
    /// 월드에 스폰한다.
    /// </summary>
    public void Spawn< T >( ObjectId actorId, Position pos, Direction dir ) where T : WorldObject, new()
    {
        var obj = new T();
        var movement = obj.Movement;
        obj.Id = actorId;
        obj.World = this;

        movement.Pos          = pos;
        movement.FromPos      = pos;
        movement.Dir          = dir;
        obj._wideClusterIndex = WorldHelper.ToWideClusterIndex( obj.Movement.Pos );

        Enter( obj );
    }

    /// <summary>
    /// 월드에 스폰한다.
    /// </summary>
    public void SpawnNpc(
        ObjectId actorId,
        DesignId npcDesingId,
        Position pos,
        Direction dir )
    {
        var npcDesign = NpcDesign.Get( npcDesingId );
        if ( npcDesign is null )
        {
            Log.Error( $"NpcDesign is null. NpcDesignId:{npcDesingId}" );
            return;
        }

        var npc = new Npc();
        npc.Id = actorId;
        npc.Movement.Pos = pos;
        npc.Movement.FromPos = pos;
        npc.Movement.Dir = dir;
        npc.World = this;
        npc._wideClusterIndex = WorldHelper.ToWideClusterIndex( npc.Movement.Pos );

        /// 임시 테스트
        npc.Design = npcDesign;
        npc.Stat.Init();
        npc.Stat.Refresh();

        Enter( npc );
    }

    public bool Initialize()
    {
        /// 각 TLS객체는 월드 워커 스레드에서 초기화를 강제로 시켜줌
        Actor.Global.Post( () =>
        {
            _ = ClusterManager;
        } );

        return true;
    }

    /// <summary>
    /// 월드에 디스폰한다.
    /// </summary>
    public void Despawn( ActorId actorId )
    {
    }

    /// <summary>
    /// 월드에 입장한다.
    /// </summary>
    public bool Enter( WorldObject obj )
    {
        if ( !_objects.TryAdd( obj.Id, obj ) )
        {
            Log.Error( $"WorldObjId:{ obj.Id } is already exist" );
            return false;
        }

        obj.Post( obj.EnterWorld, this );

        return true;
    }

    /// <summary>
    /// 퇴장한다.
    /// </summary>
    public bool Leave( WorldObject actor )
    {
        if ( !_objects.TryRemove( actor.Id, out var _ ) )
        {
            Log.Error( $"ActorId:{actor.Id} is already exist" );
            return false;
        }

		ClusterManager.WideClusterLayer.Leave( actor );

        actor.Post( actor.OnLeaveWorld );

        return true;
    }

    /// <summary>
    /// 더미 NPC를 스폰한다.
    /// </summary>
    public void SpawnDummyNpc()
    {
        int baseId = 30000;

        /// x interval = 200 / 5 / 4 = 10
        // 40000( 200 * 200 )
        const int maxX = 200;
        const int maxZ = 200;
        const f32 interval = 4f;

        for ( int z = 0; z < maxZ; z += 1 )
        {
            int curZ = z;
            ThreadPoolExecutorService.Post( () =>
            {
                for ( int x = 0; x < maxX; x += 1 )
                {
                    var pos = new Position( x * interval * Consts.Multiple, 0f, curZ * interval * Consts.Multiple );
                    u64 npcId = (u64)( baseId + ( curZ * 1000 ) + x );
                    SpawnNpc( new( npcId ), (DesignId)( 1 ), pos, Direction.Zero );
				}
            } );
        }

        Log.Info( "Spawn Dummy Npc...Ok" );
    }

    public bool TryGetPc( ObjectId actorId, out Pc pc )
    {
        if( _objects.TryGetValue( actorId, out var acotr ) )
        {
            pc = acotr as Pc;
            if ( pc is not null )
                return true;
        }

        pc = null;
        return false;
    }

    public bool TryGetNpc( ObjectId actorId, out Npc npc )
    {
        if ( _objects.TryGetValue( actorId, out var actor ) )
        {
            npc = actor as Npc;
            if ( npc is not null )
                return true;
        }

        npc = null;
        return false;
    }
}

