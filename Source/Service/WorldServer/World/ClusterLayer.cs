﻿using Mala.Logging;
using Mala.Math;
using GC = System.GC;


public class ClusterLayer
{
    Cluster[] _clusterGrid = GC.AllocateArray< Cluster >( Consts.Cluster.Capacity, true );

    /// <summary>
    /// 생성자
    /// </summary>
    public ClusterLayer()
    {
    }

    public void Init()
    {
        for ( int i = 0; i < Consts.Cluster.Capacity; i += 1 )
        {
            _clusterGrid[ i ] = new();
        }
    }

    ref Cluster GetCluster( Position pos )
    {
        var clusterIndex = WorldHelper.ToClusterIndex( pos );
        return ref GetCluster( clusterIndex );
    }

    ref Cluster GetCluster( Index index )
    {
        return ref _clusterGrid[ ( index.y * ( Consts.Cluster.MaxCol ) ) + index.x ];
    }

    public void OnEnter( WorldObject actor )
    {
        var toClusterIndex = WorldHelper.ToClusterIndex( actor.Movement.Pos );
        OnEnter( toClusterIndex, actor );
    }
    public void OnEnter( Index to, WorldObject actor )
    {
        ref var toCluster = ref GetCluster( to );
        toCluster.Enter( actor );
    }

    public List< ActorMap > GatherWorldObjects( Position pos, EWorldObjectType worldObjType = EWorldObjectType.Max )
    {
        var gatherList = ViewComponent.ListForGather;

        var clusterIndex = WorldHelper.ToClusterIndex( pos );
        foreach ( var policy in WorldHelper.ClusterAccessPolicy )
        {
            var nearClusterIndex = clusterIndex + new Vector2Int( policy.Key, policy.Value );
            if ( !WorldHelper.ValidateClusterIndex( nearClusterIndex ) )
                continue;

            ref var cluster = ref GetCluster( nearClusterIndex );

            switch ( worldObjType )
            {
                case EWorldObjectType.Npc:
                    gatherList.Add( cluster.Npcs );
                    break;
                case EWorldObjectType.Pc:
                    gatherList.Add( cluster.Pcs );
                    break;
                case EWorldObjectType.Max:
                    gatherList.Add( cluster.Pcs );
                    gatherList.Add( cluster.Npcs );
                    break;
            }
        }

        return gatherList;
    }

    public void OnLeave( Index from, WorldObject worldObj )
    {
        ref var fromCluster = ref GetCluster( from );
        if ( fromCluster.Leave( worldObj ) )
            return;

        Log.Critical( $"Remove Fail.... WorldObjId[{worldObj.Id}] Index[{from}] " );
    }

    public void Switch( Index from, Index to, WorldObject worldObj )
    {
        OnLeave( from, worldObj );
        OnEnter( to, worldObj );
    }

    public void OnMove( Position fromPos, Position toPos, WorldObject worldObj )
    {
        var fromIndex = WorldHelper.ToClusterIndex( fromPos );
        var toIndex   = WorldHelper.ToClusterIndex( toPos );

        if ( fromIndex != toIndex )
            Switch( fromIndex, toIndex, worldObj );
    }

}
