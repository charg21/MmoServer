﻿using Mala.Math;
using Mala.Net;

/// <summary>
/// 넓은 범위 월드의 가상의 격자 공간
/// </summary>
public struct WideCluster
{
    /// <summary>
    /// Npc 컬렉션
    /// </summary>
    public NpcMap Npcs { get; set; } = new( Consts.WideCluster.DefaultNpcMapCapacity );

    /// <summary>
    /// 플레이어 컬렉션
    /// </summary>
    public PcMap Pcs { get; set; } = new( Consts.WideCluster.DefaultPcMapCapacity );

    /// <summary>
    /// 클러스터 인덱스
    /// </summary>
    public Index _index { get; set; } = new();
    public Box _box = new();

    /// <summary>
    /// 생성자
    /// </summary>
    public WideCluster()
    {
    }

    /// <summary>
    /// 초기화 한다
    /// </summary>
    public bool Initialize( Index index )
    {
        _index = index;

        _box.left   = (f32)( index.x * Consts.WideCluster.Width );
        _box.bottom = (f32)( index.y * Consts.WideCluster.Height );

        _box.right = (f32)( _box.left   + Consts.WideCluster.Width  - 1 );
        _box.top   = (f32)( _box.bottom + Consts.WideCluster.Height - 1 );

        _box.right  = Math.Min( _box.right, (f32)( Consts.World.Width  - 1 ) );
        _box.top    = Math.Min( _box.top,   (f32)( Consts.World.Height - 1 ) );

        return true;
    }

    /// <summary>
    /// 포함 여부를 반환한다
    /// </summary>
    public bool Contains( ObjectId objectId )
        => Pcs.ContainsKey( objectId ) || Npcs.ContainsKey( objectId );

    /// <summary>
    /// 입장한다
    /// </summary>
    public bool Enter( WorldObject @object )
    {
        return @object.Type switch
        {
            EWorldObjectType.Pc => Pcs.TryAdd( @object.Id, @object as Pc ),
            EWorldObjectType.Npc => Npcs.TryAdd( @object.Id, @object as Npc ),
            _ => false,
        };
    }

    /// <summary>
    /// 퇴장한다
    /// </summary>
    public bool Leave( WorldObject worldObj )
    {
        return worldObj.Type switch
        {
            EWorldObjectType.Pc => Pcs.Remove( worldObj.Id ),
            EWorldObjectType.Npc => Npcs.Remove( worldObj.Id ),
            _ => false,
        };
    }

    /// <summary>
    /// NPC를 반환한다
    /// </summary>
    public Npc? GetNpc( ObjectId worldObjId )
    {
        if ( Npcs.TryGetValue( worldObjId, out var npc ) )
            return npc;

        return null;
    }

    /// <summary>
    /// PC를 반환한다
    /// </summary>
    public Pc? GetPc( ObjectId worldObjId )
    {
        if ( Pcs.TryGetValue( worldObjId, out var pc ) )
            return pc;

        return null;
    }

    public void Send( ref SendSegment sendSegment )
    {
        foreach ( var ( _, pc ) in Pcs )
            pc.Send( ref sendSegment );
    }
}
