global using Position        = Mala.Math.Vector3;
global using Direction       = Mala.Math.Vector2;
global using Index           = Mala.Math.Vector2Int;
global using ViewList        = System.Collections.Generic.Dictionary< ObjectId, WorldObject >;
global using ActorList       = System.Collections.Generic.List< WorldObject >;
global using ActorMap        = System.Collections.Generic.Dictionary< ObjectId, WorldObject >;
global using NpcMap          = System.Collections.Generic.Dictionary< ObjectId, Npc >;
global using PcMap           = System.Collections.Generic.Dictionary< ObjectId, Pc >;
global using SkillTargetList = System.Collections.Generic.Dictionary< ObjectId, WorldObject >;
global using GiveItemList    = ValueList< PktItem >;
global using TakeItemList    = ValueList< PktItem >;

global using f32       = System.Single;
global using f64       = System.Double;

global using i8        = System.SByte;
global using i16       = System.Int16;
global using i32       = System.Int32;
global using i64       = System.Int64;

global using u8        = System.Byte;
global using u16       = System.UInt16;
global using u32       = System.UInt32;
global using u64       = System.UInt64;

global using ActorId  = System.UInt64;
