﻿using Mala.Logging;
using Mala.Threading;


public class WorldExecutorThread
{
    public static void DoWorkerJob( WorldServer2 service )
    {
        ClientSessionManager.SendRequestList = new( 4096 );
#if NATIVE
        NativeWorkerThread.DoInit();
#endif
        ThreadPoolExecutorService.Initialize();

        Log.Info( $"Tls Init Ok... CurThread Id: {ThreadPoolExecutorService._tlsContext.Id}" );

        for ( ;; )
        {
#if NATIVE
            service.Dispatch( 16 );
#endif
            ThreadPoolExecutorService.Run();

            ClientSessionManager.FlushSend();
        }
    }

}
