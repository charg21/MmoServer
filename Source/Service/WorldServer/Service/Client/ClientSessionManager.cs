﻿using Mala.Collection;
using Mala.Net;
using Mala.Threading;
using MySqlX.XDevAPI;
using System.Collections.Concurrent;
using Windows.Media.Protection.PlayReady;

/// <summary>
/// 클라이언트 세션 관리자
/// </summary>
public class ClientSessionManager : Actor
{
    /// <summary>
    /// 정적 인스턴스
    /// </summary>
    public static ClientSessionManager Instance = new();

    /// <summary>
    /// 세션 식별자 발급기
    /// </summary>
    public ulong _sessionIdIssuer = 0;

    [ThreadStatic]
    public static HashSet< ClientSession > SendRequestList;

    static HashSet< ClientSession > _clientSessions = new();
    static ConcurrentDictionary< ulong, ClientSession > _clientSessionHolder = new();
    static ReaderWriterLockSlim _lock = new();

    /// <summary>
    /// 송신한다.
    /// </summary>
    public void Send( ref SendSegment segment )
    {
        //_lock.EnterReadLock();

        //foreach ( var ( _, session ) in _sessionDict )
        //    session.Send( segment );

        //_lock.ExitReadLock();
    }

    public static void FlushSend()
    {
        foreach ( var session in SendRequestList )
            session.FlushSendQueue();

        SendRequestList.Clear();
    }

    /// <summary>
    /// 생성한다.
    /// </summary>
    public static ClientSession Generate()
    {
        var client = new ClientSession();
        client.SessionId = Interlocked.Increment( ref Instance._sessionIdIssuer );

        /// 의도치 않은 타이밍에 GC에 의해 수거되지 않도록 강제로 참조를 유지한다.
        _clientSessionHolder.TryAdd( client.SessionId, client );
        return client;
    }

    /// <summary>
    /// 생성한다.
    /// </summary>
    public static void Add( ClientSession client )
    {
        {
            _lock.EnterWriteLock();
            _clientSessions.Add( client );
            _lock.ExitWriteLock();
        }
    }

    /// <summary>
    /// 생성한다.
    /// </summary>
    public static void Remove( ClientSession session )
    {
        _lock.EnterWriteLock();
        _clientSessions.Remove( session );
        _lock.ExitWriteLock();

        /// 홀더에서 제거 하지않으면 GC 수거가 되지 않기에 제거한다.
        _clientSessionHolder.Remove( session.SessionId, out var _ );
    }

    /// <summary>
    /// 생성한다.
    /// </summary>
    public static int Count()
    {
        _lock.EnterReadLock();
        var count = _clientSessions.Count();
        _lock.ExitReadLock();

        return count;
    }
}
