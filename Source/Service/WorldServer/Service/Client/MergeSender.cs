﻿using Mala.Net;

public interface ISenderPolicy
{
}

public struct MergeSender : ISenderPolicy
{
    ClientSession Session;

    /// <summary>
    /// 현재 점유중인 송신버퍼 세그먼트
    /// </summary>
    public SendSegment SendingSegment = new( null, null );

    public MergeSender( ClientSession session )
    {
        Session = session;
    }

    /// <summary>
    ///
    /// </summary>
    public void MergeSend( ref SendSegment segment )
    {
        /// 세그먼트 세팅
        if ( SendingSegment._owner is null )
        {
            SendingSegment = segment;

            /// 잡을떄는 레퍼런스 카운트를 올려줘야 한다.
            SendingSegment.AddRef();
        }
        /// 세그먼트 스왑 케이스
        else if ( !SendingSegment.CanMerge( ref segment ) )
        {
            /// 기존 버퍼는 송신 큐에 추가
            Session.RequestSend( ref SendingSegment );

            SendingSegment.ReleaseRef();

            /// 세그먼트를 재설정한다.
            SendingSegment = segment;
            SendingSegment.AddRef();
        }
        else
        {
            /// 이외 케이스에대해 버퍼를 통합
            SendingSegment.Merge( ref segment );
        }
    }

    public void FlushSendingSegment()
    {
        /// 송신이 발생하지 않았다면 리턴
        if ( SendingSegment._owner is null )
            return;

        Session.RequestSend( ref SendingSegment );

        ClientSessionManager.SendRequestList.Add( Session );

        SendingSegment.ReleaseRef();

        /// 초기화
        SendingSegment = new( null, null );
    }

    public void Reset()
    {
        Session = null;
    }
}
