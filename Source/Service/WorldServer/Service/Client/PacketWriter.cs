using Mala.Math;
using Mala.Net;


public static class PacketWriter
{
    public static void FromPc( ref PktPc pktPc, Pc pc )
    {
        var movement = pc.Movement;
        pktPc._pos = movement.Pos.ToPktVector3();
        pktPc._yaw = MathHelper.Vector2ToYaw( movement.Dir );
        pktPc._objectId = (ulong)pc.Id;
    }

    public static void FromActor( ref PktObject actor, WorldObject @object )
    {
        var movement = @object.Movement;
        actor._pos = movement.Pos.ToPktVector3();
        actor._yaw = MathHelper.Vector2ToYaw( movement.Dir );
        actor._objectId = @object.Id.AsPrimitive();
        actor._objectType = @object.Type;
    }

    public static SendSegment FromS_LoadPc( Pc pc, ELogicResult logicResult = ELogicResult.Success )
    {
        var sLoadPc = new ValueS_LoadPc(){ _result = logicResult };
        if ( logicResult != ELogicResult.Success )
            return sLoadPc.Write();

        PacketWriter.FromPc( ref sLoadPc._pktPc, pc );
        //pc.Bag!.ExportToItemList( ref sLoadPc._items );
        pc.Equip!.ExportToEquipList( ref sLoadPc._equips );
        pc.SkillList!.ExportToSkillList( ref sLoadPc._skills );
        pc.Stat!.ExportToStatList( ref sLoadPc._stats );

        return sLoadPc.Write();
    }

    public static SendSegment FromS_Move( WorldObject actor )
    {
        return new ValueS_Move()
        {
            _objectId = (ulong)actor.Id,
            _pos = actor.Movement.Pos.ToPktVector3(),
            _toPos = actor.Movement.ToPos.ToPktVector3(),
            _yaw = MathHelper.Vector2ToYaw( actor.Movement.Dir )
        }.Write();
    }

    public static SendSegment FromS_EnterWorld( WorldObject worldObject )
    {
        return new ValueS_EnterWorld()
        {
            _my = new PktPc()
            {
                _pos = worldObject.Movement.Pos.ToPktVector3(),
                _yaw = MathHelper.Vector2ToYaw( worldObject.Movement.Dir ),
                _objectId = (ulong)worldObject.Id,
            },
        }.Write();
    }

    public static SendSegment FromS_LeaveWorld( WorldObject worldObject )
    {
        return new ValueS_LeaveWorld
        {
            _objectId = (ulong)worldObject.Id
        }.Write();
    }

    public static SendSegment FromS_Spawn( WorldObject other )
        => FromS_Spawn( other, other.Movement.Pos );

    public static SendSegment FromS_Spawn( WorldObject worldObject, Position pos )
    {
        var spawn = new ValueS_Spawn();

        FromActor( ref spawn._other, worldObject );
        spawn._other._objectId = worldObject.Id.AsPrimitive();
        spawn._other._pos = pos.ToPktVector3();

        return spawn.Write();
    }


    public static SendSegment FromS_Despawn( ObjectId objectId )
    {
        return new ValueS_Despawn()
        {
            _objectId = (ulong)objectId
        }.Write();
    }

    public static SendSegment FromS_Move( ObjectId otherId, in Position pos, Direction dir, in Position toPos )
    {
        return new ValueS_Move
        {
            _objectId = (ulong)otherId,
            _pos = pos.ToPktVector3(),
            _yaw = MathHelper.Vector2ToYaw( dir ),
            _toPos = toPos.ToPktVector3()
        }.Write();
    }

    public static SendSegment FromS_Hit( ObjectId casterId, ObjectId targetId, DesignId skillDesignId, long damage, long curHp )
    {
        return new ValueS_Hit
        {
            _casterId = (ulong)casterId,
            _targetId = (ulong)targetId,
            _damage = damage,
            _curHp = curHp,
            _skillDesignId = (uint)skillDesignId
        }.Write();
    }

    public static SendSegment FromS_StartSkill( ELogicResult logicResult )
    {
        return new ValueS_StartSkill
        {
            _result = logicResult
        }.Write();
    }

    public static SendSegment FromS_StartSkill( ObjectId casterId, ObjectId targetId, DesignId skillDesignId, long damage, long curHp )
    {
        /// 피격 판정 패킷 전송
        return new ValueS_StartSkill
        {
            _casterId = (ulong)casterId,
            _targetId = (ulong)targetId,
            _skillDesignId = (uint)skillDesignId
        }.Write();
    }

    public static SendSegment FromS_Chat( string name, string message )
    {
        return new ValueS_Chat
        {
            _name = name,
            _message = message
        }.Write();
    }

}

