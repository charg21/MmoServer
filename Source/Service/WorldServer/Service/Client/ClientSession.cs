﻿using System.Net;
using Mala.Net;
using Mala.Logging;
using Mala.Threading;
using Mala.Db;
using System;

/// <summary>
/// 클라이언트 세션 객체
/// </summary>
public partial class ClientSession : PacketSession
{
    public ulong SessionId { get; set; }

    public Pc Pc { get; set; } = new();
    public UserDbModel UserDbModel;

    public ClientSession()
    {
        Pc.Session = this;
        //Sender = new( this );
        _txExecutor = new( _executor );
    }

    public void MergeSend( SendSegment segment )
        => this.Send( segment );

    public override void OnConnected( EndPoint endPoint, bool success )
    {
        Log.Info( $"Client OnConnected : { endPoint }" );
        ClientSessionManager.Add( this );
        Tick();
    }

    public override void OnRecvPacket( ref ReadOnlySpan< byte > buffer )
    {
        var packetId = BitConverter.ToUInt16( buffer.Slice( HeaderLength, HeaderType ) );
        var packet   = PacketFactory.TlsInstance.Value!.GetPacket( packetId );
        DispatchPacket( ref buffer, packetId, packet );
    }

    private void DispatchPacket( ref ReadOnlySpan< byte > buffer, ushort packetId, PacketHeader? packet )
    {
        if ( !ClientPacketDispatcher.Instance.HandlerMap.TryGetValue( packetId, out var handler ) )
            return;

        packet!.Read( ref buffer );
        handler.Invoke( packet, this );
    }


    public override void OnDisconnected( EndPoint endPoint )
    {
        base.OnDisconnected( endPoint );
        Pc?.Post( LeaveWorldJob.Do, Pc );
        ClientSessionManager.Remove( this );
        Log.Info( $"OnDisConnected : { endPoint }" );
    }

    public override void OnSend( int numOfBytes )
    {
    }
}


public partial class ClientSession : IExecutor
{
    public Actor _executor { get; set; } = new();

    public void Execute()
        => _executor.Execute();

    public void PostAfter( TimeSpan afterTime, Action action )
        => _executor.PostAfter( afterTime, action );

    public void PostAfter< T1 >( TimeSpan afterTime, Action< T1 > action, T1 t1 )
        => _executor.PostAfter( afterTime, action, t1 );

    public void Post( Action action ) => _executor.Post( action );

    public void Post< T1 >( Action< T1 > action, T1 t1 )
        => _executor.Post( action, t1 );

    public void Post< T1 >( Action< T1 > action, in T1 t1 )
        => _executor.Post( action, in t1 );

    public void Post< T1, T2 >( Action< T1, T2 > action, T1 t1, T2 t2 )
        => _executor.Post( action, t1, t2 );

    public void Post< T1, T2 >( Action< T1, T2 > action, in T1 t1, in T2 t2 )
        => _executor.Post( action, in t1, in t2 );

    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > action, T1 t1, T2 t2, T3 t3 )
        => _executor.Post( action, t1, t2, t3 );

    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > action, in T1 t1, in T2 t2, in T3 t3 )
        => _executor.Post( action, in t1, in t2, in t3 );

    public void Post< T1, T2, T3, T4 >( Action< T1, T2, T3, T4 > action, T1 t1, T2 t2, T3 t3, T4 t4 )
        => _executor.Post( action, t1, t2, t3, t4 );

    public void Post< T1, T2, T3, T4 >( Action< T1, T2, T3, T4 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4 )
        => _executor.Post( action, in t1, in t2, in t3, in t4 );

    public void Post< T1, T2, T3, T4, T5 >( Action< T1, T2, T3, T4, T5 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5 )
        => _executor.Post( action, t1, t2, t3, t4, t5 );

    public void Post< T1, T2, T3, T4, T5 >( Action< T1, T2, T3, T4, T5 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4, in T5 t5 )
        => _executor.Post( action, in t1, in t2, in t3, in t4, in t5 );

    public void Tick()
    {
        if ( !IsConnected() )
            return;

		/// 일단 폴링을 통해 처리( 디버깅 ), 잡 처리 인터페이스 있음
		_txExecutor.FlushTx();

		PostAfter( new( Consts.Session.TickInterval ), TickJob.DoTick, this );
    }

    public void PostAfter< T1, T2 >( TimeSpan afterTime, Action< T1, T2 > action, T1 t1, T2 t2 )
        => _executor.PostAfter( afterTime, action, in t1, in t2 );
}

public partial class ClientSession : ITxExecutor
{
    /// <summary>
    /// 트랜잭션 실행기
    /// </summary>
    TxExecutor _txExecutor;

    /// <summary>
    /// 유저
    /// </summary>
    public User User { get; set; } = new();
    public Actor TxJobExecutor => _txExecutor.TxJobExecutor;

    public void PostTx( TxContext txContext ) => _txExecutor.PostTx( txContext );

    public void PostTx( TxContext txContext, Action< ETxResult > completionJob )
    {
        txContext._completionJob = completionJob;
        PostTx( txContext );
    }

    public void PopTx() => _txExecutor.PopTx();

    public void ReserveTx() => _txExecutor.ReserveTx();
}
