﻿using System.Net;
using System.Text;

[ViewController]
public static class HomeController
{
    [Route( "/" )]
    public static void HandleTest( HttpListenerContext context )
    {
        var response = context.Response;
        string responseString =
            "<html><body>" +
            "HELLO 안녕하세요, 웹 서버가 동작 중입니다!</br>" +
            "Index</br>" +
            "<a href=\"/npcview?npcId=0\">NpcView</a></br>" +
            "<a href=\"/metrics\">Metrics</a>" +
            "</body></html>";
        var buffer = Encoding.UTF8.GetBytes( responseString );
        response.ContentType = "text/html; charset=UTF-8";
        response.ContentLength64 = buffer.Length;
        response.OutputStream.Write( buffer, 0, buffer.Length );
        response.OutputStream.Close();
    }

    [Route( "/favicon.ico" )]
    public static void HandleFavicon( HttpListenerContext context )
    {
        var response = context.Response;
        string responseString = "<html><body></body></html>";
        byte[] buffer = Encoding.UTF8.GetBytes( responseString );
        response.ContentType = "text/html; charset=UTF-8";
        response.ContentLength64 = buffer.Length;
        response.OutputStream.Write( buffer, 0, buffer.Length );
        response.OutputStream.Close();
    }
}