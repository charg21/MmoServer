﻿using Mala.Collection;
using System.Net;
using System.Text;
using System.Text.Json;

[ViewController]
public static class NpcViewController
{
    [Route( "/npcview" )]
    public static void HandleNpcView( HttpListenerContext context )
    {
        // 쿼리 파라미터 얻기
        var query = context.Request.QueryString;
        string npcIdParam = query[ "npcId" ];
        if ( !ActorId.TryParse( npcIdParam, out var npcId ) )
            npcId = 30001;

        var model = new NpcViewModel
        {
            Npc = GetNpc( new( npcId ) ),
        };

        RestApiService.Global.RunView(
            context,
            $"{ Config.ScriptPath }/View/NpcView.cshtml",
            model );
    }

    private static Npc? GetNpc( ObjectId npcId )
    {
        Npc? npc = null;
        ChannelManager.Instance.ForEach( channel =>
        {
            //if ( !channel.World.ClusterManager.TryGetNpc( npcId, out npc ) )
            if ( !WorldObjectManager.Local.TryGetNpc( npcId, out npc ) )
                return EForEachResult.Continue;

            return EForEachResult.Break;
        } );

        return npc;
    }

}

[ViewController]
public static class NpcController
{
    [Route( "/npc" )]
    public static void HandleNpc( HttpListenerContext context )
    {
        var response = context.Response;

        // JSON 데이터 생성
        var data = new
        {
            Name = "HELLO 안녕하세요, 웹 서버가 동작 중입니다!",
            Id = 1234,
            DesignId = 0,
        };

        // JSON 직렬화
        string jsonResponse = JsonSerializer.Serialize( data );
        var buffer = Encoding.UTF8.GetBytes( jsonResponse );

        // 응답 설정
        response.ContentType = "application/json; charset=UTF-8";
        response.ContentLength64 = buffer.Length;
        response.OutputStream.Write( buffer, 0, buffer.Length );
        response.OutputStream.Close();
    }
}