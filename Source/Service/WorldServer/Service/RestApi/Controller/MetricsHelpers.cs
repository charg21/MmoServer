using System.Text;

internal static class MetricsHelpers
{

    public static void WriteCounter( string name, long value, StringBuilder sb, string helpMsg = "" )
        => WriteMetrics( name, "counter", $"{value}", sb, helpMsg );

    public static void WriteGauge<TLable, Value>( string name, string label, IDictionary<TLable, Value> values, StringBuilder sb, string helpMsg = "" )
    {
        WriteMetricsHeader( name, "gauge", sb, helpMsg );
        foreach ( var (key, value) in values )
            sb.Append( $"{name}{{{label}=\"{key}\"}} {value}\n" );
    }

    public static void WriteGauge( string name, double value, StringBuilder sb, string helpMsg = "" )
        => WriteMetrics( name, "gauge", $"{value}", sb, helpMsg );

    public static void WriteGauge( string name, long value, StringBuilder sb, string helpMsg = "", string label = "" )
        => WriteMetrics( name, "gauge", $"{value}", sb, helpMsg );

    public static void WriteMetrics( string name, string type, string value, StringBuilder sb, string helpMsg = "" )
    {
        WriteMetricsHeader( name, type, sb, helpMsg );
        sb.Append( $"{name} {value}\n" );
    }

    public static void WriteMetricsHeader( string name, string type, StringBuilder sb, string helpMsg = "" )
    {
        // \r\n 줄바꿈을 쓰면 안되서 \n으로 변경
        sb.Append( $"# HELP {name} {helpMsg}\n" );
        sb.Append( $"# TYPE {name} gauge\n" );
    }
}
