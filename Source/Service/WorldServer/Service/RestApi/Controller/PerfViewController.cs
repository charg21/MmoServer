﻿using Mala.Collection;
using Mala.Logging;
using Mala.Net;
using System.Net;
using System.Text;

[ViewController]
public static class PerfViewController
{
    [Route( "/perfview" )]
    public static void HandlePerfView( HttpListenerContext context )
    {
        var model = new PerfViewModel
        {
        };

        RestApiService.Global.RunView(
            context,
            $"{ Config.ScriptPath }/View/PerfView.cshtml",
            model );
    }

    public static long AsMb( long byteSize ) => byteSize / ( 1024 * 1024 );
}
