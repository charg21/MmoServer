﻿using Google.Protobuf.WellKnownTypes;
using Mala.Collection;
using Mala.Db;
using Mala.Logging;
using Mala.Net;
using Mala.Threading;
using System.Net;
using System.Text;


[ViewController]
public static class MetricsController
{
    [Route( "/metrics" )]
    public static void HandleMetrics( HttpListenerContext context )
    {
        var perfLog = GetLastPerfLog();

        var apiBodyBuilder = new StringBuilder( 4096 );
        MetricsHelpers.WriteGauge( "send_buffer_count", perfLog.GetSendBufferCount(), apiBodyBuilder, "송신 버퍼 수량" );
        MetricsHelpers.WriteGauge( "global_job_count", perfLog.GetGlobalJobCount(), apiBodyBuilder, "전역 작업 객체 수" );
        MetricsHelpers.WriteGauge( "npc_count", perfLog.GetNpc(), apiBodyBuilder, "NPC 수" );
        MetricsHelpers.WriteGauge( "ccu", perfLog.GetCcu(), apiBodyBuilder, "현재 유저 수" );
        MetricsHelpers.WriteGauge( "cpu_usage", perfLog.GetCpuUsage(), apiBodyBuilder, "CPU 사용량" );
        MetricsHelpers.WriteGauge( "working_set", perfLog.GetWorkingSet(), apiBodyBuilder, "워킹셋 메모리" );
        MetricsHelpers.WriteGauge( "debug_log_count", perfLog.GetDebugLogCount(), apiBodyBuilder, "디버그 로그 수" );
        MetricsHelpers.WriteGauge( "info_log_count", perfLog.GetInfoLogCount(), apiBodyBuilder, "인포 로그 수" );
        MetricsHelpers.WriteGauge( "critical_log_count", perfLog.GetCriticalLogCount(), apiBodyBuilder, "크리티컬 로그 수" );
        MetricsHelpers.WriteGauge( "error_log_count", perfLog.GetErrorLogCount(), apiBodyBuilder, "에러 로그 수" );
        MetricsHelpers.WriteCounter( "gc_gen0", perfLog.GetGCGen0(), apiBodyBuilder, "GC 0세대" );
        MetricsHelpers.WriteCounter( "gc_gen1", perfLog.GetGCGen1(), apiBodyBuilder, "GC 1세대" );
        MetricsHelpers.WriteCounter( "gc_gen2", perfLog.GetGCGen2(), apiBodyBuilder, "GC 2세대" );
        MetricsHelpers.WriteGauge( $"actor_execute_count_per_thread", "thread_id", ThreadPoolExecutorService.GetMetricsInfos(), apiBodyBuilder, $"스레드별 액터 실행 횟수" );

        RestApiService.Global.Run( context, apiBodyBuilder.ToString() );
    }

    public static PerfLog GetLastPerfLog()
    {
        /// 최근 PerfLog를 가져온다.
        /// 메모리값을 실시간으로 가져와도 되지만, 추후 프로세스가 분리될 가능성이 있으니 DB에서 가져온다.
        PerfLog perfLog = null;
        DbExectuor.Post( ctx =>
        {
            var perfLogDb = new PerfLogDbModel();
            perfLogDb.BindAllField();
            var ( result, list ) = perfLogDb.SelectAll< PerfLog >(
                ref ctx,
                new(){ limit = 1, orderBy = $"id DESC" } );
            if ( result == EDbResult.Fail )
                return false;

            perfLog = list[ 0 ];
            return true;
        } );

        while ( perfLog == null )
            Thread.Sleep( 16 );

        return perfLog;
    }
}

