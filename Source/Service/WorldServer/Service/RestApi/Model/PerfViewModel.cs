﻿
public class PerfViewModel
{
    public int GcGen2Count { get; internal set; }
    public int GcGen1Count { get; internal set; }
    public int GcGen0Count { get; internal set; }
    public GCMemoryInfo GcMemoryInfo { get; internal set; }
    public long GcTotalMemory { get; internal set; }
    public int ThreadCountClrThreadPool { get; internal set; }
    public int ThreadCountWorker { get; internal set; }
    public long SendBufferRturnCount { get; internal set; }
    public long SendBufferRentCount { get; internal set; }
    public long SendBufferNewCount { get; internal set; }
    public long GcAllocBytePerThread { get; internal set; }
}
