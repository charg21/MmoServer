﻿[AttributeUsage( AttributeTargets.Method )]
internal class RouteAttribute : Attribute
{
    /// <summary>
    /// 경로
    /// </summary>
    public string AbsolutePath { get; private set; } = string.Empty;

    public RouteAttribute( string absolutePath )
    {
        AbsolutePath = absolutePath;
    }
}

