﻿using Mala.Logging;
using RazorEngineCore;
using System.Net;
using System.Reflection;
using System.Text;

public partial class RestApiService : Creature
{
    static RestApiService _instance = null;
    public static RestApiService Global => _instance ??= new();

    /// <summary>
    /// Http Listener
    /// </summary>
    HttpListener _listener = new();

    /// <summary>
    /// 라우팅 테이블
    /// </summary>
    private readonly Dictionary< string, Action< HttpListenerContext > > _routes = new();

    /// <summary>
    /// 레이저 엔진
    /// </summary>
    private readonly IRazorEngine _razorEngine = new RazorEngine();

    /// <summary>
    /// csthml 템플릿 캐시
    /// </summary>
    private readonly Dictionary< string, IRazorEngineCompiledTemplate > _templateCache = new();

    public RestApiService()
    {
    }

    /// <summary>
    /// 초기화한다.
    /// </summary>
    public void Initialize()
    {
        List< Type > controllerTypes = [];

        /// 컨트롤러 타입을 찾는다.
        foreach ( var assembly in AppDomain.CurrentDomain.GetAssemblies() )
        {
            foreach ( var type in assembly.GetTypes() )
            {
                if (
                     type.GetCustomAttribute< ViewControllerAttribute >() is null &&
                     type.GetCustomAttribute< ApiControllerAttribute >() is null )
                    continue;

                controllerTypes.Add( type );
            }
        }

        foreach ( var type in controllerTypes )
        {
            /// 라우팅 메서드를 등록한다.
            foreach ( var method in type.GetMethods() )
            {
                var route = method.GetCustomAttribute< RouteAttribute >();
                if ( route is null )
                    continue;

                try
                {
                    RegisterHandler( route, method );
                }
                catch ( System.Exception ex )
                {
                    Console.WriteLine( $"Excepotion Occur!!: {ex.Message}" );
                }
            }
        }
    }

    /// <summary>
    /// 핸들러를 등록한다.
    /// </summary>
    private void RegisterHandler( RouteAttribute route, MethodInfo method )
    {
        _routes.Add(
            route.AbsolutePath.ToLower(),
            method.IsStatic ?
                ( Action< HttpListenerContext > )Delegate.CreateDelegate( typeof( Action< HttpListenerContext > ), method ) :
                ( Action< HttpListenerContext > )Delegate.CreateDelegate( typeof( Action< HttpListenerContext > ), this, method ) );
    }

    public void Start()
    {
        /// 새로운 스레드
        ThreadPool.UnsafeQueueUserWorkItem( _ =>
        {
            try
            {
                _listener.Prefixes.Add( "http://127.0.0.1:9099/" );
                _listener.Start();

                while ( _listener.IsListening )
                {
                    var context = _listener.GetContextAsync().Result;

                    /// RestApiService 스레드에서 Post 사용시 문제가 발생함( ThreadPoolExecutionContext가 생성됨 )
                    /// 작업 실행은 World Worker Thread에서 처리 되도록 예약
                    Post2( Process, context );
                }
            }
            catch ( Exception ex )
            {
                Console.WriteLine( $"Excepotion Occur!!: { ex.Message }" );
            }
        },
        null );
    }

    public void Process( HttpListenerContext context )
    {
        var request = context.Request;
        var url = context.Request.Url;

        // 요청 경로에 따라 라우팅
        if ( !_routes.TryGetValue( url.AbsolutePath.ToLower(), out var handler ) )
        {
            HandleNotFound( context );
            return;
        }

        handler( context );
    }

    public IRazorEngineCompiledTemplate CompileView( HttpListenerContext context, string viewPath )
    {
        var urlPath = context.Request.Url.AbsolutePath.ToLower();

        if ( _templateCache.TryGetValue( urlPath, out var compiledTemplate ) )
            return compiledTemplate;

        var template = File.ReadAllText( viewPath );
        compiledTemplate = _razorEngine.Compile(
            template,
            static builder =>
            {
                //builder.Options.ReferencedAssemblies.Add( Assembly.Load( new AssemblyName( "Microsoft.CSharp" ) ) );
                //builder.Options.ReferencedAssemblies.Add( Assembly.Load( new AssemblyName( "System.Web" ) ) );
                //builder.Options.ReferencedAssemblies.Add( Assembly.Load( new AssemblyName( "System.Web.Helpers" ) ) );
                //builder.Options.ReferencedAssemblies.Add( Assembly.Load( new AssemblyName( "System.Runtime" ) ) );
                //builder.Options.ReferencedAssemblies.Add( Assembly.Load( new AssemblyName( "System.Collections" ) ) );
                //builder.Options.ReferencedAssemblies.Add( typeof( object ).Assembly );
                //builder.Options.ReferencedAssemblies.Add( Assembly.Load( new AssemblyName( "mscorlib" ) ) );
                builder.IncludeDebuggingInfo();
            } );
        _templateCache.Add( urlPath, compiledTemplate );
        return compiledTemplate;
    }

    public void RunView( HttpListenerContext context, string viewPath, object? model )
    {
        var compiledTemplate = CompileView(
            context,
            viewPath );

        var result = compiledTemplate.Run( model );
        var buffer = Encoding.UTF8.GetBytes( result );

        var response = context.Response;
        response.ContentType = "text/html; charset=UTF-8";
        response.ContentLength64 = buffer.Length;
        response.OutputStream.Write( buffer, 0, buffer.Length );
        response.OutputStream.Close();
    }

    public void Run( HttpListenerContext context, string body )
    {
        var buffer = Encoding.UTF8.GetBytes( body );

        var response = context.Response;
        response.ContentType = "text/plain; charset=UTF-8";
        response.ContentLength64 = buffer.Length;
        response.OutputStream.Write( buffer, 0, buffer.Length );
        response.OutputStream.Close();
    }

    public static void HandleNotFound( HttpListenerContext context )
    {
        var response = context.Response;

        // 경로가 없을 경우 404 응답
        Log.Info( $"404 Not Found Path: { context.Request.Url.AbsolutePath }" );
        response.StatusCode = (int)HttpStatusCode.NotFound;
        var buffer = Encoding.UTF8.GetBytes(
            $"404 - Not Found\n" +
            "Available Routes:\n" +
            $"NpcView: {Config.WebHost}npcview?npcId=\n" +
            $"Metrics: {Config.WebHost}metrics" );
        response.ContentLength64 = buffer.Length;
        response.OutputStream.Write( buffer, 0, buffer.Length );
        response.OutputStream.Close();
        response.Close();
    }
}
