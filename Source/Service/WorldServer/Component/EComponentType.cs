
/// <summary>
/// 컴포넌트 타입
/// </summary>
public enum EComponentType : long
{
    /// <summary>
    /// 없음
    /// </summary>
    None = 0,

    /// <summary>
    /// 시야
    /// </summary>
    View = 1 << 0,

    /// <summary>
    /// 전투
    /// </summary>
    Combat = 1 << 1,

    /// <summary>
    /// 가방
    /// </summary>
    Bag = 1 << 2,

    /// <summary>
    /// 채팅
    /// </summary>
    Chat = 1 << 3,

    /// <summary>
    /// 인공능지
    /// </summary>
    Ai = 1 << 4,

    /// <summary>
    /// 장착 컴포넌트
    /// </summary>
    Equipment = 1 << 5,

    /// <summary>
    /// 스탯 컴포넌트
    /// </summary>
    Stat = 1 << 6,

    /// <summary>
    /// 유저
    /// </summary>
    User = 1 << 7,

    /// <summary>
    /// 상호작용
    /// </summary>
    Interaction = 1 << 8,

    /// <summary>
    /// 콜리전 그리드 소유자
    /// </summary>
    CollisionGridOwner = 1 << 9,

    /// <summary>
    /// 최대 값
    /// </summary>
    Max = 1 << 10,

    /// <summary>
    /// PC용 컴포넌트
    /// </summary>
    ForPc = View | Combat | Bag | Chat | Equipment | Stat | User,

    /// <summary>
    /// NPC용 컴포넌트
    /// </summary>
    ForNpc = View | Combat | Stat,

    /// <summary>
    /// 프로젝타일용 컴포넌트
    /// </summary>
    ForProjectile = View | Combat | Stat,

    All = ~1
}
