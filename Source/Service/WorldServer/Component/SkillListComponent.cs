﻿using Mala.Db;

using SkillMap = System.Collections.Generic.Dictionary< DesignId, Skill >;

/// <summary>
/// 스킬 목록 컴포넌트
/// </summary>
public class SkillListComponent : IComponent
{
    protected SkillMap _skillMap = new();

    WorldObject? Owner { get; set; }

    public void AddSkill( ref SkillDesign skillDesign )
    {
        if ( !_skillMap.TryAdd(
            skillDesign.Id,
            new Skill{ Id = skillDesign.Id, Design = SkillDesign.Get( skillDesign.Id ) } ) )
        {
        }
    }

    /// <summary>
    ///
    /// </summary>
    public void RemoveSkill( DesignId skillDisignId )
    {
    }

    public virtual void Init()
    {
        /// 0번 기본공격
        _skillMap.Add( (DesignId)( 0 ), new BaseAttack() );
        _skillMap.Add( (DesignId)( 1 ), new Heal() );
        _skillMap.Add( (DesignId)( 2 ), new DotHeal() );
    }

    /// <summary>
    /// 스킬 보유 여부를 반환한다.
    /// </summary>
    public bool HasSkill( DesignId skillDisignId )
    {
        return _skillMap.ContainsKey( skillDisignId );
    }

    /// <summary>
    /// 스킬 획득 시도한다.
    /// </summary>
    public bool TryGetSkill( DesignId skillDisignId, out Skill skill )
    {
        return _skillMap.TryGetValue( skillDisignId, out skill );
    }

    /// <summary>
    ///
    /// </summary>
    public bool CanUse( DesignId skillInfoId )
    {
        return true;
    }

    /// <summary>
    /// 아이템을 추가한다.
    /// </summary>
    public void Clear()
    {
        _skillMap.Clear();
    }

    public void ExportToSkillList( ref List< PktSkill > skills )
    {
        skills.Capacity = skills.Capacity + _skillMap.Count;
        foreach ( var skill in _skillMap.Values )
        {
            skills.Add( new PktSkill
            {
                _skillDesignId = skill.Design.Ref.Id.AsPrimitive(),
            } );
        }
    }

    public void ExportToSkillIdList( ref List< uint > skillDesignIdList )
    {
        skillDesignIdList.Capacity = skillDesignIdList.Capacity + _skillMap.Count;

        foreach ( var skill in _skillMap.Keys )
        {
            skillDesignIdList.Add( skill.AsPrimitive() );
        }
    }

    public void OnBind()
    {
        Init();
    }

    public void OnUnbind()
    {
    }

}

/// <summary>
/// Pc 스킬 목록 컴포넌트
/// </summary>
public class PcSkillListComponent : SkillListComponent, IDbLoadComponent
{
    Pc _owner;

    public PcSkillListComponent( Pc owner )
    {
        _owner = owner;
    }

    public EDbResult LoadFromDb( ref DbExecutionContext dbContext, TxContext tx )
    {
        //var forSelect = new SkillDbModel();
        //forSelect.SetKeyOwnerId( Owner.GetId() );
        //forSelect.BindAllField();
        //var skillList = skill.SelectMany< Skill >( dbContext, dbContext.Connection );

        //tx.AddDbReplyJob( ( txResult ) =>
        //{
        //    if ( txResult != ETxResult.Ok )
        //        return;
        //
        //    Clear();
        //    foreach ( var skill in skillList )
        //        AddSkill( skill );
        //} );

        return EDbResult.Success;
    }

}

/// <summary>
/// Npc 스킬 목록 컴포넌트
/// </summary>
public class NpcSkillListComponent : SkillListComponent
{
    Npc _owner;

    public NpcSkillListComponent( Npc owner )
    {
        _owner = owner;
    }
}