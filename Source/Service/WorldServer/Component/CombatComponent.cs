using Mala.Core;
using Mala.Logging;
using Mala.Math;
using Mala.Net;
using Org.BouncyCastle.Asn1.X509;
using System.Collections.Generic;
using static Consts;
using SkillDict = System.Collections.Generic.Dictionary< DesignId, Skill >;


/// <summary>
/// 복합 사유일 수 도 있으니 플래그
/// </summary>
public enum EDeadReason
{
    None = 0,
    Hit  = 1 << 0,

    Max
}
public interface ICombatComponent : IComponent, ITickComponent
{
    public ELogicResult StartSkill( Design< SkillDesign > skillDesign, ObjectId targetId );

    /// <summary>
    /// 실린더( 공격 처리 용 )
    /// </summary>
    public Cylinder Cylinder { get; }
}

/// <summary>
/// 액터 전투 컴포넌트
///
/// 전투의 액션은 스킬을 통해서 처리( 평타도 스킬 )
/// </summary>
public abstract partial class CombatComponent : ICombatComponent
{
    /// <summary>
    /// 소유자
    /// </summary>
    WorldObject Owner { get; }

    /// <summary>
    /// 시전 정보 문맥( 현재 한개, 멀티 캐스팅 ㄴㄴ )
    /// </summary>
    CastingContext Context = new();

    /// <summary>
    /// 쿨타임 목록
    /// </summary>
    Dictionary< DesignId, long > _coolTimeList = new();

    /// <summary>
    /// 스킬 목록 컴포넌트
    /// </summary>
    SkillListComponent SkillList => Owner.SkillList!;

    /// <summary>
    /// 스탯 컴포넌트
    /// </summary>
    StatComponent Stat => Owner.Stat!;

    /// <summary>
    /// 시야 컴포넌트
    /// </summary>
    ViewComponent View => Owner.View!;

    /// <summary>
    /// 실린더( 공격 처리 용 )
    /// </summary>
    public Cylinder Cylinder => new Cylinder( Owner.Movement.Pos, 0.1f );

    /// <summary>
    /// 생성자
    /// </summary>
    public CombatComponent( WorldObject owner )
    {
        Owner = owner;
    }

    /// <summary>
    /// 캐스팅 가능 여부를 반환한다.
    /// </summary>
    bool CanCast( in DesignId skillDesignId )
    {
        /// 이미 캐스팅 중이면
        if ( DoingCast )
            return false;

        /// 쿨타임이면
        if ( _coolTimeList.ContainsKey( skillDesignId ) )
            return false;

        /// ETC...

        return true;
    }

    public virtual bool CanHit()
    {
        /// 대상이 살아있는지 체
        return IsAlive;
    }

    /// <summary>
    /// 캐스팅중 여부를 반환한다.
    /// </summary>
    bool DoingCast => Context._doingSkill is not null;

    /// <summary>
    /// 스킬을 시작한다.
    /// </summary>
    public ELogicResult StartSkill( Design< SkillDesign > skillDesign, ObjectId targetId )
    {
        if ( skillDesign is null )
            return ELogicResult.InvalidDesignId;

        ref var skillDesignRef = ref skillDesign.Ref;
        if ( !CanCast( skillDesignRef.Id ) )
            return ELogicResult.AlreadyDoingCast;

        if ( !SkillList.TryGetSkill( skillDesignRef.Id, out var skill ) )
            return ELogicResult.UnownedSkill;

        /// 타겟팅 타입은 선처리로 미리 체크( 스킬이 발동까지 타겟팅 유지 )
        /// 대상이 없으면 스킬 시작 실패 처리
        if ( skillDesignRef.TargetingType == ESkillTargeting.Target )
        {
            SkillHelper.GatherTargetList(
                Context._targetList,
                Owner,
                targetId,
                null,
                in skillDesignRef.HitList[ 0 ] );
            if ( Context._targetList.Count is 0 )
                return ELogicResult.TargetNotInView; /// 기획상 시야내에서만 처리함
        }

        //var args = new StartSkillArgs()
        //{
        //    Caster  = caster,
        //    CastPos = caster.Pos,
        //    CastDir = caster.Dir,
        //    Target = target,
        //    Design = Design!,
        //};

        /// 컨텍스트에 저장
        Context._doingSkill = skill;
        Context._startTick = DateTime.Now.Ticks;
        Context._endTick = Context._startTick + skillDesignRef.CastingTime;
        /// 쿨타임 리스트 갱신
        _coolTimeList.Add( skillDesignRef.Id, Context._endTick );

        /// 히트 리스트의 첫번째의 캐스팅 타임이 0이면 바로 실행해야함
        var result = Context.Tick( Owner, new() );
        if ( Context._doingSkill.IsEnd() )
            Context.Reset();

        if ( !result.IsSuccess() )
        {
            Context.Reset();
            return result;
        }

        return ELogicResult.Success;
    }

    public void DoCompleteSkill()
    {
        //Context.Skill!.OnComplete();
        Context.Reset();
    }

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public void OnTick( TimeSpan deltaTime )
    {
        RefreshCoolTimeList( deltaTime );

        if ( !DoingCast )
            return;

        //Context.Skill!.Tick( Owner, deltaTick );
        Context.Tick( Owner, deltaTime );
        if ( Context._doingSkill.IsEnd() )
            DoCompleteSkill();
    }

    private void RefreshCoolTimeList( TimeSpan deltaTick )
    {
        var now = DateTime.Now;

        var removeList = new FixedSizeList2< DesignId >( stackalloc DesignId[ 32 ] );
        foreach ( var ( id, endTime )  in _coolTimeList )
        {
            if ( endTime <= now.Ticks )
                removeList.Add( id );
        }

        foreach ( var id in removeList )
        {
            _coolTimeList.Remove( id );
        }
    }

    /// <summary>
    /// 데미지를 주다
    /// </summary>
    public void TakeDamage( WorldObject caster, DesignId skillDesignId, i64 damage )
    {
        /// 1. 자신의 HP를 감소
        /// 2. 자신의 OnDamaged 콜백
        /// 3. 자신의 HP가 0인 경우 OnDead 콜백
        var curHp = DecreaseHp( damage );
        OnDamaged( caster, damage );
        if ( curHp <= 0 )
            Die( caster, EDeadReason.Hit );

        Log.Info( $"Hit! CasterId: {caster.Id} => TargetId: {Owner.Id} Damge: {damage} curHp: {curHp}" );

        View.Send( PacketWriter.FromS_Hit( caster.Id, Owner.Id, skillDesignId, damage, curHp ) );
    }

    public void Die( WorldObject killer, EDeadReason reason )
    {
        /// HP 0으로 가정, 사망 처리
        OnDead( killer, reason );

        /// 킬러의 OnKill 콜백 호출
        killer.Post( killer.Combat.OnKill, Owner );

        /// 자신의 사망, 월드에서 본인 제거
        LeaveWorldJob.OnLeave( Owner );

        /// 타이머로 부활 처리 예약
        Owner.PostAfter( new( 5000 ), Revive );
    }

    public virtual void OnDead( WorldObject killer, EDeadReason reason )
    {
    }


    public void Revive()
    {
        Owner.Movement.ToPos = Owner.Movement.Pos;

        /// 이미 사망 처리 된 상태에서 호출 콜백
        OnRevive();

        /// HP 회복, 회복이 진행되어야 상호 작용 가능( 최대한 지연 )
        IncreaseHp( 10 ); // Consts.Stat.MaxHp );

        /// 월드 입장
        EnterWorldJob.Enter( Owner, Owner.World.ChannelId );
    }

    public virtual void OnRevive()
    {
    }

    public void OnKill( WorldObject target ) { }

    public void OnHit( WorldObject caster ) { }


    public long Heal( WorldObject target, long heal )
    {
        return IncreaseHp( heal );
    }

    public long DecreaseHp( long damage )
        => Stat.Decrease( EStatType.Hp, damage, 0 );

    public long IncreaseHp( long damage )
        => Stat.Increase( EStatType.Hp, damage, Consts.Stat.MaxHp );

    public bool IsAlive => Stat.Get( EStatType.Hp ) > 0;

    public virtual void OnDamaged( WorldObject caster, long damage )
    {
        Log.Info( $"Caster.ObjectId {caster.Id}" );
    }

    public void OnBind()
    {
    }

    public void OnUnbind()
    {
    }

}

/// <summary>
/// Pc 전투 컴포넌트
/// </summary>
public class PcCombatComponent : CombatComponent
{
    public Pc Pc { get; init; }

    public PcCombatComponent( Pc owner ) : base( owner )
    {
        Pc = owner;
    }

}

/// <summary>
/// Npc 전투 컴포넌트
/// </summary>
public class NpcCombatComponent : CombatComponent
{
    public Npc Npc { get; init; }
    public NpcFsmAi Ai => Npc.Ai;

    public NpcCombatComponent( Npc owner ) : base( owner )
    {
        Npc = owner;
    }

    public override void OnDead( WorldObject killer, EDeadReason reason )
    {
        Ai.OnDead( killer, reason );
        Log.Info( $"I'm Dead.. My ObjectId {Npc.Id} Killer.ObjectId {killer.Id}" );

        if ( Npc.Design is null )
            return;

        Random.Shared.Next( 0, 10000 );

        //// TEST 보상 지급
        ref var npcDesign = ref Npc.Design.Ref;
        npcDesign.LootDropRate = (BasisPoint)5000;
        if ( FastRand.Next( 10_000 ) < (long)npcDesign.LootDropRate )
        {
            var pc = killer as Pc;
            pc?.Post(
                GiveItemTx.Run,
                pc,
                new GiveItemTx.GiveParam()
                {
                    Source = EItemSource.Loot,
                    ItemDesignId = (DesignId)100 /// 포션
                } );
        }
    }

    public override void OnRevive()
    {
        Log.Info( $"Revive My ObjectId {Npc.Id}" );

        Npc.Movement.Pos = Npc.ReturnPos;

        Ai.OnRevive();
    }

    public override void OnDamaged( WorldObject caster, long damage )
    {
        Log.Info( $"Caster.ObjectId {caster.Id}" );

        Ai.OnDamaged( caster, damage );
    }
}
