using Mala.Db;
using Mala.Logging;
using ItemDict = System.Collections.Generic.Dictionary< ulong, Item_ >;

public partial class BagComponent : IComponent, IDbLoadComponent
{
    /// <summary>
    /// 임시 상수
    /// </summary>
    public const int DefaultBagCapacity = 100;

    /// <summary>
    /// 소유자
    /// </summary>
    Pc Owner;

    /// <summary>
    /// 아이템 컬렉션
    /// </summary>
    ItemDict _itemDict = new();

    /// <summary>
    /// 생성자
    /// </summary>
    public BagComponent( Pc owner )
    {
        Owner = owner;
    }

    /// <summary>
    /// 아이템을 추가한다.
    /// </summary>
    public void AddItem( Item_ item )
    {
#if RELEASE
        if ( ItemDesign.Get( (DesignId)item.GetDesignId() ) is null )
        {
            Log.Critical( $"Item Add Failed.... DesignId[ {item.GetDesignId()} ] OnwerId[ {item.GetOwnerId()} ]" );
            return;
        }
#endif
        if ( !_itemDict.TryAdd( item.GetId(), item ) )
            Log.Critical( $"Item Add Failed.... DesignId[ { item.GetId() } ] OnwerId[ { item.GetOwnerId() } ]" );
    }

    /// <summary>
    /// 아이템을 획득한다.
    /// </summary>
    public bool TryGetItemByDesignId( DesignId designId, out Item_ item )
    {
        foreach ( var ( _, i ) in _itemDict )
        {
            if ( i.GetDesignId() == designId )
            {
                item = i;
                return true;
            }
        }

        item = null;
        return false;
    }

    /// <summary>
    /// 초기 상태로 되돌린다.
    /// </summary>
    public void Reset()
    {
        _itemDict.Clear();
    }

    /// <summary>
    /// 아이템을 로드한다.
    /// </summary>
    public EDbResult LoadFromDb( ref DbExecutionContext dbContext, TxContext tx )
    {
        var item = new ItemDbModel();
        item.SetKeyOwnerId( (ulong)Owner.Id );
        item.BindAllField();

        /// 아이템을 로드한다.
        var itemList = item.SelectMany< Item_ >( ref dbContext );

        tx.OnSuccessThen( () =>
        {
            Reset();
            ImportFromList( itemList );
        } );

        return EDbResult.Success;
    }

    private void ImportFromList( List< Item_ > itemList )
    {
        foreach ( var item in itemList )
            AddItem( item );
    }

    internal void ExportToItemList( ref ValueS_ListItem sItemList )
    {
        ExportToItemList( ref sItemList._items );
    }

    public void ExportToItemList( ref ValueList< PktItem > itemList )
    {
        itemList.Capacity = _itemDict.Count;

        foreach ( var ( _, item ) in _itemDict )
        {
            ref var pktItem = ref itemList.ReserveOneItem();
            pktItem._id = item.GetId();
            pktItem._count = item.GetCount();
            pktItem._designId = item.GetDesignId();
        }
    }

    internal bool HssItem( ulong itemId )
    {
        return _itemDict.ContainsKey( itemId );
    }

    internal bool TryGetItem( ulong itemId, out Item_ item )
    {
        return _itemDict.TryGetValue( itemId, out item );
    }

    /// <summary>
    /// 아이템 삽입전 사전 체크
    /// </summary>
    public ELogicResult CheckGive( in GiveItemTx.GiveParam args )
    {
        if ( _itemDict.Count + args.Count >= DefaultBagCapacity )
        {
            Log.Critical( $"Bag is Full. BagCapacity[ {DefaultBagCapacity} ]" );
            return ELogicResult.NotEnoughBagSpace;
        }

        return ELogicResult.Success;
    }

    /// <summary>
    /// 아이템 누적 가능 여부
    /// </summary>
    public bool CanStack( DesignId itemDesginId )
    {
        ref var itemDesign = ref ItemDesign.Ref( itemDesginId );
        if ( itemDesign.IsNull )
        {
            Log.Critical( $"ItemDesign is Null. ItemDesignId {itemDesginId}" );
            return false;
        }

        if ( itemDesign.IsStackable )
            return true;

        if ( !TryGetItemByDesignId( itemDesginId, out var item ) )
            return false;

        return true;
    }

}

/// <summary>
/// 가방 미구현 인터페이스
/// </summary>
public partial class BagComponent : IComponent
{
    public void OnBind() {}
    public void OnUnbind() {}
}
