﻿using Mala.Collection;
using Mala.Db;

/// <summary>
/// 스탯 컴포넌트
/// </summary>
public partial class StatComponent : IComponent
{
    /// <summary>
    /// 스탯 배열 캐시( 외부에서 참조 )
    /// </summary>
    protected long[] _statCache = new long[ (short)( EStatType.Max ) ];

    /// <summary>
    /// 스탯 배열
    /// </summary>
    protected long[] _stat = new long[ (short)( EStatType.Max ) ];

    /// <summary>
    /// 외부에서 접근하는 스레드 세이프한 스탯 정보
    /// </summary>
    public float MoveSpeed => (float)( _stat[ (short)EStatType.Speed ] );
    public int Hp => (int)_stat[ (short)EStatType.Hp ];
    public int Power => (int)_stat[ (short)EStatType.Power ];

    public ref long Get( EStatType statType )
    {
        return ref _stat[ (short)( statType ) ];
    }

    public void Set( EStatType statType, long value )
    {
        _stat[ (short)( statType ) ] = value;
    }

    public virtual ref long Ref( EStatType statType )
    {
        return ref _stat[ (short)( statType ) ];
    }

    public void Init()
    {
        Get( EStatType.Hp ) = Consts.Stat.MaxHp;
        Get( EStatType.Power ) = Consts.Stat.DefaultPower;
    }

    public long Increase( EStatType statType, long value, long maxValue )
    {
        ref var curValue = ref Get( statType );

        curValue = Math.Min( maxValue, curValue + value );

        return curValue;
    }

    public long Decrease( EStatType statType, long value, long minValue = 0 )
    {
        ref var curValue = ref Get( statType );

        curValue = Math.Max( minValue, curValue - value );

        return curValue;
    }

    /// <summary>
    /// 스킬 보유 여부를 반환한다.
    /// </summary>
    public bool Has( EStatType statType )
    {
        return true;// _StatMap.ContainsKey( StatInfoId );
    }

    /// <summary>
    /// 초기 상태로 되돌린다.
    /// </summary>
    public void Reset()
    {
    }

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public virtual void Refresh()
    {
    }

    public void ExportToStatList( ref List< PktStat > stats )
    {
        stats.Capacity = _statCache.Length;
        int index = 0;
        foreach ( var stat in _statCache )
        {
            ref var statItem =  ref stats.ReserveOneItem();
            statItem._type = (EStatType)( index );
            statItem._value = (int)_stat[ (int)( stat ) ];
        }
    }
}

/// <summary>
/// Pc 스탯
/// </summary>
public partial class PcStatComponent : StatComponent, IDbLoadComponent
{
    /// <summary>
    /// 생성자
    /// </summary>
    public PcStatComponent( Pc owner )
    {
        Owner = owner;
    }

    Pc Owner { get; init; }

    public EDbResult LoadFromDb( ref DbExecutionContext dbContext, TxContext tx )
    {
        var stat = new StatDbModel();
        stat.SetKeyPcId( (ulong)Owner.Id );
        stat.BindAllField();
        var statList = stat.SelectMany< StatDbModel >( ref dbContext );
        if ( statList.Count == 0 )
            InsertDefaultStat( ref dbContext, statList );

        tx.OnSuccessThen( () =>
        {
            Reset();
            statList.ForEach( stat => Set( stat ) );
        } );

        return EDbResult.Success;
    }

    void Set( StatDbModel stat )
    {
        Set( (EStatType)stat.GetType(), (long)stat.GetValue() );
    }

    public void InsertDefaultStat( ref DbExecutionContext dbCtx, in List<StatDbModel> statList )
    {
        var hp = new StatDbModel();
        hp.SetKeyPcId( (ulong)Owner.Id );
        hp.SetType( (ulong)EStatType.Hp );
        hp.SetValue( 10 );
        hp.Insert( dbCtx );

        var speed = new StatDbModel();
        speed.SetKeyPcId( (ulong)Owner.Id );
        speed.SetType( (ulong)EStatType.Power );
        speed.SetValue( 20 );
        speed.Insert( dbCtx );

        var power = new StatDbModel();
        power.SetKeyPcId( (ulong)Owner.Id );
        power.SetType( (ulong)EStatType.Power );
        power.SetValue( 5 );
        power.Insert( dbCtx );

        statList.Add( hp );
        statList.Add( speed );
        statList.Add( power );
    }
    public void InsertDefaultStat( ref DbExecutionContext dbCtx, TxContext tx )
    {
        var hp = tx.Add< StatDbModel >();
        hp.SetKeyPcId( (ulong)Owner.Id );
        hp.SetType( (ulong)EStatType.Hp );
        hp.SetValue( 10 );

        var speed = tx.Add< StatDbModel >();
        speed.SetKeyPcId( (ulong)Owner.Id );
        speed.SetType( (ulong)EStatType.Power );
        speed.SetValue( 20 );

        var power = tx.Add< StatDbModel >();
        power.SetKeyPcId( (ulong)Owner.Id );
        power.SetType( (ulong)EStatType.Power );
        power.SetValue( 5 );
    }
}

/// <summary>
/// Npc 스탯
/// </summary>
public partial class NpcStatComponent : StatComponent
{
    /// <summary>
    /// 생성자
    /// </summary>
    public NpcStatComponent( Npc owner )
    {
        Set( EStatType.Speed, (int)Globals.NpcMoveSpeed );
        Set( EStatType.Speed, (int)Globals.NpcMoveSpeed );

        Owner = owner;
    }

    Npc Owner { get; init; }

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public override void Refresh()
    {
        if ( Owner.Design is null )
            return;

        ref var npcDesgin = ref Owner.Design.Ref;
        Get( EStatType.Speed ) = (int)npcDesgin.WalkSpeed;
        Get( EStatType.Hp ) = (int)npcDesgin.Hp;
        Get( EStatType.Power ) = (int)npcDesgin.Power;
    }
}


/// <summary>
/// 스탯 미구현 인터페이스
/// </summary>
public partial class StatComponent : IComponent
{
    public void OnBind() { }
    public void OnUnbind() { }
}