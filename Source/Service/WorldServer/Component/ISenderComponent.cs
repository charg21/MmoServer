﻿
using Mala.Net;

public interface ISenderComponent
{
    /// <summary>
    /// 패킷을 전송한다.
    /// </summary>
    public void Send( SendSegment segment );
    public void Send( ref SendSegment segment );
}


public class PcSenderComponent : ISenderComponent
{
    Pc _pc;

    public PcSenderComponent( Pc pc )
    {
        _pc = pc;
    }

    public void Send( SendSegment segment ) => _pc.Send( ref segment );

    public void Send( ref SendSegment segment ) => _pc.Send( ref segment );
}
