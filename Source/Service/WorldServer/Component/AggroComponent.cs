﻿
using System.ComponentModel;

public interface IAggro
{
    /// public WeakReference< Pc > Pc { get; }
    public ulong Value { get; }
}


/// <summary>
/// 기본 어그로
/// </summary>
public struct Aggro : IAggro
{
    public ulong Value { get; set; }
}

/// <summary>
/// 어그로 전파
/// </summary>
public struct SpreadAggro : IAggro
{
    public ulong Value { get; set; }
}

public class AggroComponent : IComponent
{
    Npc _owner;

    public AggroComponent( Npc owner )
    {
        _owner = owner;
    }

    public void AddAggro( Aggro aggro )
    {
    }

    public void OnBind()
    {
    }

    public void OnUnbind()
    {
    }
}
