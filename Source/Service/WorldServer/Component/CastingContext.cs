public abstract partial class CombatComponent
{
    /// <summary>
    /// 스킬 스탯 캐싱( 스킬 시전시점 기준으로 캐싱 )
    /// </summary>
    public class SkillStatCache
    {
        int lv;
        int hp;
        int power;
        int critical;
    }

    /// <summary>
    /// 스킬 시전 문맥
    /// </summary>
    public class CastingContext
    {
        /// <summary>
        /// 예약된 스킬
        /// </summary>
        public Skill? _reservedSkill;

        /// <summary>
        /// 실행중인 스킬
        /// </summary>
        public Skill? _doingSkill;

        /// <summary>
        /// 시작 시간
        /// </summary>
        public long _startTick;

        /// <summary>
        /// 종료 시간
        /// </summary>
        public long _endTick;

        /// <summary>
        /// 시전 위치
        /// </summary>
        Position _castPos { get; set; }

        /// <summary>
        /// 현재 타격 정보 목록 인덱스
        /// </summary>
        public int _hitListIndex;

        long _duration = 0;

        /// <summary>
        /// 이미 피격된 액터 목록
        /// </summary>
        HashSet< WorldObject > _alreadyHitObjects = [];

        /// <summary>
        /// 대상 목록
        /// </summary>
        public List< WorldObject >? _targetList { get; set; } = [];

        public CastingContext()
        {
        }

        public void Reset()
        {
            _doingSkill = null;
            _startTick = 0;
            _endTick = 0;
            _hitListIndex = 0;
            _alreadyHitObjects.Clear();
            _targetList.Clear();
        }

        /// <summary>
        /// 매 프레임 틱을 돈다. (타이머 대신, 일단 Tick 처리)
        /// </summary>
        public ELogicResult Tick( WorldObject owner, TimeSpan time )
        {
            /// 틱을 더하고
            _duration += time.Ticks;

            ref var skillDesign = ref _doingSkill.Design.Ref;
            var startHitIndex = _hitListIndex;

            /// 저장된 히트 리스트 부터 순회
            for ( ; _hitListIndex < skillDesign.HitList.Length; )
            {
                /// 타겟을 구함
                ref var hit = ref skillDesign.HitList[ _hitListIndex ];

                /// 시작틱이 안되었다면 종료
                if ( _duration < hit.StartTick )
                    return ELogicResult.Success;

                WorldObject taregt = null;
                if ( _targetList.Count > 0 )
                    taregt = _targetList[ 0 ];

                /// 피격시, 타겟팅이면 이미 대상을 타겟팅중, 논타겟팅이면 타겟 식별자가 필요 없음.
                var targetList = SkillHelper.GatherTargetList( owner, new(), taregt, in hit );
                if ( targetList.Count is 0 )
                {
                    switch ( hit.Targeting )
                    {
                        case ESkillTargeting.Target: return ELogicResult.TargetNotInView;
                        case ESkillTargeting.Self:   return ELogicResult.InvalidDesignId; // 다른 에러 코드 추가해야함.
                        case ESkillTargeting.Range:
                            _hitListIndex += 1; /// 아직 처리 안함
                            continue;
                        default:
                            return ELogicResult.InvalidDesignId;
                    }
                }

                var skillDesignId = skillDesign.Id;
                var targetingType = skillDesign.TargetingType;
                targetList.ForEach(
                    target =>
                    {
                        /// 피격 가능 여부 체크, 불가능한 경우 패스
                        if ( !target.Combat.CanHit() )
                            return;

                        /// 스킬 데미지 비동기 처리시
                        /// 피격 처리시 시전자의 위치와 너무 멀어져서 대상이 안보이는데 공격이 들어온 케이스가 발생 가능
                        /// 피격자 입장에서 의문의 공격이 갑자기 들어온거처럼 보일 수 있음( 예외 처리 필요할지 논의 사항 )
                        ///
                        /// 예시, 텔레포트중에 이동시 발생 가능하여, 텔레포트 끝난후 바로 맞은거처럼 처리 될 수 잇음
                        /// 시전자와 피격자 모두 컨텍스트 스위칭, 스레더 처리 밀림 등 다양한 사유로 부분 지연이 발생 가능
                        if ( targetingType == ESkillTargeting.Range ) { }
                        // Do Something... ;

                        /// 대상에게 데미지를 줌
                        target.Post( target.Combat.TakeDamage, owner, skillDesignId, 50L );
                    } );

                _hitListIndex += 1;
            }

            if ( IsComplete( skillDesign, startHitIndex ) )
            {
                //Reset();
            }

            return ELogicResult.Success;
        }

        private bool IsComplete( SkillDesign skillDesign, int startHitIndex )
        {
            return
                startHitIndex != _hitListIndex &&
                _hitListIndex == skillDesign.HitList.Length;
        }
    }

}
