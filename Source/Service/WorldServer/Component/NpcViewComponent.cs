﻿/// <summary>
/// Npc 시야 컴포넌트
/// </summary>
public class NpcViewComponent : ViewComponent
{
    Npc _npc;

    /// <summary>
    /// 생성자
    /// </summary>
    public NpcViewComponent( Npc owner ) : base( owner, EWorldObjectType.Pc )
    {
        _npc = owner;
    }

    override public void OnEnter( WorldObject other )
    {
        if ( other.Type is EWorldObjectType.Npc )
            return;

        TryAdd( other );

        other.Post( ViewJob.DoEnter, other, _npc, Movement.Pos );
    }

    public override void OnLeave( WorldObject other )
    {
        if ( other.Type is EWorldObjectType.Npc )
            return;

        // 내 시야에서 상대 제거
        TryRemove( other.Id );

        other.Post( ViewJob.DoLeave, other, _npc.Id );
    }

    public override void OnEnterSomeOne( WorldObject actor, Position otherPos )
    {
        base.OnEnterSomeOne( actor, otherPos );

        if ( _npc.IsAwaken() )
            return;

        _npc.RegisterTick( Consts.Npc.TickInterval );
    }

    public override void OnMove( WorldObject other )
    {
        /// NPC는 기존 시야에 NPC가 들어올일이 없어서 NPC 체크는 패스
        other.Post( ViewJob.DoMove, other, _npc.Id, in Movement.Pos, Movement.Dir, in Movement.ToPos );
    }
}

