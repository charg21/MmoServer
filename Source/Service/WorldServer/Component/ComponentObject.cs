using Mala.Collection;
using Mala.Db;
using System.Numerics;


/// <summary>
/// 컴포넌트 액터
/// </summary>
public class ComponentObject : Creature
{
    /// <summary>
    /// 생성자
    /// </summary>
    public ComponentObject()
    {
        //var capacity = BitOperations.TrailingZeroCount( (int)EComponentType.Max );
        //_array = new IComponent[ capacity ];

        // 리플렉션으로 멤버 필드중에 IDBLoadComponent를 상속받은 컴포넌트를 찾아서 배열에 저장한다.
    }

    IDbLoadComponent[] _dbLoadComponents;

    /// <summary>
    /// 소유한 Enum Flag
    /// </summary>
    EComponentType _ownFlag;

    /// <summary>
    /// Enum Flag에 대응하는 값
    /// </summary>
    IComponent[] _array;

    /// <summary>
    /// 컴포넌트 보유 여부를 반환한다.
    /// </summary>
    public bool HasComponent( EComponentType componentType ) => !_ownFlag.HasFlag( componentType );

    /// <summary>
    /// 컴포넌트 인덱스로 변환한다.
    /// </summary>
    public static int ToIndex( EComponentType componentType ) => BitOperations.TrailingZeroCount( (long)( componentType ) );

    /// <summary>
    /// 컴포넌트 반환을 시도한다.
    /// </summary>
    public bool TryGetComponent( EComponentType componentType, out IComponent value )
    {
        if ( !HasComponent( componentType ) )
        {
            value = null;
            return false;
        }
        var index = ToIndex( componentType );
        value = _array[ index ];
        return true;
    }

    public bool TryRemoveComponent( EComponentType componentType )
    {
        if ( !HasComponent( componentType ) )
            return false;

        _ownFlag &= ~componentType; // 플래그 제거

        var index = ToIndex( componentType );
        _array[ index ]?.OnUnbind();
        _array[ index ] = null;
        return true;
    }

    public bool TryBindComponent( EComponentType componentType, IComponent component )
    {
        if ( component is null )
            return false;

        if ( HasComponent( componentType ) )
            return true;

        var index = ToIndex( componentType );
        _ownFlag |= componentType; // 플래그 추가
        _array[ index ] = component;
        component.OnBind();
        return false;
    }

    public void Reset()
    {
        _ownFlag = EComponentType.None;
        _array.Clear();
    }

    public EDbResult LoadComponentFromDb( ref DbExecutionContext dbContext, TxContext tx )
    {
        foreach ( var component in _array )
        {
            if ( component is null )
                continue;

            var dbLoadComponent = component as IDbLoadComponent;
            if ( dbLoadComponent is null )
                continue;

            /// Bag, Equipment, SKill, Quest... Etc..
            var result = dbLoadComponent.LoadFromDb( ref dbContext, tx );
            if ( result == EDbResult.Fail )
                return result;
        }

        return EDbResult.Success;
    }

    public void ForEachComponenet( Action< IComponent > componenetJob )
    {
        foreach ( var component in _array )
        {
            if ( component is null )
                continue;

            componenetJob( component );
        }
    }
}
