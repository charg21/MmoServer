﻿using Mala.Math;


/// <summary>
/// 충돌 격자 소유자
/// 격자 기반 충돌 처리를 위함
/// </summary>
public class CollisionGridOwnerComponent : IComponent, IWorldObjectComponent
{
    /// <summary>
    /// 월드 액터
    /// </summary>
    WorldObject _owner;

    /// <summary>
    /// 충돌 격자 인덱스
    /// </summary>
    public Vector2Int GridIndex { get; set; } = new();

    public void OnBind()
    {
    }

    public void OnUnbind()
    {
    }

    public void OnEnterWorld()
    {
    }

    public void OnLeaveWorld()
    {
    }

}