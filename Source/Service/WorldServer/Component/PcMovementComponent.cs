﻿

/// <summary>
/// 시야 컴포넌트 인터페이스
/// </summary>
/// <summary>
/// PC 이동 컴포넌트
/// </summary>
public class PcMovementComponent : MovementComponent
{
    /// <summary>
    /// 소유자
    /// </summary>
    Pc _pc;

    /// <summary>
    /// 생성자
    /// </summary>
    public PcMovementComponent( Pc owner )
    {
        _pc = owner;
    }

    public override void OnMove()
    {
        _pc.Send( PacketWriter.FromS_Move( _pc ) );
    }
}
