﻿using Mala.Collection;
using Mala.Db;

public interface IEquipmentComponent : IComponent, IDbLoadComponent
{
    void ExportToEquipList( ref List< PktEquip > equips );
}

public enum EEquipmentSlot
{
    Body,
    Weapon,
    Accessory,

    Max
}

/// <summary>
/// Pc 장비 컴포넌트
/// </summary>
public partial class PcEquipComponent : IEquipmentComponent
{
    /// <summary>
    /// 소유자
    /// </summary>
    Pc _owner;

    readonly EquipSlot[] _equipSlotList = new EquipSlot[ (int)EEquipmentSlot.Max ];

    /// <summary>
    /// 가방
    /// </summary>
    public BagComponent Bag => _owner.Bag;

    /// <summary>
    /// 생성자
    /// </summary>
    public PcEquipComponent( Pc owner )
    {
        _owner = owner;
    }

    /// <summary>
    /// DB로 부터 로드한다.
    /// </summary>
    public EDbResult LoadFromDb( ref DbExecutionContext dbContext, TxContext tx )
    {
        var equipSlot = new EquipSlotDbModel();
        equipSlot.SetKeyOwnerId( (ulong)_owner.Id );
        equipSlot.BindAllField();

        /// 장착 정보를 로드한다.
        var equipSlotList = equipSlot.SelectMany< EquiptSlot2 >( ref dbContext );

        tx.OnSuccessThen( () =>
        {
            Reset();
            equipSlotList.ForEach( equipSlot => Equip( equipSlot ) );
        } );

        return EDbResult.Success;
    }

    public bool IsEquipped( ulong id )
    {
        foreach ( var item in _equipSlotList )
        {
            if ( item.GetItemId() == id )
                return true;
        }

        return false;
    }

    bool IsEquippedByDesignId( DesignId itemDesignId )
    {
        return false;
    }

    void Reset()
    {
        _equipSlotList?.Clear();
    }

    void Equip( EquiptSlot2 equipSlot )
    {
        /// 가방에 있는지( 종속성이 걸림 )
        if ( !Bag.TryGetItem( equipSlot.GetItemId(), out var item ) )
            return;

        equipSlot.Item = item;
        _equipSlotList[ equipSlot.GetSlot() ] = equipSlot;
    }

    public void ImportFromList()
    {

    }

    public void ExportToEquipList( ref List< PktEquip > equips )
    {
        equips.Capacity = _equipSlotList.Count();

        foreach ( var equipSlot in _equipSlotList )
        {
            if ( equipSlot is null )
                continue;

            var pktEquiup = new PktEquip();
            pktEquiup._slot   = equipSlot.GetSlot();
            pktEquiup._itemId = equipSlot.GetItemId();
            equips.Add( pktEquiup );
        }
    }

    public void OnBind()
    {
    }

    public void OnUnbind()
    {
    }

}

public partial class EquiptSlot2 : EquipSlot
{
    public object Item { get; internal set; }
}

