﻿

/// <summary>
/// 시야 컴포넌트 인터페이스
/// </summary>
public class MovementComponent : IComponent, ITickComponent
{
    public MovementComponent()
    {
    }

    public virtual void OnMove()
    {
    }

    public void SetPos( Position toPos, Direction dir )
    {
        FromPos = Pos;
        Pos = toPos;
        Dir = dir;

        OnMove();
    }

    /// <summary> 도착 여부를 반환한다. </summary>
    public bool IsArrived => Pos == ToPos;

    /// <summary> 거의 도착했는지 여부를 반환한다. </summary>
    public bool IsNearlyArrived => Pos == ToPos;

    /// <summary>
    /// 좌표
    /// </summary>
    public Position Pos = new();

    /// <summary>
    /// 방향
    /// </summary>
    public Direction Dir { get; set; } = new();

    /// <summary>
    /// 이전 좌표
    /// </summary>
    public Position FromPos { get; set; } = new();

    /// <summary>
    /// 목표 좌표
    /// </summary>
    public Position ToPos = new();

    public void OnEnterWorld()
    {
        FromPos = Pos;
    }

    public void OnBind() {}

    public void OnUnbind() {}

    public void OnTick( TimeSpan deltaTick )
    {
    }

}
