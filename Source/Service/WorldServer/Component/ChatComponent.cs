﻿public class ChatComponent : IComponent
{
    Pc _pc;

    public ViewComponent View => _pc.View;

    ISenderComponent Sender => _pc.Sender!;

    /// <summary>
    /// 생성자
    /// </summary>
    public ChatComponent( Pc owner )
    {
        _pc = owner;
    }

    public void Say( string message )
    {
        var chat = PacketWriter.FromS_Chat( _pc.Name, message );

        Sender!.Send( chat );
        View!.Send( chat );
    }

    public void OnBind()
    {
    }

    public void OnUnbind()
    {
    }

}
