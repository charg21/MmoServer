﻿[AttributeUsage( AttributeTargets.Field )]
public class BindComponent : Attribute
{
    public BindComponent()
    {
    }
}

[AttributeUsage( AttributeTargets.Class )]
public class DbLoadComponent : Attribute
{
}
