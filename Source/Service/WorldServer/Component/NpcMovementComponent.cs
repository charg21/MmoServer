﻿/// <summary>
/// Npc 이동 컴포넌트
/// </summary>
public class NpcMovementComponent : MovementComponent
{
    /// <summary>
    /// 소유자
    /// </summary>
    Npc _npc;

    /// <summary>
    /// 생성자
    /// </summary>
    public NpcMovementComponent( Npc owner )
    {
        _npc = owner;
    }
}
