
/// <summary>
/// Pc 시야 컴포넌트
/// </summary>
public class PcViewComponent : ViewComponent
{
    Pc _pc;

    /// <summary>
    /// 생성자
    /// </summary>
    public PcViewComponent( Pc owner ) : base( owner, EWorldObjectType.Max )
    {
        _pc = owner;
    }

    public override void OnEnter( WorldObject other )
    {
        TryAdd( other );

        _pc.Send( PacketWriter.FromS_Spawn( other ) );

        other.Post( other.View.OnEnterSomeOne, _pc, Movement.Pos );
    }

    public override void OnLeave( WorldObject other )
    {
        // 내 시야에서 상대 제거
        TryRemove( other.Id );

        _pc.Send( PacketWriter.FromS_Despawn( other.Id ) );

        other.Post( ViewJob.DoLeave, other, _pc.Id );
    }

    public override void OnMoveSomeOne( ObjectId otherId, Position pos, Direction dir, Position toPos )
    {
        _pc.Send( PacketWriter.FromS_Move( otherId, in pos, dir, in toPos ) );
    }

    public override void OnLeaveSomeOne( ObjectId otherId )
    {
        // 퇴장 처리 후 패킷을 보내준다.
        if ( TryRemove( otherId ) )
            _pc.Send( new ValueS_Despawn() { _objectId = (uint)otherId }.Write() );
    }

    public override void OnEnterSomeOne( WorldObject other, Position otherPos )
    {
        base.OnEnterSomeOne( other, otherPos );

        _pc.Send( PacketWriter.FromS_Spawn( other, otherPos ) );
    }

}

