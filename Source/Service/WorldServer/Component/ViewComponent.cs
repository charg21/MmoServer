﻿using Mala.Math;
using Mala.Net;

/// <summary>
/// 시야 컴포넌트
/// </summary>
public abstract class ViewComponent : IComponent, IWorldObjectComponent
{
    WorldObject _owner;
    WideClusterLayer WideClusterManager => _owner.World.ClusterManager.WideClusterLayer;
    ClusterLayer ClusterManager => _owner.World.ClusterManager.ClusterLayer;
	public MovementComponent Movement => _owner.Movement!;

	/// <summary>
	/// 시야 대상 타입
	/// Max: Pc:1 Npc: 1
	/// Pc:  Pc:1 Npc: 0
	/// Npc: Pc:0 Npc: 1
	/// </summary>
	EWorldObjectType _viewType = EWorldObjectType.Max;

    public ViewComponent( WorldObject owner, EWorldObjectType gatherObject )
    {
        _worldObjects = new( Consts.View.DefaultViewCapacity );
        _owner = owner;
        _viewType = gatherObject;
    }

    /// <summary>
    /// 시야내 오브젝트 목록
    /// </summary>
    public ViewList2 _worldObjects { get; init; }

    /// <summary>
    /// 수집용 시야 목록
    /// </summary>
    public static List< ActorMap > ListForGather
    {
        get
        {
            var viewList = ViewListForGatherInternal.Value!;
            viewList.Clear();
            return viewList;
        }
    }

    public static ThreadLocal< List< ActorMap > > ViewListForGatherInternal { get; set; } = new( () => new( Consts.View.DefaultViewGatherListCapacity ) );

    /// <summary>
    /// 복사용 시야 목록
    /// </summary>
    public static ViewList ViewListForSnapshot
    {
        get
        {
            var viewList = ViewListForSnapshotInternal.Value!;
            viewList.Clear();

            return viewList;
        }
    }

    public static ThreadLocal< ViewList > ViewListForSnapshotInternal { get; set; } = new( () => new( Consts.View.DefaultViewGatherListCapacity ) );

    public void Send( SendSegment sendSegment )
    {
        Send( ref sendSegment );
    }

    public void Send( ref SendSegment sendSegment )
    {
        foreach ( var worldObj in _worldObjects.AsReadonlySpan() )
        {
            worldObj.Sender?.Send( ref sendSegment );
        }
    }

	public bool TryAdd( WorldObject other )
        => _worldObjects.TryAdd( other.Id, other );

    public bool TryRemove( ObjectId otherId )
        => _worldObjects.Remove( otherId );

    public bool TryGet( ObjectId otherId, out WorldObject @object )
        => _worldObjects.TryGetValue( otherId, out @object );

    public int Count => _worldObjects.Count;
    public bool Empty => _worldObjects.Count == 0;

    /// <summary>
    /// 현재 시야의 스냅샷을 반환한다.
    /// </summary>
    public ViewList GetViewSnapshot()
    {
        var viewListSnapshot = ViewListForSnapshot;

        foreach ( var worldObj in _worldObjects.AsReadonlySpan() )
            viewListSnapshot.Add( worldObj.Id, worldObj );

        return viewListSnapshot;
    }

    /// <summary>
    /// 범위내 액터를 모은다.
    /// </summary>
    public List< ActorMap > GatherWorldObjects()
        => ClusterManager.GatherWorldObjects(
            _owner.Movement.Pos,
            _viewType );


    /// <summary>
    /// 시야를 갱신한다.
    /// </summary>
    public void Rebuild()
    {
        var enterViewMapList = GatherWorldObjects();
        var viewSnapshot     = GetViewSnapshot();

        // 이동 및 시야에 등장한 플레이어 처리
        var myActorId = _owner.Id;
        foreach ( var enterViewMap in enterViewMapList )
        {
            foreach ( var ( id, other ) in enterViewMap )
            {
                if ( viewSnapshot.Remove( id ) )
                {
                    OnMove( other ); // 기존 시야에 존재
                }
                else
                {
                    if ( myActorId != id )
                        OnEnter( other ); // 기존 시야에 없던 경우
                }
            }
        }

        // 시야에서 벗어난 플레이어 처리
        foreach ( var ( _, other ) in viewSnapshot )
            OnLeave( other );

        viewSnapshot.Clear();
    }

    public void OnEnterWorld()
    {
        Rebuild();
    }

    public void OnLeaveWorld()
    {
        var curViewList = GetViewSnapshot();

        foreach ( var ( _, otherObject ) in curViewList )
            OnLeave( otherObject );

        curViewList.Clear();
    }

    public virtual void OnMove( WorldObject other )
    {
        if ( other.Type == EWorldObjectType.Npc )
            return;

        other.Post( ViewJob.DoMove, other, _owner.Id, in Movement.Pos, Movement.Dir, in Movement.ToPos );
    }

    public void ForEach( Action< WorldObject > value )
    {
        foreach ( var worldObj in _worldObjects.AsReadonlySpan() )
            value( worldObj );
    }

    public void OnBind()
    {
    }

    public void OnUnbind()
    {
    }

    public virtual void OnEnter( WorldObject other )
    {
    }

    public virtual void OnLeave( WorldObject other )
    {
    }

    public virtual void OnMoveSomeOne( ObjectId otherId, Position pos, Direction dir, Position toPos )
    {
    }


    public virtual void OnLeaveSomeOne( ObjectId otherId )
        => TryRemove( otherId );

    public virtual void OnEnterSomeOne( WorldObject other, Position otherPos )
        => TryAdd( other );

}

