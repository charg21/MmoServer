﻿using Mala.Common.Ai;

/// <summary>
/// 인공 능지 컴포넌트 인터페이스
/// </summary>
public interface IAiComponent : IComponent, IWorldObjectComponent, ITickComponent
{
}

/// <summary>
/// Npc 인공능지 컴포넌트
/// </summary>
public class NpcAiComponent : IAiComponent
{
    Npc _owner;

    public NpcAiComponent( Npc owner )
    {
        _owner = owner;
    }

    public void OnBind()
    {
    }

    public void OnUnbind()
    {
    }

    public void OnEnterWorld()
    {
    }

    public void OnLeaveWorld()
    {
    }

    public virtual void OnTick( TimeSpan deltaTime )
    {
    }

    virtual public bool IsBusy => true;

    /// <summary>
    /// 이벤트 드리븐 기반 인터페이스
    /// </summary>
    public void OnRecvEvent( int @event )
    {
    }

    public virtual void OnDead( WorldObject killer, EDeadReason reason )
    {
    }

    public virtual void OnDamaged( WorldObject caster, long damage )
    {
    }

    public virtual void OnRevive()
    {
    }
}

public class NpcFsmAiComponent : NpcAiComponent
{
    NpcFsmAi _fsm;

    public NpcFsmAiComponent( Npc owner ) : base( owner )
    {
        _fsm = new( owner );
    }

    public bool IsBusy => _fsm.IsBusy;

    public override void OnTick( TimeSpan deltaTime )
    {
        _fsm.Tick( deltaTime );
    }

    public virtual void OnRevive()
    {
        _fsm.Transit( ENpcAiState.Idle );
    }
}

public class NpcBehaviorTreeAiComponent : NpcAiComponent
{
    /// <summary>
    /// 행동 트리
    /// </summary>
    public BehaviorTree BehaviorTree;

    public NpcBehaviorTreeAiComponent( Npc owner ) : base( owner )
    {
    }

    public override void OnTick( TimeSpan deltaTime )
    {
    }
}

public class NpcPageBehaviorTreeAiComponent : NpcAiComponent
{
    /// <summary>
    /// 페이지 기반 행동 트리
    /// </summary>
    PageBehaviorTree PageBehaviorTree;

    public NpcPageBehaviorTreeAiComponent( Npc owner ) : base( owner )
    {
    }

}