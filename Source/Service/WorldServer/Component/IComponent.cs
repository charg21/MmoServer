﻿using Mala.Db;

/// <summary>
/// 컴포넌트 인터페이스
/// </summary>
public interface IComponent
{
    /// <summary>
    /// 컴포넌트 부착시 호출되는 콜백
    /// </summary>
    void OnBind();

    /// <summary>
    /// 컴포넌트 제거시 호출되는 콜백
    /// </summary>
    void OnUnbind();
}

/// <summary>
/// Tick 컴포넌트
/// </summary>
public interface ITickComponent
{
    /// <summary>
    /// 갱신시 호출되는 콜백
    /// </summary>
    void OnTick( TimeSpan deltaTime );
}

/// <summary>
/// 월드 상호 작용 컴포넌트 인터페이스
/// </summary>
public interface IWorldObjectComponent
{
    /// <summary>
    /// 월드 입장시 호출되는 콜백
    /// </summary>
    public void OnEnterWorld();

    /// <summary>
    /// 월드 퇴장시 호출되는 콜백
    /// </summary>
    public void OnLeaveWorld();
}

/// <summary>
/// Db 로드 컴포넌트 인터페이스
/// </summary>
public interface IDbLoadComponent
{
    /// <summary>
    /// DB로부터 로드
    /// </summary>
    public EDbResult LoadFromDb( ref DbExecutionContext dbContext, TxContext txContext );
}


/// <summary>
/// 리셋 컴포넌트
/// </summary>
public interface IResetComponent
{
}