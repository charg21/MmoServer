﻿using Mala.Common.Ai;

public interface INpcFsmEvent
{
}

/// <summary>
/// NPC 유한 상태 머신
/// </summary>
public class NpcFsmAi : Fsm< Npc, ENpcAiState, NpcState >
{
    /// <summary>
    /// 생성자
    /// </summary>
    public NpcFsmAi( Npc owner )
    {
        Owner = owner;

        Add( ENpcAiState.Idle, new NpcIdleState( owner ) );
        Add( ENpcAiState.Chase, new NpcChaseState( owner ) );
        Add( ENpcAiState.Return, new NpcReturnState( owner ) );
        Add( ENpcAiState.Combat, new NpcCombatState( owner ) );

        Transit( ENpcAiState.Idle );
    }

    public bool IsBusy => CurState.State == ENpcAiState.Return;

    public void OnRecvEvent( INpcFsmEvent fsmEvent )
    {
    }

    public void OnDamaged( WorldObject caster, long damage ) => CurState.OnDamaged( caster, damage );

    public void OnDead( WorldObject killer, EDeadReason reason ) => CurState.OnDied( killer );

    public void OnRevive()
    {
    }

}
