﻿using Mala.Common.Ai;
using Mala.Logging;
using Org.BouncyCastle.Asn1.X509;
using System.Collections.Frozen;

/// <summary>
/// 추적 상태
/// </summary>
public class NpcChaseState : NpcState
{
    public NpcChaseState( Npc owner )
    {
        State = ENpcAiState.Chase;
        Owner = owner;
    }

    public override void OnEnter()
    {
        Log.Info( "Enter Chase State" );
    }

    public override void OnTick( TimeSpan deltaTick )
    {
        // 타겟 생존 여부 체크
        if ( Owner.Target is null || !Owner.Target.Combat.IsAlive )
        {
            Owner.Target = null;
            Fsm.Transit( ENpcAiState.Return );
            return;
        }


        // 최대 추적 거리 체크
        var movement = Owner.Movement;
        var distanceSqrt = ( movement.Pos - Owner.Target.Movement.Pos ).SizeSquared();
        if ( distanceSqrt > ( Consts.Npc.MaxChaseDistance * Consts.Npc.MaxChaseDistance ) )
        {
            Owner.Target = null;
            Fsm.Transit( ENpcAiState.Return );
            return;
        }

        MoveToTarget( deltaTick );
    }

    public override void OnDied( WorldObject killer )
    {
        // Fsm.Transit( ENpcAiState.Return );
        return;
    }

    public override void TransitIfNeeded()
    {
        // 시야에 아무도 없어진다면...
        if ( Owner.Target is null )
        {
            Fsm.Transit( ENpcAiState.Return );
            return;
        }

        // 타겟에 가까워진 경우 공격 상태로 전환
        // if ( 타겟에 가까워진 경우 공격 상태로 전환 )
        // {
        //     Fsm.Transit( ENpcAiState.Combat );
        //     return;
        // }
    }

    void MoveToTarget( TimeSpan deltaTick )
    {
        var ( nextPos, dir ) = WorldHelper.GetNextPos( Owner.Movement.Pos, Owner.Target.Movement.Pos, deltaTick );

        Owner.SetPos( nextPos, dir );
    }

    bool SetupTarget()
    {
        /// 이미 시야에 있는 경우 근처 범위안에 있음
        Owner.View.ForEach( obj =>
        {
            /// 임의로 첫번째 대상을 타겟으로 선정, 이후에 정책이 추가될때 변경
            /// 일단 더티 리드 허용.. 틱마다 검사를 다시 할예정
            if ( !obj.Combat.IsAlive )
                return;

            if ( obj.Type != EWorldObjectType.Pc )
                return;

            Owner.Target = obj as Pc;
        } );

        return false;
    }
}
