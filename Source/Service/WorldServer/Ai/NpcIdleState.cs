﻿using Mala.Core;


/// <summary>
///
/// </summary>
public class NpcIdleState : NpcState
{
    public NpcIdleState( Npc owner )
    {
        State = ENpcAiState.Idle;
        Owner = owner;
    }

    public override void OnTick( TimeSpan deltaTick )
    {
        if ( Owner.Stat.MoveSpeed <= 0.1f )
            return;

        var movement = Owner.Movement;
        if ( movement.IsArrived )
            movement.ToPos = WorldHelper.GetNextPos(
                movement.Pos,
                (EDirection)( FastRand.Next() % 4 ),
                Globals.NpcAiMoveDistance );

        Roam( deltaTick );
    }

    private void Roam( TimeSpan deltaTick )
    {
        var ( nextPos, dir ) = WorldHelper.GetNextPos(
            Owner.Movement.Pos,
            Owner.Movement.ToPos,
            deltaTick,
            Owner.Stat.MoveSpeed );

        Owner.SetPos( nextPos, dir );
    }

    public override void OnDamaged( WorldObject caster, long damage )
    {
        Owner.Target = caster as Pc;
        Owner.Movement.ToPos = caster.Movement.Pos;
        Fsm.Transit( ENpcAiState.Chase );
    }

    public override void TransitIfNeeded()
    {
        /// 시야에 플레이어가 존재하는 경우
        // if ( Owner.View.List.Any() )
        //     Fsm.Transit( ENpcAiState.Chase );
    }
}
