﻿/// <summary>
/// Npc Ai 상태 열거형
/// </summary>
public enum ENpcAiState
{
    Idle,   /// 일반
    Chase,  /// 추적
    Return, /// 복귀
    Combat, /// 전투
}
