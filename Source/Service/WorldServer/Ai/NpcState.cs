﻿using Mala.Common.Ai;

public class NpcState : FsmState< Npc, ENpcAiState >
{
    public virtual void OnDamaged( WorldObject caster, long damage )
    {
    }

    public virtual void OnDied( WorldObject killer )
    {
    }

}
