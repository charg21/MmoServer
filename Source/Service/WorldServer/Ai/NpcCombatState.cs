﻿/// <summary>
/// 전투 상태
/// </summary>
public class NpcCombatState : NpcState
{
    public ViewComponent View => Owner.View;
    public CombatComponent Combat => Owner.Combat;

    /// <summary>
    /// 생성자
    /// </summary>
    public NpcCombatState( Npc owner )
    {
        State = ENpcAiState.Combat;
        Owner = owner;
    }

    public override void OnEnter()
    {
    }

    public override void OnTick( TimeSpan deltaTime )
    {
        var target = Owner.Target;

        if ( target is null || !target.Combat.IsAlive || !View.TryGet( target.Id, out var _ ) )
        {
            ResetTarget();
            Fsm.Transit( ENpcAiState.Return );
            return;
        }

        StartSkillJob.Do( Owner, target.Id, (DesignId)1 );
    }

    public void ResetTarget()
    {
        Owner.Target = null;
    }

    public override void OnExit()
    {
        ResetTarget();
    }

}
