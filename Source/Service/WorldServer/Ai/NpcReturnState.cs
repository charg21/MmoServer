﻿using Mala.Common.Ai;
using Mala.Core;
using Mala.Logging;
using Mala.Math;

/// <summary>
/// 복커 상태
/// </summary>
public class NpcReturnState : NpcState
{
    Vector3 ReturnPos;

    public NpcReturnState( Npc owner )
    {
        State = ENpcAiState.Return;
        Owner = owner;
    }

    public override void OnEnter()
    {
        Owner.Movement.ToPos = Owner.ReturnPos;
        Log.Info( "Enter ReturnState" );
    }

    public override void OnTick( TimeSpan deltaTime )
    {
        MoveToReturnPos( deltaTime );
        Owner.WakeUpState = EWakeUpState.Awaken;
    }

    private void MoveToReturnPos( TimeSpan deltaTime )
    {
        var movement = Owner.Movement;

        var ( nextPos, direction ) = WorldHelper.GetNextPos( movement.Pos, Owner.ReturnPos, deltaTime );
        Owner.SetPos( nextPos, direction );
    }

    public override void TransitIfNeeded()
    {
        var movement = Owner.Movement;
        if ( Owner.ReturnPos == movement.Pos )
        {
            Log.Info( "Enter Return => Idle" );

            Fsm.Transit( ENpcAiState.Idle );
            return;
        }
    }
}