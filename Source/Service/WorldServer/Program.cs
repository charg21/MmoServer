﻿using Mala.Core;
using Mala.Logging;
using System.Net;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;

Config.Load( args, "ServiceConfig" );
ExceptionHandler.Initialize();
LogManager.Initialize( "WorldServer" );

var services = new ServiceCollection();
services.AddTransient( _ => new IPEndPoint( IPAddress.Parse( "127.0.0.1" ), 18888 ) );
services.AddTransient< SessionFactory >( _ => ClientSessionManager.Generate );
services.AddTransient( provider =>
{
    var endPoint = provider.GetRequiredService< IPEndPoint >();
    var sessionFactory = provider.GetRequiredService< SessionFactory >();

    return new WorldServer2(
        endPoint,
        sessionFactory,
        1000 );
} );
var serviceProvider = services.BuildServiceProvider();
var worldServer = serviceProvider.GetService< WorldServer2 >();
worldServer.Initialize();
worldServer.Start();
