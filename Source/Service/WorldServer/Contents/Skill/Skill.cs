﻿using Mala.Logging;


/// <summary>
///
/// </summary>
public class SkillStage
{
}

/// <summary>
/// 스킬 파라미터
/// </summary>
public struct StartSkillParam
{
    /// <summary>
    /// 시전자
    /// </summary>
    public WorldObject? Caster;

    /// <summary>
    /// 대상
    /// </summary>
    public WorldObject? Target;

    /// <summary>
    /// 스킬 기획 데이터
    /// </summary>
    public Design< SkillDesign > Design;

    /// <summary>
    /// 시전 위치
    /// </summary>
    public Position CastPos;

    /// <summary>
    /// 시전 방향
    /// </summary>
    public Direction CastDir;

    /// <summary>
    /// 시전 시간
    /// </summary>
    public long StartTime;
}

/// <summary>
/// 스킬
/// </summary>
public class Skill
{
    /// <summary>
    /// 스킬 식별자
    /// </summary>
    public DesignId Id { get; set; } = new DesignId();

    /// <summary>
    /// 스킬 정보
    /// </summary>
    public Design< SkillDesign > Design { get; set; }

    /// <summary>
    /// 대상을 공격할수 있는지 체크
    /// </summary>
    public virtual bool IsInDistance( Position targetPos )
    {
        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public virtual ELogicResult Start( WorldObject caster, WorldObject target )
    {
        return ELogicResult.Success;
        //var param = new StartSkillParam()
        //{
        //    Caster  = caster,
        //    CastPos = caster.Pos,
        //    CastDir = caster.Dir,
        //    Target = target,
        //    Design = Design!,
        //};
        ///// 히트 리스트의 첫번째의 캐스팅 타임이 0이면 바로 실행해야함
        //return Tick( caster, 0 );
    }
    public void Reset()
    {
    }

    internal bool IsEnd()
    {
        return true;
    }
}


/// <summary>
/// 평타
/// </summary>
public class BaseAttack : Skill
{
    /// <summary>
    /// 생성자
    /// </summary>
    public BaseAttack()
    {
        Design = SkillDesign.Get( new DesignId( 0 ) );
    }

    /// <summary>
    ///
    /// </summary>
    public override ELogicResult Start( WorldObject caster, WorldObject target )
    {
        if ( target is null )
            return ELogicResult.NullPc;

        if ( !target.Combat.IsAlive )
            return ELogicResult.AlreadyDeadPc;

        var damage = caster.Stat.Get( EStatType.Power );
        target.Post( target.Combat.TakeDamage, caster, Id, damage );

        return ELogicResult.Success;
    }
}


/// <summary>
/// 힐
/// </summary>
public class Heal : Skill
{
    /// <summary>
    /// 생성자
    /// </summary>
    public Heal()
    {
        Design = SkillDesign.Get( (DesignId)( 1 ) );
    }

    /// <summary>
    ///
    /// </summary>
    public override ELogicResult Start( WorldObject caster, WorldObject target )
    {
        if ( target is null )//|| !target.IsAlive() )
            return ELogicResult.NullPc;

        return ELogicResult.Success;
    }
}

/// <summary>
/// 힐
/// </summary>
public class DotHeal : Skill
{
    /// <summary>
    /// 발동 횟수
    /// </summary>
    public int _fireCount = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public DotHeal()
    {
        Design = SkillDesign.Get( (DesignId)( 2 ) );
    }

    /// <summary>
    ///
    /// </summary>
    public override ELogicResult Start( WorldObject caster, WorldObject target )
    {
        if ( target is null || !target.Combat.IsAlive )
            return ELogicResult.NullPc;

        return ExecuteAndRegister( caster, target );
    }

    private ELogicResult ExecuteAndRegister( WorldObject caster, WorldObject target )
    {
        //var curHp = caster.Combat.Heal( target, Design.Ref.Value );
        //if ( curHp >= Consts.Stat.MaxHp )
        //    return ELogicResult.Success;

        //_fireCount += 1;
        //if ( _fireCount >= Design.Ref.Count )
        //    return ELogicResult.Success;

        //caster.PostAfter( 1000, caster.Combat.StartSkill, target, this );

        return ELogicResult.Success;
    }
}
