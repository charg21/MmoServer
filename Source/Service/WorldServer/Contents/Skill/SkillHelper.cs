﻿using Mala.Logging;
using Mala.Math;
using static Consts;
using Vector3 = Mala.Math.Vector3;

/// <summary>
/// 스킬 타겟팅 헬퍼
/// </summary>
public static class SkillHelper
{
    /// <summary>
    ///
    /// </summary>
    private static ThreadLocal< List< WorldObject > > LTargetListInternal = new( () => new( 4096 ) );

    /// <summary>
    ///
    /// </summary>
    public static List< WorldObject > SharedTargetList
    {
        get
        {
            var targetList = LTargetListInternal.Value!;
            targetList.Clear();
            return targetList;
        }
    }

    /// <summary>
    /// 대상을 수집한다.
    /// </summary>
    public static List< WorldObject > GatherTargetList(
           WorldObject  caster,
           ObjectId     targetId,
           WorldObject? target,
        in SkillHit     skillHit )
    {
        var list = SharedTargetList;
        GatherTargetList( list, caster, targetId, target, in skillHit );
        return list;
    }

    /// <summary>
    /// 대상을 수집한다.
    /// </summary>
    public static void GatherTargetList(
           List< WorldObject > targetList,
           WorldObject  caster,
           ObjectId     targetId,
           WorldObject? target,
        in SkillHit     skillHit )
    {
        switch ( skillHit.Targeting )
        {
            case ESkillTargeting.Self:
                targetList.Add( caster );
                break;

            case ESkillTargeting.Target:
                {
                    if ( target is not null )
                        targetList.Add( target );
                    else if ( targetId != 0 && caster.View.TryGet( targetId, out target ) ) // 의도상 시야내에서만 처리함
                        targetList.Add( target );
                    break;
                }

            case ESkillTargeting.Range:
                GatherTargetListForRange( caster, in skillHit, in targetList );
                break;
        }
    }

    /// <summary>
    /// 범위 내 타겟을 모은다.
    /// </summary>
    private static void GatherTargetListForRange(
           WorldObject  caster,
        in SkillHit    skillHit,
        in ActorList   targetList )
    {
        if ( skillHit.Shape == EHitShape.None )
        {
            Log.Error( "Error Design Data" );
            return;
        }

        /// 타격 범위를 생성
        HitShape hitShape = HitShapeBuilder.Build( caster, skillHit );

        /// 타격 범위 기반 논타겟 처리
        foreach ( var actor in caster.View!._worldObjects )
        {
            if ( !actor.Combat.IsAlive )
                continue;

            var targetCylinder = actor.Combat.Cylinder;
            if ( IsOverlapped( in skillHit, in hitShape, in targetCylinder ) )
                targetList.Add( actor );
        }
    }


    /// <summary>
    /// 충돌 여부를 반환한다.
    /// </summary>
    private static bool IsOverlapped(
        in SkillHit skillHit,
        in HitShape hitShape,
        in Cylinder targetCylinder )
    {
        switch ( skillHit.Shape )
        {
            case EHitShape.Circle:
                var distanceSquared = MathHelper.GetDistance2DSquard( in targetCylinder.center, in hitShape.Circle.center );

                /// 두 원의 반지름의 합의 제곱 계산
                var radiusSum        = targetCylinder.radius + hitShape.Circle.radius;
                var radiusSumSquared = radiusSum * radiusSum;

                /// 충돌 여부 판별
                if ( distanceSquared >= radiusSumSquared )
                    return false;
                break;

            case EHitShape.Rect:
                break;

            case EHitShape.Triangle:
                break;

            case EHitShape.Donut:
                break;

        }
        return true;
    }
}


public static class HitShapeBuilder
{
    public static HitShape Build( WorldObject caster, SkillHit skillHit )
    {
        var hitShape = new HitShape();
        switch ( skillHit.Shape )
        {
            case EHitShape.Circle:
                /// 각도를 라디안으로 변경( 미리 세팅해도 될듯 )
                var dir            = caster.Movement.Dir.ToVector3();
                var angleInRadians = MathHelper.Deg2Rad( skillHit.Angle );

                var newDir         = MathHelper.Rotate( ref dir, angleInRadians );
                var pos            = newDir * skillHit.Distance;

                hitShape.Circle = new Circle( pos, skillHit.HitShapeData.Circle.radius );
                break;

            case EHitShape.Triangle:
                break;

            case EHitShape.Rect:
                break;

            case EHitShape.Donut:
                break;

            case EHitShape.None:
                break;
        }

        return hitShape;
    }

}
