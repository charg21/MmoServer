﻿using Mala.Core;
using Mala.Db;
using Mala.Logging;


/// <summary>
/// 스킬 배움
/// </summary>
public static class LearnSkillTx
{
    /// <summary>
    ///
    /// </summary>
    public static Action< ClientSession, ActorId, string, TxContext? > Learn;

    static LearnSkillTx()
    {
        Pc pc = new Pc();
        Learn = Execute;
    }

    public static void Execute( ClientSession session, ActorId userId, string token, TxContext? tx = null )
    {
        tx ??= TxContext.New();

        session.PostTx( tx, txResult =>
        {
            SendResult( session, txResult );
        } );
    }


    /// <summary>
    /// 조건을 체크한다.
    /// </summary>
    private static ELogicResult CheckCondition(
        PcDbModel   pcDb,
        UserDbModel userDb )
    {
        return ELogicResult.Success;
    }

    private static void SendResult( ClientSession session, ETxResult result )
    {
    }
}
