﻿using Mala.Logging;


public static class StartSkillJob
{
    public static Action< WorldObject, ObjectId, DesignId > Do = Execute;

    public static void Execute( WorldObject caster, ObjectId targetId, DesignId skillDesignId )
    {
        var preCheckResult = PreCheckBeforeMotion( caster );
        if ( !preCheckResult.IsSuccess() )
        {
            caster.Sender?.Send( PacketWriter.FromS_StartSkill( preCheckResult ) );
            return;
        }

        var startSkillResult = caster.Combat.StartSkill( SkillDesign.Get( skillDesignId ), targetId );
        if ( !startSkillResult.IsSuccess() )
        {
            caster.Sender?.Send( PacketWriter.FromS_StartSkill( startSkillResult ) );
            return;
        }

        caster.View!.Send( PacketWriter.FromS_StartSkill( (ObjectId)caster.Id, (ObjectId)targetId, (DesignId)skillDesignId, 0, 0 ) );
    }

    private static ELogicResult PreCheckBeforeMotion( WorldObject caster )
    {
        /// 컴포넌트가 없는 경우 로직 에러
        if ( caster.Combat is null )
            return ELogicResult.NullCombat;

        /// 스킬 목록 컴포넌트가 없는 경우 로직 에러
        if ( caster.SkillList is null )
            return ELogicResult.NullSkillList;

        //if ( !caster.IsAlive )
        //    return ELogicResult.AlreadyDeadPc;

        return ELogicResult.Success;
    }
}
