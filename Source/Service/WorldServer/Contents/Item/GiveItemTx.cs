﻿
using Mala.Db;
using Mala.Logging;
using MySqlX.XDevAPI.Common;
using System.Runtime.InteropServices;

enum ERewardType : byte
{
    Item,
    Gold,
    Max
}

/// <summary>
/// 아이템 출처 타입
/// </summary>
public enum EItemSource : byte
{
    /// <summary>
    /// NPC 루트
    /// </summary>
    Loot,


    Max
}


/// <summary>
/// 아이템 ( 아이템을 지급하는 공용 로직 )
/// </summary>
public static partial class GiveItemTx
{
    /// <summary>
    /// 지급 인자
    /// </summary>
    public struct GiveParam
    {
        public EItemSource Source = EItemSource.Max;
        public DesignId ItemDesignId = (DesignId)0;
        public int Count = 0;

        public GiveParam()
        {
        }
    }
    public static void Run( Pc pc, GiveParam param )
    {
        var tx = TxContext.New();
        var items = new GiveItemList( param.Count );

        var result = Give( tx, pc, items, param );
        if ( result != ELogicResult.Success )
            return;

        tx.OnEndThen( txResult =>
        {
            if ( txResult != ETxResult.Ok )
                return;

            ValueS_ListItem sItemList = ExportTo( items );
            pc.Send( sItemList.Write() );
        } );
    }

    private static ValueS_ListItem ExportTo( GiveItemList items )
    {
        var sItemList = new ValueS_ListItem();
        foreach ( PktItem item in items )
            sItemList._items.Add( item );
        return sItemList;
    }

    public static ELogicResult Give( TxContext? tx, Pc pc, GiveItemList? giveItemList, GiveParam param )
    {
        /// 트랜잭션 설계
        var localTx = tx ?? TxContext.New();

        var result = ELogicResult.Success;
        if ( pc.Bag.CanStack( param.ItemDesignId ) )
            result = StackItem( pc, in param, giveItemList, localTx );
        else
            result = AddItem( pc, param, localTx );

        if ( !result.IsSuccess() )
            return result;

        if ( tx is null )
            pc.PostTx( localTx );

        return ELogicResult.Success;
    }

    private static ELogicResult AddItem( Pc pc, GiveParam param, TxContext localTx )
    {
        ELogicResult result = pc.Bag.CheckGive( param );
        if ( result == ELogicResult.Success )
            CreateItem( pc, in param, localTx );

        return result;
    }

    private static ELogicResult StackItem( Pc pc, in GiveParam param, GiveItemList? sendItemList, TxContext tx )
    {
        if ( !pc.Bag.TryGetItemByDesignId( param.ItemDesignId, out var item ) )
            return ELogicResult.InvalidDesignId;

        var proxyItem = tx.Update< Item_ >( item );

        // Increase 등으로 변경
        proxyItem.SetCount( item.GetCount() + param.Count );

        ExportToItemList( sendItemList, proxyItem );

        return ELogicResult.Success;
    }

    private static void ExportToItemList( GiveItemList? sendItemList, Item_ proxyItem )
    {
        // itemList
        ref var reservedItem = ref sendItemList.ReserveOneItem();
        reservedItem._id = proxyItem.GetId();
        reservedItem._designId = proxyItem.GetDesignId();
        reservedItem._count = proxyItem.GetCount();
    }

    private static Item_ CreateItem( Pc pc, in GiveParam args, TxContext tx )
    {
        var newItem = tx.Add< Item_ >();
        newItem.SetKeyId( IdHelper.Next() );
        newItem.SetKeyRootOwnerId( pc.GetId() );
        newItem.SetKeyOwnerId( pc.GetId() );
        newItem.SetDesignId( args.ItemDesignId.AsPrimitive() );
        newItem.SetCount( args.Count );

        return newItem;
    }

}





