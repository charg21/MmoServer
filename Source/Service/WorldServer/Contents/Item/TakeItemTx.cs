﻿
using Mala.Db;
/// <summary>
/// 아이템 ( 아이템을 지급하는 공용 로직 )
/// </summary>
public static partial class TakeItemTx
{
    /// <summary>
    /// 회수 인자
    /// </summary>
    public struct TakeParam
    {
        public DesignId ItemDesignId = (DesignId)0;
        public int Count = 0;

        public TakeParam()
        {
        }
    }


    public static void Take( Pc pc, TakeParam args )
    {
        var tx = TxContext.New();

        Take( tx, pc, args );

        //tx.EndThen( txResult =>
        //{
        //    if ( txResult != ETxResult.Ok )
        //        return;

        //    var sItemList = new ValueS_ListItem();
        //    foreach ( PktItem item in items )
        //        sItemList._items.Add( item );
        //    pc.Send( sItemList.Write() );
        //} );
    }

    private static ELogicResult Take( TxContext tx, Pc pc, TakeParam args )
    {
        /// 트랜잭션에 대한 계획을 세움
        var localTx = tx ?? TxContext.New();

        ref var itemDesign = ref ItemDesign.Ref( args.ItemDesignId );
        if ( itemDesign.IsNull )
        {
            //Log.Error( $"ItemDesign is Null. ItemDesignId {args.ItemDesignId}" );
            return ELogicResult.InvalidDesignId;
        }

        var result = ELogicResult.Success;
        if ( itemDesign.IsStackable )
        {
        }
        else
        {
        }

        if ( tx is null )
            pc.PostTx( localTx );

        return ELogicResult.Success;
    }

}





