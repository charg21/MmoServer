﻿
using Mala.Db;

/// <summary>
/// 아이템 장착 트랜잭션
/// </summary>
public static partial class EuquipItemTx
{
    public static void Equip( TxContext? tx, Pc pc, ulong itemId, Action< ETxResult, ELogicResult > onComplete )
    {
        /// 트랜잭션에 대한 계획을 세움
        tx ??= TxContext.New();

        var ( result, item ) = ToParam( itemId );
        if ( !result.IsSuccess() )
        {
            onComplete( ETxResult.JobFail, result );
            return;
        }

        if ( !pc.Bag.HssItem( itemId ) )
        {
            // 실패 처리...
            return;
        }

        /// 세션에서 트랜잭션 작업 실행
        pc.PostTx(
            tx,
            txResult =>
            {
                onComplete( txResult, ELogicResult.Success );
            } );
    }

    /// <summary>
    /// 조건을 체크한다.
    /// </summary>
    private static ( ELogicResult, ItemDbModel ) ToParam( ulong itemId )
    {
        return ( ELogicResult.Success, null );
    }

    /// <summary>
    /// 조건을 체크한다.
    /// </summary>
    private static ELogicResult CheckCondition( ulong itemId )
    {
        return ELogicResult.Success;
    }
}

/// <summary>
/// 아이템 장착 해제 트랜잭션
/// </summary>
public static partial class UnEuquipItemTx
{
}

/// <summary>
/// 아이템 장착 변경 트랜잭션
/// </summary>
public static partial class ChangeEuquipItemTx
{
}
