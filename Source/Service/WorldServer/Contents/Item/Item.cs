﻿using Mala.Db;

public partial class Item_ : ItemDbModel, IDbModel
{
    public ulong DbId => GetId();

    bool IsExpired => false;

    public override void Sync( TxContext tx, EQueryType queryType )
    {
        var pc = tx.Owner as Pc;
        if ( pc is null )
            return;

        switch ( queryType )
        {
            case EQueryType.Update:
                {
                    if ( pc.Bag.TryGetItem( DbId, out var item ) )
                        CopyToOnlyUpdateField( item );
                    break;
                }

            case EQueryType.Insert:
                {
                    pc.Bag!.AddItem( this );
                    break;
                }

            case EQueryType.Delete:
                break;
            default:
                break;
        }


    }
}

