﻿using Mala.Logging;

/// <summary>
/// 이동 작업 단위
/// </summary>
public static class MoveJob
{
    /// <summary>
    ///
    /// </summary>
    public static Action< WorldObject, Position, Position, Direction > Do;

    /// <summary>
    ///
    /// </summary>
    public readonly static bool CheckCollision = false;

    static MoveJob()
    {
        Do = ( WorldObject @object, Position toPos, Position destPos, Direction dir ) =>
        {
            /// 좌표 보정을 해줌
            //if ( !Simulate( actor, ref toPos ) )
            //    return;

            @object.SetPos( toPos, dir );
            @object.SetDestPos( destPos );
        };
    }

    /// <summary>
    /// 충돌 이동 시뮬레이션을 한다.
    /// </summary>
    private static bool Simulate( WorldObject actor, ref Position toPos )
    {
        /// 충돌 여부 체크
        var collisionGridOwner = actor.CollisionGridOwner;
        if ( collisionGridOwner is null )
            return true;

        var collisionGridManager = actor.World.CollisionGridManager;
        var fromPos              = actor.Movement.Pos;
        var curPos               = fromPos;
        var nextPos              = fromPos;
        var distance             = toPos - fromPos; // For Debug
        var distanceSqrt         = ( toPos - fromPos ).SizeSquared();
        var dir                  = distance.Normalize();
        var gridUnit             = dir * Consts.CollisionGrid.Unit;
        var moveDistance         = new Position();

        /// 직선 거리상 충돌 여부 체크
        /// 이동속도에따라 여러 그리드가 포함되어 시뮬레이션
        /// 충돌이 발생하는 구간에서 이동을 중단하고 좌표를 보정
        for ( ;; )
        {
            /// 다음 좌표를 구함
            nextPos      += gridUnit;
            moveDistance += gridUnit;

            /// 그리드 단위로 이동시, 다음 이동 거리를 초과하는 케이스
            if ( moveDistance.SizeSquared() > distanceSqrt )
            {
                /// 이동을 했다는 애니메이션 정보를 보내야함
                toPos = curPos;
                return true;
            }

            var nextGridIndex = WorldHelper.GetCollisionGridIndex( nextPos );
            var curGridIndex  = collisionGridOwner.GridIndex;
            /// Log.Info( $"Collisoin Grid Move PREV POS:{curPos} => NEXT POS:{nextPos}" );
            if ( nextGridIndex != curGridIndex )
            {
                /// 다음 충돌 그리드 획득 시도
                if ( collisionGridManager.TryLock( nextPos, actor.Id ) )
                {
                    /// Log.Info( $"Grid Get Ok... {curGridIndex} => {nextGridIndex}" );
                    /// 성공시 소유권을 획득, 이전 그리드에 대한 소유권을 반환한다.
                    collisionGridOwner.GridIndex = nextGridIndex;
                    collisionGridManager.Unlock( curGridIndex.x, curGridIndex.y );
                    /// 이동 시뮬레이션 좌표 갱신
                    curPos = nextPos;
                }
                else
                {
                    /// 충돌 그리드 소유권 획득 실패시
                    /// 마지막 시뮬레이션 성공 좌표를 최종 좌표로 갱신
                    Log.Info( $"Collision Overlapped Occur! {curGridIndex} => {nextGridIndex}" );
                    toPos = curPos;
                    return true;
                }
            }
            else
            {
                /// 이동을 못하는 케이스 끝단에서 발생 예정
                ///Log.Info( $"Collisoin Grid Equal {curGridIndex} => {nextGridIndex}" );

                /// 같은 그리내에서 이동하는 케이스
                /// 자연스러운 이동을 보이게 하기 위해 그리드 사이즈 보다 작은 이동에 대해 반영
                /// 타일 단위로 이동한다면, 아래는 주석처리 함
                if ( nextPos != curPos )
                    toPos = nextPos;

                return true;
            }
        }

        return true;
    }


    /// <summary>
    /// 충돌 이동 시뮬레이션을 한다.
    /// </summary>
    private static bool Simulate2( WorldObject actor, ref Position toPos )
    {
        /// 충돌 여부 체크
        var collisionGridOwner = actor.CollisionGridOwner;
        if ( collisionGridOwner is null )
            return true;

        var collisionGridManager = actor.World.CollisionGridManager;
        var fromPos              = actor.Movement.Pos;
        var curPos               = fromPos;
        var nextPos              = fromPos;
        var distance             = toPos - fromPos; // For Debug
        var distanceSqrt         = ( toPos - fromPos ).SizeSquared();
        var dir                  = distance.Normalize();
        var gridUnit             = dir * Consts.CollisionGrid.Unit;
        var moveDistance         = new Position();

        /// 직선 거리상 충돌 여부 체크
        /// 이동속도에따라 여러 그리드가 포함되어 시뮬레이션
        /// 충돌이 발생하는 구간에서 이동을 중단하고 좌표를 보정
        for ( ;; )
        {
            /// 다음 좌표를 구함
            nextPos      += gridUnit;
            moveDistance += gridUnit;

            /// 그리드 단위로 이동시, 다음 이동 거리를 초과하는 케이스
            if ( moveDistance.SizeSquared() > distanceSqrt )
            {
                /// 이동을 했다는 애니메이션 정보를 보내야함
                toPos = curPos;
                return true;
            }

            var nextGridIndex = WorldHelper.GetCollisionGridIndex( nextPos );
            var curGridIndex  = collisionGridOwner.GridIndex;
            /// Log.Info( $"Collisoin Grid Move PREV POS:{curPos} => NEXT POS:{nextPos}" );
            if ( nextGridIndex != curGridIndex )
            {
                /// 다음 충돌 그리드 획득 시도
                if ( collisionGridManager.TryLock( nextPos, actor.Id ) )
                {
                    /// Log.Info( $"Grid Get Ok... {curGridIndex} => {nextGridIndex}" );
                    /// 성공시 소유권을 획득, 이전 그리드에 대한 소유권을 반환한다.
                    collisionGridOwner.GridIndex = nextGridIndex;
                    collisionGridManager.Unlock( curGridIndex.x, curGridIndex.y );
                    /// 이동 시뮬레이션 좌표 갱신
                    curPos = nextPos;
                }
                else
                {
                    /// 충돌 그리드 소유권 획득 실패시
                    /// 마지막 시뮬레이션 성공 좌표를 최종 좌표로 갱신
                    Log.Info( $"Collision Overlapped Occur! {curGridIndex} => {nextGridIndex}" );
                    toPos = curPos;
                    return true;
                }
            }
            else
            {
                /// 이동을 못하는 케이스 끝단에서 발생 예정
                ///Log.Info( $"Collisoin Grid Equal {curGridIndex} => {nextGridIndex}" );

                /// 같은 그리내에서 이동하는 케이스
                /// 자연스러운 이동을 보이게 하기 위해 그리드 사이즈 보다 작은 이동에 대해 반영
                /// 타일 단위로 이동한다면, 아래는 주석처리 함
                if ( nextPos != curPos )
                    toPos = nextPos;

                return true;
            }
        }

        return true;
    }
}

