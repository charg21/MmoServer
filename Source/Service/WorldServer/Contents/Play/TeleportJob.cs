﻿/// <summary>
///
/// </summary>
public static class TeleportJob
{
    public static Action< WorldObject, Position > Do;

    static TeleportJob()
    {
        Do = DoTeleport;
    }

    public static void DoTeleport( WorldObject actor, Position toPos )
    {
        Position fromPos = actor.Movement.Pos;

        var fromIndex = WorldHelper.ToWideClusterIndex( fromPos );
        var toIndex   = WorldHelper.ToWideClusterIndex( toPos );

        actor.Movement.Pos = toPos;

        //actor.SwitchCluster( fromIndex, toIndex );

        actor.View.Rebuild();
    }
}

