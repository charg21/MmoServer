﻿
/// <summary>
/// 시야 작업
/// </summary>
public static class ViewJob
{
    public static Action< WorldObject, ObjectId, Position, Direction, Position > DoMove;
    public static Action< WorldObject, WorldObject, Position > DoEnter;
    public static Action< WorldObject, ObjectId > DoLeave;

    static ViewJob()
    {
        DoEnter = ( WorldObject caster, WorldObject target, Position pos )
            => caster.View.OnEnterSomeOne( target, pos );

        DoMove = ( WorldObject caster, ObjectId otherId, Position fromPos, Direction dir, Position toPos )
            => caster.View.OnMoveSomeOne( otherId, fromPos, dir, toPos );

        DoLeave = ( WorldObject caster, ObjectId otherId )
            => caster.View.OnLeaveSomeOne( otherId );
    }
}

