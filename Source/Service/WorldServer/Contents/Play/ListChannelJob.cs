﻿public static class ListChannelJob
{
    public static Action< Pc > Do;


    static ListChannelJob()
    {
        Do = Execute;
    }

    public static void Execute( Pc pc )
    {
        var listChannel = new ValueS_ListChannel();

        foreach ( var channel in ChannelManager.Instance.Channels )
        {
            if ( channel != null )
                listChannel._channels.Add( channel.Id );
        }

        pc.Send( listChannel.Write() );
    }
}
