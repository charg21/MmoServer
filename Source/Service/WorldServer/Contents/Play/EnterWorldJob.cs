﻿
using Mala.Logging;


public static class EnterWorldJob
{
    public static Action< WorldObject, int > Enter = OnEnter;
    static Random rand = new();

    public static void OnEnter( WorldObject worldObj, int channelId )
    {
        //if ( Config.UseDb )
        //{
        //    var pc = worldObj as Pc;
        //    pc?.SetKeyId( worldObj.worldObjId );
        //}

        if ( worldObj.Movement.Pos.SizeSquared() == 0 )
        {
            //worldObj.Pos.x = (float)( Random.Shared.Next() % Consts.World.Width );
            //worldObj.Pos.y = 0f;
            //worldObj.Pos.z = (float)( Random.Shared.Next() % Consts.World.Height );

            //worldObj.FromPos = worldObj.Pos;
        }

        if ( !ChannelManager.Instance.GetChannel( channelId, out var channel ) )
        {
            Log.Critical( $"Get Channel Failed{ channelId }" );

            return;
        }

        worldObj.EnterWorld( channel.World );
    }
}

