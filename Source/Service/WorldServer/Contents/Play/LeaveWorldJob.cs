﻿using System.Numerics;

/// <summary>
///
/// </summary>
public static class LeaveWorldJob
{
    public static Action< WorldObject > Do = OnLeave;

    static LeaveWorldJob()
    {

    }

    public static void OnLeave( WorldObject worldObj )
    {
        worldObj.LeaveWorld();
    }
}

