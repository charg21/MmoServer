﻿
/// <summary>
///
/// </summary>
public static class SpawnJob< T > where T : WorldObject, new()
{
    public struct SpawnArgs
    {
        public ulong ActorId;
        public EWorldObjectType ActorType;
        public Position Pos;
        public Direction Dir;
    }

    public static Action< World, ObjectId, Position, Direction > Do;
    public static Action< Channel, ObjectId, Position, Direction > DoForChannel;

    /// <summary>
    ///
    /// </summary>
    static SpawnJob()
    {
        Do = ( world, actorId, pos, dir ) =>
        {
            world.Spawn< T >( actorId, pos, dir );
        };

        DoForChannel = ( channel, actorId, pos, dir ) =>
        {
            channel.World.Spawn< T >( actorId, pos, dir );
        };
    }
}

/// <summary>
///
/// </summary>
public static class DespawnJob
{
    public static Action< World, ulong > Do;

    public static Action< World, ulong > DoBySpawnId;

    /// <summary>
    ///
    /// </summary>
    static DespawnJob()
    {
        Do = ( world, actorId ) => world.Despawn( actorId );
    }
}



