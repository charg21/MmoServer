﻿
using Mala.Threading;

/// <summary>
/// 
/// </summary>
public static class TickJob
{
    /// <summary>
    /// 
    /// </summary>
    public static Action< IExecutor> DoTick { get; set; }

    static TickJob()
    {
        DoTick = ( actor ) =>
        {
            actor.Tick();
        };
    } 
}
