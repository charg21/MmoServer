﻿using System.Runtime.InteropServices;

public class ViewList2 : List< WorldObject >
{
    /// <summary>
    /// 오브젝트 인덱스 딕셔너리
    /// </summary>
    Dictionary< ObjectId, int > _objectIndexDict;

    /// <summary>
    /// 생성자
    /// </summary>
    public ViewList2( int capacity ) : base( capacity )
    {
        _objectIndexDict = new( capacity );
    }

    /// <summary>
    /// 액터를 추가한다.
    /// </summary>
    public bool TryAdd( WorldObject worldObject ) => TryAdd( worldObject.Id, worldObject );

    /// <summary>
    /// 액터를 추가한다.
    /// </summary>
    public bool TryAdd( ObjectId objectId, WorldObject worldObject )
    {
        if ( _objectIndexDict.TryAdd( objectId, base.Count ) )
        {
            base.Add( worldObject );
            return true;
        }

        /// 이미 맵에 존재
        return false;
    }

    public bool Remove( ObjectId objectId ) => TryRemove( objectId );

    public bool TryRemove( ObjectId objectId )
    {
        if ( !_objectIndexDict.Remove( objectId, out var index ) )
            return false;

        /// 마지막 액터가 아닌 경우
        if ( index != Count - 1 )
        {
            var lastObject = base[ Count - 1 ];
            base[ index ] = lastObject;
            _objectIndexDict[ lastObject.Id ] = index;
        }

        /// 마지막 null 제거
        base.RemoveAt( Count - 1 );

        return true;
    }

    public bool TryRemoveRange( ReadOnlySpan< ObjectId > actorIds )
    {
        /// 삭제 대상 인덱스 한번에 가져옴
        List< int > removeIndexList = new( actorIds.Length );
        foreach ( var actorId in actorIds )
        {
            if ( _objectIndexDict.Remove( actorId, out var index ) )
                removeIndexList.Add( index );
        }

        if ( removeIndexList.Count == 0 )
            return false;

        /// 삭제 대상 인덱스 제거
        for ( int i = 0; i < removeIndexList.Count; i += 1 )
        {
            var removeIndex = removeIndexList[ i ];
            var lastIndex = Count -i - 1;
            if ( removeIndex != lastIndex )
            {
                var lastObject = base[ lastIndex ];
                base[ removeIndex ] = lastObject;
                _objectIndexDict[ lastObject.Id ] = removeIndex;
            }
        }

        /// 리스트 마지막 정리
        base.RemoveRange( base.Count - removeIndexList.Count, removeIndexList.Count );

        return true;
    }

    public bool TryGetValue( ObjectId objectId, out WorldObject worldObject )
    {
        if ( _objectIndexDict.TryGetValue( objectId, out var index ) )
        {
            worldObject = base[ index ];
            return true;
        }

        worldObject = null!;
        return false;
    }

    public bool TryGet< TWorldObject >( ObjectId objectId, out TWorldObject? tWorldObject ) where TWorldObject : WorldObject
    {
        if ( _objectIndexDict.TryGetValue( objectId, out var index ) )
        {
            tWorldObject = base[ index ] as TWorldObject;
            if ( tWorldObject is null )
                throw new Exception( "Invalid WorldObject Type" );

            return true;
        }

        tWorldObject = null;
        return false;
    }

    public Span< WorldObject > AsSpan() => CollectionsMarshal.AsSpan( this );

    public ReadOnlySpan< WorldObject > AsReadonlySpan() => AsSpan();

    public new void Clear()
    {
        base.Clear();
        _objectIndexDict.Clear();
    }
}
