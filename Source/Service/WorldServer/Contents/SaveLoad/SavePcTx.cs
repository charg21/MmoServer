﻿using Mala.Db;

public static class SavePcTx
{
    public static Action< Pc, ESaveType, TxContext? > Save;

    static SavePcTx()
    {
        Save = ( pc, saveType, contentsType ) =>
        {
            // 나중에 캐싱 처리
            var tx = TxContext.New();

            var pcDb = tx.Update< Pc >( pc );
            pcDb.SetX( pc.Movement.Pos.x );
            pcDb.SetY( pc.Movement.Pos.y );
            pcDb.SetZ( pc.Movement.Pos.z );

            pc.PostTx( tx );
        };
    }
}