﻿using Mala.Core;
using Mala.Db;
using Mala.Logging;


/// <summary>
/// PC 로드 트랜잭션
/// </summary>
public static class LoadPcTx
{
    public static Action< ITxExecutor, UserDbModel, Pc, ActorId, Action< ETxResult > > OnLoadJob;

    static LoadPcTx()
    {
        OnLoadJob = Load;
    }

    public static void Load(
        ITxExecutor         executor,
        UserDbModel         userDb,
        Pc                  pc,
        ActorId             pcId,
        Action< ETxResult > onComplete )
    {
        /// 트랜잭션에 대한 계획을 세움
        var tx = TxContext.New();

        /// 트랜잭션에 DB 작업을 추가한다.
        tx.ThenDbJob( dbContext =>
        {
            /// 플레이어 로드|userDb
            var loadResult = LoadPc( ref dbContext, pc, pcId, userDb.GetId(), tx );
            if ( loadResult == EDbResult.Success )
                return EDbResult.Success;

            /// 플레이어가 없다면 생성
            var createResult = CreatePc( ref dbContext, pc.dbModel, userDb, pcId );
            if ( createResult == EDbResult.Success )
                return EDbResult.Success;

            /// 실패시 생성 실패 사유 반환
            return createResult;
        } );

        /// 현재 트랜잭션이 종료되었을 경우 실행하는 작업 추가
        tx.OnEndThen( txResult =>
        {
            ApplyToPc( pc, pc.DbModel as PcDbModel );
            SendResult( pc, txResult );
        } );

        /// 세션에서 트랜잭션 작업 실행
        executor.PostTx(
            tx,
            txResult =>
            {
                onComplete( txResult );
            } );
    }

    private static EDbResult CreatePc(
        ref DbExecutionContext context ,
            PcDbModel          pcDb,
            UserDbModel        user,
            ulong              newPcId = 0 )
    {
        if ( newPcId == 0 )
            newPcId = IdHelper.Next();

        pcDb.SetName     ( $"{ newPcId }"                           );
        pcDb.SetKeyId    ( newPcId                                  );
        pcDb.SetKeyUserId( user.GetId()                             );
        pcDb.SetX        ( Random.Shared.Next( Consts.World.Width ) );
        //pcDb.SetY        ( 0                                        );
        pcDb.SetZ        ( Random.Shared.Next( Consts.World.Width ) );

        if ( user is not null )
            pcDb.SetKeyUserId( user.GetId() );

        if ( !pcDb.Insert( ref context ) )
            return EDbResult.Fail;

        //if ( user is null )
        //    return EDbResult.Success;

        user.SetPcId( newPcId );
        if ( !user.Update( ref context ) )
            return EDbResult.Fail;

        return EDbResult.Success;
    }

    private static EDbResult LoadPc( ref DbExecutionContext dbContext, Pc pc, ActorId pcId, ulong userId, TxContext tx )
    {
        if ( pcId == 0 )
            return EDbResult.Empty;

        /// Pc Db 로드
        var pcDb = new PcDbModel();
        pcDb.SetKeyId    ( pcId   );
        pcDb.SetKeyUserId( userId );
        pcDb.BindAllField();
        var result = pcDb.Select( ref dbContext );
        if ( result == EDbResult.Fail )
            return EDbResult.Fail;

        /// 앞선 Db 로드 작업 성공시, 추가적으로 컨텐츠 데이터를 로드함
        //result = pc.LoadComponentFromDb( ref dbContext, tx );
        //if ( result == EDbResult.Fail )
        //    return EDbResult.Fail;

        //tx.EndWith( txResult =>
        //{
        //    ApplyPcDb( pc, pcDb );
        //    SendResult( pc, txResult );
        //} );

        return EDbResult.Success;
    }

    private static void ApplyToPc( Pc pc, PcDbModel pcDb )
    {
        /// 성공한 경우 Db 임시 객체의 내용을 플레이어에 반영
        pcDb.CopyTo( pc.DbModel );

        pc.Id = new( pcDb.GetId() );
        pc.Movement.Pos = new( pcDb.GetX(), pcDb.GetY(), pcDb.GetZ() );
        pc.Movement.FromPos = pc.Movement.Pos;
    }

    private static void SendResult( Pc pc, ETxResult txResult )
    {
        if ( txResult != ETxResult.Ok )
        {
            /// 트랜잭션의 결과가 성공이 아닌경우 리턴
            pc.Send( PacketWriter.FromS_LoadPc( pc, ELogicResult.DbFail ) );
            return;
        }

        /// PC 로드 정보를 송신
        /// 월드 입장전까진 Pc의 Tick이 돌지 않음. Session에서 직접 송신
        pc.Send( PacketWriter.FromS_LoadPc( pc ) );
    }

}
