﻿using CloudStructures.Structures;
using Mala.Core;
using Mala.Db;
using Mala.Logging;
using MySql.Data.MySqlClient;
using Org.BouncyCastle.Ocsp;
using Windows.System;


public static class LoginTx
{
    /// <summary>
    ///
    /// </summary>
    public static Action< ClientSession, string, string, TxContext? > Do;

    static LoginTx()
    {
        Do = Run;
    }

    public static void Run(
        ClientSession session,
        string        account,
        string        pw,
        TxContext?    tx = null )
    {
        tx ??= TxContext.New();

        session.UserDbModel ??= new UserDbModel();

        tx.ThenDbJob( ctx =>
        {
            var result = Login( ref ctx, session.UserDbModel, account, pw );
            if ( result == EDbResult.Success )
                return EDbResult.Success;

            /// Empty인 케이스
            result = Register( ref ctx, session.UserDbModel, account, pw );
            if ( result == EDbResult.Success )
                return EDbResult.Success;

            return EDbResult.Fail;
        } );

        session.PostTx( tx, txResult =>
        {
            SendResult( session, txResult );
        } );
    }

    /// <summary>
    /// 로그인한다.
    /// </summary>
    public static EDbResult Login(
        ref DbExecutionContext ctx,
            UserDbModel        user,
            string             account,
            string             pw )
    {
        /// User Select 처리
        user.SetKeyAccount( account );
        user.SetPassword( pw );
        user.BindAllField();

        return user.Select( ref ctx );
    }

    /// <summary>
    /// 가입하다
    /// </summary>
    public static EDbResult Register(
        ref DbExecutionContext ctx,
            UserDbModel        user,
            string             account,
            string             pw )
    {
        user.SetKeyId( IdHelper.Next() );
        user.SetKeyAccount( account );
        user.SetPassword( pw );

        return user.Insert( ref ctx ) ? EDbResult.Success : EDbResult.Fail;
    }

    private static void SendResult( ClientSession session, ETxResult dbResult )
    {
        var loginAck = new ValueS_Login();
        loginAck._result = ETxResult.Ok == dbResult ? ELogicResult.Success : ELogicResult.DbFail;

        if ( session.UserDbModel.GetPcId() != 0 )
            loginAck._pcIds.Add( session.UserDbModel.GetPcId() );

        session.Send( loginAck.Write() );
    }
}
