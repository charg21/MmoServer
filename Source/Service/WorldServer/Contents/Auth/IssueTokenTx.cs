﻿using Mala.Db;

/// <summary>
/// 토큰 발급 작업
/// </summary>
public static class IssueTokenTx
{
    public static Action< ulong, TxContext > Do;

    static IssueTokenTx()
    {
        Do = Execute;
    }

    public static void Execute( ulong userId, TxContext tx )
    {
        var proxyToken = tx.Add< Token >();
        proxyToken.SetKeyUserId( userId );
        proxyToken.SetKeyId( (int)IdHelper.Next() );
        proxyToken.SetValue( IdHelper.Next() );
        proxyToken.SetExpired( DateTime.Now.AddHours( 4 ) );
    }
}
