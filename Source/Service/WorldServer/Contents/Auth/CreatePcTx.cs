using Mala.Core;
using Mala.Db;
using Mala.Logging;

/// <summary>
/// 캐릭터 생성
/// </summary>
public static class CreatePcTx
{
    public static void Create( ClientSession session, ActorId userId, string token, TxContext? tx = null )
    {
        tx ??= TxContext.New();

        tx.ThenDbJob( ( dbContext =>
        {
            var createResult = CreatePc( ref dbContext, session.Pc.dbModel, session.UserDbModel );
            if ( createResult != EDbResult.Success )
                return createResult;

            return EDbResult.Success;
        } ) );

        session.PostTx( tx, txResult =>
        {
            SendResult( session, txResult );
        } );
    }

    /// <summary>
    /// 플레이어를 생성한다.
    /// </summary>
    private static EDbResult CreatePc(
        ref DbExecutionContext context,
            PcDbModel          pcDb,
            UserDbModel        userDb )
    {
        var newPcId = IdHelper.Next();

        pcDb.SetName     ( $"{newPcId}"                              );
        pcDb.SetKeyId    ( newPcId                                   );
        pcDb.SetKeyUserId( userDb.GetId()                            );
        pcDb.SetX        ( Random.Shared.Next( Consts.World.Width )  );
        //pcDb.SetY        ( 0                                         );
        pcDb.SetZ        ( Random.Shared.Next( Consts.World.Height ) );

        if ( !pcDb.Insert( ref context ) )
            return EDbResult.Fail;

        /// 유저의 플레이어 아이디를 설정한다.
        userDb.SetPcId( newPcId );
        if ( !userDb.Update( ref context ) )
            return EDbResult.Fail;

        return EDbResult.Success;
    }

    /// <summary>
    /// 조건을 체크한다.
    /// </summary>
    private static ELogicResult CheckCondition(
        PcDbModel   pcDb,
        UserDbModel userDb )
    {
        return ELogicResult.Success;
    }

    private static void SendResult( ClientSession session, ETxResult result )
    {
        var sLoadPc = new ValueS_LoadPc();
        var pc = session.Pc;
        if ( result == ETxResult.Ok )
        {
            sLoadPc._result = ELogicResult.Success;
            //session.PlayerDbModel.ExportTo( ref loadAck.sLoadPc );
            sLoadPc._pktPc._objectId = pc.dbModel.GetId();
            sLoadPc._pktPc._pos._x  = pc.dbModel.GetX();
            sLoadPc._pktPc._pos._y  = pc.dbModel.GetY();
            sLoadPc._pktPc._pos._z  = pc.dbModel.GetZ();
        }
        else
        {
            sLoadPc._result = ELogicResult.DbFail;
        }

        session.Send( sLoadPc.Write() );
    }
}
