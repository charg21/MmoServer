﻿using Mala.Net;
using Org.BouncyCastle.Asn1.X509;
using static Consts;

public enum EBroadCastType
{
    /// <summary>
    /// AOI 영역
    /// </summary>
    Aoi,

    /// <summary>
    /// 현재 클러스터
    /// </summary>
    Cluster,

    /// <summary>
    /// 인접 클러스터
    /// </summary>
    NearCluster,

    /// <summary>
    /// 전체
    /// </summary>
    All
}
