using Mala.Net;

public partial class C_CheatHandler
{
	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
		var cheat = packet as C_Cheat;
		CheatManager.Instance.Handle( session as ClientSession, cheat._command );
	}

}

