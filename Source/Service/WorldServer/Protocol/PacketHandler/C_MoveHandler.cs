using Mala.Math;
using Mala.Net;
public partial class C_MoveHandler
{
	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var move          = packet as C_Move;
        var clientSession = session as ClientSession;

        var dir = MathHelper.YawToVector2( move._yaw );

        var pos = move._pos.ToVector3();
        pos.x = MathHelper.Clamp( 0f, pos.x, Consts.World.Width  );
        pos.z = MathHelper.Clamp( 0f, pos.z, Consts.World.Height );

        var toPos = move._toPos.ToVector3();
        toPos.x = MathHelper.Clamp( 0f, toPos.x, Consts.World.Width );
        toPos.z = MathHelper.Clamp( 0f, toPos.z, Consts.World.Height );

        var pc = clientSession?.Pc;

        pc?.Post( MoveJob.Do, in pc, in pos, in toPos, in dir );
    }

}

