using Mala.Net;
public partial class C_ChatHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        // TODO: 로직을 작성하세요.
        var chat          = packet as C_Chat;
        var clientSession = session as ClientSession;

        var pc = clientSession?.Pc;
        pc?.Post( pc.Chat!.Say, chat!._message );
    }
}

