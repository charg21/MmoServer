using Mala.Db;
using Mala.Net;
public partial class C_LoginHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var login = packet as C_Login;
        var clientSession = session as ClientSession;

        clientSession?.Post(
            LoginTx.Run,
            clientSession,
            login._account,
            login._password,
            (TxContext)null );
    }

}

