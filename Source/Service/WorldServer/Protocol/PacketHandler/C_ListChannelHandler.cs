using Mala.Net;
public partial class C_ListChannelHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var listChannel = packet as C_ListChannel;
        var clientSession = session as ClientSession;

        var pc = clientSession?.Pc;
        pc.Post( ListChannelJob.Do, pc );
    }

}
