using Mala.Net;

public partial class C_StartSkillHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var startSkill    = packet as C_StartSkill;
        var clientSession = session as ClientSession;
        var pc            = clientSession?.Pc;

        pc?.Post( StartSkillJob.Do,
            pc,
            (ObjectId)startSkill._targetId,
            (DesignId)startSkill._skillDesignId );
    }

}

