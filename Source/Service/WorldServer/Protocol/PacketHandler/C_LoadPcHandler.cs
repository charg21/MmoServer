using Mala.Db;
using Mala.Logging;
using Mala.Net;
public partial class C_LoadPcHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var loadPc = packet as C_LoadPc;

        var clientSession = session as ClientSession;

        clientSession?.Post(
            LoadPcTx.Load,
            clientSession,
            clientSession.UserDbModel,
            clientSession.Pc,
            loadPc._pcId,
            ( ETxResult result ) => { } );
    }

}

