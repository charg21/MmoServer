using Mala.Db;
using Mala.Logging;
using Mala.Net;

public partial class C_EnterWorldHandler
{
    public static int channelIdGen = 0;

    public static void DoHandle( PacketHeader packet, PacketSession session )
    {
        var enter         = packet as C_EnterWorld;
        var clientSession = session as ClientSession;

        var pc = clientSession.Pc;
        pc.Session = clientSession;

        if ( !Config.UseDb || pc.Id == 0 )
        {
            pc.Id = new( clientSession.SessionId );
            pc.Movement.Pos = new( Random.Shared.Next( Consts.World.Width ), 0, Random.Shared.Next( Consts.World.Width ) );
            pc.Movement.FromPos = pc.Movement.Pos;
        }

        //channelIdGen.Value += 1;
        int randomChannel = Interlocked.Increment( ref channelIdGen ) % ChannelManager.Instance.Count;
        pc?.Post( EnterWorldJob.Enter, pc, randomChannel );
    }

}

