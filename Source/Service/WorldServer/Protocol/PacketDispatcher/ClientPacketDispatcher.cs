using Mala.Net;

public class ClientPacketDispatcher : PacketDispatcher
{
	/// <summary>
	/// 정적 인스턴스
	/// </summary>
	public static ClientPacketDispatcher Instance = new();
	public override void Initialize()
	{
        HandlerMap.Add( (int)( EPacketType.C_EnterWorld ), C_EnterWorldHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_LeaveWorld ), C_LeaveWorldHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_Move ), C_MoveHandler.DoHandle  );
        HandlerMap.Add( (int)( EPacketType.C_StartSkill ), C_StartSkillHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_Login ), C_LoginHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_Pong ), C_PongHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_Chat ), C_ChatHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_LoadPc ), C_LoadPcHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_Cheat ), C_CheatHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_ListChannel ), C_ListChannelHandler.DoHandle );
        HandlerMap.Add( (int)( EPacketType.C_UseItem ), C_UseItemHandler.DoHandle );
        //Initialize2();
    }

    public void Initialize2()
	{
        foreach ( var assembly in AppDomain.CurrentDomain.GetAssemblies() )
		{
            foreach ( var type in assembly.GetTypes() )
			{
				if ( /*type.Namespace == "Mala.Net" && */type.Name.StartsWith( "C_" ) )
                {
                    var method = type.GetMethod( "DoHandle" );
                    if ( method is not null )
                    {
						try
						{
                            var handler = Delegate.CreateDelegate( typeof( Action< PacketHeader, PacketSession > ), method ) as Action< PacketHeader, PacketSession >;
                            if ( handler is not null )
                                HandlerMap.Add( (ushort)( Enum.Parse< EPacketType >( type.Name.AsSpan().Slice( 0, type.Name.Length - 7 ) ) ), handler );
                        }
                        catch ( Exception e )
						{
							Console.WriteLine( e );
                        }

                        //var handler = Delegate.CreateDelegate( typeof( Action< PacketHeader, PacketSession > ), method ) as Action< PacketHeader, PacketSession >;
                        //if ( handler is not null )
                        //    HandlerMap.Add( (ushort)( Enum.Parse< EPacketType >( type.Name.AsSpan().Slice( 0, type.Name.Length - 6 ) ) ), handler );
                    }
                }
            }
        }

    }
}

