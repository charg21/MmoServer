﻿/// <summary>
/// 일단 채널과 월드는 1:1 대응 관계
/// </summary>
public class Channel
{
    /// <summary>
    /// 채널 식별자
    /// </summary>
    public int _id;
    public int Id => _id;

    /// <summary>
    /// 월드
    /// </summary>
    public World World { get; set; }
}
