﻿using Mala.Collection;
using Mala.Core;

/// <summary>
/// 채널 관리자
/// </summary>
public class ChannelManager : Singleton< ChannelManager >
{
    /// <summary>
    ///
    /// </summary>
    public int _channelIdGenerator = -1;

    /// <summary>
    ///
    ///
    /// </summary>
    public Channel[] Channels = new Channel[ Consts.Channel.MaxCount ];

    /// <summary>
    ///
    /// </summary> 
    public int Count => _channelIdGenerator + 1;

    /// <summary>
    /// </summary>
    public void Initialize()
    {
        for ( int channelId = 0; channelId < Consts.Channel.DefaultCount; channelId += 1 )
        {
            var channel = CreateChannel();
            channel.World = new World( channel );
        }
    }

    public Channel CreateChannel()
    {
        var channel = new Channel()
        {
            _id = Interlocked.Increment( ref _channelIdGenerator )
        };

        Channels[ channel.Id ] = channel;

        return channel;
    }

    public bool GetChannel( int channelId, out Channel channel )
    {
        for ( int n = 0; n < Count; n += 1 )
        {
            channel = Channels[ n ];
            if ( channel != null && channel.Id == channelId )
                return true;
        }

        channel = null;
        return false;
    }

    public void ForEach( Action< Channel > channelJob )
    {
        for ( int n = 0; n < Count; n += 1 )
        {
            var channel = Channels[ n ];
            if ( channel is null )
                return;

            channelJob( channel );
        }
    }

    public void ForEach( Func< Channel, EForEachResult > channelJob )
    {
        for ( int n = 0; n < Count; n += 1 )
        {
            var channel = Channels[ n ];
            if ( channel is null )
                return;

            EForEachResult result = channelJob( channel );
            if ( result == EForEachResult.Break )
                return;
        }
    }
}
