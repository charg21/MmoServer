﻿using Mala.Logging;
using Mala.Threading;

public class BotWorker
{
    public static void DoWorkerJob( BotClient botClient )
    {
#if NATIVE
        NativeWorkerThread.DoInit();
#endif
        ThreadPoolExecutorService.Initialize();

        Log.Info( $"Tls Init Ok... CurThread Id: {ThreadPoolExecutorService._tlsContext.Id}" );

        var lastTick = Environment.TickCount64;
        var simulationTick = 0L;

        for ( ;; )
        {
#if NATIVE
            botClient.Dispatch( 16 );
#endif
            ThreadPoolExecutorService.Run();

            //ClientSessionManager.FlushSend();
        }
    }
}
