﻿public struct SimpleTimer
{
    long Timeout { get; set; } = 0;
    long _timer { get; set; } = 0;
    public Action _onExpired { get; set; }

    public SimpleTimer()
    {
        Reset();
    }

    public void Reset()
    {
        _timer = 0;
    }

    public void Update( TimeSpan deltaTime )
    {
        _timer += deltaTime.Ticks;
        if ( _timer >= Timeout )
        {
            _onExpired?.Invoke();
            Reset();
        }
    }

    internal void Restart( long timeout, Action task )
    {
        _timer = 0;
        Timeout = timeout;
        _onExpired = task;
    }
}
