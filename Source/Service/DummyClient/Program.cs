﻿using Mala.Core;
using Mala.Logging;

ExceptionHandler.Initialize();
Config.Load( args, "DummyClient" );
ServerPacketDispatcher.Instance.Initialize();
LogManager.Initialize( "BotClient" );

Config.WorkerThreadCount = 24;
var app = new BotClient(
    "127.0.0.1",
    18888,
    BotSessionManager.Generate,
    0 );

app.Initialize();
app.Start();
