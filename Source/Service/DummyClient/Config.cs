﻿using System.Text.Json;


public static class Config
{
    /// <summary>
    /// 워커 스레드 갯수
    /// </summary>
    public static int WorkerThreadCount = 1;

    /// <summary>
    /// 더미 Pc 수
    /// </summary>
    public static int DummyPcCount = 0;

    /// <summary>
    /// 서버 IP
    /// </summary>
    public static string ServerIp = "127.0.0.1";

    /// <summary>
    /// 서버 포트
    /// </summary>
    public static int ServerPort = 0;

    /// <summary>
    /// 행동 트리 사용 여부
    /// </summary>
    public static bool UseBehaviorTree = false;

    static Config()
    {
    }

    public static void Load( string[] args, string configPath )
    {
        if ( string.IsNullOrEmpty( configPath ) )
            configPath = $"config";

        configPath += ".json";

        if ( args.Length > 0 && !string.IsNullOrEmpty( args[ 0 ] ) )
            configPath = args[ 0 ];

        Console.WriteLine( $"Config Path = {configPath}" );
        var configString = File.ReadAllText( configPath );

        /// Json 역직렬화를 진행한다.
        var config = JsonSerializer.Deserialize< ConfigDto >( configString );
        ImportFrom( in config );
    }

    /// <summary>
    /// 리플렉션을 사용하여 환경설정을 반영한다.
    /// </summary>
    /// <param name="config"></param>
    private static void ImportFrom< T >( in T config )
    {
        foreach ( var propConfig in config.GetType().GetProperties() )
        {
            var fieldEnv = typeof( Config ).GetField( propConfig.Name );
            if ( fieldEnv is null )
                continue;

            fieldEnv.SetValue( null, propConfig.GetValue( config ) );
            Console.WriteLine( $"{ propConfig.Name } = { propConfig.GetValue( config ) } " );
        }
    }

    /// <summary>
    /// 리플렉션을 사용하여 필드 값을 수정한다.
    /// </summary>
    public static void SetField( string fieldName, string value )
    {
        var fieldEnv = typeof( Config ).GetField( fieldName );
        if ( fieldEnv is null )
            return;

        object typeValue = Convert.ChangeType( value, fieldEnv.FieldType );

        fieldEnv.SetValue( null, typeValue );
        Console.WriteLine( $"{ fieldName } = { value } " );
    }
}

/// <summary>
/// 설정 데이터 전달 객체
/// </summary>
public struct ConfigDto
{
    /// <summary>
    /// 생성자
    /// </summary>
    public ConfigDto()
    {
    }

#region Thread

    /// <summary>
    /// 워커 스레드 수
    /// </summary>
    public int WorkerThreadCount { get; set; } = 0;

#endregion

    /// <summary>
    /// 더미 플레이어 수
    /// </summary>
    public int DummyPcCount { get; set; } = 500;

    /// <summary>
    /// 서버 IP
    /// </summary>
    public string ServerIp { get; set; } = "127.0.0.1";

    /// <summary>
    /// 서버 포트
    /// </summary>
    public int ServerPort { get; set; } = 0;

}
