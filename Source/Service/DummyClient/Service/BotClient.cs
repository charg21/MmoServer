﻿using Mala.Collection;
using Mala.Logging;
using Mala.Net;
using Mala.Threading;
using System.Net;
using System.Runtime;


/// <summary>
/// 봇 클라이언트
/// </summary>
public class BotClient : NativeClientServiceInternal
{
    static WaitFreeQueue< BotClient > Bots = new();
    Func< BotSession > SessionFactory;

    /// <summary>
    /// 봇 매니저
    /// </summary>
    public BotPcManager BotManager { get; set; }

    /// <summary>
    /// 이름
    /// </summary>
    string Name => "BotClient";

    /// <summary>
    /// 봇 명령어 수신기
    /// </summary>
    public BotCommandListener CommandListner;

    /// <summary>
    /// 생성자
    /// </summary>
    public BotClient(
        string         ip,
        int            port,
        SessionFactory sessionFactory,
        int            capacity )
    : base( ip, port, sessionFactory, capacity )
    {
        SessionFactory = () =>
        {
            var session = new BotSession();

            // TLS별 존재하는 Bot 플레이어 매니저를 획득
            session.BotManager = BotPcManager.Instance;

            return session;
        };

        Bots.Enqueue( this );
    }

    /// <summary>
    /// 시작한다
    /// </summary>
    public override bool OnStart()
    {
        Console.Title = $"{ Name } Started...";

        while ( true )
        {
            var command = CommandListner.Listen();
            if ( command is null )
                continue;

            command.Execute();
        }

        return true;
    }

    /// <summary>
    /// 접속한다
    /// </summary>
    public void Connect( IPEndPoint endPoint, int dummyPlayerCount = 0 )
    {
        for ( int n = 0; n < dummyPlayerCount; n += 1 )
        {
            Connect( 1 );
        }
    }

    /// <summary>
    /// 첫번째 Bot에 작업을 실행시킨다.
    /// </summary>
    public static bool ExecuteOnFirstBot( Action< BotPc > job )
    {
        Bots.ForEach( client =>
        {
            if ( client.BotManager.PostOnFirstBot( job ) )
                return EForEachResult.Break;
            else
                return EForEachResult.Continue;
        } );

        return true;
    }

    /// <summary>
    /// 초기화한다
    /// </summary>
    protected override bool OnPreInit()
    {
        SetThreadCount( Config.WorkerThreadCount );
		Actor.Initialize( Config.WorkerThreadCount );
		Console.Title = $"{ Name } Initializing...";

        return true;
    }

    /// <summary>
    /// 초기화한다
    /// </summary>
    protected override bool OnPostInit()
    {
        CommandListner = new BotCommandListener( this );
        CommandListner.Init();

        ThreadManager.Launch(
            () => BotWorker.DoWorkerJob( this ),
            "BotWorker",
            Config.WorkerThreadCount );

        return true;
    }

}