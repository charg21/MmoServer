﻿using Mala.Collection;
using Mala.Core;
using Mala.Net;
using System.Net;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.DependencyInjection;


public class BotApplicationOptions
{
    /// <summary>
    /// 커맨드 라인 아규먼트
    /// </summary>
    public string[]? Args { get; init; }

    /// <summary>
    /// The environment name.
    /// </summary>
    public string? EnvironmentName { get; init; }

    /// <summary>
    /// The application name.
    /// </summary>
    public string? ApplicationName { get; init; }

    /// <summary>
    /// The Degisn data path
    /// </summary>
    public string? DesignPath { get; init; }
}


/// <summary>
///
/// </summary>
public class BotApplicationBuilder
{
    public BotApplicationOptions options { get; init; }

    /// <summary>
    /// 생성자
    /// </summary>
    public BotApplicationBuilder( BotApplicationOptions optoins )
    {
    }

    /// <summary>
    ///
    /// </summary>
    public BotApplication Build()
    {
        return null; // new DummyClient2();
    }
}

/// <summary>
/// 봇 클라이언트
/// </summary>
public class BotApplication
{
    public static BotApplicationBuilder CreateBuilder( string[] args )
        => new( new() { Args = args } );

    public void Start()
    {
    }
}
