﻿using Mala.Collection;
using Mala.Core;
using Mala.Net;
using System.Net;
using System.Runtime.CompilerServices;

//public class DummyClientOptions
//{
//    /// <summary>
//    /// 커맨드 라인 인자
//    /// </summary>
//    public string[]? Args { get; init; }
//}


///// <summary>
/////
///// </summary>
//public class DummyClientBuilder
//{
//    public DummyClientOptions options { get; init; }

//    /// <summary>
//    /// 생성자
//    /// </summary>
//    public DummyClientBuilder( DummyClientOptions optoins )
//    {
//    }

//    /// <summary>
//    ///
//    /// </summary>
//    public DummyClient Build()
//    {
//        return new DummyClient();
//    }
//}


///// <summary>
///// 더미 클라이언트
///// </summary>
//public class DummyClient : Service
//{
//    public static DummyClientBuilder CreateBuilder( string[] args )
//        => new( new() { Args = args } );

//    static WaitFreeQueue< DummyClient > DummyClients = new();

//    Connector _connector = new();
//    Func< BotSession > SessionFactory;
//    public BotPlayerManager PlayerManager { get; set; }
//    int _dummyPlayerCount = 0;

//    public DummyClient( int dummyPlayerCount = 0 )
//    {
//        _dummyPlayerCount = dummyPlayerCount;

//        PlayerManager = BotPlayerManager.Instance;

//        SessionFactory = () =>
//        {
//            var session = new BotSession();
//            session.BotManager = PlayerManager;

//            return session;
//        };

//        DummyClients.Enqueue( this );
//    }

//    /// <summary>
//    /// 시작한다
//    /// </summary>
//    public override void Start()
//    {
//        var ipAddress = IPAddress.Parse( Config.ServerIp );
//        var endPoint  = new IPEndPoint( ipAddress, Config.ServerPort );

//        var lastTick = Environment.TickCount64;
//        var simulationTick = 0L;
//        var context = new MainThreadContext();
//        Console.Title = $"{ Name } Started...";

//        Connect( endPoint, _dummyPlayerCount );

//        for ( ;; )
//        {
//            Thread.Sleep( Math.Max( 1, (int)( 50 - simulationTick ) ) );

//            var deltaTick = GetDeltaTick( ref lastTick );

//            PlayerManager.Tick( context, deltaTick );

//            //simulationTick = Environment.TickCount64 - lastTick;
//        }
//    }

//    /// <summary>
//    ///
//    /// </summary>
//    private static long GetDeltaTick( ref long lastTick )
//    {
//        var curTick = Environment.TickCount64;
//        var deltaTick = curTick - lastTick;
//        lastTick = curTick;

//        return deltaTick;
//    }

//    /// <summary>
//    /// 접속한다
//    /// </summary>
//    public void Connect( IPEndPoint endPoint, int dummyPlayerCount = 0 )
//    {
//        if ( dummyPlayerCount == 0 )
//            dummyPlayerCount = Math.Max( 1, Config.DummyPcCount );

//        _connector.Connect( endPoint,
//                   SessionFactory,
//                   dummyPlayerCount );
//    }


//    /// <summary>
//    /// 첫번째 플레이어에 작업을 실행시킨다.
//    /// </summary>
//    public static bool ExecuteOnFirstPlayer( Action< BotPlayer > job )
//    {
//        bool executed = false;
//        DummyClients.ForEach( client =>
//        {
//            executed = client.PlayerManager.PostOnFirstBot( job );
//            if ( executed )
//                return EForEachResult.Break;
//            else
//                return EForEachResult.Continue;
//        } );

//        return executed;
//    }

//    /// <summary>
//    /// 초기화한다
//    /// </summary>
//    public override bool Initialize()
//    {
//        Console.Title = $"{ Name } Initializing...";

//        return true;
//    }

//    /// <summary>
//    /// 설정을 불러온다.
//    /// </summary>
//    public override bool LoadConfig( string[] args )
//    {
//        Config.Load( args, Name );
//        return true;
//    }
//}
