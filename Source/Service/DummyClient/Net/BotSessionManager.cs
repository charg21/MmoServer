﻿using System;
using System.Net;
using Mala.Logging;
using Mala.Net;
using Mala.Threading;

/// <summary>
/// 봇 세션 매니저
/// </summary>
public partial class BotSessionManager
{
    /// <summary>
    /// 정적 인스턴스
    /// </summary>
    public static BotSessionManager Instance = new();
    /// <summary>
    ///
    /// </summary>
    public Dictionary< ulong, BotSession > _sessionDict = new();
    /// <summary>
    ///
    /// </summary>
    public ulong _sessionIdIssuer = 0;
    static HashSet< BotSession > _botSessions = new();

    private static Lock _lock = new();

    /// <summary>
    /// 생성한다.
    /// </summary>
    public static BotSession Generate()
    {
        var client = new BotSession();
        client.SessionId = Interlocked.Increment( ref Instance._sessionIdIssuer );

        lock ( _lock )
        {
            _botSessions.Add( client );
        }

        return client;
    }

}
