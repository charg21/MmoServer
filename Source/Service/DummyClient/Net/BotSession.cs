﻿using System;
using System.Net;
using Mala.Logging;
using Mala.Net;
using Mala.Threading;

/// <summary>
/// 서버 세션
/// </summary>
public partial class BotSession : PacketSession, IExecutor
{
    public ulong SessionId = 0;
    public BotPc Pc { get; set; }
    public BotPcManager BotManager { get; set; }

    public void OnAssignThread( int threadId )
    {
        BotManager = BotPcManager.Instance;
    }

    public override void OnConnected( EndPoint endPoint, bool success )
    {
        Log.Info( $"OnConnected : {endPoint}" );

        BotManager = BotPcManager.Instance;
        Pc = BotManager.Generate();
        Pc.Session = this;

        BotManager.AddBot( Pc );

        RegisterTick( new( 100 ) );
    }

    public void Send( ref SendSegment segment )
    {
        base.Send( ref segment );
    }

    public void Send( SendSegment segment )
    {
        base.Send( ref segment );
    }

    public override void OnDisconnected( EndPoint endPoint )
    {
        Log.Info( $"OnDisConnected : {endPoint}" );
    }

    public override void OnRecvPacket( ref ReadOnlySpan< byte > buffer )
    {
        var packetId = BitConverter.ToUInt16( buffer.Slice( HeaderLength, HeaderType ) );
        if ( !CanDispatch( packetId ) )
            return;

        var packet = PacketFactory.MakePacket( packetId );
        packet.Read( ref buffer );
        Pc.PacketQueue.Enqueue( packet );
    }

    public override void OnSend( int numOfBytes )
    {
    }

    private static bool CanDispatch( ushort packetId )
    {
        switch ( (EPacketType)( packetId ) )
        {
            case EPacketType.S_EnterWorld:
            case EPacketType.S_Move:
            case EPacketType.S_StartSkill:
            case EPacketType.S_Login:
            case EPacketType.S_Chat:
            case EPacketType.S_LoadPc:
            case EPacketType.S_ListChannel:
            case EPacketType.S_Spawn:
            case EPacketType.S_Despawn:
                return true;
        }

        return false;
    }
}
public partial class BotSession : PacketSession, IExecutor
{
    Actor JobExecutor { get; set; } = new();

    /// <summary>
    /// 마지막 갱신 틱
    /// </summary>
    public i64 LastTick { get; set; } = 0;
    public bool _tickRegistered = false;
    Action TickJob;


    public BotSession()
    {
        TickJob = Tick;
    }

    public void RegisterTick( TimeSpan afterTime )
    {
        if ( _tickRegistered )
            return;

        PostAfter( afterTime, TickJob );

        _tickRegistered = true;
    }

    public virtual void OnTick( TimeSpan deltaTime )
    {
        Pc.Tick( deltaTime );
    }

    public float GetDeltaTime() => GetDeltaTick() / 1000f;

    public long GetDeltaTick()
        => ThreadPoolExecutorService._tlsContext.LastTickCount - LastTick;


    public void Execute() => JobExecutor.Execute();

    public void Tick()
    {
        _tickRegistered = false;

        var curTick = Environment.TickCount64;
        var deltaTick = new TimeSpan( curTick - LastTick );
        OnTick( deltaTick );

        LastTick = curTick;

        // if ( CanTick() )
        PostAfter( Consts.Npc.TickInterval, TickJob );
    }

    public void PostAfter( TimeSpan afterTime, Action action )
        => JobExecutor.PostAfter( afterTime, action );

    public void PostAfter< T1 >( TimeSpan afterTime, Action< T1 > action, T1 t1 )
        => JobExecutor.PostAfter( afterTime, action, t1 );

    public void PostAfter< T1, T2 >( TimeSpan afterTime, Action< T1, T2 > action, T1 t1, T2 t2 )
        => JobExecutor.PostAfter( afterTime, action, t1, t2 );

    public void Post( Action action )
        => JobExecutor.Post( action );

    public void Post< T1 >( Action< T1 > action, T1 t1 )
        => JobExecutor.Post( action, t1 );

    public void Post< T1, T2 >( Action< T1, T2 > action, T1 t1, T2 t2 )
        => JobExecutor.Post( action, t1, t2 );

    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > action, T1 t1, T2 t2, T3 t3 )
        => JobExecutor.Post( action, t1, t2, t3 );

    public void Post< T1, T2, T3, T4 >( Action< T1, T2, T3, T4 > action, T1 t1, T2 t2, T3 t3, T4 t4 )
        => JobExecutor.Post( action, t1, t2, t3, t4 );

    public void Post< T1, T2, T3, T4, T5 >( Action< T1, T2, T3, T4, T5 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5 )
        => JobExecutor.Post( action, t1, t2, t3, t4, t5 );
}
