﻿using Mala.Net;
public partial class S_MoveHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var serverSession = session as BotSession;
        var move = packet as S_Move;
        var pc = serverSession.Pc;

        /// 추후 타인의 시야 갱신이 필요한경우, 반영
        if ( pc.ObjectId != move._actorId )
            return;

        pc.Pos = new( move._pos._x, 0f, move._pos._z );
    }

}

