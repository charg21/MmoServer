using Mala.Net;
public partial class S_PingHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
		session?.Send( new ValueC_Pong().Write() );
	}

}

