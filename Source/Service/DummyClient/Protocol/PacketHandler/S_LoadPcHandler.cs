using Mala.Logging;
using Mala.Net;
public partial class S_LoadPcHandler
{
	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var serverSession = session as BotSession;
        var ack = packet as S_LoadPc;
        var pc = serverSession.Pc;

        if ( ack._result != ELogicResult.Success )
            return;

        pc.LoadCompleted = true;
        pc.ImportFrom( ack._pktPc );
    }

}

