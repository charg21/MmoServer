using Mala.Net;
public partial class S_ListChannelHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var serverSession = session as BotSession;
        var listChannel = packet as S_ListChannel;
        var pc = serverSession.Pc;

        pc.ChannelId = listChannel._channels[ (int)( pc.BotId ) % listChannel._channels.Count ];
    }

}

