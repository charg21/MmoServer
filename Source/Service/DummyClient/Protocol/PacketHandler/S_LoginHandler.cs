using Mala.Logging;
using Mala.Net;
public partial class S_LoginHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var serverSession = session as BotSession;
        var sLogin = packet as S_Login;
        var pc = serverSession.Pc;

        if ( sLogin._result != ELogicResult.Success )
        {
            pc.Fsm.Transit( EBotFsmState.Login );
            return;
        }

        pc.LoginCompleted = true;
        if ( sLogin._pcIds.Count != 0 )
            pc.ObjectId = (ObjectId)sLogin._pcIds[ 0 ];

        Log.Debug( $"Login Ack Bot{ pc.BotId } ActorId:{ pc.ObjectId } " );
    }

}

