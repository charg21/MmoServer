using Mala.Logging;
using Mala.Net;

public partial class S_MessageHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
		var serverMsg = packet as S_Message;

		Log.Info( $"MyId [??] Received Msg... Text[ { serverMsg?._message } ]" );
	}

}

