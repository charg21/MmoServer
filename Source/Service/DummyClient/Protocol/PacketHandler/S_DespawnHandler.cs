using Mala.Net;
public partial class S_DespawnHandler
{
    public static void DoHandle( PacketHeader packet, PacketSession session )
    {
        var serverSession = session as BotSession;
        var spawn         = packet as S_Despawn;
        var pc            = serverSession.Pc;

        if ( pc.ObjectId == spawn._actorId )
            return;

        pc.BotPcViewComponent.Remove( spawn._actorId );
    }

}

