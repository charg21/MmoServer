using Mala.Net;
public partial class S_SpawnHandler
{

	public static void DoHandle( PacketHeader packet, PacketSession session )
	{
        var serverSession = session as BotSession;
        var spawn         = packet as S_Spawn;
        var pc            = serverSession.Pc;

        if ( pc.ObjectId == spawn._other._actorId )
            return;

        pc.BotPcViewComponent.Add( in spawn._other );
    }

}

