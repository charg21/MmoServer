using Mala.Logging;
using Mala.Net;
public partial class S_EnterWorldHandler
{
    static int _enteredCount = 0;

    public static void DoHandle( PacketHeader packet, PacketSession session )
    {
        var serverSession = session as BotSession;
        var enter         = packet as S_EnterWorld;
        var pc            = serverSession.Pc;

        pc.Pos     = new( enter._my._pos._x, 0f, enter._my._pos._z );
        pc.ToPos   = pc.Pos;
        pc.Entered = true;

        Log.Info( $"Pc Bot{ pc.ObjectId } Pos[ { pc.Pos } ]" );

        var resultCount = Interlocked.Increment( ref _enteredCount );
        if ( resultCount % 10 == 0 )
            Console.Title = $"DummyClient Entered: { resultCount }";
        if ( Config.DummyPcCount == resultCount )
            Console.Title = $"DummyClient Entered: { resultCount }";
    }
}