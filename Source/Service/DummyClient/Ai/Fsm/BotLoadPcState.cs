﻿using Mala.Logging;

/// <summary>
/// PC 정보 로드 상태
/// </summary>
public class BotLoadPcState : BotFsmState
{
    public BotLoadPcState( BotPc owner )
    {
        Owner = owner;
    }

    public override void OnEnter()
    {
        //var listChannel = new ValueC_ListChannel();
        //Owner?.Send( listChannel.Write() );

        var loadPc = new ValueC_LoadPc();
        loadPc._pcId = (ulong)Owner.ObjectId;
        Owner?.Send( loadPc.Write() );

        Log.Info( $"Load Start... Bot{ Owner.BotId } ChannelId { Owner.ChannelId }" );
    }

    /// <summary>
    ///
    /// </summary>
    public override void TransitIfNeeded()
    {
        if ( !Owner.LoadCompleted )
            return;

        Log.Info( $"Load Pc Ok... Bot{ Owner.BotId } ChannelId { Owner.ChannelId }" );
        Owner.Fsm.Transit( EBotFsmState.EnterWorld );
    }
}

