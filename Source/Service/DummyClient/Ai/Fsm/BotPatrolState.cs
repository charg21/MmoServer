﻿using Mala.Logging;

public class BotPatrolState : BotFsmState
{
    SimpleTimer _changeTimer = new();

    public BotPatrolState( BotPc owner )
    {
        Owner = owner;
    }

    public override void OnEnter()
    {
        Log.Info( $"Enter Patrol State" );

        _changeTimer.Restart(
            5000,
            () =>
            {
                Owner.Fsm.Transit( EBotFsmState.Combat );
            } );
    }

    public override void OnTick( TimeSpan deltaTime )
    {
        Owner.MoveToRandPos( deltaTime );
        _changeTimer.Update( deltaTime );
    }

    public override void OnExit()
    {
        Log.Info( $"Exit State" );
    }
}
