﻿using Mala.Common.Ai;


public class BotCreateState : BotFsmState
{
    public BotCreateState( BotPc owner )
    {
        Owner = owner;
    }

    public override void TransitIfNeeded()
    {
        //Owner.Fsm.Transit( EBotState.EnterWorld );
        Owner.Fsm.Transit( EBotFsmState.Login );
        //Owner.Fsm.Transit( EBotState.LoadPc );
    }
}
