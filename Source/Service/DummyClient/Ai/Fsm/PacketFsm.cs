﻿using Mala.Core;
using Mala.Common.Ai;

/// <summary>
/// 패킷 FSM
/// </summary>
public class PacketFsm< TActor, TState, TFsmState > : Fsm< TActor, TState, TFsmState >, IPacketEventListener
    where TActor : new()
    where TState : Enum
    where TFsmState : FsmState< TActor, TState >
{
    /// <summary>
    /// 생성자
    /// </summary>
    public PacketFsm( TActor owner )
    {
        Owner = owner;
    }

    /// <summary>
    /// 패킷 수신시
    /// </summary>
    public virtual void OnReceivePacket( PacketHeader packet )
    {
        var state = CurState as PacketFsmState< TActor, TState >;
        state?.OnReceivePacket( packet );
    }

    /// <summary>
    /// 패킷 송신시
    /// </summary>
    public virtual void OnSend( PacketHeader packet )
    {
        var state = CurState as PacketFsmState< TActor, TState >;
        state?.OnSend( packet );
    }
}

public class PacketFsmState< TActor, TState > : FsmState< TActor, TState >, IPacketEventListener
    where TActor : new()
    where TState : Enum
{
    public virtual void OnReceivePacket( PacketHeader packet )
    {
    }

    public virtual void OnSend( PacketHeader packet )
    {
    }
}