﻿using Mala.Logging;

public class BotCombatState : BotFsmState
{
    SimpleTimer _attackTimer = new();
    ulong _targetId = 0;

    public BotCombatState( BotPc owner )
    {
        Owner = owner;
    }

    public override void OnEnter()
    {
        _attackTimer.Restart(
            500,
            () =>
            {
                // 아무나 잡고 공격
                _targetId = (ulong)Owner.BotPcViewComponent.SelectOne();
                if ( _targetId == 0 )
                {
                    Owner.Fsm.Transit( EBotFsmState.Patrol );
                    return;
                }

                Log.Info( $"Find TargetId:{_targetId} " );

                var startSkill = new ValueC_StartSkill();
                startSkill._targetId = _targetId;
                startSkill._skillDesignId = 1;

                Owner.Send( startSkill.Write() );

                // 다음 상태로 변경
                Owner.Fsm.Transit( EBotFsmState.Patrol );
            } );
    }

    public override void OnTick( TimeSpan deltaTime )
    {
        _attackTimer.Update( deltaTime );
    }

    public override void OnExit()
    {
        _targetId = 0;
    }

}
