﻿using Mala.Core;
using Mala.Common.Ai;
using Mala.Logging;

/// <summary>
/// 로그인 상태
/// </summary>
public class BotLoginState : BotFsmState
{
    public int _tryCount = 0;

    public BotLoginState( BotPc owner )
    {
        Owner = owner;
    }

    public override void OnEnter()
    {
        _tryCount += 1;

        var login = new ValueC_Login(){ _account  = $"Bot{ Owner.BotId }", _password = $"1q2w3e4r" };
        Owner?.Send( login.Write() );

        Log.Info( $"Login Start... Bot{ Owner.BotId } TryCount:{ _tryCount }" );

    }

    /// <summary>
    ///
    /// </summary>
    public override void TransitIfNeeded()
    {
        if ( Owner.LoginCompleted )
        {
            Log.Info( $"Login Ok... Bot{ Owner.BotId }" );
            // Owner.Fsm.Transit( EDummyState.EnterWorld );
            Owner.Fsm.Transit( EBotFsmState.LoadPc );
        }
    }
}

