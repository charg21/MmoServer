﻿using Mala.Common.Ai;
using Mala.Logging;

public class BotEnterWorldState : BotFsmState
{
    public BotEnterWorldState( BotPc owner )
    {
        Owner = owner;
    }

    public override void OnEnter()
    {
        var enter = new ValueC_EnterWorld(){ _channel = Owner.ChannelId };

        Owner?.Send( enter.Write() );

        Log.Info( $"Enter State" );
    }

    public override void TransitIfNeeded()
    {
        if ( Owner.Entered )
        {
            Owner.Fsm.Transit( EBotFsmState.Patrol );
        }
    }
}
