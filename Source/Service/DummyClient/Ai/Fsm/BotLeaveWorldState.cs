﻿using Mala.Common.Ai;

public class BotLeaveWorldState : BotFsmState
{
    public BotLeaveWorldState( BotPc owner )
    {
        Owner = owner;
    }

    public override void OnEnter()
    {
        var enter = new ValueC_LeaveWorld();
        Owner?.Send( enter.Write() );
    }

    public override void TransitIfNeeded()
    {
    }
}
