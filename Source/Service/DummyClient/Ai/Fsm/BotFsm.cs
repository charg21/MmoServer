﻿using Mala.Common.Ai;

/// <summary>
/// 봇 상태
/// </summary>
public enum EBotFsmState
{
    Create,
    Login,
    LoadPc,
    EnterWorld,
    Patrol,
    Combat,
    Talk,
    LeaveWorld,
}

/// <summary>
/// 봇의 FSM
/// </summary>
public class BotFsm : PacketFsm< BotPc, EBotFsmState, BotFsmState >
{
    /// <summary>
    /// 생성자
    /// </summary>
    public BotFsm( BotPc owner ) : base( owner )
    {
        Add( EBotFsmState.Create, new BotCreateState( owner ) );
        Add( EBotFsmState.Login, new BotLoginState( owner ) );
        Add( EBotFsmState.LoadPc, new BotLoadPcState( owner ) );
        Add( EBotFsmState.EnterWorld, new BotEnterWorldState( owner ) );
        Add( EBotFsmState.Patrol, new BotPatrolState( owner ) );
        Add( EBotFsmState.Combat, new BotCombatState( owner ) );
        Add( EBotFsmState.LeaveWorld, new BotLeaveWorldState( owner ) );

        Transit( EBotFsmState.Create );
    }
}
