﻿using Mala.Collection;
using Mala.Threading;

namespace Mala.Common.Ai;

public class MoveToRandPos : BehaviorTreeAction
{
    BotPc _owner;

    public MoveToRandPos( BlackBoard blackboard, BotPc owner )
    : base( blackboard )
    {
        _owner = owner;
    }

    public override EBehaviorNodeState Update( TimeSpan deltaTime )
    {
        _owner.MoveToRandPos( deltaTime );

        return EBehaviorNodeState.Success;
    }

    public override EBehaviorNodeState UpdateMessage( JobEntry message )
    {
        return EBehaviorNodeState.Success;
    }
}

public class NextPage : BehaviorTreeAction
{
    BehaivorTreePage CurPage;
    EBotFsmState PageId;

    public NextPage( BlackBoard blackboard, EBotFsmState pageId, BehaivorTreePage page )
    : base( blackboard )
    {
        CurPage = page;
        PageId = pageId;
    }

    public override EBehaviorNodeState Update( TimeSpan deltaTime )
    {
        CurPage.Owner.ChangePage( (int)PageId );

        return EBehaviorNodeState.Success;
    }

    public override EBehaviorNodeState UpdateMessage( JobEntry message )
    {
        return EBehaviorNodeState.Success;
    }
}

