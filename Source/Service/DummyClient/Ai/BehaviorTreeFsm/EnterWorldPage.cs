﻿using Mala.Collection;
using Mala.Common.Ai;

public class CreatePage : BehaivorTreePage
{
    public CreatePage()
    {
        Build();
    }

    public void Build()
    {
        var sequence = new Sequence();
        sequence.AddChild( new NextPage( _behaviorTree.BlackBoard, EBotFsmState.LoadPc, this ) );
        _behaviorTree.Root = sequence;
    }
}


public class LoadPcPage : BehaivorTreePage
{
    BotPc Bot;

    public LoadPcPage( BotPc bot )
    {
        Bot = bot;
        PageId = (int)( EBotFsmState.LoadPc );

        Build();
    }

    public void Build()
    {
        var sequence = new Sequence();
        sequence.AddChild( new NextPage( _behaviorTree.BlackBoard, EBotFsmState.EnterWorld, this ) );
        _behaviorTree.Root = sequence;
    }
}

public class EnterWorldPage : BehaivorTreePage
{
    BotPc Bot;

    public EnterWorldPage( BotPc bot )
    {
        Bot = bot;
        PageId = (int)( EBotFsmState.EnterWorld );

        _behaviorTree.Root = new Sequence().AddChild(
            new NextPage( _behaviorTree.BlackBoard, EBotFsmState.Patrol, this ) );
    }

    /// <summary>
    /// 페이지 진입시 호출되는 콜백
    /// </summary>
    public override void OnEnter()
    {
        var enter = new ValueC_EnterWorld();
        enter._channel = Bot.ChannelId;
        Bot?.Send( enter.Write() );
    }
}

public class RoamingPage : BehaivorTreePage
{
    BotPc Bot;

    public RoamingPage( BotPc bot )
    {
        Bot = bot;
        PageId = (int)( EBotFsmState.Patrol );

        //var sequence
        _behaviorTree.Root = new Sequence().AddChild(
            new MoveToRandPos( _behaviorTree.BlackBoard, bot ) );
    }
}

public class CombatPage : BehaivorTreePage
{
    BotPc Bot;

    public CombatPage( BotPc bot )
    {
        Bot = bot;
        PageId = (int)( EBotFsmState.Patrol );

        //var sequence
        _behaviorTree.Root = new Sequence().AddChild(
            new MoveToRandPos( _behaviorTree.BlackBoard, bot ) );
    }
}

public class FindTargetTask : Selector
{

}
