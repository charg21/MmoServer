﻿public class ConfigCommand : BotCommand
{
    string fieldName = string.Empty;
    string fieldValue = string.Empty;

    public ConfigCommand( BotClient botClient, string[] strings )
    : base( botClient )
    {
        if ( strings.Count() < 3 )
            return;

        fieldName = strings[ 1 ];
        fieldValue = strings[ 2 ];
    }

    public override void Execute()
    {
        Config.SetField( fieldName, fieldValue );
    }
}