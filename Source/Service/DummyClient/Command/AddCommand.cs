﻿public class AddCommand : BotCommand
{
    public AddCommand( BotClient botClient, string[] strings )
    : base( botClient )
    {
        if ( strings.Count() < 2 )
            return;

        DummyCount = int.Parse( strings[ 1 ] );
    }

    public int DummyCount { get; set; } = 0;

    public override void Execute()
    {
        for ( int i = 0; i < DummyCount; i += 1 )
            BotClient.Connect( 1 );

        //var thread = new Thread( () =>
        //{
        //    var dummyClinet = new DummyClient( DummyCount );
        //    dummyClinet.Initialize();
        //    dummyClinet.Start();
        //} );

        // thread.Name = "Dummy";
        // thread.Start();
    }
}
