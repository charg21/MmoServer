﻿using Mala.Core;
using Microsoft.Extensions.DependencyInjection;
using System.Collections;

public class BotCommandListener
{
    public Dictionary< string, Func< string[], BotCommand > > Factory = new();
    private BotClient botClient;

    public BotCommandListener( BotClient botClient )
    {
        this.botClient = botClient;
    }

    public int Count => throw new NotImplementedException();

    public bool IsReadOnly => throw new NotImplementedException();

    public ServiceDescriptor this[ int index ] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

    public void Init()
    {
        Factory.Add( "add", ( strings )    => new AddCommand( botClient, strings )    );
        Factory.Add( "config", ( strings ) => new ConfigCommand( botClient, strings ) );
        Factory.Add( "cheat", ( strings )  => new CheatCommand( botClient, strings )  );
    }

    public BotCommand? Parse( string? command )
    {
        var cmdStrings = command.Split( ' ', StringSplitOptions.RemoveEmptyEntries );
        if ( cmdStrings.Length == 0 )
            return null;

        var commandType = cmdStrings[ 0 ].ToLower();
        if ( !Factory.TryGetValue( commandType, out var factory ) )
            return null;

        return factory.Invoke( cmdStrings );
    }

    public BotCommand Listen()
    {
        var commands = Console.ReadLine();
        return Parse( commands );
    }

    //public int IndexOf( ServiceDescriptor item )
    //{
    //    throw new NotImplementedException();
    //}

    //public void Insert( int index, ServiceDescriptor item )
    //{
    //    throw new NotImplementedException();
    //}

    //public void RemoveAt( int index )
    //{
    //    throw new NotImplementedException();
    //}

    //public void Add( ServiceDescriptor item )
    //{
    //    throw new NotImplementedException();
    //}

    //public void Clear()
    //{
    //    throw new NotImplementedException();
    //}

    //public bool Contains( ServiceDescriptor item )
    //{
    //    throw new NotImplementedException();
    //}

    //public void CopyTo( ServiceDescriptor[] array, int arrayIndex )
    //{
    //    throw new NotImplementedException();
    //}

    //public bool Remove( ServiceDescriptor item )
    //{
    //    throw new NotImplementedException();
    //}

    //public IEnumerator<ServiceDescriptor> GetEnumerator()
    //{
    //    throw new NotImplementedException();
    //}

    //IEnumerator IEnumerable.GetEnumerator()
    //{
    //    throw new NotImplementedException();
    //}
}
