﻿
public abstract class BotCommand
{
    public string Type = string.Empty;
    public BotClient BotClient;

    public BotCommand( BotClient botClient )
    {
        BotClient = botClient;
    }

    public abstract void Execute();
}
