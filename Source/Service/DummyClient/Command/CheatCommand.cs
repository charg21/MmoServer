﻿using System.Text;

/// <summary>
///
/// </summary>
public class CheatCommand : BotCommand
{
    public CheatCommand( BotClient botClient, string[] strings )
    : base( botClient )
    {
        if ( strings.Count() < 2 )
            return;

        Cheat = BuildCommand( strings );
    }

    public string Cheat { get; set; } = string.Empty;

    public override void Execute()
    {
        BotClient.ExecuteOnFirstBot( ( BotPc pc ) =>
        {
            var cheatChat = new ValueC_Cheat(){ _command = Cheat };
            pc.Send( cheatChat.Write() );
        } );
    }


    public static string BuildCommand( string[] strings )
    {
        var commandBuilder = new StringBuilder();

        for ( int i = 1; i < strings.Length; i += 1 )
        {
            commandBuilder.Append( $"{ strings[ i ] } " );
        }

        // 마지막 한글자 지우기
        commandBuilder.Remove( strings.Length, 1 );

        return commandBuilder.ToString();
    }
}
