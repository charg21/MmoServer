﻿using Mala.Collection;
using Mala.Common.Ai;
using Mala.Core;
using Mala.Logging;
using Mala.Math;
using Mala.Net;


/// <summary>
/// 봇 PC
/// </summary>
public class BotPc : IBotActor
{
    /// <summary>
    /// 봇 식별자
    /// </summary>
    public ulong BotId { get; set; } = 0;
    public int ChannelId { get; set; } = -1;
    public ulong UserId { get; set; } = 0;
    public ObjectId ObjectId { get; set; } = new();
    public Position Pos { get; set; } = new();
    public Direction Dir { get; set; } = new();
    public Position ToPos { get; set; } = new();
    public BotSession Session { get; set; }
    public WaitFreeQueue< PacketHeader > PacketQueue { get; private set; } = new();
    public bool Entered { get; set; } = false;
    public bool LoginCompleted { get; set; } = false;
    public bool LoadCompleted { get; internal set; }
    public IPacketEventListener PacketListener { get; set; }

    BehaviorTree BehaviorTree;
    PageBehaviorTree PageBehaviorTree;

    public BotPcViewComponent BotPcViewComponent { get; init; }

    public BotFsm Fsm { get; set; }

    /// <summary>
    /// 생성자
    /// </summary>
    public BotPc()
    {
        Fsm = new( this );
        BotPcViewComponent = new( this );
        PacketListener = Fsm;
    }

    void BuildAi()
    {
        if ( Config.UseBehaviorTree )
        {
            BehaviorTree = new();
            BehaviorTree.Root =
                new Sequence().AddChild( new MoveToRandPos( BehaviorTree.BlackBoard, this ) );

            //BehaviorTree.Tick( 0.1f );
            PageBehaviorTree = new();
            PageBehaviorTree.AddAndSetPage( new EnterWorldPage( this ) );
            PageBehaviorTree.AddPage( new RoamingPage( this ) );
        }
        else
        {
            Fsm = new( this );
        }
    }

    public void Send( SendSegment segment )
        => Session?.Send( ref segment );

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public void Tick( TimeSpan deltaTime )
    {
        DispatchPcaket();

        if ( Config.UseBehaviorTree )
            BehaviorTree.Tick( deltaTime );
        else
            Fsm.Tick( deltaTime );
    }

    public void Post( Action job ) => Session.Post( job );

    private void DispatchPcaket()
    {
        while ( PacketQueue.TryDequeue( out var packet ) )
        {
            var handlerMap = ServerPacketDispatcher.Instance.HandlerMap;
            if ( handlerMap.TryGetValue( packet.Type, out var handler ) )
                handler.Invoke( packet, Session );
            else
                Log.Critical( $"Invalid Packet Type:{ packet.Type } PcId: { ObjectId }" );

            PacketListener.OnReceivePacket( packet );
        }
    }

    public void MoveToRandPos( TimeSpan deltaTick )
    {
        if ( !Entered )
            return;

        if ( Pos == ToPos )
            ToPos = WorldHelper.GetNextPos( Pos, (EDirection)( FastRand.Next() % 4 ), Globals.DummyMoveDistance );

        var ( nextPos, dir ) = WorldHelper.GetNextPos( Pos, ToPos, deltaTick );

        /// 이동 벡터로 현재 위치를 업데이트
        SetPos( nextPos, dir );
    }

    public void SetPos( Position pos, Direction dir )
    {
        Dir = dir;
        Pos = pos;

        var move = new ValueC_Move
        {
            _pos   = Pos.ToPktVector3(),
            _toPos = ToPos.ToPktVector3(),
            _yaw   = MathHelper.Vector2ToYaw( Dir ),
        };

        Send( move.Write() );
    }

    public void ImportFrom( in PktPc pc )
    {
        ObjectId = (ObjectId)pc._actorId;
        Pos = new( pc._pos._x, pc._pos._y, pc._pos._z );
    }
}


