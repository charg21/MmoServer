﻿using Mala.Common.Ai;
using Mala.Net;


public class Network : ComponentBase< ComponentActor >
{
    public BotSession Session { get; set; }

    public Dictionary< short, int > PacketCount { get; set; } = new();

    public override void Tick( TimeSpan deltaTime )
    {
    }

    public void Send( SendSegment segment )
        => Session?.Send( ref segment );

    public void Send( ref SendSegment segment )
        => Session?.Send( ref segment );

    /// <summary>
    /// 수신한다( 대기 )
    /// </summary>
    public async void Receive( IPacketHeader packet, Action continueJob )
    {
        //Task.Run( () =>
        //{

        //} );
        //if ( Owner.ContinueWithJob is not null )
        //{
        //}

        //Owner.ContinueWithJob = continueJob;
        //Owner.Yield( continueJob );
    }

    /// <summary>
    /// 일정시간 동안 수신 대기한다.
    /// </summary>
    public void ReceiveWhile< T >( TimeSpan expireTime, ref IPacketHeader packet )
    {
    }

}
