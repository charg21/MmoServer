﻿using System.Collections.Concurrent;

public interface IBotActor
{
    public void Tick( TimeSpan deltaTime );
}

public class ComponentBase< TOwner > where TOwner : ComponentActor
{
    public TOwner Owner { get; set; }

    public ComponentBase()
    {
    }

    public virtual void Tick( TimeSpan deltaTime )
    {
    }
}

public enum EBotResult
{
    Success,
    Pending,
}


/// <summary>
///
/// </summary>
public class ActorBase
{
    /// <summary>
    /// 스레드별 실해중인 액터
    /// </summary>
    [ThreadStatic]
    ActorBase? LCurrentActor = null;

    /// <summary>
    /// 작업 큐
    /// </summary>
    public ConcurrentQueue< Func< EBotResult > > _jobQueue { get; set; } = new();

    /// <summary>
    /// 이어서 실행할 작업
    /// </summary>
    public List< Action? > ContinueWithJobs = null;

    /// <summary>
    ///
    /// </summary>
    int _postJobCount = 0;

    /// <summary>
    /// 작업을 추가한다.
    /// </summary>
    public void Post( Func< EBotResult > job )
    {
        var canExecute = AddPostCount();

        _jobQueue.Enqueue( job );

        if ( canExecute )
        {
            if ( LCurrentActor is not null )
            {
                using var _ = ExecutionContext.SuppressFlow();

                ThreadPool.UnsafeQueueUserWorkItem( _ => Execute(), null );
            }
            else
            {
                Execute();
            }
        }
    }

    public void Yield()
    {
        ThreadPool.UnsafeQueueUserWorkItem( _ =>
        {
            foreach ( var job in ContinueWithJobs )
                job();

            Execute();
        },
        null );
    }

    /// <summary>
    /// Job 개수를 증가시킨다.
    /// </summary>
    /// <returns> 잡 실행 가능 여부를 반환한다. </returns>
    bool AddPostCount() => Interlocked.Increment( ref _postJobCount ) == 1;

    /// <summary>
    /// Job 개수를 감소시킨다.
    /// </summary>
    /// <returns> 예약된 태스크가 전부 실행되었는지 여부를 반환한다. </returns>
    bool ReleasePostCount( int executeCount ) => Interlocked.Add( ref _postJobCount, -executeCount ) <= 0;

    /// <summary>
    /// Task를 실행한다.
    /// </summary>
    void Execute()
    {
        LCurrentActor = this;

        ValueTuple< EBotResult, int > result = new();
        for ( ;; )
        {
            result = ExecuteInternal();
            if ( ReleasePostCount( result.Item2 ) )
                break;

            /// 실해중인거.... 패스
            if ( result.Item1 == EBotResult.Pending )
            {
                LCurrentActor = null;
                Yield();
                return;
            }

        }

        LCurrentActor = null;
    }

    /// <summary>
    /// Task를 실행한다.
    /// </summary>
    /// <returns> Task 실행 횟수를 반환한다. </returns>
    private ( EBotResult, int ) ExecuteInternal()
    {
        int executeCount = 0;
        while ( _jobQueue.TryDequeue( out var job ) )
        {
            var result = job();
            if ( result == EBotResult.Pending )
                return ( EBotResult.Pending, executeCount );

            executeCount += 1;
        }

        return ( EBotResult.Success, executeCount );
    }

}


/// <summary>
/// 봇 액터
/// </summary>
public class ComponentActor : ActorBase
{
    public Dictionary< Type, ComponentBase< ComponentActor > > Components { get; } = new();

    public bool Has< T >() where T : ComponentBase< ComponentActor >
        => Components.ContainsKey( typeof( T ) );

    public T Get< T >() where T : ComponentBase< ComponentActor >
    {
        if ( Components.TryGetValue( typeof( T ), out var component ) )
            return (T)component;

        return null;
    }

    public bool TryGet< T >( out T component ) where T : ComponentBase< ComponentActor >
    {
        if ( Components.TryGetValue( typeof( T ), out var component1 ) )
        {
            component = component1 as T;
            return true;
        }

        component = default;

        return false;
    }

    public T Add< T >() where T : ComponentBase< ComponentActor >, new()
    {
        if ( Has< T >() )
            return Get<T>();

        var newComponent = new T();
        newComponent.Owner = this;

        Components.Add( typeof( T ), newComponent );

        return (T)newComponent;
    }

    public T Ensure< T >() where T : ComponentBase< ComponentActor >, new()
    {
        if ( Components.TryGetValue( typeof( T ), out var component ) )
            return (T)component;

        var newComponent = new T();
        newComponent.Owner = this;
        Components.Add( typeof( T ), newComponent );

        return newComponent;
    }

    public void ForEach( Action< ComponentBase< ComponentActor > > job )
    {
        foreach ( var ( _, cmpt ) in Components )
        {
            job( cmpt );
        }
    }
}
