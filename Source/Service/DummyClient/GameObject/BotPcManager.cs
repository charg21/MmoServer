﻿using Mala.Collection;
using Mala.Core;
using Mala.Logging;

/// <summary>
/// 봇 관리자
/// </summary>
public class BotPcManager : Singleton< BotPcManager >
{
    /// <summary>
    /// 플레이어 맵
    /// </summary>
    HashSet< BotPc > Bots { get; set; } = new();

    /// <summary>
    /// 작업 큐
    /// </summary>
    WaitFreeQueue< Action > JobQueue { get; set; } = new();


    /// <summary>
    /// 봇을 추가한다.
    /// </summary>
    public void AddBot( BotPc bot )
        => JobQueue.Enqueue( () => Bots.Add( bot ) );

    /// <summary>
    /// 플레이어를 제거한다.
    /// </summary>
    public void RemoveBot( BotPc bot )
        => JobQueue.Enqueue( () => Bots.Remove( bot ) );

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public void Tick( MainThreadContext context, TimeSpan deltaTime )
    {
        // 작업 큐를 비운다.
        Flush();

        // 각 봇에대한 갱신처리를 진행한다.
        foreach ( var bot in Bots )
            bot.Tick( deltaTime );
    }

    /// <summary>
    /// 생성한다.
    /// </summary>
    /// <returns></returns>
    public BotPc Generate()
    {
        var newBot = new BotPc();
        newBot.BotId = Interlocked.Increment( ref _botIdIssuer );

        return newBot;
    }

    /// <summary>
    /// 봇 식별자 발급기
    /// </summary>
    static ActorId _botIdIssuer = 0;

    public void Flush() => JobQueue.ConsumeAll( job => job() );

    internal bool PostOnFirstBot( Action< BotPc > job )
    {
        if ( Bots.Count == 0 )
            return false;

        JobQueue.Enqueue( () =>
        {
            var firstBot = Bots.First();
            if ( firstBot is null )
            {
                Log.Critical( "Not Found Pc..." );
                return;
            }

            firstBot?.Post( () =>
            {
                job( firstBot );
            } );
        } );

        return true;
    }
}
