﻿using Mala.Collection;

public class BotPcViewComponent
{
    BotPc _botPc;

    //HashSet< ObjectId > _aoiPcIds;
    HashSet< ObjectId > _aoiNpcIds = new();

    public BotPcViewComponent( BotPc botPc )
    {
        _botPc = botPc;
    }

    public void Add( in PktActor other )
    {
        if ( other._actorType == EWorldObjectType.Pc )
            return;

        _aoiNpcIds.Add( (ObjectId)other._actorId );
    }

    public void Remove( ulong objectId )
    {
        //_aoiPcIds.Add( (ObjectId)objectId );
        _aoiNpcIds.Add( (ObjectId)objectId );
    }

    public bool Contains( ObjectId otherId )
        => _aoiNpcIds.Contains( otherId );

    public ObjectId SelectOne()
    {
        if ( _aoiNpcIds.TryGetRandomValue( out var v ) )
            return v;

        return new();
    }
}


