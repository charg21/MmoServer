﻿using Mala.Math;

public class Movement : ComponentBase< ComponentActor >
{
    /// <summary>
    /// 현재 위치
    /// </summary>
    public Position Pos { get; set; } = new();

    /// <summary>
    /// 방향
    /// </summary>
    public Direction Dir { get; set; } = new();

    /// <summary>
    /// 목적지
    /// </summary>
    public Position ToPos { get; set; } = new();

    /// <summary>
    /// 속도
    /// </summary>
    public float Speed { get; set; } = 1.0f;

    public override void Tick( TimeSpan deltaTime )
    {
        /// 목표 좌표로 이동한다.
        if ( Pos == ToPos )
        {
        }
    }

    public void SetPosition( Position pos, Direction dir )
    {
        Dir = dir;
        Pos = pos;

        if ( !Owner.TryGet< Network >( out var net ) )
            return;

        net.Send(
            new ValueC_Move
            {
                _pos = Pos.ToPktVector3(),
                _toPos = ToPos.ToPktVector3(),
                _yaw = MathHelper.Vector2ToYaw( Dir ),
            }.Write() );
    }
}
