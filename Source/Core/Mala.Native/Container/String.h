#pragma once

#include <string>;

using namespace std;

namespace Mala
{


using NativeString     = std::basic_string< wchar_t, std::char_traits< wchar_t >, Mala::Core::StlAllocator< wchar_t > >;
using NativeStringRef  = const NativeString&;
using NativeStringView = std::wstring_view;
using namespace std::string_view_literals;

//_EXPORT_STD _NODISCARD inline int stoi( StringRef _Str, size_t* _Idx = nullptr, int _Base = 10 ) {
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const long _Ans = _CSTD wcstol( _Ptr, &_Eptr, _Base );
//
//    if ( _Ptr == _Eptr ) {
//        _Xinvalid_argument( "invalid stoi argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stoi argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast<size_t>( _Eptr - _Ptr );
//    }
//
//    return static_cast<int>( _Ans );
//}
//
//_NODISCARD inline long stol( StringRef _Str, size_t* _Idx = nullptr, int _Base = 10 )
//{
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const long _Ans = _CSTD wcstol( _Ptr, &_Eptr, _Base );
//
//    if ( _Ptr == _Eptr ) {
//        _Xinvalid_argument( "invalid stoi argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stol argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast<size_t>( _Eptr - _Ptr );
//    }
//
//    return _Ans;
//}
//
//_EXPORT_STD _NODISCARD inline unsigned long stoul( StringRef _Str, size_t* _Idx = nullptr, int _Base = 10 ) {
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const unsigned long _Ans = _CSTD wcstoul( _Ptr, &_Eptr, _Base );
//
//    if ( _Ptr == _Eptr ) {
//        _Xinvalid_argument( "invalid stoi argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stoul argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast< size_t >( _Eptr - _Ptr );
//    }
//
//    return _Ans;
//}
//
//_EXPORT_STD _NODISCARD inline long long stoll( StringRef _Str, size_t* _Idx = nullptr, int _Base = 10 ) {
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const long long _Ans = _CSTD wcstoll( _Ptr, &_Eptr, _Base );
//
//    if ( _Ptr == _Eptr ) {
//        _Xinvalid_argument( "invalid stoi argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stoll argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast<size_t>( _Eptr - _Ptr );
//    }
//
//    return _Ans;
//}
//
//_EXPORT_STD _NODISCARD inline unsigned long long stoull( StringRef _Str, size_t* _Idx = nullptr, int _Base = 10 ) {
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const unsigned long long _Ans = _CSTD wcstoull( _Ptr, &_Eptr, _Base );
//
//    if ( _Ptr == _Eptr ) {
//        _Xinvalid_argument( "invalid stoi argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stoull argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast<size_t>( _Eptr - _Ptr );
//    }
//
//    return _Ans;
//}
//
//_EXPORT_STD _NODISCARD inline float stof( StringRef _Str, size_t* _Idx = nullptr ) {
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const float _Ans = _CSTD wcstof( _Ptr, &_Eptr );
//
//    if ( _Ptr == _Eptr ) {
//        _Xinvalid_argument( "invalid stof argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stof argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast<size_t>( _Eptr - _Ptr );
//    }
//
//    return _Ans;
//}
//
//_EXPORT_STD _NODISCARD inline double stod( StringRef _Str, size_t* _Idx = nullptr ) {
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const double _Ans = _CSTD wcstod( _Ptr, &_Eptr );
//
//    if ( _Ptr == _Eptr ) {
//        _Xinvalid_argument( "invalid stod argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stod argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast<size_t>( _Eptr - _Ptr );
//    }
//
//    return _Ans;
//}
//
//_EXPORT_STD _NODISCARD inline long double stold( StringRef _Str, size_t* _Idx = nullptr ) {
//    int& _Errno_ref = errno; // Nonzero cost, pay it once
//    const wchar_t* _Ptr = _Str.c_str();
//    wchar_t* _Eptr;
//    _Errno_ref = 0;
//    const long double _Ans = _CSTD wcstold( _Ptr, &_Eptr );
//
//    if ( _Ptr == _Eptr ) {
//         _Xinvalid_argument( "invalid stold argument" );
//    }
//
//    if ( _Errno_ref == ERANGE ) {
//        _Xout_of_range( "stold argument out of range" );
//    }
//
//    if ( _Idx ) {
//        *_Idx = static_cast<size_t>( _Eptr - _Ptr );
//    }
//
//    return _Ans;
//}
//
//template <class _Elem, class _Ty>
//_NODISCARD basic_string<_Elem, std::char_traits< _Elem >, Mala::Core::StlAllocator< _Elem > > _Integral_to_string( const _Ty _Val ) {
//    // convert _Val to string
//    static_assert( is_integral_v<_Ty>, "_Ty must be integral" );
//    using _UTy = make_unsigned_t<_Ty>;
//    _Elem _Buff[ 21 ]; // can hold -2^63 and 2^64 - 1, plus NUL
//    _Elem* const _Buff_end = _STD end( _Buff );
//    _Elem* _RNext = _Buff_end;
//    const auto _UVal = static_cast<_UTy>( _Val );
//    if ( _Val < 0 ) {
//        _RNext = _UIntegral_to_buff( _RNext, 0 - _UVal );
//        *--_RNext = '-';
//    }
//    else {
//        _RNext = _UIntegral_to_buff( _RNext, _UVal );
//    }
//
//    return basic_string<_Elem, std::char_traits< _Elem >, Mala::Core::StlAllocator< _Elem > >( _RNext, _Buff_end );
//}
//
//// TRANSITION, CUDA - warning: pointless comparison of unsigned integer with zero
//template <class _Elem, class _Ty>
//_NODISCARD basic_string<_Elem, std::char_traits< _Elem >, Mala::Core::StlAllocator< _Elem > > _UIntegral_to_string( const _Ty _Val ) {
//    // convert _Val to string
//    static_assert( is_integral_v<_Ty>, "_Ty must be integral" );
//    static_assert( is_unsigned_v<_Ty>, "_Ty must be unsigned" );
//    _Elem _Buff[ 21 ]; // can hold 2^64 - 1, plus NUL
//    _Elem* const _Buff_end = _STD end( _Buff );
//    _Elem* const _RNext = _UIntegral_to_buff( _Buff_end, _Val );
//    return basic_string<_Elem, std::char_traits< _Elem >, Mala::Core::StlAllocator< _Elem > >( _RNext, _Buff_end );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( int _Val ) {
//    return ::_Integral_to_string<wchar_t>( _Val );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( unsigned int _Val ) {
//    return ::_UIntegral_to_string<wchar_t>( _Val );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( long _Val ) {
//    return ::_Integral_to_string<wchar_t>( _Val );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( unsigned long _Val ) {
//    return ::_UIntegral_to_string<wchar_t>( _Val );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( long long _Val ) {
//    return ::_Integral_to_string<wchar_t>( _Val );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( unsigned long long _Val ) {
//    return ::_UIntegral_to_string<wchar_t>( _Val );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( double _Val ) {
//    const auto _Len = static_cast<size_t>( _CSTD _scwprintf( L"%f", _Val ) );
//    NativeString _Str( _Len, L'\0' );
//    _CSTD swprintf_s( &_Str[ 0 ], _Len + 1, L"%f", _Val );
//    return _Str;
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( float _Val ) {
//    return ::to_wstring( static_cast<double>( _Val ) );
//}
//
//_EXPORT_STD _NODISCARD inline NativeString to_wstring( long double _Val ) {
//    return ::to_wstring( static_cast<double>( _Val ) );
//}
//
}