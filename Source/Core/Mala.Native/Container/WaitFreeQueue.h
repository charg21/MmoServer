#pragma once

namespace Mala::Container
{

template< typename T, typename TAllocator = Mala::Core::StlAllocator< T > >
class WaitFreeQueue
{
private:
    struct Node
    {
        Node* _next;
        T     _data;
    };

public:
    using ConsumeJob    = std::function< void( T& ) >;
    using NodeAllocator = typename TAllocator::template rebind< Node >::other;

public:
    WaitFreeQueue()
    : _tail { nullptr }
    , _head { nullptr }
    {
        _head = _NewNode();

        _tail = _head;
        _head->_next = nullptr;
    }

    ~WaitFreeQueue()
    {
        T t;
        while ( TryDequeue( OUT t ) )
            ;

        _DeleteNode( _head );
    }

    void push( T&& src )
    {
        Emplace( std::move( src ) );
    }

    void push( const T& src )
    {
        Emplace( src );
    }

    void Enqueue( T&& src )
    {
        Emplace( std::move( src ) );
    }

    void Enqueue( const T& src )
    {
        Emplace( src );
    }

    template< typename Iterator >
    void EnqueueBulk( Iterator begin, Iterator end )
    {
        Node* prev_node      = nullptr;
        Node* last_new_node  = nullptr;
        Node* first_new_node = nullptr;

        for( Iterator it = begin; it != end; ++it )
        {
            last_new_node = _NewNode( *it );// new node{ ._next { nullptr }, ._data { *it } };

            if ( prev_node != nullptr )
                prev_node->_next = last_new_node;
            else
                first_new_node = last_new_node;

            prev_node = last_new_node;
        }

        _PushBackNode( first_new_node, last_new_node );
    }

    template< typename... Args >
    void Emplace( Args&&... args )
    {
        Node* newNode = _NewNode( std::forward< Args >( args )... );

        _PushBackNode( newNode, newNode );
    }

    bool TryDequeue( T& dest )
    {
        Node* head = _head;
        Node* next = _head->_next; // [] - []
        if ( !next )
        {
            return false;
        }
        else
        {
            if constexpr ( std::move_constructible< T > )
                dest = std::move( next->_data );
            else
                dest = next->_data;

            Node* headPtr = _head;

            _head = next;

            _DeleteNode( headPtr );

            return true;
        }
    }

    bool TryPeek( T& dest )
    {
        Node* next = _head->_next; // [] - []
        if ( !next )
        {
            return false;
        }
        else
        {
            dest = next->_data;

            return true;
        }
    }

    void ForEach( const ConsumeJob& job )
    {
        Node* eachNode{ _head->_next };

         while ( eachNode )
         {
             job( eachNode->_data );

             eachNode = eachNode->_next;
         }
    }

    template< typename TConsumeJob >
    void ForEach2( TConsumeJob job )
    {
        Node* eachNode{ _head->_next };

        while ( eachNode )
        {
            job( eachNode->_data );

            eachNode = eachNode->_next;
        }
    }

    //MALA_NODISCARD int64_t ConsumeAll( ConsumeJob&& consumer )
    //{
    //    int64_t consumedCount = 0;

    //    T t;
    //    while ( TryDequeue( t ) )
    //    {
    //        if constexpr ( std::move_constructible< T > )
    //            consumer( std::move( t ) );
    //        else
    //            consumer( t );

    //        consumedCount += 1;
    //    }

    //    return consumedCount;
    //}

    MALA_NODISCARD i64 ConsumeAll( const ConsumeJob& consumer )
    {
        i64 consumedCount = 0;

        T t;
        while ( TryDequeue( t ) )
        {
            //if constexpr ( std::move_constructible< T > )
            //    consumer( std::move( t ) );
            //else
                consumer( t );

            consumedCount += 1;
        }

        return consumedCount;
    }

    template< typename TConsumeJob >
    MALA_NODISCARD i64 ConsumeAll2( TConsumeJob consumer )
    {
        i64 consumed_count = 0;

        T t;
        while ( TryDequeue( t ) )
        {
            if constexpr ( std::move_constructible< T > )
                consumer( std::move( t ) );
            else
                consumer( t );

            consumed_count += 1;
        }

        return consumed_count;
    }

    template< typename Container = std::vector< T > >
    Container TakeAll()
    {
        Container container;

        T t;
        while ( TryDequeue( t ) )
        {
            if constexpr ( std::move_constructible< T > )
                container.emplace_back( std::move( t ) );
            else
                container.emplace_back( t );
        }

        return std::move( container );
    }

    template< typename Container = std::vector< T > >
    void TakeAll( Container& outCountainer )
    {
        T t;
        while ( TryDequeue( t ) )
        {
            if constexpr ( std::move_constructible< T > )
                outCountainer.emplace_back( std::move( t ) );
            else
                outCountainer.emplace_back( t );
        }
    }

    // 컨슈머가 사용시에만 safe가 보장된다.
    // 매우 위험한 메서드. 다른 스레드에서 _head가 가리키던 노드를 지운 후,
    // _head->_next에 접근하여 크래시가 발생 가능하다.
    // 문제 방지를 위핸선 _head를 lock으로 감싼 WritefreeQueue를 사용
    MALA_NODISCARD constexpr bool UnsafeEmpty() const noexcept
    {
        return _head->_next == nullptr;
    }

protected:
    void _PushBackNode( Node* newBegin, Node* newEnd )
    {
        Node* prevTail = (Node*)(
            ::InterlockedExchangePointer( (void* volatile*)&_tail, newEnd ) );

        prevTail->_next = newBegin;
    }

    template< typename... Args >
    Node* _NewNode( Args&&... args )
    {
		Node* newNode = NodeAllocator{}.allocate( 1 );

        new( newNode ) Node{ ._next { nullptr }, ._data{ std::forward< Args >( args )... } };

        return newNode;
    }

    void _DeleteNode( Node* ptr )
    {
        ptr->~Node();

        NodeAllocator{}.deallocate( ptr, 1 );
    }

protected:
    ALIGN_CACHE Node* volatile _head;
    ALIGN_CACHE Node* volatile _tail;
};

}
