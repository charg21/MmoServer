#pragma once

namespace Mala::Net
{
/// <summary>
/// RecvBuffer
/// </summary>
class RecvBuffer
{
	constexpr static int BUFFER_COUNT    { 10    };
	constexpr static int BUFFER_CAPACITY { 65536 };

public:
	/// <summary>
	/// ������
	/// </summary>
	RecvBuffer( int bufferSize, bool useRio = false );
	~RecvBuffer();

	void Clean();
	bool OnRead( int numOfBytes );
	bool OnWrite( int numOfBytes );

	BYTE* ReadPos() { return &buffer[ _readPos ]; }
	BYTE* WritePos() { return &buffer[ _writePos ]; }
	int WritePosOffset() { return _writePos; }
	int DataSize() { return _writePos - _readPos; }
	int FreeSize() { return _capacity - _writePos; }

	RIO_BUFFERID GetRioBufferId() { return _rioBufferId; }

private:
	int	_capacity = 0;
	int	_bufferSize = 0;
	int	_readPos = 0;
	int	_writePos = 0;

	BYTE* buffer;
	RIO_BUFFERID _rioBufferId;
};

}