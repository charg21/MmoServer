﻿#pragma once

#include "RecvBuffer.h"
#include "EndPoint.h"
#include "Core/Compressor.h"

using namespace Mala::Container;
using namespace Mala::Core;

namespace Mala::Net
{

/// <summary>
/// Rio 소켓모델 네트워크 세션
/// </summary>
class RioSession : public INetObject
{
	friend class RioListener;
	friend class RioCore;
	friend class RioService;
	friend class IocpService;

	inline static constexpr size_t BUFFER_SIZE{ 0x10000 };

	using SendBufferSpan = std::pair< unsigned char*, int >;

public:
	RioSession();
	virtual ~RioSession();

public:
	void Send( unsigned char* sendBuffer, int len );
	bool LazySend( SendBufferPtr&& sendBuffer );
	void FlushSendQueue();

	bool Connect();
	void Disconnect( const std::wstring_view& reason );

	void SetService( RioServiceRef service );
	void SetService( RioServicePtr&& service );
	void SetEndPoint( EndPoint address ) { _endPoint = address; }

	RioServicePtr GetService() { return _service.lock(); }
	EndPoint GetEndPoint() { return _endPoint; }
	SOCKET GetSocket() { return _socket; }
	RioSessionPtr GetSessionPtr();
	BYTE* GetSendBuffer() { return reinterpret_cast< BYTE* >( _sendEvent.buffer ); }

	void Clear(){};
	bool IsConnected() { return _connected; }

private:
	HANDLE GetHandle() final;
	void Dispatch( struct INetEvent* netEvent, int numOfBytes ) final;

private:
	bool RegisterConnect();
	bool RegisterDisConnect();
	void RegisterRecv();
	void RegisterSend();

	void ProcessConnect( bool success );
	void ProcessDisconnect();
	void ProcessRecv( int numOfBytes );
	void ProcessSend( int numOfBytes );

	void HandleError( int errorCode );
	void PrepareSendData();

protected:
	virtual void OnConnected( const std::wstring& ip, int port, bool success );
	virtual int	OnRecv( BYTE* buffer, int len ) { return len; }
	virtual void OnSend( int len ) {}
	virtual void OnRegsiterSend() {}
	virtual void OnDisconnected() {}

private:
	RioServiceWeakPtr _service;
	SOCKET _socket{ INVALID_SOCKET };
	EndPoint _endPoint;
	Atomic< bool > _connected{};
	Compressor* _compressor{};
	Decompressor* _decompressor{};
private:
	USE_LOCK;

	RecvBuffer _recvBuffer;
	WaitFreeQueue< SendBufferSpan > _sendQueue;
	Atomic< bool > _sendRegistered{};

private:
	SendEvent _sendEvent;
	RecvEvent _recvEvent;
	ConnectEvent _connectEvent;
	DisconnectEvent _disconnectEvent;
};

} // namespace Mala::Net


/// <summary>
/// Rio 소켓모델 네트워크 세션
/// </summary>
class CliRioSession : public Mala::Net::RioSession
{
public:
	using DisconnectedJobPtr = void(*)();
	using ConnectedJobPtr = void(*)( const std::wstring& ip, int port, bool success );
	using SendJobPtr = void(*)( int );
	using RecvJobPtr = int(*)( unsigned char*, int );

public:
	using RioSession::Send;

public:
	void SetOnConnected( void* onConnect )
	{
		OnConnectedJob = (ConnectedJobPtr)( onConnect );
	}

	void SetOnDisconnected( void* onDisconnect )
	{
		OnDisconnectedJob = (DisconnectedJobPtr)( onDisconnect );
	}

	void SetOnSend( void* onSend )
	{
		OnSendJob = (SendJobPtr)( onSend );
	}

	void SetOnRecv( void* onRecv )
	{
		OnRecvJob = (RecvJobPtr)( onRecv );
	}

public:
	void OnDisconnected() final
	{
		return OnDisconnectedJob();
	}

	void OnConnected( const std::wstring& ip, int port, bool success ) final
	{
		return OnConnectedJob( ip, port, success );
	}

	int	OnRecv( BYTE* buffer, int len ) final
	{
		return OnRecvJob( buffer, len );
	}

	void OnSend( int len ) final
	{
		return OnSendJob( len );
	}

public:
	DisconnectedJobPtr OnDisconnectedJob{};
	ConnectedJobPtr OnConnectedJob{};
	SendJobPtr OnSendJob{};
	RecvJobPtr OnRecvJob{};
};
