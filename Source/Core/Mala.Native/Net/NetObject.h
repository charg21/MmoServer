#pragma once

namespace Mala::Net
{

/// <summary>
/// 네트워크 오브젝트 인터페이스
/// </summary>
class INetObject : public std::enable_shared_from_this< INetObject >
{
public:
    virtual HANDLE GetHandle() abstract;
    virtual void Dispatch( class INetEvent* netEvent, int numofBytes ) abstract;
    int GetThreadId() { return _threadId; }
    RIO_RQ GetRequestQueue() { return _requestQueue; }
    virtual bool IsParkingObject(){ return false; }

public: // For Rio
    /// <summary>
    /// 통지 받을 Completion Queue의 스레드 식별자
    /// </summary>
    int _threadId{};
    RIO_RQ _requestQueue{};
};

}
