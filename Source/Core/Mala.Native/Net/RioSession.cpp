﻿#include "MalaPch.h"
#include "SocketHelper.h"
#include "Core/Compressor.h"

using namespace Mala::Net;

RioSession::RioSession()
: _recvBuffer { BUFFER_SIZE, true }
{
	_socket = SocketHelper::MakeSocketRio();
}

RioSession::~RioSession()
{
	SocketHelper::Close( _socket );
	xdelete( _compressor );
	xdelete( _decompressor );
}

void RioSession::Send( unsigned char* sendBuffer, int len )
{
	if ( !IsConnected() )
		return;

	_sendQueue.Emplace( sendBuffer, len );

	bool registerSend = !_sendRegistered.exchange( true );
	if ( registerSend )
		RegisterSend();
}

bool RioSession::LazySend( SendBufferPtr&& sendBuffer )
{
	return false;
}

void RioSession::FlushSendQueue()
{
}

bool RioSession::Connect()
{
	return RegisterConnect();
}

void RioSession::Disconnect( const std::wstring_view& reason )
{
	if ( !_connected.exchange( false ) )
		return;

	std::wcout << ::GetLastError() << L" Disconnet Reason : " << reason << std::endl;

	RegisterDisConnect();
}

void RioSession::SetService( RioServiceRef service )
{
	if ( service->GetServiceType() == EServiceType::Client )
	{
		_compressor = xnew< NullCompressor >( _sendEvent.buffer, 65536 );
		_decompressor = new Lz4Decompressor();
		//_decompressor = xnew< Lz4Decompressor >();
	}
	else
	{
		//_compressor = xnew< NullCompressor >( _sendEvent._buffer, 65536 );
		_compressor = xnew< Lz4Compressor >( _sendEvent.buffer, 65536 * 2 );
	}

	_service = service;
}

void RioSession::SetService( RioServicePtr&& service )
{
	if ( service->GetServiceType() == EServiceType::Client )
	{
		_compressor = xnew< NullCompressor >( _sendEvent.buffer, 65536 );
		_decompressor = new Lz4Decompressor();
	}
	else
	{
		_compressor = xnew< Lz4Compressor >( _sendEvent.buffer, 65536 * 2 );
	}

	_service = std::move( service );
}

RioSessionPtr RioSession::GetSessionPtr()
{
	return std::static_pointer_cast< RioSession >( INetObject::shared_from_this() );
}

HANDLE RioSession::GetHandle()
{
	return reinterpret_cast< HANDLE >( _socket );
}

void RioSession::Dispatch( INetEvent* netEvent, int numOfBytes )
{
	switch ( netEvent->eventType )
	{
	case EEventType::Connect:
		ProcessConnect( numOfBytes ? false : true );
		break;
	case EEventType::DisConnect:
		ProcessDisconnect();
		break;
	case EEventType::Recv:
		ProcessRecv( numOfBytes );
		break;
	case EEventType::Send:
		ProcessSend( numOfBytes );
		break;
	default:
		CRASH( "Invalid Net Event Type" );
		break;
	}
}

bool RioSession::RegisterConnect()
{
	if ( IsConnected() )
		return false;

	//Client 서비스에서만 가능
	if ( GetService()->GetServiceType() != EServiceType::Client )
		return false;

	if ( !SocketHelper::SetReuseAddress( _socket, true ) )
		return false;

	if ( !SocketHelper::BindAnyAddress( _socket, 0 ) )
		return false;

	_connectEvent.Reset();
	_connectEvent.owner = shared_from_this();

	DWORD numOfBytes = 0;
	if ( !SocketHelper::ConnectEx(
		_socket,
		GetService()->GetEndPoint().AsSockaddr(),
		sizeof( SOCKADDR ),
		nullptr,
		0,
		&numOfBytes,
		&_connectEvent.overlapped ) )
	{
		int errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING )
		{
			_connectEvent.owner = nullptr;
			return false;
		}
	}

	return true;
}

bool RioSession::RegisterDisConnect()
{
	_disconnectEvent.Reset();
	_disconnectEvent.owner = shared_from_this();

	if ( !SocketHelper::DisconnectEx(
		_socket,
		&_disconnectEvent.overlapped,
		TF_REUSE_SOCKET,
		0 ) )
	{
		int errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING )
		{
			_disconnectEvent.owner = nullptr;
			return false;
		}
	}

	return true;
}

void RioSession::RegisterRecv()
{
	if ( !IsConnected() )
		return;

	_recvEvent.owner           = shared_from_this();
	_recvEvent.rioBuf.BufferId = _recvBuffer.GetRioBufferId();
	_recvEvent.rioBuf.Length   = static_cast< ULONG >( _recvBuffer.FreeSize() );
	_recvEvent.rioBuf.Offset   = _recvBuffer.WritePosOffset();

	{
		DWORD flags{};
		WRITE_LOCK;

		if ( !SocketHelper::RioFuncTable.RIOReceive(
			_requestQueue,
			(PRIO_BUF)( &_recvEvent ),
			1,
			flags,
			&_recvEvent ) )
		{
			int errorCode = ::WSAGetLastError();
			if ( errorCode != WSA_IO_PENDING )
			{
				HandleError( errorCode );
				_recvEvent.owner = nullptr;
			}
		}
	}
}

void RioSession::RegisterSend()
{
	if ( !IsConnected() )
		return;

	_sendEvent.owner = shared_from_this();
	_sendEvent.rioBuf.Offset = 0;
	_sendEvent.rioBuf.Length = 0;

	{
		WRITE_LOCK;

		PrepareSendData();

		if ( _sendEvent.rioBuf.Length == 0 )
		{
			printf_s( "[DEBUG] Send Size 0: %d\n", GetLastError() );
			_sendEvent.owner = nullptr;
			_sendRegistered.store( false );
			return;
		}

		DWORD flags{};
		if ( !SocketHelper::RioFuncTable.RIOSend(
			_requestQueue,
			&_sendEvent.rioBuf,
			1,
			flags,
			&_sendEvent ) )
		{
			int errorCode = ::WSAGetLastError();
			if ( errorCode != WSA_IO_PENDING )
			{
				HandleError( errorCode );

				printf_s( "[DEBUG] RIOSend error: %d\n", GetLastError() );

				_sendEvent.owner = nullptr;
			}
		}
	}
}

void RioSession::ProcessConnect( bool success )
{
	_connectEvent.owner = nullptr;

	_connected.store( success );

	//컨텐츠코드 오버로딩
	OnConnected( L"127.0.0.1", 18888, success );
	if ( !success )
		return;

	GetService()->AddSession( GetSessionPtr() );

	RegisterRecv();
}

void RioSession::ProcessDisconnect()
{
	OnDisconnected();

	_disconnectEvent.owner = nullptr;

	GetService()->ReleaseSession( GetSessionPtr() );
}

void RioSession::ProcessRecv( int numOfBytes )
{
	_recvEvent.owner = nullptr;
	if ( numOfBytes == 0 )
	{
		Disconnect( L"Rio Recv 0" );
		return;
	}

	if ( !_recvBuffer.OnWrite( numOfBytes ) )
	{
		Disconnect( L"OnWrite OverFlow1" );
		return;
	}

	if ( _decompressor )
	{
		auto decompressLen{ _decompressor->Decompress( _recvBuffer.ReadPos(), _recvBuffer.DataSize() ) };
		if ( decompressLen < 0 || !_recvBuffer.OnRead( decompressLen ) )
		{
			Disconnect( L"Decompress Error" );
			return;
		}

		int dataSize{ _decompressor->GetLength() };
		int processLen = OnRecv( _decompressor->GetBuffer(), dataSize );
		if ( processLen < 0 || dataSize < processLen )
		{
			Disconnect( L"OnRead OverFlow3" );
			return;
		}

		_decompressor->Clear( processLen );
		_recvBuffer.Clean();
	}
	else
	{
		int dataSize{ _recvBuffer.DataSize() };
		int processLen = OnRecv( _recvBuffer.ReadPos(), dataSize );
		if ( processLen < 0 || dataSize < processLen || !_recvBuffer.OnRead( dataSize ) )
		{
			Disconnect( L"OnRead OverFlow3" );
			return;
		}
		_recvBuffer.Clean();
	}

	//수신 등록
	RegisterRecv();
}

void RioSession::ProcessSend( int numOfBytes )
{
	_sendEvent.owner = nullptr;

	if ( numOfBytes == 0 )
	{
		Disconnect( L"Rio Send 0" );
		return;
	}

	OnSend( numOfBytes );

	WRITE_LOCK;
	if ( _sendQueue.UnsafeEmpty() )
	{
		_sendRegistered.store( false );
	}
	else
	{
		RegisterSend();
	}
}

void RioSession::HandleError( int errorCode )
{
	switch ( errorCode )
	{
	case WSAECONNRESET:
	case WSAECONNABORTED:
		Disconnect( L"Handle Error" );
		break;
	default:
		//TODO Log.
		std::wcout << L" Handle Error : " << errorCode << std::endl;
		break;
	}
}

void RioSession::PrepareSendData()
{
	_compressor->Clear();

	SendBufferSpan sendBuffer{};
	while ( _sendQueue.TryPeek( sendBuffer ) && _compressor->Write( sendBuffer ) )
		_sendQueue.TryDequeue( sendBuffer );

	_sendEvent.rioBuf.Length = _compressor->Compress();
}

void RioSession::OnConnected( const std::wstring& ip, int port, bool success )
{
	printf_s( "[DEBUG] Client Connected: Unmanaged ThreadId = %d\n", _threadId );
}
