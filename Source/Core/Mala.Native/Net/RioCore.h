#pragma once

#include "NetEvent.h"

using namespace Mala::Threading;
using namespace Mala::Container;

namespace Mala::Net
{

class RioListener;
class Connector;
class SessionManager;

/// <summary>
/// Rio 코어
/// </summary>
class RioCore : public INetObject
{
public:
    using JobPtr = void(*)();
    using ServiceList = Vector< RioServicePtr >;

public:
    RioCore();
    virtual ~RioCore();

    bool Register( INetObjectRef rioObjectRef );
    bool Dispatch( u32 timeoutMs = INFINITE );
    void Dispatch( INetEvent* netEvent, int numofBytes ) final;

    HANDLE GetHandle() final { return {}; }
    HANDLE GetHandle( int threadId ) { return _iocpHandles[ threadId ]; }
    const RIO_CQ& GetCompletionQueue( int threadId ) { return _completionQueues[ threadId ]; }
    int GetConcurrentThreadCount() { return _threadPoolCapacity; }

    void Initialize( int threadCount = 1 );
    void RegisterService( RioServiceRef service );
    void UnregisterService( RioServiceRef service );

private:
    Vector< RIO_CQ > _completionQueues;
    Vector< HANDLE > _iocpHandles;
    int              _threadPoolCapacity{};
    ServiceList      _rioServices;
    INetEvent        _netEvent{ EEventType::All };
};

} // namespace Mala::Net
