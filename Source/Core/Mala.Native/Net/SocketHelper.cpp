#include "MalaPch.h"

#include "SocketHelper.h"
#include "EndPoint.h"

namespace Mala::Net
{

template< typename T >
inline bool SetSockOpt( SOCKET socket, int level, int optName, T optVal )
{
    return SOCKET_ERROR != ::setsockopt( socket, level, optName, reinterpret_cast< const char* >( &optVal ), sizeof( T ) );
}

/// <summary>
/// 소켓 유틸을 초기화한다
/// </summary>
/// <param name="UseExtendedApi"> 확장 API 사용 여부 </param>
MALA_NODISCARD bool SocketHelper::Initialize()
{
    WSADATA wsaData{};

    if ( 0 != ::WSAStartup( MAKEWORD( 2, 2 ), &wsaData ) )
        return false;

    return InitializeExtendedApi();
}

/// <summary>
/// 소켓 확장 API( 비동기 IO )를 초기화한다
/// </summary>
MALA_NODISCARD bool SocketHelper::InitializeExtendedApi()
{
    SOCKET noUseDummySocket = socket( AF_INET, SOCK_STREAM, 0 );
    if ( noUseDummySocket == INVALID_SOCKET )
        return false;

    const void*  lpfnPtrs[ 3 ] { &ConnectEx, &AcceptEx, &DisconnectEx };
    const size_t lpfnSize[ 3 ] { sizeof( ConnectEx ), sizeof( AcceptEx ), sizeof( DisconnectEx ) };
    const GUID   guids   [ 3 ] { WSAID_CONNECTEX, WSAID_ACCEPTEX, WSAID_DISCONNECTEX };

    bool success = true;
    for ( int index {}; index < 3; ++index )
    {
        DWORD bytes = 0;
        if ( 0 != WSAIoctl(
            noUseDummySocket,
            SIO_GET_EXTENSION_FUNCTION_POINTER,
            (void*)( &guids[ index ] ),
            sizeof( guids[ index ] ),
            (void*)( lpfnPtrs[ index ] ),
            lpfnSize[ index ],
            &bytes,
            nullptr,
            nullptr ) )
        {
            success = false;
            break;
        }
    }

    /// RIO function table
    GUID functionTableId = WSAID_MULTIPLE_RIO;
    DWORD bytes = 0;
    if ( WSAIoctl(
        noUseDummySocket,
        SIO_GET_MULTIPLE_EXTENSION_FUNCTION_POINTER,
        &functionTableId,
        sizeof( GUID ),
        ( void** )&RioFuncTable,
        sizeof( RioFuncTable ),
        &bytes,
        NULL,
        NULL ) )
        return false;

    if ( 0 != ::closesocket( noUseDummySocket ) )
        return false;

    return success;
}

/// <summary>
/// 소켓 유틸을 정리한다
/// </summary>
void SocketHelper::Finalize()
{
    ::WSACleanup();
}

/// <summary>
/// 소켓을 생성한다
/// </summary>
MALA_NODISCARD SOCKET SocketHelper::MakeSocket()
{
    return ::WSASocketW( AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED );
}

/// <summary>
/// 소켓을 생성한다
/// </summary>
MALA_NODISCARD SOCKET SocketHelper::MakeSocketRio()
{
    return ::WSASocketW(
        AF_INET,
        SOCK_STREAM,
        IPPROTO_TCP,
        NULL,
        0,
        WSA_FLAG_REGISTERED_IO | WSA_FLAG_OVERLAPPED );
}

/// <summary>
/// 소켓을 주소와 포트를 할당한다
/// </summary>
bool SocketHelper::Bind( SOCKET sock, const EndPoint& endPoint )
{
    return SOCKET_ERROR != ::bind( sock, endPoint.AsSockaddr(), endPoint.Size() );
}

bool SocketHelper::BindAnyAddress( SOCKET socket, u16 port )
{
    SOCKADDR_IN myAddress;
    myAddress.sin_family = AF_INET;
    myAddress.sin_addr.s_addr = ::htonl( INADDR_ANY );
    myAddress.sin_port = ::htons( port );

    return SOCKET_ERROR != ::bind( socket, reinterpret_cast<const SOCKADDR*>( &myAddress ), sizeof( myAddress ) );
}

/// <summary>
/// 문자열 주소를 통해 세팅 IN_ADDR 구조체에 주소를 설정한다
/// </summary>
IN_ADDR SocketHelper::IpToAddress( const std::wstring& ip )
{
    IN_ADDR inAddr{};
    ::InetPtonW( AF_INET, ip.c_str(), &inAddr );

    return inAddr;
}

/// <summary>
/// 문자열 주소를 통해 세팅 IN_ADDR 구조체에 주소를 설정한다
/// </summary>
IN_ADDR SocketHelper::IpToAddress( const std::string& ip )
{
    IN_ADDR inAddr{};
    ::InetPtonA( AF_INET, ip.c_str(), &inAddr );

    return inAddr;
}

void SocketHelper::Clear()
{
    ::WSACleanup();
}

bool SocketHelper::Listen( SOCKET socket, int backlog )
{
    return SOCKET_ERROR != ::listen( socket, backlog );
}

INT SocketHelper::RioNotify( RIO_CQ cq )
{
	return RioFuncTable.RIONotify( cq );
}

ULONG SocketHelper::RioDequeueCompletion( RIO_CQ cq, RIORESULT* results, ULONG arraSize )
{
	return RioFuncTable.RIODequeueCompletion( cq, results, arraSize );
}

bool SocketHelper::Close( SOCKET& socket )
{
    if ( socket != INVALID_SOCKET )
        ::closesocket( socket );

    socket = INVALID_SOCKET;
    return false;
}

bool SocketHelper::SetLinger( SOCKET socket, u32 onoff, u32 linger )
{
    LINGER option{ .l_onoff = (u16)( onoff ), .l_linger = (u16)( linger ) };
    return SetSockOpt( socket, SOL_SOCKET, SO_LINGER, option );
}


bool SocketHelper::SetReuseAddress( SOCKET socket, bool flag )
{
    return SetSockOpt( socket, SOL_SOCKET, SO_REUSEADDR, flag );
}

bool SocketHelper::SetRecvBufferSize( SOCKET socket, int size )
{
    return SetSockOpt( socket, SOL_SOCKET, SO_RCVBUF, size );
}

bool SocketHelper::SetSendBufferSize( SOCKET socket, int size )
{
    return SetSockOpt( socket, SOL_SOCKET, SO_SNDBUF, size );
}

bool SocketHelper::SetTcpNoDelay( SOCKET socket, bool flag )
{
    return SetSockOpt( socket, SOL_SOCKET, TCP_NODELAY, flag );
}

//listenSocket의 특성을 Client속성에 전파한다.
bool SocketHelper::SetUpdateAcceptSocket( SOCKET socket, SOCKET listenSocket )
{
    return SetSockOpt( socket, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT, listenSocket );
}

}