#include "MalaPch.h"
#include "RioCore.h"
#include "SocketHelper.h"
#include "Core/Dump.h"

using namespace Mala::Net;

RioCore::RioCore()
: _threadPoolCapacity{}
{
    Dump::Setup();
}

RioCore::~RioCore()
{
    for ( auto iocpHandle : _iocpHandles )
    {
        ::CloseHandle( iocpHandle );
    }

    _netEvent.owner.reset();
    _iocpHandles.clear();
}

void RioCore::Initialize( int threadCount )
{
    _threadPoolCapacity = threadCount;
    _iocpHandles.resize( threadCount );
    _completionQueues.resize( threadCount );
	_netEvent.owner = shared_from_this();

    for ( int i{}; i < threadCount; i += 1 )
    {
        _iocpHandles[ i ] = CreateIoCompletionPort( INVALID_HANDLE_VALUE, NULL, 0, 1 );
        ASSERT_CRASH( _iocpHandles[ i ] != NULL );

        RIO_NOTIFICATION_COMPLETION completionType{};
        completionType.Type               = RIO_IOCP_COMPLETION;
        completionType.Iocp.IocpHandle    = _iocpHandles[ i ];
        completionType.Iocp.CompletionKey = (void*)( nullptr );
        completionType.Iocp.Overlapped    = (LPOVERLAPPED)( &_netEvent );

        _completionQueues[ i ] = SocketHelper::RioFuncTable.RIOCreateCompletionQueue( MAX_CQ_SIZE, &completionType );
        ASSERT_CRASH( _completionQueues[ i ] != RIO_INVALID_CQ );

        int notifyResult = SocketHelper::RioNotify( _completionQueues[ i ] );
        ASSERT_CRASH( notifyResult == ERROR_SUCCESS );
    }
}

void RioCore::RegisterService( RioServiceRef service )
{
    _rioServices.emplace_back( service );
}

void RioCore::UnregisterService( RioServiceRef service )
{
	_rioServices.erase(
        std::remove( _rioServices.begin(), _rioServices.end(), service ), _rioServices.end() );
}

bool RioCore::Register( INetObjectRef rioObject )
{
    /// TODO: CQ 로드밸런싱하여 배정 되도록 수정
    static std::atomic< int > threadLoadBalancer = 0;

    if ( !rioObject->IsParkingObject() )
        rioObject->_threadId = threadLoadBalancer.fetch_add( 1 ) % _threadPoolCapacity;

    if ( auto rioSession = std::dynamic_pointer_cast< RioSession >( rioObject ) )
    {
        auto requestQueue = SocketHelper::RioFuncTable.RIOCreateRequestQueue(
            rioSession->GetSocket(),
            MAX_RECV_RQ_SIZE_PER_SOCKET,
            1,
            MAX_SEND_RQ_SIZE_PER_SOCKET,
            1,
            GetCompletionQueue( rioObject->GetThreadId() ),
            GetCompletionQueue( rioObject->GetThreadId() ),
            NULL );
        ASSERT_CRASH( requestQueue != RIO_INVALID_RQ );
        rioObject->_requestQueue = requestQueue;
    }

    /// 컴플리션 포트 등록
    HANDLE resultHandle = ::CreateIoCompletionPort(
        rioObject->GetHandle(),
        _iocpHandles[ rioObject->GetThreadId() ],
        /*completionKey*/0,
        0 );

    return resultHandle == _iocpHandles[ rioObject->GetThreadId() ];
}

bool RioCore::Dispatch( u32 timeoutMs )
{
    u32        threadId  { LThreadId % _threadPoolCapacity };
    HANDLE     iocpHandle{ _iocpHandles[ threadId ]        };
    DWORD      numOfBytes{};
    ULONG_PTR  key       {};
    INetEvent* netEvent  {};

    if ( ::GetQueuedCompletionStatus(
        iocpHandle,
        OUT &numOfBytes,
        OUT reinterpret_cast< PULONG_PTR >( &key ),
        OUT reinterpret_cast< LPOVERLAPPED* >( &netEvent ),
        timeoutMs ) )
    {
        ASSERT_CRASH( netEvent != nullptr );
        auto netObject{ netEvent->owner };
        netObject->Dispatch( netEvent, numOfBytes );
    }
    else
    {
        int errorCode = ::WSAGetLastError();
        switch ( errorCode )
        {
        case WAIT_TIMEOUT:
            return true;

        case ERROR_CONNECTION_REFUSED:
            {
                // Connect시 원격 컴퓨터가 네트워크 연결을 거부했습니다.
                auto rioObject{ netEvent->owner };
                numOfBytes = errorCode;
                rioObject->Dispatch( netEvent, numOfBytes );
                return true;
            }
        default:
            //TODO : 로그 찍기.
            auto rioObject{ netEvent->owner };
            rioObject->Dispatch( netEvent, numOfBytes );
            return true;
        }
    }

    return true;
}

void RioCore::Dispatch( INetEvent* netEvent, int numofBytes )
{
    u32    threadId{ LThreadId % _threadPoolCapacity };
    RIO_CQ cq      { _completionQueues[ threadId ] };
    static thread_local auto tlsResults{ std::make_unique< std::array< RIORESULT, MAX_RIO_RESULT > >() };
    auto* results{ reinterpret_cast< RIORESULT* >( tlsResults.get() ) };
    ULONG numResults{ SocketHelper::RioDequeueCompletion( cq, results, MAX_RIO_RESULT ) };
    if ( !numResults || RIO_CORRUPT_CQ_CODE == numResults )
        CRASH( L"" );

    /// RioDequeueCompletion 완료 후 CompletionQueue 사용 가능 통지
    auto notifyResult = SocketHelper::RioNotify( cq );
    ASSERT_CRASH( notifyResult == ERROR_SUCCESS );

    for ( ULONG i = 0; i < numResults; ++i )
    {
        auto* netEvent   { reinterpret_cast< INetEvent* >( results[ i ].RequestContext ) };
        auto& object     { netEvent->owner };
        ULONG transferred{ results[ i ].BytesTransferred };

        object->Dispatch( netEvent, transferred );
    }
}
