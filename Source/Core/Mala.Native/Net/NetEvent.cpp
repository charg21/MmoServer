﻿#include "MalaPch.h"

#include "SocketHelper.h"
#include "NetEvent.h"


using namespace Mala::Container;
using namespace Mala::Net;


void INetEvent::Reset()
{
	// 아래의 두개의 Overlapped 멤버를 초기화하면 16바이트 크기인 RIO_BUF도 초기화 된다.
	overlapped.Internal = 0;     // offset 0
	overlapped.InternalHigh = 0; // offset 8

	// 초기화 생략
	//OVERLAPPED::Offset = 0;     // offset 12  비동기 파일 IO에서 사용
	//OVERLAPPED::OffsetHigh = 0; // offset 16 ( 예약 공간 )

	// overlapped구조체에서 이벤트를 사용하지 않는다면 0으로 리셋
	// 맨 처음 0으로 초기화 후 더이상 접근하지 않음.
	//overlapped.hEvent = 0;      // offset 24
}

RecvEvent::RecvEvent()
: INetEvent{ EEventType::Recv }
{
}

SendEvent::SendEvent()
: INetEvent{ EEventType::Send }
{
    buffer = (char*)( Mala::Windows::VirtualAllocEx( BUFFER_CAPACITY ) );

    rioBuf.RIO_BUF::BufferId = SocketHelper::RioFuncTable.RIORegisterBuffer(
        buffer,
		BUFFER_CAPACITY );
}

SendEvent::~SendEvent()
{
    SocketHelper::RioFuncTable.RIODeregisterBuffer( rioBuf.RIO_BUF::BufferId );

    Mala::Windows::VirtualFreeEx( buffer );
}

