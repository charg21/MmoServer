#include "MalaPch.h"
#include "SocketHelper.h"

using namespace Mala::Net;

EndPoint::EndPoint( SOCKADDR_IN sockAddr )
: _sockaddr_in{ sockAddr }
{
}

EndPoint::EndPoint( const std::string& address, uint16_t port )
{
    _sockaddr_in.sin_family = AF_INET;
    _sockaddr_in.sin_port   = ::htons( port );
    _sockaddr_in.sin_addr   = SocketHelper::IpToAddress( address );
}

EndPoint::EndPoint( const std::wstring& address, uint16_t port )
{
    _sockaddr_in.sin_family = AF_INET;
    _sockaddr_in.sin_port   = ::htons( port );
    _sockaddr_in.sin_addr   = SocketHelper::IpToAddress( address );
}

const sockaddr* EndPoint::AsSockaddr() const
{
    return &_sockaddr;
}

sockaddr* EndPoint::AsSockaddr()
{
    return &_sockaddr;
}

sockaddr_in* EndPoint::AsSockaddrIn()
{
    return &_sockaddr_in;
}

const sockaddr_in* EndPoint::AsSockaddrIn() const
{
    return &_sockaddr_in;
}

std::wstring EndPoint::ToString()
{
    return L"";// xformat( L"{}:{}", GetIp(), GetPort() );
}

const uint16_t EndPoint::GetPort() const
{
    return ::ntohs( _sockaddr_in.sin_port );
}

std::string EndPoint::get_ip()
{
    char buffer[ 16 ] {}; // xxx:xxx:xxx:xxx

    switch ( _sockaddr_in.sin_family )
    {
    case AF_INET:
        return ::inet_ntop( AF_INET, &_sockaddr_in.sin_addr, buffer, sizeof( buffer ) );
    }

    return std::string {};
}

std::wstring EndPoint::GetIp()
{
    wchar_t buffer[ 16 ]{}; // xxx:xxx:xxx:xxx

    switch ( _sockaddr_in.sin_family )
    {
    case AF_INET:
        return ::InetNtopW( AF_INET, &_sockaddr_in.sin_addr, buffer, sizeof( buffer ) );
    }

    return std::wstring{};
}
