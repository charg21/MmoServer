﻿#pragma once

#include "RioSessionFactory.h"
#include "EndPoint.h"

using namespace Mala::Container;

namespace Mala::Net
{

class AcceptEvent;
class ServerService;

/// <summary>
/// Listenr
/// </summary>
class RioListener : public INetObject
{
public:
	RioListener() = default;
	~RioListener();

public:
	bool StartAccept( RioServerServiceRef service );
	void CloseSocket();
	void Initialize( const EndPoint& endPoint, int capacity, RioSessionFactoryRef sessionFactory );

public:
	HANDLE GetHandle() final;
	void Dispatch( class INetEvent* netEvent, int numofBytes ) final;
	bool IsParkingObject() final { return true; }

private:
	void RegisterAccept( AcceptEvent* acceptEvent );
	void ProcessAccept( AcceptEvent* acceptEvent );

protected:
	SOCKET _socket{ INVALID_SOCKET };

	Vector< AcceptEvent* > _acceptEvents;

	RioServerServicePtr _service;

	int _capacity{};

	std::function< RioSessionPtr() > _sessionFactory;

	EndPoint _endPoint{};
};

}