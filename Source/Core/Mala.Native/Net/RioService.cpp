#include "MalaPch.h"

using namespace Mala::Threading;
using namespace Mala::Net;

/// <summary>
/// 持失切
/// </summary>
RioClientService::RioClientService(
	EndPoint             address,
	RioCoreRef           core,
	RioSessionFactoryRef factory,
	int                  maxSessionCount )
: RioService{ EServiceType::Client, address, core, factory, maxSessionCount }
{
}

bool RioClientService::Start()
{
	if ( !CanStart() )
		return false;

	const int sessionCount = GetMaxSessionCount();
	for ( int i = 0; i < sessionCount; i++ )
	{
		RioSessionPtr session = CreateSession();
		if ( !session->Connect() )
			return false;
	}

	return true;
}

///// <summary>
///// 持失切
///// </summary>
//RioServerService::RioServerService()
//: RioService{ EServiceType::Server, address, core, factory, maxSessionCount }
//{
//}

/// <summary>
/// 持失切
/// </summary>
RioServerService::RioServerService( RioCoreRef core )
: RioService{ EServiceType::Server, {}, core, {}, {} }
{
}

void RioServerService::RegisterListener( RioListenerRef listener )
{
	_listeners.emplace( listener );
}

bool RioServerService::Start()
{
	if ( !CanStart() )
		return false;

	auto service = static_pointer_cast< RioServerService >( shared_from_this() );
	for ( RioListenerRef listener : _listeners )
	{
		if ( !listener->StartAccept( service ) )
			return false;
	}

	return true;
}

void RioServerService::CloseService()
{
	RioService::CloseService();
}

/// <summary>
/// 持失切
/// </summary>
RioService::RioService(
	EServiceType         type,
	EndPoint             endPoint,
	RioCoreRef           core,
	RioSessionFactoryRef factory,
	int                  maxSessionCount )
: _type           { type            }
, _endPoint       { endPoint        }
, _rioCore        { core            }
, _sessionFactory { factory         }
, _maxSessionCount{ maxSessionCount }
{
}

RioSessionPtr RioService::CreateSession()
{
	RioSessionPtr session = _sessionFactory();
	return CreateSession( session );
}

RioSessionPtr RioService::CreateSession( RioSessionRef session )
{
	session->SetService( shared_from_this() );
	if ( !_rioCore->Register( session ) )
		return nullptr;

	return session;
}

void RioService::AddSession( RioSessionRef session )
{
	WRITE_LOCK;

	_sessionCount++;
	_sessions.insert( session );
}

void RioService::ReleaseSession( RioSessionRef session )
{
	WRITE_LOCK;

	ASSERT_CRASH( _sessions.erase( session ) != 0 );
	_sessionCount--;
}
