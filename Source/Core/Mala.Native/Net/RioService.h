#pragma once

#include "RioSessionFactory.h"

using namespace Mala::Threading;


namespace Mala::Net
{

class RioListener;
class Connector;
class SessionManager;

/// <summary>
/// Rio ����
/// </summary>
class RioService : public std::enable_shared_from_this< RioService >
{
    using Sessions = std::set< RioSessionPtr >;

public:
    RioService(
        EServiceType         type,
        EndPoint             endPoint,
        RioCoreRef           core,
        RioSessionFactoryRef factory,
        int                  maxSessionCount = 1 );
    virtual ~RioService() = default;

    virtual bool Start() abstract; //Start;
    virtual bool CanStart() { return _sessionFactory != nullptr; }
    virtual void CloseService() {}

    RioSessionPtr CreateSession( RioSessionRef session );
    RioSessionPtr CreateSession();

    void AddSession( RioSessionRef session );
    void ReleaseSession( RioSessionRef session );
    int	GetCurrentSessionCount() { return _sessionCount; }
    int	GetMaxSessionCount() { return _maxSessionCount; }

public:
    EServiceType GetServiceType() { return _type; }
    const EndPoint& GetEndPoint() { return _endPoint; }
    RioCorePtr GetCore() { return _rioCore; }
    RioCoreRef GetCoreRef() { return _rioCore; }

protected:
	USE_LOCK;

	EServiceType _type;
	EndPoint _endPoint{};
	RioCorePtr _rioCore;

    Sessions _sessions;
    int _sessionCount{};
    int _maxSessionCount{};

    RioSessionFactory _sessionFactory;
};

class RioClientService : public RioService
{
public:
    RioClientService(
        EndPoint             address,
        RioCoreRef           core,
        RioSessionFactoryRef factory,
        int                  maxSessionCount = 1 );

    ~RioClientService() override {};

    bool Start() override;
    void CloseService() override{}

};

class RioServerService : public RioService
{
    using RioListenerList = std::set< RioListenerPtr >;

public:
    RioServerService( RioCoreRef core );
    ~RioServerService() override {};

    void RegisterListener( RioListenerRef listener );
    virtual bool CanStart() { return _listeners.size(); }

    bool Start() override;
    void CloseService() override;

private:
    RioListenerList _listeners;
};

} // namespace Mala::Net