﻿#pragma once

#include "MalaPch.h"
#include "SocketHelper.h"

using namespace Mala::Net;

RioListener::~RioListener()
{
	SocketHelper::Close( _socket );

	for ( AcceptEvent* acceptEvent : _acceptEvents )
	{
		xdelete( acceptEvent );
	}
}

bool RioListener::StartAccept( RioServerServiceRef service )
{
	_service = service;

	if ( !_service )
		return false;

	_socket = SocketHelper::MakeSocketRio();
	if ( _socket == INVALID_SOCKET )
		return false;

	if ( !_service->GetCoreRef()->Register( shared_from_this() ) )
		return false;

	if ( !SocketHelper::SetReuseAddress( _socket, true ) )
		return false;

	if ( !SocketHelper::SetLinger( _socket, 0, 0 ) )
		return false;

	if ( !SocketHelper::Bind( _socket, _endPoint ) )
		return false;

	if ( !SocketHelper::Listen( _socket ) )
		return false;

	//accept 등록
	const int acceptCount = _capacity;
	_acceptEvents.reserve( acceptCount );
	for ( int i = 0; i < acceptCount; i++ )
	{
		AcceptEvent* acceptEvent = xnew< AcceptEvent >();
		acceptEvent->owner = shared_from_this();
		_acceptEvents.push_back( acceptEvent );
		RegisterAccept( acceptEvent );
	}

	return true;
}

void RioListener::CloseSocket()
{
	Mala::Net::SocketHelper::Close( _socket );
}

void RioListener::Initialize(
	const EndPoint&            endPoint,
	      int                  capacity,
	      RioSessionFactoryRef sessionFactory )
{
	_endPoint       = endPoint;
	_capacity       = capacity;
	_sessionFactory = sessionFactory;
}

HANDLE RioListener::GetHandle()
{
	return reinterpret_cast< HANDLE >( _socket );
}

void RioListener::Dispatch( INetEvent* netEvent, int numofBytes )
{
	ASSERT_CRASH( netEvent->eventType == EEventType::Accept );
	AcceptEvent* acceptEvent = static_cast< AcceptEvent* >( netEvent );
	ProcessAccept( acceptEvent );
}

void RioListener::RegisterAccept( AcceptEvent* acceptEvent )
{
	RioSessionPtr session = _service->CreateSession( _sessionFactory() );
	acceptEvent->Reset();
	acceptEvent->rioSession = session;

	DWORD bytesReceived = 0;

	if ( !SocketHelper::AcceptEx( _socket,
		session->GetSocket(),
		session->_recvBuffer.WritePos(),
		0,
		sizeof( SOCKADDR_IN ) + 16,
		sizeof( SOCKADDR_IN ) + 16,
		OUT & bytesReceived,
		static_cast< LPOVERLAPPED >( (LPOVERLAPPED)acceptEvent ) ) )
	{
		const int errorCode = ::WSAGetLastError();
		if ( errorCode != WSA_IO_PENDING )
			RegisterAccept( acceptEvent );
	}
}

void RioListener::ProcessAccept( AcceptEvent* acceptEvent )
{
	RioSessionPtr session = std::move( acceptEvent->rioSession );

	//_threadId = rand() % RIOManager::ConcurrentThreadCount;

	if ( !SocketHelper::SetUpdateAcceptSocket( session->GetSocket(), _socket ) )
	{
		//문제가 발생하면 RegisterAccept를 걸어줘야한다. 재사용 해야 하기 때문.
		RegisterAccept( acceptEvent );
		return;
	}

	SOCKADDR_IN sockAddress;
	int sizeofSockAddr = sizeof( sockAddress );
	if ( SOCKET_ERROR == ::getpeername( session->GetSocket(),
		OUT reinterpret_cast< SOCKADDR* >( &sockAddress ),
		&sizeofSockAddr ) )
	{
		RegisterAccept( acceptEvent );
		return;
	}

	session->SetEndPoint( EndPoint( sockAddress ) );
	session->ProcessConnect( true );
	//cout << "Client Connected !" << endl;

	RegisterAccept( acceptEvent );
}
