#pragma once


namespace Mala::Net
{

using RioSessionFactory    = std::function< RioSessionPtr() >;
using RioSessionFactoryRef = const RioSessionFactory&;

};