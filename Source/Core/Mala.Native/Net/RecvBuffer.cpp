#pragma once

#include "MalaPch.h"
#include "RecvBuffer.h"
#include "SocketHelper.h"

using namespace Mala::Net;

RecvBuffer::RecvBuffer( int bufferSize, bool useRio )
: _bufferSize{ bufferSize }
, _rioBufferId{}
{
    _capacity = bufferSize;
    buffer = reinterpret_cast< BYTE* >( ::VirtualAllocEx(
        GetCurrentProcess(),
        0,
        bufferSize,
        MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE ) );
    if ( buffer == nullptr )
        printf_s( "[DEBUG] VirtualAllocEx Error: %d\n", GetLastError() );

    if ( useRio )
    {
        _rioBufferId = SocketHelper::RioFuncTable.RIORegisterBuffer( (PCHAR)buffer, bufferSize );
        if ( _rioBufferId == RIO_INVALID_BUFFERID )
            printf_s( "[DEBUG] RIORegisterBuffer Error: %d\n", GetLastError() );
    }
}

RecvBuffer::~RecvBuffer()
{
    ::VirtualFreeEx( GetCurrentProcess(), buffer, 0, MEM_RELEASE );

    if ( _rioBufferId )
        SocketHelper::RioFuncTable.RIODeregisterBuffer( _rioBufferId );
}

void RecvBuffer::Clean()
{
    int dataSize = DataSize();
    if ( dataSize == 0 )
    {
        _readPos = _writePos = 0;
    }
    else
    {
        // 여유 공간이 버퍼 1개 크기 미만이면, 데이터를 앞으로 땅긴다.
        if ( FreeSize() < _bufferSize )
        {
            ::memcpy( &buffer[ 0 ], &buffer[ _readPos ], dataSize );
            _readPos = 0;
            _writePos = dataSize;
        }
    }
}

bool RecvBuffer::OnRead( int numOfBytes )
{
    if ( numOfBytes > DataSize() )
        return false;

    _readPos += numOfBytes;
    return true;
}

bool RecvBuffer::OnWrite( int numOfBytes )
{
    if ( numOfBytes > FreeSize() )
        return false;

    _writePos += numOfBytes;
    return true;
}
