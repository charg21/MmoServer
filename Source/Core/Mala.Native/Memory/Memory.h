#pragma once

#include "Container/StaticVector.h"

using namespace Mala::Core;
using namespace Mala::Container;

class MemoryPool;
struct MemoryHeader;

class Memory2
{
public:
	enum
	{
		// ~1024까지 32단위, ~2048까지 128단위, ~4096까지 256단위
		POOL_COUNT = ( 1024 / 32 ) + ( 1024 / 128 ) + ( 2048 / 256 ),
		MAX_ALLOC_SIZE = 4096,
	};

public:
	Memory2();
	~Memory2();

	void* Allocate( int size );
	void* AllocateChunk( int size );
	void Release( void* ptr );
	void Release( MemoryHeader* header, void* ptr );
	size_t MemorySize();

private:
	StaticVector< MemoryPool*, POOL_COUNT > _pools;

	// 메모리 크기 <-> 메모리 풀
	// 0(1) 빠르게 찾기 위한 테이블
	// 0~32
	MemoryPool* _poolTable[ MAX_ALLOC_SIZE + 1 ];
};

class MemoryCache
{
public:
	using CachePool           = std::vector< MemoryHeader* >;
	using CachePoolContainer  = StaticVector< CachePool*, Memory2::POOL_COUNT >;
	using MemoryCacheRegister = StaticVector< MemoryCache*, 32 >;

public:
	MemoryCache();
	~MemoryCache();

	void* Allocate( int size );
	void Release( void* ptr );

	i64 GetAllocByte( int size ) const;
	static void ForEachMemoryCache( const std::function< void( const MemoryCache* ) >& job );

private:
	CachePoolContainer _pools;

	/// <summary>
	///
	/// </summary>
	inline static MemoryCacheRegister _cachePoolRegister;

	// 메모리 크기 <-> 메모리 풀
	// 0(1) 빠르게 찾기 위한 테이블
	// 0~32
	CachePool* _poolTable[ Memory2::MAX_ALLOC_SIZE + 1 ]{};
};

template< typename Type, typename... Args >
Type* xnew( Args&&... args )
{
	Type* memory = static_cast< Type* >( PoolAllocator::Alloc( sizeof( Type ) ) );
	new( memory )Type( std::forward< Args >( args )... );

	return memory;
}

template< typename Type >
void xdelete( Type* obj )
{
	obj->~Type();
	PoolAllocator::Release( obj );
}

//template< typename Type, typename... Args >
//TPtr< Type > MakeT( Args&&... args )
//{
//	return{ xnew< Type >( std::forward< Args >( args )... ), xdelete< Type > };
//}

template< typename Type, typename... Args >
std::shared_ptr< Type > MakeShared( Args&&... args )
{
	return std::allocate_shared< Type >( StlAllocator< Type >{}, std::forward< Args >( args )... );
}

template< typename Type, typename... Args >
std::atomic< std::shared_ptr< Type > > MakeAtomicShared( Args&&... args )
{
	return std::allocate_shared< Type >( StlAllocator< Type >{}, std::forward< Args >( args )... );// { xnew< Type >( std::forward< Args >( args )... ), xdelete< Type > };
}

template< typename Type, typename... Args >
std::unique_ptr< Type, void(*)( Type* ) > MakeUnique( Args&&... args )
{
	return{ xnew< Type >( std::forward< Args >( args )... ), xdelete< Type > };
}
