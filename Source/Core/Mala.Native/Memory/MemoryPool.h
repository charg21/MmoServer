#pragma once
#include "MalaPch.h"


constexpr auto SLIST_ALIGNMENT = 16;

/// <summary>
/// 메모리 헤더
/// </summary>
struct alignas( SLIST_ALIGNMENT ) MemoryHeader : public SLIST_ENTRY
{
	// [MemoryHeader][Data]
	MemoryHeader( int size ) 
	: allocSize{ size }
	{
	}

	[[nodiscard]] static void* AttachHeader( MemoryHeader* header, int size )
	{
		new( header ) MemoryHeader( size );	// placement new
		return reinterpret_cast< void* >( ++header );
	}

	[[nodiscard]] static MemoryHeader* DetachHeader( void* ptr )
	{
		MemoryHeader* header = reinterpret_cast< MemoryHeader* >( ptr ) - 1;
		return header;
	}

	int allocSize;
};

/// <summary>
/// 메모리 풀
/// </summary>
class alignas( SLIST_ALIGNMENT ) MemoryPool
{
public:
	/// <summary>
	/// 생성자
	/// </summary>
	MemoryPool( int allocSize );
	~MemoryPool();

	void Push( MemoryHeader* ptr );
	[[nodiscard]] MemoryHeader* Pop();
	[[nodiscard]] std::vector< MemoryHeader* >& PopChunk();

private:
	SLIST_HEADER _header;
	int _allocSize{};
	std::atomic< int > _useCount{};
	std::atomic< int > _reserveCount{};
};
