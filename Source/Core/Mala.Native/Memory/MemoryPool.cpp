#pragma once

#include "MalaPch.h"
#include "MemoryPool.h"

/// <summary>
/// 생성자
/// </summary>
MemoryPool::MemoryPool( int allocSize ) 
: _allocSize{ allocSize }
{
	::InitializeSListHead( &_header );
}

/// <summary>
/// 소멸자
/// </summary>
MemoryPool::~MemoryPool()
{
	// 메모리 비우기
	while ( auto memory = static_cast< MemoryHeader* >( ::InterlockedPopEntrySList( &_header ) ) )
	{
		::_aligned_free( memory );
	}
}

/// <summary>
/// 추가한다
/// </summary>
void MemoryPool::Push( MemoryHeader* ptr )
{
	ptr->allocSize = 0;

	// Pool 메모리 반납
	::InterlockedPushEntrySList( &_header, static_cast< PSLIST_ENTRY >( ptr ) );
	_useCount.fetch_sub( 1 );
	_reserveCount.fetch_add( 1 );
}

/// <summary>
/// 제거한다
/// </summary>
MemoryHeader* MemoryPool::Pop()
{
	auto memory = static_cast< MemoryHeader* >( ::InterlockedPopEntrySList( &_header ) );
	if ( !memory ) [[unlikely]]
	{
		memory = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( _allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		//ASSERT_CRASH( memory->allocSize == 0 );
		_reserveCount.fetch_sub( 1 );
	}

	_useCount.fetch_add( 1 );

	return memory;
}

std::vector< MemoryHeader* >& MemoryPool::PopChunk()
{
	static thread_local std::vector< MemoryHeader* > memoryChunk( 32 );
	memoryChunk.clear();

	auto memory = static_cast< MemoryHeader* >( ::InterlockedPopEntrySList( &_header ) );
	if ( !memory ) [[unlikely]]
	{
		for ( int n{}; n < 32; n += 1 )
		{
			memory = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( _allocSize, SLIST_ALIGNMENT ) );
			memoryChunk.push_back( memory );
		}

		_reserveCount.fetch_sub( 32 );
		_useCount.fetch_add( 32 );
	}
	else
	{
		//ASSERT_CRASH( memory->allocSize == 0 );
		memoryChunk.push_back( memory );
		_reserveCount.fetch_sub( 1 );
		_useCount.fetch_add( 1 );
	}

	return memoryChunk;
}
