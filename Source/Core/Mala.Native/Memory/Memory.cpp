#include "MalaPch.h"
#include "MemoryPool.h"

using namespace Mala::Container;

Memory2::Memory2()
{
	int size = 0;
	int tableIndex = 0;

	// 32씩 블록 나눠서 지정
	for ( size = 32; size <= 1024; size += 32 )
	{
		MemoryPool* pool = new MemoryPool( size );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 2048; size += 128 )
	{
		MemoryPool* pool = new MemoryPool( size );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 4096; size += 256 )
	{
		MemoryPool* pool = new MemoryPool( size );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}
}

Memory2::~Memory2()
{
	for ( MemoryPool* pool : _pools )
	{
		delete pool;
	}

	_pools.clear();
}

void* Memory2::Allocate( int size )
{
	MemoryHeader* header = nullptr;
	const int allocSize = size + sizeof( MemoryHeader );

#ifdef _STOMP
	header = reinterpret_cast< MemoryHeader* >( StompAllocator::Alloc( allocSize ) );
#else
	if ( allocSize > MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 할당
		header = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		// 메모리 풀에서 꺼내온다
		header = _poolTable[ allocSize ]->Pop();
	}
#endif

	return MemoryHeader::AttachHeader( header, allocSize );
}

void* Memory2::AllocateChunk( int size )
{
	MemoryHeader* header = nullptr;
	const int allocSize = size + sizeof( MemoryHeader );

#ifdef _STOMP
	header = reinterpret_cast< MemoryHeader* >( StompAllocator::Alloc( allocSize ) );
#else
	if ( allocSize > MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 할당
		header = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		// 메모리 풀에서 꺼내온다
		auto& chunk = _poolTable[ allocSize ]->PopChunk();
		header = chunk[ 0 ];

		for ( int n = 1; n < chunk.size(); n += 1 )
			LMemoryCache->Release( MemoryHeader::AttachHeader( chunk[ n ], allocSize ) );
	}
#endif

	return MemoryHeader::AttachHeader( header, allocSize );
}

#include <psapi.h>

size_t Memory2::MemorySize()
{
	PROCESS_MEMORY_COUNTERS pmc;
	if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof( pmc ) ) )
		return pmc.WorkingSetSize;

	return pmc.PeakWorkingSetSize;
}

void Memory2::Release( void* ptr )
{
	MemoryHeader* header = MemoryHeader::DetachHeader( ptr );

	Release( header, ptr );
}

void Memory2::Release( MemoryHeader* header, void* ptr )
{
	const int allocSize = header->allocSize;
	//ASSERT_CRASH( allocSize > 0 );

#ifdef _STOMP
	StompAllocator::Release( header );
#else
	if ( allocSize > MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 해제
		::_aligned_free( header );
	}
	else
	{
		// 메모리 풀에 반납
		_poolTable[ allocSize ]->Push( header );
	}
#endif
}

MemoryCache::MemoryCache()
{
	int size = 0;
	int tableIndex = 0;

	// 32씩 블록 나눠서 지정
	for ( size = 32; size <= 1024; size += 32 )
	{
		auto* pool = new std::vector< MemoryHeader* >();
		pool->reserve( 4096 );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 2048; size += 128 )
	{
		auto* pool = new std::vector< MemoryHeader* >();
		pool->reserve( 2048 );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	for ( ; size <= 4096; size += 256 )
	{
		auto* pool = new std::vector< MemoryHeader* >();
		pool->reserve( 1024 );
		_pools.push_back( pool );

		while ( tableIndex <= size )
		{
			_poolTable[ tableIndex ] = pool;
			tableIndex++;
		}
	}

	auto& cachePoolRegister = _cachePoolRegister;
	cachePoolRegister.push_back( this );
}

MemoryCache::~MemoryCache()
{
}

void* MemoryCache::Allocate( int size )
{
	MemoryHeader* header = nullptr;
	const int allocSize = size + sizeof( MemoryHeader );

#ifdef _STOMP
	header = reinterpret_cast< MemoryHeader* >( StompAllocator::Alloc( allocSize ) );
#else
	if ( allocSize > Memory2::MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 할당
		header = reinterpret_cast< MemoryHeader* >( ::_aligned_malloc( allocSize, SLIST_ALIGNMENT ) );
	}
	else
	{
		// 메모리 풀에서 꺼내온다
		auto* pool = _poolTable[ allocSize ];
		if ( pool->empty() )
			return GMemory->AllocateChunk( size );

		// 메모리 풀에서 꺼내온다
		header = pool->back();
		pool->pop_back();
	}
#endif
	return MemoryHeader::AttachHeader( header, allocSize );
}

i64 MemoryCache::GetAllocByte( int size ) const
{
	const int allocSize = size + sizeof( MemoryHeader );
	if ( allocSize <= Memory2::MAX_ALLOC_SIZE )
	{
		if ( auto* pool = _poolTable[ allocSize ] )
			return pool->size() * allocSize;
	}

	return 0;
}


void MemoryCache::Release( void* ptr )
{
	MemoryHeader* header = MemoryHeader::DetachHeader( ptr );
	const int allocSize = header->allocSize;

	if ( allocSize > Memory2::MAX_ALLOC_SIZE )
	{
		// 메모리 풀링 최대 크기를 벗어나면 일반 해제
		::_aligned_free( header );
	}
	else
	{
		// 메모리 풀에 반납
		_poolTable[ allocSize ]->push_back( header );
		//return GMemory->Release( ptr );
	}
}

void MemoryCache::ForEachMemoryCache( const std::function< void( const MemoryCache* ) >& func )
{
	for ( const auto* cache : _cachePoolRegister )
	{
		if ( !cache )
			return;

		func( cache );
	}
}
