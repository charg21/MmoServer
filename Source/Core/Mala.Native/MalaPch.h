#pragma once

// pch.h: 미리 컴파일된 헤더 파일입니다.
// 아래 나열된 파일은 한 번만 컴파일되었으며, 향후 빌드에 대한 빌드 성능을 향상합니다.
// 코드 컴파일 및 여러 코드 검색 기능을 포함하여 IntelliSense 성능에도 영향을 미칩니다.
// 그러나 여기에 나열된 파일은 빌드 간 업데이트되는 경우 모두 다시 컴파일됩니다.
// 여기에 자주 업데이트할 파일을 추가하지 마세요. 그러면 성능이 저하됩니다.

#pragma once

// 여기에 미리 컴파일하려는 헤더 추가
#include <atomic>
#include <array>
#include <chrono>
#include <deque>
#include <concepts>
#include <functional>
#include <thread>
#include <type_traits>

#include <memory>
#include <stack>

#include <iostream>

#define WIN32_LEAN_AND_MEAN

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN
#endif

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <DbgHelp.h>
#include <tchar.h>
#include <strsafe.h>
#include <signal.h>
#include <crtdbg.h>

#pragma comment( lib, "ws2_32" )
#pragma comment( lib, "Dbghelp" )

#include <MalaMacro.h>
#include <Core/Types.h>
#include <Core/Allocator.h>
#include <Core/CoreGlobal.h>
#include <Core/CoreTLS.h>
#include <Core/CoreEnum.h>
#include <Core/Crash.h>
#include <Core/CodeLocation.h>
#include <Core/TPtr.h>
#include <Core/TickCounter.h>
#include <Core/TypeList.h>
#include <Core/TypeCast.h>
#include <Core/TypeTraits.h>
#include <Container/Container.h>
#include <Container/String.h>
#include <Container/WaitFreeQueue.h>
#include <Memory/Memory.h>
#include <Threading/Lock.h>
#include <Net/EndPoint.h>
#include <Net/NetObject.h>
#include <Net/NetEvent.h>
#include <Net/RioCore.h>
#include <Net/RioSession.h>
#include <Net/RioService.h>
#include <Net/RioListener.h>



namespace Mala::Windows
{

inline u64 GetTick()
{
    return ::GetTickCount64();
}

inline void* VirtualAllocEx( usize size )
{
    return ::VirtualAllocEx( GetCurrentProcess(), 0, size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
}

inline void VirtualFreeEx( void* bufferPtr )
{
    ::VirtualFreeEx( GetCurrentProcess(), bufferPtr, 0, MEM_RELEASE );
}

}
