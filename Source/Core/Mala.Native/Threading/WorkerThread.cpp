#include "MalaPch.h"
#include "WorkerThread.h"

static void Dispatch( int timeoutMs )
{
	Mala::Net::GNetCore->Dispatch( timeoutMs );
}

void WorkerThread::DoInit()
{
    static std::atomic< int > idIssuer{ 0 };

    LThreadId = idIssuer++;
}

void WorkerThread::DoWorkerJob()
{
    Mala::Net::GNetCore->Dispatch( 16 );
}

void WorkerThread::DoClusterJob()
{
}

void WorkerThread::DoSendJob()
{
}

std::wstring WorkerThread::GetName()
{
    return std::wstring();
}

int WorkerThread::GetNativeThreadId()
{
	return LThreadId;
}
