#include "MalaPch.h"

#include "ThreadManager.h"
#include "Core/CoreGlobal.h"

using namespace Mala::Threading;

/// <summary>
/// 생성자
/// </summary>
ThreadManager::ThreadManager()
{
    //InitTLS();
}

/// <summary>
/// 소멸자
/// </summary>
ThreadManager::~ThreadManager()
{
    //DestroyTLS();
}

/// <summary>
/// 실행한다
/// </summary>
void ThreadManager::Launch( ThreadJob initJob, ThreadJob threadJob )
{
    _threads.Emplace( ( [ = ]()
    {
        InitTLS();
        initJob();

        for ( ;; )
        {
			GNetCore->Dispatch( 8 );
            threadJob();
        }

        DestroyTLS();
    } ) );
}

inline void SetThisThreadDesc( const std::wstring &desc )
{
    ::SetThreadDescription( GetCurrentThread(), desc.c_str() );
}

void ThreadManager::InitTLS()
{
    static Atomic< u32 > sThreadId = 0;
    LThreadId = sThreadId++;

    SetThisThreadDesc( std::format( L"UnmangedThread{0}", LThreadId ) );
}

void ThreadManager::DestroyTLS()
{
}

void Mala::Threading::ThreadManager::Launch( JobPtr initJob, JobPtr threadJob )
{
    _threads.Emplace( ( [ = ]()
    {
        InitTLS();
        initJob();

        for ( ;; )
        {
            GNetCore->Dispatch( 16 );
            threadJob();
        }

        DestroyTLS();
    } ) );
}
