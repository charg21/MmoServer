#include "MalaPch.h"

#include "Lock.h"
#define LOCK_ORDER_DONT_CARE

using namespace Mala::Container;
using namespace Mala::Threading;

void RWLock::lock_shared()
{
    u32 expected;
    for ( ;; )
    {
        for ( int i = 0; i < MAX_SPIN_COUNT; i++ )
        {
            expected = ( _lockFlag.load() & READ_LOCK_MASK );
            if ( _lockFlag.compare_exchange_weak( expected, expected + 1 ) )
                return;
        }

        std::this_thread::yield();
    }
}

void RWLock::unlock_shared()
{
    if ( !( _lockFlag.fetch_sub( 1 ) & READ_LOCK_MASK ) )
    {
        // HandleError( "ReadUnlock Error" );
    }
}

void RWLock::lock()
{
    const int lockThreadId = ( _lockFlag.load() & WRITE_LOCK_MASK ) >> 16;
    if ( ( LThreadId + 1 ) == lockThreadId )
    {
        _writeCnt++;
        return;
    }

    // get write lock
    u32 expected = EMPTY_FLAG;
    u32 desired = ( ( LThreadId + 1 ) << 16 ) & WRITE_LOCK_MASK;

    for ( ;; )
    {
        for ( int i = 0; i < MAX_SPIN_COUNT; i++ )
        {
            expected = EMPTY_FLAG;
            if ( _lockFlag.compare_exchange_weak( expected, desired ) )
            {
                _writeCnt++;
                return;
            }
        }

        std::this_thread::yield();
    }
}

void RWLock::try_lock()
{
    const int lockThreadId = ( _lockFlag.load() & WRITE_LOCK_MASK ) >> 16;
    if ( ( LThreadId + 1 ) == lockThreadId )
    {
        _writeCnt++;
        return;
    }

    // get write lock
    u32 expected = EMPTY_FLAG;
    u32 desired = ( ( LThreadId + 1 ) << 16 ) & WRITE_LOCK_MASK;

    if ( _lockFlag.compare_exchange_weak( expected, desired ) )
    {
        _writeCnt++;
        return;
    }
    else
    {
        return;
    }
}

void RWLock::unlock()
{
    u32 lockThreadId = ( _lockFlag.load() & WRITE_LOCK_MASK ) >> 16;
    if ( lockThreadId == ( LThreadId + 1 ) )
    {
        const u32 lockCnt = --_writeCnt;
        if ( lockCnt == 0 )
        {
            _lockFlag.store( EMPTY_FLAG );
        }
    }
}

