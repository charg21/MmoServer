#define LOCK_ORDER_DONT_CARE

class WorkerThread
{
public:
	static void Dispatch( int timeoutMs );
	static void DoInit();
	static void DoWorkerJob();
	static void DoClusterJob();
	static void DoSendJob();

	static std::wstring GetName();
	static int GetNativeThreadId();

};
