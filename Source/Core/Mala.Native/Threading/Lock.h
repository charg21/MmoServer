#pragma once

#include <shared_mutex>

#define LOCK_ORDER_DONT_CARE

using namespace Mala::Container;

namespace Mala::Threading
{

/// <summary>
/// 락 인터페이스를 가진 객체
/// </summary>
template< typename TLockable >
concept Lockable = requires( TLockable lockable )
{
    lockable.lock_shared();
    lockable.unlock_shared();
    lockable.lock();
    lockable.unlock();
};

/// <summary>
/// 락 인터페이스를 가진 객체
/// </summary>
struct NullLock
{
    void lock_shared(){}
    void unlock_shared(){}
    void lock(){}
    void unlock(){}
};

/// <summary>
///
/// </summary>
template< typename T >
struct IsNullLock
{
    inline static constexpr bool value = std::is_same_v< NullLock, T >;
};


#ifdef LOCK_ORDER_DONT_CARE

class RWLock final
{
    enum : u32
    {
        WRITE_LOCK_MASK = 0xFFFF0000,
        READ_LOCK_MASK = 0x0000FFFF,
        MAX_SPIN_COUNT = 5000,
        EMPTY_FLAG = 0x00000000
    };

public:
    RWLock() = default;
    RWLock( const RWLock& other ) = delete;
    RWLock( RWLock&& other ) = delete;
    RWLock& operator=( const RWLock& other ) = delete;
    RWLock& operator=( RWLock&& other ) = delete;
    ~RWLock() = default;

    void lock_shared();
    void unlock_shared();

    void lock();
    void try_lock();
    void unlock();

private:
    Atomic< u32 > _lockFlag{};
    int _writeCnt = 0;
};


/// <summary>
/// 락 객체
/// </summary>
using Lock = RWLock;


#else

enum class ELockOrder
{
    DontCare,
    FirstClass,
    BusinessClass,
    EconomyClass,
};

class Lock;
class LockOrderChecker
{
    friend class Lock;

    using LockOrderRecords = Mala::Container::Array< Lock*, 64 >;

    void Push( Lock& lock );
    void Pop( Lock& lock );
    const bool IsEmpty() const;

    size_t           _top{};
    LockOrderRecords _lockOrderRecords{};
};

// LockOrderChecker* lLockOrderChecker = nullptr;

class Lock : public std::shared_mutex
{
    using Base = std::shared_mutex;

public:
    Lock( ELockOrder order = ELockOrder::DontCare );
    Lock( const Lock& ) = delete;
    Lock( Lock&& ) = delete;

    void lock_shared();
    void unlock_shared();
    void lock();
    void unlock();

    ELockOrder _order{};
};

void LockOrderChecker::Push( Lock& lock )
{
    if ( lock._order == ELockOrder::DontCare )
        return;

    if ( _top > 0 )
    {
        const Lock* lastOrderRecord = _lockOrderRecords[ _top ];

        if ( lastOrderRecord->_order < lock._order )
            Mala::Core::Crash();
    }

    _lockOrderRecords[ _top++ ] = &lock;
}

void LockOrderChecker::Pop( Lock& lock )
{
    if ( lock._order == ELockOrder::DontCare )
        return;

    if ( !_top )
        Mala::Core::Crash();

    const Lock* lastOrderRecord = _lockOrderRecords[ _top - 1 ];

    if ( lastOrderRecord != &lock )
        Mala::Core::Crash();

    _lockOrderRecords[ --_top ] = nullptr;
}

const bool LockOrderChecker::IsEmpty() const
{
    return !_top;
}

Lock::Lock( ELockOrder order )
: _order{ order }
{
}

void Lock::lock_shared()
{
    Base::lock_shared();

    lLockOrderChecker->Push( *this );
}

void Lock::unlock_shared()
{
    lLockOrderChecker->Pop( *this );

    Base::unlock_shared();
}

void Lock::lock()
{
    Base::lock();

    lLockOrderChecker->Push( *this );
}

void Lock::unlock()
{
    lLockOrderChecker->Pop( *this );

    Base::unlock();
}

#endif

template< Lockable TLock = Lock >
using WriteLock = std::unique_lock< TLock >;
using WriteLockGuard = WriteLock< Lock >;

template< Lockable TLock = Lock >
using ReadLock = std::shared_lock< TLock >;
using ReadLockGuard = ReadLock< Lock >;

} // namespace Mala::Threading