﻿#include "MalaPch.h"
#include "Compressor.h"
#include "Lz4Helper.h"

using namespace Mala::Core;

NullCompressor::NullCompressor( char* buffer, int capacity )
: Compressor{ buffer, capacity }
{
}

Lz4Compressor::Lz4Compressor( char* buffer, int capacity )
: Compressor{ buffer, capacity }
{
}


BYTE* NullCompressor::GetTempBuffer()
{
	return (BYTE*)buffer;
}

BYTE* Lz4Compressor::GetTempBuffer()
{
	static thread_local char tempBuffer[ TEMP_BUFFER_LENGTH ];
	return (BYTE*)tempBuffer;
}

size_t Lz4Compressor::Compress()
{
	// 압축이 필요없다면 그냥 복사
	if ( _length < SendPacketCompressionLength )
	{
		/// 나중엔 이것도 swap으로 처리 하도록 수정 필요
		std::memcpy( buffer, GetTempBuffer(), _length );
		return _length;
	}

	int compressedLen{ Lz4Helper::Compress(
		(const char*)GetTempBuffer(),
		_length,
		&buffer[ COMPRESSION_HEADER_LENGTH ],
		_capacity - COMPRESSION_HEADER_LENGTH ) };
	ASSERT_CRASH( compressedLen > 0 );/// 실패시 크래시

	auto header{ MakeCompressionHeader( compressedLen ) };
	*reinterpret_cast< unsigned short* >( buffer ) = header;
	_length = compressedLen + COMPRESSION_HEADER_LENGTH;
	return _length;
}

u16 Lz4Compressor::MakeCompressionHeader( u16 len )
{
	return static_cast< unsigned short >( ( len + COMPRESSION_HEADER_LENGTH ) | 0x8000 );
}

Compressor::Compressor( char* buffer, int capacity )
: buffer  { buffer   }
, _capacity{ capacity }
{
}

bool Compressor::Write( unsigned char* buffer, int len )
{
	if ( _length + len > _capacity )
		return false;

	std::memcpy( &GetTempBuffer()[ _length ], buffer, len );
	_length += len;

	return true;
}

bool Compressor::Write( std::pair< unsigned char*, int > buffer )
{
	return Write( buffer.first, buffer.second );
}

Decompressor::Decompressor()
{
}

Decompressor::~Decompressor()
{
}

int Decompressor::Decompress( BYTE* buffer, int len )
{
	if ( _length + len > _capacity )
		return 0;

	std::memcpy( buffer, buffer, len );

	buffer = buffer;
	_length = len;

	return len;
}

void Decompressor::Clear( int len )
{
	if ( _length == len )
	{
		_length = 0;
		return;
	}

	std::memcpy( buffer, &buffer[ len ], _length - len );
}

Lz4Decompressor::Lz4Decompressor()
{
	buffer = new BYTE[ 65536 ];
	_capacity = 65536;
}

Lz4Decompressor::~Lz4Decompressor()
{
	delete buffer;
}

int Lz4Decompressor::Decompress( BYTE* buffer, int len )
{
	//if ( _length + len > _capacity )
	//	return 0;

	size_t totalDecompressedLen{};
	for ( ;; )
	{
		if ( len < COMPRESSION_HEADER_LENGTH )
			return totalDecompressedLen;

		u16 length{ *reinterpret_cast< u16* >( &buffer[ totalDecompressedLen ] ) };
		u16 compressedLen{ ExtractCompressedLength( length ) };
		if ( compressedLen > len )
			return totalDecompressedLen;

		if ( IsCompressed( length ) )
		{
			auto decompressedLen = Lz4Helper::DeCompress(
				(const char*)( &buffer[ totalDecompressedLen + COMPRESSION_HEADER_LENGTH ] ),
				compressedLen - COMPRESSION_HEADER_LENGTH,
				(char*)( &GetBuffer()[ _length ] ),
				65536 - _length );
			if ( !decompressedLen )
			{
				printf( "ERROR ERROR!!" );
				return -1;
			}
			_length += decompressedLen;
		}
		else
		{
			std::memcpy( &GetBuffer()[ _length ], &buffer[ totalDecompressedLen ], compressedLen );
			_length += compressedLen;
		}

		totalDecompressedLen += compressedLen;
		len -= compressedLen;
	}

	return totalDecompressedLen;
}
