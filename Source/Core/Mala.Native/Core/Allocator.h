#pragma once

#include "MalaPch.h"
#include "Types.h"

namespace Mala::Core
{
/// <summary>
/// Base Allocator
/// </summary>
class BaseAllocator
{
public:
    static void* Alloc( int size );
    static void Release( void* ptr );
};

/// <summary>
/// Stomp Allocator
/// </summary>
class StompAllocator
{
    constexpr static auto PAGE_SIZE{ 0x1000 };

public:
    static void* Alloc( int size );
    static void Release( void* ptr );
};

/// <summary>
/// Pool Allocator
/// </summary>
class PoolAllocator
{
public:
    static void* Alloc( int size );
    static void Release( void* ptr );
};

template< typename T >
class StlAllocator
{
public:
    /// <summary>
    /// 커스텀 얼로케이터 타입 정의
    /// </summary>
    using size_type       = size_t;
    using difference_type = ptrdiff_t;
    using pointer         = T*;
    using const_pointer   = const T*;
    using reference       = T&;
    using const_reference = const T&;
    using value_type      = T;

    /// <summary>
    /// 컨테이너 타입에 사용하기 위한 중첩 구조 템플릿
    /// </summary>
    template< typename Other >
    struct rebind
    {
        using other = StlAllocator< Other >;
    };

    StlAllocator() {}
    template< typename Other >
    StlAllocator( const StlAllocator< Other >& ) {}

    T* Allocate( size_t count ) { return allocate( count ); }
    T* allocate( size_t count );

    void Deallocate( T* ptr ) { deallocate( ptr, 1 ); }
    void deallocate( T* ptr, size_t count );
};

template< typename T >
inline T* StlAllocator< T >::allocate( size_t count )
{
	const int size = static_cast< int >( count * sizeof( T ) );
	return static_cast< T* >( PoolAllocator::Alloc( size ) );
}

template< typename T >
inline void StlAllocator< T >::deallocate( T* ptr, size_t count )
{
	PoolAllocator::Release( ptr );
}


}
