#include "MalaPch.h"

namespace Mala::Core
{

/// <summary>
/// Base Allocator
/// </summary>
void* BaseAllocator::Alloc( int size )
{
	return ::malloc( size );
}

void BaseAllocator::Release( void* ptr )
{
	::free( ptr );
}

/// <summary>
/// Stomp Allocator
/// </summary>
void* StompAllocator::Alloc( int size )
{
	const i64 pageCount = ( size + PAGE_SIZE - 1 ) / PAGE_SIZE;
	return ::VirtualAlloc( NULL, pageCount * PAGE_SIZE, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE );
}

void StompAllocator::Release( void* ptr )
{
	const i64 address = reinterpret_cast< i64 >( ptr );
	const i64 baseAddress = address - ( address % PAGE_SIZE );

	::VirtualFree( reinterpret_cast< void* >( baseAddress ), 0, MEM_RELEASE );
}


/// <summary>
/// Pool Allocator
/// </summary>
void* PoolAllocator::Alloc( int size )
{
    struct MemoryCacheInitializer
    {
        MemoryCacheInitializer()
        {
            if ( !LMemoryCache )
                LMemoryCache = new MemoryCache();
        }

        ~MemoryCacheInitializer()
        {
            delete LMemoryCache;
        }
    };

	thread_local static MemoryCacheInitializer initOncePerThread;

	return LMemoryCache->Allocate( size );
	//return GMemory->Allocate( size );
}

void PoolAllocator::Release( void* ptr )
{
	LMemoryCache->Release( ptr );
	//GMemory->Release( ptr );
}

}

