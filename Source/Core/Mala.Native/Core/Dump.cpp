#include "MalaPch.h"
#include "Dump.h"
#include <stacktrace>

void myPurecallHandler()
{
    Crash();
}

void myInvalidParamaterHandler(
    const wchar_t* expression,
    const wchar_t* function,
    const wchar_t* filfile,
    unsigned int       line,
    unsigned long long pReserved )
{
    Crash();
}

void signalHandler( int error )
{
    Crash();
}

inline static constexpr size_t MAX_BUFF_SIZE = 1024;

long __stdcall exception_filter( EXCEPTION_POINTERS* e )
{
    printf( "Exception Address : 0x%08p Exception : 0x%08X\r\n",
        e->ExceptionRecord->ExceptionAddress,
        e->ExceptionRecord->ExceptionCode );
    std::cout << std::stacktrace::current() << std::endl;

    static unsigned long long dump_count = 0;
    const unsigned long long ret = ::InterlockedIncrement( &dump_count );

    if ( ret > 1 )
        return -1;

    _invalid_parameter_handler oldHandler, newHandler;
    newHandler = myInvalidParamaterHandler;

    oldHandler = _set_invalid_parameter_handler( newHandler ); // crt 함수에 null 포인터 등을 넣었을 때....
    _CrtSetReportFile( _CRT_WARN, 0 );   // CRT 오류 메시지 표시 중단. 바로 덤프로 남도록.
    _CrtSetReportFile( _CRT_ASSERT, 0 ); // CRT 오류 메시지 표시 중단. 바로 덤프로 남도록.
    _CrtSetReportFile( _CRT_ERROR, 0 );  // CRT 오류 메시지 표시 중단. 바로 덤프로 남도록.

    //_CrtSetReportHook(_custom_Report_hook);

    //---------------------------------------------------------------------------
    // pure virtual function called 에러 핸드러를 사용자 정의 함수로 우회시킨다.
    //---------------------------------------------------------------------------
    _set_purecall_handler( myPurecallHandler );

    _set_abort_behavior( 0, _WRITE_ABORT_MSG | _CALL_REPORTFAULT );

    signal( SIGABRT, signalHandler );
    signal( SIGINT, signalHandler );
    signal( SIGILL, signalHandler );
    signal( SIGFPE, signalHandler );
    signal( SIGSEGV, signalHandler );
    signal( SIGTERM, signalHandler );

    TCHAR      tszFileName[ MAX_BUFF_SIZE ]{};
    SYSTEMTIME stTime{};
    GetSystemTime( &stTime );
    StringCbPrintf(
        tszFileName,
        _countof( tszFileName ),
        _T( "[%s][%s]_%4d%02d%02d_%02d%02d%02d_%d.dmp" ),
        _T( "dump" ),
        _T( "Mala.Unmanaged" ),
        stTime.wYear,
        stTime.wMonth,
        stTime.wDay,
        stTime.wHour,
        stTime.wMinute,
        stTime.wSecond,
        ret );

    HANDLE hFile = CreateFile(
        tszFileName, GENERIC_WRITE, /*FILE_SHARE_READ*/ FILE_SHARE_READ, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL,
        0 );
    if ( hFile == INVALID_HANDLE_VALUE )
    {
        printf( "Dump Error : %d \n", GetLastError() );
        return EXCEPTION_EXECUTE_HANDLER;
    }

    MINIDUMP_EXCEPTION_INFORMATION exceptionInfo;
    exceptionInfo.ThreadId = GetCurrentThreadId();
    exceptionInfo.ExceptionPointers = e;
    exceptionInfo.ClientPointers = FALSE;

    MINIDUMP_TYPE mdt =
        (MINIDUMP_TYPE)( MiniDumpWithPrivateReadWriteMemory | MiniDumpWithDataSegs | MiniDumpWithHandleData | MiniDumpWithFullMemoryInfo | MiniDumpScanMemory | MiniDumpWithThreadInfo | MiniDumpWithUnloadedModules | MiniDumpWithFullMemory );

    // MiniDumpWriteDump를 사용하여 hFile에 덤프 기록
    MiniDumpWriteDump(
        GetCurrentProcess(), GetCurrentProcessId(), hFile,
        mdt, //(MINIDUMP_TYPE)(MiniDumpWithPrivateReadWriteMemory | MiniDumpScanMemory | MiniDumpWithFullMemory),
        ( e != nullptr ) ? &exceptionInfo : 0, 0, NULL );

    if ( hFile )
    {
        CloseHandle( hFile );
        hFile = NULL;
    }

    return 0;
}

void Dump::Setup()
{
    SetUnhandledExceptionFilter( exception_filter );
}