#pragma once

namespace Mala::Core
{

/// <summary>
///
/// </summary>
struct DefaultDeleter
{
    template< typename T >
    static void Delete( T* ptr ) noexcept
    {
        delete ptr;
    }
};

struct IRefCountable{};

/// <summary>
///
/// </summary>
template< typename Counter, typename Deleter = DefaultDeleter >
struct TRefCountableBase : public IRefCountable
{
    using DeleterType = Deleter;
    using CounterType = Counter;

    TRefCountableBase()
    : _refCount{ 1 }
    {
    }

    virtual ~TRefCountableBase() {}

    int AddRef()
    {
        return ++_refCount;
    }

    int GetRefCount()
    {
        return _refCount;
    }

    int ReleaseRef()
    {
        auto refCount = --_refCount;
        if ( !refCount )
            Deleter::Delete( this );

        return refCount;
    }

    Counter _refCount;
};

/// <summary>
///
/// </summary>
/// <typeparam name="Deleter"></typeparam>
template< typename Deleter >
using TRefCountable      = TRefCountableBase< int, Deleter >;
template< typename Deleter >
using TAtomicRefCountable = TRefCountableBase< std::atomic< int >, Deleter >;

using RefCountable       = TRefCountable< DefaultDeleter >;
using AtomicRefCountable = TAtomicRefCountable< DefaultDeleter >;

template< typename TRefCountable >
concept RefCountableType = requires( TRefCountable refCountable )
{
    refCountable.AddRef();
    refCountable.ReleaseRef();
};

/// <summary>
///
/// </summary>
/// <typeparam name="T"></typeparam>
template< typename T >
struct IsRefCountable
{
    //using CounterType = typename T::CounterType;
    //using DeleterType = typename T::DeleterType;

    inline static constexpr bool value = std::is_base_of< IRefCountable, T >::value;
        //std::is_base_of< TRefCountableBase< CounterType, DeleterType >, T >::value;
};

using Rc  = RefCountable;
using Arc = AtomicRefCountable;

/// <summary>
/// 침습형 레퍼런스 카운트를 사용하는 스마트 포인터
/// </summary>
/// <typeparam name="T"> 타입 </typeparam>
template< RefCountableType T >
class TPtr
{
    static_assert( IsRefCountable< T >::value,"T Must Derived Type From TRefCountable" );

public:
    using element_type   = T;
    using pointer_type   = T*;
    using reference_type = T&;

public:
    /// <summary>
    /// 생성자
    /// </summary>
    TPtr()
    : _refCountable{}
    {
    }

    TPtr( nullptr_t )
    : _refCountable{}
    {
    }

    TPtr( T* refCount )
    : _refCountable{ refCount }
    {
        if ( !refCount )
            return;

        refCount->AddRef();
    }

    TPtr( T* refCount, bool )
    : _refCountable{ refCount }
    {
    }

    TPtr( const TPtr& other )
    : _refCountable{ other._refCountable }
    {
        if ( !other._refCountable )
            return;

        other._refCountable->AddRef();
    }

    TPtr( TPtr&& other ) noexcept
    : _refCountable{ other._refCountable }
    {
        other._refCountable = nullptr;
    }

    template< typename U >
    TPtr( const TPtr< U >& other )
    : _refCountable{ static_cast< T* >( other._refCountable ) }
    {
        if ( !other._refCountable )
            return;

        other._refCountable->AddRef();
    }

    template< typename U >
    TPtr( TPtr< U >&& other ) noexcept
    : _refCountable{ static_cast< T* >( other._refCountable ) }
    {
        other._refCountable = nullptr;
    }

    /// <summary>
    /// 소멸자
    /// </summary>
    ~TPtr()
    {
        if ( _refCountable )
            _refCountable->ReleaseRef();

        _refCountable = nullptr;
    }

    TPtr& operator=( nullptr_t )
    {
        Release();

        return *this;
    }

    TPtr& operator=( const TPtr& other )
    {
        if ( this == &other )
            return *this;

        Release();
        Set( other._refCountable );

        return *this;
    }

    TPtr& operator=( TPtr&& other ) noexcept
    {
        Release();

        _refCountable = other._refCountable;
        other._refCountable = nullptr;

        return *this;
    }

    int GetRefCount()
    {
        if ( !_refCountable )
            return 0;

        return _refCountable->GetRefCount();
    }

    const int GetRefCount() const
    {
        if ( !_refCountable )
            return 0;

        return _refCountable->GetRefCount();
    }

    T* Get()
    {
        return _refCountable;
    }

    T* Get() const
    {
        return _refCountable;
    }

    T* operator->()
    {
        return Get();
    }

    T* operator->() const
    {
        return Get();
    }

    bool operator!()
    {
        return _refCountable == nullptr;
    }

    const bool operator!() const
    {
        return _refCountable == nullptr;
    }

    bool operator==( const TPtr& other ) const
    {
        return _refCountable == other._refCountable;
    }

    bool operator!=( const TPtr& other ) const
    {
        return _refCountable != other._refCountable;;
    }

    inline void Release()
    {
        if ( !_refCountable )
            return;

        _refCountable->ReleaseRef();
        _refCountable = nullptr;
    }

    inline void Set( T* ptr )
    {
        _refCountable = ptr;
        if ( ptr )
            ptr->AddRef();
    }

    friend static bool operator==( const TPtr& lhs, const TPtr& rhs );
    friend static bool operator!=( const TPtr& lhs, const TPtr& rhs );
    template< RefCountableType Type >
    friend class TPtr;
    template< RefCountableType To, RefCountableType From >
    friend TPtr< To > TypeCast( TPtr< From >& ptr );

private:
    T* _refCountable;
};

template< typename T >
using InstrusivePtr = TPtr< T >;

/// <summary>
///
/// </summary>
template< typename T >
bool operator==( const TPtr< T >& lhs, const TPtr< T >& rhs )
{
    return lhs._refCountable == rhs._refCountable;
}

/// <summary>
///
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="lhs"></param>
/// <param name="rhs"></param>
/// <returns></returns>
template< typename T >
bool operator!=( const TPtr< T >& lhs, const TPtr< T >& rhs )
{
    return lhs._refCountable != rhs._refCountable;
}

/// <summary>
///
/// </summary>
template< typename T, typename... Args >
TPtr< T > MakeInstrusivePtr( Args&&... args )
{
    return TPtr< T >( new T( std::forward< Args >( args )... ), true );
}

template< typename T, typename... Args >
TPtr< T > MakeTPtr( Args&&... args )
{
    return TPtr< T >( new T( std::forward< Args >( args )...), true );
}

/// <summary>
/// 침습형 스마트 포인터 클래스
/// </summary>
/// <typeparam name="T"></typeparam>
template< typename T >
struct IsInstrusivePtr
{
    using element_type   = typename T::element_type;

    inline static constexpr bool value =
        std::is_same< IsInstrusivePtr< element_type >, T >::value;
};

/// <summary>
/// 침습형 스마트 포인터 클래스
/// </summary>
/// <typeparam name="T"></typeparam>
template< RefCountableType T >
inline constexpr bool IsInstrusivePtrV = IsInstrusivePtr< T >::value;

template< typename T >
concept InstrusivePtrType = std::is_same_v< T, TPtr< typename T::element_type > >;
template< typename T >
concept TPtrType = std::is_same_v< T, TPtr< typename T::element_type > >;


}

#include "MalaPch.h"