#pragma once

namespace Mala::Core
{

/// <summary>
/// 크래시를 발생시킨다( 유효하지 않은 메모리 영역 접근 )
/// </summary>
inline void Crash()
{
    *( (size_t*)nullptr ) = 0xDEADBEEF;
}

/// <summary>
/// 표현식의 결과가 거짓이라면 크래시를 발생시킨다
/// </summary>
inline void CrashIfFalse( bool isFalse )
{
    if ( !isFalse )
        Crash();
}

/// <summary>
/// 포인터가 널이라면 크래시를 발생시킨다
/// </summary>
inline void CrashIfNull( void* pointer )
{
    if ( !pointer )
        Crash();
}

} // namespace Mala::Core


#include "MalaPch.h"