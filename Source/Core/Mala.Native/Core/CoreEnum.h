#pragma once

#include <type_traits>

namespace Mala
{

/// <summary>
/// 서비스 타입 열거형
/// </summary>
enum class EServiceType
{
	Server = 1 << 0,
	Client = 1 << 1,

	All = ~0
};

/// <summary>
///
/// </summary>
enum class EDisconnectReason
{
	Kickout,
};

enum
{
    CP_UTF7_ = 65000,      // UTF-7 translation
    CP_UTF8_ = 65001,       // UTF-8 translation


    TIMEOUT_INFINITE = INFINITE,
    INVALID_SOCK = INVALID_SOCKET,

    CK_RIO = 0xC0DE,
    CK_JOB = CK_RIO + 1,
    RIO_CORRUPT_CQ_CODE = RIO_CORRUPT_CQ,

    SESSION_BUFFER_SIZE = 65536,

    MAX_RIO_RESULT = 1024,
    MAX_SEND_RQ_SIZE_PER_SOCKET = 128,
    MAX_RECV_RQ_SIZE_PER_SOCKET = 8,
    MAX_CLIENT = 5000,
    MAX_CQ_SIZE = ( MAX_SEND_RQ_SIZE_PER_SOCKET + MAX_RECV_RQ_SIZE_PER_SOCKET ) * MAX_CLIENT,
};

}