#pragma once

#include <type_traits>;
#include <concepts>;
#include <bit>;

namespace Mala
{

template< typename T > concept Enumerable = std::is_enum_v< T >;
template< typename T > concept Enumeration = std::is_enum_v< T >;

/// 64비트 값의 최하위 비트가 1인 위치
int CustomFfs( unsigned int value )
{
    unsigned long index{}; // 비트 인덱스를 저장할 변수
    if ( _BitScanForward( &index, value ) )
        return static_cast< int >( index + 1 ); // 1-based 인덱스 반환

    return 0; // 모든 비트가 0인 경우
}

/// 64비트 값의 최하위 비트가 1인 위치를 반환한다.
int CustomFfsll( unsigned long long value )
{
    unsigned long index{}; // 비트 인덱스를 저장할 변수
    if ( _BitScanForward64( &index, value ) )
        return static_cast< int >( index + 1 ); // 1-based 인덱스 반환

    return 0; // 모든 비트가 0인 경우
}

//template< typename TTypeList, typename T >
//constexpr int GetEnumFlagValue()
//{
//	return Mala::Math::PowOfTwo< IndexOfV< TTypeList, T > >();
//}

template< Enumerable TEnum >
struct EnumHelper
{
	static void SetFlag( TEnum& value, TEnum flag )
	{
		value = (TEnum)( static_cast< std::underlying_type_t< TEnum > >( value ) |
			static_cast< std::underlying_type_t< TEnum > >( flag ) ) ;
	}

	static bool HasFlag( const TEnum value, const TEnum flag )
	{
		return static_cast< std::underlying_type_t< TEnum > >( value ) &
			   static_cast< std::underlying_type_t< TEnum > >( flag );
	}

	static void ForEachEnum( const TEnum value )
	{
	}

    constexpr static int AsIndex( TEnum enumValue )
    {
        return CustomFfsll( static_cast< std::underlying_type_t< TEnum > >( enumValue ) );
    }

    constexpr static int AsIndex( unsigned long long enumValue )
    {
        return CustomFfsll( enumValue );
    }

};

}
