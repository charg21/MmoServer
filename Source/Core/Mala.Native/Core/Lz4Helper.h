#pragma once

class Lz4Helper
{
public:
    inline static constexpr int CompressionHeaderLength{ 2 };

    static bool IsCompressed( const char* buffer, size_t bufferLen );

    static size_t ExtractLength( size_t dataLen );

    static int Compress(
        const char* inBuffer,
        int inBufferLen,
        char* outBuffer,
        int outBufferLen );

    static size_t DeCompress(
        const char* inBuffer,
        size_t inBufferLen,
        char* outBuffer,
        size_t outBufferLen );
};

