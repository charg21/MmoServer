#pragma once


#pragma region TypeList
template< typename... T >
struct TypeList;

template< typename T, typename U >
struct TypeList< T, U >
{
    using Head = T;
    using Tail = U;
};

template< typename T, typename... U >
struct TypeList< T, U... >
{
    using Head = T;
    using Tail = TypeList< U... >;
};
#pragma endregion

#pragma region Length
template< typename T >
struct Length;

template<>
struct Length< TypeList<> >
{
    inline static constexpr auto value{ 0 };
};

template< typename T, typename... U >
struct Length< TypeList< T, U... > >
{
    inline static constexpr auto value{ Length< TypeList< U... > >::value + 1 };
};

//template< typename TL, typename T >
//constexpr auto LengthV = Length< TL, T >::value;

template< typename TL >
constexpr int LengthV()
{
    return Length< TL >::value;
}

#pragma endregion

#pragma region TypeAt
template< typename TL, int Index >
struct TypeAt;

template< typename Head, typename... Tail >
struct TypeAt< TypeList< Head, Tail... >, 0 >
{
    using Result = Head;
};

template< typename Head, typename... Tail, int Index >
struct TypeAt< TypeList< Head, Tail... >, Index >
{
    using Result = typename TypeAt< TypeList< Tail... >, Index - 1 >::Result;
};

template< typename TL, int Index >
using TypeAtT = TypeAt< TL, Index >::Result;

#pragma endregion

#pragma  region IndexOf
template< typename TL, typename T >
struct IndexOf;

template< typename... Tail, typename T >
struct IndexOf< TypeList< T, Tail... >, T >
{
    inline static constexpr auto value{ 0 };
};

template< typename T >
struct IndexOf< TypeList<>, T >
{
    inline static constexpr auto value{ -1 };
};

template< typename Head, typename... Tail, typename T >
struct IndexOf< TypeList< Head, Tail... >, T >
{
private:
    inline static constexpr auto temp{ IndexOf< TypeList< Tail...>, T >::value };

public:
    inline static constexpr auto value{ ( temp == -1 ) ? -1 : temp + 1 };
};

template< typename TL, typename T >
constexpr auto IndexOfV = IndexOf< TL, T >::value;

#pragma endregion

#pragma region PushBack

template< typename TL, typename T >
struct PushBack;

template< typename... Ts, typename T >
struct PushBack< TypeList< Ts... >, T >
{
    using Result = TypeList< Ts..., T >;
};

template< typename TL, typename T >
using PushBackT = typename PushBack< TL, T >::Result;

#pragma endregion


#pragma region ToTypeList

template< typename Tuple >
struct ToTypeListInternal;

template< typename... T >
struct ToTypeListInternal< std::tuple< T... > >
{
    using Result = TypeList< T... >;
};

template< typename Tuple >
using ToTypeList = typename ToTypeListInternal< Tuple >::Result;

#pragma endregion

#pragma region ToTuple

template< typename TL >
struct ToTuple;

template< typename... Types >
struct ToTuple< TypeList< Types...> >
{
    using Result = std::tuple< typename std::decay< Types >::type... >;
};

template< typename TL, typename... Args >
typename ToTuple< TL >::Result MakeTuple( Args&&... args )
{
    return typename ToTuple< TL >::Result{ std::forward< Args >( args )... };
}

template < int Index, typename T, typename S >
void ForeachTupleUsingIndex( T&& t, S&& f )
{
    if constexpr ( Index < std::tuple_size_v< std::decay_t< T > > )
    {
        f( std::get< Index >( t ) );

        ForeachTupleUsingIndex< Index + 1 >( t, std::forward< S >( f ) );
    }
}

template< typename... Ts, typename S >
void ForeachTuple( const std::tuple< Ts... >& t, S&& s )
{
    ForeachTupleUsingIndex< 0 >( t, std::forward< S >( s ) );
}

template< typename... T, typename F >
void ForeachTupleType( const std::tuple< T... >& t, F&& f )
{
    ( f( ( T* )nullptr ), ... );
}
#pragma endregion

#include "MalaPch.h"