#pragma once

#define OUT

#define NAMESPACE_BEGIN( name ) namespace name {
#define NAMESPACE_END }

/// <summary>
/// Lock
/// </summary>
#define USE_MANY_LOCKS( count )	Lock _locks[ count ];
#define USE_LOCK				USE_MANY_LOCKS( 1 );
#define READ_LOCK_IDX( idx )	ReadLockGuard readLockGuard_##idx( _locks[ idx ], typeid( this ).name() );
#define READ_LOCK				READ_LOCK_IDX( 0 );
#define WRITE_LOCK_IDX( idx )	WriteLockGuard writeLockGuard_##idx( _locks[ idx ], typeid( this ).name() );
#define WRITE_LOCK				WRITE_LOCK_IDX( 0 );

/// <summary>
/// Memory
/// </summary>
#ifdef _DEBUG
#define Xalloc( size )		PoolAllocator::Alloc( size )
#define Xrelease( size )	PoolAllocator::Release( size )
#else
#define Xalloc( size )		BaseAllocator::Alloc( size )
#define Xrelease( size )	BaseAllocator::Release( size )
#endif

/// <summary>
/// Crash
/// </summary>
#define CRASH( cause )						\
{											\
	u32* crash = nullptr;				    \
	_Analysis_assume_( crash != nullptr );	\
	*crash = 0XDEADBEEF;					\
}

#define ASSERT_CRASH( expr )				\
{											\
	if ( !( expr ) )						\
	{										\
		CRASH( "ASSERT_CRASH" );			\
		_Analysis_assume_( expr );			\
	}										\
}

/// <summary>
/// Forwarding
/// </summary>
#define MOVE( object ) object = std::move( object );
#define FORWARD( object ) std::forward< decltype( object ) >( object )
#define FORWARD2( object ) std::forward< decltype( object ) >( object )...

/// <summary>
/// Smart Pointer
/// </summary>
#define USING_SHARED_PTR( CLASS )                      \
using CLASS##Ptr     = std::shared_ptr< class CLASS >; \
using CLASS##Ref     = const CLASS##Ptr&;              \
using CLASS##WeakPtr = std::weak_ptr< class CLASS >;   \
using CLASS##WeakRef = const CLASS##WeakPtr&;

/// <summary>
/// Strong Type
/// </summary>
#define USING_STRONG_TYPE( Name, Base ) \
struct Name ## StrongTypeTag : public StrongTypeTag {}; \
using Name = StrongType< Base, Name ## StrongTypeTag >;

#define USING_STRONG_VALUE_TYPE( Name, Base ) \
struct Name ## StrongTypeTag : public StrongTypeTag {}; \
using Name = StrongValueType< Base, Name ## StrongTypeTag >;

