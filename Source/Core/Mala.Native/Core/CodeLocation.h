#pragma once

#define WIDEN(x)           L ## x
#define WIDEN2(x)         WIDEN(x)
#define __WFILE__          WIDEN2( __FILE__ )
#define __WFUNCTION__ WIDEN2( __FUNCTION__ )

#ifdef _NODISCARD
#else
#define _NODISCARD      [[ nodiscard ]]
#endif
#define _NODISCARD_CTOR [[ nodiscard ]]

struct CodeLocation
{
public:
    _NODISCARD static consteval CodeLocation Here(
        const u32 _Line_   = __builtin_LINE(),
        const char* const _File_ = __builtin_FILE(),
// #if defined( __clang__ ) || defined( __EDG__ ) // TRANSITION, DevCom-10199227 and LLVM-58951
        const char* const _Function_ = __builtin_FUNCTION()
//#else // ^^^ workaround / no workaround vvv
//        const char* const _Function_ = __builtin_FUNCSIG()
//#endif // TRANSITION, DevCom-10199227 and LLVM-58951
    ) noexcept
    {
        CodeLocation _Result;

        _Result._Line     = _Line_;
        _Result._File     = _File_;
        _Result._Function = _Function_;

        return _Result;
    }

    _NODISCARD_CTOR constexpr CodeLocation() noexcept = default;

    _NODISCARD constexpr u32 line() const noexcept
    {
        return _Line;
    }

    _NODISCARD constexpr const char* file_name() const noexcept
    {
        return _File;
    }

    _NODISCARD constexpr const char* function_name() const noexcept
    {
        return _Function;
    }


    u32 _Line {};
    const char* _File = "";
    const char* _Function = "";
};

using SourceLocation = CodeLocation;
