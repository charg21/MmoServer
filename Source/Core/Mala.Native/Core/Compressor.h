﻿#pragma once

//namespace Mala::Core
//{

class Compressor
{
public:
	Compressor() = default;
	Compressor( char* buffer, int capacity );
	virtual ~Compressor() = default;
	virtual size_t Compress(){ /* Do Nothing... */ return _length; }

	/// <summary>
	/// 인플레이스 압축 미지원, 임시 버퍼 필요
	/// </summary>
	virtual BYTE* GetTempBuffer() = 0;
	bool Write( unsigned char* buffer, int len );
	bool Write( std::pair< unsigned char*, int > buffer );
	int GetLength() { return _length; }
	void Clear() { _length = 0; }

protected:
	char* buffer{};
	int   _capacity{};
	int   _length{};
};

/// <summary>
/// 압축 안함
/// </summary>
class NullCompressor final : public Compressor
{
public:
	NullCompressor( char* buffer, int len );
	BYTE* GetTempBuffer() final;
};

/// <summary>
/// LZ4 알고리즘을 사용하는 압축기
/// </summary>
class Lz4Compressor final : public Compressor
{
	static constexpr size_t TEMP_BUFFER_LENGTH{ 1024 * 1024 };
	static constexpr size_t COMPRESSION_HEADER_LENGTH{ 2 };
	static constexpr size_t SendPacketCompressionLength{ 100 };

public:
	Lz4Compressor( char* buffer, int len );
	BYTE* GetTempBuffer() final;
	size_t Compress() final;

private:
	u16 MakeCompressionHeader( u16 len );
};


class Decompressor
{
	static constexpr size_t TEMP_BUFFER_LENGTH{ 1024 * 1024 };

public:
	Decompressor();
	virtual ~Decompressor();
	virtual int Decompress( BYTE* buffer, int len );

	BYTE* GetBuffer(){ return buffer; }
	int GetLength() { return _length; }
	void Clear( int len );
protected:
	BYTE* buffer{};
	int   _capacity{};
	int   _length{};
};

class Lz4Decompressor final : public Decompressor
{
	static constexpr size_t COMPRESSION_HEADER_LENGTH{ 2 };
public:
	Lz4Decompressor();
	virtual ~Lz4Decompressor();

	virtual int Decompress( BYTE* buffer, int len );
	static bool IsCompressed( u16 header ){ return header & 0x8000; }
	static u16 ExtractCompressedLength( u16 header ){ return header & 0x7FFF; }
};



//} // namespace Mala::Core