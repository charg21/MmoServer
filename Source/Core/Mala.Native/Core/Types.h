#pragma once

/// <summary>
/// 기본 타입 정의
/// </summary>
using i8    = char;
using i16   = short;
using i32   = int;
using i64   = long long;
using isize = i64;

using u8 = unsigned char;
using u16 = unsigned short;
using u32 = unsigned int;
using u64 = unsigned long long;
using usize = unsigned long long;

using f32 = float;
using f64 = double;

template< typename T >
using Atomic = std::atomic< T >;
using ai8 = Atomic< char >;
using ai16 = Atomic< short >;
using aint = Atomic< int >;
using ai64 = Atomic< long long >;
using aisize = Atomic< long long >;

using BYTE = unsigned char;


template< typename TRet, typename... Args >
using Func = std::function< TRet( Args... ) >;
template< typename... Args >
using Task = std::function< void( Args... ) >;


/// <summary>
/// 플래그 타입 정의
/// </summary>
using Flag = unsigned long long;

/// <summary>
/// 문자열 뷰 타입 정의
/// </summary>
using StringView = std::wstring_view;
using StringViewRef = const StringView&;


NAMESPACE_BEGIN( Mala::Net )

USING_SHARED_PTR( INetObject );

USING_SHARED_PTR( RioCore );
USING_SHARED_PTR( RioEvent );
USING_SHARED_PTR( RioObject );
USING_SHARED_PTR( RioSession );
USING_SHARED_PTR( RioService );
USING_SHARED_PTR( RioClientService );
USING_SHARED_PTR( RioServerService );
USING_SHARED_PTR( RioListener );

USING_SHARED_PTR( NetCore );
USING_SHARED_PTR( SendBuffer );
USING_SHARED_PTR( SendBufferChunk );

USING_SHARED_PTR( PacketSession );
USING_SHARED_PTR( HttpSession );

NAMESPACE_END( Mala::Net )

#include "MalaPch.h"
