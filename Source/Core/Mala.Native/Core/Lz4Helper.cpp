#pragma once
#include "MalaPch.h"
#include "Lz4Helper.h"
#include "lz4/lz4.h"

bool Lz4Helper::IsCompressed( const char* buffer, size_t bufferLen )
{
    if ( bufferLen < CompressionHeaderLength )
        return false;

    auto header = *reinterpret_cast< const unsigned short* >( buffer );
    if ( header & 0x8000 )
        return false;

    return true;
}

size_t Lz4Helper::ExtractLength( size_t dataLen )
{
    return dataLen &= 0x7FFF;
}

int Lz4Helper::Compress(
    const char* inBuffer,
    int inBufferLen,
    char* outBuffer,
    int outBufferLen )
{
    int maxCompressedSize{ LZ4_compressBound( inBufferLen ) };
    if ( outBufferLen < maxCompressedSize )
        return 0;

    int compressedLen{ LZ4_compress_default(
        inBuffer,
        outBuffer,
        inBufferLen,
        maxCompressedSize ) };
    if ( compressedLen <= 0 )
        return compressedLen;

    //printf( "%d => %d\n", inBufferLen, compressedLen );

    return compressedLen;
}

size_t Lz4Helper::DeCompress(
    const char* compressedBuffer,
    size_t compressedSize,
    char* outBuffer,
    size_t outBufferLen )
{
    int decompressedSize = LZ4_decompress_safe( compressedBuffer, outBuffer, compressedSize, outBufferLen );
    if ( decompressedSize < 0 )
        return 0;

    //printf( "%d => %d\n", compressedSize, decompressedSize );

    return decompressedSize;
}
