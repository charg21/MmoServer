#pragma once

/// <summary>
/// T타입 기본타입 여부 추론 클래스
/// </summary>
template< typename T >
struct IsDefaultType
{
    inline static constexpr bool value =
        std::is_integral< T >::value || std::is_floating_point< T >::value;
};

/// <summary>
/// T 타입 기본 타입 여부
/// </summary>
template< typename T >
inline constexpr bool IsDefaultTypeV = IsDefaultType< T >::value;

/// <summary>
/// T타입 Pod 타입 여부 추론 클래스
/// </summary>
template< typename T >
struct IsPod
{
    inline static constexpr bool value =
        std::is_standard_layout< T >::value || std::is_trivially_copyable< T >::value;
};

/// <summary>
/// T 타입 Pod 타입 여부
/// </summary>
template< typename T >
inline constexpr bool IsPodV = IsPod< T >::value;

/// <summary>
/// 조건에 따른 타입
/// </summary>
template< bool condition, typename T, typename F >
struct Conditional
{
    using type = T;
};

template< typename T, typename F >
struct Conditional< false, T, F >
{
    using type = F;
};

template< bool condition, typename T, typename F >
using ConditionalT = Conditional< condition, T, F >::type;

/// <summary>
/// 타입 로그용, 빌드 실패
/// </summary>
template< typename T >
class TypeLog;

#include "MalaPch.h"