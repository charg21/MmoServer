#pragma once

#include <memory>

NAMESPACE_BEGIN( Mala::Threading )

inline extern class ThreadManager*        GThreadManager{};
inline extern class GlobalQueue*          GGlobalQueue{};
inline extern class JobTimer*             GJobTimer{};

NAMESPACE_END( Mala::Threading )

NAMESPACE_BEGIN( Mala::Net )

inline extern class SendBufferManager*              GSendBufferManager{};
inline extern std::shared_ptr< Mala::Net::RioCore > GNetCore{};

NAMESPACE_END( Mala::Net )

NAMESPACE_BEGIN( Mala::Db )
NAMESPACE_END( Mala::Db )

NAMESPACE_BEGIN( Mala::Diagnostics )

inline extern class PerformanceCounter* GPerformanceCounter{};

NAMESPACE_END( Mala::Diagnostics )

inline extern class Memory2* GMemory{};


/// <summary>
///
/// </summary>
class CoreGlobal
{
public:
	CoreGlobal();
	~CoreGlobal();
};

inline static CoreGlobal GCoreGlobal;