#pragma once

using namespace std;

namespace Mala::Core
{

__forceinline constexpr size_t GetTick()
{
	return *reinterpret_cast< size_t* >( 0x7FFE'0008 ) / 1'0000;
}

}

#include "MalaPch.h"