#pragma once

#include "MalaPch.h"

using namespace Mala::Threading;
using namespace Mala::Net;


inline extern thread_local       u32          LThreadId{ (u32)( -1 ) };
inline extern thread_local       u64          LEndTickCount{};
inline extern thread_local       u64          LLastTickCount{};
inline extern thread_local class MemoryCache* LMemoryCache{};

