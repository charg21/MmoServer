using System.Net.Sockets;

namespace Mala.Net;

/// <summary>
/// 패킷 통신 세션
/// </summary>
#if !NATIVE
public abstract class PacketSession : ManagedSession
#else
public abstract class PacketSession : NativeSession
#endif
{
    public const int HeaderLength = 2;
    public const int HeaderType = 2;

    /// <summary>
    /// 데이터를 수신한경우 호출되는 콜백
    /// [ size( 2 ) ] [ packetType( 2 ) ] [ payload( ... ) ]
    /// </summary>
    public override int OnRecv( ReadOnlySpan< byte > buffer )
    {
        int processLen  = 0;
        int packetCount = 0;

        for ( ;; )
        {
            // 최소한 헤더는 파싱할 수 있는지 확인
            if ( buffer.Length < HeaderLength )
                break;

            // 패킷이 완전체로 도착했는지 확인
            u16 dataSize = BitConverter.ToUInt16( buffer );
            if ( buffer.Length < dataSize )
                break;

            OnRecvPacket( ref buffer );
            packetCount += 1;

            processLen += dataSize;
            buffer     =  buffer.Slice( dataSize );
        }

        return processLen;
    }

    public abstract void OnRecvPacket( ref ReadOnlySpan< byte > buffer );
}