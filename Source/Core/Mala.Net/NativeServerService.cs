﻿using Mala.Net;
using System.Net;

/// <summary>
/// 네이티브 서비스 래핑 클래스
/// </summary>
public class NativeServerService : NativeServerServiceInternal
{
    SessionFactory _sessionFactory;

    IPEndPoint _endPoint;

    NativeListener _listener;

    public NativeServerService( IPEndPoint endPoint, SessionFactory factory, int capacity )
    : base( endPoint.Address.ToString(), endPoint.Port, factory, capacity )
    {
        /// 라이프사이클이 끝나지 않도록 잡고 홀딩
        _sessionFactory = factory;
        _endPoint       = endPoint;
        _listener       = new NativeListener( _endPoint, _sessionFactory, capacity );

        RegisterListener( _listener );
    }

    public NativeServerService( string ip, int port, SessionFactory factory, int capacity )
    : base( ip, port, factory, capacity )
    {
        /// 라이프사이클이 끝나지 않도록 잡고 홀딩
        _sessionFactory = factory;
        _endPoint       = new( IPAddress.Parse( ip ), port );
        _listener       = new NativeListener( _endPoint, _sessionFactory, capacity );

        RegisterListener( _listener );
    }

    public bool LoadConfig( string[] args ) { return true; }

    protected override bool OnInit()
    {
        return true;
    }

    public override bool OnStart()
    {
        Console.WriteLine( "Start!!" );

        for ( ;; )
        {
            Thread.Sleep( 30 * 1000 );
        }

        return true;
    }

    public bool Start()
    {
        base.Start();
        return OnStart();
    }

}
