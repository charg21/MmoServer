﻿using System.Net;
using System.Net.Sockets;

namespace Mala.Net;

public class Connector
{
#if !NATIVE
    private Func< Session > _sessionFactory;

    public void Connect( IPEndPoint endPoint, Func< Session > sessionFactory, int count = 1 )
    {
        for ( int i = 0; i < count; i++ )
        {
            var socket = new Socket( endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp );
            _sessionFactory = sessionFactory;

            var args = new SocketAsyncEventArgs();
            args.Completed += OnConnectCompleted;
            args.RemoteEndPoint = endPoint;
            args.UserToken = socket;

            RegisterConnect( args );
        }
    }
#else
    private Func< NativeSession > _sessionFactory;

    public void Connect( IPEndPoint endPoint, Func< NativeSession > sessionFactory, int count = 1 )
    {
        _sessionFactory = sessionFactory;

        for ( int i = 0; i < count; i++ )
        {
            var session = sessionFactory.Invoke();
            session.Connect();

            //session
            // var socket = new Socket( endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp );

            //var args = new SocketAsyncEventArgs();
            //args.Completed      += OnConnectCompleted;
            //args.RemoteEndPoint = endPoint;
            //args.UserToken      = socket;

            //RegisterConnect( args );
        }
    }
#endif

    void RegisterConnect( SocketAsyncEventArgs args )
    {
        Socket socket = args.UserToken as Socket;
        if ( socket is null )
            return;

        bool pending = socket.ConnectAsync( args );
        if ( !pending )
            OnConnectCompleted( null, args );
    }

    void OnConnectCompleted( object sender, SocketAsyncEventArgs args )
    {
#if !NATIVE
        if ( args.SocketError == SocketError.Success )
        {

            Session session = _sessionFactory.Invoke();
            session.Start( args.ConnectSocket );
            session.OnConnected( args.RemoteEndPoint );
        }
        else
        {
            Console.WriteLine( $"OnConnectCompeted Fail: { args.SocketError }" );
        }
#else
#endif
    }
}

public class NatvieConnector
{
    private Func< NativeSession > _sessionFactory;
    private IPEndPoint _endPoint;

    NatvieConnector( IPEndPoint endPoint, Func< NativeSession > sessionFactory )
    {
        _sessionFactory = sessionFactory;
        _endPoint       = endPoint;
    }

    public void Connect( int count = 1 )
    {
        for ( int i = 0; i < count; i++ )
        {
            var session = _sessionFactory.Invoke();
            session.Connect();
        }
    }
}
