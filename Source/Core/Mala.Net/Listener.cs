﻿using System.Net;
using System.Net.Sockets;

namespace Mala.Net;

/// <summary>
///
/// </summary>
public class Listener
{
    /// <summary>
    ///
    /// </summary>
    private Socket _listenSocket;

    /// <summary>
    ///
    /// </summary>
    private Func< Session > _sessionFactory;

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public void Initialize( IPEndPoint endPoint, Func< Session > sessionFactory, int register = 10, int backlog = 100 )
    {
        _listenSocket   =  new Socket( endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp );
        _sessionFactory += sessionFactory;

        _listenSocket.Bind( endPoint );
        _listenSocket.Listen( backlog );

        for ( int i = 0; i < register; i++ )
        {
            var args = new SocketAsyncEventArgs();
            args.Completed += OnAcceptCompleted;
            RegisterAccept( args );
        }
    }

    /// <summary>
    ///
    /// </summary>
    void RegisterAccept( SocketAsyncEventArgs args )
    {
        args.AcceptSocket = null;

        bool pending = _listenSocket.AcceptAsync( args );
        if ( !pending )
            OnAcceptCompleted( null, args );
    }

    /// <summary>
    ///
    /// </summary>
    void OnAcceptCompleted( object sender, SocketAsyncEventArgs args )
    {
        if ( args.SocketError == SocketError.Success )
        {
            Session session = _sessionFactory.Invoke();
            session.Start( args.AcceptSocket );
            session.OnConnected( args.AcceptSocket.RemoteEndPoint, true );
        }
        else
        {
            Console.WriteLine( args.SocketError.ToString() );
        }

        RegisterAccept( args );
    }
}