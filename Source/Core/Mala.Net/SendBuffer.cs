﻿using Mala.Logging;
using Microsoft.Extensions.ObjectPool;
using Pipelines.Sockets.Unofficial.Buffers;

namespace Mala.Net;

public class SendBufferHelper
{
    public static ThreadLocal< SendBuffer > CurrentBuffer = new( () => null );

    public const int ChunkSize = 65536 * ( 256 / 2 );
    public static DefaultObjectPool< SendBuffer > SendBufferPool { get; set; } = new( new SendBufferPoolPolicy(), 256 );

    /// <summary>
    ///
    /// </summary>
    public static SendSegment Open( int reserveSize )
    {
        if ( CurrentBuffer.Value is null )
            Rent();

        if ( CurrentBuffer.Value!.FreeSize < reserveSize )
            Rent();

        return CurrentBuffer.Value.Open( reserveSize );
    }

    public static void Rent()
    {
        CurrentBuffer.Value?.ReleaseRef();

        Interlocked.Increment( ref SendBuffer.RentCount );

        CurrentBuffer.Value = SendBufferPool.Get();
        CurrentBuffer.Value.AddRef();
    }

    public static SendSegment Close( int usedSize )
        => CurrentBuffer.Value!.Close( usedSize );
}

public class SendBufferPoolPolicy : IPooledObjectPolicy< SendBuffer >
{
    public SendBuffer Create()
    {
        Interlocked.Increment( ref SendBuffer.NewCount );

        return new SendBuffer();
    }

    public bool Return( SendBuffer buffer )
    {
        buffer._usedSize = 0;

        if ( buffer._releaseRefCount != buffer._addRefCount )
        {
            Log.Critical( $"Why.... YesYesMe Release { buffer._releaseRefCount } Add { buffer._addRefCount }" );
            Mala.Core.Exception.ThrowIfFailed( false, $"What The... YesYesMe Release { buffer._releaseRefCount } Add { buffer._addRefCount}" );
        }

        return true;
    }
}

public struct SendSegment
{
    public ArraySegment< byte > _segment = new();

    public Span< byte > Span
        => new( _segment.Array, _segment.Offset, _segment.Count );

    public SendBuffer _owner;

    /// <summary>
    ///  생성자
    /// </summary>
    public SendSegment( SendBuffer owner, ArraySegment< byte > segment )
    {
        _owner   = owner;
        _segment = segment;
    }

    /// <summary>
    /// 배열
    /// </summary>
    public byte[] Array => _segment.Array;

    /// <summary>
    /// 오프셋
    /// </summary>
    public int Offset => _segment.Offset;

    /// <summary>
    /// 갯수
    /// </summary>
    public int Count => _segment.Count;

    /// <summary>
    /// 참조 카운트를 올린다.
    /// </summary>
    public void AddRef() => _owner.AddRef();

    /// <summary>
    /// 참조 카운트를 해제한다.
    /// </summary>
    public void ReleaseRef() => _owner.ReleaseRef();

    public void Clear()
    {
        _owner = null;
    }

    /// <summary>
    /// 통합한다.
    /// </summary>
    public bool CanMerge( ref SendSegment other )
    {
        if ( _owner != other._owner )
            return false;

        if ( ( Offset + Count ) != other.Offset )
            return false;

        return true;
    }

    /// <summary>
    /// 통합한다.
    /// </summary>
    public void Merge( ref SendSegment other )
    {
        Mala.Core.Exception.ThrowIfFailed( _owner == other._owner, "Why???" );
        Mala.Core.Exception.ThrowIfFailed( ( Offset + Count ) == other.Offset, "헉..." );

        _segment = new( Array, Offset, Count + other.Count );
    }
}

/// <summary>
///
/// </summary>
public class SendBuffer
{
    public static long NewCount = 0;
    public static long RentCount = 0;
    public static long ReturnCount = 0;

    /// <summary>
    /// 버퍼
    /// </summary>
    public byte[] _buffer;

    /// <summary>
    /// 사용된 크기
    /// </summary>
    public int _usedSize = 0;

    /// <summary>
    /// 레퍼런스 카운트
    /// </summary>
    public i64 _addRefCount = 0;

    /// <summary>
    /// 반환된 레퍼런스 카운트
    /// </summary>
    public i64 _releaseRefCount = 0;

    /// <summary>
    /// 남은 공간
    /// </summary>
    public int FreeSize => _buffer.Length - _usedSize;

    /// <summary>
    /// 스레드에서 할당하는 숫자
    /// </summary>
    public i64 AddRef() => ++_addRefCount;

    /// <summary>
    /// 해제 숫자
    /// </summary>
    public i64 ReleaseRef()
    {
        var releasedRefCount = Interlocked.Increment( ref _releaseRefCount );
        var diffRefCount = _addRefCount - releasedRefCount;
        if ( diffRefCount == 0 )
        {
            Interlocked.Increment( ref SendBuffer.ReturnCount );
            SendBufferHelper.SendBufferPool.Return( this );
        }
        else if ( diffRefCount < 0 )
        {
            Mala.Core.Exception.Throw( $"말도 안됨...{ diffRefCount } " );
        }

        return releasedRefCount;
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public SendBuffer()
    {
        // 명시적으로 POH에 할당
        _buffer = GC.AllocateUninitializedArray< byte >( SendBufferHelper.ChunkSize, true );
    }

    public SendSegment Open( int reserveSize )
    {
        return new( this, new ArraySegment< byte >( _buffer, _usedSize, reserveSize ) );
    }


    public SendSegment Close( int usedSize )
    {
        var segment = new ArraySegment< byte >( _buffer, _usedSize, usedSize );
        _usedSize += usedSize;

        return new( this, segment );
    }
}
