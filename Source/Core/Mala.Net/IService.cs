namespace Mala.Net;

/// <summary>
/// 네트워크 서비스 인터페이스
/// </summary>
public interface IService
{
    /// <summary>
    /// 설정을 불러온다
    /// </summary>
    public bool LoadConfig( string[] args );
    
    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public bool Initialize();

    /// <summary>
    /// 서비스를 시작한다
    /// </summary>
    public void Start();

    /// <summary>
    /// 서비스 이름
    /// </summary>
    public string Name { get; }
}

/// <summary>
/// 네트워크 서비스
/// </summary>
public abstract class Service : IService
{
    /// <summary>
    /// 서비스 이름
    /// </summary>
    public string Name => GetType().Name;

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public abstract bool Initialize();

    /// <summary>
    /// 설정을 불러온다
    /// </summary>
    public abstract bool LoadConfig( string[] args );

    /// <summary>
    /// 서비스를 시작한다
    /// </summary>
    public abstract void Start();
}
