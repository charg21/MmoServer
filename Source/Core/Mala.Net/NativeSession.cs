﻿using Mala.Collection;
using System.Collections.Generic;
using System.Net;

namespace Mala.Net;

/// <summary>
/// Mala.CLI C++ 세션 래핑 클래스
/// </summary>
public abstract class NativeSession : NativeSessionInternal, ISession
{
    /// <summary>
    /// 송신예약 중인 버퍼 목록
    /// </summary>
    private WaitFreeQueue< SendSegment > _sendQueue = new();

    /// <summary>
    /// 송신중인 버퍼 목록
    /// </summary>
    private List< SendSegment > _pendingList = new( 1024 );

    /// <summary>
    /// 락
    /// </summary>
    private Lock _lock = new();

    public override void Clear()
    {
        ClearPendingList( 0 );
    }

    public void ClearPendingList( int bytes )
    {
        var newPendingList = new List< SendSegment >( _pendingList.Capacity );
        List< SendSegment > pendingList;

        lock ( _lock )
        {
            pendingList = _pendingList;
            _pendingList = newPendingList;
        }

        foreach ( var buff in pendingList.AsSpan() )
            buff.ReleaseRef();
        pendingList.Clear();
    }

    public void Send( ref SendSegment sendBuff )
    {
        if ( !IsConnected() )
            return;

        RequestSend( ref sendBuff );
        FlushSendQueue();
    }

    public void Send( SendSegment sendBuff ) => Send( ref sendBuff );

    public void FlushSendQueue()
    {
        lock ( _lock )
        {
            if ( _pendingList.Count == 0 )
                RegisterSend();
        }
    }

    void RegisterSend()
    {
        while ( _sendQueue.TryDequeue( out var buff ) )
            _pendingList.Add( buff );

        if ( _pendingList.Count == 0 )
            return;

        foreach ( var buff in _pendingList.AsSpan() )
            base.Send( buff._segment.AsSpan() );
    }

    public void RequestSend( SendSegment sendBuff ) => RequestSend( ref sendBuff );

    public void RequestSend( ref SendSegment sendBuff )
    {
        sendBuff.AddRef();
        _sendQueue.Enqueue( sendBuff );
    }

    public override void OnDisconnected( System.Net.EndPoint endPoint )
    {
        lock ( _lock )
        {
            while ( _sendQueue.TryDequeue( out var buff ) )
                buff.ReleaseRef();
        }
    }

    /// <summary>
    /// Mala.CLI Rio코어에서 호출되는 Send 완료후 콜백
    /// </summary>
    public override void OnSendCompleted( int bytes )
    {
        ClearPendingList( bytes );
        OnSend( bytes );
    }

    public abstract void OnSend( int bytes );

}
