﻿namespace Mala.Net;

/// <summary>
/// 서버간 통신 세션 객체
/// </summary>
public abstract class BypassPacketSession : PacketSession
{
    public const int BypassFilter = 0x8000;

    /// <summary>
    /// 데이터를 수신한경우 호출되는 콜백
    /// [ size( 2 ) ]
    /// [ packetType( 2 ) ] 상위 1bit는 bypass여부
    /// [ payload( ... ) ]
    /// </summary>
    public override int OnRecv( ReadOnlySpan< byte > buffer )
    {
        int processLen = 0;

        for ( ;; )
        {
            // 최소한 헤더는 파싱할 수 있는지 확인
            if ( buffer.Length < HeaderLength )
                break;

            // 패킷이 완전체로 도착했는지 확인
            u16 dataSize = BitConverter.ToUInt16( buffer );
            if ( buffer.Length < dataSize )
                break;

            // 바이패스 패킷 여부를 확인한다.
            u16 packetType = BitConverter.ToUInt16( buffer.Slice( 2, 2 ) );
            if ( packetType >= BypassFilter )
                OnRecvBaypassPacket( ref buffer );
            else
                OnRecvPacket( ref buffer );

            processLen += dataSize;
            buffer     = buffer.Slice( dataSize );
        }

        return processLen;
    }

    public abstract void OnRecvBaypassPacket( ref ReadOnlySpan< byte > buffer );

}
