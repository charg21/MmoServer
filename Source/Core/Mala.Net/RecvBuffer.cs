﻿using System.Collections.Concurrent;

namespace Mala.Net;

/// <summary>
/// 
/// </summary>
public class RecvBufferHelper
{
    /// <summary>
    /// 
    /// </summary>
    public static ConcurrentStack< RecvBuffer > Buffers = new();

    /// <summary>
    /// 
    /// </summary>
    public static ConcurrentStack< RecvBufferChunk > BufferChunks = new();

    /// <summary>
    /// 
    /// </summary>
    public const int BufferSize = 65536;

    /// <summary>
    /// 
    /// </summary>
    public const int BufferCount = 128;

    /// <summary>
    /// 
    /// </summary>
    public const int ChunkSize = BufferSize * BufferCount;

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public static RecvBuffer Rent()
    {
        for ( ;; )
        {
            if ( Buffers.TryPop( out var buffer ) )
                return buffer;

            BufferChunks.Push( new RecvBufferChunk() );
        }
        
        return null;
    }

    public static void Return( RecvBuffer buffer )
    {
        buffer.Clean();
        Buffers.Push( buffer );
    }
}

/// <summary>
/// 수신 버퍼 
/// </summary>
public class RecvBufferChunk
{
    /// <summary>
    /// 생성자
    /// </summary>
    public RecvBufferChunk()
    {
        /// POH 할당
        var buf = GC.AllocateArray< byte >( RecvBufferHelper.ChunkSize, true );

        for ( var i = 0; i < RecvBufferHelper.BufferCount; i += 1 )
        {
            RecvBuffer buffer = new( new( buf, RecvBufferHelper.BufferSize * i, RecvBufferHelper.BufferSize ) );
            RecvBufferHelper.Buffers.Push( buffer );
        }
    }
}


/// <summary>
/// 수신 버퍼
/// </summary>
public class RecvBuffer
{
    /// <summary>
    /// 
    /// </summary>
    private ArraySegment< byte > _buffer;

    /// <summary>
    /// 
    /// </summary>
    private int _readPos;

    /// <summary>
    /// 
    /// </summary>
    private int _writePos;

    /// <summary>
    /// 생성자
    /// </summary>
    public RecvBuffer( ArraySegment< byte > segment )
    {
        _buffer = segment;
    }

    /// <summary>
    /// 
    /// </summary>
    public int DataSize => _writePos - _readPos;

    /// <summary>
    /// 
    /// </summary>
    public int FreeSize => _buffer.Count - _writePos;

    public ArraySegment< byte > ReadSegment => new( _buffer.Array, _buffer.Offset + _readPos, DataSize );
    public ReadOnlySpan< byte > ReadSpan => new( _buffer.Array, _buffer.Offset + _readPos, DataSize );

    public ArraySegment< byte > WriteSegment => new( _buffer.Array, _buffer.Offset + _writePos, FreeSize );

    public void Clean()
    {
        int dataSize = DataSize;
        if ( dataSize == 0 )
        {
            _readPos = _writePos = 0;
        }
        else
        {
            Array.Copy( _buffer.Array, _buffer.Offset + _readPos, _buffer.Array, _buffer.Offset, dataSize );

            _readPos  = 0;
            _writePos = dataSize;
        }
    }

    public void Reset()
    {
        _readPos = 0;
        _writePos = 0;
    }

    public bool OnRead( int numOfByte )
    {
        if ( numOfByte > DataSize )
            return false;

        _readPos += numOfByte;
        return true;
    }

    public bool OnWrite( int numOfByte )
    {
        if ( numOfByte > FreeSize )
            return false;

        _writePos += numOfByte;
        return true;
    }
}