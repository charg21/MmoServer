﻿using Mala.Collection;
using System;
using System.Net;
using System.Net.Sockets;

namespace Mala.Net;

/// <summary>
/// 연결을 나타내는 객체
/// </summary>
public abstract partial class Session : ISession
{
    /// <summary>
    /// 소켓
    /// </summary>
    private Socket _socket;

    /// <summary>
    /// 연경 종료 여부
    /// </summary>
    private int _disconnected = 0;

    /// <summary>
    /// 수신 버퍼
    /// </summary>
    private RecvBuffer _recvBuffer;

    /// <summary>
    /// 락
    /// </summary>
    private Lock _lock = new();

    /// <summary>
    /// 송신예약 중인 버퍼 목록
    /// </summary>
    private WaitFreeQueue< SendSegment > _sendQueue = new();

    /// <summary>
    /// 송신중인 버퍼 목록
    /// </summary>
    private List< SendSegment > _pendingList2 = new( 512 );
    private List< ArraySegment< byte > > _pendingList = new( 512 );

    /// <summary>
    /// 송신 이벤트 구조체
    /// </summary>
    private SocketAsyncEventArgs _sendArgs = new();

    /// <summary>
    /// 수신 이벤트 구조체
    /// </summary>
    private SocketAsyncEventArgs _recvArgs = new();

    public bool IsDisconnected => _disconnected == 1;

    /// <summary>
    /// 시작한다
    /// </summary>
    public void Start( Socket socket )
    {
        _socket = socket;

        _recvBuffer = RecvBufferHelper.Rent();

        _recvArgs.Completed += OnRecvCompleted;
        _sendArgs.Completed += OnSendCompleted;

        RegisterRecv();
    }

    /// <summary>
    ///
    /// </summary>
    void Clear()
    {
        lock ( _lock )
        {
            // _sendQueue.Clear();
            _pendingList.Clear();

            foreach ( var sendBuff in _pendingList2 )
                sendBuff.ReleaseRef();

            _pendingList2.Clear();
        }
    }

    public void Send( SendSegment sendBuff )
    {
        sendBuff.AddRef();
        _sendQueue.Enqueue( sendBuff );

        FlushSendQueue();
    }
    public void Send( ref SendSegment sendBuff )
    {
        RequestSend( ref sendBuff );
        FlushSendQueue();
    }

    public void RequestSend( SendSegment sendBuff )
    {
        sendBuff.AddRef();
        _sendQueue.Enqueue( sendBuff );
    }

    public void RequestSend( ref SendSegment sendBuff )
    {
        sendBuff.AddRef();
        _sendQueue.Enqueue( sendBuff );
    }

    public void FlushSendQueue()
    {
        lock ( _lock )
        {
            if ( _pendingList.Count == 0 )
                RegisterSend();
        }
    }

    public void Send( List< SendSegment > sendBuffList )
    {
        if ( _pendingList.Count == 0 )
            return;

        foreach ( var buffer in sendBuffList )
            buffer.AddRef();

        _sendQueue.Enqueue( sendBuffList );

        lock ( _lock )
        {
            if ( _pendingList.Count == 0 )
                RegisterSend();
        }
    }

    /// <summary>
    ///
    /// </summary>
    public void Disconnect( EDisconnectReason reason )
    {
        if ( Interlocked.Exchange( ref _disconnected, 1 ) == 1 )
            return;

        OnDisconnected( _socket.RemoteEndPoint );//, reason );

        _socket.Shutdown( SocketShutdown.Both );
        _socket.Close();

        var recvBuffer = _recvBuffer;
        _recvBuffer = null;
        RecvBufferHelper.Return( recvBuffer );
    }

    #region Network IO Method

    void RegisterSend()
    {
        if ( IsDisconnected )
            return;

        while ( _sendQueue.TryDequeue( out var buff ) )
        {
            _pendingList2.Add( buff );
            _pendingList.Add( buff._segment );
        }

        if ( _pendingList.Count == 0 )
            return;

        _sendArgs.BufferList = _pendingList;

        try
        {
            bool pending = _socket.SendAsync( _sendArgs );
            if ( !pending )
                OnSendCompleted( null, _sendArgs );
        }
        catch ( Exception ex )
        {
            Console.WriteLine( $"RegisterSend Fail { ex } " );
        }
    }

    void OnSendCompleted( object sender, SocketAsyncEventArgs args )
    {
        if ( args is { BytesTransferred: > 0, SocketError: SocketError.Success } )
        {
            lock ( _lock )
            {
                try
                {
                    foreach ( var sendBuff in _pendingList2 )
                        sendBuff.ReleaseRef();

                    _pendingList2.Clear();
                    _pendingList.Clear();
                    // _sendArgs.BufferList = null;

                    OnSend( _sendArgs.BytesTransferred );

                    // 디큐가 가능한 경우
                    if ( _sendQueue.TryPeek( out var dest ) )
                        RegisterSend();
                }
                catch ( Exception e )
                {
                    Console.WriteLine( $"OnSendCompleted Failed { e }" );
                }
            }
        }
        else
        {
            Disconnect( EDisconnectReason.Kickout );
        }
    }

    void RegisterRecv()
    {
        if ( IsDisconnected )
            return;

        _recvBuffer.Clean();
        var segment = _recvBuffer.WriteSegment;
        _recvArgs.SetBuffer( segment.Array, segment.Offset, segment.Count );

        try
        {
            bool pending = _socket.ReceiveAsync( _recvArgs );
            if ( !pending )
                OnRecvCompleted( null, _recvArgs );
        }
        catch ( Exception e )
        {
            Console.WriteLine( $"RegisterRecv Fail{e}" );
            throw;
        }
    }


    void OnRecvCompleted( object sender, SocketAsyncEventArgs args )
    {
        if ( args is { BytesTransferred: > 0, SocketError: SocketError.Success } )
        {
            try
            {
                // Write 커서 이동
                if ( !_recvBuffer.OnWrite( args.BytesTransferred ) )
                {
                    Disconnect( EDisconnectReason.Kickout );
                    return;
                }

                // 컨텐츠 쪽으로 데이터를 넘겨주고 얼마나 처리했는지를 받는다.
                int processLen = OnRecv( _recvBuffer.ReadSpan );
                if ( processLen < 0 || _recvBuffer.DataSize < processLen )
                {
                    Disconnect( EDisconnectReason.Kickout );
                    return;
                }

                // Read 커서 이동
                if ( !_recvBuffer.OnRead( processLen ) )
                {
                    Disconnect( EDisconnectReason.Kickout );
                    return;
                }

                RegisterRecv();
            }
            catch ( Exception e )
            {
                Console.WriteLine( $"OnRecvCompleted Failed {e}" );
            }
        }
        else
        {
            Disconnect( EDisconnectReason.Kickout );
        }
    }
    #endregion
}

/// <summary>
///
/// </summary>
public interface ISession
{
    public void OnConnected( EndPoint endPoint, bool success );

    public int OnRecv( ReadOnlySpan< byte > buffer );

    public void OnSend( int numOfBytes ) { }

    public void OnDisconnected( EndPoint endPoint );

}


/// <summary>
///
/// </summary>
public partial class Session
{
    public abstract void OnConnected( EndPoint endPoint, bool success );

    public abstract int OnRecv( ReadOnlySpan< byte > buffer );

    public abstract void OnSend( int numOfBytes );

    public abstract void OnDisconnected( EndPoint endPoint );

}