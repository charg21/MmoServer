﻿//using Mala.Collection;
using System.Net;

namespace Mala.Net;

/// <summary>
///
/// </summary>
public class NativeListener : NativeListenerInternal
{
    /// <summary>
    ///
    /// </summary>
    SessionFactory _sessionFactory;

    /// <summary>
    ///
    /// </summary>
    IPEndPoint _endPoint;

    public NativeListener()
    {
    }

    public NativeListener( IPEndPoint endPoint, SessionFactory sessionFactory, int capacity )
    {
        Init( endPoint, sessionFactory, capacity );
    }

    public void Init( IPEndPoint endPoint, SessionFactory sessionFactory, int capacity )
    {
        /// 라이프사이클이 끝나지 않도록 잡고 홀딩
        _sessionFactory = sessionFactory;
        _endPoint       = endPoint;

        base.Init( endPoint.Address.ToString(), endPoint.Port, sessionFactory, capacity );
    }

}
