#include "CliPch.h"
#include "NativeWorkerThread.h"
#include "../Mala.Native/Core/CoreTLS.h"
#include "../Mala.Native/Core/CoreGlobal.h"
#include "../Mala.Native/Threading/WorkerThread.h"

using namespace Mala::Net;

using namespace System::Runtime::InteropServices;
using namespace System;


void NativeWorkerThread::DoInit()
{
	WorkerThread::DoInit();
}

bool NativeWorkerThread::Dispatch( int timeoutMs )
{
	return Mala::Net::GNetCore->Dispatch( timeoutMs );
}

int NativeWorkerThread::GetNativeThreadId()
{
	return WorkerThread::GetNativeThreadId();
}
