#pragma once

public ref class NativeWorkerThread
{
public:
	static void DoInit();
	static bool Dispatch( int timeoutMs );
	static int GetNativeThreadId();
};

