#pragma once

#include "NativeSessionFactory.h"


using namespace System::Runtime::InteropServices;
using namespace System;

namespace Mala::Net
{
class RioService;
class RioListener;
class RioClientService;
class RioServerService;
};

using namespace Mala::Net;

ref class NativeSessionInternal;
ref class NativeServerServiceInternal;

using ListenerPtr = std::shared_ptr< RioListener >;
using ListenerRef = const ListenerPtr&;

public ref class NativeListenerInternal
{
public:
	NativeListenerInternal();
	NativeListenerInternal(
		System::String^ ip,
		int             port,
		SessionFactory^ sessionFactoryPtr,
		int             capacity );

	bool Init(
		System::String^    ip,
		int                port,
		SessionFactory^    sessionFactoryPtr,
		int                capacity );

	virtual ~NativeListenerInternal();
	//bool Start();
	ListenerPtr GetNativeListener();

protected:
	virtual bool OnInit(){ return true; }

private:
	/// <summary>
	/// ���� ���丮
	/// </summary>
	msclr::gcroot< SessionFactory^ >* _sessionFactory{};

	ListenerPtr* _listenerHolder{};
};