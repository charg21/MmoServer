#pragma once

#include "NativeSessionFactory.h"

using namespace System::Runtime::InteropServices;
using namespace System;

namespace Mala::Net
{
class RioService;
class RioClientService;
class RioServerService;
};

using namespace Mala::Net;

ref class NativeListenerInternal;

public delegate void WorkerThreadJob();

enum class ENativeServiceType
{
	Server,
	Client,
	Max
};

class ServiceHolder
{
public:
	/// <summary>
	/// RIO 서비스
	/// </summary>
	std::shared_ptr< RioService > _ptr;

	/// <summary>
	/// 세션 팩토리
	/// </summary>
	msclr::gcroot< SessionFactory^ > _sessionFactoryPtr;

	/// <summary>
	/// 서비스 타입
	/// </summary>
	ENativeServiceType _type;

public:
	/// 생성자
	ServiceHolder(
		ENativeServiceType type,
		System::String^    ip,
		int                port,
		SessionFactory^    sessionFactoryPtr,
		int                capacity );

	/// 소멸자
	~ServiceHolder();

	const RioService* operator->() const
	{
		return _ptr.get();
	}

	RioService* operator->()
	{
		return _ptr.get();
	}

};


public ref class NativeServiceInternal
{
public:
	NativeServiceInternal(
		ENativeServiceType type,
		System::String^    ip,
		int                port,
		SessionFactory^    sessionFactoryPtr,
		int                capacity );

	virtual ~NativeServiceInternal();

	bool Initialize();

	bool Start();
	virtual bool OnStart(){ return true; }

	void Dispatch( int timeoutMs );

	long GetNativeMemorySize();
	String^ GetNativeMemoryStatus();
	void SetThreadCount( int threadCount ){ _threadCount = threadCount; }

protected:
	virtual bool OnPreInit(){ return true; }
	virtual bool OnInit(){ return true; }
	virtual bool OnPostInit(){ return true; }

	ServiceHolder* _holder;

	int _threadCount{};
};


public ref class NativeServerServiceInternal : NativeServiceInternal
{
public:
	NativeServerServiceInternal(
		System::String^ ip,
		int             port,
		SessionFactory^ sessionFactory,
		int             capacity );
	~NativeServerServiceInternal() override;

	void RegisterListener( NativeListenerInternal^ nativeListener );
};


public ref class NativeClientServiceInternal : NativeServiceInternal
{
public:
	NativeClientServiceInternal(
		System::String^ ip,
		int             port,
		SessionFactory^ sessionFactory,
		int             capacity );

	~NativeClientServiceInternal() override;

	void Connect( int sessionCount );
};