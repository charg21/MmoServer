#pragma once

#include "CliPch.h"
#include "NativeSession.h"

#pragma managed(push, off)

#include "../Mala.Native/MalaPch.h"
#include "../Mala.Native/Net/RioSession.h"
#include "../Mala.Native/Core/CoreGlobal.h"
#pragma managed(pop)

using namespace Mala::Net;


NativeSessionInternal::NativeSessionInternal()
{
	_holder = new SessionHolder();

	_sendJobHandler = gcnew SendJobHandler( this, &NativeSessionInternal::OnSendCompleted );
	_holder->_ptr->SetOnSend( Marshal::GetFunctionPointerForDelegate( _sendJobHandler ).ToPointer() );

	_recvJobHandler = gcnew RecvJobHandler( this, &NativeSessionInternal::OnRecvNative );
	_holder->_ptr->SetOnRecv( Marshal::GetFunctionPointerForDelegate( _recvJobHandler ).ToPointer() );

	_connectedJobHandler = gcnew ConnectedJobHandler( this, &NativeSessionInternal::OnConnectedNative );
	_holder->_ptr->SetOnConnected( Marshal::GetFunctionPointerForDelegate( _connectedJobHandler ).ToPointer() );

	_disconnectedJobHandler = gcnew DisconnectedJobHandler( this, &NativeSessionInternal::OnDisconnectedNative );
	_holder->_ptr->SetOnDisconnected( Marshal::GetFunctionPointerForDelegate( _disconnectedJobHandler ).ToPointer() );
}

/// <summary>
/// C++ 인터페이스
/// </summary>
void NativeSessionInternal::Clear()
{
}

/// <summary>
/// C++ 인터페이스
/// </summary>
inline int NativeSessionInternal::OnRecvNative( unsigned char* buffer, int len )
{
	// C++의 버퍼를 Span으로 변환
	return OnRecv( ReadOnlySpan< unsigned char >( buffer, len ) );
}

void NativeSessionInternal::OnConnectedNative( const std::wstring& ip, int port, bool success )
{
	auto managedIp = gcnew System::String( ip.c_str(), 0, ip.length() );

	// Create a new IPEndPoint
	auto endPoint = gcnew System::Net::IPEndPoint( System::Net::IPAddress::Parse( managedIp ), port );

	return OnConnected( endPoint, success );
}

void NativeSessionInternal::OnDisconnectedNative()
{
	System::String^ managedIp = gcnew System::String( std::wstring( L"127.0.0.1" ).c_str(), 0, 9 );

	// Create a new IPEndPoint
	System::Net::IPEndPoint^ endPoint = gcnew System::Net::IPEndPoint( System::Net::IPAddress::Parse( managedIp ), 18888 );

	OnDisconnected( endPoint );

	//delete _holder;
}

NativeSessionInternal::~NativeSessionInternal()
{
	delete _holder;
}

void NativeSessionInternal::Send( System::Span< unsigned char > sendBuffer )
{
	if ( !_holder )
		return;

	/// pinning을 해도 괜찮도록 C#단 SendBuffer POH에서 할당함
	pin_ptr< unsigned char > ptr = &sendBuffer[ 0 ];
	_holder->_ptr->Send( (unsigned char*)( ptr ), sendBuffer.Length );
}

void NativeSessionInternal::Connect()
{
	_holder->_ptr->Connect();
}

bool NativeSessionInternal::IsConnected()
{
	return _holder->_ptr->IsConnected();
}

inline SessionHolder::SessionHolder()
{
	_ptr = MakeShared< CliRioSession >();
}

inline SessionHolder::~SessionHolder()
{
	_ptr.reset();
}

