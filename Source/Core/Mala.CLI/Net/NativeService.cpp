#pragma once

#include "CliPch.h"
#include "NativeService.h"
#include "NativeSession.h"
#include "NativeListener.h"
#include <msclr/marshal.h>
#pragma managed(push, off)

#include "../Mala.Native/MalaPch.h"
#include "../Mala.Native/Net/RioSession.h"
#include "../Mala.Native/Net/RioService.h"
#include "../Mala.Native/Net/EndPoint.h"
#include "../Mala.Native/Core/CoreGlobal.h"
#include "../Mala.Native/Core/CoreTLS.h"
#include "../Mala.Native/Memory/Memory.h"
#include "../Mala.Native/Threading/WorkerThread.h"
#include "../Mala.Native/Threading/ThreadManager.h"

#pragma managed(pop)


/// <summary>
/// 생성자
/// </summary>
NativeServiceInternal::NativeServiceInternal(
	ENativeServiceType type,
	System::String^    ip,
	int                port,
	SessionFactory^    sessionFactoryPtr,
	int                capacity )
: _holder{ new ServiceHolder( type, ip, port, sessionFactoryPtr, capacity ) }
{
}

/// <summary>
/// 소멸자
/// </summary>
NativeServiceInternal::~NativeServiceInternal()
{
	delete _holder;
}

/// <summary>
/// 초기화 한다.
/// </summary>
bool NativeServiceInternal::Initialize()
{
	if ( !OnPreInit() )
		return false;

	_holder->_ptr->GetCoreRef()->Initialize( _threadCount );
	if ( !OnInit() )
		return false;

	return OnPostInit();
}

bool NativeServiceInternal::Start()
{
	_holder->_ptr->Start();

	return OnStart();
}

long NativeServiceInternal::GetNativeMemorySize()
{
	return GMemory->MemorySize();
}

void NativeServiceInternal::Dispatch( int timeoutMs )
{
	_holder->_ptr->GetCoreRef()->Dispatch( timeoutMs );
}

String^ NativeServiceInternal::GetNativeMemoryStatus()
{
	return gcnew String( "" );
}

ServiceHolder::ServiceHolder(
	ENativeServiceType type,
	System::String^    ip,
	int                port,
	SessionFactory^    sessionFactory,
	int                capacity )
: _type             { type           }
, _sessionFactoryPtr{ sessionFactory }
{
	//struct CoreInitializer
	//{
	//	CoreInitializer()
	//	{
	//		GNetCore = MakeShared< Mala::Net::RioCore >();
	//	}
	//};

	//static CoreInitializer initOnce{};

	auto core = MakeShared< Mala::Net::RioCore >();

	if ( type == ENativeServiceType::Server )
	{
		_ptr = MakeShared< RioServerService >( core );
	}
	else
	{
		pin_ptr< const wchar_t > wch = PtrToStringChars( ip );

		auto factoryTask = [ this ]
		{
			NativeSessionInternal^ session = _sessionFactoryPtr->Invoke();
			return session->_holder->_ptr;
		};

		_ptr = MakeShared< RioClientService >(
			EndPoint{ wch, (u16)( port ) },
			core,
			factoryTask,
			capacity );
	}
}

ServiceHolder::~ServiceHolder()
{
	_ptr.reset();
}

NativeServerServiceInternal::NativeServerServiceInternal(
	System::String^ ip,
	int             port,
	SessionFactory^ sessionFactory,
	int             capacity )
: NativeServiceInternal( ENativeServiceType::Server, ip, port, sessionFactory, capacity )
{
}

NativeServerServiceInternal::~NativeServerServiceInternal()
{}

void NativeServerServiceInternal::RegisterListener( NativeListenerInternal^ nativeListener )
{
	auto serverService = std::dynamic_pointer_cast< RioServerService >( _holder->_ptr );
	serverService->RegisterListener( nativeListener->GetNativeListener() );
}

NativeClientServiceInternal::NativeClientServiceInternal(
	System::String^ ip,
	int             port,
	SessionFactory^ sessionFactory,
	int             capacity )
: NativeServiceInternal( ENativeServiceType::Client, ip, port, sessionFactory, capacity )
{
}

NativeClientServiceInternal::~NativeClientServiceInternal()
{}

/// <summary>
/// 접속한다
/// </summary>
void NativeClientServiceInternal::Connect( int sessionCount )
{
	auto session = _holder->_ptr->CreateSession();
	session->Connect();
}

