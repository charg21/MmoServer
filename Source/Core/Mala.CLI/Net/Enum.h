#pragma once

public enum class EDisconnectReason
{
	None,
	RemoteClosing,
	Kickout,
	ReceiveBufferFull,
};
