#pragma once

using namespace System::Runtime::InteropServices;
using namespace System;

public delegate void SendJobHandler( int data );
public delegate int RecvJobHandler( unsigned char* data, int len );
public delegate void ConnectedJobHandler( const std::wstring& ip, int port, bool success );
public delegate void DisconnectedJobHandler();

class CliRioSession;

class SessionHolder
{
public:
	std::shared_ptr< CliRioSession > _ptr;

public:
	SessionHolder();
	~SessionHolder();

	const CliRioSession* operator->() const
	{
		return _ptr.get();
	}

	CliRioSession* operator->()
	{
		return _ptr.get();
	}
};

public ref class NativeSessionInternal
{
public:
	NativeSessionInternal();
	virtual ~NativeSessionInternal();

	virtual void Clear();

	void Send( System::Span< unsigned char > sendBuffer );
	void Connect();

	/// <summary>
	/// C# 인터페이스
	/// </summary>
	virtual int OnRecv( System::ReadOnlySpan< unsigned char > buffer ) = 0;

	/// <summary>
	///
	/// </summary>
	virtual void OnSendCompleted( int len ) = 0;

	/// <summary>
	/// C++ 인터페이스
	/// </summary>
	int OnRecvNative( unsigned char* buffer, int len );

	/// <summary>
	///
	/// </summary>
	virtual void OnConnected( System::Net::EndPoint^, bool success ) = 0;

	/// <summary>
	///
	/// </summary>
	virtual void OnConnectedNative( const std::wstring& ip, int port, bool success );

	/// <summary>
	///
	/// </summary>
	virtual void OnDisconnected( System::Net::EndPoint^ ) = 0;

	/// <summary>
	///
	/// </summary>
	virtual void OnDisconnectedNative();

	/// <summary>
	///
	/// </summary>
	bool IsConnected();

public:
	SessionHolder* _holder;

private:
	[MarshalAsAttribute( UnmanagedType::FunctionPtr )]
	SendJobHandler^ _sendJobHandler;

	[MarshalAsAttribute( UnmanagedType::FunctionPtr )]
	RecvJobHandler^ _recvJobHandler;

	[MarshalAsAttribute( UnmanagedType::FunctionPtr )]
	ConnectedJobHandler^ _connectedJobHandler;

	[MarshalAsAttribute( UnmanagedType::FunctionPtr )]
	DisconnectedJobHandler^ _disconnectedJobHandler;
};
