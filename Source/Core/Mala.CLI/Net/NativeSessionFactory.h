#pragma once

using namespace System::Runtime::InteropServices;
using namespace System;

ref class NativeSessionInternal;

public delegate NativeSessionInternal^ SessionFactory();