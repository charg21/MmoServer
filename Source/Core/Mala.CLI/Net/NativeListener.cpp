#pragma once

#include "CliPch.h"
#include "NativeService.h"
#include "NativeSession.h"
#include "NativeListener.h"
#include <msclr/marshal.h>
#pragma managed(push, off)

#include "../Mala.Native/MalaPch.h"
#include "../Mala.Native/Net/RioSession.h"
#include "../Mala.Native/Net/RioService.h"
#include "../Mala.Native/Net/RioListener.h"
#include "../Mala.Native/Net/EndPoint.h"
#include "../Mala.Native/Core/CoreGlobal.h"
#include "../Mala.Native/Core/CoreTLS.h"
#include "../Mala.Native/Memory/Memory.h"

#pragma managed(pop)


NativeListenerInternal::NativeListenerInternal()
{
	_sessionFactory = new msclr::gcroot< SessionFactory^ >();
	_listenerHolder = new ListenerPtr();
}

NativeListenerInternal::NativeListenerInternal(
	System::String^ ip, int port, SessionFactory^ sessionFactoryPtr, int capacity )
{
	_sessionFactory = new msclr::gcroot< SessionFactory^ >();
	_listenerHolder = new ListenerPtr();

	Init( ip, port, sessionFactoryPtr, capacity );
}

/// <summary>
/// 매니지드 코드에서 로컬 람다가 사용 불가능하여 임시로 추가
/// </summary>
struct TempFactoryTask
{
	RioSessionPtr operator()()
	{
		auto s = _sf->operator->();

		return s()->_holder->_ptr;
	}

	msclr::gcroot< SessionFactory^ >* _sf;
};

bool NativeListenerInternal::Init(
	System::String^ ip,
	int             port,
	SessionFactory^ sessionFactory,
	int             capacity )
{
	( *_sessionFactory ) = sessionFactory;

	pin_ptr< const wchar_t > wch = PtrToStringChars( ip );

	TempFactoryTask ft{ ._sf = _sessionFactory };
	*_listenerHolder = MakeShared< RioListener >();
	( *_listenerHolder )->Initialize(
		EndPoint( wch, port ),
		capacity,
		std::move( ft ) );

	return true;
}

NativeListenerInternal::~NativeListenerInternal()
{
	_listenerHolder->reset();

	delete _sessionFactory;
	delete _listenerHolder;
}

ListenerPtr NativeListenerInternal::GetNativeListener()
{
	if ( _listenerHolder )
		return *_listenerHolder;

	return {};
}
