#pragma once

#include "NativeSessionFactory.h"

using namespace System::Runtime::InteropServices;
using namespace System;

namespace Mala::Net
{
class RioService;
class RioClientService;
class RioServerService;
};

using namespace Mala::Net;

ref class NativeListenerInternal;

//
//class NativeCore
//{
//public:
//	/// <summary>
//	/// RIO 서비스
//	/// </summary>
//	std::shared_ptr< RioService > _ptr;
//
//	/// <summary>
//	/// 세션 팩토리
//	/// </summary>
//	msclr::gcroot< SessionFactory^ > _sessionFactoryPtr;
//
//	/// <summary>
//	/// 서비스 타입
//	/// </summary>
//	ENativeServiceType _type;
//
//public:
//	/// 생성자
//	ServiceHolder(
//		ENativeServiceType type,
//		System::String^    ip,
//		int                port,
//		SessionFactory^    sessionFactoryPtr,
//		int                capacity );
//
//	/// 소멸자
//	~ServiceHolder();
//
//	const RioService* operator->() const
//	{
//		return _ptr.get();
//	}
//
//	RioService* operator->()
//	{
//		return _ptr.get();
//	}
//
//};

