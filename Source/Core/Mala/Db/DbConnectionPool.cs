﻿using System.Collections.Concurrent;
using Mala.Core;
using MySql.Data.MySqlClient;

namespace Mala.Db;

/// <summary>
/// Db 커넥션 풀
/// </summary>
public class DbConnectionPool : Singleton< DbConnectionPool >
{
    /// <summary>
    /// 커넥션 스택
    /// </summary>
    private ConcurrentStack< MySqlConnection > _connections = new();

    /// <summary>
    /// 스키마
    /// </summary>
    public string Schema { get; set; } = string.Empty;

    /// <summary>
    /// 유저
    /// </summary>
    public string User { get; set; } = string.Empty;

    /// <summary>
    /// 패스워드
    /// </summary>
    public string Password { get; set; } = string.Empty;

    private string _connectionString;

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public void Initialize( string schema, string user, string pw, int connectionCount )
    {
        Schema   = schema;
        User     = user;
        Password = pw;

        Initialize( $"Server=localhost;Database={ schema };Uid={ user };Pwd={ pw };", connectionCount );
    }

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public void Initialize( string connectionString, int connectionCount )
    {
        _connectionString = connectionString;

        for ( int n = 0; n < connectionCount; n += 1 )
        {
            var connection = new MySqlConnection( connectionString );
            connection.Open();

            Push( connection );
        }
    }

    public void Push( MySqlConnection connection )
    {
        _connections.Push( connection );
    }

    public MySqlConnection Pop()
    {
        if ( _connections.TryPop( out var connection ) )
        {
            return connection;
        }

        var newConnection = new MySqlConnection( _connectionString );
        newConnection.Open();

        return newConnection;
    }

}