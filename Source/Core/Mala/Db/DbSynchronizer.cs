using Mala.Core;
using Mala.Db.Model;
using MySql.Data.MySqlClient;

namespace Mala.Db;

/// <summary>
/// Db 동기화를 담당하는 객체
/// </summary>
public class DbSynchronizer : Singleton< DbSynchronizer >
{
	/// <summary>
	/// 스키마
	/// </summary>
	public string Schema { get; set; } = string.Empty;

	/// <summary>
	/// 유저
	/// </summary>
	public string User { get; set; } = string.Empty;

	/// <summary>
	/// 비밀번호
	/// </summary>
	public string Password { get; set; } = string.Empty;
	private string _connectionString = string.Empty;

    string tableName = $"select `TABLE_NAME` from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = { "" }";

    /// <summary>
    /// 원격 테이블 정보
    /// </summary>
    public Dictionary< string, TableInfo > RemoteTableInfoMap { get; set; } = new();

    /// <summary>
    /// 로컬 테이블 정보
    /// </summary>
    public Dictionary< string, TableInfo> LocalTableInfoMap { get; set; } = new(); 


	//	public Dictionary<string, TableMetadata> TableMetaDataDiff { get; set; } = new(); //<

	//	public void ForEachDiffTable( Action<TableMetadata> job )
	//	{
	//		foreach ( var (_, tableMetaData) in TableMetaDataDiff )
	//			job( tableMetaData );
	//	}

	//	public void Merge()
	//	{
	//		var updateTableSet = LocalTableMetaData.Where(
	//			nameAndLocalTable => RemoteTableMetaData.ContainsKey( nameAndLocalTable.Key ) );
	//		foreach ( var (name, localTable) in updateTableSet )
	//		{
	//			//if ( localTable == RemoteTableMetaData[ name ] )
	//			//    continue;

	//			localTable.State = TableState.Updated;
	//			TableMetaDataDiff.Add( name, localTable );
	//		}

	//		var newTableSet = LocalTableMetaData.Where(
	//			nameAndTable => !RemoteTableMetaData.ContainsKey( nameAndTable.Key ) );
	//		foreach ( var (name, newTable) in newTableSet )
	//		{
	//			newTable.State = TableState.Created;
	//			TableMetaDataDiff.Add( name, newTable );
	//		}

	//		var deleteTableSet = RemoteTableMetaData.Where(
	//			nameAndTable => !LocalTableMetaData.ContainsKey( nameAndTable.Key ) );
	//		foreach ( var (name, dropTable) in deleteTableSet )
	//		{
	//			dropTable.State = TableState.Deleted;
	//			TableMetaDataDiff.Add( name, dropTable );
	//		}
	//	}

	//	public void SyncDb()
	//	{
	//		var connection = DbConnectionPool.Instance.Pop();

	//		foreach ( var (_, tableMetaData) in TableMetaDataDiff )
	//		{
	//			var query = string.Empty;

	//			var queryBuilder = new QueryBuilder();
	//			switch ( tableMetaData.State )
	//			{
	//				case TableState.Created:
	//					query = queryBuilder.MakeCreateQuery( Schema, tableMetaData );
	//					break;

	//				case TableState.Deleted:
	//					if ( !UseDropTable )
	//						continue;

	//					query = queryBuilder.MakeDropQuery( Schema, tableMetaData );
	//					break;

	//				case TableState.None or TableState.Updated:
	//					continue;
	//			}

	//			if ( UseQueryConsoleLog )
	//				Console.WriteLine( query );

	//			using var createCommand = new MySqlCommand( query, connection );
	//			using var reader        = createCommand.ExecuteReader();
	//		}

	//		DbConnectionPool.Instance.Push( connection );
	//	}


	//	public bool LoadLocalDBMetaData( string dirPath )
	//	{
	//		if ( !Directory.Exists( dirPath ) )
	//		{
	//			Console.WriteLine( $"Not Found Directory... Path : {dirPath}" );
	//			return false;
	//		}

	//		var filePathList = Directory.GetFiles( dirPath );
	//		foreach ( var filePath in filePathList )
	//		{
	//			if ( !_ParseLocalDbMetaData( filePath ) )
	//				Console.WriteLine( $"Invalid File Path... : {filePath}" );
	//		}



	//		return true;
	//	}

	//	private bool _ParseLocalDbMetaData( string path )
	//	{
	//		if ( path.Contains( ".cs" ) )
	//		{
	//			string schema = File.ReadAllText( path );
	//			DbSchemaParser.ParseCode( schema );

	//			return true;
	//		}

	//		if ( !path.Contains( ".json" ) )
	//			return false;

	//		if ( !File.Exists( path ) )
	//		{
	//			Console.WriteLine( $"Cant Find File : {path}" );
	//			return false;
	//		}

	//		string json = File.ReadAllText( path );
	//		if ( string.IsNullOrEmpty( json ) )
	//		{
	//			Console.WriteLine( $"Empty File : {path}" );
	//			return false;
	//		}

	//		var parser = new DbJsonParser();
	//		TableMetadata tableMetadata = parser.ParseFrom( json );

	//		LocalTableMetaData.Add( tableMetadata.Name, tableMetadata );

	//		return true;
	//	}


	public void LoadRemoteDbInfo( string schema )
	{
		Schema = schema;

		var connection = DbConnectionPool.Instance.Pop();
		var allTableSelectQuery 
			= $"SELECT `TABLE_NAME` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{ schema }';";
		var dbCommand = new MySqlCommand( allTableSelectQuery, connection );

		using MySqlDataReader tableNameReader = dbCommand.ExecuteReader();

		int n = 0;
		while ( tableNameReader.Read() )
		{
			var tableName = tableNameReader[ "TABLE_NAME" ].ToString();
			Console.WriteLine( $"{ n++ }: { tableName }" );
			var tableInfo = new TableInfo( tableName );
			RemoteTableInfoMap.Add( tableName, tableInfo );

			_LoadAndParseRemoteFieldInfo( tableInfo );
			_LoadAndParseRemoteIndexInfo( tableInfo );
		}

		DbConnectionPool.Instance.Push( connection );
	}

	private void _LoadAndParseRemoteFieldInfo( TableInfo tableMetadata )
	{
		var connection           = DbConnectionPool.Instance.Pop();
		var allColumnSelectQuery = $"SHOW FULL COLUMNS FROM `{ tableMetadata.Name }`;";
		var cmd1                 = new MySqlCommand( allColumnSelectQuery, connection );

		using MySqlDataReader columnReader = cmd1.ExecuteReader();

		while ( columnReader.Read() )
		{
			var columnInfo = new FieldInfo()
			{
				Name    = columnReader[ 0 ].ToString()!,
				//DbType  = columnReader[1].ToString()!,
				////Nullable = reader[ 3 ].ToString() != string.Empty,
				//InIndex = columnReader[4].ToString() != string.Empty, // "PRI" or "MUL" or "UNI"
			};

			// tableMetadata.AddColumnInfo( columnInfo );
		}

		DbConnectionPool.Instance.Push( connection );
	}

    private void _LoadAndParseRemoteIndexInfo( TableInfo tableInfo )
    {
    	var connection          = DbConnectionPool.Instance.Pop();
    	var allIndexSelectQuery = $"show indexes from { tableInfo.Name }";
    	var cmd2                = new MySqlCommand( allIndexSelectQuery, connection );

    	using MySqlDataReader indexReader = cmd2.ExecuteReader();

    	while ( indexReader.Read() )
    	{
    		var indexName  = indexReader![ "Key_name"    ].ToString()!;
    		var columnName = indexReader![ "Column_name" ].ToString()!;

    		//if ( !tableInfo.IndexMetaDataDict.TryGetValue( indexName, out var indexMetaData ) )
    		//{
    		//	indexMetaData = new IndexInfo()
    		//	{
    		//		Name = indexName,
    		//	};
    		//	tableInfo.AddIndexMetadata( indexMetaData );
    		//}

    		//if ( tableInfo.ColumnMetadataDict.TryGetValue( columnName, out var columnMetaData ) )
    		//	indexMetaData.AddColumnMetaData( columnMetaData );

    	}

    	DbConnectionPool.Instance.Push( connection );
    }
}


///*
//// DB의 전체 테이블 조회
//SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DB명';

//// DB의 전체 테이블명 및 테이블 코멘트 조회
//SELECT TABLE_NAME, TABLE_COMMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'DB명' ORDER BY TABLE_NAME;

//// 특정 테이블의 모든 컬럼 정보 조회
//SHOW FULL COLUMNS FROM 테이블명;

//// 여러 테이블의 모든 컬럼 정보 조회
//SELECT
//TABLE_NAME AS 'table_name',
//ORDINAL_POSITION AS 'no',
//COLUMN_NAME AS 'field_name',
//COLUMN_COMMENT AS 'comment',
//COLUMN_TYPE AS 'type',
//COLUMN_KEY AS 'KEY',
//IS_NULLABLE AS 'NULL',
//EXTRA AS 'auto',
//COLUMN_DEFAULT 'default'
//FROM
//INFORMATION_SCHEMA.COLUMNS
//WHERE
//TABLE_SCHEMA = 'DB명'
//AND TABLE_NAME IN ('테이블명','테이블명','테이블명')
//ORDER BY
//TABLE_NAME, ORDINAL_POSITION
//;

//*/