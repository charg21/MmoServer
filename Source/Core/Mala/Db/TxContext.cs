﻿using MySql.Data.MySqlClient;
using Mala.Db.Model;
using Mala.Logging;
using System.Runtime.CompilerServices;
using Mala.Threading;
using MySqlX.XDevAPI.Common;
using System.Diagnostics;

namespace Mala.Db;

/// <summary>
/// 트랜잭션 결과
/// </summary>
public enum ETxResult
{
    /// <summary>
    /// 성공
    /// </summary>
    Ok,

    /// <summary>
    /// 로직 에러
    /// </summary>
    JobFail,

    /// <summary>
    /// 쿼리 에러
    /// </summary>
    QueryFail,

    /// <summary>
    /// DBJob Fail..
    /// </summary>
    DbJobFail,

}

/// <summary>
/// 일련의 트랜잭션 작업 문맥
/// </summary>
public partial class TxContext
{
    /// <summary>
    /// Tx 상태 열거형
    /// </summary>
    public enum ETxContextState
    {
        /// <summary>
        /// 최초 상태
        /// </summary>
        Created,

        /// <summary>
        /// 실행하기 위해 대기중( 예약된 상태 )
        /// </summary>
        WaitingToExecute,

        /// <summary>
        /// 실행중 로직 작업
        /// </summary>
        Executing,

        /// <summary>
        /// 실행중 Db 작업
        /// </summary>
        ExecutingDb,

        /// <summary>
        /// 실행중 Db 작업
        /// </summary>
        ExecutingDbCompleted,

        /// <summary>
        /// 실행중 동기화 작업
        /// </summary>
        ExecutingSync,

        /// <summary>
        /// 완료됨( 성공한 상태, 실패했다면 Failed 로감 )
        /// </summary>
        Completed,

        /// <summary>
        /// 작업 취소 상태, 요청으로 인해 취소
        /// </summary>
        Canceled,

        /// <summary>
        /// 작업중 실패
        /// </summary>
        Failed,
    }

    /// <summary>
    ///
    /// </summary>
    public List< KeyValuePair< EQueryType, IDbModel > > _dbModels = new();

    /// <summary>
    /// 작업 목록
    /// </summary>
    public List< Func< bool > >? _jobList;

    /// <summary>
    /// Db 작업 목록
    /// </summary>
    public List< Func< DbExecutionContext, EDbResult > >? _dbJobList;

    /// <summary>
    /// Tx 결과에 따른 작업 목록
    /// </summary>
    public List< Action< ETxResult > >? _endThenJobList;

    /// <summary>
    /// 결과가 성고시 호출될 작업 목록
    /// </summary>
    public List< Action >? _successThenJobList;

    /// <summary>
    /// Tx 완료시 호출되는 작업
    /// </summary>
    public Action< ETxResult >? _completionJob;

    /// <summary>
    /// Tx 소유자
    /// </summary>
    public ITxContextOwner Owner { get; set; }

    /// <summary>
    /// 트랜잭션 진행 상태
    /// </summary>
    public ETxContextState _state = ETxContextState.Created;

    /// <summary>
    /// 트랜잭션 결과 상태
    /// </summary>
    public ETxResult _result = ETxResult.Ok;

    /// <summary>
    /// 재사용을 위한 Pool
    /// </summary>
    static Pool< TxContext > Pool = new();

    /// <summary>
    /// 초기 상태로 되돌린다.
    /// </summary>
    public void Reset()
    {
        _dbModels?.Clear();
        _jobList?.Clear();
        _dbJobList?.Clear();
        _endThenJobList?.Clear();

        _completionJob = null;
        Owner = null;
        _state = ETxContextState.Created;
        _result = ETxResult.Ok;
    }

    public T Add< T >() where T : IDbModel, new()
    {
        var proxy = new T();

        _dbModels.Add( new( EQueryType.Insert, proxy ) );

        return proxy;
    }

    public T Update< T >( IDbModel origin ) where T : class, IDbModel, new()
    {
        /// 이미 갱신중인 DbModel이 있다면 그것을 반환, 아니라면 생성
        T proxy = FindOrDefault< T >( origin );
        return proxy;
    }

    private T FindOrDefault< T >( IDbModel origin ) where T : class, IDbModel, new()
    {
        foreach ( var ( _, value ) in _dbModels )
        {
            if ( value.Origin == origin )
            {
                var v = value as T;
                if ( v is null )
                {
                    Log.Critical( "Invalid DbModel" );
                    return null;
                }

                return v;
            }
        }

        T proxy = new T();
        proxy.Origin = origin;
        origin.CopyToOnlyKeys( proxy );
        _dbModels.Add( new( EQueryType.Update, proxy ) );
        return proxy;
    }


    public T Delete< T >( IDbModel origin ) where T : IDbModel, new()
    {
        var proxy    = new T();
        proxy.Origin = origin;

        _dbModels.Add( new( EQueryType.Delete, proxy ) );
        return proxy;
    }

    /// <summary>
    /// 실행한다.
    /// </summary>
    public void Do()
    {
        switch ( _state )
        {
            // op
            case ETxContextState.WaitingToExecute:
                _state = ETxContextState.Executing;
                Execute_Job();
                break;

            case ETxContextState.Executing:
                _state = ETxContextState.ExecutingDb;
                HandoverDbJob();
                break;

            case ETxContextState.ExecutingDbCompleted:
                _state = ETxContextState.ExecutingSync;
                Execute_Sync();
                break;

            case ETxContextState.Failed:  // // Failed인 경우
                Execute_Fialed();
                _state = ETxContextState.Completed;
                Execute_Completed();
                break;
            case ETxContextState.ExecutingSync:
                _state = ETxContextState.Completed;
                Execute_Completed();
                break;

            // nop
            case ETxContextState.ExecutingDb: break;
            case ETxContextState.Completed:   break;
            case ETxContextState.Created:     break;
            case ETxContextState.Canceled:    break;

            default:                          break;
        }
    }

    private void Execute_Fialed()
    {
#if !SHIPPING
        Log.Critical( $"TxContext Failed. File:{SourceFilePath}\nMember{MemberName}:{SourceLineNumber}" );
#endif
    }

    /// <summary>
    /// DB 실행기로 작업을 넘긴다.
    /// </summary>
    private void HandoverDbJob()
    {
        DbExectuor.Post( ctx =>
        {
            bool result = Execute_DbJob( ref ctx );
            if ( result )
            {
                _state = ETxContextState.ExecutingDbCompleted;
                _result = ETxResult.Ok;
            }
            else
            {
                _state = ETxContextState.Failed;
                _result = ETxResult.QueryFail;
            }

            /// Pc의 Tick을 돌떄와, DB작업이 완료시 두가지 이벤트에서 TxExecutor 갱신 했지만...
            /// DB Executor에서 Tx 실행 예약시, World Worker에서 사용되는 문맥들이 생성될 수 있음.
            //var txExecutor = Owner as ITxExecutor;
            //txExecutor?.ReserveTx();

            return result;
        } );
    }

    public List< T > Find< T >() where T : IDbModel, new()
    {
        return new();
    }

    public T FindFirst< T, T1 >( T1 t1 ) where T : IDbModel, new()
    {
        return new();
    }

    public T FindFirst< T, T1, T2 >( T1 t1, T2 t2 ) where T : IDbModel, new()
    {
        var dbModel = new T();
        dbModel.SetField( 0, t1.ToString() );

        return new();
    }

    public T FindFirst< T, T1, T2, T3 >( T1 t1, T2 t2, T3 t3 ) where T : DbModel, new()
    {
        return new();
    }

    public T GetOrDefault< T >() where T : DbModel, new()
    {
        //newDbModel.TableInfo.DbModelFactory.Invoke();

        return null;// new T();
    }

    public bool Execute_DbJob( ref DbExecutionContext ctx )
    {
        // In Db Thread
        using var tx = ctx.Connection.BeginTransaction();

        foreach ( var dbJob in _dbJobList )
        {
            EDbResult result = dbJob( ctx );
            if ( result != EDbResult.Success )
            {
                // Hanlding... DB Error
                return false;
            }
        }

        foreach ( var ( queryType, dbModel ) in _dbModels )
        {
            string query = BuildQuery( queryType, dbModel );
            Log.Debug( $"Query: {query}" );
            var cmd = new MySqlCommand( query, ctx.Connection );
            var affectedRows = cmd.ExecuteNonQuery();

            if ( affectedRows > 0 )
            {
                // Log.Debug( "Query executed successfully." );
            }
            else
            {
                Log.Critical( "No rows affected by the query." );
                _state = ETxContextState.Failed;
                return false;
            }
        }

        tx.Commit();

        return true;
    }

    private static string BuildQuery( EQueryType queryType, IDbModel dbModel )
    {
        if ( queryType == EQueryType.Select )
            return "not supported quyery type";

        return QueryBuilder.ToQuery( queryType, dbModel );
    }

    public bool Execute_Sync()
    {
        if ( _dbModels is null )
            return true;

        foreach ( var ( queryType, dbModel ) in _dbModels )
            dbModel.Sync( this, queryType );

        return true;
    }

    public void Execute_Completed()
    {
        if ( _endThenJobList is not null )
            foreach ( var endWithJob in _endThenJobList )
                endWithJob.Invoke( _result );

        if ( _successThenJobList is not null )
            foreach ( var endWithJob in _successThenJobList )
                endWithJob.Invoke();

        _completionJob?.Invoke( _result );
    }

    public bool Execute_Job()
    {
        if ( _jobList is null )
            return true;

        foreach ( var job in _jobList )
        {
            bool result = job();
            if ( !result )
                return false;
        }

        return true;
    }

    /// <summary>
    /// 미지원
    /// </summary>
    public T Add< T >( Func< T, bool > value )
    {
        var t = Add( value );

        return t;
    }

    public void Cancel()
    {
        _state = ETxContextState.Canceled;
    }

    /// <summary>
    ///  Tx를 획득한다.
    /// </summary>
#if SHIPPING
    public static TxContext New() => Pool.GetOrDefault();
#else
    public static TxContext New(
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0 )
    {
        var tx = Pool.GetOrDefault();

        tx.SourceFilePath   = sourceFilePath;
        tx.MemberName       = memberName;
        tx.SourceLineNumber = sourceLineNumber;

        return tx;
    }
#endif
    /// <summary>
    ///  Tx를 반환한다.( 필요하다면 Return에도 디버깅 소스 경로 남길 것 )
    /// </summary>
    public void Return()
    {
        Reset();
        Pool.Return( this );
    }

    public void ThenDbJob( Func< DbExecutionContext, EDbResult > dbJob )
    {
        ( _dbJobList ??= [] ).Add( dbJob );
    }

    /// <summary>
    /// 작업이 종료되고 실행될 작업을 추가함
    /// </summary>
    public void OnEndThen( Action< ETxResult > job )
    {
        ( _endThenJobList ??= [] ).Add( job );
    }

    /// <summary>
    /// 작업이 성공하고 실행될 작업 추가
    /// </summary>
    public void OnSuccessThen( Action job )
    {
        _successThenJobList ??= [];
        _successThenJobList.Add( job );
    }
}

#if !SHIPPING

/// <summary>
/// 일련의 트랜잭션 작업 문맥
/// </summary>
public partial class TxContext
{
    /// <summary>
    /// 멤버 이름 디버깅
    /// </summary>
    public string MemberName { get; set; } = string.Empty;

    /// <summary>
    /// 소스 파일 경로
    /// </summary>
    public string SourceFilePath { get; set; } = string.Empty;

    /// <summary>
    /// 소스 라인
    /// </summary>
    public int SourceLineNumber { get; set; } = 0;
}

#endif