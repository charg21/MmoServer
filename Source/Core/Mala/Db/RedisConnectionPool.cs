﻿using CloudStructures;
using System.Collections.Concurrent;
using Mala.Core;
using MySql.Data.MySqlClient;

namespace Mala.Db;

/// <summary>
/// 레디스 커넥션 풀
/// </summary>
public class RedisConnectionPool : Singleton< RedisConnectionPool >
{
    /// <summary>
    /// 커넥션 스택
    /// </summary>
    private ConcurrentStack< RedisConnection > _connections = new();

    /// <summary>
    /// 스키마
    /// </summary>
    public string Name => _config.Name;

    /// <summary>
    /// 호스트
    /// </summary>
    public string Host { get; set; } = string.Empty;

    /// <summary>
    /// 레디스 설정
    /// </summary>
    private RedisConfig _config;

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public void Initialize( string name, string host, int connectionCount )
    {
        Host = host;

        _config = new( name, Host );

        for ( int n = 0; n < connectionCount; n += 1 )
        {
            var connection = new RedisConnection( _config );

            _connections.Push( connection );
        }
    }

    public void Push( RedisConnection connection )
    {
        _connections.Push( connection );
    }

    public RedisConnection Pop()
    {
        if ( _connections.TryPop( out var connection ) )
        {
            return connection;
        }

        var newConnection = new RedisConnection( _config );

        return newConnection;
    }
}