﻿using Mala.Collection;
using Mala.Threading;
using System.Drawing.Text;

namespace Mala.Db;

/// <summary>
/// 트랜잭션 실행자
/// </summary>
public interface ITxExecutor
{
    /// <summary>W
    /// 작업 실행기
    /// </summary>
    Actor TxJobExecutor { get; }

    /// <summary>
    /// Tx를 실행한다.
    /// </summary>
    public void PostTx( TxContext txContext );
    public void PostTx( TxContext txContext, Action< ETxResult > completionJob );

    /// <summary>
    /// Tx를 예약한다.
    /// </summary>
    public void ReserveTx();
}

/// <summary>
/// 트랜잭션 소유자
/// </summary>
public interface ITxContextOwner
{
}

public class TxExecutor : ITxExecutor, ITxContextOwner
{
    /// <summary>
    /// 생성자
    /// </summary>
    public TxExecutor( Actor jobExecutor )
    {
        TxJobExecutor = jobExecutor;
    }

    /// <summary>
    /// MPSC Tx 큐
    /// </summary>
    WaitFreeQueue< TxContext > _txQueue { get; set; } = new();

    /// <summary>
    /// Owner의 JobExecutor
    /// </summary>
    public Actor TxJobExecutor { get; init; }

    /// <summary>
    ///
    /// </summary>
    public void PostTx( TxContext txContext )
    {
        if ( txContext is null )
            return;

        txContext.Owner = this;
        txContext._state = TxContext.ETxContextState.WaitingToExecute;

        _txQueue.Enqueue( txContext );

        ///// 실행 예약
        //TxJobExecutor?.Post( FlushTx );
    }

    public void PostTx( TxContext txContext, Action< ETxResult > completionJob )
    {
        txContext._completionJob = completionJob;
        PostTx( txContext );
    }

    /// <summary>
    ///
    /// </summary>
    public void FlushTx()
    {
        while ( _txQueue.TryPeek( out var txContext ) )
        {
            txContext.Do();

            if ( txContext._state == TxContext.ETxContextState.ExecutingDb )
            {
                //ReserveTx();
                //return;
            }
            else if ( txContext._state == TxContext.ETxContextState.Completed )
            {
                PopTx();
                continue;
            }
        }
    }

    /// <summary>
    ///
    /// </summary>
    public void ReserveTx() => TxJobExecutor?.Post( FlushTx );

    /// <summary>
    ///
    /// </summary>
    public void PopTx()
    {
        _txQueue.TryDequeue( out var completedTxContext );
        completedTxContext?.Return();
    }

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public void UpdateReserveTx() => TxJobExecutor?.Post( FlushTx );
}
