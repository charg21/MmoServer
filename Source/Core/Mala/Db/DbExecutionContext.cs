﻿using MySql.Data.MySqlClient;

namespace Mala.Db;
public struct DbExecutionContext : IDisposable
{
    public MySqlConnection Connection { get; set; }
    public bool _disposed = false;

    public DbExecutionContext()
    {
        Connection = DbConnectionPool.Instance.Pop();
    }

    public void Dispose()
    {
        if ( _disposed )
            return;

        DbConnectionPool.Instance.Push( Connection );

        _disposed = true;
    }
}