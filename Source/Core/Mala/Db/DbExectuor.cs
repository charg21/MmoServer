﻿using Mala.Logging;
using MySql.Data.MySqlClient;

namespace Mala.Db;

/// <summary>
/// Db 스레드 풀
/// </summary>
public static class DbExectuor
{
    public static void Post( Func< DbExecutionContext, bool > dbJob )
    {
        /// https://leafbird.github.io/devnote/2021/01/01/C-%EA%B3%A0%EC%84%B1%EB%8A%A5-%EC%84%9C%EB%B2%84-Thread-Local-Storage/
        /// 의도치 않는 TLS 변수의 복사를 막기위해 호출
        using var _ = ExecutionContext.SuppressFlow();

        ThreadPool.UnsafeQueueUserWorkItem( _ =>
        {
            using var dbExecutionContext = new DbExecutionContext();

            try
            {
                var result = dbJob( dbExecutionContext );
            }
            catch ( Exception ex )
            {
                Log.Critical( $"Occur! DbJob Error: { ex.Message }" );
            }
        },
        null );
    }
}
