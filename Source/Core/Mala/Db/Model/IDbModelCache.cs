﻿namespace Mala.Db;


public enum DbCachePolicy
{
    /// <summary>
    /// 없음
    /// </summary>
    None,

    /// <summary>
    ///영속성을 지님
    /// </summary>
    Persistent,

    /// <summary>
    ///
    /// </summary>
    Lru
}


/// <summary>
/// DbModel 캐시 인터페이스
/// </summary>
public partial interface IDbModelCache
{
    void Add( DbModel dbModel );
}

public class PeristentCache
{
    void Add( DbModel dbModel )
    {
    }
}

