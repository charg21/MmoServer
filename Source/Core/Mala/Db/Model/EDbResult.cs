﻿namespace Mala.Db;

/// <summary>
/// 
/// </summary>
public enum EDbResult
{
    Success,
    Fail,
    Empty,
    LogicError,
}
