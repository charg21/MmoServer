﻿using Org.BouncyCastle.Bcpg;
using System.Numerics;
using System.Text;

namespace Mala.Db.Model;

/// <summary>
/// Db 쿼리 생성기
/// </summary>
public partial class QueryBuilder
{
    /// <summary>
    /// TLS별 StringBuilder 인스턴스
    /// </summary>
    static ThreadLocal< StringBuilder > _stringBuilderPerThread = new( () => new( 1024 ) );
    static StringBuilder SharedBuilder
    {
        get
        {
            var builder = _stringBuilderPerThread.Value;
            builder!.Clear();
            return builder;
        }
    }

    public static string ToQuery( EQueryType queryType, IDbModel dbModel )
    {
        switch ( queryType )
        {
            case EQueryType.Insert: return ToInsertQuery( dbModel );
            case EQueryType.Update: return ToUpdateQuery( dbModel );
            case EQueryType.Delete: return ToDeleteQuery( dbModel );
            case EQueryType.Select: return ToSelectAllQuery( dbModel );
        }

        return string.Empty;
    }

    /// <summary>
    /// select count 쿼리를 만든다.
    /// </summary>
    public static string ToCountQuery( IDbModel iDbModel )
    {
        var sb        = SharedBuilder;
        var dbModel   = iDbModel.DbModel;
        var tableInfo = dbModel.TableInfo;

        sb.Append( $"SELECT COUNT(*) FROM `{tableInfo.Name}`" );
        AppendWhereCondition( sb, dbModel );

        return sb.ToString();
    }

    /// <summary>
    /// select 쿼리를 만든다.
    /// </summary>
    public static string ToSelectAllQuery( IDbModel iDbModel, DbQueryBuilderOption? option = null )
    {
        var builder   = SharedBuilder;
        var dbModel   = iDbModel.DbModel;
        var tableInfo = dbModel.TableInfo;

        builder.Append( "SELECT " );

        /// 전체 필드가 바인딩된 케이스에서 *로 쿼리 단축 처리
        /// 예외 사항으로 바인딩 안된 케이스에서도 일단 *로 처리( 예외로 처리할지 고려 )
        if ( dbModel.AllFieldBinding() || !dbModel.HasBindingField() && option?.validate == false )
        {
            builder.Append( '*' );
        }
        else
        {
            AppendFieldName( builder, dbModel );
            builder.Remove( builder.Length - 2, 2 );
        }

        builder.Append( $" FROM `{tableInfo.Name}`" );

        AppendWhereCondition( builder, dbModel );
        AppendOption( builder, option );

        return builder.ToString();
    }

    private static void AppendOption( StringBuilder builder, DbQueryBuilderOption? option = null )
    {
        if ( option is null )
            return;

        if ( !string.IsNullOrEmpty( option.Value.orderBy ) )
            builder.Append( $" ORDER BY {option.Value.orderBy}" );

        if ( option.Value.limit > 0 )
            builder.Append( $" LIMIT {option.Value.limit}" );
    }

    /// <summary>
    /// SELECT 쿼리로 변환한다.
    /// </summary>
    public static string ToSelectQuery( IDbModel iDbModel, DbQueryBuilderOption? option = null )
    {
        var model = iDbModel.DbModel;
        if ( model.GetValidFirstIndex() is null )
            return "";

        return ToSelectAllQuery( iDbModel, option );
    }

    public static string ToSelectManyQuery( IDbModel iDbModel, DbQueryBuilderOption? option = null )
    {
        var builder   = SharedBuilder;
        var dbModel   = iDbModel.DbModel;
        var tableInfo = dbModel.TableInfo;

        builder.Append( "SELECT " );

        /// 전체 필드가 바인딩된 케이스에서 *로 쿼리 단축 처리
        /// 예외 사항으로 바인딩 안된 케이스에서도 일단 *로 처리( 예외로 처리할지 고려 )
        if ( dbModel.AllFieldBinding() || !dbModel.HasBindingField() )
        {
            builder.Append( '*' );
        }
        else
        {
            AppendFieldName( builder, dbModel );

            if ( dbModel.Fields.Length > 0 )
                builder.Remove( builder.Length - 2, 2 );
        }

        builder.Append( $" FROM `{tableInfo.Name}`" );

        AppendWhereCondition( builder, dbModel );
        AppendOption( builder, option );

        return builder.ToString();
    }


    private static void AppendFieldName( StringBuilder builder, DbModel dbModel )
    {
        foreach ( var field in dbModel.Fields )
        {
            if ( !dbModel.IsBindingField( in field ) )
                continue;

            builder.Append( $"`{ field.FieldInfo.Name }`, " );
        }
    }

    /// <summary>
    /// update 쿼리를 만든다.
    /// </summary>
    public static string ToUpdateQuery( IDbModel iDbModel )
    {
        var sb        = SharedBuilder;
        var dbModel   = iDbModel.DbModel;
        var tableInfo = dbModel.TableInfo;

        if ( !dbModel.HasUpdateField() )
            return "INVALID UPDATE FIELD";

        sb.Append( "UPDATE `"     );
        sb.Append( tableInfo.Name );
        sb.Append( "` SET "       );

        foreach ( var field in dbModel.Fields )
        {
            if ( !dbModel.IsUpdateField( in field ) )
                continue;

            sb.Append( '`'                         );
            sb.Append( field.FieldInfo.Name        );
            sb.Append( "` = "                      );
            sb.Append( field.Var.ToDbValueString() );

            sb.Append( ", " );
        }

        if ( dbModel.Fields.Length > 0 )
            sb.Remove( sb.Length - 2, 2 );

        AppendWhereCondition( sb, dbModel );

        return sb.ToString();
    }

    /// <summary>
    /// insert 쿼리를 만든다.
    /// </summary>
    public static string ToInsertQuery( IDbModel dbModel )
    {
        var sb = SharedBuilder;

        var tableInfo = dbModel.DbModel.TableInfo;

        sb.Append( "INSERT INTO `" );
        sb.Append( tableInfo.Name  );
        sb.Append( "` VALUES ( "   );

        foreach ( var field in dbModel.DbModel.Fields )
        {
            sb.Append( field.Var.ToDbString() );
            sb.Append( ", " );
        }

        sb.Remove( sb.Length - 2, 2 );
        sb.Append( " ) " );

        return sb.ToString();
    }

    /// <summary>
    /// delete 쿼리를 만든다.
    /// </summary>
    public static string ToDeleteQuery( IDbModel dbModel )
    {
        var model = dbModel.DbModel;
        var indexInfo = model.GetValidFirstIndex();
        if ( indexInfo is null )
            return "";

        var builder   = SharedBuilder;
        var tableInfo = dbModel.DbModel.TableInfo;

        builder.Append( "DELETE FROM `" );
        builder.Append( tableInfo.Name  );
        builder.Append( '`'             );

        AppendWhereCondition( builder, dbModel );

        return builder.ToString();
    }

    public static bool AppendWhereCondition( StringBuilder sb, IDbModel dbModel )
    {
        var model = dbModel.DbModel;
        /// 인덱스에 해당하는 필드가 세팅되었는지 확인
        var indexInfo = model.GetValidFirstIndex();
        if ( indexInfo is null )
            return false;

        var curLength = sb.Length;

        sb.Append( " WHERE " );

        /// 인덱스에 해당하는 값들이 있다면 기준으로 where절을 생성
        foreach ( var field in indexInfo.FieldInfos )
        {
            sb.Append( '`'         );
            sb.Append( field.Name  );
            sb.Append( "` = "      );
            sb.Append( model.Fields[ field._no ].Var.ToDbString() );
            sb.Append( " AND "    );
        }

        // 키 세팅 X
        if ( curLength == sb.Length )
            return false;

        sb.Remove( sb.Length - 5, 5 ); // " AND " 제거
        return true;
    }
}