﻿using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;

namespace Mala.Db;

public partial interface IDbQueryBuildable
{ }



/// <summary>
/// DbSyncable 인터페이스
/// </summary>
public partial interface IDbModel
{
    /// <summary>
    ///
    /// </summary>
    public Actor JobExecutor { get; }

    /// <summary>
    /// 원본
    /// </summary>
    public IDbModel Origin { get; set; }

    /// <summary>
    ///
    /// </summary>
    public EDbResult Select( ref DbExecutionContext ctx );

    /// <summary>
    ///
    /// </summary>
    public bool Insert( ref DbExecutionContext ctx );

    /// <summary>
    /// 동기화 한다.
    /// </summary>
    public void Sync( TxContext txContext, EQueryType queryType );

    /// <summary>
    /// 대상에 복사한다.
    /// </summary>
    public void CopyToOnlyKeys( IDbModel model );

    /// <summary>
    ///
    /// </summary>
    void SetField( int no, string value );

    /// <summary>
    /// 필드를 획득한다.
    /// </summary>
    public ref Field GetField( int no );

    /// <summary>
    /// 모든 필드를 바인딩한다.
    /// </summary>
    public void BindAllField();

    /// <summary>
    ///
    /// </summary>
    public DbModel DbModel { get; }

}

