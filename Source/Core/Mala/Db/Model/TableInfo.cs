﻿using Mala.Core;
using Mala.Db;
using System.Drawing;
using System.Reflection.Metadata.Ecma335;

namespace Mala.Db.Model;

/// <summary>
/// 테이블 정보 구조체
/// </summary>
public partial class TableInfo : IEquatable< TableInfo >
{
    /// <summary>
    /// 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 필드 정보 목록
    /// </summary>
    public List< FieldInfo > FieldInfos { get; set; } = new();

    /// <summary>
    /// 인덱스 정보 목록
    /// </summary>
    public List< IndexInfo > IndexInfos { get; set; } = new();

    public int FieldCount => FieldInfos.Count;
    public int IndexCount => IndexInfos.Count;
    public ulong AllFiledBitMask;

    /// <summary>
    /// 생성자
    /// </summary>
    public TableInfo( string name )
    {
        Name = name;
    }

    /// <summary>
    /// 필드 정보를 추가한다.
    /// </summary>
    public FieldInfo AddFieldInfo(
        string       fieldName,
        int          fieldNumber,
        EVariantType type,
        bool         isInIndex )
    {
        var fieldInfo        = GetOrDefaultFieldInfo( fieldName );
        fieldInfo._no        = fieldNumber;
        fieldInfo.FieldType  = type;
        fieldInfo._isInIndex = isInIndex;

        AllFiledBitMask |= 1UL << fieldNumber;

        return fieldInfo;
    }

    /// <summary>
    /// 인덱스 정보를 추가한다.
    /// </summary>
    public IndexInfo AddIndexInfo( string indexName, FieldInfo fieldInfo )
    {
        var indexInfo = GetOrDefaultIdnexInfo( indexName );
        indexInfo.AddFieldInfo( fieldInfo );

        Rebuild( indexInfo );

        return indexInfo;
    }

    /// <summary>
    /// 인덱스 정보를 추가한다.
    /// </summary>
    public IndexInfo AddIndexInfo( string indexName, List< FieldInfo > fieldInfoList )
    {
        var indexInfo = GetOrDefaultIdnexInfo( indexName );
        indexInfo.AddFieldInfos( fieldInfoList );

        Rebuild( indexInfo );

        return indexInfo;
    }

    /// <summary>
    /// 인덱스 정보를 획득한다.
    /// </summary>
    public IndexInfo GetOrDefaultIdnexInfo( string indexName )
    {
        foreach ( var eachIndexInfo in IndexInfos )
        {
            if ( eachIndexInfo.Name == indexName )
                return eachIndexInfo;
        }

        var indexInfo = new IndexInfo( indexName );
        IndexInfos.Add( indexInfo );

        return indexInfo;
    }


    /// <summary>
    /// 필드 정보를 획득한다.
    /// </summary>
    public FieldInfo GetOrDefaultFieldInfo( string fieldName )
    {
        foreach ( var eachFieldInfo in FieldInfos )
        {
            if ( eachFieldInfo.Name == fieldName )
                return eachFieldInfo;
        }

        var fieldInfo = new FieldInfo() { Name = fieldName };
        FieldInfos.Add( fieldInfo );

        return fieldInfo;
    }

    /// <summary>
    /// 팩터리 메서드
    /// </summary>
    public Func< DbModel > DbModelFactory;

    /// <summary>
    ///
    /// </summary>
    bool IEquatable< TableInfo >.Equals( TableInfo? rhs )
    {
        if ( Name != rhs.Name )
            return false;

        return true;
    }

    void Rebuild( IndexInfo index )
    {
    }

}


/// <summary>
/// 테이블 정보 구조체
/// </summary>
public partial class TableInfo : IEquatable< TableInfo >
{
    public static List< ulong > bitMaskTable = [ 0 ];

    static TableInfo()
    {
        bitMaskTable.Capacity = 65;
        for ( int i = 0; i < 64; i++ )
            bitMaskTable.Add( ( 1UL << i ) );
    }
}