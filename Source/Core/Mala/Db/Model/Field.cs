﻿using Mala.Core;

namespace Mala.Db.Model;

/// <summary>
/// 필드
/// </summary>
public struct Field
{
    /// <summary>
    /// Field 값을 저장하는 변수
    /// </summary>
    public Variant Var;

    /// <summary>
    /// 필드 정보 구조체
    /// </summary>
    FieldInfo? _info = new();

    /// <summary>
    /// 생성자
    /// </summary>
    /// <param name="fieldInfo"></param>
    public Field( FieldInfo fieldInfo )
    {
        FieldInfo = fieldInfo;
        Var = new( fieldInfo.FieldType );
    }

    /// <summary>
    /// 문자열을 반환한다.
    /// </summary>
    /// <returns></returns>
    public override string ToString() 
        => Var.ToString();

    /// <summary>
    /// 필드 정보 구조체를 반환한다.
    /// </summary>
    public FieldInfo? FieldInfo { get => _info; set => _info = value; }

    /// <summary>
    /// 
    /// </summary>
    public string? Value => ToString();
}
