﻿using Mala.Core;

namespace Mala.Db.Model;

/// <summary>
/// Db용 Variant 확장 클래스 메소드
/// </summary>
public static class DbVariant
{
    public static string ToDbString( this Variant var )
    {
        if ( var.Type == EVariantType.DateTime || var.Type == EVariantType.Str )
            return $"\"{ var }\"";
        else
            return var.ToString();
    }

    public static string ToDbValueString( this Variant var )
    {
        if ( var.Type == EVariantType.Str || var.Type == EVariantType.Str )
            return $"\"{ var }\"";
        else
            return var.ToDbString();
    }
}
