﻿using Mala.Core;

namespace Mala.Db.Model;

/// <summary>
/// 필드 정보
/// </summary>
public class FieldInfo
{
    /// <summary>
    /// 테이블 정보
    /// </summary>
    public TableInfo _table;

    /// <summary>
    /// 타입
    /// </summary>
    public EVariantType FieldType { get; set; }

    /// <summary>
    /// 몇번째 필드인지 정보
    /// </summary>
    public int _no;

    /// <summary>
    /// 필드 명
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 인덱스에 포함 되는지 여부
    /// </summary>
    public bool _isInIndex;// => _isInPrimaryIndex || _isInSecondaryIndex;

    /// <summary>
    /// pk에 포함 되는지 여부
    /// </summary>
    public bool _isInPrimaryIndex;

    /// <summary>
    /// 인덱스 구성요소에 포함되는지 여부
    /// </summary>
    public bool _isInSecondaryIndex;

    /// <summary>
    /// default not_null is false
    /// </summary>
    public bool _nullable;

    /// <summary>
    /// 기본 값
    /// </summary>
    public Variant _defaultValue;
}
