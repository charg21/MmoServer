﻿using Windows.Devices.Input;

namespace Mala.Db.Model;

/// <summary>
/// 인덱스 정보
/// </summary>
public class IndexInfo
{
    /// <summary>
    /// 테이블 정보
    /// </summary>
    public TableInfo TableInfo { get; set; }

    /// <summary>
    /// 필드 정보 목록
    /// </summary>
    public List< FieldInfo > FieldInfos { get; set; } = new();

    /// <summary>
    /// 비트 마스크
    /// </summary>
    public ulong BitMask;

    /// <summary>
    /// 인덱스 이름
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// 생성자
    /// </summary>
    public IndexInfo()
    {
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public IndexInfo( string name )
    {
        Name = name;
    }

    public void AddFieldInfo( FieldInfo fieldInfo )
    {
        int bitMaskIndex = fieldInfo._no;
        FieldInfos.Add( fieldInfo );
        BitMask |= 1UL << bitMaskIndex;
    }

    public void AddFieldInfos( List< FieldInfo > fieldInfoList )
    {
        foreach ( var fieldInfo in fieldInfoList )
            AddFieldInfo( fieldInfo );
    }
}
