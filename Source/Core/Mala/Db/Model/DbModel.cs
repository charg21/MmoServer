﻿using Mala.Logging;
using Mala.Core;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;

namespace Mala.Db;

/// <summary>
/// Db 동기화 객체
/// </summary>
public partial class DbModel
{
    /// <summary>
    /// 필드정보
    /// </summary>
    public Field[] Fields { get; set; }

    /// <summary>
    /// 테이블 정보
    /// </summary>
    public TableInfo TableInfo { get; set; }

    /// <summary>
    /// 필드 갱신 플래그
    /// </summary>
    ulong _fieldUpdatedFlag = 0;

    /// <summary>
    /// 필드 바인딩 세팅 플래그
    /// </summary>
    ulong _fieldBinding = 0;

    /// <summary>
    /// 키 세팅 플래그
    /// </summary>
    ulong _fieldKeySetup = 0;

    /// <summary>
    /// 원본
    /// </summary>
    public IDbModel? _root;

    /// <summary>
    /// 테이블 정보
    /// </summary>
    public static TableInfo StaticTableInfo;

    /// <summary>
    /// 생성자
    /// </summary>
    public DbModel( TableInfo tableInfo )
    {
        Fields = new Field[ tableInfo.FieldInfos.Count ];

        TableInfo = tableInfo;
        foreach ( var fieldInfo in tableInfo.FieldInfos )
        {
            Fields[ fieldInfo._no ] = new Field( fieldInfo );
        }
    }

    /// <summary>
    /// 필드를 획득한다.
    /// </summary>
    public ref Field GetField( int no ) => ref Fields[ no ];

    /// <summary>
    /// 필드를 설정한다
    /// </summary>
    public void SetField( int no, string value ) => Fields[ no ].Var.Set( value );

    /// <summary>
    /// 필드를 설정한다
    /// </summary>
    public void SetKeyField( int no, string value )
    {
        Fields[ no ].Var.Set( value );
        SetKeyField( no );
    }

    /// <summary>
    /// 필드를 설정한다
    /// </summary>
    public void SetField( int no, object value )
    {
        //MySqlField v = value as MySqlByte;
        //Fields[ no ].Var.Set( value );
    }

    /// <summary>
    /// 각 필드를 순횐한다.
    /// </summary>
    public void ForEachField( Action< Field > job )
    {
        foreach ( var field in Fields )
        {
            job( field );
        }
    }

    /// <summary>
    /// 모든 필드를 바인딩한다.
    /// </summary>
    public void BindAllField()
    {
        _fieldBinding = TableInfo.AllFiledBitMask;
    }

    /// <summary>
    /// 모든 필드를 바인딩 처리한다.
    /// </summary>
    public void SetUpdateAllField()
    {
        _fieldBinding = TableInfo.AllFiledBitMask;
    }

    /// <summary>
    /// 업데이트 필드로 설정한다.
    /// </summary>
    protected void SetUpdateField( int fieldNo )
    {
        _fieldUpdatedFlag |= ( 1UL << fieldNo );
    }

    /// <summary>
    /// 바인드 필드로 설정한다.
    /// </summary>
    public void BindField( int fieldNo )
    {
        _fieldBinding |= ( 1UL << fieldNo );
    }

    /// <summary>
    /// 키 필드로 설정한다.
    /// </summary>
    protected void SetKeyField( int fieldNo )
    {
        _fieldKeySetup |= ( 1UL << fieldNo );
    }

    /// <summary>
    /// 키 및 바인드 필드로 설정한다.
    /// </summary>
    protected void SetUpdateAndSetupKeyField( int fieldNo )
    {
        _fieldUpdatedFlag |= ( 1UL << fieldNo );
        _fieldKeySetup    |= ( 1UL << fieldNo );
    }

    /// <summary>
    /// 갱신 정보를 초기화한다.
    /// </summary>
    public void ResetUpdateFlag()
    {
        _fieldUpdatedFlag = 0;
    }

    /// <summary>
    /// 필드 갱신 여부를 반환한다.
    /// </summary>
    public bool IsUpdateField( in Field field )
    {
        ulong mask = ( 1UL << field.FieldInfo._no );
        bool result = 1UL <= ( _fieldUpdatedFlag & mask );
        return result;
    }

    /// <summary>
    /// 바인딩 컬럼 여부 반환
    /// </summary>
    public bool IsBindingField( in Field field )
    {
        ulong mask = ( 1UL << field.FieldInfo._no );
        bool result = 1UL <= ( _fieldBinding & mask );
        return result;
    }

    /// <summary>
    /// 바인딩 컬럼 여부 반환
    /// </summary>
    public bool IsKeySetupField( in Field field )
    {
        ulong mask = ( 1UL << field.FieldInfo._no );
        bool result = 1UL <= ( _fieldKeySetup & mask );
        return result;
    }

    /// <summary>
    /// 업데이트 필드 보유 여부를 반환한다.
    /// </summary>
    public bool HasUpdateField() => _fieldUpdatedFlag > 0;

    /// <summary>
    /// 바인딩 필드 보유 여부를 반환한다.
    /// </summary>
    public bool HasBindingField() => _fieldBinding > 0;

    /// <summary>
    /// 키셋팅 필드 보유 여부를 반환한다.
    /// </summary>
    public bool HasKeySetupField() => _fieldKeySetup > 0;

    public bool AllFieldBinding() => ( _fieldBinding & TableInfo.AllFiledBitMask ) == TableInfo.AllFiledBitMask;

    /// <summary>
    /// 복사한다.
    /// </summary>
    public void CopyTo( DbModel target )
    {
        target._fieldBinding     = _fieldBinding;
        target._fieldUpdatedFlag = _fieldUpdatedFlag;
        target._fieldKeySetup    = _fieldKeySetup;

        for ( int n = 0; n < Fields.Length; n += 1 )
        {
            //ref var targetField = ref target.Fields[ n ];
            //ref var myField     = ref Fields[ n ];
            //targetField = myField;
            target.Fields[ n ] = Fields[ n ];
        }
    }

    /// <summary>
    /// 갱신한 필드만 복사한다.
    /// </summary>
    public void CopyToOnlyUpdateField( DbModel target )
    {
        CopyFlag( target );

        for ( int n = 0; n < Fields.Length; n += 1 )
        {
            if ( !IsUpdateField( in Fields[ n ] ) )
                continue;

            target.Fields[ n ] = Fields[ n ];
        }
    }
    private void CopyFlag( DbModel target )
    {
        target._fieldBinding     = _fieldBinding;
        target._fieldUpdatedFlag = _fieldUpdatedFlag;
        target._fieldKeySetup    = _fieldKeySetup;
    }
    public void CopyToOnlyKeys( IDbModel target )
    {
        target.DbModel.FieldKeySetup = _fieldKeySetup;
        for ( int n = 0; n < Fields.Length; n += 1 )
        {
            if ( !IsKeySetupField( in Fields[ n ] ) )
                continue;

            ref var field = ref target.GetField( n );
            field = Fields[ n ];
        }
    }
    public bool IsValidIndex( IndexInfo index )
    {
        return ( index.BitMask & _fieldKeySetup ) == index.BitMask;
    }

    public IndexInfo GetValidFirstIndex()
    {
        foreach ( var indexInfo in TableInfo.IndexInfos )
        {
            if ( IsValidIndex( indexInfo ) )
                return indexInfo;
        }

        return null;
    }
}

public enum EOrderBy
{
    None,
    Asc,
    Desc,
}

public struct DbQueryBuilderOption
{
    public bool validate = false;
    public int limit = 0;
    public string orderBy = string.Empty;

    public DbQueryBuilderOption()
    {
    }
}


/// <summary>
///
/// </summary>
public partial class DbModel
{
    /// <summary>
    /// 조회한다.
    /// </summary>
    public EDbResult Select( ref DbExecutionContext ctx )
    {
        var query = QueryBuilder.ToSelectQuery( this );
        var cmd = new MySqlCommand( query, ctx.Connection );
        using MySqlDataReader reader = cmd.ExecuteReader();

        Log.Critical( $"Query: {query}" );

        if ( reader.Read() )
        {
            ApplyDbResult( reader );

            return EDbResult.Success;
        }
        else
        {
            return EDbResult.Empty;
        }
    }

    /// <summary>
    /// 조회한다.
    /// </summary>
    public ( EDbResult, List< T > ) SelectAll< T >( ref DbExecutionContext ctx, DbQueryBuilderOption? option = null ) where T : IDbModel, new()
    {
        var query = QueryBuilder.ToSelectAllQuery( this, option );
        var cmd = new MySqlCommand( query, ctx.Connection );
        using MySqlDataReader reader = cmd.ExecuteReader();

        Log.Debug( $"Query: {query}" );

        if ( reader.Read() )
        {
            var list = new List< T >();
            do
            {
                var model = new T();
                model.DbModel.ApplyDbResult( reader );
                list.Add( model );
            } while ( reader.Read() );

            return ( EDbResult.Success, list );
        }
        else
        {
            return ( EDbResult.Empty, null );
        }
    }

    /// <summary>
    /// 조회한다.
    /// </summary>
    public List< T > SelectMany< T >( ref DbExecutionContext ctx ) where T : IDbModel, new()
    {
        var query = QueryBuilder.ToSelectQuery( this );
        var cmd = new MySqlCommand( query, ctx.Connection );

        using MySqlDataReader reader = cmd.ExecuteReader();

        Log.Debug( $"Query: { query }" );

        var selectList = new List< T >();

        while ( reader.Read() )
        {
            var t = new T();
            t.DbModel.ApplyDbResult( reader );
            selectList.Add( t );
        }

        return selectList;
    }


    public void Select2( MySqlConnection connection )
    {
        var query = QueryBuilder.ToSelectQuery( this );
        var cmd = new MySqlCommand( query, connection );
        using MySqlDataReader reader = cmd.ExecuteReader();

        // 쿼리 결과 읽기
        while ( reader.Read() )
        {
            foreach ( var fieldInfo in TableInfo.FieldInfos )
            {
                switch ( fieldInfo.FieldType )
                {
                    case EVariantType.Bool:
                        Fields[ fieldInfo._no ].Var.SetBool( reader.GetFieldValue< bool >( fieldInfo._no ) );
                        break;
                    case EVariantType.U8:
                        Fields[ fieldInfo._no ].Var.SetU8( reader.GetFieldValue< u8 >( fieldInfo._no ) );
                        break;
                    case EVariantType.U16:
                        Fields[ fieldInfo._no ].Var.SetU16( reader.GetFieldValue< u16 >( fieldInfo._no ) );
                        break;
                    case EVariantType.U32:
                        Fields[ fieldInfo._no ].Var.SetU32( reader.GetFieldValue< u32 >( fieldInfo._no ) );
                        break;
                    case EVariantType.U64:
                        Fields[ fieldInfo._no ].Var.SetU64( reader.GetFieldValue< ulong >( fieldInfo._no ) );
                        break;
                    case EVariantType.I8:
                        Fields[ fieldInfo._no ].Var.SetI8( reader.GetFieldValue< i8 >( fieldInfo._no ) );
                        break;
                    case EVariantType.I16:
                        Fields[ fieldInfo._no ].Var.SetI16( reader.GetFieldValue< i16 >( fieldInfo._no ) );
                        break;
                    case EVariantType.I32:
                        Fields[ fieldInfo._no ].Var.SetI32( reader.GetFieldValue< int >( fieldInfo._no ) );
                        break;
                    case EVariantType.I64:
                        Fields[ fieldInfo._no ].Var.SetI64( reader.GetFieldValue< i64 >( fieldInfo._no ) );
                        break;
                    case EVariantType.F32:
                        Fields[ fieldInfo._no ].Var.SetF32( reader.GetFieldValue< float >( fieldInfo._no ) );
                        break;
                    case EVariantType.F64:
                        Fields[ fieldInfo._no ].Var.SetF64( reader.GetFieldValue< double >( fieldInfo._no ) );
                        break;
                    case EVariantType.Str:
                        Fields[ fieldInfo._no ].Var.SetStr( reader.GetFieldValue< string >( fieldInfo._no ) );
                        break;
                    default:
                        break;
                        // SetField( fieldInfo._no, reader.GetInt16( fieldInfo._no ) );
                }
            }

        }
    }

    /// <summary>
    /// 조회한다.
    /// </summary>
    public async Task< bool > SelectAsync( MySqlConnection connection )
    {
        var query = QueryBuilder.ToSelectQuery( this );
        var cmd   = new MySqlCommand( query, connection );

        using var reader = await cmd.ExecuteReaderAsync() as MySqlDataReader;

        if ( reader.Read() )
            ApplyDbResult( reader );

        return true;
    }

    /// <summary>
    /// 조회한다.
    /// </summary>
    public async Task< List< T > > SelectManyAsync< T >() where T : IDbModel, new()
    {
        var conncetion = DbConnectionPool.Instance.Pop();

        var query = QueryBuilder.ToSelectQuery( this );
        var cmd = new MySqlCommand( query, conncetion );

        using var reader = await cmd.ExecuteReaderAsync() as MySqlDataReader;

        Log.Critical( $"Query: {query}" );

        var selectList = new List< T >();

        while ( reader.Read() )
        {
            var t = new T();
            t.DbModel.ApplyDbResult( reader );
            selectList.Add( t );
        }

        DbConnectionPool.Instance.Push( conncetion );

        return selectList;
    }


    /// <summary>
    /// Db결과를 반영한다
    /// </summary>
    private void ApplyDbResult( MySqlDataReader? reader )
    {
        // 쿼리 결과 읽기
        foreach ( var fieldInfo in TableInfo.FieldInfos )
        {
            if ( fieldInfo._isInIndex )
                SetKeyField( fieldInfo._no, reader[ fieldInfo._no ].ToString() );
            else
                SetField( fieldInfo._no, reader[ fieldInfo._no ].ToString() );
        }
    }

    /// <summary>
    /// 조회한다.
    /// </summary>
    public async Task< bool > SelectAsync()
    {
        var conncetion = DbConnectionPool.Instance.Pop();

        var result = await SelectAsync( conncetion );

        DbConnectionPool.Instance.Push( conncetion );

        return result;
    }


    /// <summary>
    /// 조회한다.
    /// </summary>
    public async Task< i64 > CountAsync( MySqlConnection connection )
    {
        var query = QueryBuilder.ToCountQuery( this );
        var cmd   = new MySqlCommand( query, connection );

        var affectedRows = (i64)( await cmd.ExecuteScalarAsync() );
        return affectedRows;
    }

    /// <summary>
    /// 조회한다.
    /// </summary>
    public async Task< i64 > CountAsync()
    {
        var conncetion = DbConnectionPool.Instance.Pop();

        var result = await CountAsync( conncetion );

        DbConnectionPool.Instance.Push( conncetion );

        return result;
    }

    public bool Insert( ref DbExecutionContext ctx )
    {
        var query = QueryBuilder.ToInsertQuery( this );
        var cmd   = new MySqlCommand( query, ctx.Connection );

        //Log.Critical( $"{ query }" );

        return cmd.ExecuteNonQuery() > 0;
    }

    public bool Insert( DbExecutionContext ctx )
    {
        var connection = DbConnectionPool.Instance.Pop();

        bool result = Insert( ref ctx );

        DbConnectionPool.Instance.Push( connection );

        return result;
    }

    public async Task< bool > InsertAsync()
    {
        var connection = DbConnectionPool.Instance.Pop();

        var query = QueryBuilder.ToInsertQuery( this );
        var cmd   = new MySqlCommand( query, connection );

        var affectedRows = await cmd.ExecuteNonQueryAsync();

        DbConnectionPool.Instance.Push( connection );

        return affectedRows > 0;
    }

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public bool Update( ref DbExecutionContext ctx )
    {
        var updateQuery = QueryBuilder.ToUpdateQuery( this );

        Log.Critical( updateQuery );

        var cmd = new MySqlCommand( updateQuery, ctx.Connection );

        return cmd.ExecuteNonQuery() > 0;
    }

}

/// <summary>
/// IDbModel 구현 인터페이스
/// </summary>
public partial class DbModel : IDbModel
{
    public IDbModelIndexKey IndexKey { get; }
    /// <summary>
    /// 작업 실행기
    /// </summary>
    public Actor JobExecutor { get; set; } = null;

    /// <summary>
    ///
    /// </summary>
    public ulong FieldKeySetup { get => _fieldKeySetup; set => _fieldKeySetup = value; }

    /// <summary>
    /// 원본
    /// </summary>
    IDbModel IDbModel.Origin { get => _root; set => _root = value; }

    /// <summary>
    ///
    /// </summary>
    DbModel IDbModel.DbModel => this;

    /// <summary>
    /// 동기화한다
    /// </summary>
    public virtual void Sync( TxContext tx, EQueryType queryType )
    {
        _root?.Sync( tx, queryType );
    }

    void IDbModel.CopyToOnlyKeys( IDbModel model )
    {
        _root?.CopyToOnlyKeys( model );
    }

}
