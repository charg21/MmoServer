﻿namespace Mala.Db;

/// <summary>
/// 쿼리 타입 열거형
/// </summary>
public enum EQueryType
{
    /// <summary>
    /// 삽입
    /// </summary>
    Insert,

    /// <summary>
    /// 갱신
    /// </summary>
    Update,

    /// <summary>
    /// 제거
    /// </summary>
    Delete,

    /// <summary>
    /// 조회
    /// </summary>
    Select
}
