﻿using Cysharp.Text;
using Microsoft.Extensions.Logging;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics;
using ZLogger;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Options;

namespace Mala.Logging;

/// <summary>
/// 로그 레벨
/// </summary>
public enum ELogLevel
{
    Debug    = 1 << 0,
    Info     = 1 << 1,
    Critical = 1 << 2,
    Error    = 1 << 3,

    All = Debug | Info | Critical | Error,
}

/// <summary>
/// 로그 레벨 확장 메소드
/// </summary>
public static class LogLevelEx
{
    /// <summary>
    /// logLevel 플래그 보유 여부를 반환한다.
    /// Enum.HasFlag( Enum )가 IL에서 박싱이 발생하여( Enum으로 캐스팅시 발생 ),
    /// 직접 비트 연산을 통해 비교
    /// </summary>
    public static bool HasFlagFast( this ELogLevel thisLogLevel, ELogLevel logLevel )
    {
        return ( (int)thisLogLevel & (int)logLevel ) > 0;
    }
}

/// <summary>
/// Ref https://leafbird.github.io/devnote/2020/12/26/C-%EA%B3%A0%EC%84%B1%EB%8A%A5-%EC%84%9C%EB%B2%84-FILE-LINE-%EB%8C%80%EC%B2%B4%EC%A0%9C/
/// </summary>
public class Log
{
    public readonly static ILogger< Log > Global = LogManager.GetLogger< Log >();

    static ELogLevel LogLevel { get; set; } = ELogLevel.All;

    public static long DebugLogCount => Interlocked.Exchange( ref _debugLogCount, 0 );
    public static long InfoLogCount => Interlocked.Exchange( ref _infoLogCount, 0 );
    public static long CriticalLogCount => Interlocked.Exchange( ref _criticalLogCount, 0 );
    public static long ErrorLogCount => Interlocked.Exchange( ref _errorLogCount, 0 );

    static long _debugLogCount;
    static long _infoLogCount;
    static long _criticalLogCount;
    static long _errorLogCount;

    public static void Debug(
        string message,
        [CallerMemberName] string memberName = "",
        [CallerLineNumber] int sourceLineNumber = 0 )
    {
        if ( !LogLevel.HasFlagFast( ELogLevel.Debug ) )
            return;

        Interlocked.Increment( ref _debugLogCount );

        Global.LogDebug( $"[{ memberName }:{ sourceLineNumber}] {message}" );
    }

    public static void Info(
        string message,
        [CallerMemberName] string memberName = "",
        [CallerLineNumber] int sourceLineNumber = 0 )
    {
        if ( !LogLevel.HasFlagFast( ELogLevel.Info ) )
            return;

        Interlocked.Increment( ref _infoLogCount );

        Global.LogInformation( $"[{memberName}:{sourceLineNumber}] {message}" );
    }

    public static void Critical(
        string message,
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0 )
    {
        if ( !LogLevel.HasFlagFast( ELogLevel.Critical ) )
            return;

        Interlocked.Increment( ref _criticalLogCount );

        Global.LogCritical( $"[{sourceFilePath}:{sourceLineNumber}] {message}" );
    }

    public static void Error(
        string message,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0 )
    {
        if ( !LogLevel.HasFlagFast( ELogLevel.Error ) )
            return;

        Interlocked.Increment( ref _errorLogCount );

        Console.WriteLine( $"[{sourceFilePath}:{memberName}:{sourceLineNumber}] {message}" );
        Process.GetCurrentProcess().Kill();
        throw new Exception( message );
    }
}

public static class LogManager
{
    static ILogger globalLogger;
    static ILoggerFactory loggerFactory;

    public static string LogFilePrefix = string.Empty;

    public static void SetLoggerFactory( ILoggerFactory loggerFactory, string categoryName )
    {
        LogManager.loggerFactory = loggerFactory;
        LogManager.globalLogger = loggerFactory.CreateLogger( categoryName );
    }

    public static void Initialize( string logFilePrefix )
    {
        LogFilePrefix = logFilePrefix;

        //GlobalLogger
        var loggerFactory = LoggerFactory.Create( builder =>
        {
            builder
                .ClearProviders()
                .SetMinimumLevel( LogLevel.Debug )
                .AddZLoggerConsole( options =>
                {
                    options.EnableStructuredLogging = false;
                    options.PrefixFormatter = ( writer, info ) =>
                            ZString.Utf8Format( writer, "[{0}][{1}]", info.LogLevel, info.Timestamp.DateTime.ToLocalTime() );
                } )
                .AddZLoggerRollingFile(
                    fileNameSelector: ( dt, x ) => $"Logs/{LogFilePrefix}{dt.ToLocalTime() :yyyy-MM-dd}_{x :000}.log",
                    timestampPattern: x => x.ToLocalTime().Date,
                    rollSizeKB: 1024 * 1024,
                    option =>
                    {
                        option.PrefixFormatter = ( writer, info ) =>
                               ZString.Utf8Format( writer, "[{0}][{1}]", info.LogLevel, info.Timestamp.DateTime.ToLocalTime() );
                    } );
        } );

        LogManager.SetLoggerFactory( loggerFactory, "Global" );
    }

    public static ILogger Logger => globalLogger;

    public static ILogger< T > GetLogger< T >() where T : class => loggerFactory.CreateLogger< T >();

    public static ILogger GetLogger( string categoryName ) => loggerFactory.CreateLogger( categoryName );
}