﻿using Mysqlx.Crud;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;

namespace Mala.Core;

public static class Exception
{
    public static void ThrowIfFailed( bool condition, string? paramName = null )
    {
        if ( !condition )
            Throw( paramName );
    }

    public static void ThrowIfNull( object value, string? paramName = null )
    {
        if ( value is null )
            Throw( paramName );
    }

    [DoesNotReturn]
    public static void Throw( string? paramName )
        => throw new System.Exception( paramName );

    public static void MustBeNull< T >( this T expected ) where T : class
    {
        if ( expected is not null )
            Throw( "" );
    }
    public static void MustBeNull< T >( this T expected, string msg ) where T : class
    {
        if ( expected is not null )
            Throw( msg );
    }

    public static void MustNotBeNull< T >( this T expected, string? msg ) where T : class
    {
        if ( expected is null )
            Throw( msg );
    }
}
