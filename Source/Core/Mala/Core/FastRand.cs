namespace Mala.Core;

/// <summary>
/// 단순한 연산으로 생성하는 빠른 난수 생성기
/// 고른 분포가 중요하지 않을때 사용
/// </summary>
public class FastRand
{
    public FastRand()
    {
        InitSeed();
    }

    [ThreadStatic]
    static long _seedPerThread;

    [ThreadStatic]
    static bool _seedInitFlagPerThread;

    public static int Next( int maxValue = int.MaxValue )
    {
        InitSeed();

        _seedPerThread = 214013 * _seedPerThread + 2531011;

        return (int)( _seedPerThread >> 16 ) & maxValue;
    }

    public static long NextInt64( long maxValue = long.MaxValue )
    {
        InitSeed();

        _seedPerThread = 214013 * _seedPerThread + 2531011;

        return ( _seedPerThread >> 16 ) & maxValue;
    }

    static void InitSeed()
    {
        if ( !_seedInitFlagPerThread )
            return;

        /// 최소 0이 아닌 값으로 초기화
        _seedPerThread = Random.Shared.NextInt64();
    }
}

