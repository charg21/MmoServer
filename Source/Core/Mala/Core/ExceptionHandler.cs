﻿using Mala.Logging;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection.Metadata;
using System.Runtime.CompilerServices;

namespace Mala.Core;

using Exception = System.Exception;

public static class ExceptionHandler
{
    /// <summary>
    ///
    /// </summary>
    private static volatile int handled = 0;

    public static void Initialize()
    {
        AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionHandler;
    }

    private static void UnhandledExceptionHandler( object sender, UnhandledExceptionEventArgs args )
    {
        HandleException( args.ExceptionObject as System.Exception );
    }

    private static void MakeDump( System.Exception e )
    {
        string dumpFileName = $"MiniDump_{ DateTime.Now.ToString( "yyyy_MM_d_HH_m_ss_fff" ) }.dmp";
        MiniDump.Write( dumpFileName );
    }

    private static void HandleException( System.Exception e )
    {
        if ( GuardExceptionHandling() )
            return;

        try
        {
            Log.Error( $"Exception Occur! [{ e.Message }][{ e.StackTrace }]" );

            if ( Debugger.IsAttached )
            {
                System.Diagnostics.Trace.WriteLine( e.ToString() );
                Debugger.Break();
            }

            if ( Thread.CurrentThread.Name is null )
            {
                Thread.CurrentThread.Name = "CrashThread";
            }

            MakeDump( e );
        }
        catch ( System.Exception localException )
        {
            Log.Error( localException.Message );
        }
        finally
        {
            // terminating app
            Environment.Exit( -2 );
        }
    }

    private static bool GuardExceptionHandling()
    {
        return Interlocked.Exchange( ref handled, 1 ) != 0;
    }
}
