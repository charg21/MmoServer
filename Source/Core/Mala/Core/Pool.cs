﻿using System.Collections.Concurrent;

/// <summary>
/// 기본적인 풀
/// </summary>
public class Pool< T > where T : class, new()
{
    ConcurrentBag< T > _poolCollection = new();

    public T? Get()
    {
        _poolCollection.TryTake( out var element );
        return element;
    }

    public T GetOrDefault()
    {
        if ( !_poolCollection.TryTake( out var element ) )
            element = new T();

        return element;
    }

    public void Return( T t )
    {
        _poolCollection.Add( t );
    }
}


/// <summary>
///
/// </summary>
public class QueuePool< T > where T : class, new()
{
    ConcurrentQueue< T > _poolCollection = new();

    public T Get()
    {
        if ( !_poolCollection.TryDequeue( out var element ) )
            element = new T();

        return element;
    }

    public T? TryGet()
    {
        if ( _poolCollection.TryDequeue( out var element ) )
            return element;

        return null;
    }


    public void Return( T t )
    {
        _poolCollection.Enqueue( t );
    }
}

