﻿using System.Runtime.InteropServices;

namespace Mala.Core;

public enum EVariantType
{
    Bool,
    I8,
    I16,
    I32,
    I64,
    U8,
    U16,
    U32,
    U64,
    F32,
    F64,
    DateTime,

    Str
}

/// <summary>
/// 8byte + string 크기
/// </summary>
[StructLayout( LayoutKind.Explicit )]
public struct VariantInternal
{
    [FieldOffset( 0 )]
    public bool _bool;

    [FieldOffset( 0 )]
    public i8 _i8;

    [FieldOffset( 0 )]
    public i16 _i16;

    [FieldOffset( 0 )]
    public int _i32;

    [FieldOffset( 0 )]
    public i64 _i64;

    [FieldOffset( 0 )]
    public u8 _u8;

    [FieldOffset( 0 )]
    public u16 _u16;

    [FieldOffset( 0 )]
    public u32 _u32;

    [FieldOffset( 0 )]
    public u64 _u64;

    [FieldOffset( 0 )]
    public f32 _f32;

    [FieldOffset( 0 )]
    public f64 _f64;

    [FieldOffset( 0 )]
    public DateTime _dateTime;

    [FieldOffset( 8 )]
    public string _str = string.Empty;

    public VariantInternal()
    {
    }
}

public struct Variant
{
    VariantInternal _var = new();
    public EVariantType Type = EVariantType.I32;

    public Variant( EVariantType variantType )
    {
        Type = variantType;
    }

    public i8 GetI8()
    {
        return _var._i8;
    }

    public void SetI8( i8 v )
    {
        _var._i8 = v;
    }

    public i16 GetI16()
    {
        return _var._i16;
    }

    public void SetI16( i16 v )
    {
        _var._i16 = v;
    }

    public int GetI32()
    {
        return _var._i32;
    }

    public void SetI32( int v )
    {
        _var._i32 = v;
    }

    public i64 GetI64()
    {
        return _var._i64;
    }

    public void SetI64( i64 v )
    {
        _var._i64 = v;
    }

    public u8 GetU8()
    {
        return _var._u8;
    }

    public void SetU8( u8 v )
    {
        _var._u8 = v;
    }

    public u16 GetU16()
    {
        return _var._u16;
    }

    public void SetU16( u16 v )
    {
        _var._u16 = v;
    }

    public u32 GetU32()
    {
        return _var._u32;
    }

    public void SetU32( u32 v )
    {
        _var._u32 = v;
    }

    public u64 GetU64()
    {
        return _var._u64;
    }

    public void SetU64( u64 v )
    {
        _var._u64 = v;
    }

    public f32 GetF32()
    {
        return _var._f32;
    }

    public void SetF32( f32 v )
    {
        _var._f32 = v;
    }

    public f64 GetF64()
    {
        return _var._f64;
    }

    public void SetF64( f64 v )
    {
        _var._f64 = v;
    }

    public bool GetBool()
    {
        return _var._bool;
    }

    public void SetBool( bool v )
    {
        _var._bool = v;
    }

    public DateTime GetDateTime()
    {
        return _var._dateTime;
    }

    public void SetDateTime( DateTime v )
    {
        _var._dateTime = v;
    }

    public string GetStr()
    {
        return _var._str;
    }

    public void SetStr( string v )
    {
        _var._str = v;
    }

    public override readonly string ToString()
    {
        return Type switch
        {
            EVariantType.Bool => _var._bool ? "true" : "false",
            EVariantType.I8   => _var._i8.ToString(),
            EVariantType.I16  => _var._i16.ToString(),
            EVariantType.I32  => _var._i32.ToString(),
            EVariantType.I64  => _var._i64.ToString(),
            EVariantType.U8   => _var._u8.ToString(),
            EVariantType.U16  => _var._u16.ToString(),
            EVariantType.U32  => _var._u32.ToString(),
            EVariantType.U64  => _var._u64.ToString(),
            EVariantType.F32  => _var._f32.ToString(),
            EVariantType.F64  => _var._f64.ToString(),
            EVariantType.DateTime => _var._dateTime.ToString( "yyyy-MM-dd HH:mm:ss" ),
            EVariantType.Str  => _var._str,

            _ => string.Empty,
        };
    }

    public void Set( string value )
    {
        switch ( Type )
        {
            case EVariantType.Bool: bool.TryParse( value, out _var._bool ); break;
            case EVariantType.I8:   i8  .TryParse( value, out _var._i8  ); break;
            case EVariantType.I16:  i16 .TryParse( value, out _var._i16 ); break;
            case EVariantType.I32:  int .TryParse( value, out _var._i32 ); break;
            case EVariantType.I64:  i64 .TryParse( value, out _var._i64 ); break;
            case EVariantType.U8:   u8  .TryParse( value, out _var._u8  ); break;
            case EVariantType.U16:  u16 .TryParse( value, out _var._u16 ); break;
            case EVariantType.U32:  u32 .TryParse( value, out _var._u32 ); break;
            case EVariantType.U64:  u64 .TryParse( value, out _var._u64 ); break;

            case EVariantType.F32: f32.TryParse( value, out _var._f32 ); break;
            case EVariantType.F64: f64.TryParse( value, out _var._f64 ); break;

            case EVariantType.DateTime: DateTime.TryParse( value, out _var._dateTime ); break;

            case EVariantType.Str: _var._str = value; break;
        };
    }
}
