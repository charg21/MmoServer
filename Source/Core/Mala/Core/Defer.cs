﻿namespace Mala.Core;

public ref struct Defer : IDisposable
{
    Action _job;

    public Defer( Action job )
    {
        _job = job;
    }

    public void Dispose()
    {
        _job();
    }
}
