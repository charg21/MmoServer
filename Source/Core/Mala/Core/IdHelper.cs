﻿using Mala.Core;
using Mala.Math;
using Mala.Threading;
using MySqlX.XDevAPI.Relational;
using System;

/// <summary>
/// ID
/// YearOffset(3) Month(4) Day(5)
/// Hour(5) Minute(6) Second(6) Millisec(10)
/// ThreadId(6) 46비트 + 난수 or Sequence 19비트
/// ref: https://github.com/sator-imaging/Half-Ulid + ThreadId 개념 추가
/// </summary>
public static class IdHelper
{
    const long YearMax     = 1 << 3;  // 2 ^ 3  = 8   ( 8년 )
    const long MonthMax    = 1 << 4;  // 2 ^ 4  = 16  ( 16개월 )
    const long DayMax      = 1 << 5;  // 2 ^ 5  = 32  ( 32일 )

    const long HourMax     = 1 << 5;  // 2 ^ 5  = 32  ( 32시간 )
    const long MinuteMax   = 1 << 6;  // 2 ^ 6  = 64  ( 64분 )
    const long SecondMax   = 1 << 6;  // 2 ^ 6  = 64  ( 64초 )
    const long MillisecMax = 1 << 9;  // 2 ^ 9  = 512 ( 1024 / 2 = 512 밀리초 )

    const long ThreadIdMax = 1 << 7;  // 2 ^ 7  = 128 ( 128 스레드 )
    const long IdMax       = 1 << 19; // 2 ^ 19 = 524288 ( 랜덤 or 시퀀스 )

    const int YearOrigin = 2024;

    /// <summary>
    /// 쓰레드 로컬 ID 발급기
    /// </summary>
    [ThreadStatic]
    static long _idIssuer = 0;

    /// <summary>
    /// 스레드별 고유 식별자
    /// </summary>
    [ThreadStatic]
    static int _threadId = 0;
    static int _threadIdIssuer = 0;

    /// <summary>
    /// 쓰레드 로컬 마지막 리셋 틱
    /// </summary>
    [ThreadStatic]
    static long _lastResetTickWithoutMsPerThread;

    /// <summary>
    /// 쓰레드 로컬 ID 중복 체크 필터
    /// </summary>
    [ThreadStatic]
    static HashSet< uint > _randomIdFilterPerThread;

    /// <summary>
    /// 다음 원소를 반환합니다.
    /// </summary>
    public static ulong Next()
    {
        long id = NextBase();
        id |= ( _idIssuer++ ) % IdMax;

        return (ulong)id;
    }

    ///// <summary>
    ///// 다음 원소를 반환합니다.
    ///// </summary>
    //public static long NextRand( long threadId = 0 )
    //{
    //    _randomIdFilterPerThread ??= new HashSet< uint >( 1024 );

    //    long id = NextBase( threadId );

    //    uint randomId;
    //    do
    //    {
    //        randomId = (uint)( FastRand.Next() % IdMax );
    //    } while ( !_randomIdFilterPerThread.Add( randomId ) );

    //    id |= (uint)( randomId );

    //    return id;
    //}

    /// <summary>
    ///
    /// </summary>
    private static void ResetIfNeed( DateTime now )
    {
        //var tickWithoutSec = now.Ticks % 1000;
        //if ( _lastResetTickWithoutMsPerThread != tickWithoutSec )
        //{
        //    _lastResetTickWithoutMsPerThread = tickWithoutSec;
        //    _idIssuer = 0;
        //    _randomIdFilterPerThread?.Clear();
        //}
    }

    /// <summary>
    /// 공용으로 사용되는
    /// </summary>
    private static long NextBase()
    {
        var now = DateTime.UtcNow;
        ResetIfNeed( now );

        long id = (long)( ( now.Year - YearOrigin ) % YearMax ) << 61; // 상위 3
        id |= (long)now.Month                 << 57; // 4
        id |= (long)now.Day                   << 52; // 5
        id |= (long)now.Hour                  << 47; // 5
        id |= (long)now.Minute                << 41; // 6
        id |= (long)now.Second                << 35; // 6
        id |= (long)( now.Ticks % 1000 )      << 29; // 10
        id |= ( GetThreadId() % ThreadIdMax ) << 19; // 7

        return id;
    }

    private static long GetThreadId()
    {
        if ( _threadId == 0 )
            _threadId = Interlocked.Increment( ref _threadIdIssuer );

        return _threadId;
    }
}
