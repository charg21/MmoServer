﻿using Mala.Logging;
using System.Diagnostics;
using System.Management;

namespace Mala.Core;

public class PerfHelper
{
    private static PerformanceCounter _cpuProcessorTime;
    private static PerformanceCounter _cpuPrivilegedTime;
    private static PerformanceCounter _cpuInterruptTime;
    private static PerformanceCounter _cpuDPCTime;
    private static PerformanceCounter _systemCallCounter;
    private static PerformanceCounter _memoryUsageCounter;
    private static PerformanceCounter _contextSwitchesCount;
    private static PerformanceCounter _workingSet;
    private static PerformanceCounter _privateMemory;
    private static PerformanceCounter _cacheFaults;
    private static PerformanceCounter _receivedBytes;
    private static PerformanceCounter _sentBytes;
    private static PerformanceCounter _receivedPackets;
    private static PerformanceCounter _sentPackets;
    private static PerformanceCounter _poolPagedBytesCounter;
    private static PerformanceCounter _nonPagedBytesCounter;

    //private static PerformanceCounter _cpuProcessorUtility;
    //private static PerformanceCounter _memAvailable;
    //private static PerformanceCounter _memCommited;
    //private static PerformanceCounter _memCommitLimit;
    //private static PerformanceCounter _memCommitedPerc;
    //private static PerformanceCounter _cachedBytesCounter;
    //private static PerformanceCounter _pageFileUseCounter;
    //private static PerformanceCounter _processorQueueLengh;
    //private static PerformanceCounter _diskQueueLengh;
    //private static PerformanceCounter _diskRead;
    //private static PerformanceCounter _diskWrite;
    //private static PerformanceCounter _diskAverageTimeRead;
    //private static PerformanceCounter _diskAverageTimeWrite;
    //private static PerformanceCounter _diskTime;

    static PerfHelper()
    {
        try
        {
            var processName = ProcessName;

            _cpuProcessorTime = new( "Processor", "% Processor Time", "_Total", true );
            _cpuPrivilegedTime = new( "Processor", "% Privileged Time", "_Total" );
            _cpuInterruptTime = new( "Processor", "% Interrupt Time", "_Total" );
            _cpuDPCTime = new( "Processor", "% DPC Time", "_Total" );
            _systemCallCounter = new( "System", "System Calls/sec", null );
            _memoryUsageCounter = new( "Memory", "Available MBytes" );
            _contextSwitchesCount = new( "System", "Context Switches/sec", null );
            _workingSet = new( "Process", "Working Set", processName );
            _privateMemory = new( "Process", "Private Bytes", processName );
            _cacheFaults = new( "Memory", "Cache Faults/sec" );
            _poolPagedBytesCounter = new( "Memory", "Pool Paged Bytes", null );
            _nonPagedBytesCounter = new( "Memory", "Pool Nonpaged Bytes", null );

            var nicName = NicName;

            _receivedBytes = new( "Network Interface", "Bytes Received/sec", nicName );
            _sentBytes = new( "Network Interface", "Bytes Sent/sec", nicName );
            _receivedPackets = new( "Network Interface", "Packets Received/sec", nicName );
            _sentPackets = new( "Network Interface", "Packets Sent/sec", nicName );

            _cpuProcessorTime.NextValue();
            _cpuPrivilegedTime.NextValue();
            _cpuInterruptTime.NextValue();
            _cpuDPCTime.NextValue();
            _systemCallCounter.NextValue();
            _memoryUsageCounter.NextValue();
            _contextSwitchesCount.NextValue();
            _workingSet.NextValue();
            _privateMemory.NextValue();
            _cacheFaults.NextValue();
            _poolPagedBytesCounter.NextValue();
            _nonPagedBytesCounter.NextValue();

            //_cpuProcessorUtility = new("Processor Information", "% Processor Utility", "_Total");
            //_memAvailable = new("Memory", "Available MBytes", null);
            //_memCommited = new("Memory", "Committed Bytes", null);
            //_memCommitLimit = new("Memory", "Commit Limit", null);
            //_memCommitedPerc = new("Memory", "% Committed Bytes In Use", null);
            //_cachedBytesCounter = new("Memory", "Cache Bytes", null);
            //_pageFileUseCounter = new("Paging File", "% Usage", "_Total");
            //_processorQueueLengh = new("System", "Processor Queue Length", null);
            //_diskQueueLengh = new("PhysicalDisk", "Avg. Disk Queue Length", "_Total");
            //_diskRead = new("PhysicalDisk", "Disk Read Bytes/sec", "_Total");
            //_diskWrite = new("PhysicalDisk", "Disk Write Bytes/sec", "_Total");
            //_diskAverageTimeRead = new("PhysicalDisk", "Avg. Disk sec/Read", "_Total");
            //_diskAverageTimeWrite = new("PhysicalDisk", "Avg. Disk sec/Write", "_Total");
            //_diskTime = new("PhysicalDisk", "% Disk Time", "_Total");

            _receivedBytes.NextValue();
            _sentBytes.NextValue();
            _receivedPackets.NextValue();
            _sentPackets.NextValue();
        }
        catch ( System.Exception e )
        {
            Log.Error( e.Message );
        }
    }

    public static string NicName
    {
        get
        {
            var category = new PerformanceCounterCategory( "Network Interface" );
            var instanceNames = category.GetInstanceNames();
            foreach ( var name in instanceNames )
            {
                if ( name.Contains( "Ethernet" ) )
                    return name;
            }

            return string.Empty;
        }
    }

    public static int TotalRAMSize
    {
        get
        {
            var mc = new ManagementClass( "Win32_ComputerSystem" );
            ManagementObjectCollection moc = mc.GetInstances();
            int size = 0;

            foreach ( ManagementObject item in moc )
            {
                size += (int)( item.Properties[ "TotalPhysicalMemory" ].Value ) / ( 1024 * 1024 * 1024 );
            }

            return size;
        }
    }

    public static string ProcessName => Process.GetCurrentProcess().ProcessName;

    public static float CpuUsage => MathF.Round( _cpuProcessorTime.NextValue(), 2 );

    public static float SytemCallPerSec => _systemCallCounter.NextValue();

    public static float MemoryUsage => _memoryUsageCounter.NextValue();

    public static float ContextSwitchesCount => _contextSwitchesCount.NextValue();

    public static float WorkingSet => _workingSet.NextValue();

    public static float PrivateMemory => _privateMemory.NextValue();

    public static float CacheFaults => _cacheFaults.NextValue();

    public static float ReceivedBytes => _receivedBytes.NextValue();

    public static float SentBytes => _sentBytes.NextValue();

    public static float ReceivedPackets => _receivedPackets.NextValue();

    public static float SentPackets => _sentPackets.NextValue();

    public static float CpuPrivilegedTime => _cpuPrivilegedTime.NextValue();

    public static float CpuInterruptTime => _cpuInterruptTime.NextValue();

    public static float CpuDPCTime => _cpuDPCTime.NextValue();
}


