namespace Mala.Core;
public static class Crash
{
    /// <summary>
    /// 크래시를 발생 시킴
    /// </summary>
    public static void Do()
    {
        unsafe
        {
            /// 고기 먹고 카페 가고 싶다
            *(ulong*)( null ) = 0xBEEF_BEEF_CAFE_CAFE;
        }
    }
}
