﻿using Mala.Collection;

namespace Mala.Threading;

/// <summary>
/// 작업 큐
/// </summary>
public class JobQueue
{
    volatile JobEntry _tail;
    CacheLinePadding32 _tailPadding;

    JobEntry _head;

    JobEntry _stub;

    /// <summary>
    /// 생성자
    /// </summary>
    public JobQueue()
    {
        _stub = new JobEntry();
        _head = _stub;
        _tail = _stub;

        _tailPadding = new();
    }

    /// <summary>
    ///
    /// </summary>
    public void Push( JobEntry job )
    {
        var prevEntry = Interlocked.Exchange( ref _tail, job );

        prevEntry._next = job;
    }

    /// <summary>
    ///
    /// </summary>
    public JobEntry? Pop()
    {
        JobEntry head = _head;
        JobEntry next = head._next;

        if ( head == _stub )
        {
            if ( next is null )
                return null;

            // first pop
            _head = next;
            head  = next;
            next  = next._next;
        }

        if ( next is not null )
        {
            _head = next;

            /// 참조 제거
            head._next = null;

            return head;
        }

        JobEntry tail = _tail;
        if ( tail != head )
            return null;

        // last pop
        _stub._next = null;

        var prevEntry = Interlocked.Exchange( ref _tail, _stub );
        prevEntry._next = _stub;

        next = head._next;
        if ( next is not null )
        {
            _head = next;

            /// 참조 제거
            head._next = null;

            return head;
        }

        return null;
    }

    /// <summary>
    /// 작업 큐를 비운다.
    /// </summary>
    public void Flush()
    {
        for ( ;; )
        {
            var job = Pop();
            if ( job is null )
                break;

            job.Execute();
            job.Return();
        }
    }

}
