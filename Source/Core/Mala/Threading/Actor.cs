﻿namespace Mala.Threading;

/// <summary>
/// 작업 실행기
/// </summary>
public partial class Actor : IExecutor
{
    public const int GlobalExecutorQueueCapacity = 19;

    /// <summary>
    /// 글로벌 작업 실행기( Fixed-Size, Wait-Free Queue )
    /// </summary>
    static public GlobalQueue Global;

    /// <summary>
    /// 작업 큐
    /// </summary>
    JobQueue _jobQueue = new();

    /// <summary>
    /// 남은 작업 수
    /// </summary>
    i64 _remainJobCount = 0;

#if !NATIVE
    /// <summary>
    /// 작업 중 여부
    /// </summary>
    i64 _isExecuting = 0;
#endif

    public static void Initialize( int threadPoolCount = 32 )
    {
        Global = new( GlobalExecutorQueueCapacity, threadPoolCount );
    }

    /// <summary>
    ///
    /// </summary>
    public void Execute()
    {
#if !NATIVE
        var isExecuting = Interlocked.Exchange( ref _isExecuting, 1 );
        if ( isExecuting == 1 )
            return;

        if ( _remainJobCount == 0 )
        {
            _isExecuting = 0;
            return;
        }
#endif
        int executeJobCount = 0;
        /// 메서드로 빼지않은 이유는, StackOverflow가 발생함
        for ( ;; )
        {
            var job = _jobQueue.Pop();
            if ( job is not null )
            {
                job.Execute();
                job.Return();

                executeJobCount += 1;
            }
            else
            {
                if ( executeJobCount > 0 )
                {
                    OnFlushJob();
                    if ( Interlocked.Add( ref _remainJobCount, -executeJobCount ) == 0 )
                        break;

                    executeJobCount = 0;
                }
            }
        }

#if !NATIVE
        _isExecuting = 0;
        if ( _remainJobCount > 0 )
            Flush();
#endif
    }

    public void PostAfter( TimeSpan afterTime, Action action )
        => ThreadPoolExecutorService.Delay( afterTime, this, Job.New( action ) );
    public void PostAfter< T1 >( TimeSpan afterTime, Action< T1 > action, T1 t1 )
        => ThreadPoolExecutorService.Delay( afterTime, this, Job< T1 >.New( action, in t1 ) );
    public void PostAfter< T1 >( TimeSpan afterTime, Action< T1 > action, in T1 t1 )
        => ThreadPoolExecutorService.Delay( afterTime, this, Job< T1 >.New( action, in t1 ) );
    public void PostAfter< T1, T2 >( TimeSpan afterTime, Action< T1, T2 > action, T1 t1, T2 t2 )
        => ThreadPoolExecutorService.Delay( afterTime, this, Job< T1, T2 >.New( action, in t1, in t2 ) );
    public void PostAfter< T1, T2 >( TimeSpan afterTime, Action< T1, T2 > action, in T1 t1, in T2 t2 )
        => ThreadPoolExecutorService.Delay( afterTime, this, Job< T1, T2 >.New( action, in t1, in t2 ) );

    public void Post( Action action )
        => Dispatch( Job.New( action ) );
    public void Post< T1 >( Action< T1 > action, T1 t1 )
        => Dispatch( Job< T1 >.New( action, in t1 ) );
    public void Post< T1 >( Action< T1 > action, in T1 t1 )
        => Dispatch( Job< T1 >.New( action, in t1 ) );
    public void Post< T1, T2 >( Action< T1, T2 > action, T1 t1, T2 t2 )
        => Dispatch( Job< T1, T2 >.New( action, in t1, in t2 ) );
    public void Post< T1, T2 >( Action< T1, T2 > action, in T1 t1, in T2 t2 )
        => Dispatch( Job< T1, T2 >.New( action, in t1, in t2 ) );
    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > action, T1 t1, T2 t2, T3 t3 )
        => Dispatch( Job< T1, T2, T3 >.New( action, in t1, in t2, in t3 ) );
    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > action, in T1 t1, in T2 t2, in T3 t3 )
        => Dispatch( Job< T1, T2, T3 >.New( action, in t1, in t2, in t3 ) );
    public void Post< T1, T2, T3, T4 >( Action< T1, T2, T3, T4 > action, T1 t1, T2 t2, T3 t3, T4 t4 )
        => Dispatch( Job< T1, T2, T3, T4 >.New( action, in t1, in t2, in t3, in t4 ) );
    public void Post< T1, T2, T3, T4 >( Action< T1, T2, T3, T4 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4 )
        => Dispatch( Job< T1, T2, T3, T4 >.New( action, in t1, in t2, in t3, in t4 ) );
    public void Post< T1, T2, T3, T4, T5 >( Action< T1, T2, T3, T4, T5 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5 )
        => Dispatch( Job< T1, T2, T3, T4, T5 >.New( action, in t1, in t2, in t3, in t4, in t5 ) );
    public void Post< T1, T2, T3, T4, T5 >( Action< T1, T2, T3, T4, T5 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4, in T5 t5 )
        => Dispatch( Job< T1, T2, T3, T4, T5 >.New( action, in t1, in t2, in t3, in t4, in t5 ) );
    public void Post< T1, T2, T3, T4, T5, T6 >( Action< T1, T2, T3, T4, T5, T6 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4, in T5 t5, in T6 t6 )
        => Dispatch( Job< T1, T2, T3, T4, T5, T6 >.New( action, in t1, in t2, in t3, in t4, in t5, in t6 ) );

    public void Post2< T1 >( Action< T1 > action, T1 t1 )
        => DispatchJobAsync( Job< T1 >.New( action, t1 ) );

    public void Dispatch( JobEntry jobEntry )
    {
        if ( Push( jobEntry ) == 1 )
            ThreadPoolExecutorService.ExecuteOrRegister( this );
    }

    public void DispatchJobAsync( JobEntry jobEntry )
    {
        if ( Push( jobEntry ) == 1 )
            ThreadPoolExecutorService.Register( this );
    }

    public long Push( JobEntry jobEntry )
    {
        var jobCount = Interlocked.Increment( ref _remainJobCount );
        _jobQueue.Push( jobEntry );

        return jobCount;
    }

    public virtual void Tick()
    {
    }

    /// <summary>
    /// 잡큐를 비워내는 이벤트시 실행되는 콜백
    /// 한번의 Flush()중 여러번 실행될 수 있음
    /// </summary>
    protected virtual void OnFlushJob()
    {
    }

    /// <summary>
    /// 현재 실행중인 익스큐터 여부를 반환
    /// </summary>
    /// <returns></returns>
    public bool IsCurrentExecutor()
        => ThreadPoolExecutorService.SharedContext?.CurrentActor == this;

    public long RemainJobCount => _remainJobCount;
};
