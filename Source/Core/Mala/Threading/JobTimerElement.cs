﻿using Mala.Threading;

/// <summary>
/// 타이머 작업 원소
/// </summary>
public class JobTimerElement
{
    static Pool< JobTimerElement > _timerElementPool = new();

    //public WeakReference< JobExecutor > _executor;
    public Actor _executor;
    public JobEntry _job;

    public void Execute()
    {
        //_executor.DoJob( _job );
        _executor.DispatchJobAsync( _job );
    }

    public void Return()
    {
        _executor = null;
        _job      = null;

        _timerElementPool.Return( this );
    }

    public static JobTimerElement New( Actor executor, JobEntry job )
    {
        var element = _timerElementPool.GetOrDefault();

        //element._executor = new WeakReference< JobExecutor >( executor );
        element._executor = executor;
        element._job = job;

        return element;
    }
}
