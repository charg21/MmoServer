﻿using System.Collections.Concurrent;

namespace Mala.Threading;


public interface IJob
{
    public void Execute();
}


public class JobEntry : IJob
{
    internal JobEntry _next = null;

    public virtual void Execute() {}
    public virtual void Return() {}
}

public class PackagedJob : JobEntry
{
    List< JobEntry > _jobs = new();

    void Push( JobEntry job ) => _jobs.Add( job );

    public override void Execute()
    {
        foreach( JobEntry job in _jobs )
        {
            job.Execute();
        }
    }

    public override void Return()
    {
        foreach ( JobEntry job in _jobs )
        {
            job.Return();
        }

        _jobs.Clear();
        _next = null;
    }

}

public partial class Job : JobEntry
{
    Action _action;

    static ConcurrentBag< Job > _jobPool;

    static Job()
    {
        _jobPool = new();
    }

    public Job()
    {
    }

    public Job( Action action )
    {
        _action = action;
    }

    public override void Execute() => _action();

    public override void Return()
    {
        _action = null;
        _next = null;

#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static Job New( Action action )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job();
#endif
        job._action = action;

        return job;
    }
}

public partial class Job< T1 > : JobEntry
{
    Action< T1 > _action;
    T1 _t1;

    static ConcurrentBag< Job< T1 > > _jobPool;

    static Job()
    {
        _jobPool = new();
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public Job()
    {
    }

    public Job( Action< T1 > action, ref T1 t1 )
    {
        _action = action;
        _t1     = t1;
    }


    public Job( Action< T1 > action, T1 t1 )
    {
        _action = action;
        _t1 = t1;
    }

    public override void Execute() => _action( _t1 );

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _next = null;

#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static Job< T1 > New( Action< T1 > action, in T1 t1 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1 >();
#endif
        job._action = action;
        job._t1     = t1;

        return job;
    }

    public static Job< T1 > New( Action< T1 > action, T1 t1 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1 >();
#endif
        job._action = action;
        job._t1     = t1;

        return job;
    }
}

public partial class Job< T1, T2 > : JobEntry
{
    Action< T1, T2 > _action;
    T1 _t1;
    T2 _t2;

    static ConcurrentBag< Job< T1, T2 > > _jobPool;

    static Job()
    {
        _jobPool = new();
    }

    public Job()
    {
    }

    public Job( Action< T1, T2 > action, T1 t1, T2 t2 )
    {
        _action = action;
        _t1 = t1;
        _t2 = t2;
    }

    public override void Execute() => _action( _t1, _t2 );

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _t2 = default( T2 );
        _next = null;

#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static Job< T1 , T2 > New( Action< T1, T2 > action, T1 t1, T2 t2 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2 >();
#endif
        job._action = action;
        job._t1     = t1;
        job._t2     = t2;

        return job;
    }

    public static Job< T1 , T2 > New( Action< T1, T2 > action, in T1 t1, in T2 t2 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2 >();
#endif
        job._action = action;
        job._t1     = t1;
        job._t2     = t2;

        return job;
    }
}

public partial class Job< T1, T2, T3 > : JobEntry
{
    Action< T1, T2, T3 > _action;
    T1 _t1;
    T2 _t2;
    T3 _t3;

    static ConcurrentBag< Job< T1, T2, T3 > > _jobPool;

    static Job()
    {
        _jobPool = new();
    }

    public Job()
    {
    }

    public Job( Action< T1, T2, T3 > action, T1 t1, T2 t2, T3 t3 )
    {
        _action = action;
        _t1 = t1;
        _t2 = t2;
        _t3 = t3;
    }

    public override void Execute() => _action( _t1, _t2, _t3 );

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _t2 = default( T2 );
        _t3 = default( T3 );
        _next = null;
#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static Job< T1, T2, T3 > New( Action< T1, T2, T3 > action, T1 t1, T2 t2, T3 t3 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3 >();
#endif
        job._action = action;
        job._t1     = t1;
        job._t2     = t2;
        job._t3     = t3;

        return job;
    }

    public static Job< T1, T2, T3 > New( Action< T1, T2, T3 > action, in T1 t1, in T2 t2, in T3 t3 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3 >();
#endif
        job._action = action;
        job._t1     = t1;
        job._t2     = t2;
        job._t3     = t3;

        return job;
    }
}

public partial class Job< T1, T2, T3, T4 > : JobEntry
{
    Action< T1, T2, T3, T4 > _action;
    T1 _t1;
    T2 _t2;
    T3 _t3;
    T4 _t4;

    public Job()
    {
    }

    public Job( Action< T1, T2, T3, T4 > action, T1 t1, T2 t2, T3 t3, T4 t4 )
    {
        _action = action;
        _t1 = t1;
        _t2 = t2;
        _t3 = t3;
        _t4 = t4;
    }

    public override void Execute() => _action( _t1, _t2, _t3, _t4 );

    static ConcurrentBag< Job< T1, T2, T3, T4 > > _jobPool;
    static Job()
    {
        _jobPool = new();
    }

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _t2 = default( T2 );
        _t3 = default( T3 );
        _t4 = default( T4 );
        _next = null;
#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static Job< T1, T2, T3, T4 > New( Action< T1, T2, T3, T4 > action, T1 t1, T2 t2, T3 t3, T4 t4 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3, T4 >();
#endif
        job._action = action;
        job._t1 = t1;
        job._t2 = t2;
        job._t3 = t3;
        job._t4 = t4;

        return job;
    }

    public static Job< T1, T2, T3, T4 > New( Action< T1, T2, T3, T4 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3, T4 >();
#endif
        job._action = action;
        job._t1 = t1;
        job._t2 = t2;
        job._t3 = t3;
        job._t4 = t4;

        return job;
    }
}

public partial class Job< T1, T2, T3, T4, T5 > : JobEntry
{
    Action< T1, T2, T3, T4, T5 > _action;
    T1 _t1;
    T2 _t2;
    T3 _t3;
    T4 _t4;
    T5 _t5;

    static ConcurrentBag< Job< T1, T2, T3, T4, T5 > > _jobPool;

    static Job()
    {
        _jobPool = new();
    }

    public Job()
    {
    }

    public Job( Action< T1, T2, T3, T4, T5 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5 )
    {
        _action = action;
        _t1 = t1;
        _t2 = t2;
        _t3 = t3;
        _t4 = t4;
        _t5 = t5;
    }

    public override void Execute() => _action( _t1, _t2, _t3, _t4, _t5 );

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _t2 = default( T2 );
        _t3 = default( T3 );
        _t4 = default( T4 );
        _t5 = default( T5 );
        _next = null;
#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static Job< T1, T2, T3, T4, T5 > New( Action< T1, T2, T3, T4, T5 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3, T4, T5 >();
#endif
        job._action = action;
        job._t1 = t1;
        job._t2 = t2;
        job._t3 = t3;
        job._t4 = t4;
        job._t5 = t5;

        return job;
    }

    public static Job< T1, T2, T3, T4, T5 > New( Action< T1, T2, T3, T4, T5 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4, in T5 t5 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3, T4, T5 >();
#endif
        job._action = action;
        job._t1 = t1;
        job._t2 = t2;
        job._t3 = t3;
        job._t4 = t4;
        job._t5 = t5;

        return job;
    }

}


public partial class Job< T1, T2, T3, T4, T5, T6 > : JobEntry
{
    Action< T1, T2, T3, T4, T5, T6 > _action;
    T1 _t1;
    T2 _t2;
    T3 _t3;
    T4 _t4;
    T5 _t5;
    T6 _t6;

    static ConcurrentBag< Job< T1, T2, T3, T4, T5, T6 > > _jobPool;

    static Job()
    {
        _jobPool = new();
    }

    public Job()
    {
    }

    public Job( Action< T1, T2, T3, T4, T5, T6 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6 )
    {
        _action = action;
        _t1 = t1;
        _t2 = t2;
        _t3 = t3;
        _t4 = t4;
        _t5 = t5;
        _t6 = t6;
    }

    public override void Execute() => _action( _t1, _t2, _t3, _t4, _t5, _t6 );

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _t2 = default( T2 );
        _t3 = default( T3 );
        _t4 = default( T4 );
        _t5 = default( T5 );
        _t6 = default( T6 );
        _next = null;
#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static Job< T1, T2, T3, T4, T5, T6 > New( Action< T1, T2, T3, T4, T5, T6 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3, T4, T5, T6 >();
#endif
        job._action = action;
        job._t1 = t1;
        job._t2 = t2;
        job._t3 = t3;
        job._t4 = t4;
        job._t5 = t5;
        job._t6 = t6;

        return job;
    }

    public static Job< T1, T2, T3, T4, T5, T6 > New( Action< T1, T2, T3, T4, T5, T6 > action, in T1 t1, in T2 t2, in T3 t3, in T4 t4, in T5 t5, in T6 t6 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new();
#else
        var job = new Job< T1, T2, T3, T4, T5, T6 >();
#endif
        job._action = action;
        job._t1 = t1;
        job._t2 = t2;
        job._t3 = t3;
        job._t4 = t4;
        job._t5 = t5;
        job._t6 = t6;

        return job;
    }

}