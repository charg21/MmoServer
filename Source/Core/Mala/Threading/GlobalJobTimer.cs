﻿using Mala.Collection;

namespace Mala.Threading;

public class GlobalJobTimer
{
    /// <summary>
    /// 롱 타이머용 큐
    /// </summary>
    PriorityQueue< JobTimerElement, DateTime > _priorityQueue = new( (int)Math.MathHelper.PowOfTwo( 12 ) );

    /// <summary>
    /// 지연 처리 큐
    /// </summary>
    WaitFreeQueue< ( JobTimerElement, DateTime ) > _lazyTimerJobQueue = new();

    /// <summary>
    /// 바로 발동할 작업 타이머 휠
    /// </summary>
    JobWheelTimer _wheelTimer = new( 12, JobWheelTimer.SlotCapacity );

    /// <summary>
    /// 마지막 갱신 틱
    /// </summary>
    public bool _isExecuting = false;
    public i64 LastTick { get; set; } = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public GlobalJobTimer()
    {
    }

    //public override void Tick( long prev )
    //{
    //    Flush( prev );
    //    Distribute( prev );

    //    _tickRegistered = false;
    //}

    public void Flush( long prev )
    {
        for ( ;; )
        {
            if ( Interlocked.Exchange( ref _isExecuting, true ) )
                return;

            var now     = DateTime.Now;
            var nowTick = now.Ticks;
            while ( _lazyTimerJobQueue.TryDequeue( out var job ) )
            {
                if ( nowTick + 4096 >= job.Item2.Ticks )
                {
                    _wheelTimer.PostAfter( new( job.Item2.Ticks - nowTick ), job.Item1 );
                }
                else
                {
                    _priorityQueue.Enqueue( job.Item1, job.Item2 );
                }
            }

            Distribute( now );
        }

    }

    public void Distribute( DateTime now )
    {
        _wheelTimer.Flush( now.Ticks );

        while ( _priorityQueue.TryPeek( out var job, out var execTime ) )
        {
            if ( execTime > now )
                break;

            job.Execute();
            job.Return();

            _priorityQueue.Dequeue();
        }
    }

    /// <summary>
    ///
    /// </summary>
    public void PostAfter( TimeSpan afterTime, Actor executor, JobEntry job )
    {
        _lazyTimerJobQueue.Enqueue(
            ( JobTimerElement.New( executor, job ), DateTime.Now + afterTime ) );
    }

}

