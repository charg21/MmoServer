﻿using Mala.Threading;

using System.Collections.Concurrent;

/// <summary>
/// 클래스터 갱신 작업
/// </summary>
public class GlobalJobEntry : IJob
{
    public int _refCount = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public GlobalJobEntry( int refCount )
    {
        _refCount = refCount;
    }

    public int ReleaseRef() => Interlocked.Decrement( ref _refCount );

    public bool Update()
    {
        Execute();

        int refCount = ReleaseRef();
        if ( refCount is 0 )
        {
            OnFinish();
            return true;
        }
        else
        {
            return false;
        }
    }

    public virtual void Execute() { }
    public virtual void OnFinish() { }
    public virtual void Return() { }
}


public partial class GlobalJob : GlobalJobEntry
{
    Action _action;

    static ConcurrentBag< GlobalJob > _jobPool;

    static GlobalJob()
    {
        _jobPool = new();
    }

    public GlobalJob( int refCount ) : base( refCount )
    {
    }

    public GlobalJob( int refCount, Action action ) : base( refCount )
    {
        _action = action;
    }

    public override void Execute() => _action();

    public override void Return()
    {
        _action = null;
        _jobPool.Add( this );
    }

    public static GlobalJob New( int refCount, Action action )
    {
        if ( !_jobPool.TryTake( out var job ) )
            job = new( refCount );

        job._action = action;
        job._refCount = refCount;

        return job;
    }
}

public partial class GlobalJob< T1 > : GlobalJobEntry
{
    Action< T1 > _action;
    T1 _t1;

    static ConcurrentBag< GlobalJob< T1 > > _jobPool;

    static GlobalJob()
    {
        _jobPool = new();
    }

    public GlobalJob( int refCount ) : base( refCount )
    {
    }

    public GlobalJob( int refCount, Action< T1 > action, T1 t1 ) : base( refCount )
    {
        _action = action;
        _t1 = t1;
    }

    public override void Execute() => _action( _t1 );

    public override void Return()
    {
        _action = null;
        _t1 = default;
        _jobPool.Add( this );
    }

    public static GlobalJob< T1 > New( int refCount, Action< T1 > action, T1 t1 )
    {
        return New( refCount, action, in t1 );
    }

    public static GlobalJob< T1 > New( int refCount, Action< T1 > action, in T1 t1 )
    {
        if ( !_jobPool.TryTake( out var job ) )
            job = new( refCount );

        job._action = action;
        job._t1 = t1;
        job._refCount = refCount;

        return job;
    }
}

public partial class GlobalJob< T1, T2 > : GlobalJobEntry
{
    Action< T1, T2 > _action;
    T1 _t1;
    T2 _t2;

    static ConcurrentBag< GlobalJob< T1, T2 > > _jobPool;

    static GlobalJob()
    {
        _jobPool = new();
    }

    public GlobalJob( int refCount ) : base( refCount )
    {
    }

    public GlobalJob( int refCount, Action< T1, T2 > action, T1 t1, T2 t2 ) : base( refCount )
    {
        _action = action;
        _t1 = t1;
        _t2 = t2;
    }

    public override void Execute() => _action( _t1, _t2 );

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _t2 = default( T2 );

#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static GlobalJob< T1 , T2 > New( int refCount, Action< T1, T2 > action, T1 t1, T2 t2 )
    {
        return New( refCount, action, in t1, in t2 );
    }

    public static GlobalJob< T1 , T2 > New( int refCount, Action< T1, T2 > action, in T1 t1, in T2 t2 )
    {
#if JOB_POOLING
        //var job = _jobPool.Get();
        if ( !_jobPool.TryTake( out var job ) )
            job = new( refCount );
#else
        var job = new GlobalJob< T1, T2 >( refCount );
#endif
        job._action = action;
        job._t1     = t1;
        job._t2     = t2;
        job._refCount = refCount;

        return job;
    }
}

public partial class GlobalJob< T1, T2, T3 > : GlobalJobEntry
{
    Action< T1, T2, T3 > _action;
    T1 _t1;
    T2 _t2;
    T3 _t3;

    static ConcurrentBag< GlobalJob< T1, T2, T3 > > _jobPool;

    static GlobalJob()
    {
        _jobPool = new();
    }

    public GlobalJob( int refCount ) : base( refCount )
    {
    }

    public GlobalJob( int refCount, Action< T1, T2, T3 > action, T1 t1, T2 t2, T3 t3 ) : base( refCount )
    {
        _action = action;
        _t1 = t1;
        _t2 = t2;
        _t3 = t3;
    }

    public override void Execute() => _action( _t1, _t2, _t3 );

    public override void Return()
    {
        _action = null;
        _t1 = default( T1 );
        _t2 = default( T2 );
        _t3 = default( T3 );
#if JOB_POOLING
        _jobPool.Add( this );
#endif
    }

    public static GlobalJob< T1, T2, T3 > New( int refCount, Action< T1, T2, T3 > action, in T1 t1, in T2 t2, in T3 t3 )
    {
#if JOB_POOLING
        if ( !_jobPool.TryTake( out var job ) )
            job = new( refCount );
#else
        var job = new GlobalJob< T1, T2, T3 >( refCount );
#endif
        job._action = action;
        job._t1     = t1;
        job._t2     = t2;
        job._t3     = t3;
        job._refCount = refCount;

        return job;
    }
}

