﻿namespace Mala.Threading;

public interface IExecutor : IJob
{
    public void Tick();

#region Timer
    public void PostAfter( TimeSpan afterTime, Action action );
    public void PostAfter< T1 >( TimeSpan afterTime, Action< T1 > action, T1 t1 );
    public void PostAfter< T1, T2 >( TimeSpan afterTime, Action< T1, T2 > action, T1 t1, T2 t2 );
#endregion

#region Job
    public void Post( Action action );
    public void Post< T1 >( Action< T1 > action, T1 t1 );
    public void Post< T1, T2 >( Action< T1, T2 > action, T1 t1, T2 t2 );
    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > action, T1 t1, T2 t2, T3 t3 );
    public void Post< T1, T2, T3, T4 >( Action< T1, T2, T3, T4 > action, T1 t1, T2 t2, T3 t3, T4 t4 );
    public void Post< T1, T2, T3, T4, T5 >( Action< T1, T2, T3, T4, T5 > action, T1 t1, T2 t2, T3 t3, T4 t4, T5 t5 );
#endregion
}
