﻿namespace Mala.Threading;

using System;
using System.CodeDom.Compiler;
using Windows.UI.WebUI;
using ExecutorLoadBalancer = Collection.WaitFreeBoundedQueue< Actor >;

/// <summary>
/// 스레드 타입 열거형
/// </summary>
public enum EThreadType
{
    Main,
    Worker,
    None
}

public class ThreadPoolExecutionContext
{
    /// <summary>
    /// JobExecutor
    /// </summary>
    public Actor Executor = null;

    /// <summary>
    /// 현재 점유중인 JobExecutor
    /// </summary>
    public Actor CurrentActor = null;

    /// <summary>
    /// 작업 타이머
    /// 2^10 = 1024, 2^11 = 2048, 2^12 = 4096
    /// 최대 65.536초
    /// </summary>
    public JobWheelTimer _timeWheel = null;

    /// <summary>
    /// 마지막 실행 틱
    /// </summary>
    public i64 LastTickCount { get; set; } = Environment.TickCount64;

    /// <summary>
    /// 식별자
    /// </summary>
    public i64 Id = 0;

    public void Init()
    {
        Executor = new();
        _timeWheel = new( 12, JobWheelTimer.SlotCapacity );
    }
}

public static class ThreadPoolExecutorService
{
    public static readonly int MaxExecutionThreadCount = 28;
    public static ExecutorLoadBalancer _loadBalancer = new( 16 );
    static JobTimer GlobalJobTimer = new();

    public static long _jobIssueCount = 0;
    public static ThreadPoolExecutionContext[] _conetexts;

    /// <summary>
    /// 작업 타이머
    /// 2^10 = 1024, 2^11 = 2048, 2^12 = 4096
    /// 최대 65.536초를 커버
    /// </summary>
    public static JobWheelTimer[] _timeWheels;

    [ThreadStatic]
    public static EThreadType _threadType;

    public static long[] _actorExecuteCount;

    [ThreadStatic]
    public static ThreadPoolExecutionContext _tlsContext;
    public static ThreadPoolExecutionContext SharedContext
    {
        get
        {
            if ( _threadType == EThreadType.Worker)
                return _tlsContext ??= new();

            return null;
        }
    }

    static ThreadPoolExecutorService()
    {
        _conetexts = GC.AllocateArray< ThreadPoolExecutionContext >( MaxExecutionThreadCount, true );
        _actorExecuteCount = GC.AllocateArray< long >( MaxExecutionThreadCount, true );
    }

    public static int _centralContextCount = -1;

    public static void ExecuteOrRegister( Actor actor )
    {
        var context = SharedContext;

#if NATIVE
        if ( context.CurrentActor is null )
        {
            context.CurrentActor = actor;

            actor.Execute();

            context.CurrentActor = null;

            AddExecuteCount( 1 );
        }
        else
        {
            Register( actor );
        }
#else
        /// ConcurrentBag에 Add시 Local WorkStealingQueue가 생성됨
        if ( _threadType == EThreadType.Worker )
            ThreadPoolExecutorService.GExecutorList.Add( executor );
        else
            Handover( executor );
#endif
    }

    public static void Register( Actor executor )
    {
        _loadBalancer.Add( executor );
    }

    public static void Delay( TimeSpan afterTime, Actor executor, JobEntry action )
    {
        _tlsContext._timeWheel.PostAfter( afterTime, executor, action );
    }

    /// <summary>
    /// 작업을 실행한다.
    /// </summary>
    public static void Post( Action action )
    {
        var id       = NextJobIsuueCount();
        var executor = _conetexts[ id ].Executor;

        executor.DispatchJobAsync( Job.New( action ) );
    }

    /// <summary>
    /// 분배 한다.
    /// </summary>
    public static void Distribute()
    {
        var context = _tlsContext;
        context._timeWheel.Flush( context.LastTickCount );
    }

    public static void Run()
    {
        Update();
        Distribute();
        Dispatch();

        _tlsContext.LastTickCount = Environment.TickCount64;
    }

    /// <summary>
    /// 작업을 처리한다
    /// </summary>
    public static void Dispatch()
    {
        var context = _tlsContext;
        var actorExecuteCount = 0;
        while ( _loadBalancer.TryTake( out var executor ) )
        {
            /// 현재 스레드의 잡실행기 지정
            context.CurrentActor = executor;

            executor.Execute();

            context.CurrentActor = null;

            ++actorExecuteCount;
        }

        AddExecuteCount( actorExecuteCount );
    }

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public static void Initialize()
    {
        _threadType = EThreadType.Worker;
        var _ = SharedContext;
        _tlsContext.Init();

        _tlsContext.Id = GenId();
        _conetexts[ _tlsContext.Id ] = _tlsContext;
    }

    /// <summary>
    /// 갱신한다
    /// </summary>
    public static void Update()
    {
        Actor.Global.Execute();
    }

    public static long GetTotalExecuteCount()
    {
        if ( _actorExecuteCount is null )
            return 0;

        long total = 0;
        for ( var i = 0; i < MaxExecutionThreadCount; i++ )
            total += Interlocked.Exchange( ref _actorExecuteCount[ i ], 0 );
        return total;
    }

    public static SortedDictionary< int, long > GetMetricsInfos()
    {
        if ( _actorExecuteCount is null )
            return new();

        long total = 0;
        var actorExecuteCountPerThread = new SortedDictionary< int, long >();
        for ( int i = 0; i < MaxExecutionThreadCount; ++i )
            actorExecuteCountPerThread.Add( i, Interlocked.Exchange( ref _actorExecuteCount[ i ], 0 ) );

        return actorExecuteCountPerThread;
    }

    public static int GenId() => Interlocked.Increment( ref _centralContextCount );
    public static long NextJobIsuueCount() => Interlocked.Increment( ref _jobIssueCount ) % _centralContextCount;
    static void AddExecuteCount( long count ) => Interlocked.Add( ref _actorExecuteCount[ _tlsContext.Id ], count );
    public static long[] ActorExecuteCount => _actorExecuteCount;

}

