﻿using Mala.Math;


/// <summary>
/// 전 스레드에 반영하기 위한 전역 작업 처리기
/// </summary>
public class GlobalQueue
{
    /// <summary>
    /// 글로벌 잡 배열
    /// </summary>
    public GlobalJobEntry[] _jobArray;

    public long _jobIssueCount = -1;
    public readonly long _jobCapacity = 0;
    public readonly long _jobSizeMask = 0;
    public static int _threadPoolCount = 0;
    public static int _jobPopCount = 0;

    public ThreadLocal< long > _lJobReader = new( () => 0 );

    public GlobalQueue( int powOfTwo, int threadPoolCount )
    {
        _jobCapacity = MathHelper.PowOfTwo( powOfTwo );
        _jobSizeMask = _jobCapacity - 1;

        _jobArray = GC.AllocateArray< GlobalJobEntry >( (int)( _jobCapacity ), true );
        _threadPoolCount = threadPoolCount;
    }

    /// <summary>
    /// 작업을 추가한다
    /// </summary>
    public void Post( Action job )
    {
        var clusterJob = GlobalJob.New( _threadPoolCount, job );

        Push( clusterJob );
    }

    /// <summary>
    /// 작업을 추가한다
    /// </summary>
    public void Post< T1 >( Action< T1 > job, T1 t1 )
    {
        var clusterJob = GlobalJob< T1 >.New( _threadPoolCount, job, in t1 );

        Push( clusterJob );
    }

    /// <summary>
    /// 작업을 추가한다
    /// </summary>
    public void Post< T1, T2 >( Action< T1, T2 > job, in T1 t1, in T2 t2 )
    {
        var clusterJob = GlobalJob< T1, T2 >.New( _threadPoolCount, job, in t1, in t2 );

        Push( clusterJob );
    }

    /// <summary>
    /// 작업을 추가한다
    /// </summary>
    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > job, in T1 t1, in T2 t2, in T3 t3 )
    {
        var clusterJob = GlobalJob< T1, T2, T3 >.New( _threadPoolCount, job, in t1, in t2, in t3 );

        Push( clusterJob );
    }

    /// <summary>
    /// 작업을 추가한다
    /// </summary>
    public void Post< T1, T2, T3 >( Action< T1, T2, T3 > job, T1 t1, T2 t2, T3 t3 )
    {
        var clusterJob = GlobalJob< T1, T2, T3 >.New( _threadPoolCount, job, t1, t2, t3 );

        Push( clusterJob );
    }

    /// <summary>
    /// 작업을 추가한다
    /// </summary>
    public void Push( GlobalJobEntry job )
    {
        Mala.Core.Exception.ThrowIfFailed( job._refCount == _threadPoolCount );

        long nextJobPos = Interlocked.Increment( ref _jobIssueCount );

        var prevJob = Interlocked.Exchange( ref _jobArray[ ( nextJobPos & _jobSizeMask ) ], job );

        Mala.Core.Exception.MustBeNull( prevJob );
    }

    public GlobalJobEntry? Peek()
        => _jobArray[ ( _jobSizeMask & _lJobReader.Value ) ];

    public void Pop()
    {
        var job = Interlocked.Exchange( ref _jobArray[ _jobSizeMask & _lJobReader.Value ], null );

        Mala.Core.Exception.ThrowIfFailed( job is not null );

        Interlocked.Increment( ref _jobPopCount );

        job.Return();
    }

    /// <summary>
    /// 갱신한다
    /// </summary>
    public void Execute()
    {
        for ( ;; )
        {
            var job = Peek();
            if ( job is null )
                break;

            bool finished = job.Update();
            if ( finished )
                Pop();

            _lJobReader.Value += 1;
        }
    }

    public long GetJobCount()
    {
        return ( _jobIssueCount + 2 ) - _jobPopCount;
    }

}
