﻿using Mala.Math;
using Mala.Threading;
//using Slot = System.Collections.Generic.List< JobTimerElement >;
using Slot = ChunkList< JobTimerElement >;

/// <summary>
/// 타임 휠 방식 타이머
/// </summary>
public class JobWheelTimer
{
    /// <summary>
    /// 슬롯 기본 수용량,
    /// </summary>
    public const int SlotCapacity = 10000;

    /// <summary>
    /// 작업 간격
    /// </summary>
    public const long _tickInterval = 16;

    /// <summary>
    /// 마지막에 실행된 틱
    /// </summary>
    public TimeSpan _lastFlushedTick;

    /// <summary>
    /// 마지막에 실행된 인덱스
    /// </summary>
    public long _lastFlushedIndex;

    /// <summary>
    /// 슬롯 배열
    /// </summary>
    public Slot[] _slotArray;

    /// <summary>
    /// 슬롯 별 작업 최대 갯수
    /// </summary>
    public int _slotCapacity = 0;

    /// <summary>
    /// 슬롯 최대 갯수
    /// </summary>
    public long _slotsCapacity = 0;

    /// <summary>
    ///
    /// </summary>
    public long _slotSizeMask = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public JobWheelTimer( int slotsPowOfCapacity = 12, int slotCapacity = 4096 )
    {
        _slotsCapacity = MathHelper.PowOfTwo( slotsPowOfCapacity );
        _slotCapacity  = slotCapacity;
        _slotSizeMask  = _slotsCapacity - 1;
        _slotArray     = GC.AllocateArray< Slot >( (int)( _slotsCapacity ), pinned: true );

        for ( int i = 0; i < _slotsCapacity; i += 1 )
            _slotArray[ i ] = new Slot( 4096 );
            //_slotArray[ i ] = new Slot( _slotCapacity );

        _lastFlushedTick  = new( Environment.TickCount64 );
        _lastFlushedIndex = 0;
    }

    public void PostAfter( TimeSpan afterTime, Actor executor, JobEntry job )
        => PostAfter( afterTime, JobTimerElement.New( executor, job ) );

    public void PostAfter( TimeSpan afterTick, JobTimerElement timerElement )
    {
        long slotIndex = afterTick.Ticks / _tickInterval;

        long pushSlotIndex = ( slotIndex + _lastFlushedIndex ) & _slotSizeMask;

        _slotArray[ pushSlotIndex ].Add( timerElement );
    }

    /// <summary>
    /// 작업을 비운다.
    /// </summary>
    public void Flush( long curTick )
    {
        long tickGap = curTick - _lastFlushedTick.Ticks;
        if ( tickGap < _tickInterval )
            return;

        long popCount = tickGap / _tickInterval;
        Distribute( popCount );
    }

    /// <summary>
    /// 작업을 분배한다.
    /// </summary>
    public void Distribute( long popCount )
    {
        for ( long i = 0; i < popCount; i += 1 )
        {
            long nextSlotIndex = ( i + _lastFlushedIndex ) & _slotSizeMask;

            var curSlot = _slotArray[ nextSlotIndex ];

            int executionCount = 0;
            do
            {
                for ( ; executionCount < curSlot.Count; executionCount += 1 )
                {
                    var timerElement = curSlot[ executionCount ];
                    curSlot[ executionCount ] = null;

                    timerElement.Execute();
                    timerElement.Return();
                }
            }
            while ( executionCount < curSlot.Count );

            curSlot.Clear();
        }

        _lastFlushedTick += new TimeSpan( popCount * _tickInterval );
        _lastFlushedIndex += popCount;
    }
}
