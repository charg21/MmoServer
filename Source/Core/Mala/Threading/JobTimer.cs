﻿using Mala.Collection;
using Mala.Core;
using Mala.Threading;
using System.Collections.Generic;


/// <summary>
/// 잡 타이머
/// </summary>
public class JobTimer : Singleton< JobTimer >
{
    /// <summary>
    ///
    /// </summary>
    WaitFreeQueue< ( JobTimerElement, long ) > _lazyQueue = new();

    /// <summary>
    ///
    /// </summary>
    PriorityQueue< JobTimerElement, long > _pq = new( (int)Mala.Math.MathHelper.PowOfTwo( 12 ) );
    JobWheelTimer _wheelTimer = new( 12, JobWheelTimer.SlotCapacity );

    /// <summary>
    ///
    /// </summary>
    long _distributing = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public JobTimer()
    {
    }

    /// <summary>
    ///
    /// </summary>
    public void PostAfter(
        i64         afterTick,
        Actor executor,
        JobEntry    job )
    {
        _lazyQueue.Enqueue(
        (
            JobTimerElement.New( executor, job ),
            DateTime.UtcNow.Ticks + afterTick
        ) );
    }

    /// <summary>
    ///
    /// </summary>
    public void Flush( TimeSpan now )
    {
        /// 모아서 정리
        //while ( _lazyQueue.TryDequeue( out var a_b ) )
        //{
        //    if ( now + new TimeSpan( 50000 ) > a_b.Item2 )
        //        _wheelTimer.PostAfter( a_b.Item2 - now, a_b.Item1._executor, a_b.Item1._job );
        //    else
        //        _pq.Enqueue( a_b.Item1, a_b.Item2 );
        //}

        //int n = 0;
        //int bfsdf =3;

        //Distribute( now );
    }

    /// <summary>
    ///
    /// </summary>
    public void Distribute( i64 now )
    {
        //while ( _subQueue.TryPeek( out var timerElement, out var executeTick )  )
        //{
        //    if ( now < executeTick )
        //        break;

        //    timerElement.Execute();
        //    timerElement.Return();

        //    _subQueue.Dequeue();
        //}
    }

}
