﻿using System;
using System.Buffers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public struct FixedSizeList< T > : IDisposable, IEnumerable< T >
{
    private T[] _items;
    private int _count;
    private bool _disposed;

    public FixedSizeList( int capacity )
    {
        _items = ArrayPool< T >.Shared.Rent( capacity );
        _count = 0;
        _disposed = false;
    }

    public int Count => _count;

    public int Capacity => _items.Length;

    public void Add( T item )
    {
        if ( _count >= _items.Length )
        {
            throw new InvalidOperationException( "List capacity exceeded." );
        }

        _items[ _count++ ] = item;
    }

    public T this[ int index ]
    {
        get
        {
            if ( index < 0 || index >= _count )
            {
                throw new ArgumentOutOfRangeException( nameof( index ) );
            }

            return _items[ index ];
        }
        set
        {
            if ( index < 0 || index >= _count )
            {
                throw new ArgumentOutOfRangeException( nameof( index ) );
            }

            _items[ index ] = value;
        }
    }

    public void Clear()
    {
        Array.Clear( _items, 0, _count );
        _count = 0;
    }

    public void Dispose()
    {
        if ( _disposed )
            return;

        ArrayPool< T >.Shared.Return( _items, clearArray: true );
        _items = null!;
        _disposed = true;
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public IEnumerator< T > GetEnumerator()
    {
        throw new NotImplementedException();
    }

    public struct Enumerator : IEnumerator< T >
    {
        private readonly FixedSizeList< T > _list;
        private int _index;
        private bool _disposed;

        public Enumerator( FixedSizeList< T > list )
        {
            _list = list;
            _index = -1;
            _disposed = false;
        }

        public T Current => _list._items[ _index ];

        object IEnumerator.Current => Current;

        public bool MoveNext()
        {
            return ++_index < _list._count;
        }

        public void Reset()
        {
            _index = -1;
        }

        public void Dispose()
        {
        }
    }
}

public ref struct FixedSizeList2< T > : IDisposable
{
    private Span< T > _items;
    private int _count;
    private bool _disposed;

    public FixedSizeList2( Span< T > initialBuffer )
    {
        _items = initialBuffer;
        _count = 0;
        _disposed = false;
    }

    public int Count => _count;

    public int Capacity => _items.Length;

    public void Add( T item )
    {
        if ( _count >= _items.Length )
        {
            throw new InvalidOperationException( "List capacity exceeded." );
        }

        _items[ _count++ ] = item;
    }

    public T this[ int index ]
    {
        get
        {
            if ( index < 0 || index >= _count )
            {
                throw new ArgumentOutOfRangeException( nameof( index ) );
            }

            return _items[ index ];
        }
        set
        {
            if ( index < 0 || index >= _count )
            {
                throw new ArgumentOutOfRangeException( nameof( index ) );
            }

            _items[ index ] = value;
        }
    }

    public void Clear()
    {
        _items.Slice( 0, _count ).Clear();
        _count = 0;
    }

    public void Dispose()
    {
        if ( _disposed )
            return;

        Clear();
        _disposed = true;
    }

    public Enumerator GetEnumerator()
    {
        return new Enumerator( this );
    }

    public ref struct Enumerator
    {
        private readonly FixedSizeList2< T > _list;
        private int _index;

        public Enumerator( FixedSizeList2< T > list )
        {
            _list = list;
            _index = -1;
        }

        public T Current => _list._items[ _index ];

        public bool MoveNext()
        {
            return ++_index < _list._count;
        }
    }
}
