﻿using Mala.Math;
using System.Reflection.Metadata;
using System.Runtime.InteropServices;

namespace Mala.Collection;


/// <summary>
/// MPSC, Bounded, Circular Queue
/// </summary>
public class WaitFreeCircularQueue< T > where T : class
{
    /// <summary>
    /// 배열
    /// </summary>
    public T[] _elements;
    CacheLinePadding32 _elemPadding;

    /// <summary>
    ///
    /// </summary>
    public i64 _writePos = -1;
    CacheLinePadding32 _writerPadding;

    public i64 _readPos = 0;
    CacheLinePadding32 _readerPadding;

    /// <summary>
    ///
    /// </summary>
    public readonly i64 _capacity = 0;
    public readonly i64 _sizeMask = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public WaitFreeCircularQueue( int powOfTwo )
    {
        _capacity = MathHelper.PowOfTwo( powOfTwo );
        _sizeMask = _capacity - 1;

        _elements = GC.AllocateArray< T >( (int)_capacity, true );
    }

    /// <summary>
    ///
    /// </summary>
    public void Enqueue( T element )
    {
        long enqueuePos = Interlocked.Increment( ref _writePos );

        var prevJob = Interlocked.Exchange( ref _elements[ ( enqueuePos & _sizeMask ) ], element );

        Core.Exception.ThrowIfFailed( prevJob is null );
    }

    /// <summary>
    ///
    /// </summary>
    public T? Peek() => _elements[ _sizeMask & _readPos ];

    /// <summary>
    ///
    /// </summary>
    public T? Dequeue()
    {
        T? element = Interlocked.Exchange( ref _elements[ _readPos & _sizeMask ], null );

        _readPos += 1;

        return element;
    }

    /// <summary>
    ///
    /// </summary>
    public bool TryDequeue( out T dest )
    {
        var element = Peek();
        if ( element is null )
        {
            dest = null;
            return false;
        }

        dest = Interlocked.Exchange( ref _elements[ _sizeMask & _readPos ], null );

        Core.Exception.ThrowIfFailed( dest is not null );

        _readPos += 1;

        return true;
    }

}
