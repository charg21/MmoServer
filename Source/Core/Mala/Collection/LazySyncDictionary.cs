﻿using Org.BouncyCastle.Crypto.Paddings;
using System.Collections.Concurrent;

/// <summary>
/// 게으른 동기화 딕셔너리
/// </summary>
public class LazySyncDictionary< TKey, TValue > : Dictionary< TKey, TValue >
{
    /// <summary>
    ///
    /// </summary>
    ConcurrentQueue< Action > _jobQueue = new();

    /// <summary>
    /// 비동기로 원소를 추가한다.
    /// </summary>
    public void AddAsync( TKey key, TValue value )
    {
        _jobQueue.Enqueue( () => TryAdd( key, value )  );
    }

    /// <summary>
    /// 비동기로 원소를 제거한다.
    /// </summary>
    public void RemoveAsync( TKey key )
    {
        _jobQueue.Enqueue( () => Remove( key ) );
    }

    /// <summary>
    ///
    /// </summary>
    public bool TryAddSync( TKey key, TValue value )
    {
        bool added = false;

        _jobQueue.Enqueue( () => added = TryAdd( key, value ) );

        Flush();

        return added;
    }

    /// <summary>
    ///
    /// </summary>
    public void Flush()
    {
        while ( _jobQueue.TryDequeue( out var job ) )
        {
            job();
        }
    }

    /// <summary>
    ///
    /// </summary>
    public bool TryGetValueSync( TKey key, out TValue? value )
    {
        Flush();
        return TryGetValue( key, out value );
    }
}
