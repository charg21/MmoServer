﻿namespace Mala.Collection;

/// <summary>
/// MPMC Wait-Free Queue
/// Fixed-Size, Array-Based
/// </summary>
public class WaitFreeBoundedQueue< T > where T : class
{
    /// <summary>
    /// 원소의 배열
    /// </summary>
    private T[] _elements;
    CacheLinePadding32 _elemPadding;

    /// <summary>
    /// 제거 위치
    /// </summary>
    private i64 _headPos = 0;
    CacheLinePadding32 _headPadding;

    /// <summary>
    /// 삽입 위치
    /// </summary>
    private i64 _tailPos = -1;

    /// <summary>
    /// 원소 최대 수용량
    /// </summary>
    private readonly i64 _capacity = 0;

    /// <summary>
    ///
    /// </summary>
    private readonly i64 _sizeMask = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public WaitFreeBoundedQueue( int capacityOfPower )
    {
        _capacity = Math.MathHelper.PowOfTwo( capacityOfPower );
        _sizeMask = _capacity - 1;

        _elements = GC.AllocateArray< T >( (int)_capacity, pinned: true );
    }

    /// <summary>
    /// 원소의 갯수
    /// </summary>
    public i64 Count => _tailPos - _headPos;

    /// <summary>
    /// 원소를 추가한다
    /// </summary>
    public void Enqueue( T item )
    {
        var enqueuePos = Interlocked.Increment( ref _tailPos );

        _elements[ enqueuePos & _sizeMask ] = item;
    }

    /// <summary>
    /// 원소를 추가한다
    /// </summary>
    public void Add( T item )
    {
        var enqueuePos = Interlocked.Increment( ref _tailPos );

        _elements[ enqueuePos & _sizeMask ] = item;
    }


    /// <summary>
    /// 원소를 제거한다.
    /// </summary>
    public bool TryDequeue( out T dest )
    {
        var dequeuedElement = Interlocked.Exchange< T >( ref _elements[ _headPos & _sizeMask ], null );
        if ( dequeuedElement is not null )
        {
            dest = dequeuedElement;
            Interlocked.Increment( ref _headPos );

            return true;
        }

        dest = null;
        return false;
    }

    /// <summary>
    /// 원소를 제거한다.
    /// </summary>
    public bool TryTake( out T dest ) => TryDequeue( out dest );

    /// <summary>
    /// 원소를 제거한다.
    /// </summary>
    /// <returns></returns>
    public T? Dequeue()
    {
        var dequeuedElement = Interlocked.Exchange< T >( ref _elements[ _headPos & _sizeMask ], null );
        if ( dequeuedElement is not null )
            Interlocked.Increment( ref _headPos );

        return dequeuedElement;
    }

    public void ForEach( Action< T > job )
    {
        for ( var cur = _headPos; cur < _capacity; cur += 1 )
        {
            var element = _elements[ cur ];
            if ( element is not null )
                job( element );
        }
    }

    public void Clear()
    {
        _headPos = 0;
        _tailPos = -1;
    }
}
