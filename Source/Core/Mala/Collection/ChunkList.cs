﻿using System.Collections;
using System.Diagnostics;

/// <summary>
/// 원소를 청크 단위로 관리하는 리스트
/// </summary>
[DebuggerDisplay( "Count = {Count}" )]
public class ChunkList< T > : List< List< T > >, IList
{
    /// <summary>
    /// 청크를 관리하는 풀
    /// </summary>
    static QueuePool< List< T > > _chunkPool;

    static ChunkList()
    {
        _chunkPool = new();
    }

    /// <summary>
    /// 전체 수용량
    /// </summary>
    int _capacity = 0;

    /// <summary>
    /// 청크별 수용량
    /// </summary>
    int _chunkCapacity = 0;

    /// <summary>
    /// 현재 청크( = 리스트 )
    /// </summary>
    List< T > _curList = null;

    /// <summary>
    /// 원소의 갯수
    /// </summary>
    int _size = 0;

    /// <summary>
    ///
    /// </summary>
    public new int Count => _size;

    /// <summary>
    ///
    /// </summary>
    public int ChunkCount => base.Count;
    public int Cpapcity => base.Count * _chunkCapacity;

    List< T > RentChunk( int chunkCapacity = 4096 )
    {
        var chunk = _chunkPool.TryGet();
        if ( chunk is null )
            chunk = new List< T >( chunkCapacity );

        return chunk;
    }

    void ReturnChunk( List< T > chunk )
    {
        _chunkPool.Return( chunk );
    }

    /// <summary>
    ///
    /// </summary>
    public ChunkList( int chunkCapacity = 1024, int chunkCount = 1 ) : base()
    {
        _size = 0;
        _chunkCapacity = chunkCapacity;
        _capacity = chunkCapacity * chunkCount;

        for ( int n = 0; n < chunkCount; n += 1 )
        {
            base.Add( RentChunk( chunkCapacity ) );
        }

        if ( _capacity > 0 )
            _curList = base[ 0 ];
    }

    public void Add( T item )
    {
        EnsureChunk();

        _curList.Add( item );
        _size += 1;
    }

    public void EnsureChunk()
    {
        if ( _curList.Count >= _chunkCapacity )
        {
            _curList = RentChunk( _chunkCapacity );
            base.Add( _curList );
        }
    }

    public new T this[ int index ]
    {
        get
        {
            if ( (uint)index >= (uint)_size )
                return default( T );

            return base[ index / _chunkCapacity ][ index % _chunkCapacity ];
        }
        set
        {
            if ( (uint)index >= (uint)_size )
            {
                // TODO: Throw Exception or Alloc Chunk;
                throw new IndexOutOfRangeException();
            }

            base[ index / _chunkCapacity ][ index % _chunkCapacity ] = value;
        }
    }


    public new void Clear()
    {
        int chunkCount = ChunkCount;

        for ( int n = chunkCount - 1; n >= 0; n -= 1 )
        {
            base[ n ].Clear();
            if ( n != 0 )
            {
                var lastChunk = base[ n ];

                /// 제거
                base.RemoveAt( n );

                /// 풀에 반환
                _chunkPool.Return( lastChunk );
            }
        }

        _size = 0;
        _capacity = _chunkCapacity * ChunkCount;
        _curList = null;
        if ( _capacity > 0 )
            _curList = base[ 0 ];
    }
}
