﻿using System.Numerics;

namespace Mala.Collection;

/// <summary>
/// Enum 플래그들에 대해 특수화한 딕셔너리 인터페이스 콜렉션
/// </summary>
public class EnumFlagDictionary< TEnumKey, TValue >
    where TEnumKey : unmanaged
    where TValue : new()
{
    enum InternalFlag : ulong
    {
        None = 0,

        _1  = 1 << 0,
        _2  = 1 << 1,
        _3  = 1 << 2,
        _4  = 1 << 3,
        _5  = 1 << 4,
        _6  = 1 << 5,
        _7  = 1 << 6,
        _8  = 1 << 7,
        _9  = 1 << 8,
        _10 = 1 << 9,


        All = ulong.MaxValue,
    }

    /// <summary>
    /// Enum Flag을 Index로 변환( *박싱 없이 )
    /// </summary>
    /// <ref>https://www.sysnet.pe.kr/2/0/11565</ref>
    /// <returns></returns>
    public unsafe ulong ConvertToIndex( TEnumKey key )
    {
        TEnumKey* idx = &key;
        var intPtr = (ulong*)idx;
        return *intPtr;
    }

    unsafe InternalFlag ConvertToInternalFlag( TEnumKey key )
    {
        TEnumKey* idx = &key;
        var intPtr = (InternalFlag*)( idx );
        return *intPtr;
    }

    /// <summary>
    /// 소유한 Enum Flag
    /// </summary>
    InternalFlag _ownFlag;

    /// <summary>
    /// Enum Flag에 대응하는 값
    /// </summary>
    List< TValue > _array = new( 64 );

    /// <summary>
    /// 순회용
    /// </summary>
    List< TValue > _values = new( 64 );

    public ICollection<TValue> Values => _values;

    public int Count => Values.Count;

    public void Add( TEnumKey key, TValue value )
    {
        ulong keyFlag = ConvertToIndex( key );
        _array[ BitOperations.TrailingZeroCount( keyFlag ) ] = value;
    }

    public void Clear()
    {
        _ownFlag = InternalFlag.None;
        _values.Clear();
        _array.Clear();
    }

    public bool ContainsKey( TEnumKey key )
    {
        var keyFlag = ConvertToInternalFlag( key );
        return HasFlag( keyFlag );
    }

    public bool TryGet( TEnumKey key, out TValue value )
    {
        var keyFlag = ConvertToInternalFlag( key );
        if ( HasFlag( keyFlag ) )
        {
            value = _array[ BitOperations.TrailingZeroCount( (ulong)keyFlag ) ];
            return true;
        }

        value = default;
        return false;
    }

    public bool Remove( TEnumKey key )
    {
        var keyFlag = ConvertToInternalFlag( key );
        if ( HasFlag( keyFlag ) )
        {
            // 비트 스캔을 사용하여 가장 낮은 비트가 설정된 위치를 찾습니다.
            int index = BitOperations.TrailingZeroCount( ( ulong)keyFlag );
            _ownFlag &= ~(InternalFlag)( 1 << index );
            _values.RemoveAt( index );
            _array[ index ] = default;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Flag 보유 여부를 반환한다.
    /// Enum.HasFlag()가 IL에서 박싱이 발생하여, 직접 비트 연산을 통해 비교
    /// </summary>
    bool HasFlag( InternalFlag flag )
    {
        return ( (ulong)( _ownFlag  ) & (ulong)( flag ) ) > (ulong)( flag );
    }
}
