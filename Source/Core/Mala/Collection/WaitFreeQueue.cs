﻿using System.Collections.Concurrent;

namespace Mala.Collection;

/// <summary>
/// MPSC Wait-Free Queue
/// </summary>
public class WaitFreeQueue< T >
{
    /// <summary>
    /// producer
    /// </summary>
    private NodeEntry _head;

    /// <summary>
    /// consumer
    /// </summary>
    private volatile NodeEntry _tail;

    /// <summary>
    /// 생성자
    /// </summary>
    public WaitFreeQueue()
    {
        _head = new NodeEntry();
        _tail = _head;
    }

    public bool Empty => _head._next is null;

    /// <summary>
    /// 원소를 추가한다.
    /// </summary>
    public void Enqueue( T item )
    {
        var newNode = NewNode();
        newNode._item = item;

        NodeEntry prevNode = Interlocked.Exchange( ref _tail, newNode );
        prevNode._next = newNode;
    }

    /// <summary>
    /// 노드를 생성한다.
    /// </summary>
    NodeEntry NewNode()
    {
        if ( !_nodePool.TryTake( out var newNode ) )
            newNode = new();

        return newNode;
    }

    /// <summary>
    /// 원소를 추가한다.
    /// </summary>
    public void Enqueue( IEnumerable< T > collection )
    {
        NodeEntry tempHead = null;
        NodeEntry tempTail = null;

        foreach ( T item in collection )
        {
            NodeEntry newNode = NewNode();
            newNode._item = item;

            if ( null == tempHead )
            {
                tempHead = newNode;
            }
            else
            {
                tempHead._next = newNode;
            }

            tempTail = newNode;
        }

        if( null == tempHead )
            return;

        NodeEntry prevTailNode = Interlocked.Exchange( ref _tail, tempTail );
        prevTailNode._next = tempHead;
    }


    /// <summary>
    /// 원소를 제거한다
    /// </summary>
    public bool TryDequeue( out T? dest )
    {
        NodeEntry dummyNode = _head;
        NodeEntry iterator = _head._next;

        if ( null == iterator )
        {
            dest = default;
            return false;
        }

        dest = iterator._item;
        _head = iterator;

        /// 노드풀에 반환전엔 무조건 다른 객체와의 링크등을 제거해야함.
        /// GC환경에서 객체간 처리
        dummyNode._next = null;
        iterator._item = default( T );

        //_nodePool.Return( dummyNode );
        _nodePool.Add( dummyNode );

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public bool TryDequeueAll( ICollection< T > collection )
    {
        NodeEntry dummyIterator = _head;
        NodeEntry iterator      = dummyIterator._next;

        while ( iterator is not null )
        {
            collection.Add( iterator._item );

            iterator._item      = default( T );
            dummyIterator._next = null;

            //_nodePool.Return( dummyIterator );
            _nodePool.Add( dummyIterator );

            dummyIterator = iterator;
            iterator      = dummyIterator._next;
        }

        _head = dummyIterator;

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public bool TryDequeueAll( ICollection< T > collection, int maxSize )
    {
        NodeEntry dummyIterator = _head;
        NodeEntry iterator      = dummyIterator._next;

        while ( iterator is not null )
        {
            collection.Add( iterator._item );

            iterator._item      = default( T );
            dummyIterator._next = null;

            dummyIterator = iterator;
            iterator      = dummyIterator._next;
        }

        _head = dummyIterator;

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public bool TryPeek( out T? dest )
    {
        NodeEntry iterator = _head._next;
        if ( null == iterator )
        {
            dest = default;
            return false;
        }

        dest = iterator._item;

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public bool Consume( Action< T > job )
    {
        NodeEntry head     = _head._next;
        NodeEntry iterator = _head._next;

        if ( null != iterator )
        {
            job( iterator._item );

            iterator._item = default( T );
            head._next = null;

            _head = iterator;

            return true;
        }

        return false;
    }

    public long ConsumeAll( Action< T > job )
    {
        long consumedCount = 0;

        NodeEntry dummyIterator = _head;
        NodeEntry iterator      = dummyIterator._next;

        while ( null != iterator )
        {
            job( iterator._item );

            consumedCount += 1;

            iterator._item = default( T );
            dummyIterator._next = null;

            dummyIterator = iterator;
            iterator = dummyIterator._next;
        }

        _head = dummyIterator;

        return consumedCount;
    }

    public void ForEach( Action< T > job )
    {
        NodeEntry dummyIterator = _head;
        NodeEntry iterator      = dummyIterator._next;

        while ( null != iterator )
        {
            job( iterator._item );

            dummyIterator = iterator;
            iterator = dummyIterator._next;
        }
    }

    public void ForEach( Func< T, EForEachResult > job )
    {
        NodeEntry dummyIterator = _head;
        NodeEntry iterator      = dummyIterator._next;

        while ( null != iterator )
        {
            var result = job( iterator._item );
            if ( result == EForEachResult.Break )
                return;

            dummyIterator = iterator;
            iterator = dummyIterator._next;
        }
    }

    /// <summary>
    ///
    /// </summary>
    public class NodeEntry
    {
        public T         _item;
        public NodeEntry _next;
    }

    /// <summary>
    ///
    /// </summary>
    public static ConcurrentBag< NodeEntry > _nodePool = [];

}
