﻿using Google.Protobuf.WellKnownTypes;
using System.Collections;

/// <summary>
/// 
/// </summary>
public class BitGrid
{
    /// <summary>
    /// 
    /// </summary>
    int _width;

    /// <summary>
    /// 
    /// </summary>
    int _height;

    /// <summary>
    /// 
    /// </summary>
    BitArray _bitArray;

    /// <summary>
    /// 생성자
    /// </summary>
    public BitGrid( int width, int height )
    {
        _bitArray = new BitArray( width * height );
        _width = width;
        _height = height;
    }

    /// <summary>
    /// 인덱서
    /// </summary>
    public bool this[ int col, int row ]
    {
        get => Get( col, row );
        set => Set( col, row, value );
    }

    /// <summary>
    /// 세터
    /// </summary>
    public void Set( int col, int row, bool value ) => _bitArray[ ( row * _width ) + col ] = value;

    /// <summary>
    /// 게터
    /// </summary>
    public bool Get( int col, int row ) => _bitArray[ ( row * _width ) + col ];
}
