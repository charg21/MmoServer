﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using ZstdSharp;

/// <summary>
/// 구조체 박싱을 줄이기 위해 특수화된 리스트
/// </summary>
[DebuggerDisplay( "Count = {Count}" )]
public class ValueList< T > : IList, IReadOnlyList< T > where T : notnull
{
    private const int DefaultCapacity = 4;

    private T[] _items;
    private int _size = 0;
#pragma warning disable CA1825, IDE0300 // avoid the extra generic instantiation for Array.Empty<T>()
    private static readonly T[] s_emptyArray = new T[0];
#pragma warning restore CA1825, IDE0300

    // Constructs a List. The list is initially empty and has a capacity
    // of zero. Upon adding the first element to the list the capacity is
    // increased to DefaultCapacity, and then increased in multiples of two
    // as required.
    public ValueList()
    {
        _items = s_emptyArray;
    }

    public ValueList( int capacity )
    {
        if ( capacity < 0 )
            throw new ArgumentException( "Capacity Must Non Negative Value." );

        if ( capacity == 0 )
            _items = s_emptyArray;
        else
            _items = new T[ capacity ];
    }

    // Read-only property describing how many elements are in the List.
    public int Count => _size;

    public bool IsReadOnly => false;
    public bool IsFixedSize => false;
    public object SyncRoot => this;
    public bool IsSynchronized => false;


    object? IList.this[ int index ] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

    public int Add( object value )
    {
        if ( value is T item )
        {
            Add( item );
            return _size - 1;
        }
        throw new ArgumentException( "Value is of incorrect type." );
    }

    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public void Add( in T item )
    {
        T[] array = _items;
        int size = _size;
        if ( (uint)size < (uint)array.Length )
        {
            _size = size + 1;
            array[ size ] = item;
        }
        else
        {
            AddWithResize( in item );
        }
    }

    // Non-inline from List.Add to improve its code quality as uncommon path
    [MethodImpl( MethodImplOptions.NoInlining )]
    private void AddWithResize( in T item )
    {
        Debug.Assert( _size == _items.Length );
        int size = _size;
        Grow( size + 1 );
        _size = size + 1;
        _items[ size ] = item;
    }

    // Clears the contents of List.
    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public void Clear()
    {
        if ( RuntimeHelpers.IsReferenceOrContainsReferences< T >() )
        {
            int size = _size;
            _size = 0;
            if ( size > 0 )
            {
                Array.Clear( _items, 0, size ); // Clear the elements so that the gc can reclaim the references.
            }
        }
        else
        {
            _size = 0;
        }
    }

    public bool Contains( object value )
    {
        return value is T item && IndexOf( item ) >= 0;
    }

    public int IndexOf( object value )
    {
        if ( value is T item )
        {
            return IndexOf( item );
        }
        return -1;
    }

    public int IndexOf( T item )
    {
        for ( int i = 0; i < _size; i++ )
        {
            if ( _items[ i ].Equals( item ) )
            {
                return i;
            }
        }
        return -1;
    }

    public void Insert( int index, object value )
    {
        throw new NotImplementedException();
        //if ( value is T item )
        //{
        //    Insert( index, item );
        //}
        //else
        //{
        //    throw new ArgumentException( "Value is of incorrect type." );
        //}
    }

    public void Insert( int index, T item )
    {
        if ( index < 0 || index > _size )
        {
            throw new ArgumentOutOfRangeException( nameof( index ) );
        }

        if ( _size == _items.Length )
        {
            Array.Resize( ref _items, _items.Length * 2 );
        }

        Array.Copy( _items, index, _items, index + 1, _size - index );
        _items[ index ] = item;
        _size++;
    }

    public void Remove( object value )
    {
        if ( value is T item )
        {
            Remove( item );
        }
    }

    public bool Remove( T item )
    {
        int index = IndexOf( item );
        if ( index >= 0 )
        {
            RemoveAt( index );
            return true;
        }
        return false;
    }

    public void RemoveAt( int index )
    {
        if ( index < 0 || index >= _size )
        {
            throw new ArgumentOutOfRangeException( nameof( index ) );
        }

        _size--;
        Array.Copy( _items, index + 1, _items, index, _size - index );
        _items[ _size ] = default;
    }

    public T this[ int index ]
    {
        get
        {
            if ( index < 0 || index >= _size )
            {
                throw new ArgumentOutOfRangeException( nameof( index ) );
            }
            return _items[ index ];
        }
        set
        {
            if ( value is T item )
            {
                if ( index < 0 || index >= _size )
                {
                    throw new ArgumentOutOfRangeException( nameof( index ) );
                }
                _items[ index ] = item;
            }
            else
            {
                throw new ArgumentException( "Value is of incorrect type." );
            }
        }
    }

    public void CopyTo( Array array, int index )
    {
        if ( array is T[] itemsArray )
        {
            Array.Copy( _items, 0, itemsArray, index, _size );
        }
        else
        {
            throw new ArgumentException( "Array is of incorrect type." );
        }
    }


    public T Last()
    {
        if ( _size == 0 )
            throw new InvalidOperationException( "The list is empty." );

        return _items[ _size - 1 ];
    }

    public ref T LastRef()
    {
        if ( _size == 0 )
            throw new InvalidOperationException( "The list is empty." );

        return ref _items[ _size - 1 ];
    }

    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public ref T ReserveOneItem()
    {
        T[] array = _items;
        int size = _size;
        if ( (uint)size < (uint)array.Length )
        {
            _size = size + 1;
        }
        else
        {
            ReserveWithResize();
        }

        return ref LastRef();
    }

    // Non-inline from List.Add to improve its code quality as uncommon path
    [MethodImpl( MethodImplOptions.NoInlining )]
    private void ReserveWithResize()
    {
        Debug.Assert( _size == _items.Length );
        int size = _size;
        Grow( size + 1 );
        _size = size + 1;
    }


    public int EnsureCapacity( int capacity )
    {
        if ( capacity < 0 )
        {
            throw new InvalidOperationException( "The list is empty." );
        }
        if ( _items.Length < capacity )
        {
            Grow( capacity );
        }

        return _items.Length;
    }

    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    private int GetNewCapacity( int capacity )
    {
        Debug.Assert( _items.Length < capacity );

        int newCapacity = _items.Length == 0 ? DefaultCapacity : 2 * _items.Length;

        // Allow the list to grow to maximum possible capacity (~2G elements) before encountering overflow.
        // Note that this check works even when _items.Length overflowed thanks to the (uint) cast
        if ( (uint)newCapacity > Array.MaxLength ) newCapacity = Array.MaxLength;

        // If the computed capacity is still less than specified, set to the original argument.
        // Capacities exceeding Array.MaxLength will be surfaced as OutOfMemoryException by Array.Resize.
        if ( newCapacity < capacity ) newCapacity = capacity;

        return newCapacity;
    }

    // Gets and sets the capacity of this list.  The capacity is the size of
    // the internal array used to hold items.  When set, the internal
    // array of the list is reallocated to the given capacity.
    //
    public int Capacity
    {
        get => _items.Length;
        set
        {
            if ( value < _size )
            {
                throw new IndexOutOfRangeException( "OutOfRangeException." );
            }

            if ( value != _items.Length )
            {
                if ( value > 0 )
                {
                    T[] newItems = new T[value];
                    if ( _size > 0 )
                    {
                        Array.Copy( _items, newItems, _size );
                    }
                    _items = newItems;
                }
                else
                {
                    _items = s_emptyArray;
                }
            }
        }
    }


    /// <summary>
    /// Increase the capacity of this list to at least the specified <paramref name="capacity"/>.
    /// </summary>
    /// <param name="capacity">The minimum capacity to ensure.</param>
    internal void Grow( int capacity )
    {
        Capacity = GetNewCapacity( capacity );
    }

    IEnumerator< T > IEnumerable< T >.GetEnumerator()
    {
        throw new NotImplementedException();
    }

    public IEnumerator GetEnumerator()
    {
        for ( int i = 0; i < _size; i++ )
        {
            yield return _items[ i ];
        }
    }

    public ref T GetElementRef( int index )
    {
        if ( index < 0 || index >= _size )
        {
            throw new IndexOutOfRangeException( nameof( index ) );
        }

        return ref _items[ index ];
    }

    [MethodImpl( MethodImplOptions.AggressiveInlining )]
    public ReadOnlySpan< T > AsSpan()
    {
        if ( _size == 0 )
            return ReadOnlySpan< T >.Empty;

        return _items.AsSpan( 0, _size );
    }
}