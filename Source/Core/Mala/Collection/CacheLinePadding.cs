﻿using System.Runtime.InteropServices;

namespace Mala.Collection;

[StructLayout( LayoutKind.Explicit, Size = 8 )]
public struct CacheLinePadding8
{
    [FieldOffset( 0 )]
    internal int DummyValue;
}

[StructLayout( LayoutKind.Explicit, Size = 16 )]
public struct CacheLinePadding16
{
    [FieldOffset( 0 )]
    internal int DummyValue;
}

[StructLayout( LayoutKind.Explicit, Size = 32 )]
public struct CacheLinePadding32
{
    [FieldOffset( 0 )]
    internal int DummyValue;
}

[StructLayout( LayoutKind.Explicit, Size = 64 )]
public struct CacheLinePadding64
{
    [FieldOffset( 0 )]
    internal int DummyValue;
}
