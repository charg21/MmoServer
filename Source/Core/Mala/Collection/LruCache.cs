﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mala.Collection;

public class LruCache< TKey, TElement > where TKey : notnull
{
    private readonly LinkedList< ( TKey Key, TElement Value ) > _timeline = new();
    private readonly Dictionary< TKey, LinkedListNode< ( TKey Key, TElement Value ) > > _index;

    public int Capacity { get; }

    public LruCache( int capacity )
    {
        _index = new( capacity );
        Capacity = capacity;
    }

    public LruCache( int maxCount, IEqualityComparer< TKey > comparer )
    {
        _index = new Dictionary< TKey, LinkedListNode< ( TKey, TElement ) > >( maxCount, comparer );
        Capacity = maxCount;
    }

    public int Count => _timeline.Count;
    public IEnumerable< TElement > Values => _timeline.Select( e => e.Value );

    public bool TryGetValue( TKey key, out TElement result )
    {
        if ( !_index.TryGetValue( key, out var node ) )
        {
            result = default!;
            return false;
        }

        if ( node is null )
        {
            result = default!;
            return false;
        }

        result = node.Value.Value;
        _timeline.Remove( node );
        _timeline.AddFirst( node );
        return true;
    }

    public bool Insert( TKey key, [DisallowNull] TElement element )
    {
        bool insert = false;
        if ( !_index.TryGetValue( key, out var node ) ) //// 기존에 값이 존재하지 않음.
        {
            while ( _timeline.Count >= Capacity )
            {
                node = _timeline.Last!; // 캐시 크기가 넘치면, 기존의 node를 재활용한다.
                _timeline.RemoveLast();
                _index.Remove( node.Value.Key );
            }

            if ( node is not null )
            {
                node.Value = ( key, element );
            }
            else
            {
                node = new( ( key, element ) );
            }

            _index.Add( key, node );
            insert = true;
        }
        else //// 기존에 값이 존재했다면 timeline을 최신화 해준다.
        {
            _timeline.Remove( node );
        }

        _timeline.AddFirst( node );
        return insert;
    }

    public bool Upsert( TKey key, TElement element )
    {
        bool insert = false;
        if ( _index.TryGetValue( key, out var node ) == false ) //// 기존에 값이 존재하지 않음.
        {
            while ( _timeline.Count >= Capacity ) //// delete least recently used element
            {
                node = _timeline.Last!; // 캐시 크기가 넘치면, 기존의 node를 재활용한다.
                _timeline.RemoveLast();
                _index.Remove( node.Value.Key );
            }

            insert = true;
        }
        else //// 기존에 값이 존재했다면 timeline을 최신화 해준다.
        {
            _timeline.Remove( node );
        }

        if ( node is not null )
        {
            node.Value = (key, element);
        }
        else
        {
            node = new( ( key, element ) );
        }

        _timeline.AddFirst( node );
        _index[ key ] = node;
        return insert;
    }

    public void Remove( TKey key )
    {
        if ( !_index.TryGetValue( key, out var node ) ) //// 기존에 값이 존재하지 않음.
        {
            return;
        }

        _timeline.Remove( node );
        _index.Remove( key );
    }

}
