﻿using Google.Protobuf.WellKnownTypes;
using Mala.Core;
using System;
using System.Runtime.InteropServices;
using ZstdSharp;

namespace Mala.Collection;

/// <summary>
/// Fisher-Yates 셔플 알고리즘
/// </summary>
public static class CollectionFastShuffle
{
    public static void Shuffle< T >( this IList< T > list )
    {
        int count    = list.Count;
        int capacity = count;

        while ( count > 1 )
        {
            count -= 1;
            int k = Random.Shared.Next() % capacity;

            // Swap
            ( list[ k ], list[ count ] ) = ( list[ count ], list[ k ] );
        }
    }
}

/// <summary>
/// Array Clear
/// </summary>
public static class ArrayClear
{
    public static void Clear< T >( this T[] array )
    {
        for ( int n = 0; n < array.Length; n+= 1 )
        {
            array[ n ] = default( T );
        }
    }
}

/// <summary>
/// ForEach 결과
/// </summary>
public enum EForEachResult
{
    /// <summary>
    /// 계속
    /// </summary>
    Continue,

    /// <summary>
    /// 중단
    /// </summary>
    Break
}

/// <summary>
///
/// </summary>
public static class CollectionForEach
{
    /// <summary>
    /// 원소를 순회한다
    /// </summary>
    public static void ForEach< T >( this IList< T > list, Func< T, EForEachResult > action )
    {
        if ( action is null )
            return;

        for ( int i = 0; i < list.Count; i++ )
        {
            var result = action( list[ i ] );
            if ( result == EForEachResult.Break )
                return;
        }
    }
}

/// <summary>
/// 컬렉션 랜덤 원소 반환
/// </summary>
public static class CollectionRandomValue
{
    public static bool TryGetRandomKeyValue< TKey, TValue >(
        this Dictionary< TKey, TValue > dict,
        out TKey? key,
        out TValue? value )
    {
        if ( dict is null || dict.Count == 0 )
        {
            ( key, value ) = ( default( TKey ), default( TValue ) );
            return false;
        }

        var randomIndex = Random.Shared.Next( dict.Count );
        ( key, value ) = dict.ElementAt( randomIndex );
        return true;
    }

    /// <summary>
    /// 랜덤 값을 획득 시도한다
    /// </summary>
    public static bool TryGetRandomValue< TKey, TValue >(
        this Dictionary< TKey, TValue > dict,
        out TValue value )
    {
        if ( dict is null || !dict.Any() )
        {
            value = default;
            return false;
        }

        var randomIndex = Random.Shared.Next( dict.Count );
        value = dict.ElementAt( randomIndex ).Value;
        return true;
    }

    /// <summary>
    /// 랜덤 값을 획득 시도한다
    /// </summary>
    public static bool TryGetRandomValue< TValue >( this ICollection< TValue > set, out TValue value )
    {
        if ( set is null || !set.Any() )
        {
            value = default;
            return false;
        }

        var randomIndex = Random.Shared.Next( set.Count );
        value = set.ElementAt( randomIndex );
        return true;
    }
    /// <summary>
    /// 랜덤 키를 획득 시도한다
    /// </summary>
    public static bool TryGetRandomKey< TKey, TValue >(
        this Dictionary< TKey, TValue > dict,
        out TKey key )
    {
        if ( dict is null || dict.Count == 0 )
        {
            key = default;
            return false;
        }

        var randomIndex = Random.Shared.Next( dict.Count );
        key = dict.ElementAt( randomIndex ).Key;
        return true;
    }
}

/// <summary>
/// 컬렉션 마지막 원소 추가 및 레퍼런스 반환
/// </summary>
public static class ListReserve
{
    public static ref T ReserveOneItem< T >( this List< T > list ) where T : new()//, strcut
    {
        list.Add( new T() );
        return ref CollectionsMarshal.AsSpan( list )[ list.Count - 1 ];
    }

    public static Span< T > AsSpan< T >( this List< T > list ) //where T : new()//, strcut
    {
        return CollectionsMarshal.AsSpan( list );
    }

}