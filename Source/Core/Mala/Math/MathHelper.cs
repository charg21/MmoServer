﻿using System.Runtime.InteropServices;

namespace Mala.Math;

/// <summary>
///
/// </summary>
static public partial class MathHelper
{
    public static float Rad2Deg( float radians )
        => radians / MathUnit.PI * 180f;

    public static float Deg2Rad( float degree )
        => degree * MathUnit.PI / 180f;

    public static float Cos( float value, float max )
    {
        if ( max < value )
            return max;

        return value;
    }

    public static long PowOfTwo( int exponent )
    {
        if ( exponent == 0 )
            return 1;
        else
            return 1 << exponent;
    }

    public static bool TryGetExponentIfPowOfTwo( ulong n, out int exponent )
    {
        exponent = 0;
        if ( n <= 0 )
            return false;

        while ( ( 1UL << exponent ) < n )
            exponent += 1;

        return ( 1UL << exponent ) == n;
    }

    public static float Clamp( float min, float value, float max )
    {
        if ( min > value )
            return min;

        if ( max < value )
            return max;

        return value;
    }

    public static float Vector2ToYaw( Vector2 v )
    {
        var yawRadian = (float)( System.Math.Atan2( v.y, v.x ) );
        return Rad2Deg( yawRadian );
    }

    public static Vector2 YawToVector2( float yaw )
    {
        var yawRadian = Deg2Rad( yaw );
        return new Vector2( MathF.Cos( yawRadian ), MathF.Sin( yawRadian ) );
    }

    // ref: https://github.com/xamarin/xamarin-macios/blob/main/src/OpenGL/OpenTK/Math/MathHelper.cs#L177
    public static float InverseSqrtFast( float x )
    {
        unsafe
        {
            float xhalf = 0.5f * x;
            int i = *(int*) &x;               // Read bits as integer.
            i = 0x5f375a86 - ( i >> 1 );      // Make an initial guess for Newton-Raphson approximation
            x = *(float*)&i;                  // Convert bits back to float
            x = x * ( 1.5f - xhalf * x * x ); // Perform left single Newton-Raphson step.
            return x;
        }
    }

    /// <summary>
    ///
    /// </summary>
    public static float InverseSqrt( float value )
        => 1.0f / (float)( System.Math.Sqrt( value ) );

    public static double InverseSqrtFast( double x )
        => InverseSqrtFast( (float)( x ) );

    /// <summary>
    /// 벡터를 회전한다.
    /// </summary>
    public static Vector3 Rotate( ref Vector3 v, float radian )
    {
        float cosAngle = MathF.Cos( radian );
        float sinAngle = MathF.Sin( radian );

        return new
        (
            v.x * cosAngle - v.z * sinAngle,
#if UNREAL
        // 유니티에서는 Y축을 기준으로 회전
            v.y,
            v.x * sinAngle + v.z * cosAngle
#else
        // 언리얼에서는 Z축을 기준으로 회전
            v.x * sinAngle + v.y * cosAngle,
            v.z
#endif
        );
    }


    /// <summary>
    /// 두 벡터 간의 2D 거리를 반환합니다.
    /// </summary>
    public static float GetDistance2D( in Vector3 lhs, in Vector3 rhs )
        => MathF.Sqrt( GetDistance2DSquard( in rhs, in rhs ) );

    /// <summary>
    /// 두 벡터 간의 2D 거리 제곱을 반환합니다.
    /// </summary>
    public static float GetDistance2DSquard( in Vector3 lhs, in Vector3 rhs )
    {
        float dx = lhs.x - rhs.x;
#if UNITY
        float dz = lhs.z - rhs.z;

        return dx * dx + dz * dz;
#else
        float dy = lhs.y - rhs.y;

        return dx * dx + dy * dy;
#endif
    }
}
