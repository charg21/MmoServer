﻿using System;

namespace Mala.Math;

[System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Explicit )]
public struct Vector2
{
    [System.Runtime.InteropServices.FieldOffset( 0 )]
    public i64 value;

    [System.Runtime.InteropServices.FieldOffset( 0 )]
    public float x;
    [System.Runtime.InteropServices.FieldOffset( 4 )]
    public float y;

    public static readonly Vector2 Zero     = new(  0,  0 );
    public static readonly Vector2 One      = new(  1,  1 );
    public static readonly Vector2 MinusOne = new( -1, -1 );
    public static readonly Vector2 Right    = new(  1,  0 );
    public static readonly Vector2 Left     = new( -1,  0 );
    public static readonly Vector2 Up       = new(  0,  1 );
    public static readonly Vector2 Down     = new(  0, -1 );

    public Vector2( float x, float y )
    {
        this.x = x;
        this.y = y;
    }

    public static Vector2 operator +( Vector2 p1, Vector2 p2 )
        => new( p1.x + p2.x, p1.y + p2.y );

    public static Vector2 operator -( Vector2 p1, Vector2 p2 )
        => new( p1.x - p2.x, p1.y - p2.y );

    public static Vector2 operator *( Vector2 p1, Vector2 p2 )
        => new( p1.x * p2.x, p1.y * p2.y );

    public static Vector2 operator *( Vector2 p1, float p2 )
        => new( p1.x * p2, p1.y * p2 );

    public static Vector2 operator *( float p1, Vector2 p2 )
        => new( p1 * p2.x, p1 * p2.y );

    public static Vector2 operator /( Vector2 p1, Vector2 p2 )
        => new( p1.x / p2.x, p1.y / p2.y );

    public static Vector2 operator /( Vector2 p1, int scala )
        => new( p1.x / scala, p1.y / scala );

    public static Vector2 operator /( int scala, Vector2 p2 )
        => new( scala / p2.x, scala / p2.y );

    public static bool operator ==( Vector2 p1, Vector2 p2 )
        => p1.x == p2.x && p1.y == p2.y;

    public static bool operator !=( Vector2 p1, Vector2 p2 )
        => p1.x != p2.x || p1.y != p2.y;

    static Vector2 FromYaw( float yawDegree )
        => MathHelper.YawToVector2( yawDegree );

    public float Size() => (float)( System.Math.Sqrt( x * x + y * y ) );
    public float Magnitude() => (float)( System.Math.Sqrt( x * x + y * y ) );
    public float SizeSquared() => x * x + y * y;
    public float SqrMagnitude() => x * x + y * y;

    public static float Dot( Vector2 lhs, Vector2 rhs )
        => ( lhs.x * rhs.x ) + ( lhs.y * rhs.y );

    public static float Distance( Vector2 a, Vector2 b )
    {
        float diff_x = a.x - b.x;
        float diff_y = a.y - b.y;

        return (float)( System.Math.Sqrt( diff_x * diff_x + diff_y * diff_y ) );
    }

    public static Vector2 Max( Vector2 lhs, Vector2 rhs )
        => new( System.Math.Max( lhs.x, rhs.x ), System.Math.Max( lhs.y, rhs.y ) );

    public static Vector2 Min( Vector2 lhs, Vector2 rhs )
        => new( System.Math.Min( lhs.x, rhs.x ), System.Math.Min( lhs.y, rhs.y ) );


    ///
    public override readonly string ToString() => $"{ x }, { y }";

    /// <summary>
    /// Vector3로 변환한다.
    /// </summary>
    /// <returns></returns>
    public readonly Vector3 ToVector3()
    {
#if UNITY
        return new( x, 0, y );
#else
        // 언리얼에서는 Z축을 기준으로 회전
        return new( x, y, 0 );
#endif
    }
}
