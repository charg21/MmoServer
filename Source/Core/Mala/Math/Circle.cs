﻿#define UNITY

namespace Mala.Math;

public struct Circle
{
    /// <summary>
    /// 위치
    /// </summary>
    public Vector3 center;

    /// <summary>
    /// 반지름
    /// </summary>
    public float radius = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public Circle()
    {
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public Circle( Vector3 centerPos, float radius )
    {
        center = centerPos;
        radius = radius;
    }

}
