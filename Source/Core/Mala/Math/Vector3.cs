﻿#define UNITY

using System.Runtime.CompilerServices;
using System;
using System.Runtime.InteropServices;

namespace Mala.Math;

/// <summary>
/// Ref: https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Math/Vector3.cs
/// </summary>
[System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Explicit )]
public struct Vector3
{
    [System.Runtime.InteropServices.FieldOffset( 0 )]
    public i64 _value;
    [System.Runtime.InteropServices.FieldOffset( 0 )]
    public float x;
    [System.Runtime.InteropServices.FieldOffset( 0 )]
    public float X;
#if UNITY
    [System.Runtime.InteropServices.FieldOffset( 4 )]
    public float z;
    [System.Runtime.InteropServices.FieldOffset( 4 )]
    public float Z;
    [System.Runtime.InteropServices.FieldOffset( 8 )]
    public float y;
    [System.Runtime.InteropServices.FieldOffset( 8 )]
    public float Y;
#else
    [System.Runtime.InteropServices.FieldOffset( 4 )]
    public float y;
    [System.Runtime.InteropServices.FieldOffset( 4 )]
    public float Y;
    [System.Runtime.InteropServices.FieldOffset( 8 )]
    public float z;
    [System.Runtime.InteropServices.FieldOffset( 8 )]
    public float Z;
#endif

    public static readonly Vector3 Zero     = new(  0,  0,  0 );
    public static readonly Vector3 One      = new(  1,  1,  1 );
    public static readonly Vector3 MinusOne = new( -1, -1, -1 );
    public static readonly Vector3 Right    = new(  1,  0,  0 );
    public static readonly Vector3 Left     = new( -1,  0,  0 );
    public static readonly Vector3 Up       = new(  0,  1,  0 );
    public static readonly Vector3 Down     = new(  0, -1,  0 );
    public static readonly Vector3 Forward  = new(  0,  0,  1 );
    public static readonly Vector3 Backward = new(  0,  0, -1 );

#region Ctor

    public Vector3( float x, float y, float z )
    {
#if UNITY
        this.x = x;
        this.z = z;
        this.y = y;
#else
        this.x = x;
        this.y = y;
        this.z = z;
#endif
    }

    public Vector3( Vector3 rhs )
    {
#if UNITY
        _value = rhs._value;
        y = rhs.y;
#else
        _xy = rhs._xy;
        z = rhs.z;
#endif
    }

    public Vector3( Vector2 vector, float z )
    {
        _value = vector.value;
        this.z = z;
    }

#endregion

    #region StaticOp
    public static Vector3 operator +( Vector3 p1, Vector3 p2 )
        => new( p1.x + p2.x, p1.y + p2.y, p1.z + p2.z );

    public static Vector3 operator +( Vector3 p1, Vector2 p2 )
        => new( p1.x + p2.x, p1.y + p2.y, p1.z );

    public static Vector3 operator +( Vector2 p1, Vector3 p2 )
        => new( p1.x + p2.x, p1.y + p2.y, p2.z );

    public static Vector3 operator -( Vector3 p1, Vector3 p2 )
        => new( p1.x - p2.x, p1.y - p2.y, p1.z - p2.z );

    public static Vector3 operator -( Vector3 p1, Vector2 p2 )
        => new( p1.x - p2.x, p1.x - p2.x, p1.z );

    public static Vector3 operator -( Vector2 p1, Vector3 p2 )
        => new( p1.x - p2.x, p1.x - p2.x, p2.z );

    public static Vector3 operator *( Vector3 p1, Vector3 p2 )
        => new( p1.x * p2.x, p1.y * p2.y, p1.z * p2.z );

    public static Vector3 operator *( Vector3 p1, float p2 )
        => new( p1.x * p2, p1.y * p2, p1.z * p2 );

    public static Vector3 operator *( float p1, Vector3 p2 )
        => new( p1 * p2.x, p1 * p2.y, p1 * p2.z );

    public static Vector3 operator /( Vector3 p1, Vector3 p2 )
        => new( p1.x / p2.x, p1.y / p2.y, p1.z / p2.z );

    public static Vector3 operator /( Vector3 p1, float p2 )
        => new( p1.x / p2, p1.y / p2, p1.z / p2 );

    public static Vector3 operator /( float p1, Vector3 p2 )
        => new( p1 / p2.x, p1 / p2.y, p1 / p2.z );

    public static bool operator ==( Vector3 p1, Vector3 p2 )
    {
        return p1._value == p2._value &&
#if UNITY
        p1.y == p2.y;
#else
        p1.z == p2.z;
#endif
    }

    public static bool operator ==( in Vector3 p1, in Vector3 p2 )
    {
        return p1._value == p2._value &&
#if UNITY
        p1.y == p2.y;
#else
        p1.z == p2.z;
#endif
    }

    public static bool operator !=( Vector3 p1, Vector3 p2 )
    {
        return p1._value != p2._value ||
#if UNITY
        p1.y != p2.y;
#else
        p1.z != p2.z;
#endif
    }

    public static bool operator !=( in Vector3 p1, in Vector3 p2 )
    {
        return p1._value != p2._value ||
#if UNITY
        p1.y != p2.y;
#else
        p1.z != p2.z;
#endif
    }


    #endregion

    public static Vector3 LerpUnclamped( Vector3 a, Vector3 b, float t )
        => new
        (
            a.x + ( b.x - a.x ) * t,
            a.y + ( b.y - a.y ) * t,
            a.z + ( b.z - a.z ) * t
        );

    public float Size() => (float)( System.Math.Sqrt( x * x + y * y + z * z ) );
    public float Magnitude() => (float)( System.Math.Sqrt( x * x + y * y + z * z ) );

    public float SizeSquared() => x * x + y * y + z * z;
    public float SqrMagnitude() => x * x + y * y + z * z;

    public static float Dot( Vector3 lhs, Vector3 rhs )
        => ( lhs.x * rhs.x ) + ( lhs.y * rhs.y ) + ( lhs.z * rhs.z );

    public static Vector3 Project( Vector3 vector, Vector3 onNormal )
    {
        float sqrMag = Dot( onNormal, onNormal );
        if ( sqrMag < MathUnit.Eplision )
        {
            return Zero;
        }
        else
        {
            float dot = Dot( vector, onNormal );
            return new
            (
                onNormal.x * dot / sqrMag,
                onNormal.y * dot / sqrMag,
                onNormal.z * dot / sqrMag
            );
        }
    }

    public static Vector3 Cross( Vector3 lhs, Vector3 rhs )
        => new
        (
            lhs.y * rhs.z - lhs.z * rhs.y,
            lhs.z * rhs.x - lhs.x * rhs.z,
            lhs.x * rhs.y - lhs.y * rhs.x
        );

    public Vector3 Normalize()
    {
        var squareSum = SizeSquared();
        if ( squareSum == 1f )
            return new( x, y, z );

        if ( squareSum == 0f )
            return new( 0, 0, 0 );

        //var invLength = MathUtil.InverseSqrtFast( squareSum );
        var invLength = MathHelper.InverseSqrt( squareSum );

        return new( x * invLength, y * invLength, z * invLength );
    }


    public static float Distance( Vector3 a, Vector3 b )
    {
        float diff_x = a.x - b.x;
        float diff_y = a.y - b.y;
        float diff_z = a.z - b.z;

        return (float)( System.Math.Sqrt( diff_x * diff_x + diff_y * diff_y + diff_z * diff_z ) );
    }

    public static Vector3 Max( Vector3 lhs, Vector3 rhs )
        => new
        (
            System.Math.Max( lhs.x, rhs.x ),
            System.Math.Max( lhs.y, rhs.y ),
            System.Math.Max( lhs.z, rhs.z )
        );

    public static Vector3 Min( Vector3 lhs, Vector3 rhs )
        => new
        (
            System.Math.Min( lhs.x, rhs.x ),
            System.Math.Min( lhs.y, rhs.y ),
            System.Math.Min( lhs.z, rhs.z )
        );

    ///
    public override readonly string ToString() => $"{ x }, { y }, { z }";


    public bool Write( Span< byte > s, ref ushort count )
    {
        if ( !RuntimeHelpers.IsReferenceOrContainsReferences< Vector3 >() )
        {
            Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
            count += (ushort)( Unsafe.SizeOf< Vector3 >() );
            return true;
        }

        bool success = true;

        success &= BitConverter.TryWriteBytes( s[ count.. ], x );
        count += sizeof( float );
        success &= BitConverter.TryWriteBytes( s[ count.. ], y );
        count += sizeof( float );
        success &= BitConverter.TryWriteBytes( s[ count.. ], z );
        count += sizeof( float );

        return success;
    }


    public void Read( ReadOnlySpan< byte > s, ref ushort count )
    {
        if ( !RuntimeHelpers.IsReferenceOrContainsReferences< Vector3 >() )
        {
            this = Unsafe.ReadUnaligned< Vector3 >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
            count += (ushort)( Unsafe.SizeOf< Vector3 >() );
            return;
        }

        x = BitConverter.ToSingle( s[ count.. ] );
        count += sizeof( float );
        y = BitConverter.ToSingle( s[ count.. ] );
        count += sizeof( float );
        z = BitConverter.ToSingle( s[ count.. ] );
        count += sizeof( float );
    }

}
