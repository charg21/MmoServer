﻿namespace Mala.Math;

/// <summary>
///
/// </summary>
static public partial class MathUnit
{
    public const float PI          = System.MathF.PI;
    public const float Eplision    = float.Epsilon;
    public const float TwoPI       = 2f * PI;
    public const float HalfPI      = PI / 2;
    public const float InvPI       = 0.31830988618f;
    public const float SmallNumber = 1e-8f;
    public const float Deg2Rad     = PI * 2F / 360F;
    public const float Rad2Deg     = 1F / Deg2Rad;
}
