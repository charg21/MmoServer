﻿#define UNITY

namespace Mala.Math;

/// <summary>
/// 호
/// </summary>
public struct Arc
{
    /// <summary>
    /// 위치
    /// </summary>
    public Vector3 center;

    /// <summary>
    /// 방향
    /// </summary>
    public Vector3 direction;

    /// <summary>
    /// 반경
    /// </summary>
    public float radius;

    /// <summary>
    /// 각도( 360도 기준 )
    /// </summary>
    public float angle;
}
