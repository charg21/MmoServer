﻿namespace Mala.Math;

using Rect = Box;

/// <summary>
/// 사각형을 나타내는 객체( AABB )
/// </summary>
public struct Box
{
    public float left   = 0f;
    public float right  = 0f;
    public float bottom = 0f;
    public float top    = 0f;

    /// <summary>
    /// 생성자
    /// </summary>
    public Box()
    {
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public Box( float l, float r, float b, float t )
    {
        this.left   = l;
        this.right  = r;
        this.bottom = b;
        this.top    = t;
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public Box( Vector2 pos, float width, float height )
    {
        this.left   = pos.x - ( width / 2 );
        this.right  = pos.x + ( width / 2 );
        this.bottom = pos.y - ( height / 2 );
        this.top    = pos.y + ( height / 2 );
    }

    /// <summary>
    /// AABB, 다른 직사각형과 겹치는지( 충돌 ) 여부를 반환한다
    /// </summary>
    public bool IsOverlapped( ref Box rhs ) => IsOverlapped( ref this, ref rhs );

    /// <summary>
    /// AABB, 다른 직사각형과 겹치는지( 충돌 ) 여부를 반환한다
    /// </summary>
    static bool IsOverlapped( ref Box lhs, ref Box rhs )
    {
        if ( lhs.left > rhs.right )
            return false;

        if ( lhs.right < rhs.left )
            return false;

        if ( lhs.top < rhs.bottom )
            return false;

        if ( lhs.bottom > rhs.top )
            return false;

        return true;
    }
}