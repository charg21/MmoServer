﻿#define UNITY

namespace Mala.Math;

/// <summary>
/// 실린더
/// </summary>
public struct Cylinder
{
    /// <summary>
    /// 위치
    /// </summary>
    public Vector3 center;

    /// <summary>
    /// 반지름
    /// </summary>
    public float radius = 0;

    /// <summary>
    /// 높이( 사실.. 아직 사용 안함 )
    /// </summary>
    public float height = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public Cylinder()
    {
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public Cylinder( Vector3 centerPos, float radius, float height = 1.0f )
    {
        center = centerPos;
        this.radius = radius;
        this.height = height;
    }
}
