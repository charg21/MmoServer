﻿namespace Mala.Math;

/// <summary>
/// 직사각형을 나타내는 객체
/// </summary>
public struct BoxInt
{
    public int left   = 0;
    public int right  = 0;
    public int bottom = 0;
    public int top    = 0;

    /// <summary>
    /// 생성자
    /// </summary>
    public BoxInt()
    { 
    }

    /// <summary>
    /// 생성자
    /// </summary>
    public BoxInt( int l, int r, int b, int t )
    {
        left   = l;
        right  = r;
        bottom = b;
        top    = t;
    }

    /// <summary>
    /// AABB, 다른 직사각형과 겹치는지( 충돌 ) 여부를 반환한다
    /// </summary>
    public bool IsOverlapped( ref BoxInt rhs ) => IsOverlapped( ref this, ref rhs );

    /// <summary>
    /// AABB, 다른 직사각형과 겹치는지( 충돌 ) 여부를 반환한다
    /// </summary>
    static bool IsOverlapped( ref BoxInt lhs, ref BoxInt rhs )
    {
        if ( lhs.left > rhs.right )
            return false;

        if ( lhs.right < rhs.left )
            return false;

        if ( lhs.top < rhs.bottom )
            return false;

        if ( lhs.bottom > rhs.top )
            return false;

        return true;
    }
}
