﻿namespace Mala.Math;

public struct Vector2Int
{
    public int x;
    public int y;

    public static readonly Vector2Int Zero     = new(  0,  0 );
    public static readonly Vector2Int One      = new(  1,  1 );
    public static readonly Vector2Int MinusOne = new( -1, -1 );
    public static readonly Vector2Int Right    = new(  1,  0 );
    public static readonly Vector2Int Left     = new( -1,  0 );
    public static readonly Vector2Int Up       = new(  0,  1 );
    public static readonly Vector2Int Down     = new(  0, -1 );

    /// <summary>
    /// 생성자
    /// </summary>
    public Vector2Int( int x, int y )
    {
        this.x = x;
        this.y = y;
    }

    public static Vector2Int operator +( Vector2Int p1, Vector2Int p2 )
        => new( p1.x + p2.x, p1.y + p2.y );

    public static Vector2Int operator -( Vector2Int p1, Vector2Int p2 )
        => new( p1.x - p2.x, p1.y - p2.y );

    public static Vector2Int operator *( Vector2Int p1, Vector2Int p2 )
        => new( p1.x * p2.x, p1.y * p2.y );

    public static Vector2Int operator *( Vector2Int p1, int scala )
        => new( p1.x * scala, p1.y * scala );

    public static Vector2Int operator *( int scala, Vector2Int p2 )
        => new( scala * p2.x, scala * p2.y );

    public static Vector2Int operator /( Vector2Int p1, Vector2Int p2 )
    => new( p1.x / p2.x, p1.y / p2.y );

    public static Vector2Int operator /( Vector2Int p1, int scala )
        => new( p1.x / scala, p1.y / scala );

    public static Vector2Int operator /( int scala, Vector2Int p2 )
        => new( scala / p2.x, scala / p2.y );
    public static bool operator ==( Vector2Int p1, Vector2Int p2 )
        => p1.x == p2.x && p1.y == p2.y;
    public static bool operator !=( Vector2Int p1, Vector2Int p2 )
        => p1.x != p2.x || p1.y != p2.y;

    ///
    public override readonly string ToString() => $"( { x }, { y } )";
}
