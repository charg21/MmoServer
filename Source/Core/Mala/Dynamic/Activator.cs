﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mala.Dynamic;

using System;

/// <summary>
/// 런타임에 IL Code를 newobj 코드를 생성해 캐싱
/// Ref. https://codingsolution.wordpress.com/2013/07/12/activator-createinstance-is-slow/, StudioBesideEngine
/// </summary>
public static class Activator< T >
{
    static Activator() 
        => CreateInstance = GetConstructorDelegate< T >();

    public static Func< T > CreateInstance { get; private set; }

    private static Func< TBase > GetConstructorDelegate< TBase >()
        => ( Func< TBase > )TypeExt.GetConstructorDelegate( typeof( TBase ), typeof( Func< TBase > ) );
}