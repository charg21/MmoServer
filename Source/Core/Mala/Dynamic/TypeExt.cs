﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TypeConstructors = System.Collections.Concurrent.ConcurrentDictionary< System.Type, System.Func< object > >;


namespace Mala.Dynamic;


/// <summary>
/// Ref. https://codingsolution.wordpress.com/2013/07/12/activator-createinstance-is-slow/
/// </summary>
public static class TypeExt
{
    private static readonly TypeConstructors s_constructors = new();

    public static object CreateInstance( this Type type )
    {
        if ( !s_constructors.TryGetValue( type, out var constructor ) )
        {
            constructor = type.GetConstructorDelegate();
            s_constructors.TryAdd( type, constructor );
        }

        return constructor();
    }

    internal static Delegate GetConstructorDelegate( Type type, Type delegateType )
    {
        ArgumentNullException.ThrowIfNull( type );
        ArgumentNullException.ThrowIfNull( delegateType );

        Type[] genericArguments = delegateType.GetGenericArguments();
        Type[] argTypes = genericArguments.Length > 1
                ? genericArguments.Take( genericArguments.Length - 1 ).ToArray()
                : Type.EmptyTypes;

        var constructorInfo = type.GetConstructor( argTypes );
        if ( constructorInfo is null )
        {
            if ( argTypes.Length == 0 )
            {
                throw new InvalidProgramException( $"Type '{ type.Name }' doesn't have a parameterless constructor." );
            }

            throw new InvalidProgramException( $"Type '{ type.Name }' doesn't have the requested constructor." );
        }

        var dynamicMethod = new DynamicMethod( $"DM$_{ type.Name }",
            type,
            argTypes,
            type );
        var ilGen = dynamicMethod.GetILGenerator();
        for ( int i = 0; i < argTypes.Length; i++ )
        {
            /// i 번째 매개변수의 값을 스택에 넣는다.
            ilGen.Emit( OpCodes.Ldarg, i );
        }

        /// 객체의 생성자를 호출한다.( 이미 스택에 생성자의 매개 변수가 들어있다고 가정 )
        /// https://learn.microsoft.com/ko-kr/dotnet/api/system.reflection.emit.opcodes.newobj?view=net-8.0
        ilGen.Emit( OpCodes.Newobj, constructorInfo );
        /// 호출자의 계산 스택에 푸시
        /// https://learn.microsoft.com/ko-kr/dotnet/api/system.reflection.emit.opcodes.ret?view=net-8.0
        ilGen.Emit( OpCodes.Ret );
        return dynamicMethod.CreateDelegate( delegateType );
    }

    private static Func< object > GetConstructorDelegate( this Type type )
        => ( Func< object > )GetConstructorDelegate( type, typeof( Func< object > ) );
}