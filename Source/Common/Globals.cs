global using Position  = Mala.Math.Vector3;
global using Direction = Mala.Math.Vector2;
global using Index     = Mala.Math.Vector2Int;

global using f32       = System.Single;
global using f64       = System.Double;

global using i8        = System.SByte;
global using i16       = System.Int16;
global using i32       = System.Int32;
global using i64       = System.Int64;

global using u8        = System.Byte;
global using u16       = System.UInt16;
global using u32       = System.UInt32;
global using u64       = System.UInt64;

public static class Globals
{
    public const int WorkerThreadCount = 8;

    public const f32 NpcMoveSpeed      = 12f;
    public const f32 NpcAiMoveDistance = 80f;
    public const f32 DummyMoveSpeed    = 12f;
    public const f32 DummyMoveDistance = 80f;
}
