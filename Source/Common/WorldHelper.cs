﻿using Mala.Math;
using ZLogger.Entries;
using static Consts;

public static class WorldHelper
{
    /// <summary>
    /// 클러스터 접근 정책
    /// </summary>
    public static KeyValuePair< int, int >[] ClusterAccessPolicy =
    [
        new( -1, -1 ), new( -1, 0 ), new( -1, 1 ),
        new(  0, -1 ), new(  0, 0 ), new(  0, 1 ),
        new(  1, -1 ), new(  1, 0 ), new(  1, 1 ),
    ];

    public static int ToClusterSclarIndex( int x, int y )
    {
        return ( y * Consts.WideCluster.MaxCol ) + x;
    }

    public static Index ToWideClusterIndex( Position pos )
    {
        var clusterX = Math.Min( ( pos.x / (float)( Consts.WideCluster.Height ) ), (float)( Consts.WideCluster.MaxCol ) );
        var clusterZ = Math.Min( ( pos.z / (float)( Consts.WideCluster.Width  ) ), (float)( Consts.WideCluster.MaxRow ) );

        return new( (int)clusterX, (int)clusterZ );
    }

    public static Index ToClusterIndex( Position pos )
    {
        var aoiClusterX = Math.Min( ( (int)pos.x / (int)( Consts.Cluster.Height ) ), (int)( Consts.Cluster.MaxCol ) );
        var aoiClusterZ = Math.Min( ( (int)pos.z / (int)( Consts.Cluster.Width  ) ), (int)( Consts.Cluster.MaxRow ) );

        return new( (int)aoiClusterX, (int)aoiClusterZ );
    }

    public static Index GetCollisionGridIndex( Position pos )
    {
        return new( (int)( pos.x / CollisionGrid.Unit ), (int)( pos.z / CollisionGrid.Unit ) );
    }

    public static Index GetCollisionHexGridIndex( Position pos )
    {
        // 1. Vector3 포지션을 큐브 좌표계로 변환
        float q = (  2.0f / 3.0f ) * pos.x / Consts.CollisionGrid.Unit;
        float r = ( -1.0f / 3.0f ) * pos.x + ( MathF.Sqrt( 3 ) / 3.0f ) * pos.z / Consts.CollisionGrid.Unit;

        // 2. 큐브 좌표계를 정수형으로 반올림
        int rq = (int)MathF.Round( q );
        int rr = (int)MathF.Round( r );
        int rs = (int)MathF.Round( -q - r );

        // 3. 큐브 좌표계의 합이 0이 되도록 조정
        float q_diff = MathF.Abs( rq - q );
        float r_diff = MathF.Abs( rr - r );
        float s_diff = MathF.Abs( rs + q + r );

        if ( q_diff > r_diff && q_diff > s_diff )
        {
            rq = -rr - rs;
        }
        else if ( r_diff > s_diff )
        {
            rr = -rq - rs;
        }
        else
        {
            rs = -rq - rr;
        }

        // 4. 큐브 좌표계를 오프셋 좌표계로 변환
        return CubeToOffset( rq, rr, rs );
    }
    public static Index CubeToOffset( int q, int r, int s )
    {
        int x = q + ( r - ( r & 1 ) ) / 2;
        int z = r;
        return new( x, z );
    }

    /// <summary>
    /// 다음 포지션을 구한다.
    /// </summary>
    public static ( Position nextPos, Direction dir ) GetNextPos(
        Position fromPos,
        Position toPos,
        TimeSpan deltaTimeSpan,
        float    moveSpeed = Globals.NpcMoveSpeed )
    {
        Position diff = toPos - fromPos;

        // 목표 위치까지의 거리 계산
        float distanceToTarget = diff.Size();

        // 목표까지 이동하려는 계산
        float deltaTime = ( deltaTimeSpan.Ticks / 1000.0f );
        float moveAmount = moveSpeed * deltaTime;
        // 이동하는 동안 이동 양이 목표 위치까지 거리보다 크지 않도록 제한
        if ( moveAmount > distanceToTarget )
            moveAmount = distanceToTarget;

        // 이동할 거리와 방향을 고려한 이동 벡터
        Position dir = diff.Normalize();
        Position moveDir = dir * moveAmount;

        //  이동 벡터로 현재 위치를 업데이트
        Position nextPos = fromPos + moveDir;

        return ( nextPos, new Direction( dir.x, dir.z ) );
    }

    /// <summary>
    /// 다음 포지션을 구한다.
    /// </summary>
    public static Position GetNextPos( Position fromPos, EDirection direction, float moveDistance )
    {
        Vector3 unit;
        switch ( direction )
        {
        case EDirection.Up:    unit = Vector3.Forward; break;
        case EDirection.Down:  unit = Vector3.Backward; break;
        case EDirection.Right: unit = Vector3.Right; break;
        case EDirection.Left:  unit = Vector3.Left; break;
        default: return Vector3.Zero;
        }

        Position toPos = fromPos + ( unit * moveDistance );
        toPos.x = MathHelper.Clamp( 0f, toPos.x, (float)( Consts.World.Width ) - 1f );
        toPos.y = MathHelper.Clamp( 0f, toPos.y, 799f );
        toPos.z = MathHelper.Clamp( 0f, toPos.z, (float)( Consts.World.Width ) - 1f );

        return toPos;
    }

    public static bool ValidateClusterIndex( Index index )
    {
        if ( index.x < 0 || index.x >= Cluster.MaxCol )
            return false;

        if ( index.y < 0 || index.y >= Cluster.MaxRow )
            return false;

        return true;
    }
}
