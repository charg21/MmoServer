﻿using System.Text.Json.Serialization;
using System.Text.Json;

/// <summary>
/// 만분율 타입
/// </summary>
public readonly struct BasisPoint : IEquatable< BasisPoint >
{
    readonly uint value;

    public BasisPoint( uint value )
    {
        this.value = value;
    }

    public readonly uint AsPrimitive() => value;
    public static explicit operator uint( BasisPoint value ) => value.value;
    public static explicit operator BasisPoint( uint value ) => new BasisPoint( value );
    public bool Equals( BasisPoint other ) => value.Equals( other.value );
    public override int GetHashCode() => value.GetHashCode();
    public override string ToString() => value.ToString();

    /// <summary>
    /// 비교 연산자
    /// </summary>
    public static bool operator ==( in BasisPoint lhs, in BasisPoint rhs ) => lhs.value == rhs.value;
    public static bool operator !=( in BasisPoint lhs, in BasisPoint rhs ) => lhs.value != rhs.value;
    public static bool operator ==( uint lhs, in BasisPoint rhs ) => lhs == rhs.value;
    public static bool operator !=( uint lhs, in BasisPoint rhs ) => lhs != rhs.value;
    public static bool operator ==( in BasisPoint lhs, uint rhs ) => lhs.value == rhs;
    public static bool operator !=( in BasisPoint lhs, uint rhs ) => lhs.value != rhs;

    /// <summary>
    /// 박싱 발생함. 사용처 여부 확인
    /// </summary>
    public override bool Equals( object? obj )
    {
        /// 사용되는 케이스를 파악하기 위해 예외를 발생시킴
        throw new System.NotImplementedException();
        return obj is BasisPoint && Equals( (BasisPoint)obj );
    }
}

public class JsonBasisPointConverter : JsonConverter< BasisPoint >
{
    public override BasisPoint Read( ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options )
    {
        return new BasisPoint( reader.GetUInt32() );
    }

    public override void Write( Utf8JsonWriter writer, BasisPoint value, JsonSerializerOptions options )
    {
        writer.WriteNumberValue( value.AsPrimitive() );
    }
}
