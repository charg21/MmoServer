﻿
/// <summary>
/// Npc 기획 데이터( TODO: 툴에의해 생성될 코드... )
/// </summary>
public partial struct NpcDesign : IDesign
{
    public NpcDesign()
    {
    }

    public bool Initialize()
    {
        return true;
    }

    /// <summary>
    /// 스킬 식별자
    /// </summary>
    public DesignId Id { get; set; }

    /// <summary>
    /// 스킬 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;
    public int RewardId { get; set; }

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static Design< NpcDesign >? Get( DesignId npcDesignId )
    {
        if ( NpcDesignManager.Instance._designDict.TryGetValue( npcDesignId, out var index ) )
            return new( NpcDesignManager.Instance._designs, index );

        return null;
    }
}

/// <summary>
/// Npc 기획 정보( 임시 )
/// </summary>
public partial struct NpcDesign : IDesign
{
    /// <summary>
    /// 레벨
    /// </summary>
    public int Level { get; set; } = 1;

    /// <summary>
    /// 경험치
    /// </summary>
    public int Exp { get; set; } = 1;

    /// <summary>
    /// 걸음 속도
    /// </summary>
    public float WalkSpeed { get; set; } = 12f;

    /// <summary>
    /// 달리기 속도
    /// </summary>
    public float RunSpeed { get; set; } = 12f;

    /// <summary>
    /// 체력
    /// </summary>
    public int Hp { get; set; }

    /// <summary>
    /// 힘
    /// </summary>
    public int Power { get; set; }

    /// <summary>
    /// 루트 드랍 확률( 만분율 )
    /// </summary>
    public BasisPoint LootDropRate { get; set; }

}