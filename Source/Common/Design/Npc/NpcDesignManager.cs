﻿using Mala.Core;
using System.Text.Json;
using NpcDesigns = System.Collections.Generic.Dictionary< DesignId, int >;

/// <summary>
/// Npc 기획 정보 관리자
/// </summary>
public class NpcDesignManager
    : Singleton< NpcDesignManager >
    , IDesignManager< NpcDesign >
{
    /// <summary>
    /// 랜덤 억세스가 가능한 디자인 객체 배열
    /// </summary>
    public NpcDesign[] _designs;

    /// <summary>
    ///
    /// </summary>
    public NpcDesigns _designDict = new();

    /// <summary>
    /// 생성자
    /// </summary>
    public NpcDesignManager()
    {
    }

    /// <summary>
    /// 초기화한다.
    /// </summary>
    public bool Load( string jsonPath )
    {
        /// Json 파일을 로드한다.
        var jsonData = File.ReadAllText( $"{ jsonPath }\\Npc.json" );

        var option = new JsonSerializerOptions
        {
            Converters = { new JsonDesignIdConverter() },
            AllowTrailingCommas = true
        };

        /// Json 역직렬화를 진행한다.
        var designs = JsonSerializer.Deserialize< NpcDesign[] >( jsonData, option );
        _designs = GC.AllocateUninitializedArray< NpcDesign >( designs.Length, true );
        designs.AsSpan().CopyTo( _designs );

        int index = 0;
        foreach ( var design in _designs )
        {
            _designDict.Add( design.Id, index++ );
        }

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public bool Initialize()
    {
        foreach ( var ( _, design ) in _designDict )
        {
            //design.Initialize();
        }

        return true;
    }

    static NpcDesign s_emptyDesign = new();

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static ref readonly NpcDesign Ref( DesignId npcDesignId )
    {
        if ( Instance._designDict.TryGetValue( npcDesignId, out var item ) )
            return ref Instance._designs[ item ];

        return ref s_emptyDesign;
    }

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static Design< NpcDesign >? Get( DesignId itemDesignId )
    {
        if ( Instance._designDict.TryGetValue( itemDesignId, out var item ) )
            return new( Instance._designs, item );

        return null;
    }
}
