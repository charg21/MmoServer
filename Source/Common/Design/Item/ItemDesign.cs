﻿
using Mala.Math;
using System.Text.Json.Serialization;

using System.Text.Json;

/// <summary>
/// 아이템 타입
/// </summary>
public enum EItemType
{
    /// <summary>
    /// 장비
    /// </summary>
    Equipment,

    /// <summary>
    /// 포션( 소모품 )
    /// </summary>
    Potion,

    /// <summary>
    /// 기타
    /// </summary>
    Etc
}

/// <summary>
/// 아이템 특성 타입
/// </summary>
public enum EItemAttribute : ulong
{
    /// <summary>
    /// 없음
    /// </summary>
    None = 0,

    /// <summary>
    /// 중첩 가능 여부
    /// </summary>
    Stackable = 1 << 0,

    /// <summary>
    /// 잠금 가능 여부
    /// </summary>
    Lockable = 1 << 1,

    /// <summary>
    /// 판매 가능 여부
    /// </summary>
    Sellable = 1 << 2,

    /// <summary>
    /// 강화 가능 여부
    /// </summary>
    Enchantable = 1 << 3,

    /// <summary>
    /// 장착 가능 여부
    /// </summary>
    Equipable = 1 << 4,

    /// <summary>
    /// 사용 가능 여부
    /// </summary>
    Usable = 1 << 5,

    All = ulong.MaxValue,
}

/// <summary>
/// 아이템 기획 데이터
/// </summary>
public struct ItemDesign
{
    public ItemDesign()
    {
    }

    /// <summary>
    /// 스킬 식별자
    /// </summary>
    public DesignId Id { get; set; }

    /// <summary>
    /// 스킬 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 아이템 타입
    /// </summary>
    public EItemType Type { get; set; }

    /// <summary>
    /// 아이템 특성
    /// </summary>
    //[JsonConverter( typeof( JsonItemAttributeConverter ) )]
    public EItemAttribute Attribute { get; set; }

    internal void Initialize()
    {
    }

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static Design< ItemDesign >? Get( DesignId itemDesignId )
    {
        if ( ItemDesignManager.Instance._designDict.TryGetValue( itemDesignId, out var index ) )
            return new( ItemDesignManager.Instance._designs, index );

        return null;
    }

    public static ItemDesign NullDesign;

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static ref ItemDesign Ref( DesignId itemDesignId )
    {
        if ( ItemDesignManager.Instance._designDict.TryGetValue( itemDesignId, out var index ) )
            return ref ItemDesignManager.Instance._designs[ index ];

        return ref NullDesign;
    }

    public bool IsNull => Id == DesignId.Invalid;

    public bool IsStackable => ( Attribute & EItemAttribute.Stackable ) != 0;
    public bool IsLockable => ( Attribute & EItemAttribute.Lockable ) != 0;
    public bool IsUsable => ( Attribute & EItemAttribute.Usable ) != 0;
    public bool IsSellable => ( Attribute & EItemAttribute.Sellable ) != 0;
    public bool IsEnchantable => ( Attribute & EItemAttribute.Enchantable ) != 0;
    public bool IsEquipable => ( Attribute & EItemAttribute.Stackable ) != 0;
}



public class JsonItemAttributeConverter : JsonConverter< EItemAttribute >
{
    JsonSerializerOptions _newOption = new(){ IncludeFields = true };

    public override EItemAttribute Read( ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options )
    {
        if ( reader.TokenType != JsonTokenType.String )
        {
            throw new JsonException();
        }

        var enumString = reader.GetString();
        if ( Enum.TryParse( enumString, out EItemAttribute attribute ) )
        {
            return attribute;
        }

        throw new JsonException( $"Unable to convert \"{enumString}\" to {nameof( EItemAttribute )}." );
    }

    public override void Write( Utf8JsonWriter writer, EItemAttribute value, JsonSerializerOptions options )
    {
        writer.WriteStringValue( value.ToString() );
    }
}
