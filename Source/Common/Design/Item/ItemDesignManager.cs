﻿using Mala.Core;
using Mala.Logging;
using System.Text.Json;
using System.Text.Json.Serialization;
using ItemDesigns = System.Collections.Generic.Dictionary< DesignId, int >;

/// <summary>
/// 아이템 기획 데이터 관리자
/// </summary>
[DesignPath( "Item.json" )]
public class ItemDesignManager
    : Singleton< ItemDesignManager >
    , IDesignManager< ItemDesign >
{
    /// <summary>
    /// 랜덤 억세스가 가능한 디자인 객체 배열
    /// </summary>
    public ItemDesign[] _designs;

    /// <summary>
    ///
    /// </summary>
    public ItemDesigns _designDict = new();

    /// <summary>
    /// 생성자
    /// </summary>
    public ItemDesignManager()
    {
    }

    /// <summary>
    /// 로드한다.
    /// </summary>
    public bool Load( string jsonPath )
    {
        /// Json 파일을 로드한다.
        var jsonData = File.ReadAllText( $"{ jsonPath }\\Item.json" );

        var option = new JsonSerializerOptions
        {
            Converters =
            {
                new JsonDesignIdConverter() ,
                new JsonItemAttributeConverter()
            },
            AllowTrailingCommas = true,
        };

        /// Json 역직렬화를 진행한다.
        var designs = JsonSerializer.Deserialize< ItemDesign[] >( jsonData, option );
        _designs = GC.AllocateUninitializedArray< ItemDesign >( designs.Length, true );
        designs.AsSpan().CopyTo( _designs );

        int index = 0;
        foreach ( var design in _designs )
        {
            _designDict.Add( design.Id, index++ );
        }

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public bool Initialize()
    {
        foreach ( var ( _, index ) in _designDict )
        {
            _designs[ index ].Initialize();
        }

        return true;
    }

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static ref readonly ItemDesign Ref( DesignId itemDesignId )
    {
        if ( Instance._designDict.TryGetValue( itemDesignId, out var item ) )
            return ref Instance._designs[ item ];

        return ref ItemDesign.NullDesign;
    }

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static Design< ItemDesign >? Get( DesignId itemDesignId )
    {
        if ( Instance._designDict.TryGetValue( itemDesignId, out var item ) )
            return new( Instance._designs, item );

        return null;
    }

}
