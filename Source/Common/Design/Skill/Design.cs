﻿public class Design< T > where T : struct
{
    public T[] _items;
    public int _index;

    public Design( T[] items, int index )
    {
        _items = items;
        _index = index;
    }

    public ref T Ref => ref _items[ _index ];
}
