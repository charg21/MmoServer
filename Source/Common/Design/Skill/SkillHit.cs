﻿using Mala.Math;
using System.Text.Json.Serialization;
using System.Text.Json;

[System.Runtime.InteropServices.StructLayout( System.Runtime.InteropServices.LayoutKind.Explicit )]
public struct HitShape
{
    [System.Runtime.InteropServices.FieldOffset(0)]
    public Circle Circle;

    [System.Runtime.InteropServices.FieldOffset(0)]
    public Box Box;

    [System.Runtime.InteropServices.FieldOffset(0)]
    public Cylinder Cylinder;

    public HitShape()
    {
    }
}

/// <summary>
/// 스킬 피격 기획 데이터
/// </summary>
public struct SkillHit
{
    /// <summary>
    /// 타겟팅 종류
    /// </summary>
    public ESkillTargeting Targeting { get; set; }

    /// <summary>
    /// 시작 틱
    /// </summary>
    public long StartTick { get; set; }

    /// <summary>
    /// 피격 형태 타입( 논타겟팅 )
    /// </summary>
    public EHitShape Shape { get; set; }

    /// <summary>
    /// 피격 형태 정보( 논타겟팅 )
    /// </summary>
    public HitShape HitShapeData { get; set; }

    /// <summary>
    /// 각도
    /// </summary>
    public float Angle { get; set; }

    /// <summary>
    /// 거리
    /// </summary>
    public float Distance { get; set; }

    /// <summary>
    /// 값
    /// </summary>
    public int Value { get; set; }

    /// <summary>
    /// 캐스팅 시간
    /// </summary>
    public short Duration { get; set; }
}

public class JsonHitShapeConverter : JsonConverter< HitShape >
{
    JsonSerializerOptions _newOption = new(){ IncludeFields = true };

    public override HitShape Read( ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options )
    {
        if ( reader.TokenType != JsonTokenType.StartObject )
        {
            throw new JsonException();
        }

        var hitShape = new HitShape();

        while ( reader.Read() )
        {
            if ( reader.TokenType == JsonTokenType.EndObject )
            {
                return hitShape;
            }

            if ( reader.TokenType == JsonTokenType.PropertyName )
            {
                string propertyName = reader.GetString();
                reader.Read();

                switch ( propertyName )
                {
                    case "Circle":
                        hitShape.Circle = JsonSerializer.Deserialize< Circle >( ref reader, _newOption );
                        break;

                    case "Box":
                        hitShape.Box = JsonSerializer.Deserialize< Box >( ref reader, _newOption );
                        break;
                }
            }
        }

        throw new JsonException( "Invalid JSON format for HitShape." );
    }

    public override void Write( Utf8JsonWriter writer, HitShape value, JsonSerializerOptions options )
    {
    }
}
