﻿using Mala.Core;
using System.Text.Json;
using System.Text.Json.Serialization;
using SkillDesigns = System.Collections.Generic.Dictionary< DesignId, int >;

/// <summary>
/// 스킬 정보 관리자
/// </summary>
[DesignPath( "Skill.json" )]
public class SkillDesignManager
    : Singleton< SkillDesignManager >
    , IDesignManager< SkillDesign >
{
    /// <summary>
    /// 랜덤 억세스가 가능한 디자인 객체 배열
    /// </summary>
    public SkillDesign[] _designs;

    /// <summary>
    /// 해싱을 위한 인덱스 테이블
    /// </summary>
    public SkillDesigns _designDict = new();

    /// <summary>
    /// 생성자
    /// </summary>
    public SkillDesignManager()
    {
    }

    /// <summary>
    /// 초기화한다.
    /// </summary>
    public bool Load( string jsonPath )
    {
        /// Json 파일을 로드한다.
        var jsonData = File.ReadAllText( $"{ jsonPath }\\Skill.json" );
        var option = new JsonSerializerOptions
        {
            Converters =
            {
                new JsonDesignIdConverter(),
                new JsonHitShapeConverter(),
                new JsonStringEnumConverter< ESkillFire >(),
                new JsonStringEnumConverter< ESkillTargeting >(),
                new JsonStringEnumConverter< EHitShape >()
            },
            AllowTrailingCommas = true,
        };

        /// Json 역직렬화를 진행한다.
        var designs = JsonSerializer.Deserialize< SkillDesign[] >( jsonData, option );
        _designs = GC.AllocateUninitializedArray< SkillDesign >( designs.Length, true );
        designs.AsSpan().CopyTo( _designs );

        int index = 0;
        foreach ( var design in _designs )
        {
            _designDict.Add( design.Id, index++ );
        }

        return true;
    }

    /// <summary>
    ///
    /// </summary>
    public bool Initialize()
    {
        foreach ( var design in _designs )
        {
            // design.Initialize();
        }

        return true;
    }

    static SkillDesign s_emptyDesign = new();

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static ref readonly SkillDesign Ref( DesignId itemDesignId )
    {
        if ( Instance._designDict.TryGetValue( itemDesignId, out var item ) )
            return ref Instance._designs[ item ];

        return ref s_emptyDesign;
    }

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static Design< SkillDesign >? Get( DesignId itemDesignId )
    {
        if ( Instance._designDict.TryGetValue( itemDesignId, out var item ) )
            return new( Instance._designs, item );

        return null;
    }
}
