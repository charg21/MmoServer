﻿/// <summary>
/// 스킬 기획 데이터
/// </summary>
public struct SkillDesign : IDesign
{
    public SkillDesign()
    {
    }

    /// <summary>
    /// 값 수치
    /// </summary>
    public i64 Value => HitList is not null ? HitList![ 0 ].Value : 0;

    /// <summary>
    /// 사거리
    /// </summary>
    public float Distance => HitList is not null ? HitList![ 0 ].Distance : 0;

    /// <summary>
    /// 스킬 식별자
    /// </summary>
    public DesignId Id { get; set; }

    /// <summary>
    /// 스킬 이름
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// 스킬 히트 목록
    /// </summary>
    public SkillHit[]? HitList { get; set; }

    /// <summary>
    /// 쿨타임
    /// </summary>
    public float CoolTime { get; set; }

    /// <summary>
    /// 시전 시간
    /// </summary>
    public long CastingTime { get; set; }

    /// <summary>
    /// 범위
    /// </summary>
    public float Range { get; set; }
    public float RangeSqr => Range * Range;

    /// <summary>
    /// 실행 타입
    /// </summary>
    public ESkillFire FireType { get; set; }

    /// <summary>
    /// 타겟팅 타입
    /// </summary>
    public ESkillTargeting TargetingType => HitList is not null ? HitList![ 0 ].Targeting : ESkillTargeting.Target;

    /// <summary>
    /// 횟수
    /// </summary>
    public int Count { get; set; }


    public bool Initialize()
    {
        return true;
    }

    /// <summary>
    /// 정보를 획득한다.
    /// </summary>
    public static Design< SkillDesign >? Get( DesignId skillDesignId )
    {
        if ( SkillDesignManager.Instance._designDict.TryGetValue( skillDesignId, out var itemIndex ) )
            return new( SkillDesignManager.Instance._designs, itemIndex );

        return null;
    }
}


/// <summary>
/// 스킬 발동 타입 열거형
/// </summary>
public enum ESkillFire
{
    Once,
    Dot,
}

/// <summary>
/// 시전 위치
/// </summary>
public enum ESkillStage
{
    Casting,
}

/// <summary>
/// 스킬 타겟팅 타입 열거형
/// </summary>
public enum ESkillTargeting : byte
{
    /// <summary>
    /// 자신
    /// </summary>
    Self,

    /// <summary>
    /// 대상 지정
    /// </summary>
    Target,

    /// <summary>
    /// 범위
    /// </summary>
    Range,
}

/// <summary>
/// 스킬 발동 타입 열거형
/// </summary>
public enum EHitShape : byte
{
    /// <summary>
    /// 없음
    /// </summary>
    None,

    /// <summary>
    /// 원
    /// </summary>
    Circle,

    /// <summary>
    /// 삼각형
    /// </summary>
    Triangle,

    /// <summary>
    /// 사각형
    /// </summary>
    Rect,

    /// <summary>
    /// 도넛, 원환면
    /// </summary>
    Donut
}
