﻿using System.Text.Json.Serialization;
using System.Text.Json;

/// <summary>
/// 기획 데이터 식별자
/// ref. https://www.meziantou.net/strongly-typed-ids-with-csharp-source-generators.htm
/// </summary>
public readonly struct DesignId : IEquatable< DesignId >
{
    readonly uint value;
    internal static readonly DesignId Invalid = (DesignId)( 0 );

    public DesignId( uint value )
    {
        this.value = value;
    }

    public readonly uint AsPrimitive() => value;
    public static explicit operator uint( DesignId value ) => value.value;
    public static explicit operator DesignId( uint value ) => new DesignId( value );
    public bool Equals( DesignId other ) => value.Equals( other.value );
    public override int GetHashCode() => value.GetHashCode();
    public override string ToString() => value.ToString();

    /// <summary>
    /// 비교 연산자
    /// </summary>
    public static bool operator ==( in DesignId lhs, in DesignId rhs ) => lhs.value == rhs.value;
    public static bool operator !=( in DesignId lhs, in DesignId rhs ) => lhs.value != rhs.value;
    public static bool operator ==( uint lhs, in DesignId rhs ) => lhs == rhs.value;
    public static bool operator !=( uint lhs, in DesignId rhs ) => lhs != rhs.value;
    public static bool operator ==( in DesignId lhs, uint rhs ) => lhs.value == rhs;
    public static bool operator !=( in DesignId lhs, uint rhs ) => lhs.value != rhs;

    /// <summary>
    /// 박싱 발생함. 사용처 여부 확인
    /// </summary>
    public override bool Equals( object? obj )
    {
        /// 사용되는 케이스를 파악하기 위해 예외를 발생시킴
        throw new System.NotImplementedException();
        return obj is DesignId && Equals( (DesignId)obj );
    }
}

public class JsonDesignIdConverter : JsonConverter< DesignId >
{
    public override DesignId Read( ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options )
    {
        return new DesignId( reader.GetUInt32() );
    }

    public override void Write( Utf8JsonWriter writer, DesignId value, JsonSerializerOptions options )
    {
        writer.WriteNumberValue( value.AsPrimitive() );
    }
}
