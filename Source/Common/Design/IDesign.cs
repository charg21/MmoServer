﻿using Mala.Core;
using System.Text.Json;

/// <summary>
/// 디자인 인터페이스
/// </summary>
public interface IDesign
{
    /// <summary>
    /// 초기화한다.
    /// </summary>
    public bool Initialize();
}
