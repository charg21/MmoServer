﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class DesignPathAttribute : System.Attribute
{
    /// <summary>
    /// 경로
    /// </summary>
    public string Path { get; private set; } = string.Empty;

    public DesignPathAttribute( string path )
    {
        Path = path;
    }
}
