﻿/// <summary>
/// 디자인 매니저 최상위 인터페이스
/// </summary>
public interface IDesignManagerBase
{
    /// <summary>
    /// 초기화한다.
    /// </summary>
    public bool Initialize();

    /// <summary>
    /// 데이터를 로드한다.
    /// </summary>
    bool Load( string path );
}

/// <summary>
/// 디자인 매니저 인터페이스
/// </summary>
public interface IDesignManager< TDesign > : IDesignManagerBase
    where TDesign : struct
{
    ///// <summary>
    /////
    ///// </summary>
    //public ref readonly TDesign Ref( DesignId designId );
}
