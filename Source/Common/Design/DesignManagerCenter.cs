﻿using Mala.Logging;
using Mala.Threading;
using System.Reflection;

/// <summary>
///
/// </summary>
public static partial class DesignManagerCenter
{
    /// <summary>
    ///
    /// </summary>
    public static List< IDesignManagerBase > _designManagers;

    /// <summary>
    ///
    /// </summary>
    public static bool Load( string path )
    {
        PrepareManagerList();

        bool sucessAll = true;
        PostDesignManagerTask( designManager =>
        {
            if ( !designManager.Load( path ) )
            {
                Log.Error( $"{ nameof( designManager ) } Load Failed..  Path:{ path }" );
                sucessAll = false;
            }
            else
            {
                Log.Info( "Load OK" );
            }
        } );

        return sucessAll;
    }

    /// <summary>
    /// 초기화한다.
    /// </summary>
    public static bool Initialize()
    {
        bool sucessAll = true;
        PostDesignManagerTask( designManager =>
        {
            if ( !designManager.Initialize() )
            {
                Log.Error( $"{ nameof( designManager ) } Init Failed.." );
                sucessAll = false;
            }
        } );

        Log.Info( "DesignManagerCneter Init OK" );

        return sucessAll;
    }
    private static void PostDesignManagerTask( Action< IDesignManagerBase > job )
    {
        var disignTasks = new List< Task >();

        foreach ( var designManager in _designManagers )
        {
            disignTasks.Add(
                 Task.Run( () => job( designManager ) ) );
        }

        try
        {
            Task.WaitAll( disignTasks );
        }
        catch ( Exception e )
        {
            Log.Error( e.Message );
        }
    }
}

/// <summary>
/// TODO: 툴에 분리할 부분
/// </summary>
public static partial class DesignManagerCenter
{
    /// <summary>
    /// 추후 툴로 분리
    /// </summary>
    private static void PrepareManagerList()
    {
        _designManagers =
        [
            SkillDesignManager.Instance,
            NpcDesignManager.Instance,
            ItemDesignManager.Instance,
        ];
    }
}
