﻿public static class ThreadManager
{
    static Lock _lock = new();

    static List< Thread > _threads = new( 32 );

    /// <summary>
    /// 실행한다.
    /// </summary>
    public static void Launch( Action threadJob, string name, int count = 1 )
    {
        for ( int id = 0; id < count; id += 1 )
        {
            Launch( new( _ => threadJob() ), $"{name}_{id}" );
        }
    }

    /// <summary>
    /// 실행한다.
    /// </summary>
    public static void Launch( Thread thread, string name )
    {
        lock ( _lock )
        {
            _threads.Add( thread );
        }

        thread.Name     = name;
        thread.Priority = ThreadPriority.Highest;
        thread.Start();
    }

}
