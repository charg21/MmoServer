
using Google.Protobuf.WellKnownTypes;
using System.Runtime.CompilerServices;

/// <summary>
/// 오브젝트 식별자
/// 각 타입별 발급되는 식별자
/// </summary>
public readonly struct ObjectId : IEquatable< ObjectId >
{
    readonly ulong value;

    public ObjectId( ulong value )
    {
        this.value = value;
    }

    public readonly ulong AsPrimitive() => value;

    public static explicit operator ulong( ObjectId rhs ) => rhs.value;
    public static explicit operator ObjectId( ulong value ) => new ObjectId( value );
    public bool Equals( ObjectId rhs ) => value.Equals( rhs.value );
    public override int GetHashCode() => value.GetHashCode();
    public override string ToString() => value.ToString();

    /// <summary>
    /// 비교 연산자
    /// </summary>
    public static bool operator ==( in ObjectId lhs, in ObjectId rhs ) => lhs.value == rhs.value;
    public static bool operator !=( in ObjectId lhs, in ObjectId rhs ) => lhs.value != rhs.value;
    public static bool operator ==( ulong lhs, in ObjectId rhs ) => lhs == rhs.value;
    public static bool operator !=( ulong lhs, in ObjectId rhs ) => lhs != rhs.value;
    public static bool operator ==( in ObjectId lhs, ulong rhs ) => lhs.value == rhs;
    public static bool operator !=( in ObjectId lhs, ulong rhs ) => lhs.value != rhs;


    /// <summary>
    /// 상위 4비트( 가변 )로 액터 타입 변환
    /// </summary>
    public EWorldObjectType ObjectType => (EWorldObjectType)( value >> 60 );

    /// <summary>
    /// 특정 오브젝트 타입 여부를 반환함
    /// </summary>
    public bool IsPc => ObjectType == EWorldObjectType.Pc;
    public bool IsNpc => ObjectType == EWorldObjectType.Npc;
    public bool IsGadget => ObjectType == EWorldObjectType.Gadget;
    public bool IsPojectile => ObjectType == EWorldObjectType.Projectile;

    public override bool Equals( object obj )
    {
        return obj is ObjectId && Equals( (ObjectId)obj );
    }
}


public class WorldObjectIdHelper
{
    /// <summary>
    /// 최소 4종 3bit로 표현( 현재 기준... )
    /// </summary>
    enum WorldObjectTypeId : ulong
    {
        Pc = 0,
        Npc = 1,
        Gadget = 2,
        Projectile = 3,
    }

    const long ObjectTypeMax = 1 << 3;  // 2 ^ 3  = 8   ( Pc, Npc, Gadget, Projectile, Rserved... )
    const long YearMax       = 1 << 3;  // 2 ^ 3  = 8   ( 8년 )
    const long MonthMax      = 1 << 4;  // 2 ^ 4  = 16  ( 16개월 )
    const long DayMax        = 1 << 5;  // 2 ^ 5  = 32  ( 32일 )

    const long HourMax     = 1 << 5;  // 2 ^ 5  = 32  ( 32시간 )
    const long MinuteMax   = 1 << 6;  // 2 ^ 6  = 64  ( 64분 )
    const long SecondMax   = 1 << 6;  // 2 ^ 6  = 64  ( 64초 )
    const long MillisecMax = 1 << 9;  // 2 ^ 9  = 512 ( 1024 / 2 = 512 밀리초 )

    const long ThreadIdMax = 1 << 7;  // 2 ^ 7  = 128   ( 128 스레드 )
    const long IdMax       = 1 << 19; // 2 ^ 16 = 65536 ( 시퀀스 )

    const long IT = ThreadIdMax | IdMax;
    const long ITM = IT |  MillisecMax;
    const long ITMS = ITM | SecondMax;
    const long ITMSM = ITMS | MinuteMax;
    const long ITMSSMH = ITMSM | HourMax;
    const long ITMSSMHD = DayMax | ITMSSMH;
    const long ITMSSMHDY = ITMSSMHD | YearMax;

    const int YearOrigin = 2024;

    /// <summary>
    /// 쓰레드 로컬 ID 발급기
    /// </summary>
    [ThreadStatic]
    static long _idIssuer = 0;

    /// <summary>
    /// 스레드별 고유 식별자
    /// </summary>
    [ThreadStatic]
    static int _threadId = 0;
    static int _threadIdIssuer = 0;

    /// <summary>
    /// 쓰레드 로컬 마지막 리셋 틱
    /// </summary>
    [ThreadStatic]
    static long _lastResetTickWithoutMsPerThread;

    public static ObjectId Next( EWorldObjectType actorType )
    {
        long id = NextBase( actorType );
        id |= ( _idIssuer++ ) % IdMax;

        return new( (ulong)id );
    }

    /// <summary>
    /// 공용으로 사용되는
    /// </summary>
    private static long NextBase( EWorldObjectType actorType )
    {
        var now = DateTime.UtcNow;
        var actorTypeId = WorldObjectIdConverter.ToId( actorType );

        long id = (long)actorTypeId  << 61; // 상위 3
        id |= (long)( ( now.Year - YearOrigin ) % YearMax ) << 58; // 상위 3
        id |= (long)now.Month << 54; // 4
        id |= (long)now.Day << 49; // 5
        id |= (long)now.Hour << 44; // 5
        id |= (long)now.Minute << 38; // 6
        id |= (long)now.Second << 32; // 6
        id |= (long)( now.Ticks % 1000 ) << 26; // 10
        id |= ( GetThreadId() % ThreadIdMax ) << 16; // 7

        return id;
    }

    private static long GetThreadId()
    {
        if ( _threadId == 0 )
            _threadId = Interlocked.Increment( ref _threadIdIssuer );

        return _threadId;
    }
}

public class WorldObjectIdConverter
{
    internal static int ToId( EWorldObjectType actorType )
    {
        return actorType switch
        {
            EWorldObjectType.Pc => 0,
            EWorldObjectType.Npc => 1,
            EWorldObjectType.Gadget => 2,
            EWorldObjectType.Projectile => 3,

            // 예외
            _ => -1,
        };
    }
}
