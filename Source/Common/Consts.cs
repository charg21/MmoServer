
public static class Consts
{
    public const int Multiple = 5;

    public const int KB = 1024;
    public const int MB = KB * 1024;
    public const int GB = MB * 1024;

    public static class Channel
    {
        public const int DefaultId = 0;
        public const int DefaultCount = 1;
        public const int MaxCount = 4;
    }

    public static class Actor
    {
        public static readonly TimeSpan TickInterval = new( 100 );
        public const int ReviveTick = 5000; // 5초뒤 부활
        public const int Width = 800 * Multiple;

        public const int DefaultActorMapCapacity = 65536;
        public const int DefaultNpcMapCapacity = 65536;
        public const int DefaultPcMapCapacity = 10000;
    }

    public static class Stat
    {
        public const int MaxHp = 100;
        public const int DefaultPower = 50;
    }

    public static class Cluster
    {
        public const int Height       = View.Height;
        public const int Width        = View.Width;
        public const int MaxRow       = ( World.Height / Height ) + ( ( World.Height % Height > 0 ) ? 1 : 0 );
        public const int MaxCol       = ( World.Width  / Width ) + ( ( World.Width % Width > 0 ) ? 1 : 0 );
        public const int Capacity     = MaxRow * MaxCol;
        public const int DefaultNpcMapCapacity = 128;
        public const int DefaultPcMapCapacity = 256;
    }

    public static class WideCluster
    {
        public const int Height       = View.Height * 2;
        public const int Width        = View.Width  * 2;
        public const int MaxRow       = ( World.Height / Height ) + ( ( World.Height % Height > 0 ) ? 1 : 0 );
        public const int MaxCol       = ( World.Width  / Width ) + ( ( World.Width % Width > 0 ) ? 1 : 0 );
        public const int Capacity     = MaxRow * MaxCol;
        public const int DefaultNpcMapCapacity = 128;
        public const int DefaultPcMapCapacity = 256;
    }

    public static class Session
    {
        public const int TickInterval = 500;
    }

    public static class World
    {
        public const int Height = 800 * Multiple;
        public const int Width  = 800 * Multiple;

        public const int DefaultActorMapCapacity  = 65536 * 2;
        public const int DefaultNpcMapCapacity    = 65536;
        public const int DefaultPcMapCapacity     = 65536 / 2;
    }

    public static class View
    {
        public const int Fov       = 19 * Multiple;
        public const f32 FovSquard = ( 19.0f * Multiple ) * ( 19.0f * Multiple );
        public const int HalfFov   = Fov / 2;
        public const int TwoFov    = Fov * 2;
        public const int Height    = Fov;
        public const int Width     = Fov;

        public const int DefaultViewCapacity = 512;
        public const int DefaultViewGatherListCapacity = 512;
    }

    public static class Npc
    {
        public static readonly TimeSpan TickInterval = new( 100 );

        public static f32 MaxChaseDistance { get; set; } = 50;
    }

    public static class Gadget
    {
        public static readonly TimeSpan TickInterval = new( 100 );
    }

    public static class Timer
    {
        public const int SlotCapacity = 10000;
    }

    public static class CollisionGrid
    {
        public const f32 Unit = 0.4f;

        public const int Width  = (int)( (f32)World.Width  / Unit );
        public const int Height = (int)( (f32)World.Height / Unit );
    }

}
