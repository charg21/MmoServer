using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_Move : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public UInt64 _objectId; 
	public PktVector3 _pos = new(); 
	public PktVector3 _toPos = new(); 
	public Single _yaw; 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_Move()
	{
		_type = (ushort)( EPacketType.S_Move );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Move ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], _objectId );
		count   += sizeof( UInt64 );
		success &= _pos.Write( s, ref count );
		success &= _toPos.Write( s, ref count );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _yaw );
		count   += sizeof( Single );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_objectId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_pos.Read( s, ref count );
		_toPos.Read( s, ref count );
		_yaw = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_Move : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public UInt64 _objectId; 
	public PktVector3 _pos = new(); 
	public PktVector3 _toPos = new(); 
	public Single _yaw; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_Move()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_Move );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Move ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], _objectId );
			count   += sizeof( UInt64 );
			success &= _pos.Write( s, ref count );
			success &= _toPos.Write( s, ref count );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _yaw );
			count   += sizeof( Single );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		_objectId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_pos.Read( s, ref count );
		_toPos.Read( s, ref count );
		_yaw = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
	}

}
