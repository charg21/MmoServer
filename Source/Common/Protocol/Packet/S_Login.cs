using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_Login : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public UInt64 _userId; /// User Id
	public List< UInt64 > _pcIds = new(); /// Pc Id 목록

	/// <summary>
	/// 생성자
	/// </summary>
	public S_Login()
	{
		_type = (ushort)( EPacketType.S_Login );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Login ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
		count   += sizeof( ELogicResult );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _userId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _pcIds.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _pcIds.AsSpan() )
		{
			success &= BitConverter.TryWriteBytes( s[ count.. ], element );
			count   += sizeof( UInt64 );
		}
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_userId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_pcIds.Clear();
		ushort _pcIdsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_pcIds.Capacity = _pcIdsLength;
		for ( int i = 0; i < _pcIdsLength; i += 1 )
		{
			var element = BitConverter.ToUInt64( s[ count.. ] );
			count += sizeof( UInt64 );
			_pcIds.Add( element );
		}
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_Login : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public UInt64 _userId; /// User Id
	public List< UInt64 > _pcIds = new(); /// Pc Id 목록

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_Login()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_Login );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Login ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Login >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_Login >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
			count   += sizeof( ELogicResult );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _userId );
			count   += sizeof( UInt64 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _pcIds.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _pcIds.AsSpan() )
			{
				success &= BitConverter.TryWriteBytes( s[ count.. ], element );
				count   += sizeof( UInt64 );
			}
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Login >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_Login >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_userId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_pcIds.Clear();
		ushort _pcIdsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_pcIds.Capacity = _pcIdsLength;
		for ( int i = 0; i < _pcIdsLength; i += 1 )
		{
			var element = BitConverter.ToUInt64( s[ count.. ] );
			count += sizeof( UInt64 );
			_pcIds.Add( element );
		}
	}

}
