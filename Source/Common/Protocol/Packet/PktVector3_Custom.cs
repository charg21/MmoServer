/// <summary>
/// 고점 소수점 처리
/// 월드맵의 크기 4000 * 4000, 0.1단위 해상도로 직렬화 처리함
/// </summary>
public partial struct PktVector3
{
    public bool Write( Span< byte > s, ref ushort count )
    {
        bool success = true;

        success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _x * 10 ) );
        count += sizeof( ushort );
        success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _y * 10 ) );
        count += sizeof( ushort );
        success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _z * 10 ) );
        count += sizeof( ushort );
        return success;
    }

    public void Read( ReadOnlySpan< byte > s, ref ushort count )
    {
        _x = BitConverter.ToUInt16( s[ count.. ] ) / 10.0f;
        count += sizeof( ushort );
        _y = BitConverter.ToUInt16( s[ count.. ] ) / 10.0f;
        count += sizeof( ushort );
        _z = BitConverter.ToUInt16( s[ count.. ] ) / 10.0f;
        count += sizeof( ushort );
    }

}
