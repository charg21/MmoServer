public partial struct PktVector2
{
    public bool Write( Span< byte > s, ref ushort count )
    {
        bool success = true;

        success &= BitConverter.TryWriteBytes( s[ count.. ], (u16)( _x * 10 ) );
        count += sizeof( u16 );
        success &= BitConverter.TryWriteBytes( s[ count.. ], (u16)( _y * 10 ) );
        count += sizeof( u16 );
        return success;
    }

    public void Read( ReadOnlySpan< byte > s, ref ushort count )
    {
        _x = BitConverter.ToUInt16( s[ count.. ] ) / 10.0f;
        count += sizeof( u16 );
        _y = BitConverter.ToUInt16( s[ count.. ] ) / 10.0f;
        count += sizeof( u16 );
    }

}
