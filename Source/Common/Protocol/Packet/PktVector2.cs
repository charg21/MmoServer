using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktVector2
{
	public Single _x; /// 
	public Single _y; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktVector2(){}

}
