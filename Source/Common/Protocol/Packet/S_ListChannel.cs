using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_ListChannel : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public List< Int32 > _channels = new(); 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_ListChannel()
	{
		_type = (ushort)( EPacketType.S_ListChannel );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_ListChannel ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _channels.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _channels.AsSpan() )
		{
			success &= BitConverter.TryWriteBytes( s[ count.. ], element );
			count   += sizeof( Int32 );
		}
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_channels.Clear();
		ushort _channelsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_channels.Capacity = _channelsLength;
		for ( int i = 0; i < _channelsLength; i += 1 )
		{
			var element = BitConverter.ToInt32( s[ count.. ] );
			count += sizeof( Int32 );
			_channels.Add( element );
		}
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_ListChannel : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public List< Int32 > _channels = new(); 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_ListChannel()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_ListChannel );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_ListChannel ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_ListChannel >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_ListChannel >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _channels.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _channels.AsSpan() )
			{
				success &= BitConverter.TryWriteBytes( s[ count.. ], element );
				count   += sizeof( Int32 );
			}
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_ListChannel >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_ListChannel >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_channels.Clear();
		ushort _channelsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_channels.Capacity = _channelsLength;
		for ( int i = 0; i < _channelsLength; i += 1 )
		{
			var element = BitConverter.ToInt32( s[ count.. ] );
			count += sizeof( Int32 );
			_channels.Add( element );
		}
	}

}
