using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class C_Chat : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Server;

	public String _message = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public C_Chat()
	{
		_type = (ushort)( EPacketType.C_Chat );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_Chat ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		ushort _messageLen = (ushort)Encoding.Unicode.GetBytes( _message.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _messageLen );
		count += sizeof( ushort );
		count += _messageLen;
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		ushort _messageLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_message = Encoding.Unicode.GetString( s.Slice( count, _messageLen ) ); 
		count += _messageLen; 
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueC_Chat : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Server;

	public String _message = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueC_Chat()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.C_Chat );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_Chat ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_Chat >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueC_Chat >() );
		}
		else
		{
			#endif
			ushort _messageLen = (ushort)Encoding.Unicode.GetBytes( _message.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _messageLen );
			count += sizeof( ushort );
			count += _messageLen;
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_Chat >() )
		{
			this = Unsafe.ReadUnaligned< ValueC_Chat >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		ushort _messageLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_message = Encoding.Unicode.GetString( s.Slice( count, _messageLen ) ); 
		count += _messageLen; 
	}

}
