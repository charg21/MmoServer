using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktQuest
{
	public UInt32 _designId; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktQuest(){}

	public bool Write( Span< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktQuest >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< PktQuest >() );
			return true;
		}

		bool success = true;

		success &= BitConverter.TryWriteBytes( s[ count.. ], _designId );
		count   += sizeof( UInt32 );

		return success;
	}

	public void Read( ReadOnlySpan< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktQuest >() )
		{
			this = Unsafe.ReadUnaligned< PktQuest >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			count += (ushort)( Unsafe.SizeOf< PktQuest >() );
			return;
		}

		_designId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
	}

}
