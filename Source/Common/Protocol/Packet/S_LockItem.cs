using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_LockItem : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public Int32 _itemId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_LockItem()
	{
		_type = (ushort)( EPacketType.S_LockItem );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_LockItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
		count   += sizeof( ELogicResult );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _itemId );
		count   += sizeof( Int32 );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_itemId = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_LockItem : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public Int32 _itemId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_LockItem()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_LockItem );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_LockItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_LockItem >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_LockItem >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
			count   += sizeof( ELogicResult );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _itemId );
			count   += sizeof( Int32 );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_LockItem >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_LockItem >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_itemId = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}
