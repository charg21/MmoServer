using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktObject
{
	public UInt64 _objectId; /// 오브젝트 식별자
	public PktVector3 _pos = new();  /// 좌표
	public Single _yaw; /// 
	public EWorldObjectType _objectType; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktObject(){}

	public bool Write( Span< byte > s, ref ushort count )
	{
		bool success = true;

		success &= BitConverter.TryWriteBytes( s[ count.. ], _objectId );
		count   += sizeof( UInt64 );
		success &= _pos.Write( s, ref count );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _yaw );
		count   += sizeof( Single );
		success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_objectType );
		count   += sizeof( EWorldObjectType );

		return success;
	}

	public void Read( ReadOnlySpan< byte > s, ref ushort count )
	{
		_objectId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_pos.Read( s, ref count );
		_yaw = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
		_objectType = (EWorldObjectType)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( EWorldObjectType );
	}

}
