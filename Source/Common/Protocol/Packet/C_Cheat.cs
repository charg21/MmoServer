using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class C_Cheat : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Server;

	public String _command = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public C_Cheat()
	{
		_type = (ushort)( EPacketType.C_Cheat );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_Cheat ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		ushort _commandLen = (ushort)Encoding.Unicode.GetBytes( _command.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _commandLen );
		count += sizeof( ushort );
		count += _commandLen;
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		ushort _commandLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_command = Encoding.Unicode.GetString( s.Slice( count, _commandLen ) ); 
		count += _commandLen; 
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueC_Cheat : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Server;

	public String _command = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueC_Cheat()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.C_Cheat );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_Cheat ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_Cheat >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueC_Cheat >() );
		}
		else
		{
			#endif
			ushort _commandLen = (ushort)Encoding.Unicode.GetBytes( _command.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _commandLen );
			count += sizeof( ushort );
			count += _commandLen;
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_Cheat >() )
		{
			this = Unsafe.ReadUnaligned< ValueC_Cheat >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		ushort _commandLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_command = Encoding.Unicode.GetString( s.Slice( count, _commandLen ) ); 
		count += _commandLen; 
	}

}
