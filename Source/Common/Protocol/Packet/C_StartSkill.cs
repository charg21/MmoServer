using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class C_StartSkill : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.World;

	public UInt64 _targetId; /// 스킬 대상 식별자( 논 타겟인 경우 0 가능 )
	public UInt32 _skillDesignId; /// 스킬 디자인 식별자
	public Single _yaw; /// 스킬 방향
	public PktVector3 _castPos = new(); /// 시전 위치

	/// <summary>
	/// 생성자
	/// </summary>
	public C_StartSkill()
	{
		_type = (ushort)( EPacketType.C_StartSkill );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_StartSkill ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], _targetId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _skillDesignId );
		count   += sizeof( UInt32 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _yaw );
		count   += sizeof( Single );
		success &= _castPos.Write( s, ref count );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_targetId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_skillDesignId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_yaw = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
		_castPos.Read( s, ref count );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueC_StartSkill : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.World;

	public UInt64 _targetId; /// 스킬 대상 식별자( 논 타겟인 경우 0 가능 )
	public UInt32 _skillDesignId; /// 스킬 디자인 식별자
	public Single _yaw; /// 스킬 방향
	public PktVector3 _castPos = new(); /// 시전 위치

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueC_StartSkill()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.C_StartSkill );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_StartSkill ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], _targetId );
			count   += sizeof( UInt64 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _skillDesignId );
			count   += sizeof( UInt32 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _yaw );
			count   += sizeof( Single );
			success &= _castPos.Write( s, ref count );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		_targetId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_skillDesignId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_yaw = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
		_castPos.Read( s, ref count );
	}

}
