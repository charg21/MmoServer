using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktPosition
{
	public Single _x; /// 
	public Single _y; /// 
	public Single _z; /// 

	public PktPosition() {}


	public bool Write( Span< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktPosition >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< PktPosition >() );
			return true;
		}

		bool success = true;

		success &= BitConverter.TryWriteBytes( s[ count.. ], _x );
		count   += sizeof( Single );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _y );
		count   += sizeof( Single );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _z );
		count   += sizeof( Single );

		return success;
	}


	public void Read( ReadOnlySpan< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktPosition >() )
		{
			this = Unsafe.ReadUnaligned< PktPosition >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			count += (ushort)( Unsafe.SizeOf< PktPosition >() );
			return;
		}

		_x = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
		_y = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
		_z = BitConverter.ToSingle( s[ count.. ] );
		count   += sizeof( Single );
	}

}

