using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_SkillList : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public List< UInt32 > _skillDesignIdList = new(); /// 스킬 디자인 식별자 목록

	/// <summary>
	/// 생성자
	/// </summary>
	public S_SkillList()
	{
		_type = (ushort)( EPacketType.S_SkillList );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_SkillList ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _skillDesignIdList.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _skillDesignIdList.AsSpan() )
		{
			success &= BitConverter.TryWriteBytes( s[ count.. ], element );
			count   += sizeof( UInt32 );
		}
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_skillDesignIdList.Clear();
		ushort _skillDesignIdListLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_skillDesignIdList.Capacity = _skillDesignIdListLength;
		for ( int i = 0; i < _skillDesignIdListLength; i += 1 )
		{
			var element = BitConverter.ToUInt32( s[ count.. ] );
			count += sizeof( UInt32 );
			_skillDesignIdList.Add( element );
		}
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_SkillList : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public List< UInt32 > _skillDesignIdList = new(); /// 스킬 디자인 식별자 목록

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_SkillList()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_SkillList );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_SkillList ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_SkillList >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_SkillList >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _skillDesignIdList.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _skillDesignIdList.AsSpan() )
			{
				success &= BitConverter.TryWriteBytes( s[ count.. ], element );
				count   += sizeof( UInt32 );
			}
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_SkillList >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_SkillList >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_skillDesignIdList.Clear();
		ushort _skillDesignIdListLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_skillDesignIdList.Capacity = _skillDesignIdListLength;
		for ( int i = 0; i < _skillDesignIdListLength; i += 1 )
		{
			var element = BitConverter.ToUInt32( s[ count.. ] );
			count += sizeof( UInt32 );
			_skillDesignIdList.Add( element );
		}
	}

}
