using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class C_LoadPc : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Server;

	public UInt64 _pcId; 
	public String _token = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public C_LoadPc()
	{
		_type = (ushort)( EPacketType.C_LoadPc );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_LoadPc ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], _pcId );
		count   += sizeof( UInt64 );
		ushort _tokenLen = (ushort)Encoding.Unicode.GetBytes( _token.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _tokenLen );
		count += sizeof( ushort );
		count += _tokenLen;
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_pcId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		ushort _tokenLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_token = Encoding.Unicode.GetString( s.Slice( count, _tokenLen ) ); 
		count += _tokenLen; 
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueC_LoadPc : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Server;

	public UInt64 _pcId; 
	public String _token = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueC_LoadPc()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.C_LoadPc );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_LoadPc ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_LoadPc >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueC_LoadPc >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], _pcId );
			count   += sizeof( UInt64 );
			ushort _tokenLen = (ushort)Encoding.Unicode.GetBytes( _token.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _tokenLen );
			count += sizeof( ushort );
			count += _tokenLen;
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_LoadPc >() )
		{
			this = Unsafe.ReadUnaligned< ValueC_LoadPc >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_pcId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		ushort _tokenLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_token = Encoding.Unicode.GetString( s.Slice( count, _tokenLen ) ); 
		count += _tokenLen; 
	}

}
