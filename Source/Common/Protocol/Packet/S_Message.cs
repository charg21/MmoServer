using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_Message : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public String _message = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_Message()
	{
		_type = (ushort)( EPacketType.S_Message );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Message ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		ushort _messageLen = (ushort)Encoding.Unicode.GetBytes( _message.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _messageLen );
		count += sizeof( ushort );
		count += _messageLen;
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		ushort _messageLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_message = Encoding.Unicode.GetString( s.Slice( count, _messageLen ) ); 
		count += _messageLen; 
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_Message : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public String _message = string.Empty; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_Message()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_Message );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Message ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Message >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_Message >() );
		}
		else
		{
			#endif
			ushort _messageLen = (ushort)Encoding.Unicode.GetBytes( _message.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _messageLen );
			count += sizeof( ushort );
			count += _messageLen;
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Message >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_Message >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		ushort _messageLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_message = Encoding.Unicode.GetString( s.Slice( count, _messageLen ) ); 
		count += _messageLen; 
	}

}
