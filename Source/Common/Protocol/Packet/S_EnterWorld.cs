using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_EnterWorld : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.World;

	public PktPc _my = new(); 
	public List< PktPc > _pcs = new(); 
	public List< PktNpc > _npcs = new(); 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_EnterWorld()
	{
		_type = (ushort)( EPacketType.S_EnterWorld );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_EnterWorld ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= _my.Write( s, ref count );
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _pcs.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _pcs.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _npcs.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _npcs.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_my.Read( s, ref count );
		_pcs.Clear();
		ushort _pcsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_pcs.Capacity = _pcsLength;
		for ( int i = 0; i < _pcsLength; i += 1 )
		{
			ref var element = ref _pcs.ReserveOneItem();
			element.Read( s, ref count );
		}
		_npcs.Clear();
		ushort _npcsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_npcs.Capacity = _npcsLength;
		for ( int i = 0; i < _npcsLength; i += 1 )
		{
			ref var element = ref _npcs.ReserveOneItem();
			element.Read( s, ref count );
		}
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_EnterWorld : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.World;

	public PktPc _my = new(); 
	public List< PktPc > _pcs = new(); 
	public List< PktNpc > _npcs = new(); 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_EnterWorld()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_EnterWorld );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_EnterWorld ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		{
			#endif
			success &= _my.Write( s, ref count );
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _pcs.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _pcs.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _npcs.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _npcs.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		_my.Read( s, ref count );
		_pcs.Clear();
		ushort _pcsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_pcs.Capacity = _pcsLength;
		for ( int i = 0; i < _pcsLength; i += 1 )
		{
			ref var element = ref _pcs.ReserveOneItem();
			element.Read( s, ref count );
		}
		_npcs.Clear();
		ushort _npcsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_npcs.Capacity = _npcsLength;
		for ( int i = 0; i < _npcsLength; i += 1 )
		{
			ref var element = ref _npcs.ReserveOneItem();
			element.Read( s, ref count );
		}
	}

}
