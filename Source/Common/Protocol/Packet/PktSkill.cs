using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktSkill
{
	public UInt32 _skillDesignId; /// 
	public UInt64 _lastUseTick; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktSkill(){}

	public bool Write( Span< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktSkill >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< PktSkill >() );
			return true;
		}

		bool success = true;

		success &= BitConverter.TryWriteBytes( s[ count.. ], _skillDesignId );
		count   += sizeof( UInt32 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _lastUseTick );
		count   += sizeof( UInt64 );

		return success;
	}

	public void Read( ReadOnlySpan< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktSkill >() )
		{
			this = Unsafe.ReadUnaligned< PktSkill >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			count += (ushort)( Unsafe.SizeOf< PktSkill >() );
			return;
		}

		_skillDesignId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_lastUseTick = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
	}

}
