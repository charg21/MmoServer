using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_ListItem : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public ValueList< PktItem > _items = new(); 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_ListItem()
	{
		_type = (ushort)( EPacketType.S_ListItem );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_ListItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _items.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _items.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_items.Clear();
		ushort _itemsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_items.Capacity = _itemsLength;
		for ( int i = 0; i < _itemsLength; i += 1 )
		{
			ref var element = ref _items.ReserveOneItem();
			element.Read( s, ref count );
		}
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_ListItem : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public ValueList< PktItem > _items = new(); 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_ListItem()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_ListItem );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_ListItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_ListItem >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_ListItem >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _items.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _items.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_ListItem >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_ListItem >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_items.Clear();
		ushort _itemsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_items.Capacity = _itemsLength;
		for ( int i = 0; i < _itemsLength; i += 1 )
		{
			ref var element = ref _items.ReserveOneItem();
			element.Read( s, ref count );
		}
	}

}
