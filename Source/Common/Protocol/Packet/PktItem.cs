using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktItem
{
	public UInt64 _id; /// 
	public UInt32 _designId; /// 
	public Int32 _count; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktItem(){}

	public bool Write( Span< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktItem >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< PktItem >() );
			return true;
		}

		bool success = true;

		success &= BitConverter.TryWriteBytes( s[ count.. ], _id );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _designId );
		count   += sizeof( UInt32 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _count );
		count   += sizeof( Int32 );

		return success;
	}

	public void Read( ReadOnlySpan< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktItem >() )
		{
			this = Unsafe.ReadUnaligned< PktItem >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			count += (ushort)( Unsafe.SizeOf< PktItem >() );
			return;
		}

		_id = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_designId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_count = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}
