using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_LoadPc : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public PktPc _pktPc = new(); 
	public List< PktItem > _items = new(); /// 보유 아이템 목록
	public List< PktEquip > _equips = new(); /// 장착 장비 목록
	public List< PktSkill > _skills = new(); /// 보유 스킬 목록
	public List< PktStat > _stats = new(); /// 스탯 목록
	public List< PktQuest > _quests = new(); /// 퀘스트 목록

	/// <summary>
	/// 생성자
	/// </summary>
	public S_LoadPc()
	{
		_type = (ushort)( EPacketType.S_LoadPc );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_LoadPc ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
		count   += sizeof( ELogicResult );
		success &= _pktPc.Write( s, ref count );
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _items.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _items.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _equips.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _equips.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _skills.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _skills.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _stats.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _stats.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _quests.Count ) );
		count += sizeof( ushort );
		foreach ( var element in _quests.AsSpan() )
		{
			success &= element.Write( s, ref count );
		}
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_pktPc.Read( s, ref count );
		_items.Clear();
		ushort _itemsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_items.Capacity = _itemsLength;
		for ( int i = 0; i < _itemsLength; i += 1 )
		{
			ref var element = ref _items.ReserveOneItem();
			element.Read( s, ref count );
		}
		_equips.Clear();
		ushort _equipsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_equips.Capacity = _equipsLength;
		for ( int i = 0; i < _equipsLength; i += 1 )
		{
			ref var element = ref _equips.ReserveOneItem();
			element.Read( s, ref count );
		}
		_skills.Clear();
		ushort _skillsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_skills.Capacity = _skillsLength;
		for ( int i = 0; i < _skillsLength; i += 1 )
		{
			ref var element = ref _skills.ReserveOneItem();
			element.Read( s, ref count );
		}
		_stats.Clear();
		ushort _statsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_stats.Capacity = _statsLength;
		for ( int i = 0; i < _statsLength; i += 1 )
		{
			ref var element = ref _stats.ReserveOneItem();
			element.Read( s, ref count );
		}
		_quests.Clear();
		ushort _questsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_quests.Capacity = _questsLength;
		for ( int i = 0; i < _questsLength; i += 1 )
		{
			ref var element = ref _quests.ReserveOneItem();
			element.Read( s, ref count );
		}
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_LoadPc : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public PktPc _pktPc = new(); 
	public List< PktItem > _items = new(); /// 보유 아이템 목록
	public List< PktEquip > _equips = new(); /// 장착 장비 목록
	public List< PktSkill > _skills = new(); /// 보유 스킬 목록
	public List< PktStat > _stats = new(); /// 스탯 목록
	public List< PktQuest > _quests = new(); /// 퀘스트 목록

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_LoadPc()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_LoadPc );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_LoadPc ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
			count   += sizeof( ELogicResult );
			success &= _pktPc.Write( s, ref count );
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _items.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _items.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _equips.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _equips.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _skills.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _skills.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _stats.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _stats.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			success &= BitConverter.TryWriteBytes( s[ count.. ], (ushort)( _quests.Count ) );
			count += sizeof( ushort );
			foreach ( var element in _quests.AsSpan() )
			{
				success &= element.Write( s, ref count );
			}
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_pktPc.Read( s, ref count );
		_items.Clear();
		ushort _itemsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_items.Capacity = _itemsLength;
		for ( int i = 0; i < _itemsLength; i += 1 )
		{
			ref var element = ref _items.ReserveOneItem();
			element.Read( s, ref count );
		}
		_equips.Clear();
		ushort _equipsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_equips.Capacity = _equipsLength;
		for ( int i = 0; i < _equipsLength; i += 1 )
		{
			ref var element = ref _equips.ReserveOneItem();
			element.Read( s, ref count );
		}
		_skills.Clear();
		ushort _skillsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_skills.Capacity = _skillsLength;
		for ( int i = 0; i < _skillsLength; i += 1 )
		{
			ref var element = ref _skills.ReserveOneItem();
			element.Read( s, ref count );
		}
		_stats.Clear();
		ushort _statsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_stats.Capacity = _statsLength;
		for ( int i = 0; i < _statsLength; i += 1 )
		{
			ref var element = ref _stats.ReserveOneItem();
			element.Read( s, ref count );
		}
		_quests.Clear();
		ushort _questsLength = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort );
		_quests.Capacity = _questsLength;
		for ( int i = 0; i < _questsLength; i += 1 )
		{
			ref var element = ref _quests.ReserveOneItem();
			element.Read( s, ref count );
		}
	}

}
