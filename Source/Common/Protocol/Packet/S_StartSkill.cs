using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_StartSkill : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public UInt32 _skillDesignId; /// 스킬 디자인 식별자
	public UInt64 _casterId; /// 스킬 디자인 식별자
	public UInt64 _targetId; /// 스킬 대상 식별자( 논 타겟인 경우 0 가능 )

	/// <summary>
	/// 생성자
	/// </summary>
	public S_StartSkill()
	{
		_type = (ushort)( EPacketType.S_StartSkill );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_StartSkill ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
		count   += sizeof( ELogicResult );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _skillDesignId );
		count   += sizeof( UInt32 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _casterId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _targetId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_skillDesignId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_casterId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_targetId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_StartSkill : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public UInt32 _skillDesignId; /// 스킬 디자인 식별자
	public UInt64 _casterId; /// 스킬 디자인 식별자
	public UInt64 _targetId; /// 스킬 대상 식별자( 논 타겟인 경우 0 가능 )

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_StartSkill()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_StartSkill );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_StartSkill ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_StartSkill >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_StartSkill >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
			count   += sizeof( ELogicResult );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _skillDesignId );
			count   += sizeof( UInt32 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _casterId );
			count   += sizeof( UInt64 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _targetId );
			count   += sizeof( UInt64 );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_StartSkill >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_StartSkill >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_skillDesignId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_casterId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_targetId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
	}

}
