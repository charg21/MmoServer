using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktVector3
{
	public Single _x; /// 
	public Single _y; /// 
	public Single _z; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktVector3(){}

}
