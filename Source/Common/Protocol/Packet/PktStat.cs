using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktStat
{
	public EStatType _type; /// 
	public Int32 _value; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktStat(){}

	public bool Write( Span< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktStat >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< PktStat >() );
			return true;
		}

		bool success = true;

		success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_type );
		count   += sizeof( EStatType );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _value );
		count   += sizeof( Int32 );

		return success;
	}

	public void Read( ReadOnlySpan< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktStat >() )
		{
			this = Unsafe.ReadUnaligned< PktStat >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			count += (ushort)( Unsafe.SizeOf< PktStat >() );
			return;
		}

		_type = (EStatType)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( EStatType );
		_value = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}
