using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_Hit : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public UInt64 _targetId; /// 대상 식별자
	public Int64 _damage; /// 피해량
	public Int64 _curHp; 
	public UInt32 _skillDesignId; /// 스킬 디자인 식별자
	public UInt64 _casterId; /// 캐스터 식별자

	/// <summary>
	/// 생성자
	/// </summary>
	public S_Hit()
	{
		_type = (ushort)( EPacketType.S_Hit );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Hit ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], _targetId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _damage );
		count   += sizeof( Int64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _curHp );
		count   += sizeof( Int64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _skillDesignId );
		count   += sizeof( UInt32 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _casterId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_targetId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_damage = BitConverter.ToInt64( s[ count.. ] );
		count   += sizeof( Int64 );
		_curHp = BitConverter.ToInt64( s[ count.. ] );
		count   += sizeof( Int64 );
		_skillDesignId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_casterId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_Hit : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public UInt64 _targetId; /// 대상 식별자
	public Int64 _damage; /// 피해량
	public Int64 _curHp; 
	public UInt32 _skillDesignId; /// 스킬 디자인 식별자
	public UInt64 _casterId; /// 캐스터 식별자

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_Hit()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_Hit );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Hit ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Hit >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_Hit >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], _targetId );
			count   += sizeof( UInt64 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _damage );
			count   += sizeof( Int64 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _curHp );
			count   += sizeof( Int64 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _skillDesignId );
			count   += sizeof( UInt32 );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _casterId );
			count   += sizeof( UInt64 );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Hit >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_Hit >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_targetId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_damage = BitConverter.ToInt64( s[ count.. ] );
		count   += sizeof( Int64 );
		_curHp = BitConverter.ToInt64( s[ count.. ] );
		count   += sizeof( Int64 );
		_skillDesignId = BitConverter.ToUInt32( s[ count.. ] );
		count   += sizeof( UInt32 );
		_casterId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
	}

}
