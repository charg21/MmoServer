using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Net;
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktEquip
{
	public UInt64 _itemId; /// 
	public UInt16 _slot; /// 

	/// <summary>
	/// 생성자
	/// </summary>
	public PktEquip(){}

	public bool Write( Span< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktEquip >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< PktEquip >() );
			return true;
		}

		bool success = true;

		success &= BitConverter.TryWriteBytes( s[ count.. ], _itemId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _slot );
		count   += sizeof( UInt16 );

		return success;
	}

	public void Read( ReadOnlySpan< byte > s, ref ushort count )
	{
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< PktEquip >() )
		{
			this = Unsafe.ReadUnaligned< PktEquip >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			count += (ushort)( Unsafe.SizeOf< PktEquip >() );
			return;
		}

		_itemId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
		_slot = BitConverter.ToUInt16( s[ count.. ] );
		count   += sizeof( UInt16 );
	}

}
