using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class C_Login : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Server;

	public String _account = string.Empty; /// 계정
	public String _password = string.Empty; /// 비밀번호

	/// <summary>
	/// 생성자
	/// </summary>
	public C_Login()
	{
		_type = (ushort)( EPacketType.C_Login );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_Login ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		ushort _accountLen = (ushort)Encoding.Unicode.GetBytes( _account.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _accountLen );
		count += sizeof( ushort );
		count += _accountLen;
		ushort _passwordLen = (ushort)Encoding.Unicode.GetBytes( _password.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _passwordLen );
		count += sizeof( ushort );
		count += _passwordLen;
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		ushort _accountLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_account = Encoding.Unicode.GetString( s.Slice( count, _accountLen ) ); 
		count += _accountLen; 
		ushort _passwordLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_password = Encoding.Unicode.GetString( s.Slice( count, _passwordLen ) ); 
		count += _passwordLen; 
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueC_Login : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Server;

	public String _account = string.Empty; /// 계정
	public String _password = string.Empty; /// 비밀번호

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueC_Login()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.C_Login );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_Login ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_Login >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueC_Login >() );
		}
		else
		{
			#endif
			ushort _accountLen = (ushort)Encoding.Unicode.GetBytes( _account.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _accountLen );
			count += sizeof( ushort );
			count += _accountLen;
			ushort _passwordLen = (ushort)Encoding.Unicode.GetBytes( _password.AsSpan(), s[ ( count + sizeof( ushort ) ).. ] );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _passwordLen );
			count += sizeof( ushort );
			count += _passwordLen;
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_Login >() )
		{
			this = Unsafe.ReadUnaligned< ValueC_Login >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		ushort _accountLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_account = Encoding.Unicode.GetString( s.Slice( count, _accountLen ) ); 
		count += _accountLen; 
		ushort _passwordLen = BitConverter.ToUInt16( s[ count.. ] );
		count += sizeof( ushort ); 
		_password = Encoding.Unicode.GetString( s.Slice( count, _passwordLen ) ); 
		count += _passwordLen; 
	}

}
