using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_UseItem : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public Int32 _itemId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_UseItem()
	{
		_type = (ushort)( EPacketType.S_UseItem );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_UseItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
		count   += sizeof( ELogicResult );
		success &= BitConverter.TryWriteBytes( s[ count.. ], _itemId );
		count   += sizeof( Int32 );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_itemId = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_UseItem : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public ELogicResult _result; 
	public Int32 _itemId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_UseItem()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_UseItem );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_UseItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_UseItem >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_UseItem >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], (Int16)_result );
			count   += sizeof( ELogicResult );
			success &= BitConverter.TryWriteBytes( s[ count.. ], _itemId );
			count   += sizeof( Int32 );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_UseItem >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_UseItem >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_result = (ELogicResult)BitConverter.ToInt16( s[ count.. ] );
		count   += sizeof( ELogicResult );
		_itemId = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}
