using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class C_UnLockItem : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.World;

	public Int32 _itemId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public C_UnLockItem()
	{
		_type = (ushort)( EPacketType.C_UnLockItem );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_UnLockItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], _itemId );
		count   += sizeof( Int32 );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_itemId = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueC_UnLockItem : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.World;

	public Int32 _itemId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueC_UnLockItem()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.C_UnLockItem );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.C_UnLockItem ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_UnLockItem >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueC_UnLockItem >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], _itemId );
			count   += sizeof( Int32 );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueC_UnLockItem >() )
		{
			this = Unsafe.ReadUnaligned< ValueC_UnLockItem >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_itemId = BitConverter.ToInt32( s[ count.. ] );
		count   += sizeof( Int32 );
	}

}
