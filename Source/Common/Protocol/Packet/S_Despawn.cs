using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using Mala.Collection;
using Mala.Net;
public partial class S_Despawn : PacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public override EServiceType To => EServiceType.Client;

	public UInt64 _objectId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public S_Despawn()
	{
		_type = (ushort)( EPacketType.S_Despawn );
	}

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public override SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Despawn ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		success &= BitConverter.TryWriteBytes( s[ count.. ], _objectId );
		count   += sizeof( UInt64 );
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public override void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type

		_objectId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
	}

}

[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueS_Despawn : IPacketHeader
{
	/// <summary>
	/// 목적지
	/// </summary>
	public EServiceType To => EServiceType.Client;

	public UInt64 _objectId; 

	/// <summary>
	/// 생성자
	/// </summary>
	public ValueS_Despawn()
	{
	}

	public ushort Length => throw new NotImplementedException();
	public ushort Type => (ushort)( EPacketType.S_Despawn );

	/// <summary>
	/// 직렬화 한다.
	/// </summary>
	public SendSegment Write()
	{
		var s = SendBufferHelper.Open( 25600 ).Span;
		bool success = BitConverter.TryWriteBytes( s[ sizeof( ushort ).. ], (ushort)( EPacketType.S_Despawn ) );
		ushort count = sizeof( ushort ) + sizeof( ushort ); // Length( Reserved ) + Type

		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Despawn >() )
		{
			Unsafe.WriteUnaligned( ref MemoryMarshal.GetReference( s[ count.. ] ), this );
			count += (ushort)( Unsafe.SizeOf< ValueS_Despawn >() );
		}
		else
		{
			#endif
			success &= BitConverter.TryWriteBytes( s[ count.. ], _objectId );
			count   += sizeof( UInt64 );
			#if !UNITY_EDITOR
		}
		#endif
		success &= BitConverter.TryWriteBytes( s, count );
		if ( !success )
		    return new( null, null );

		return SendBufferHelper.Close( count );
	}

	/// <summary>
	/// 역직렬화 한다.
	/// </summary>
	public void Read( ref ReadOnlySpan< byte > s )
	{
		ushort count = sizeof( ushort ) + sizeof( ushort ); // len + type
		#if !UNITY_EDITOR
		if ( !RuntimeHelpers.IsReferenceOrContainsReferences< ValueS_Despawn >() )
		{
			this = Unsafe.ReadUnaligned< ValueS_Despawn >( ref MemoryMarshal.GetReference( s[ count.. ] ) );
			return;
		}
		#endif
		_objectId = BitConverter.ToUInt64( s[ count.. ] );
		count   += sizeof( UInt64 );
	}

}
