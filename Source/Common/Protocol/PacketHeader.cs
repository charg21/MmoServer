﻿using Mala.Net;

public abstract class PacketHeader : IPacketHeader
{
    protected ushort _length = 0; //< 길이
    protected ushort _type  = 0;  //< 타입

    public ushort Length => _length;
    public ushort Type => _type;
    public virtual EServiceType To => EServiceType.None;

    /// <summary>
    ///
    /// </summary>
    public virtual void Read( ref ReadOnlySpan< byte > segment )
    {
    }

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    public virtual SendSegment Write()
    {
        return new( null, null );
    }

}

public class PktOptional< T > : PacketHeader where T : IPacketHeader, new()
{
    /// <summary>
    /// 유효한지 여부
    /// </summary>
    public bool IsValid { get; set; } = false;
    T _packet;

    ref T Packet
    {
        get
        {
            if ( !IsValid )
            {
                _packet = new T();
                IsValid = true;
            }

            //var cMove = new ProtocolOptional< ValueC_Move >();
            //cMove.Packet._pos = new();

            return ref _packet;
        }
    }

    /// <summary>
    ///
    /// </summary>
    public override void Read( ref ReadOnlySpan< byte > segment )
    {
        if ( IsValid )
            return;

        _packet?.Read( ref segment );
    }

    /// <summary>
    ///
    /// </summary>
    /// <returns></returns>
    public override SendSegment Write()
    {
        if ( IsValid )
            return new();

        return _packet.Write();
    }
}

