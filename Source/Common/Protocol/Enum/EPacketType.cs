public enum EPacketType : UInt16
{
	C_EnterWorld  = 0, /// 
	S_EnterWorld  = 1, /// 
	S_Spawn       = 2, /// 
	C_LeaveWorld  = 3, /// 
	S_LeaveWorld  = 4, /// 
	S_Despawn     = 5, /// 
	C_Move        = 6, /// 
	S_Move        = 7, /// 
	S_SkillList   = 8, /// 
	C_StartSkill  = 9, /// 
	S_StartSkill  = 10, /// 
	S_Hit         = 11, /// 
	C_Login       = 12, /// 
	S_Login       = 13, /// 
	S_Ping        = 14, /// 
	C_Pong        = 15, /// 
	C_Chat        = 16, /// 
	S_Chat        = 17, /// 
	C_LoadPc      = 18, /// 
	S_LoadPc      = 19, /// 
	C_Cheat       = 20, /// 
	S_Cheat       = 21, /// 
	S_Message     = 22, /// 
	C_ListChannel = 23, /// 
	S_ListChannel = 24, /// 
	C_UseItem     = 25, /// 
	S_UseItem     = 26, /// 
	S_ListItem    = 27, /// 
	C_LockItem    = 28, /// 
	S_LockItem    = 29, /// 
	C_UnLockItem  = 30, /// 
	S_UnLockItem  = 31, /// 
	Max           = 32, /// 이 열거형의 최대값
};

