public enum EWorldObjectType : Int16
{
	None       = 0, /// 없음
	Pc         = 1 << 0, /// 플레이어블 캐릭터
	Npc        = 1 << 1, /// Npc
	Gadget     = 1 << 2, /// 설치물
	Projectile = 1 << 3, /// 투사체
	Max        = 9, /// 
	All        = 15, /// 
};

