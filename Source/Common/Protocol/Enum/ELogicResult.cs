public enum ELogicResult : Int16
{
	Success           = 0, /// 성공
	Pending           = 1, /// 작업중
	DbFail            = 2, /// Db 실패
	NullPc            = 3, /// Pc X
	NullUser          = 4, /// 유저 X
	NullSkillList     = 5, /// 스킬 리스트 X
	NullCombat        = 6, /// 전투 X
	InvalidDesignId   = 7, /// 유효하지 기획 테이블 식별자
	UnownedSkill      = 8, /// 보유하지 않은 스킬
	UnownedItem       = 9, /// 보유하지 않은 아이템
	AlreadyDeadPc     = 10, /// 이미 죽은 Pc
	TargetNotInView   = 11, /// 대상이 시야에 없음
	TargetNotInRange  = 12, /// 대상이 범위에 내에 없음
	AlreadyDoingCast  = 13, /// 이미 캐스팅 중
	AlreadyAddedItem  = 14, /// 이미 추가된 아이템
	AlreadyLockedItem = 15, /// 이미 잠금 상태인 아이템
	NotEnoughBagSpace = 16, /// 가방 공간 부족
	Max               = 17, /// 
};

