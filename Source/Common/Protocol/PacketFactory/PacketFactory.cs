using Mala.Net;
public class PacketFactory
{
	/// <summary>
	/// 
	/// </summary>
	PacketHeader[] _packets = GC.AllocateArray< PacketHeader >( (int)EPacketType.Max + 1, true );

	/// <summary>
	/// Tls별 인스턴스
	/// </summary>
	public static ThreadLocal< PacketFactory > TlsInstance = new( () => new() );

	/// <summary>
	/// 생성자
	/// </summary>
	public PacketFactory()
	{
		foreach ( EPacketType pktType in Enum.GetValues< EPacketType >() )
		{
			_packets[ (int)( pktType ) ] = MakePacket( (int)( pktType ) );
		}
	}

	/// <summary>
	/// 패킷을 반환한다.
	/// </summary>
	public PacketHeader? GetPacket( int type ) => _packets[ type ];

	/// <summary>
	/// 패킷을 생성한다.
	/// </summary>
	public static PacketHeader? MakePacket( int type )
	{
		return (EPacketType)( type ) switch
		{
			EPacketType.C_EnterWorld => new C_EnterWorld(),
			EPacketType.S_EnterWorld => new S_EnterWorld(),
			EPacketType.S_Spawn => new S_Spawn(),
			EPacketType.C_LeaveWorld => new C_LeaveWorld(),
			EPacketType.S_LeaveWorld => new S_LeaveWorld(),
			EPacketType.S_Despawn => new S_Despawn(),
			EPacketType.C_Move => new C_Move(),
			EPacketType.S_Move => new S_Move(),
			EPacketType.S_SkillList => new S_SkillList(),
			EPacketType.C_StartSkill => new C_StartSkill(),
			EPacketType.S_StartSkill => new S_StartSkill(),
			EPacketType.S_Hit => new S_Hit(),
			EPacketType.C_Login => new C_Login(),
			EPacketType.S_Login => new S_Login(),
			EPacketType.S_Ping => new S_Ping(),
			EPacketType.C_Pong => new C_Pong(),
			EPacketType.C_Chat => new C_Chat(),
			EPacketType.S_Chat => new S_Chat(),
			EPacketType.C_LoadPc => new C_LoadPc(),
			EPacketType.S_LoadPc => new S_LoadPc(),
			EPacketType.C_Cheat => new C_Cheat(),
			EPacketType.S_Cheat => new S_Cheat(),
			EPacketType.S_Message => new S_Message(),
			EPacketType.C_ListChannel => new C_ListChannel(),
			EPacketType.S_ListChannel => new S_ListChannel(),
			EPacketType.C_UseItem => new C_UseItem(),
			EPacketType.S_UseItem => new S_UseItem(),
			EPacketType.S_ListItem => new S_ListItem(),
			EPacketType.C_LockItem => new C_LockItem(),
			EPacketType.S_LockItem => new S_LockItem(),
			EPacketType.C_UnLockItem => new C_UnLockItem(),
			EPacketType.S_UnLockItem => new S_UnLockItem(),
			_ => null
		}
		;
	}
}

