﻿using Mala.Net;

public interface IPacketHeader
{
    public ushort Length { get; }
    public ushort Type { get; }

    /// <summary>
    /// 목적지
    /// </summary>
    public EServiceType To { get; }

    /// <summary>
    /// 역직렬화 함수
    /// </summary>
    public void Read( ref ReadOnlySpan< byte > s );

    /// <summary>
    /// 직렬화 함수
    /// </summary>
    public SendSegment Write();
}

