﻿using Mala.Math;


public static class PacketExtension
{
    public static PktVector3 ToPktVector3( this in Vector3 v3 )
        => new() { _x = v3.x, _y = v3.y, _z = v3.z };

    public static Vector3 ToVector3( this in PktVector3 v3 )
        => new() { x = v3._x, y = v3._y, z = v3._z };

    public static string ToString2( this in PktVector3 v3 )
        => $"{ v3._x }, { v3._y }, { v3._z }";
}

public static class ELogicResultEx
{
    public static bool IsSuccess( this ELogicResult result )
        => result == ELogicResult.Success;

    public static bool IsPending( this ELogicResult result )
        => result == ELogicResult.Pending;

    public static bool IsPendingOrSuccess( this ELogicResult result )
        => result == ELogicResult.Pending || result == ELogicResult.Success;
}
