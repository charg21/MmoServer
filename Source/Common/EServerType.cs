﻿using Mala.Logging;

/// <summary>
/// 서비스 타입
/// </summary>
public enum EServiceType
{
    /// <summary>
    /// 없음
    /// </summary>
    None = 0,

    /// <summary>
    /// 클라이언트
    /// </summary>
    Client = 1 << 0,

    /// <summary>
    /// World 서버
    /// </summary>
    World = 1 << 1,

    /// <summary>
    /// Nation 서버
    /// </summary>
    Nation = 1 << 2,

    /// <summary>
    /// 서버
    /// </summary>
    Server = World | Nation,

    /// <summary>
    /// 전체
    /// </summary>
    All = ~0,
}

/// <summary>
/// 서비스 타입 확장 메소드
/// </summary>
public static class ServiceTypeEx
{
    /// <summary>
    /// ServiceType 플래그 보유 여부를 반환한다.
    /// Enum.HasFlag( Enum )가 IL에서 박싱이 발생하여( Enum으로 캐스팅시 발생 ),
    /// 직접 비트 연산을 통해 비교
    /// </summary>
    public static new bool HasFlagFast( this EServiceType thisServiceType, EServiceType serviceType )
    {
        return ( (int)thisServiceType & (int)serviceType ) > 0;
    }
}