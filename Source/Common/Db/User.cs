﻿using Mala.Db;


/// <summary>
/// user Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class UserDbModel : DbModel
{
    /// <summary>
    /// 동기화 한다.
    /// </summary>
    public override void Sync( TxContext tx, EQueryType queryType )
    {
        switch ( queryType )
        {
            case EQueryType.Update:
                {
                    var user = _root as UserDbModel;
                    CopyToOnlyUpdateField( user );
                    break;
                }

            case EQueryType.Insert:
                {
                    var user = _root as UserDbModel;
                    CopyTo( user );
                    break;
                }

            case EQueryType.Delete:
                break;

            default:
                break;
        }
    }
}

public partial class User : IDbModel
{
}
