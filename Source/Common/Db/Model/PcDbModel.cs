/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// pc Db 테이블 정보
/// </summary>
public class PcTableInfo : TableInfo
{
	/// <summary>
	/// pc 테이블 정보 전역 인스턴스
	/// </summary>
	public static PcTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		id,
		user_id,
		name,
		hp,
		x,
		y,
		z,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public PcTableInfo() : base( "pc" )
	{
		AddFieldInfo( "id"     , (int)( EFieldNumber.id )     , EVariantType.U64, true  );
		AddFieldInfo( "user_id", (int)( EFieldNumber.user_id ), EVariantType.U64, true  );
		AddFieldInfo( "name"   , (int)( EFieldNumber.name )   , EVariantType.Str, false );
		AddFieldInfo( "hp"     , (int)( EFieldNumber.hp )     , EVariantType.I32, false );
		AddFieldInfo( "x"      , (int)( EFieldNumber.x )      , EVariantType.F32, false );
		AddFieldInfo( "y"      , (int)( EFieldNumber.y )      , EVariantType.F32, false );
		AddFieldInfo( "z"      , (int)( EFieldNumber.z )      , EVariantType.F32, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.id ) ] );
		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.user_id ) ] );
	}

}
/// <summary>
/// pc Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class PcDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public PcDbModel() : base( PcTableInfo.Instance )
	{
	}

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId()
	{
		return Fields[ (int)( PcTableInfo.EFieldNumber.id ) ].Var.GetU64();
	}

	/// <summary>
	/// UserId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetUserId()
	{
		return Fields[ (int)( PcTableInfo.EFieldNumber.user_id ) ].Var.GetU64();
	}

	/// <summary>
	/// Name를( 을 ) 반환한다
	/// </summary>
	public String GetName()
	{
		return Fields[ (int)( PcTableInfo.EFieldNumber.name ) ].Var.GetStr();
	}

	/// <summary>
	/// Hp를( 을 ) 반환한다
	/// </summary>
	public Int32 GetHp()
	{
		return Fields[ (int)( PcTableInfo.EFieldNumber.hp ) ].Var.GetI32();
	}

	/// <summary>
	/// X를( 을 ) 반환한다
	/// </summary>
	public Single GetX()
	{
		return Fields[ (int)( PcTableInfo.EFieldNumber.x ) ].Var.GetF32();
	}

	/// <summary>
	/// Y를( 을 ) 반환한다
	/// </summary>
	public Single GetY()
	{
		return Fields[ (int)( PcTableInfo.EFieldNumber.y ) ].Var.GetF32();
	}

	/// <summary>
	/// Z를( 을 ) 반환한다
	/// </summary>
	public Single GetZ()
	{
		return Fields[ (int)( PcTableInfo.EFieldNumber.z ) ].Var.GetF32();
	}

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateField( (int)( PcTableInfo.EFieldNumber.id ) );
	}

	public void SetKeyId( UInt64 id )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateAndSetupKeyField( (int)( PcTableInfo.EFieldNumber.id ) );
	}

	/// <summary>
	/// UserId를( 을 ) 설정한다
	/// </summary>
	public void SetUserId( UInt64 user_id )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.user_id ) ].Var.SetU64( user_id );
		SetUpdateField( (int)( PcTableInfo.EFieldNumber.user_id ) );
	}

	public void SetKeyUserId( UInt64 user_id )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.user_id ) ].Var.SetU64( user_id );
		SetUpdateAndSetupKeyField( (int)( PcTableInfo.EFieldNumber.user_id ) );
	}

	/// <summary>
	/// Name를( 을 ) 설정한다
	/// </summary>
	public void SetName( String name )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.name ) ].Var.SetStr( name );
		SetUpdateField( (int)( PcTableInfo.EFieldNumber.name ) );
	}

	/// <summary>
	/// Hp를( 을 ) 설정한다
	/// </summary>
	public void SetHp( Int32 hp )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.hp ) ].Var.SetI32( hp );
		SetUpdateField( (int)( PcTableInfo.EFieldNumber.hp ) );
	}

	/// <summary>
	/// X를( 을 ) 설정한다
	/// </summary>
	public void SetX( Single x )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.x ) ].Var.SetF32( x );
		SetUpdateField( (int)( PcTableInfo.EFieldNumber.x ) );
	}

	/// <summary>
	/// Y를( 을 ) 설정한다
	/// </summary>
	public void SetY( Single y )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.y ) ].Var.SetF32( y );
		SetUpdateField( (int)( PcTableInfo.EFieldNumber.y ) );
	}

	/// <summary>
	/// Z를( 을 ) 설정한다
	/// </summary>
	public void SetZ( Single z )
	{
		Fields[ (int)( PcTableInfo.EFieldNumber.z ) ].Var.SetF32( z );
		SetUpdateField( (int)( PcTableInfo.EFieldNumber.z ) );
	}

}
/// <summary>
/// pc Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class Pc : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public PcDbModel dbModel = new();

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId() => dbModel.GetId();

	/// <summary>
	/// UserId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetUserId() => dbModel.GetUserId();

	/// <summary>
	/// Name를( 을 ) 반환한다
	/// </summary>
	public String GetName() => dbModel.GetName();

	/// <summary>
	/// Hp를( 을 ) 반환한다
	/// </summary>
	public Int32 GetHp() => dbModel.GetHp();

	/// <summary>
	/// X를( 을 ) 반환한다
	/// </summary>
	public Single GetX() => dbModel.GetX();

	/// <summary>
	/// Y를( 을 ) 반환한다
	/// </summary>
	public Single GetY() => dbModel.GetY();

	/// <summary>
	/// Z를( 을 ) 반환한다
	/// </summary>
	public Single GetZ() => dbModel.GetZ();

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id ) => dbModel.SetId( id );
	public void SetKeyId( UInt64 id ) => dbModel.SetKeyId( id );

	/// <summary>
	/// UserId를( 을 ) 설정한다
	/// </summary>
	public void SetUserId( UInt64 user_id ) => dbModel.SetUserId( user_id );
	public void SetKeyUserId( UInt64 user_id ) => dbModel.SetKeyUserId( user_id );

	/// <summary>
	/// Name를( 을 ) 설정한다
	/// </summary>
	public void SetName( String name ) => dbModel.SetName( name );
	/// <summary>
	/// Hp를( 을 ) 설정한다
	/// </summary>
	public void SetHp( Int32 hp ) => dbModel.SetHp( hp );
	/// <summary>
	/// X를( 을 ) 설정한다
	/// </summary>
	public void SetX( Single x ) => dbModel.SetX( x );
	/// <summary>
	/// Y를( 을 ) 설정한다
	/// </summary>
	public void SetY( Single y ) => dbModel.SetY( y );
	/// <summary>
	/// Z를( 을 ) 설정한다
	/// </summary>
	public void SetZ( Single z ) => dbModel.SetZ( z );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

