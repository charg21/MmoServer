/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// quest Db 테이블 정보
/// </summary>
public class QuestTableInfo : TableInfo
{
	/// <summary>
	/// quest 테이블 정보 전역 인스턴스
	/// </summary>
	public static QuestTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		owner_id,
		design_id,
		complete_count,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public QuestTableInfo() : base( "quest" )
	{
		AddFieldInfo( "owner_id"      , (int)( EFieldNumber.owner_id )      , EVariantType.U64, true  );
		AddFieldInfo( "design_id"     , (int)( EFieldNumber.design_id )     , EVariantType.U32, true  );
		AddFieldInfo( "complete_count", (int)( EFieldNumber.complete_count ), EVariantType.I32, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.owner_id ) ] );
		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.design_id ) ] );
	}

}
/// <summary>
/// quest Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class QuestDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public QuestDbModel() : base( QuestTableInfo.Instance )
	{
	}

	/// <summary>
	/// OwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetOwnerId()
	{
		return Fields[ (int)( QuestTableInfo.EFieldNumber.owner_id ) ].Var.GetU64();
	}

	/// <summary>
	/// DesignId를( 을 ) 반환한다
	/// </summary>
	public UInt32 GetDesignId()
	{
		return Fields[ (int)( QuestTableInfo.EFieldNumber.design_id ) ].Var.GetU32();
	}

	/// <summary>
	/// CompleteCount를( 을 ) 반환한다
	/// </summary>
	public Int32 GetCompleteCount()
	{
		return Fields[ (int)( QuestTableInfo.EFieldNumber.complete_count ) ].Var.GetI32();
	}

	/// <summary>
	/// OwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetOwnerId( UInt64 owner_id )
	{
		Fields[ (int)( QuestTableInfo.EFieldNumber.owner_id ) ].Var.SetU64( owner_id );
		SetUpdateField( (int)( QuestTableInfo.EFieldNumber.owner_id ) );
	}

	public void SetKeyOwnerId( UInt64 owner_id )
	{
		Fields[ (int)( QuestTableInfo.EFieldNumber.owner_id ) ].Var.SetU64( owner_id );
		SetUpdateAndSetupKeyField( (int)( QuestTableInfo.EFieldNumber.owner_id ) );
	}

	/// <summary>
	/// DesignId를( 을 ) 설정한다
	/// </summary>
	public void SetDesignId( UInt32 design_id )
	{
		Fields[ (int)( QuestTableInfo.EFieldNumber.design_id ) ].Var.SetU32( design_id );
		SetUpdateField( (int)( QuestTableInfo.EFieldNumber.design_id ) );
	}

	public void SetKeyDesignId( UInt32 design_id )
	{
		Fields[ (int)( QuestTableInfo.EFieldNumber.design_id ) ].Var.SetU32( design_id );
		SetUpdateAndSetupKeyField( (int)( QuestTableInfo.EFieldNumber.design_id ) );
	}

	/// <summary>
	/// CompleteCount를( 을 ) 설정한다
	/// </summary>
	public void SetCompleteCount( Int32 complete_count )
	{
		Fields[ (int)( QuestTableInfo.EFieldNumber.complete_count ) ].Var.SetI32( complete_count );
		SetUpdateField( (int)( QuestTableInfo.EFieldNumber.complete_count ) );
	}

}
/// <summary>
/// quest Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class Quest : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public QuestDbModel dbModel = new();

	/// <summary>
	/// OwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetOwnerId() => dbModel.GetOwnerId();

	/// <summary>
	/// DesignId를( 을 ) 반환한다
	/// </summary>
	public UInt32 GetDesignId() => dbModel.GetDesignId();

	/// <summary>
	/// CompleteCount를( 을 ) 반환한다
	/// </summary>
	public Int32 GetCompleteCount() => dbModel.GetCompleteCount();

	/// <summary>
	/// OwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetOwnerId( UInt64 owner_id ) => dbModel.SetOwnerId( owner_id );
	public void SetKeyOwnerId( UInt64 owner_id ) => dbModel.SetKeyOwnerId( owner_id );

	/// <summary>
	/// DesignId를( 을 ) 설정한다
	/// </summary>
	public void SetDesignId( UInt32 design_id ) => dbModel.SetDesignId( design_id );
	public void SetKeyDesignId( UInt32 design_id ) => dbModel.SetKeyDesignId( design_id );

	/// <summary>
	/// CompleteCount를( 을 ) 설정한다
	/// </summary>
	public void SetCompleteCount( Int32 complete_count ) => dbModel.SetCompleteCount( complete_count );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

