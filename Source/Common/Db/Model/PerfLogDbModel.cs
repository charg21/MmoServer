/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// perf_log Db 테이블 정보
/// </summary>
public class PerfLogTableInfo : TableInfo
{
	/// <summary>
	/// perf_log 테이블 정보 전역 인스턴스
	/// </summary>
	public static PerfLogTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		id,
		time,
		gc_gen0,
		gc_gen1,
		gc_gen2,
		memory,
		native_memory,
		working_set,
		system_call,
		context_switching,
		system_recv_bytes,
		system_sent_bytes,
		system_recv_packets,
		system_sent_packets,
		global_job_count,
		send_buffer_count,
		ccu,
		npc,
		cpu_usage,
		debug_log_count,
		info_log_count,
		critical_log_count,
		error_log_count,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public PerfLogTableInfo() : base( "perf_log" )
	{
		AddFieldInfo( "id"                 , (int)( EFieldNumber.id )                 , EVariantType.U64, true  );
		AddFieldInfo( "time"               , (int)( EFieldNumber.time )               , EVariantType.DateTime, false );
		AddFieldInfo( "gc_gen0"            , (int)( EFieldNumber.gc_gen0 )            , EVariantType.I64, false );
		AddFieldInfo( "gc_gen1"            , (int)( EFieldNumber.gc_gen1 )            , EVariantType.I64, false );
		AddFieldInfo( "gc_gen2"            , (int)( EFieldNumber.gc_gen2 )            , EVariantType.I64, false );
		AddFieldInfo( "memory"             , (int)( EFieldNumber.memory )             , EVariantType.Str, false );
		AddFieldInfo( "native_memory"      , (int)( EFieldNumber.native_memory )      , EVariantType.Str, false );
		AddFieldInfo( "working_set"        , (int)( EFieldNumber.working_set )        , EVariantType.I64, false );
		AddFieldInfo( "system_call"        , (int)( EFieldNumber.system_call )        , EVariantType.I64, false );
		AddFieldInfo( "context_switching"  , (int)( EFieldNumber.context_switching )  , EVariantType.I64, false );
		AddFieldInfo( "system_recv_bytes"  , (int)( EFieldNumber.system_recv_bytes )  , EVariantType.I64, false );
		AddFieldInfo( "system_sent_bytes"  , (int)( EFieldNumber.system_sent_bytes )  , EVariantType.I64, false );
		AddFieldInfo( "system_recv_packets", (int)( EFieldNumber.system_recv_packets ), EVariantType.I64, false );
		AddFieldInfo( "system_sent_packets", (int)( EFieldNumber.system_sent_packets ), EVariantType.I64, false );
		AddFieldInfo( "global_job_count"   , (int)( EFieldNumber.global_job_count )   , EVariantType.I64, false );
		AddFieldInfo( "send_buffer_count"  , (int)( EFieldNumber.send_buffer_count )  , EVariantType.I64, false );
		AddFieldInfo( "ccu"                , (int)( EFieldNumber.ccu )                , EVariantType.I32, false );
		AddFieldInfo( "npc"                , (int)( EFieldNumber.npc )                , EVariantType.I32, false );
		AddFieldInfo( "cpu_usage"          , (int)( EFieldNumber.cpu_usage )          , EVariantType.F32, false );
		AddFieldInfo( "debug_log_count"    , (int)( EFieldNumber.debug_log_count )    , EVariantType.I64, false );
		AddFieldInfo( "info_log_count"     , (int)( EFieldNumber.info_log_count )     , EVariantType.I64, false );
		AddFieldInfo( "critical_log_count" , (int)( EFieldNumber.critical_log_count ) , EVariantType.I64, false );
		AddFieldInfo( "error_log_count"    , (int)( EFieldNumber.error_log_count )    , EVariantType.I64, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.id ) ] );
	}

}
/// <summary>
/// perf_log Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class PerfLogDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public PerfLogDbModel() : base( PerfLogTableInfo.Instance )
	{
	}

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.id ) ].Var.GetU64();
	}

	/// <summary>
	/// Time를( 을 ) 반환한다
	/// </summary>
	public DateTime GetTime()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.time ) ].Var.GetDateTime();
	}

	/// <summary>
	/// GCGen0를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGCGen0()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.gc_gen0 ) ].Var.GetI64();
	}

	/// <summary>
	/// GCGen1를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGCGen1()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.gc_gen1 ) ].Var.GetI64();
	}

	/// <summary>
	/// GCGen2를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGCGen2()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.gc_gen2 ) ].Var.GetI64();
	}

	/// <summary>
	/// Memory를( 을 ) 반환한다
	/// </summary>
	public String GetMemory()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.memory ) ].Var.GetStr();
	}

	/// <summary>
	/// NativeMemory를( 을 ) 반환한다
	/// </summary>
	public String GetNativeMemory()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.native_memory ) ].Var.GetStr();
	}

	/// <summary>
	/// WorkingSet를( 을 ) 반환한다
	/// </summary>
	public Int64 GetWorkingSet()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.working_set ) ].Var.GetI64();
	}

	/// <summary>
	/// SystemCall를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemCall()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_call ) ].Var.GetI64();
	}

	/// <summary>
	/// ContextSwitching를( 을 ) 반환한다
	/// </summary>
	public Int64 GetContextSwitching()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.context_switching ) ].Var.GetI64();
	}

	/// <summary>
	/// SystemRecvBytes를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemRecvBytes()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_recv_bytes ) ].Var.GetI64();
	}

	/// <summary>
	/// SystemSentBytes를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemSentBytes()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_sent_bytes ) ].Var.GetI64();
	}

	/// <summary>
	/// SystemRecvPackets를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemRecvPackets()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_recv_packets ) ].Var.GetI64();
	}

	/// <summary>
	/// SystemSentPackets를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemSentPackets()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_sent_packets ) ].Var.GetI64();
	}

	/// <summary>
	/// GlobalJobCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGlobalJobCount()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.global_job_count ) ].Var.GetI64();
	}

	/// <summary>
	/// SendBufferCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSendBufferCount()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.send_buffer_count ) ].Var.GetI64();
	}

	/// <summary>
	/// Ccu를( 을 ) 반환한다
	/// </summary>
	public Int32 GetCcu()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.ccu ) ].Var.GetI32();
	}

	/// <summary>
	/// Npc를( 을 ) 반환한다
	/// </summary>
	public Int32 GetNpc()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.npc ) ].Var.GetI32();
	}

	/// <summary>
	/// CpuUsage를( 을 ) 반환한다
	/// </summary>
	public Single GetCpuUsage()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.cpu_usage ) ].Var.GetF32();
	}

	/// <summary>
	/// DebugLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetDebugLogCount()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.debug_log_count ) ].Var.GetI64();
	}

	/// <summary>
	/// InfoLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetInfoLogCount()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.info_log_count ) ].Var.GetI64();
	}

	/// <summary>
	/// CriticalLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetCriticalLogCount()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.critical_log_count ) ].Var.GetI64();
	}

	/// <summary>
	/// ErrorLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetErrorLogCount()
	{
		return Fields[ (int)( PerfLogTableInfo.EFieldNumber.error_log_count ) ].Var.GetI64();
	}

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.id ) );
	}

	public void SetKeyId( UInt64 id )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateAndSetupKeyField( (int)( PerfLogTableInfo.EFieldNumber.id ) );
	}

	/// <summary>
	/// Time를( 을 ) 설정한다
	/// </summary>
	public void SetTime( DateTime time )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.time ) ].Var.SetDateTime( time );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.time ) );
	}

	/// <summary>
	/// GCGen0를( 을 ) 설정한다
	/// </summary>
	public void SetGCGen0( Int64 gc_gen0 )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.gc_gen0 ) ].Var.SetI64( gc_gen0 );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.gc_gen0 ) );
	}

	/// <summary>
	/// GCGen1를( 을 ) 설정한다
	/// </summary>
	public void SetGCGen1( Int64 gc_gen1 )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.gc_gen1 ) ].Var.SetI64( gc_gen1 );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.gc_gen1 ) );
	}

	/// <summary>
	/// GCGen2를( 을 ) 설정한다
	/// </summary>
	public void SetGCGen2( Int64 gc_gen2 )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.gc_gen2 ) ].Var.SetI64( gc_gen2 );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.gc_gen2 ) );
	}

	/// <summary>
	/// Memory를( 을 ) 설정한다
	/// </summary>
	public void SetMemory( String memory )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.memory ) ].Var.SetStr( memory );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.memory ) );
	}

	/// <summary>
	/// NativeMemory를( 을 ) 설정한다
	/// </summary>
	public void SetNativeMemory( String native_memory )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.native_memory ) ].Var.SetStr( native_memory );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.native_memory ) );
	}

	/// <summary>
	/// WorkingSet를( 을 ) 설정한다
	/// </summary>
	public void SetWorkingSet( Int64 working_set )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.working_set ) ].Var.SetI64( working_set );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.working_set ) );
	}

	/// <summary>
	/// SystemCall를( 을 ) 설정한다
	/// </summary>
	public void SetSystemCall( Int64 system_call )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_call ) ].Var.SetI64( system_call );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.system_call ) );
	}

	/// <summary>
	/// ContextSwitching를( 을 ) 설정한다
	/// </summary>
	public void SetContextSwitching( Int64 context_switching )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.context_switching ) ].Var.SetI64( context_switching );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.context_switching ) );
	}

	/// <summary>
	/// SystemRecvBytes를( 을 ) 설정한다
	/// </summary>
	public void SetSystemRecvBytes( Int64 system_recv_bytes )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_recv_bytes ) ].Var.SetI64( system_recv_bytes );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.system_recv_bytes ) );
	}

	/// <summary>
	/// SystemSentBytes를( 을 ) 설정한다
	/// </summary>
	public void SetSystemSentBytes( Int64 system_sent_bytes )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_sent_bytes ) ].Var.SetI64( system_sent_bytes );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.system_sent_bytes ) );
	}

	/// <summary>
	/// SystemRecvPackets를( 을 ) 설정한다
	/// </summary>
	public void SetSystemRecvPackets( Int64 system_recv_packets )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_recv_packets ) ].Var.SetI64( system_recv_packets );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.system_recv_packets ) );
	}

	/// <summary>
	/// SystemSentPackets를( 을 ) 설정한다
	/// </summary>
	public void SetSystemSentPackets( Int64 system_sent_packets )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.system_sent_packets ) ].Var.SetI64( system_sent_packets );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.system_sent_packets ) );
	}

	/// <summary>
	/// GlobalJobCount를( 을 ) 설정한다
	/// </summary>
	public void SetGlobalJobCount( Int64 global_job_count )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.global_job_count ) ].Var.SetI64( global_job_count );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.global_job_count ) );
	}

	/// <summary>
	/// SendBufferCount를( 을 ) 설정한다
	/// </summary>
	public void SetSendBufferCount( Int64 send_buffer_count )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.send_buffer_count ) ].Var.SetI64( send_buffer_count );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.send_buffer_count ) );
	}

	/// <summary>
	/// Ccu를( 을 ) 설정한다
	/// </summary>
	public void SetCcu( Int32 ccu )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.ccu ) ].Var.SetI32( ccu );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.ccu ) );
	}

	/// <summary>
	/// Npc를( 을 ) 설정한다
	/// </summary>
	public void SetNpc( Int32 npc )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.npc ) ].Var.SetI32( npc );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.npc ) );
	}

	/// <summary>
	/// CpuUsage를( 을 ) 설정한다
	/// </summary>
	public void SetCpuUsage( Single cpu_usage )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.cpu_usage ) ].Var.SetF32( cpu_usage );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.cpu_usage ) );
	}

	/// <summary>
	/// DebugLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetDebugLogCount( Int64 debug_log_count )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.debug_log_count ) ].Var.SetI64( debug_log_count );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.debug_log_count ) );
	}

	/// <summary>
	/// InfoLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetInfoLogCount( Int64 info_log_count )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.info_log_count ) ].Var.SetI64( info_log_count );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.info_log_count ) );
	}

	/// <summary>
	/// CriticalLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetCriticalLogCount( Int64 critical_log_count )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.critical_log_count ) ].Var.SetI64( critical_log_count );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.critical_log_count ) );
	}

	/// <summary>
	/// ErrorLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetErrorLogCount( Int64 error_log_count )
	{
		Fields[ (int)( PerfLogTableInfo.EFieldNumber.error_log_count ) ].Var.SetI64( error_log_count );
		SetUpdateField( (int)( PerfLogTableInfo.EFieldNumber.error_log_count ) );
	}

}
/// <summary>
/// perf_log Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class PerfLog : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public PerfLogDbModel dbModel = new();

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId() => dbModel.GetId();

	/// <summary>
	/// Time를( 을 ) 반환한다
	/// </summary>
	public DateTime GetTime() => dbModel.GetTime();

	/// <summary>
	/// GCGen0를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGCGen0() => dbModel.GetGCGen0();

	/// <summary>
	/// GCGen1를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGCGen1() => dbModel.GetGCGen1();

	/// <summary>
	/// GCGen2를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGCGen2() => dbModel.GetGCGen2();

	/// <summary>
	/// Memory를( 을 ) 반환한다
	/// </summary>
	public String GetMemory() => dbModel.GetMemory();

	/// <summary>
	/// NativeMemory를( 을 ) 반환한다
	/// </summary>
	public String GetNativeMemory() => dbModel.GetNativeMemory();

	/// <summary>
	/// WorkingSet를( 을 ) 반환한다
	/// </summary>
	public Int64 GetWorkingSet() => dbModel.GetWorkingSet();

	/// <summary>
	/// SystemCall를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemCall() => dbModel.GetSystemCall();

	/// <summary>
	/// ContextSwitching를( 을 ) 반환한다
	/// </summary>
	public Int64 GetContextSwitching() => dbModel.GetContextSwitching();

	/// <summary>
	/// SystemRecvBytes를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemRecvBytes() => dbModel.GetSystemRecvBytes();

	/// <summary>
	/// SystemSentBytes를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemSentBytes() => dbModel.GetSystemSentBytes();

	/// <summary>
	/// SystemRecvPackets를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemRecvPackets() => dbModel.GetSystemRecvPackets();

	/// <summary>
	/// SystemSentPackets를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSystemSentPackets() => dbModel.GetSystemSentPackets();

	/// <summary>
	/// GlobalJobCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetGlobalJobCount() => dbModel.GetGlobalJobCount();

	/// <summary>
	/// SendBufferCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetSendBufferCount() => dbModel.GetSendBufferCount();

	/// <summary>
	/// Ccu를( 을 ) 반환한다
	/// </summary>
	public Int32 GetCcu() => dbModel.GetCcu();

	/// <summary>
	/// Npc를( 을 ) 반환한다
	/// </summary>
	public Int32 GetNpc() => dbModel.GetNpc();

	/// <summary>
	/// CpuUsage를( 을 ) 반환한다
	/// </summary>
	public Single GetCpuUsage() => dbModel.GetCpuUsage();

	/// <summary>
	/// DebugLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetDebugLogCount() => dbModel.GetDebugLogCount();

	/// <summary>
	/// InfoLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetInfoLogCount() => dbModel.GetInfoLogCount();

	/// <summary>
	/// CriticalLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetCriticalLogCount() => dbModel.GetCriticalLogCount();

	/// <summary>
	/// ErrorLogCount를( 을 ) 반환한다
	/// </summary>
	public Int64 GetErrorLogCount() => dbModel.GetErrorLogCount();

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id ) => dbModel.SetId( id );
	public void SetKeyId( UInt64 id ) => dbModel.SetKeyId( id );

	/// <summary>
	/// Time를( 을 ) 설정한다
	/// </summary>
	public void SetTime( DateTime time ) => dbModel.SetTime( time );
	/// <summary>
	/// GCGen0를( 을 ) 설정한다
	/// </summary>
	public void SetGCGen0( Int64 gc_gen0 ) => dbModel.SetGCGen0( gc_gen0 );
	/// <summary>
	/// GCGen1를( 을 ) 설정한다
	/// </summary>
	public void SetGCGen1( Int64 gc_gen1 ) => dbModel.SetGCGen1( gc_gen1 );
	/// <summary>
	/// GCGen2를( 을 ) 설정한다
	/// </summary>
	public void SetGCGen2( Int64 gc_gen2 ) => dbModel.SetGCGen2( gc_gen2 );
	/// <summary>
	/// Memory를( 을 ) 설정한다
	/// </summary>
	public void SetMemory( String memory ) => dbModel.SetMemory( memory );
	/// <summary>
	/// NativeMemory를( 을 ) 설정한다
	/// </summary>
	public void SetNativeMemory( String native_memory ) => dbModel.SetNativeMemory( native_memory );
	/// <summary>
	/// WorkingSet를( 을 ) 설정한다
	/// </summary>
	public void SetWorkingSet( Int64 working_set ) => dbModel.SetWorkingSet( working_set );
	/// <summary>
	/// SystemCall를( 을 ) 설정한다
	/// </summary>
	public void SetSystemCall( Int64 system_call ) => dbModel.SetSystemCall( system_call );
	/// <summary>
	/// ContextSwitching를( 을 ) 설정한다
	/// </summary>
	public void SetContextSwitching( Int64 context_switching ) => dbModel.SetContextSwitching( context_switching );
	/// <summary>
	/// SystemRecvBytes를( 을 ) 설정한다
	/// </summary>
	public void SetSystemRecvBytes( Int64 system_recv_bytes ) => dbModel.SetSystemRecvBytes( system_recv_bytes );
	/// <summary>
	/// SystemSentBytes를( 을 ) 설정한다
	/// </summary>
	public void SetSystemSentBytes( Int64 system_sent_bytes ) => dbModel.SetSystemSentBytes( system_sent_bytes );
	/// <summary>
	/// SystemRecvPackets를( 을 ) 설정한다
	/// </summary>
	public void SetSystemRecvPackets( Int64 system_recv_packets ) => dbModel.SetSystemRecvPackets( system_recv_packets );
	/// <summary>
	/// SystemSentPackets를( 을 ) 설정한다
	/// </summary>
	public void SetSystemSentPackets( Int64 system_sent_packets ) => dbModel.SetSystemSentPackets( system_sent_packets );
	/// <summary>
	/// GlobalJobCount를( 을 ) 설정한다
	/// </summary>
	public void SetGlobalJobCount( Int64 global_job_count ) => dbModel.SetGlobalJobCount( global_job_count );
	/// <summary>
	/// SendBufferCount를( 을 ) 설정한다
	/// </summary>
	public void SetSendBufferCount( Int64 send_buffer_count ) => dbModel.SetSendBufferCount( send_buffer_count );
	/// <summary>
	/// Ccu를( 을 ) 설정한다
	/// </summary>
	public void SetCcu( Int32 ccu ) => dbModel.SetCcu( ccu );
	/// <summary>
	/// Npc를( 을 ) 설정한다
	/// </summary>
	public void SetNpc( Int32 npc ) => dbModel.SetNpc( npc );
	/// <summary>
	/// CpuUsage를( 을 ) 설정한다
	/// </summary>
	public void SetCpuUsage( Single cpu_usage ) => dbModel.SetCpuUsage( cpu_usage );
	/// <summary>
	/// DebugLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetDebugLogCount( Int64 debug_log_count ) => dbModel.SetDebugLogCount( debug_log_count );
	/// <summary>
	/// InfoLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetInfoLogCount( Int64 info_log_count ) => dbModel.SetInfoLogCount( info_log_count );
	/// <summary>
	/// CriticalLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetCriticalLogCount( Int64 critical_log_count ) => dbModel.SetCriticalLogCount( critical_log_count );
	/// <summary>
	/// ErrorLogCount를( 을 ) 설정한다
	/// </summary>
	public void SetErrorLogCount( Int64 error_log_count ) => dbModel.SetErrorLogCount( error_log_count );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

