/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// token Db 테이블 정보
/// </summary>
public class TokenTableInfo : TableInfo
{
	/// <summary>
	/// token 테이블 정보 전역 인스턴스
	/// </summary>
	public static TokenTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		user_id,
		id,
		value,
		expired,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public TokenTableInfo() : base( "token" )
	{
		AddFieldInfo( "user_id", (int)( EFieldNumber.user_id ), EVariantType.U64, true  );
		AddFieldInfo( "id"     , (int)( EFieldNumber.id )     , EVariantType.I32, true  );
		AddFieldInfo( "value"  , (int)( EFieldNumber.value )  , EVariantType.U64, false );
		AddFieldInfo( "expired", (int)( EFieldNumber.expired ), EVariantType.DateTime, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.user_id ) ] );
		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.id ) ] );
	}

}
/// <summary>
/// token Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class TokenDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public TokenDbModel() : base( TokenTableInfo.Instance )
	{
	}

	/// <summary>
	/// UserId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetUserId()
	{
		return Fields[ (int)( TokenTableInfo.EFieldNumber.user_id ) ].Var.GetU64();
	}

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public Int32 GetId()
	{
		return Fields[ (int)( TokenTableInfo.EFieldNumber.id ) ].Var.GetI32();
	}

	/// <summary>
	/// Value를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetValue()
	{
		return Fields[ (int)( TokenTableInfo.EFieldNumber.value ) ].Var.GetU64();
	}

	/// <summary>
	/// Expired를( 을 ) 반환한다
	/// </summary>
	public DateTime GetExpired()
	{
		return Fields[ (int)( TokenTableInfo.EFieldNumber.expired ) ].Var.GetDateTime();
	}

	/// <summary>
	/// UserId를( 을 ) 설정한다
	/// </summary>
	public void SetUserId( UInt64 user_id )
	{
		Fields[ (int)( TokenTableInfo.EFieldNumber.user_id ) ].Var.SetU64( user_id );
		SetUpdateField( (int)( TokenTableInfo.EFieldNumber.user_id ) );
	}

	public void SetKeyUserId( UInt64 user_id )
	{
		Fields[ (int)( TokenTableInfo.EFieldNumber.user_id ) ].Var.SetU64( user_id );
		SetUpdateAndSetupKeyField( (int)( TokenTableInfo.EFieldNumber.user_id ) );
	}

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( Int32 id )
	{
		Fields[ (int)( TokenTableInfo.EFieldNumber.id ) ].Var.SetI32( id );
		SetUpdateField( (int)( TokenTableInfo.EFieldNumber.id ) );
	}

	public void SetKeyId( Int32 id )
	{
		Fields[ (int)( TokenTableInfo.EFieldNumber.id ) ].Var.SetI32( id );
		SetUpdateAndSetupKeyField( (int)( TokenTableInfo.EFieldNumber.id ) );
	}

	/// <summary>
	/// Value를( 을 ) 설정한다
	/// </summary>
	public void SetValue( UInt64 value )
	{
		Fields[ (int)( TokenTableInfo.EFieldNumber.value ) ].Var.SetU64( value );
		SetUpdateField( (int)( TokenTableInfo.EFieldNumber.value ) );
	}

	/// <summary>
	/// Expired를( 을 ) 설정한다
	/// </summary>
	public void SetExpired( DateTime expired )
	{
		Fields[ (int)( TokenTableInfo.EFieldNumber.expired ) ].Var.SetDateTime( expired );
		SetUpdateField( (int)( TokenTableInfo.EFieldNumber.expired ) );
	}

}
/// <summary>
/// token Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class Token : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public TokenDbModel dbModel = new();

	/// <summary>
	/// UserId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetUserId() => dbModel.GetUserId();

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public Int32 GetId() => dbModel.GetId();

	/// <summary>
	/// Value를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetValue() => dbModel.GetValue();

	/// <summary>
	/// Expired를( 을 ) 반환한다
	/// </summary>
	public DateTime GetExpired() => dbModel.GetExpired();

	/// <summary>
	/// UserId를( 을 ) 설정한다
	/// </summary>
	public void SetUserId( UInt64 user_id ) => dbModel.SetUserId( user_id );
	public void SetKeyUserId( UInt64 user_id ) => dbModel.SetKeyUserId( user_id );

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( Int32 id ) => dbModel.SetId( id );
	public void SetKeyId( Int32 id ) => dbModel.SetKeyId( id );

	/// <summary>
	/// Value를( 을 ) 설정한다
	/// </summary>
	public void SetValue( UInt64 value ) => dbModel.SetValue( value );
	/// <summary>
	/// Expired를( 을 ) 설정한다
	/// </summary>
	public void SetExpired( DateTime expired ) => dbModel.SetExpired( expired );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

