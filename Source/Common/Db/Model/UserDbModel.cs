/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// user Db 테이블 정보
/// </summary>
public class UserTableInfo : TableInfo
{
	/// <summary>
	/// user 테이블 정보 전역 인스턴스
	/// </summary>
	public static UserTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		id,
		pc_id,
		account,
		password,
		mail,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public UserTableInfo() : base( "user" )
	{
		AddFieldInfo( "id"      , (int)( EFieldNumber.id )      , EVariantType.U64, true  );
		AddFieldInfo( "pc_id"   , (int)( EFieldNumber.pc_id )   , EVariantType.U64, false );
		AddFieldInfo( "account" , (int)( EFieldNumber.account ) , EVariantType.Str, true  );
		AddFieldInfo( "password", (int)( EFieldNumber.password ), EVariantType.Str, false );
		AddFieldInfo( "mail"    , (int)( EFieldNumber.mail )    , EVariantType.Str, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.id ) ] );
		AddIndexInfo( "account", FieldInfos[ (int)( EFieldNumber.account ) ] );
	}

}
/// <summary>
/// user Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class UserDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public UserDbModel() : base( UserTableInfo.Instance )
	{
	}

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId()
	{
		return Fields[ (int)( UserTableInfo.EFieldNumber.id ) ].Var.GetU64();
	}

	/// <summary>
	/// PcId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetPcId()
	{
		return Fields[ (int)( UserTableInfo.EFieldNumber.pc_id ) ].Var.GetU64();
	}

	/// <summary>
	/// Account를( 을 ) 반환한다
	/// </summary>
	public String GetAccount()
	{
		return Fields[ (int)( UserTableInfo.EFieldNumber.account ) ].Var.GetStr();
	}

	/// <summary>
	/// Password를( 을 ) 반환한다
	/// </summary>
	public String GetPassword()
	{
		return Fields[ (int)( UserTableInfo.EFieldNumber.password ) ].Var.GetStr();
	}

	/// <summary>
	/// Mail를( 을 ) 반환한다
	/// </summary>
	public String GetMail()
	{
		return Fields[ (int)( UserTableInfo.EFieldNumber.mail ) ].Var.GetStr();
	}

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id )
	{
		Fields[ (int)( UserTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateField( (int)( UserTableInfo.EFieldNumber.id ) );
	}

	public void SetKeyId( UInt64 id )
	{
		Fields[ (int)( UserTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateAndSetupKeyField( (int)( UserTableInfo.EFieldNumber.id ) );
	}

	/// <summary>
	/// PcId를( 을 ) 설정한다
	/// </summary>
	public void SetPcId( UInt64 pc_id )
	{
		Fields[ (int)( UserTableInfo.EFieldNumber.pc_id ) ].Var.SetU64( pc_id );
		SetUpdateField( (int)( UserTableInfo.EFieldNumber.pc_id ) );
	}

	/// <summary>
	/// Account를( 을 ) 설정한다
	/// </summary>
	public void SetAccount( String account )
	{
		Fields[ (int)( UserTableInfo.EFieldNumber.account ) ].Var.SetStr( account );
		SetUpdateField( (int)( UserTableInfo.EFieldNumber.account ) );
	}

	public void SetKeyAccount( String account )
	{
		Fields[ (int)( UserTableInfo.EFieldNumber.account ) ].Var.SetStr( account );
		SetUpdateAndSetupKeyField( (int)( UserTableInfo.EFieldNumber.account ) );
	}

	/// <summary>
	/// Password를( 을 ) 설정한다
	/// </summary>
	public void SetPassword( String password )
	{
		Fields[ (int)( UserTableInfo.EFieldNumber.password ) ].Var.SetStr( password );
		SetUpdateField( (int)( UserTableInfo.EFieldNumber.password ) );
	}

	/// <summary>
	/// Mail를( 을 ) 설정한다
	/// </summary>
	public void SetMail( String mail )
	{
		Fields[ (int)( UserTableInfo.EFieldNumber.mail ) ].Var.SetStr( mail );
		SetUpdateField( (int)( UserTableInfo.EFieldNumber.mail ) );
	}

}
/// <summary>
/// user Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class User : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public UserDbModel dbModel = new();

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId() => dbModel.GetId();

	/// <summary>
	/// PcId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetPcId() => dbModel.GetPcId();

	/// <summary>
	/// Account를( 을 ) 반환한다
	/// </summary>
	public String GetAccount() => dbModel.GetAccount();

	/// <summary>
	/// Password를( 을 ) 반환한다
	/// </summary>
	public String GetPassword() => dbModel.GetPassword();

	/// <summary>
	/// Mail를( 을 ) 반환한다
	/// </summary>
	public String GetMail() => dbModel.GetMail();

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id ) => dbModel.SetId( id );
	public void SetKeyId( UInt64 id ) => dbModel.SetKeyId( id );

	/// <summary>
	/// PcId를( 을 ) 설정한다
	/// </summary>
	public void SetPcId( UInt64 pc_id ) => dbModel.SetPcId( pc_id );
	/// <summary>
	/// Account를( 을 ) 설정한다
	/// </summary>
	public void SetAccount( String account ) => dbModel.SetAccount( account );
	public void SetKeyAccount( String account ) => dbModel.SetKeyAccount( account );

	/// <summary>
	/// Password를( 을 ) 설정한다
	/// </summary>
	public void SetPassword( String password ) => dbModel.SetPassword( password );
	/// <summary>
	/// Mail를( 을 ) 설정한다
	/// </summary>
	public void SetMail( String mail ) => dbModel.SetMail( mail );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

