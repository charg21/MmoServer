/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// item Db 테이블 정보
/// </summary>
public class ItemTableInfo : TableInfo
{
	/// <summary>
	/// item 테이블 정보 전역 인스턴스
	/// </summary>
	public static ItemTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		owner_id,
		root_owner_id,
		id,
		design_id,
		count,
		locked,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public ItemTableInfo() : base( "item" )
	{
		AddFieldInfo( "owner_id"     , (int)( EFieldNumber.owner_id )     , EVariantType.U64, true  );
		AddFieldInfo( "root_owner_id", (int)( EFieldNumber.root_owner_id ), EVariantType.U64, true  );
		AddFieldInfo( "id"           , (int)( EFieldNumber.id )           , EVariantType.U64, true  );
		AddFieldInfo( "design_id"    , (int)( EFieldNumber.design_id )    , EVariantType.U32, false );
		AddFieldInfo( "count"        , (int)( EFieldNumber.count )        , EVariantType.I32, false );
		AddFieldInfo( "locked"       , (int)( EFieldNumber.locked )       , EVariantType.Bool, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.owner_id ) ] );
		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.root_owner_id ) ] );
		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.id ) ] );
		AddIndexInfo( "OWNER_AND_ID", FieldInfos[ (int)( EFieldNumber.owner_id ) ] );
		AddIndexInfo( "OWNER_AND_ID", FieldInfos[ (int)( EFieldNumber.id ) ] );
	}

}
/// <summary>
/// item Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class ItemDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public ItemDbModel() : base( ItemTableInfo.Instance )
	{
	}

	/// <summary>
	/// OwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetOwnerId()
	{
		return Fields[ (int)( ItemTableInfo.EFieldNumber.owner_id ) ].Var.GetU64();
	}

	/// <summary>
	/// RootOwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetRootOwnerId()
	{
		return Fields[ (int)( ItemTableInfo.EFieldNumber.root_owner_id ) ].Var.GetU64();
	}

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId()
	{
		return Fields[ (int)( ItemTableInfo.EFieldNumber.id ) ].Var.GetU64();
	}

	/// <summary>
	/// DesignId를( 을 ) 반환한다
	/// </summary>
	public UInt32 GetDesignId()
	{
		return Fields[ (int)( ItemTableInfo.EFieldNumber.design_id ) ].Var.GetU32();
	}

	/// <summary>
	/// Count를( 을 ) 반환한다
	/// </summary>
	public Int32 GetCount()
	{
		return Fields[ (int)( ItemTableInfo.EFieldNumber.count ) ].Var.GetI32();
	}

	/// <summary>
	/// Locked를( 을 ) 반환한다
	/// </summary>
	public Boolean GetLocked()
	{
		return Fields[ (int)( ItemTableInfo.EFieldNumber.locked ) ].Var.GetBool();
	}

	/// <summary>
	/// OwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetOwnerId( UInt64 owner_id )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.owner_id ) ].Var.SetU64( owner_id );
		SetUpdateField( (int)( ItemTableInfo.EFieldNumber.owner_id ) );
	}

	public void SetKeyOwnerId( UInt64 owner_id )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.owner_id ) ].Var.SetU64( owner_id );
		SetUpdateAndSetupKeyField( (int)( ItemTableInfo.EFieldNumber.owner_id ) );
	}

	/// <summary>
	/// RootOwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetRootOwnerId( UInt64 root_owner_id )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.root_owner_id ) ].Var.SetU64( root_owner_id );
		SetUpdateField( (int)( ItemTableInfo.EFieldNumber.root_owner_id ) );
	}

	public void SetKeyRootOwnerId( UInt64 root_owner_id )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.root_owner_id ) ].Var.SetU64( root_owner_id );
		SetUpdateAndSetupKeyField( (int)( ItemTableInfo.EFieldNumber.root_owner_id ) );
	}

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateField( (int)( ItemTableInfo.EFieldNumber.id ) );
	}

	public void SetKeyId( UInt64 id )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.id ) ].Var.SetU64( id );
		SetUpdateAndSetupKeyField( (int)( ItemTableInfo.EFieldNumber.id ) );
	}

	/// <summary>
	/// DesignId를( 을 ) 설정한다
	/// </summary>
	public void SetDesignId( UInt32 design_id )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.design_id ) ].Var.SetU32( design_id );
		SetUpdateField( (int)( ItemTableInfo.EFieldNumber.design_id ) );
	}

	/// <summary>
	/// Count를( 을 ) 설정한다
	/// </summary>
	public void SetCount( Int32 count )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.count ) ].Var.SetI32( count );
		SetUpdateField( (int)( ItemTableInfo.EFieldNumber.count ) );
	}

	/// <summary>
	/// Locked를( 을 ) 설정한다
	/// </summary>
	public void SetLocked( Boolean locked )
	{
		Fields[ (int)( ItemTableInfo.EFieldNumber.locked ) ].Var.SetBool( locked );
		SetUpdateField( (int)( ItemTableInfo.EFieldNumber.locked ) );
	}

}
/// <summary>
/// item Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class Item : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public ItemDbModel dbModel = new();

	/// <summary>
	/// OwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetOwnerId() => dbModel.GetOwnerId();

	/// <summary>
	/// RootOwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetRootOwnerId() => dbModel.GetRootOwnerId();

	/// <summary>
	/// Id를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetId() => dbModel.GetId();

	/// <summary>
	/// DesignId를( 을 ) 반환한다
	/// </summary>
	public UInt32 GetDesignId() => dbModel.GetDesignId();

	/// <summary>
	/// Count를( 을 ) 반환한다
	/// </summary>
	public Int32 GetCount() => dbModel.GetCount();

	/// <summary>
	/// Locked를( 을 ) 반환한다
	/// </summary>
	public Boolean GetLocked() => dbModel.GetLocked();

	/// <summary>
	/// OwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetOwnerId( UInt64 owner_id ) => dbModel.SetOwnerId( owner_id );
	public void SetKeyOwnerId( UInt64 owner_id ) => dbModel.SetKeyOwnerId( owner_id );

	/// <summary>
	/// RootOwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetRootOwnerId( UInt64 root_owner_id ) => dbModel.SetRootOwnerId( root_owner_id );
	public void SetKeyRootOwnerId( UInt64 root_owner_id ) => dbModel.SetKeyRootOwnerId( root_owner_id );

	/// <summary>
	/// Id를( 을 ) 설정한다
	/// </summary>
	public void SetId( UInt64 id ) => dbModel.SetId( id );
	public void SetKeyId( UInt64 id ) => dbModel.SetKeyId( id );

	/// <summary>
	/// DesignId를( 을 ) 설정한다
	/// </summary>
	public void SetDesignId( UInt32 design_id ) => dbModel.SetDesignId( design_id );
	/// <summary>
	/// Count를( 을 ) 설정한다
	/// </summary>
	public void SetCount( Int32 count ) => dbModel.SetCount( count );
	/// <summary>
	/// Locked를( 을 ) 설정한다
	/// </summary>
	public void SetLocked( Boolean locked ) => dbModel.SetLocked( locked );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

