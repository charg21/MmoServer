/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// stat Db 테이블 정보
/// </summary>
public class StatTableInfo : TableInfo
{
	/// <summary>
	/// stat 테이블 정보 전역 인스턴스
	/// </summary>
	public static StatTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		pc_id,
		type,
		value,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public StatTableInfo() : base( "stat" )
	{
		AddFieldInfo( "pc_id", (int)( EFieldNumber.pc_id ), EVariantType.U64, true  );
		AddFieldInfo( "type" , (int)( EFieldNumber.type ) , EVariantType.U64, true  );
		AddFieldInfo( "value", (int)( EFieldNumber.value ), EVariantType.U64, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.pc_id ) ] );
		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.type ) ] );
	}

}
/// <summary>
/// stat Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class StatDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public StatDbModel() : base( StatTableInfo.Instance )
	{
	}

	/// <summary>
	/// PcId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetPcId()
	{
		return Fields[ (int)( StatTableInfo.EFieldNumber.pc_id ) ].Var.GetU64();
	}

	/// <summary>
	/// Type를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetType()
	{
		return Fields[ (int)( StatTableInfo.EFieldNumber.type ) ].Var.GetU64();
	}

	/// <summary>
	/// Value를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetValue()
	{
		return Fields[ (int)( StatTableInfo.EFieldNumber.value ) ].Var.GetU64();
	}

	/// <summary>
	/// PcId를( 을 ) 설정한다
	/// </summary>
	public void SetPcId( UInt64 pc_id )
	{
		Fields[ (int)( StatTableInfo.EFieldNumber.pc_id ) ].Var.SetU64( pc_id );
		SetUpdateField( (int)( StatTableInfo.EFieldNumber.pc_id ) );
	}

	public void SetKeyPcId( UInt64 pc_id )
	{
		Fields[ (int)( StatTableInfo.EFieldNumber.pc_id ) ].Var.SetU64( pc_id );
		SetUpdateAndSetupKeyField( (int)( StatTableInfo.EFieldNumber.pc_id ) );
	}

	/// <summary>
	/// Type를( 을 ) 설정한다
	/// </summary>
	public void SetType( UInt64 type )
	{
		Fields[ (int)( StatTableInfo.EFieldNumber.type ) ].Var.SetU64( type );
		SetUpdateField( (int)( StatTableInfo.EFieldNumber.type ) );
	}

	public void SetKeyType( UInt64 type )
	{
		Fields[ (int)( StatTableInfo.EFieldNumber.type ) ].Var.SetU64( type );
		SetUpdateAndSetupKeyField( (int)( StatTableInfo.EFieldNumber.type ) );
	}

	/// <summary>
	/// Value를( 을 ) 설정한다
	/// </summary>
	public void SetValue( UInt64 value )
	{
		Fields[ (int)( StatTableInfo.EFieldNumber.value ) ].Var.SetU64( value );
		SetUpdateField( (int)( StatTableInfo.EFieldNumber.value ) );
	}

}
/// <summary>
/// stat Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class Stat : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public StatDbModel dbModel = new();

	/// <summary>
	/// PcId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetPcId() => dbModel.GetPcId();

	/// <summary>
	/// Type를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetType() => dbModel.GetType();

	/// <summary>
	/// Value를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetValue() => dbModel.GetValue();

	/// <summary>
	/// PcId를( 을 ) 설정한다
	/// </summary>
	public void SetPcId( UInt64 pc_id ) => dbModel.SetPcId( pc_id );
	public void SetKeyPcId( UInt64 pc_id ) => dbModel.SetKeyPcId( pc_id );

	/// <summary>
	/// Type를( 을 ) 설정한다
	/// </summary>
	public void SetType( UInt64 type ) => dbModel.SetType( type );
	public void SetKeyType( UInt64 type ) => dbModel.SetKeyType( type );

	/// <summary>
	/// Value를( 을 ) 설정한다
	/// </summary>
	public void SetValue( UInt64 value ) => dbModel.SetValue( value );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

