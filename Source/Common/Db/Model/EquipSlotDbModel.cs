/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using Mala.Threading;
using MySql.Data.MySqlClient;
using System;


/// <summary>
/// equipment_slot Db 테이블 정보
/// </summary>
public class EquipSlotTableInfo : TableInfo
{
	/// <summary>
	/// equipment_slot 테이블 정보 전역 인스턴스
	/// </summary>
	public static EquipSlotTableInfo Instance { get; set; } = new();

	public enum EFieldNumber
	{
		owner_id,
		slot,
		item_id,
		max,
	}

	/// <summary>
	/// 생성자
	/// </summary>
	public EquipSlotTableInfo() : base( "equip_slot" )
	{
		AddFieldInfo( "owner_id", (int)( EFieldNumber.owner_id ), EVariantType.U64, true  );
		AddFieldInfo( "slot"    , (int)( EFieldNumber.slot )    , EVariantType.U16, true  );
		AddFieldInfo( "item_id" , (int)( EFieldNumber.item_id ) , EVariantType.U64, false );

		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.owner_id ) ] );
		AddIndexInfo( "PRIMARY", FieldInfos[ (int)( EFieldNumber.slot ) ] );
	}

}
/// <summary>
/// equipment_slot Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public partial class EquipSlotDbModel : DbModel
{
	/// <summary>
	/// 생성자
	/// </summary>
	public EquipSlotDbModel() : base( EquipSlotTableInfo.Instance )
	{
	}

	/// <summary>
	/// OwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetOwnerId()
	{
		return Fields[ (int)( EquipSlotTableInfo.EFieldNumber.owner_id ) ].Var.GetU64();
	}

	/// <summary>
	/// Slot를( 을 ) 반환한다
	/// </summary>
	public UInt16 GetSlot()
	{
		return Fields[ (int)( EquipSlotTableInfo.EFieldNumber.slot ) ].Var.GetU16();
	}

	/// <summary>
	/// ItemId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetItemId()
	{
		return Fields[ (int)( EquipSlotTableInfo.EFieldNumber.item_id ) ].Var.GetU64();
	}

	/// <summary>
	/// OwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetOwnerId( UInt64 owner_id )
	{
		Fields[ (int)( EquipSlotTableInfo.EFieldNumber.owner_id ) ].Var.SetU64( owner_id );
		SetUpdateField( (int)( EquipSlotTableInfo.EFieldNumber.owner_id ) );
	}

	public void SetKeyOwnerId( UInt64 owner_id )
	{
		Fields[ (int)( EquipSlotTableInfo.EFieldNumber.owner_id ) ].Var.SetU64( owner_id );
		SetUpdateAndSetupKeyField( (int)( EquipSlotTableInfo.EFieldNumber.owner_id ) );
	}

	/// <summary>
	/// Slot를( 을 ) 설정한다
	/// </summary>
	public void SetSlot( UInt16 slot )
	{
		Fields[ (int)( EquipSlotTableInfo.EFieldNumber.slot ) ].Var.SetU16( slot );
		SetUpdateField( (int)( EquipSlotTableInfo.EFieldNumber.slot ) );
	}

	public void SetKeySlot( UInt16 slot )
	{
		Fields[ (int)( EquipSlotTableInfo.EFieldNumber.slot ) ].Var.SetU16( slot );
		SetUpdateAndSetupKeyField( (int)( EquipSlotTableInfo.EFieldNumber.slot ) );
	}

	/// <summary>
	/// ItemId를( 을 ) 설정한다
	/// </summary>
	public void SetItemId( UInt64 item_id )
	{
		Fields[ (int)( EquipSlotTableInfo.EFieldNumber.item_id ) ].Var.SetU64( item_id );
		SetUpdateField( (int)( EquipSlotTableInfo.EFieldNumber.item_id ) );
	}

}
/// <summary>
/// equip_slot Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class EquipSlot : IDbModel
{
	/// <summary>
	/// 작업 실행기
	/// </summary>
	public Actor JobExecutor => throw new NotImplementedException();

	/// <summary>
	/// Db 동기화 객체 인터페이스 구현체
	/// </summary>
	public DbModel DbModel => dbModel;

	/// <summary>
	/// 원본
	/// </summary>
	public IDbModel Origin { get => dbModel._root; set => dbModel._root = value; }

	/// <summary>
	/// Db 동기화 객체
	/// </summary>
	public EquipSlotDbModel dbModel = new();

	/// <summary>
	/// OwnerId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetOwnerId() => dbModel.GetOwnerId();

	/// <summary>
	/// Slot를( 을 ) 반환한다
	/// </summary>
	public UInt16 GetSlot() => dbModel.GetSlot();

	/// <summary>
	/// ItemId를( 을 ) 반환한다
	/// </summary>
	public UInt64 GetItemId() => dbModel.GetItemId();

	/// <summary>
	/// OwnerId를( 을 ) 설정한다
	/// </summary>
	public void SetOwnerId( UInt64 owner_id ) => dbModel.SetOwnerId( owner_id );
	public void SetKeyOwnerId( UInt64 owner_id ) => dbModel.SetKeyOwnerId( owner_id );

	/// <summary>
	/// Slot를( 을 ) 설정한다
	/// </summary>
	public void SetSlot( UInt16 slot ) => dbModel.SetSlot( slot );
	public void SetKeySlot( UInt16 slot ) => dbModel.SetKeySlot( slot );

	/// <summary>
	/// ItemId를( 을 ) 설정한다
	/// </summary>
	public void SetItemId( UInt64 item_id ) => dbModel.SetItemId( item_id );
	/// <summary>
	/// 동기화한다.
	/// </summary>
	 public void Sync( TxContext tx, EQueryType queryType ) => dbModel.Sync( tx, queryType );
	/// <summary>
	/// Select
	/// </summary>
	public EDbResult Select( ref DbExecutionContext ctx ) => dbModel.Select( ref ctx );
	/// <summary>
	/// Insert
	/// </summary>
	public bool Insert( ref DbExecutionContext ctx ) => dbModel.Insert( ref ctx );
	/// <summary>
	/// 키 컬럼을 복사한다
	/// </summary>
	public void CopyToOnlyKeys( IDbModel model ) => dbModel.CopyToOnlyKeys( model );
	/// <summary>
	/// 필드를 설정한다
	/// </summary>
	public void SetField( int v1, string v2 ) => dbModel.SetField( v1, v2 );
	/// <summary>
	/// 필드 참조를 획득한다
	/// </summary>
	public ref Field GetField( int no ) => ref dbModel.GetField( no );
	/// <summary>
	/// 모든 필드를 바인딩한다
	/// </summary>
	public void BindAllField() => dbModel.BindAllField();
}

