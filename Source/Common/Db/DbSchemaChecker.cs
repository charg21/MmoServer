﻿using Mala.Core;
using Mala.Db;
using Mala.Db.Model;

/// <summary>
/// DB 스키마 유효성 검증기
/// </summary>
public static class DbSchemaChecker
{
    public static void Initialize()
    {
        CheckDbSchema();
    }

    /// <summary>
    /// 유효성을 검증한다.
    /// </summary>
    public static void CheckDbSchema()
    {
        Type dbModelType = typeof( DbModel );
        foreach ( var assembly in AppDomain.CurrentDomain.GetAssemblies() )
        {
            foreach ( var type in assembly.GetTypes() )
            {
                if ( !dbModelType.IsAssignableFrom( type ) || type == dbModelType )
                    continue;

                //DbExectuor.Post( ctx =>
                //{
                //    var dummy = Activator.CreateInstance( type ) as DbModel;
                //    dummy.BindAllField();
                //    var dbResult = dummy.SelectAll< type >( ref ctx, new() { limit = 1 } ).Item1;
                //    return EDbResult.Empty == dbResult || EDbResult.Success == dbResult;
                //} );
            }
        }
    }
}
