﻿
/// <summary>
/// 이벤트 수신 인터페이스
/// </summary>
public interface IEventListener
{
}

/// <summary>
/// 이벤트 수신 인터페이스
/// </summary>
public interface IPacketEventListener : IEventListener
{
    /// <summary>
    /// 패킷 수신시 
    /// </summary>
    public void OnReceivePacket( PacketHeader packet );

    /// <summary>
    /// 패킷 송신시 
    /// </summary>
    public void OnSend( PacketHeader packet );
}
