﻿namespace Mala.Common.Ai;


public interface IAiEvent
{
}

public interface IFsm< TState > where TState : Enum
{
    public void Transit( TState state );
    public void OnRecvEvent( int msg );
}

/// <summary>
/// 유한 상태 머신
/// </summary>
public class Fsm< TActor, TState, TFsmState  > : IFsm< TState >
    where TActor : new()
    where TState : Enum
    where TFsmState : FsmState< TActor, TState >
{
    protected Dictionary< TState, TFsmState > _states = new();

    public TActor Owner { get; set; }

    public TFsmState CurState { get; set; }

    public void Tick( TimeSpan deltaTime )
    {
        CurState.OnTick( deltaTime );
        CurState.TransitIfNeeded();
    }

    public void Add( TState stateType, TFsmState state )
    {
        _states[ stateType ] = state;
        state.Fsm = this;
    }

    public void Transit( TState state )
    {
        CurState?.OnExit();
        CurState = _states[ state ];
        CurState.OnEnter();
    }

    public void OnRecvEvent( int msg )
    {
        CurState.OnRecvEvent( msg );
    }
}
