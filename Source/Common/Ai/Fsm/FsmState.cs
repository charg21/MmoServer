﻿using Mala.Threading;
using System.Text.Json.Serialization;

namespace Mala.Common.Ai;

/// <summary>
/// 유한 상태 머신 상태
/// </summary>
public class FsmState< TActor, TState >
    where TActor : new()
    where TState : Enum
{
    public TActor Owner { get; set; }
    public TState State { get; set; }

    public IFsm< TState > Fsm { get; set; } = null;

    public virtual void OnEnter()
    {
    }

    public virtual void OnTick( TimeSpan deltaTime )
    {

    }

    public virtual void OnRecvEvent( int job )
    {
    }

    public virtual void OnExit()
    {
    }

    public virtual void TransitIfNeeded()
    {
    }
}