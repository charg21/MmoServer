﻿using Mala.Common.Ai;


/// <summary>
/// FSM 행동 트리
/// </summary>
public class BehaivorTreePage
{
    /// <summary>
    /// 페이지 식별자
    /// </summary>
    public int PageId { get; set; } = 0;

    /// <summary>
    /// 페이지 식별자
    /// </summary>
    public PageBehaviorTree Owner { get; set; }

    /// <summary>
    /// 행동 트리
    /// </summary>
    protected BehaviorTree _behaviorTree = new();

    /// <summary>
    ///
    /// </summary>
    public void Tick( TimeSpan deltaTime ) => _behaviorTree.Tick( deltaTime );

    /// <summary>
    /// 페이지 진입시 호출되는 콜백
    /// </summary>
    public virtual void OnEnter() { }

    /// <summary>
    /// 페이지 퇴장시 호출되는 콜백
    /// </summary>
    public virtual void OnExit() { }
}

/// <summary>
/// 페이지( FSM )기반 행동 트리
/// </summary>
public class PageBehaviorTree
{
    /// <summary>
    /// 페이지
    /// </summary>
    Dictionary< int, BehaivorTreePage > Pages = new();

    /// <summary>
    /// 현재 페이지
    /// </summary>
    BehaivorTreePage? CurrentPage;

    public void Tick( TimeSpan deltaTime )
    {
        CurrentPage?.Tick( deltaTime );
    }

    public PageBehaviorTree AddPage( BehaivorTreePage page )
    {
        Pages.Add( page.PageId, page );
        return this;
    }

    public PageBehaviorTree AddAndSetPage( BehaivorTreePage page )
    {
        CurrentPage = page;
        return AddPage( page );
    }

    public virtual void Initialize()
    {
    }

    public void ChangePage( int pageId )
    {
        if ( Pages.TryGetValue( pageId, out var page ) )
        {
            CurrentPage?.OnExit();
            CurrentPage = page;
            CurrentPage.OnEnter();
        }
    }


}
