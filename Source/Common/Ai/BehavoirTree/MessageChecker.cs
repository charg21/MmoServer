﻿using Mala.Threading;

namespace Mala.Common.Ai;

public abstract class MessageChecker : Checker
{
    public MessageChecker( BlackBoard blackboard )
    : base( blackboard )
    {
    }

    public override EBehaviorNodeState UpdateMessage( JobEntry message )
    {
        if ( Check( message ) )
            return EBehaviorNodeState.Success;

        return State;
    }
}


