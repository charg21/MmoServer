﻿namespace Mala.Common.Ai;

public abstract class Leaf : BehaviorTreeNode
{
    public Leaf( BlackBoard blackboard )
    : base()
    {
        _blackboard = blackboard;
    }

    BlackBoard _blackboard;
}

//public class Decorator : BehaviorTreeNode
//{
//    BehaviorTreeNode _children;

//    public EBehaviorNodeState State { get; set; }


//    public EBehaviorNodeState Execute( float deltaTime )
//    {
//        /// 조건을 체크한다.

//        return _children.Execute( deltaTime );
//    }

//    public EBehaviorNodeState OnJob( JobEntry deltaTime )
//    {
//        return EBehaviorNodeState.Failure;
//    }
//}

//public class Repeater : BehaviorTreeNode
//{
//    BehaviorTreeNode _children;
//    int targetCount { get; set; } = 0;
//    int curCount { get; set; } = 0;

//    public EBehaviorNodeState State { get; set; }

//    public EBehaviorNodeState Execute( float deltaTime )
//    {
//        /// 조건을 체크한다.
//        return _children.Execute( deltaTime );
//    }

//    public EBehaviorNodeState OnJob( JobEntry deltaTime )
//    {
//        return EBehaviorNodeState.Failure;
//    }
//}

//public class TaskNode : BehaviorTreeNode
//{
//    public EBehaviorNodeState State { get; set; }

//    public EBehaviorNodeState Execute( float deltaTime )
//    {
//        return EBehaviorNodeState.Failure;
//    }

//    public EBehaviorNodeState OnJob( JobEntry deltaTime )
//    {
//        return EBehaviorNodeState.Failure;
//    }
//}
