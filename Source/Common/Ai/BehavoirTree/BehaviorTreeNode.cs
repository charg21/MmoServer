﻿using Mala.Threading;

namespace Mala.Common.Ai;

/// <summary>
/// 행동 트리 노드
/// </summary>
public abstract class BehaviorTreeNode
{
    /// <summary>
    /// 상태
    /// </summary>
    internal EBehaviorNodeState State { get; set; } = EBehaviorNodeState.Invalid;

    /// <summary>
    /// 틱 함수
    /// </summary>
    public EBehaviorNodeState Tick( TimeSpan deltaTick )
    {
        if ( State != EBehaviorNodeState.Running )
            Initialize();

        State = Update( deltaTick );

        if ( State != EBehaviorNodeState.Running )
            Terminate( State );

        return State;
    }

    public EBehaviorNodeState OnMessage( JobEntry job )
    {
        if ( State != EBehaviorNodeState.Running )
            Initialize();

        State = UpdateMessage( job );

        if ( State != EBehaviorNodeState.Running )
            Terminate( State );

        return State;
    }

    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public virtual void Initialize() { }

    /// <summary>
    /// 중단 한다.
    /// </summary>
    public virtual void Terminate( EBehaviorNodeState state ) { }

    /// <summary>
    /// 갱신한다.
    /// </summary>
    public abstract EBehaviorNodeState Update( TimeSpan deltaTime );

    /// <summary>
    /// 메시지 수신을 처리한다.
    /// </summary>
    public abstract EBehaviorNodeState UpdateMessage( JobEntry job );

    /// <summary>
    /// 성공 여부
    /// </summary>
    public bool IsSuccess() => State == EBehaviorNodeState.Success;

    /// <summary>
    /// 실패 여부
    /// </summary>
    public bool IsFailure() => State == EBehaviorNodeState.Failure;

    /// <summary>
    /// 진행중 여부를 반환한다.
    /// </summary>
    public bool IsRunning() => State == EBehaviorNodeState.Running;

    /// <summary>
    /// 종료 여부를 반환한다.
    /// </summary>
    public bool IsTerminated() => IsSuccess() || IsFailure();

    void Reset() => State = EBehaviorNodeState.Invalid;

}
