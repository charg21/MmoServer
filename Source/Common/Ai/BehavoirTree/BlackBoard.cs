﻿namespace Mala.Common.Ai;

public interface IBlackBoard
{
}

public class BlackBoard : IBlackBoard
{
    public bool SetBool( string key, bool value ) => _bools.TryAdd( key, value );
    public bool SetInt( string key, bool value ) => _bools.TryAdd( key, value );
    public bool SetFloat( string key, bool value ) => _bools.TryAdd( key, value );
    public bool SetDouble( string key, bool value ) => _bools.TryAdd( key, value );
    public bool SetString( string key, bool value ) => _bools.TryAdd( key, value );


    public bool TryGetBool( string key, out bool value ) => _bools.TryGetValue( key, out value );
    public bool TryGetInt( string key, out int value ) => _ints.TryGetValue( key, out value );
    public bool TryGetFloat( string key, out float value ) => _floats.TryGetValue( key, out value );
    public bool TryGetDouble( string key, out double value ) => _doubles.TryGetValue( key, out value );
    public bool TryGetString( string key, out string value ) => _strings.TryGetValue( key, out value );


    public bool GetOrDefaultBool( string key ) => _bools.GetValueOrDefault( key );
    public int GetOrDefaultInt( string key ) => _ints.GetValueOrDefault( key );
    public float GetOrDefaultFloat( string key ) => _floats.GetValueOrDefault( key );
    public double GetOrDefaultDouble( string key ) => _doubles.GetValueOrDefault( key );
    public string GetOrDefaultString( string key ) => _strings.GetValueOrDefault( key );

    public bool HasBool( string key ) => _bools.ContainsKey( key );
    public bool HasInt( string key ) => _ints.ContainsKey( key );
    public bool HasFloat( string key ) => _floats.ContainsKey( key );
    public bool HasDouble( string key ) => _doubles.ContainsKey( key );
    public bool HasString( string key ) => _strings.ContainsKey( key );


    Dictionary< string, bool > _bools = new();
    Dictionary< string, int > _ints = new();
    Dictionary< string, float > _floats = new();
    Dictionary< string, double > _doubles = new();
    Dictionary< string, string > _strings = new();
}
