﻿namespace Mala.Common.Ai;


public enum EBehaviorNodeState
{
    /// <summary>
    /// 유효하지 않음
    /// </summary>
    Invalid,

    /// <summary>
    /// 성공
    /// </summary>
    Success,

    /// <summary>
    /// 실패
    /// </summary>
    Failure,

    /// <summary>
    /// 실행중
    /// </summary>
    Running,
}
