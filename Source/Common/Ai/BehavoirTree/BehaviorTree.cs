﻿using Mala.Threading;

namespace Mala.Common.Ai;


/// <summary>
/// 행동 트리 인터페이스
/// </summary>
public interface IBehaviorTree
{
}

/// <summary>
/// 행동 트리
/// </summary>
public class BehaviorTree
{
    public BehaviorTreeNode? Root { get; set; }

    public BlackBoard BlackBoard { get; private set; } = new();

    /// <summary>
    /// 생성자
    /// </summary>
    public BehaviorTree()
    {
    }


    public EBehaviorNodeState Tick( TimeSpan deltaTime )
    {
        if ( !HasRoot )
            return EBehaviorNodeState.Invalid;

        return Root!.Tick( deltaTime );
    }

    public bool IsTerminatedRoot
    {
        get
        {
            if ( Root is null )
                return true;

            return Root.IsTerminated();
        }
    }

    public bool HasRoot => Root != null;

}

/// <summary>
/// 메시지 기반 행동 트리( 이벤트 드리븐 )
/// </summary>
public class MessageTree : BehaviorTree
{
    /// <summary>
    /// 생성자
    /// </summary>
    public MessageTree()
    {
    }

    /// <summary>
    /// 메시지 수신시 처리한다.
    /// </summary>
    public EBehaviorNodeState OnMessage( JobEntry msg )
    {
        if ( !HasRoot )
            return EBehaviorNodeState.Invalid;

        return Root!.UpdateMessage( msg );
    }

}