﻿using Mala.Threading;

namespace Mala.Common.Ai;

public abstract class Checker : Leaf
{
    public Checker( BlackBoard blackboard )
    : base( blackboard )
    {
    }

    public abstract bool Check( JobEntry message );
}

