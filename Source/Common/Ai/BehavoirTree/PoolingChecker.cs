﻿using Mala.Threading;

namespace Mala.Common.Ai;

/// <summary>
/// 풀링을 통해 체크하는 노드
/// </summary>
public abstract class PoolingChecker : Checker
{
    public PoolingChecker( BlackBoard blackboard )
    : base( blackboard )
    {
    }

    public override EBehaviorNodeState UpdateMessage( JobEntry message )
    {
        return State;
    }

    public override EBehaviorNodeState Update( TimeSpan deltaTime )
    {
        if ( Check( null ) )
            return EBehaviorNodeState.Success;

        return State;
    }

}
