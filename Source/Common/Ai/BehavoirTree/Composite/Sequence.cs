﻿using Mala.Threading;

namespace Mala.Common.Ai;


/// <summary>
///
/// </summary>
public class Sequence : Composite
{
    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public override void Initialize()
    {
        Index = 0;
    }

    public override EBehaviorNodeState Update( TimeSpan deltaTime )
    {
        if ( !HasChildren() )
            return EBehaviorNodeState.Success;

        for ( ;; )
        {
            var child = _childrens[ Index ];
            var state = child.Tick( deltaTime );

            if ( state != EBehaviorNodeState.Success )
                return state;

            if ( ++Index == _childrens.Count )
                return EBehaviorNodeState.Success;
        }
    }

    public override EBehaviorNodeState UpdateMessage( JobEntry msg )
    {
        if ( !HasChildren() )
            return EBehaviorNodeState.Success;

        for ( ;; )
        {
            var child = _childrens[ Index ];
            var state = child.UpdateMessage( msg );

            if ( state != EBehaviorNodeState.Success )
            {
                return state;
            }
            else
            {
                if ( ++Index == _childrens.Count )
                    return EBehaviorNodeState.Success;
                else
                    return EBehaviorNodeState.Running;
            }
        }
    }

}