﻿namespace Mala.Common.Ai;

/// <summary>
/// 상태의 시작점
/// </summary>
public abstract class Composite : BehaviorTreeNode
{
    /// <summary>
    /// 자식 노드
    /// </summary>
    internal List< BehaviorTreeNode > _childrens = new();

    /// <summary>
    /// 인덱스
    /// </summary>
    public int Index { get; internal set; } = 0;

    /// <summary>
    /// 자식을 추가한다.
    /// </summary>
    public Composite AddChild( BehaviorTreeNode node )
    {
        _childrens.Add( node );
        return this;
    }

    /// <summary>
    /// 자식을 제거한다.
    /// </summary>
    public void RemvoeChild( BehaviorTreeNode node ) => _childrens.Remove( node );

    /// <summary>
    /// 자식 보유 여부를 반환한다.
    /// </summary>
    public bool HasChildren() => _childrens.Any();

}
