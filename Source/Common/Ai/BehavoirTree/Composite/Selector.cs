﻿using Mala.Threading;

namespace Mala.Common.Ai;

/// <summary>
/// 자식중 하나만 성공하면 성공
/// </summary>
public class Selector : Composite
{
    /// <summary>
    /// 초기화 한다.
    /// </summary>
    public override void Initialize()
    {
        Index = 0;
    }

    public override EBehaviorNodeState Update( TimeSpan deltaTime )
    {
        if ( !HasChildren() )
            return EBehaviorNodeState.Success;

        for ( ;; )
        {
            var child = _childrens[ Index ];
            var state = child.Tick( deltaTime );

            if ( state != EBehaviorNodeState.Failure )
                return state;

            if ( ++Index == _childrens.Count )
                return EBehaviorNodeState.Failure;
        }
    }

    public override EBehaviorNodeState UpdateMessage( JobEntry msg )
    {
        if ( !HasChildren() )
            return EBehaviorNodeState.Success;

        for ( ;; )
        {
            var child = _childrens[ Index ];
            var state = child.UpdateMessage( msg );

            if ( state != EBehaviorNodeState.Failure )
            {
                return state;
            }
            else
            {
                if ( ++Index == _childrens.Count )
                    return EBehaviorNodeState.Failure;
                else
                    return EBehaviorNodeState.Running;
            }
        }
    }
}