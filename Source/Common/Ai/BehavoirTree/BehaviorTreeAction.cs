﻿using Mala.Threading;

namespace Mala.Common.Ai;

public abstract class BehaviorTreeAction : Leaf
{
    public BehaviorTreeAction( BlackBoard blackboard )
    : base( blackboard )
    {
    }

    public override void Initialize()
    {
        State = EBehaviorNodeState.Running;
    }

    public override EBehaviorNodeState Update( TimeSpan deltaTime )
    {
        return EBehaviorNodeState.Success;
    }

    public override EBehaviorNodeState UpdateMessage( JobEntry job )
    {
        return State;
    }
}
