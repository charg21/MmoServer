using System;

[Table( "packet_log", "패킷 로그" )]
public class PacketLog
{
    [Required]
    [Index( "PRIMARY" )]
    [Field( "id" )]
    ulong Id;

    [Required]
    [Field( "time" )]
    DateTime Time;

    [Required]
    [Field( "body", "varchar(1024)" )]
    string body;
}
