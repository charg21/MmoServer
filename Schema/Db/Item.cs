using System;

[Table( "item", "아이템" )]
public class Item
{
    [Required]
    [Comment( "소유자 식별자" )]
    [Index( "PRIMARY" )]
    [Field( "owner_id" )]
    ulong OwnerId;

    [Required]
    [Comment( "실제 소유자 식별자( 컨텐츠별 owner가 달라지는 케이스용 실제 owner_id )" )]
    [Index( "IX_ROOT_OWNER_ID_AND_ID" )]
    [Field( "root_owner_id" )]
    ulong RootOwnerId;

    [Required]
    [Comment( "식별자" )]
    [Index( "PRIMARY" )]
    [Index( "IX_ROOT_OWNER_ID_AND_ID" )]
    [Index( "IX_ID" )]
    [Field( "id" )]
    [StrongType( "ItemId" )]
    ulong Id;

    [Required]
    [Comment( "기획 식별자" )]
    [Field( "design_id" )]
    [StrongType( "ItemDesignId" )]
    uint DesignId;

    [Required]
    [Comment( "갯수" )]
    [Field( "count" )]
    int Count;

    [Required]
    [Comment( "잠금 여부" )]
    [Field( "locked" )]
    bool Locked;

}
