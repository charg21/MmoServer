using System;

[Table( "quest", "퀘스트" )]
public class Quest
{
    [Required]
    [Comment( "소유자 식별자" )]
    [Index( "PRIMARY" )]
    [Field( "owner_id" )]
    ulong OwnerId;

    [Required]
    [Comment( "테이블 식별자" )]
    [Index( "PRIMARY" )]
    [Field( "design_id" )]
    uint DesignId;

    [Required]
    [Comment( "완료 횟수" )]
    [Field( "complete_count" )]
    int CompleteCount;
}
