using System;

[Table( "equip_slot", "장착 슬롯" )]
public class EquipSlot
{
    [Required]
    [Comment( "소유자 식별자" )]
    [Index( "PRIMARY" )]
    [Field( "owner_id" )]
    [StrongType( "PcId" )]
    ulong OwnerId;

    [Required]
    [Comment( "슬롯" )]
    [Index( "PRIMARY" )]
    [Field( "slot" )]
    ushort Slot;

    [Required]
    [Comment( "아이템 식별자" )]
    [Field( "item_id" )]
    [StrongType( "ItemId" )]
    ulong ItemId;

}
