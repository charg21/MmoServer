using System;

[Table( "stat", "스탯" )]
public class Stat
{
    [Required]
    [Index( "PRIMARY" )]
    [Field( "pc_id" )]
    [StrongType( "PcId" )]
    ulong PcId;

    [Required]
    [Index( "PRIMARY" )]
    [Field( "type" )]
    ulong Type;

    [Required]
    [Field( "value" )]
    ulong Value;
}
