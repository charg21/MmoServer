using System;

[Table( "perf_log", "성능 로그" )]
public class PerfLog
{
    [Required]
    [Index( "PRIMARY" )]
    [Field( "id" )]
    ulong Id;

    [Required]
    [Field( "time" )]
    DateTime Time;

    [Required]
    [Field( "gc_gen0" )]
    long GCGen0;

    [Required]
    [Field( "gc_gen1" )]
    long GCGen1;

    [Required]
    [Field( "gc_gen2" )]
    long GCGen2;

    [Required]
    [Field( "memory", "varchar(1024)" )]
    string Memory;

    [Required]
    [Field( "native_memory", "varchar(1024)" )]
    string NativeMemory;

    [Required]
    [Field( "working_set" )]
    long WorkingSet;

    [Required]
    [Field( "system_call" )]
    long SystemCall;

    [Required]
    [Field( "context_switching" )]
    long ContextSwitching;

    [Required]
    [Field( "system_recv_bytes" )]
    long SystemRecvBytes;

    [Required]
    [Field( "system_sent_bytes" )]
    long SystemSentBytes;

    [Required]
    [Field( "system_recv_packets" )]
    long SystemRecvPackets;

    [Required]
    [Field( "system_sent_packets" )]
    long SystemSentPackets;

    [Required]
    [Field( "global_job_count" )]
    long GlobalJobCount;

    [Required]
    [Field( "send_buffer_count" )]
    long SendBufferCount;

    [Required]
    [Field( "ccu" )]
    int Ccu;

    [Required]
    [Field( "npc" )]
    int Npc;

    [Required]
    [Field( "cpu_usage" )]
    float CpuUsage;

    [Required]
    [Field( "debug_log_count" )]
    long DebugLogCount;

    [Required]
    [Field( "info_log_count" )]
    long InfoLogCount;

    [Required]
    [Field( "critical_log_count" )]
    long CriticalLogCount;

    [Required]
    [Field( "error_log_count" )]
    long ErrorLogCount;

}
