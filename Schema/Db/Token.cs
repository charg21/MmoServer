using System;

[Table( "token", "토큰" )]
public class Token
{
    [Required]
    [Index( "PRIMARY" )]
    [Field( "user_id" )]
    ulong UserId;

    [Required]
    [Index( "PRIMARY" )]
    [Field( "id" )]
    int Id;

    [Required]
    [Field( "value" )]
    ulong Value;

    [Required]
    [Field( "expired" )]
    public DateTime Expired;
}
