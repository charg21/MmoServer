using System;

[Table( "user", "유저" )]
public class User
{
    [Required]
    [Index( "PRIMARY" )]
    [Field( "id" )]
    [StrongType( "UserId" )]
    ulong Id;

    [Required]
    [Field( "pc_id" )]
    [StrongType( "PcId" )]
    ulong PcId;

    [Required]
    [Index( "account" )]
    [Field( "account", "varchar(20)" )]
    string Account;

    [Required]
    [Field( "password", "varchar(20)" )]
    string Password;

    [Required]
    [Field( "mail", "varchar(20)" )]
    string Mail;
}
