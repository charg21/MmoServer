using System;

[Table( "pc", "플레이어 캐릭터" )]
public class Pc
{
    [Required]
    [Index( "PRIMARY" )]
    [Field( "user_id" )]
    [StrongType( "UserId" )]
    [Comment( "식별자" )]
    ulong UserId;

    [Required]
    [Index( "PRIMARY" )]
    [Index( "IX_ID" )]
    [Field( "id" )]
    [StrongType( "PcId" )]
    [Comment( "식별자" )]
    ulong Id;

    [Required]
    [Field( "name", "varchar(20)" )]
    [Comment( "이름" )]
    string Name;

    [Required]
    [Field( "hp" )]
    int Hp;

    [Required]
    [Field( "x" )]
    float X;

    [Required]
    [Field( "y" )]
    float Y;

    [Required]
    [Field( "z" )]
    float Z;
}
