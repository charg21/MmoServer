
/// <summary>
/// 패킷 정의
/// </summary>
#region Enum

[Comment( "결과 코드" )]
public enum ELogicResult : short
{
    [Comment( "성공" )]
    Success,
    [Comment( "작업중" )]
    Pending,
    [Comment( "Db 실패" )]
    DbFail,

    [Comment( "Pc X" )]
    NullPc,
    [Comment( "유저 X" )]
    NullUser,
    [Comment( "스킬 리스트 X" )]
    NullSkillList,
    [Comment( "전투 X" )]
    NullCombat,

    [Comment( "유효하지 기획 테이블 식별자" )]
    InvalidDesignId,
    [Comment( "보유하지 않은 스킬" )]
    UnownedSkill,
    [Comment( "보유하지 않은 아이템" )]
    UnownedItem,
    [Comment( "이미 죽은 Pc" )]
    AlreadyDeadPc,
    [Comment( "대상이 시야에 없음" )]
    TargetNotInView,
    [Comment( "대상이 범위에 내에 없음" )]
    TargetNotInRange,
    [Comment( "이미 캐스팅 중" )]
    AlreadyDoingCast,
    [Comment( "이미 추가된 아이템" )]
    AlreadyAddedItem,
    [Comment( "이미 잠금 상태인 아이템" )]
    AlreadyLockedItem,
    [Comment( "가방 공간 부족" )]
    NotEnoughBagSpace,
    Max
}

[Comment( "방향" )]
public enum EDirection : byte
{
    Left,
    Right,
    Up,
    Down,

    Max
}

[EnumFlag]
[Comment( "오브젝트 타입" )]
public enum EWorldObjectType : short
{
    [Comment( "없음" )]
    None = 0,
    [Comment( "플레이어블 캐릭터" )]
    Pc = 1 << 0,
    [Comment( "Npc" )]
    Npc = 1 << 1,
    [Comment( "설치물" )]
    Gadget = 1 << 2,
    [Comment( "투사체" )]
    Projectile = 1 << 3,

    Max,

    All = Pc | Npc | Gadget | Projectile,
}

/// <summary>
/// 스탯 타입
/// </summary>
public enum EStatType : short
{
    Exp,
    Hp,
    Power,
    Speed,
    Critical,
    Max,
}

/// <summary>
/// 채팅 타입
/// </summary>
public enum EChatType : short
{
    World,
    Global
}

/// <summary>
/// 장비 슬롯
/// </summary>
public enum EEquipSlot : byte
{
    Weapon,
	Armor,
    Max
};

#endregion

#region Struct

[StructSchema( category: "Common" )]
[Comment( "벡터3 구조체" )]
[CustomSerialization]
public class PktVector3
{
    public float _x;
    public float _y;
    public float _z;
}

[StructSchema( category: "Common" )]
[Comment( "벡터3 구조체" )]
[CustomSerialization]
public class PktVector2
{
    public float _x;
    public float _y;
}

[StructSchema( category: "Common" )]
[Comment( "캐릭터 구조체" )]
public class PktObject
{
    [Comment( "오브젝트 식별자" )]
    public ulong _objectId;

    [Comment( "좌표" )]
    public PktVector3 _pos;
    public float _yaw;
    public EWorldObjectType _objectType;
}

[StructSchema( category: "Common" )]
[Comment( "Pc 구조체" )]
public class PktPc
{
    [Comment( "오브젝트 식별자" )]
    public ulong _objectId;

    [Comment( "좌표" )]
    public PktVector3 _pos;

    public float _yaw;

    [Comment( "이름" )]
    public string _name;
}

[StructSchema( category: "Common" )]
[Comment( "Npc 구조체" )]
public class PktNpc
{
    [Comment( "오브젝트 식별자" )]
    public ulong _objectId;

    [Comment( "좌표" )]
    public PktVector3 _pos;

    public float _yaw;

    [Comment( "기획 테이블 식별자" )]
    public uint _designId;
}

[StructSchema( category: "Common" )]
[Comment( "스킬 구조체" )]
public class PktSkill
{
    public uint _skillDesignId;
    public ulong _lastUseTick;
}

[StructSchema( category: "Common" )]
[Comment( "퀘스트 구조체" )]
public class PktQuest
{
    public uint _designId;
}

[StructSchema( category: "Common" )]
[Comment( "아이템 구조체" )]
public class PktItem
{
    public ulong _id;
    public uint _designId;
    public int _count;
}

[StructSchema( category: "Common" )]
[Comment( "장착 정보 구조체" )]
public class PktEquip
{
    public ulong _itemId;
    public ushort _slot;
}

[StructSchema( category: "Common" )]
[Comment( "스탯 구조체" )]
public class PktStat
{
    public EStatType _type;
    public int _value;
}

#endregion // Structure

#region WorldPacket

[PacketSchema( from: "Client", to: "Server", category: "World", use: "World" )]
[Comment( "월드 입장" )]
public class C_EnterWorld
{
    public int _channel;
}

[PacketSchema( from: "Server", to: "World", category: "World" )]
[Comment( "월드 입장 결과" )]
public class S_EnterWorld
{
    private PktPc _my;
    private List< PktPc > _pcs;
    private List< PktNpc > _npcs;
}

[PacketSchema( from: "Server", to: "Client", category: "World" )]
[Comment( "서버 스폰" )]
public class S_Spawn
{
    private PktObject _other;
}

[PacketSchema( from: "Client", to: "Server", category: "World", use: "World" )]
[Comment( "월드 퇴장" )]
public class C_LeaveWorld
{
}

[PacketSchema( from: "Server", to: "Client", category: "World" )]
[Comment( "월드 퇴장 결과" )]
public class S_LeaveWorld
{
    private ulong _objectId;
}

[PacketSchema( from: "Server", to: "Client", category: "World" )]
[Comment( "서버 디스폰" )]
public class S_Despawn
{
    private ulong _objectId;
}

[PacketSchema( from: "Client", to: "Server", category: "World", use: "World" )]
[Comment( "이동" )]
public class C_Move
{
    PktVector3 _pos;
    PktVector3 _toPos;
    float _yaw;
}

[PacketSchema( from: "Server", to: "Client", category: "World" )]
[Comment( "이동 결과" )]
public class S_Move
{
    private ulong _objectId;
    public PktVector3  _pos;
    private PktVector3 _toPos;
    public float _yaw;
}

#endregion // WorldPacket
#region CombatPacket

[PacketSchema( from: "Server", to: "Client", category: "Combat" )]
[Comment( "스킬 목록" )]
public class S_SkillList
{
    [Comment( "스킬 디자인 식별자 목록" )]
    public List< uint > _skillDesignIdList;
}

[PacketSchema( from: "Client", to: "Server", category: "Combat", use: "World" )]
[Comment( "스킬 시작" )]
public class C_StartSkill
{
    [Comment( "스킬 대상 식별자( 논 타겟인 경우 0 가능 )" )]
    ulong _targetId;

    [Comment( "스킬 디자인 식별자" )]
    uint _skillDesignId;

    [Comment( "스킬 방향" )]
    float _yaw;

    [Comment( "시전 위치" )]
    PktVector3 _castPos;
}

[PacketSchema( from: "Server", to: "Client", category: "Combat" )]
[Comment( "스킬 결과" )]
public class S_StartSkill
{
    ELogicResult _result;

    [Comment( "스킬 디자인 식별자" )]
    uint _skillDesignId;

    [Comment( "스킬 디자인 식별자" )]
    ulong _casterId;

    [Comment( "스킬 대상 식별자( 논 타겟인 경우 0 가능 )" )]
    ulong _targetId;
}

[PacketSchema( from: "Server", to: "Client", category: "Combat" )]
[Comment( "스킬 통보" )]
public class S_Hit
{
    [Comment( "대상 식별자" )]
    public ulong _targetId;

    [Comment( "피해량" )]
    long _damage;
    long _curHp;

    [Comment( "스킬 디자인 식별자" )]
    uint _skillDesignId;

    [Comment( "캐스터 식별자" )]
    ulong _casterId;
}

#endregion // CombatPacket

[PacketSchema( from: "Client", to: "Server", category: "Auth" )]
[Comment( "로그인" )]
public class C_Login
{
    [Comment( "계정" )]
    string _account;

    [Comment( "비밀번호" )]
    string _password;
}

[PacketSchema( from: "Server", to: "Client", category: "Auth" )]
[Comment( "로그인 결과" )]
public class S_Login
{
    ELogicResult _result;

    [Comment( "User Id" )]
    ulong _userId;

    [Comment( "Pc Id 목록" )]
    List< ulong > _pcIds;
}

[PacketSchema( from: "Server", to: "Client", category: "Play" )]
[Comment( "핑" )]
public class S_Ping
{
}

[PacketSchema( from: "Client", to: "Server", category: "Play" )]
[Comment( "퐁" )]
public class C_Pong
{
}

[PacketSchema( from: "Client", to: "Server", category: "Chat" )]
[Comment( "채팅" )]
public class C_Chat
{
    public string _message;
}

[PacketSchema( from: "Server", to: "Client", category: "Chat" )]
[Comment( "채팅 결과" )]
public class S_Chat
{
    public ELogicResult _result;
    public string _name;
    public string _message;
}

[PacketSchema( from: "Client", to: "Server", category: "Auth" )]
[Comment( "캐릭터 로드" )]
public class C_LoadPc
{
    ulong _pcId;
    string _token;
}

[PacketSchema( from: "Server", to: "Client", category: "Auth" )]
[Comment( "캐릭터 로드 결과" )]
public class S_LoadPc
{
    public ELogicResult _result;

    public PktPc _pktPc;

    [Comment( "보유 아이템 목록" )]
    public List< PktItem > _items;

    [Comment( "장착 장비 목록" )]
    public List< PktEquip > _equips;

    [Comment( "보유 스킬 목록" )]
    public List< PktSkill > _skills;

    [Comment( "스탯 목록" )]
    public List< PktStat > _stats;

    [Comment( "퀘스트 목록" )]
    public List< PktQuest > _quests;

    //[Comment( "옵션 목록" )]
    //public List< PktOption > _options;
}

[PacketSchema( from: "Client", to: "Server", category: "Cheat" )]
[Comment( "치트" )]
public class C_Cheat
{
    string _command;
}

[PacketSchema( from: "Server", to: "Client", category: "Cheat" )]
[Comment( "치트 결과" )]
public class S_Cheat
{
    public ELogicResult _result;
}

[PacketSchema( from: "Server", to: "Client", category: "Cheat" )]
[Comment( "서버 메시지" )]
public class S_Message
{
    public string _message;
}

[PacketSchema( from: "Client", to: "Server", category: "Channel" )]
[Comment( "채널 목록" )]
public class C_ListChannel
{
}

[PacketSchema( from: "Server", to: "Client", category: "Channel" )]
[Comment( "채널 목록" )]
public class S_ListChannel
{
    List< int > _channels;
}

[PacketSchema( from: "Client", to: "Server", category: "Item", use: "World" )]
[Comment( "아이템 사용" )]
public class C_UseItem
{
    public int _itemId;
}

[PacketSchema( from: "Server", to: "Client", category: "Item" )]
[Comment( "아이템 사용" )]
public class S_UseItem
{
    public ELogicResult _result;
    public int _itemId;
}

[PacketSchema( from: "Server", to: "Client", category: "Item" )]
[Comment( "아이템 목록" )]
public class S_ListItem
{
    ValueList< PktItem > _items;
}

[PacketSchema( from: "Client", to: "Server", category: "Item", use: "World" )]
[Comment( "아이템 잠금" )]
public class C_LockItem
{
    public int _itemId;
}

[PacketSchema( from: "Server", to: "Client", category: "Item" )]
[Comment( "아이템 잠금 결과" )]
public class S_LockItem
{
    public ELogicResult _result;
    public int _itemId;
}

[PacketSchema( from: "Client", to: "Server", category: "Item", use: "World" )]
[Comment( "아이템 잠금 해제" )]
public class C_UnLockItem
{
    public int _itemId;
}

[PacketSchema( from: "Server", to: "Client", category: "Item" )]
[Comment( "아이템 잠금 결과" )]
public class S_UnLockItem
{
    public ELogicResult _result;
    public int _itemId;
}
