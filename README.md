# MmoServer.Net

.Net, Actor Model, MMORPG, SeamlessWorld

시연
===

단일 심리스 월드( 4000 x 4000 )

 NPC: 4만명( AI를 통해 랜덤 위치 이동  Update Tick 초당 20회 )

더미 클라이언트( Update Tick 초당 20회 )

기존서버 C# 포팅후 더미 클라이언트 500명: https://www.youtube.com/watch?v=Ia3pG5VpZeo  
최적화 후 2차 영상 더미 클라이언트 1'100명: https://youtu.be/XHuU92otwDM  
최적화 후 3차 영상 더미 클라이언트 4'000명: https://www.youtube.com/watch?v=ykbZ7M7qB3w  
RIO 도입 후 영상 더미 클라이언트 10'000명: https://www.youtube.com/watch?v=3XevfWYpZH0  


테스트 클라이언트
===
https://gitlab.com/charg21/MmoClient


프로젝트 구조
===

Executable
```Text
실행 파일
```

Schema
```Text
Packet 및 Db Orm 객체 스키마
```
Source

```Text
1. Core
    ㄴ 1.1 Mala: 프로젝트 Core 라이브러리
    ㄴ 1.2 Mala.CLI: Core C++/CLI 라이브러리
    ㄴ 1.2 Mala.Native: Core C++ 라이브러리( RIO 소켓모델 사용 )

2. Service
    ㄴ 2.1 DummyClient: 더미 클라이언트
    ㄴ 2.2 WorldServer: 월드( 이동, 시야처리, 전투 )로직이 돌아가는 서버

3. Shared
    ㄴ 3.1 Common: 콘텐츠 기반 공용 코드

4. Tools
    ㄴ 4.1 CodeGenerator: 코드 생성기 라이브러리
    ㄴ 4.2 OrmGenerator: Db 동기화 및 Db Orm 객체 코드 생성기
    ㄴ 4.3 ProtocolGenerator: 패킷 기반 Rpc 코드 생성기
```
목적
===

- C# 학습
- C#의 생산성 확인

RPC
===

Schema/Packet.cs에 정의 후 ProtocolGenerator.exe 파일을 실행하여 줍니다.

Schema/Packet.cs을 기반으로 동적으로 컴파일 하여 프로토콜 소스를 생성합니다.

프로토콜 정의
```cs 

/// <summary>
/// 열거형 정의
/// </summary>
[EnumFlag]
[Comment( "액터 타입" )]
public enum EActorType : short
{
    // 주석의 경우 아래와 같이 [Comment] 어트리뷰트를 사용해 작성합니다.
    // 일반 주석의 경우 컴파일시 메타 데이터가 남지 않음에 따라 어트리뷰트를 사용합니다.
    [Comment( "없음" )]
    None = 0,
    [Comment( "플레이어블 캐릭터" )]
    Pc = 1 << 0,
    [Comment( "Npc" )]
    Npc = 1 << 1,

    Max,

    All = Pc | Npc | Gadget | Projectile,

}

/// <summary>
/// 구조체 정의
/// </summary>
[ StructSchema( Category: "Common" ) ]
public class PktVector3
{
    public int _x;
    public int _y;
    public int _z;
}

/// <summary>
/// 패킷 정의( 예시 )
/// from: 발신처
/// to: 수신처
/// category: 카테고리
/// use: 패킷 사용 서비스( 바이패스등 처리 )
/// </summary>
[PacketSchema( from: "Client", to: "Server", category: "World", use: "World" )]
[Comment( "이동 요청" )]
public class C_Move
{
    PktVector3 _pos;
    PktVector3 _toPos;
    float _yaw;
}

````

결과물
---
```cs

/// Enum.cs
public enum EActorType : Int16
{
	None       = 0, /// 없음
	Pc         = 1 << 0, /// 플레이어 캐릭터
	Npc        = 1 << 1, /// Npc
	Gadget     = 1 << 2, /// 설치물
	Projectile = 1 << 3, /// 투사체
	Max        = 9, /// 
	All        = 15, /// 
};


/// PktVector3.cs
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct PktVector3
{
    public Single _x; /// 
    public Single _y; /// 
    public Single _z; /// 

    
    public bool Write( Span< byte > s, ref ushort count )
    {
        // ... 생략        
    }

    public void Read( ReadOnlySpan< byte > s, ref ushort count )
    {
        // ... 생략
    }
}


/// C_Move.cs
public partial class C_Move : PacketHeader
{
	public PktVector3 _pos = new();  /// 
	public PktVector3 _toPos = new();  /// 
	public Single _yaw; /// 

    public override SendSegment Write()
    {
        // ... 생략
    }

    public override void Read( ReadOnlySpan< byte > s )
    {
        // ... 생략
    }
}

/// 
[StructLayout( LayoutKind.Sequential, Pack = 1 )]
public partial struct ValueC_Move : IPacketHeader
{
    public PktVector3 _pos = new();  /// 
	public PktVector3 _toPos = new();  /// 
	public Single _yaw; /// 

	public SendSegment Write()
    {
        // ... 생략
    }

	public void Read( ReadOnlySpan< byte > s )
    {
        // ... 생략
    }
}


```

ORM
===

스키마 정의는 EF Core 인터페이스와 유사하게 cs 소스 파일로 테이블 정의 합니다.

```cs
[Comment( "아이템" )]
[Table( "item" )]
public class Item
{
    [Index( "ids" )]
    [Field( "owner_id" )]
    ulong OwnerId; // 소유자 식별자

    [Index( "ids" )]
    [Field( "info_id" )]
    uint InfoId; // 기획 테이블 식별자

    int Count;  // 갯수
}
```

결과물
---
- DB와 동기화 가능한 객체가 생성됩니다.

``` cs
/// <summary>
/// 툴에 의해 자동 생성된 코드입니다.
/// </summary>

using Mala.Core;
using Mala.Db;
using Mala.Db.Model;
using System;


/// <summary>
/// item Db 테이블 정보
/// </summary>
public class ItemTableInfo : TableInfo
{
    //... 생략
}

/// <summary>
/// item Db 테이블과 동기화 가능한 Db Orm 객체
/// </summary>
public class ItemDbModel : DbModel
{
    // ... 생략
}

/// <summary>
/// item Db 테이블 구현체( C#에서 다중 상속 미지원으로 Has-A 관계로 구현 )
/// </summary>
public partial class Item
{
    ItemDbModel _dbModel = new();

    // ... 생략
}
```

사용 예제
---
```cs

// Item.cs
public partial class Item
{
    // ... 추가 구현 내용 생략
}

// Program.cs
void DoSelectItem( ref DbExecutionContext dbContext )
{
    var item = new ItemDbModel();

    item.SetKeyOwnerId( 1234 ); // 키 세팅
    item.SetKeyInfoId ( 1    ); // 키 세팅
    item.BindAllField();     // 모든 컬럼을 bind( select ) 필드로 지정

    // 내부적으로 DB 쿼리를 생성 후 실행, 결과는 item객체에 반영한다.
    //
    // SELECT `owner_id`, `info_id`, `count` 
    // FROM `item` 
    // WHERE `owner_id` = 1234 and `info_id`;
    item.Select( ref dbContext );

    // 성공시 Item 객체의 bind된 컬럼 정보가 세팅된다.
    Console.WriteLine( $"owner_id: { item.GetOnwerId() }, info_id: { item.GetInfoId() } " );
}



```

모니터링 시스템
---
1. 서버 성능 지표 수집

- 서버에서 성능에 관련된 성능 지표 정보를 수집하여 DB에 저장합니다.

2. 정보 요청 및 가공
 - 프로메테우스 프로세스가 주기적으로 서버에 지표 정보를 요청하며, 서버는 DB에 저장된 지표정보를 가공하여 전달합니다.

3. 모니터링
 - 그라파나를 통해 수집된 지표 정보를 모니터링합니다.



![alt text](Docs/MonitoringSystem.png)